#include "defines.h"

module ColourTypes

  use Kinds
  implicit none
  private

  integer, parameter, public :: nc=3 !! The number of colours.

  !! Obsoloete.
  type real_vector
     sequence
     real(wp), dimension(nc) :: cl
  end type real_vector

  type colour_vector
     sequence
     complex(wc), dimension(nc) :: cl
  end type colour_vector

  type colour_matrix
     sequence
     complex(wc), dimension(nc,nc) :: cl
  end type colour_matrix

  type(colour_vector), parameter :: zero_vector = colour_vector( (/ 0,0,0 /) )
  type(colour_vector), parameter :: unit_vector_1 = colour_vector( (/ 1,0,0 /) )
  type(colour_vector), parameter :: unit_vector_2 = colour_vector( (/ 0,1,0 /) )
  type(colour_vector), parameter :: unit_vector_3 = colour_vector( (/ 0,0,1 /) )

  type(colour_matrix), parameter :: zero_matrix = colour_matrix( reshape( (/ 0,0,0,0,0,0,0,0,0 /), (/ 3,3 /) ) )
  type(colour_matrix), parameter :: unit_matrix = colour_matrix( reshape( (/ 1,0,0,0,1,0,0,0,1 /), (/ 3,3 /) ) )
  
  public :: real_vector
  public :: colour_vector, colour_matrix
  public :: random_colour_vector, random_colour_matrix
  public :: zero_vector, unit_vector_1,  unit_vector_2,  unit_vector_3
  public :: zero_matrix, unit_matrix

  !*To Do* Convert unit_matrix, unit_vector, etc into overloaded functions?
  !*To Do* Remove random_vector 
  public :: random_vector

contains

  subroutine random_vector(z)
    
    type(colour_vector), intent(out) :: z
    real(dp) :: zr(nc), zi(nc)
    
    call random_number(zr)
    call random_number(zi)

    z%cl = cmplx(zr,zi,dc)

  end subroutine random_vector

  subroutine random_colour_vector(z)
    
    type(colour_vector), intent(out) :: z
    real(dp) :: zr(nc), zi(nc)
    
    call random_number(zr)
    call random_number(zi)

    z%cl = cmplx(zr,zi,dc)

  end subroutine random_colour_vector

  subroutine random_colour_matrix(z)
    
    type(colour_matrix), intent(out) :: z
    real(dp) :: zr(nc,nc), zi(nc,nc)
    
    call random_number(zr)
    call random_number(zi)

    z%cl = cmplx(zr,zi,dc)

  end subroutine random_colour_matrix

end module ColourTypes
