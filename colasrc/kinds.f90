#include "defines.h"

!! Provides definitions of the kinds of complex and real scalars in single and double precision,
!! and also some useful constants. Quadruple precision kinds are also defined, if supported.
!! *Note*: Quadruple precision may not be supported by all architectures/compilers.

module Kinds

  implicit none
  public

  integer, parameter :: long = selected_int_kind(18) ! 64 bit integer.

  integer, parameter :: sp = kind(1.0) !! Single precision real scalars.
  integer, parameter :: dp = kind(1.0D0) !! Double precision real scalars.
#ifdef _quad_prec_
  integer, parameter :: qp = 2*kind(1.0D0) !! Quadruple precision real scalars.
#endif
  integer, parameter :: wp = dp ! Working precision for real arithmetic.

  integer, parameter :: sc = kind((1.0,1.0)) !! Single precision complex scalars.
  integer, parameter :: dc = kind((1.0D0,1.0D0)) !! Double precision complex scalars.
#ifdef _quad_prec_
  integer, parameter :: qc = 2*kind((1.0D0,1.0D0)) !! Quadruple precision complex scalars.
#endif
  integer, parameter :: wc = dc ! Working precision for complex arithmetic.

  real(dp), parameter :: pi=3.141592653589793238462643383279502884197_dp !! pi in double precision.
  real(dp), parameter :: two_pi = 6.283185307179586476925286766559005768394_dp !! 2*pi in double precision.
  real(dp), parameter :: half_pi = 1.57079632679489661923132169163975144209858_dp !! pi/2 in double precision.

  complex(sc), parameter :: i_sc = ( 0.0_sp, 1.0_sp ) !! The square root of -1, in single precision.
  complex(dc), parameter :: i_dc = ( 0.0_dp, 1.0_dp ) !! The square root of -1, in double precision.
#ifdef _quad_prec_
  complex(qc), parameter :: i_qc = ( 0.0d0, 1.0d0 )  !! The square root of -1, in quadruple precision.
#endif

  complex(wc), parameter :: i = ( 0.0_wp, 1.0_wp ) !! The square root of -1, in double precision.
  complex(wc), parameter :: i_wc = ( 0.0_wp, 1.0_wp ) !! The square root of -1, in double precision.

  
  !! Provides an "intrinsic" for generating random complex numbers.
  interface random_complex
     module procedure random_complex_sc
     module procedure random_complex_dc
#ifdef _quad_prec_
     module procedure random_complex_qc
#endif
  end interface

  private :: random_complex_sc,random_complex_dc
#ifdef _quad_prec_
  private :: random_complex_qc
#endif

contains

  !! Generates a random complex number in single precision.
  subroutine random_complex_sc(z)
    
    complex(sc), intent(out) :: z
    real(sp) :: zr, zi
    
    call random_number(zr)
    call random_number(zi)

    z = cmplx(zr,zi,sc)

  end subroutine random_complex_sc


  !! Generates a random complex number in double precision.
  subroutine random_complex_dc(z)
    
    complex(dc), intent(out) :: z
    real(dp) :: zr, zi
    
    call random_number(zr)
    call random_number(zi)

    z = cmplx(zr,zi,dc)

  end subroutine random_complex_dc

#ifdef _quad_prec_
  !! Generates a random complex number in quadruple precision.
  subroutine random_complex_qc(z)
    
    complex(qc), intent(out) :: z
    real(qp) :: zr, zi
    
    call random_number(zr)
    call random_number(zi)

    z = cmplx(zr,zi,qc)

  end subroutine random_complex_qc
#endif

end module Kinds
