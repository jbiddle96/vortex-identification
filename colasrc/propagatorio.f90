#include "defines.h"
#include "fermionfields.h"

module PropagatorIO
  use Kinds
  use TextIO
  use Strings
  use FermionAction
  use ColourTypes
  use SpinorTypes
  use FermionTypes
  use LatticeSize
  use MPIInterface
  use FermionField
  use FermionFieldIO
  use RealFieldMPIComms
  use ComplexFieldMPIComms
  use FermionFieldMPIComms
  use QuarkPropagator
  use FermionAction
  use GaugeField
  use QuarkPropTypes
  use SourceTypes
  use FermionAlgebra
  use ColourFieldOps
  implicit none
  private

  public :: GetPropSuffix
  public :: GetPropFilename
  public :: ReadPropagator
  public :: WritePropagator
  !public :: ReadInverseFMSoln
  !public :: WriteInverseFMSoln
  public :: ReadInverseFMSoln_EO
  public :: WriteInverseFMSoln_EO
  public :: InitialiseSource
  public :: PrepareSource
  public :: FinaliseSource
  public :: WriteIFMSProp
  !public :: PropToTimeSlice
  !public :: ReadPropagatorColumn
  !public :: WritePropagatorColumn
  
  character(len=_FILENAME_LEN_), public :: qpio_GaugeFieldFile
  character(len=32), public :: qpio_GaugeFieldFileFormat
  character(len=_FILENAME_LEN_), public :: qpio_PropFilePrefix
  character(len=32), public :: qpio_PropFileFormat = "ifms"
  !logical, public :: qpio_parallel
  character(len=8), public :: qpio_version = "1.0"
  character(len=128), public :: qpio_header 
  real(dp), public :: ifms_u0check

  character(len=*), public, parameter :: ifms_header = "! binary file format = ifms, qpio_version = 1.0, gamma_basis = chiral, fm_preconditioner = even_odd"
  character(len=*), public, parameter :: prop_header = "! binary file format = prop, qpio_version = 1.0, gamma_basis = sakurai"
  character(len=*), public, parameter :: prop_dp_header = "! binary file format = prop_dp, qpio_version = 1.0, gamma_basis = sakurai"
  character(len=*), public, parameter :: soln_header = "! binary file format = soln, qpio_version = 1.0, gamma_basis = chiral"
  character(len=*), public, parameter :: tsp_header = "! binary file format = tsp, qpio_version = 1.0, gamma_basis = sakurai"

  type IFMSSizeType
     integer :: n_x
     integer :: n_v
  end type IFMSSizeType

  type(IFMSSizeType), public :: ifms_size

  
contains


  subroutine ReadQuarkPropIOParams(file_unit)
    integer :: file_unit
    
    call Get(qpio_GaugeFieldFile,file_unit)
    call Get(qpio_GaugeFieldFileFormat,file_unit)
    call Get(qpio_PropFilePrefix,file_unit)
    call Get(qpio_PropFileFormat,file_unit)

  end subroutine ReadQuarkPropIOParams

  subroutine GetQuarkPropIOParams(file_stub)

    character(len=*) :: file_stub

    character(len=_FILENAME_LEN_) :: qpiofile
    integer :: file_unit

    qpiofile = trim(file_stub)//".qpio"
    
    call OpenTextfile(trim(qpiofile),file_unit,action='read')
    call ReadQuarkPropIOParams(file_unit)
    call CloseTextfile(file_unit)

    call Put("Propagator IO parameters:")
    call Put("Input configuration:"//trim(qpio_GaugeFieldFile))
    call Put("Gaugefield file format:"//trim(qpio_GaugeFieldFileFormat))
    call Put("Propagator file prefix:"//trim(qpio_PropFilePrefix))
    call Put("Propagator file format:"//trim(qpio_PropFileFormat))
    !call Put("Use parallel io?:"//str(qpio_parallel,6))

  end subroutine GetQuarkPropIOParams

  subroutine GetPropFilename(PropFile,Prefix,Suffix,kappa)

    character(len=*) :: PropFile
    character(len=*) :: Prefix
    character(len=*) :: Suffix
    real(DP) :: kappa !quark mass

    character(len=7) :: kappaString

    write( kappaString, '(F7.5)') kappa
    kappaString = "k" // kappaString(3:7)
    PropFile = trim(Prefix) // trim(kappaString) // trim(Suffix)

  end subroutine GetPropFilename

  subroutine GetPropSuffix(PropFileFormat,Suffix)
    character(len=*) :: PropFileFormat
    character(len=*) :: Suffix

    if ( trim(PropFileFormat) == "prop_dp" ) then
       Suffix = ".prop"
    else
       Suffix = "."//trim(PropFileFormat)
    end if

  end subroutine GetPropSuffix

  subroutine IFMSToProp_eo(InvFMS_eo,psipsibar,ifms)
    type(dirac_fermion), dimension(:,:) :: InvFMS_eo
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar !solution vector
    type(IFMSMetaDataType) :: ifms

    integer :: i_v, jc, js
    _fermion_field_type_4x_, allocatable :: chi
    _fermion_field_type_eo_, allocatable :: chi_e, chi_o
    _fermion_field_type_eo_, allocatable :: Dchi_o, rho_o
    real(dp) :: err
    ! Due to reconstruction from single precision, need to relax tolerance slightly.
    real(dp), parameter :: eps = 2.0*epsilon(1.0) 

    allocate(chi(_fermion_field_extents_4x_))
    allocate(chi_o(_fermion_field_extents_eo_))
    allocate(chi_e(_fermion_field_extents_eo_))
    allocate(rho_o(_fermion_field_extents_eo_))
    allocate(Dchi_o(_fermion_field_extents_eo_))

    do js=1,ns
       do jc=1,nc
          call ifms%GetSource(chi,js,jc,U_xd)
          i_v = jc + (js-1)*nc
          call InvLeftILUMatrix(chi,chi_e,rho_o)
          chi_o(1:n_xeo) = InvFMS_eo(1:n_xeo,i_v)

          call D_eo(chi_o,Dchi_o)
          call PsiMinusPhi(Dchi_o,rho_o)
          err = fermion_norm(Dchi_o)/fermion_norm(rho_o)
          if ( err > ifms%tolerance + eps ) call Abort("IFMSToProp_eo: Failed: solution exceeds tolerance."// &
               & "js="//str(js,5)//"jc="//str(jc,5)//"err="//str(err,24))
          call InvRightILUMatrix(chi_e,chi_o,chi)
          call PutPropColumn(chi,js,jc,psipsibar)
       end do
    end do

    deallocate(chi, chi_e, chi_o)
    deallocate(rho_o, Dchi_o)

  end subroutine IFMSToProp_eo

  subroutine PropToIFMS_eo(psipsibar,InvFMS_eo)
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar !solution vector
    type(dirac_fermion), dimension(:,:) :: InvFMS_eo

    integer :: i_v, jc, js
    _fermion_field_type_4x_, allocatable :: chi
    _fermion_field_type_eo_, allocatable :: chi_e, chi_o

    allocate(chi(_fermion_field_extents_4x_))
    allocate(chi_o(_fermion_field_extents_eo_))
    allocate(chi_e(_fermion_field_extents_eo_))

    do js=1,ns
       do jc=1,nc
          i_v = jc + (js-1)*nc
          call GetPropColumn(chi,js,jc,psipsibar)
          call RightILUMatrix(chi,chi_e,chi_o)
          InvFMS_eo(1:n_xeo,i_v) = chi_o(1:n_xeo)
       end do
    end do

    deallocate(chi, chi_e, chi_o)
  end subroutine PropToIFMS_eo

  subroutine InitialiseSource(qp,file_stub)
    class(QPMetaDataType) :: qp
    character(len=*) :: file_stub

    character(len=_FILENAME_LEN_) :: qpsrcfile

    select case(qp%iSourceType)
    case(1)
       qpsrcfile = trim(file_stub)//".qpsrc_pt"
    case(2)
       qpsrcfile = trim(file_stub)//".qpsrc_wl"
    case(3)
       qpsrcfile = trim(file_stub)//".qpsrc_sm"
    case(4)
       qpsrcfile = trim(file_stub)//".qpsrc_ns"
    case(5)
       qpsrcfile = trim(file_stub)//".qpsrc_sst"
    end select

    call SelectSourceType(qp%qpsrc,qp%iSourceType)
    call qp%qpsrc%Read(qpsrcfile)

    select type(src => qp%qpsrc)
    type is (PointSourceType)
       call Put("Point source parameters")
       call Put("qpsrc_pt%jx="//str(src%jx,6))
       call Put("qpsrc_pt%jy="//str(src%jy,6))
       call Put("qpsrc_pt%jz="//str(src%jz,6))
       call Put("qpsrc_pt%jt="//str(src%jt,6))

    type is (WallSourceType)
       call Put("Wall source parameters")
       call Put("qpsrc_wl%jt="//str(src%jt,6))
    type is (SmearedSourceType)
       call Put("Smeared source parameters")
       call Put("qpsrc_pt%jx="//str(src%jx,6))
       call Put("qpsrc_pt%jy="//str(src%jy,6))
       call Put("qpsrc_pt%jz="//str(src%jz,6))
       call Put("qpsrc_pt%jt="//str(src%jt,6))
       call Put("qpsrc_sm%n_smsrc="//str(src%n_smsrc,6))
       call Put("qpsrc_sm%alpha_smsrc="//str(src%alpha_smsrc,20))
       call Put("qpsrc_sm%UseUzero="//str(src%UseUzero,6))
       call Put("qpsrc_sm%u0_smsrc="//str(src%u0_smsrc,20))
       call Put("qpsrc_sm%UseStoutLinks="//str(src%UseStoutLinks,6))
       call Put("qpsrc_sm%alpha_stout="//str(src%alpha_stout,20))
       call Put("qpsrc_sm%n_stout="//str(src%n_stout,6))

    type is (NoiseSourceType)
       call Put("Noise source parameters") 
       call Put("Noise Source file name:"//trim(src%NoiseFile))
       call Put("qpsrc_ns%iColourDilution="//str(src%iColourDilution,24))
       call Put("qpsrc_ns%iSpinDilution="//str(src%iSpinDilution,24))
       call Put("qpsrc_ns%iTimeDilution="//str(src%iTimeDilution,24))
    type is (SSTSourceType)
       call Put("SST source parameters")
       call Put("qpsrc_sst%SourcePropFile="//trim(src%SourcePropFile))
       call Put("qpsrc_sst%kappa="//str(src%kappa,24))
       call Put("qpsrc_sst%j_mu="//str(src%j_mu,6))
       call Put("qpsrc_sst%it_s="//str(src%it_s,6))
       call Put("qpsrc_sst%j_x="//str(src%j_x,18,'(3i6)'))
       call Put("qpsrc_sst%p_x="//str(src%p_x,18,'(3i6)'))
       call Put("qpsrc_sst%currentType="//str(src%currentType,6))
    end select

    call PrepareSource(qp)

  end subroutine InitialiseSource

  subroutine PrepareSource(qp)
    class(QPMetaDataType) :: qp
    integer :: NoiseFileID
    character(len=32) :: FileFormat
    type(QPMetaDataType) :: qp_md

    select type(src => qp%qpsrc)
    type is (SSTSourceType)
       allocate(qp_sst_source_prop(nx,ny,nz,nt))
       call GetFileFormatFromMetaDataFile(FileFormat,trim(src%SourcePropFile)//".metadata")
       call ReadPropagator(src%SourcePropFile,qp_sst_source_prop,FileFormat,parallel_io, qp_metadata = qp_md)
       if (trim(qp_md%FermActName) /= trim(qp%FermActName)) call Abort("PrepareSource: fermion action mismatch.")
#ifdef _chiral_basis_
       call SakuraiBasisToChiral(qp_sst_source_prop)
#endif
    type is (NoiseSourceType)
       call src%diln%Initialise(src%iColourDilution,src%iSpinDilution,src%iTimeDilution)
       call CreateFermionField(qp_noise_field,1)
       call Put("Reading noise source: "//trim(src%NoiseFile))
       call OpenFermionFieldVectorFile(trim(src%NoiseFile),NoiseFileID,"read")
       call ReadFermionField_bin(qp_noise_field,NoiseFileId,1)
       call CloseFermionFieldVectorFile(NoiseFileID)
    end select

  end subroutine PrepareSource

  subroutine FinaliseSource(qp)
    class(QPMetaDataType) :: qp
    
    select type(src => qp%qpsrc)
    type is (SSTSourceType)
       if (associated(qp_sst_source_prop) ) deallocate(qp_sst_source_prop)
    type is (NoiseSourceType)
       call DestroyFermionField(qp_noise_field)
    end select

  end subroutine FinaliseSource

  subroutine WriteIFMSProp(filename, InvFMS_eo, file_format, parallel_io, ifms)

    character(len=*) :: filename
    type(dirac_fermion), dimension(:,:) :: InvFMS_eo
    character(len=*) :: file_format
    logical :: parallel_io
    type(IFMSMetaDataType) :: ifms

    type(spin_colour_matrix), dimension(:,:,:,:), allocatable :: psipsibar !solution vector

    call GetUZero(U_xd,ifms%u0check)
    if ( trim(file_format)=="ifms" ) then
       ifms%header = ifms_header
       call WriteInverseFMSoln_EO(filename,InvFMS_eo,parallel_io)
       call ifms%Write(trim(filename)//".metadata")
    else
       allocate(psipsibar(nx,ny,nz,nt))
       call IFMSToProp_eo(InvFMS_eo,psipsibar,ifms)
#ifdef _chiral_basis_
       call ChiralBasisToSakurai(psipsibar)
#endif
       call WritePropagator(filename,psipsibar,file_format,parallel_io,ifms%QPMetaDataType)
       deallocate(psipsibar)
    end if

  end subroutine WriteIFMSProp

  subroutine WritePropagator(filename, psipsibar, file_format, parallel_io, qp_metadata)

    character(len=*) :: filename
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar !solution vector
    character(len=*) :: file_format
    logical :: parallel_io
    type(QPMetaDataType), optional :: qp_metadata

    logical :: init_eo

    type(dirac_fermion), dimension(:,:), allocatable :: InvFMS_eo
    type(IFMSMetaDataType) :: ifms

    select case (trim(file_format))
    case("prop")
       call WritePropagator_prop(filename,psipsibar,parallel_io)
       if ( present(qp_metadata) ) then
          qp_metadata%header = prop_header
          call qp_metadata%Write(trim(filename)//".metadata")
       end if
    case("prop_dp")
       call WritePropagator_prop_dp(filename,psipsibar,parallel_io)
       if ( present(qp_metadata) ) then
          qp_metadata%header = prop_dp_header
          call qp_metadata%Write(trim(filename)//".metadata")
       end if
    case("tsp")
       call WritePropagator_tsp(filename,psipsibar,parallel_io)
       if ( present(qp_metadata) ) then
          qp_metadata%header = tsp_header
          call qp_metadata%Write(trim(filename)//".metadata")
       end if
    case("soln")
       call WritePropagator_soln(filename,psipsibar,parallel_io)
       if ( present(qp_metadata) ) then
          ifms%QPMetaDataType = qp_metadata
          ifms%header = soln_header
          ifms%n_x = nlattice
          ifms%n_v = nc*ns
          call ifms%Write(trim(filename)//".metadata")
       end if
    case("ifms")
       if ( .not. allocated(U_xd) ) then
          call Abort("WritePropagator: Gauge field must be initialised to write propagators in ifms format")
       end if
       allocate(InvFMS_eo(n_xeo,nc*ns))
       init_eo = .not. eo_initialised
       ifms%QPMetaDataType = qp_metadata
       if ( init_eo ) call InitialiseFermionAction(U_xd,ifms%kappa,ifms%kBFStrength,even_odd=.true.)
#ifdef _chiral_basis_
       call SakuraiBasisToChiral(psipsibar)
#endif
       call PropToIFMS_eo(psipsibar,InvFMS_eo)
       call WriteInverseFMSoln_EO(filename,InvFMS_eo,parallel_io)
       ifms%n_x = nlattice_eo
       ifms%n_v = size(InvFMS_eo,dim=2)
       ifms%nl_d = (/ nlx, nly, nlz, nlt /)
       ifms%header = ifms_header
       call GetUZero(U_xd,ifms%u0check)
       call ifms%Write(trim(filename)//".metadata")
       if ( init_eo ) call FinaliseFermionAction(even_odd=.true.)
       deallocate(InvFMS_eo)
    case default
       call Abort("WritePropagator: Unrecognised propagator format code:"//trim(file_format))
    end select

  end subroutine WritePropagator

  subroutine ReadPropagator(filename, psipsibar, file_format, parallel_io, sst_source_prop, qp_metadata)

    character(len=*) :: filename
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar !solution vector
    character(len=*) :: file_format
    logical :: parallel_io
    type(spin_colour_matrix), dimension(:,:,:,:), target, optional :: sst_source_prop !solution vector
    type(QPMetaDataType), optional :: qp_metadata

    type(dirac_fermion), dimension(:,:), allocatable :: InvFMS_eo
    logical :: init_eo
    real(dp) :: u0check
    type(IFMSMetaDataType) :: ifms

    select case (trim(file_format))
    case("prop")
       call ReadPropagator_prop(filename,psipsibar,parallel_io)
       if ( present(qp_metadata) ) call qp_metadata%Read(trim(filename)//".metadata")
    case("prop_dp")
       call ReadPropagator_prop_dp(filename,psipsibar,parallel_io)
       if ( present(qp_metadata) ) call qp_metadata%Read(trim(filename)//".metadata")
    case("tsp")
       call ReadPropagator_tsp(filename,psipsibar,parallel_io)
    case("csp")
       call ReadPropagator_csp(filename,psipsibar,parallel_io)
    case("soln")
       call ReadPropagator_soln(filename,psipsibar,parallel_io)
       if ( present(qp_metadata) ) then
          call ifms%Read(trim(filename)//".metadata")
          qp_metadata = ifms%QPMetaDataType
       end if
    case("FLIC6","flic6")
       call ReadPropagator_flic6(filename,psipsibar,parallel_io)
    case("ifms")
       if ( .not. allocated(U_xd) ) then
          call Abort("ReadPropagator: Gauge field must be initialised to read propagators in ifms format")
       end if
       call ifms%Read(trim(filename)//".metadata")
       call GetUZero(U_xd,u0check)
       if ( abs(u0check-ifms%u0check) > 1.0e-6 ) call Abort("ReadPropagator: Gauge field u0 mismatch: u0 = "//str(u0check,12,'(f12.8)')//" /= "//str(ifms%u0check,12,'(f12.8)'))
       call SetFermionActionParams(ifms%fermact)
       call SelectFermionAction(ifms%FermActName)
       init_eo = .not. eo_initialised
       if ( init_eo ) call InitialiseFermionAction(U_xd,ifms%kappa,ifms%kBFStrength,even_odd=.true.)
       allocate(InvFMS_eo(n_xeo,nc*ns))
       call ReadInverseFMSoln_EO(filename,InvFMS_eo,parallel_io)
       if ( ifms%iSourceType == 5 ) then
          if ( present(sst_source_prop) ) then
#ifdef _chiral_basis_
             call SakuraiBasisToChiral(sst_source_prop)
#endif
             qp_sst_source_prop => sst_source_prop
          end if
          if ( .not. associated(qp_sst_source_prop) ) call Abort("ReadPropagator: The 2 point propagator is needed to read 3 point propagators in ifms format.")
       end if
       call IFMSToProp_eo(InvFMS_eo,psipsibar,ifms)

#ifdef _chiral_basis_
       call ChiralBasisToSakurai(psipsibar)
       if (ifms%iSourceType == 5 .and. present(sst_source_prop)) call ChiralBasisToSakurai(sst_source_prop)
#endif
       if ( init_eo ) call FinaliseFermionAction(even_odd=.true.)
       deallocate(InvFMS_eo)
       if ( present(qp_metadata) ) qp_metadata = ifms%QPMetaDataType
    case default
       call Abort("ReadPropagator: Unrecognised propagator format code:"//trim(file_format))
    end select

    
  end subroutine ReadPropagator

  subroutine WritePropagatorColumn(OutputPrefix,OutputSuffix,kappa,chi,parallel_io)
    ! begin args: OutputPrefix,OutputSuffix,kappa,chi,parallel_io

    character(len=*) :: OutputPrefix
    character(len=*) :: OutputSuffix
    real(DP) :: kappa !quark mass
    type(colour_vector), dimension(:,:,:,:,:) :: chi !solution vector
    logical :: parallel_io

    ! begin local_vars
    character(len=273) :: PropagatorFile
    character(len=8) :: PropagatorSuffix
    character(len=6) :: ProcSuffix

    !Write the propagator out as a sequence of fermion field vectors.
    !The ordering for each propagator column is colour(c) varies first and then spin(s).

    ! begin execution
    if ( parallel_io ) then
       write( PropagatorSuffix, '(a,F7.5)') "k", kappa
       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       PropagatorFile = trim(OutputPrefix) // PropagatorSuffix // trim(OutputSuffix) // ProcSuffix

       open (200+mpi_rank, file=PropagatorFile, status = "unknown", form = "unformatted", action = "write", position = "append" )
       call WriteSplitFermionField(chi,200+mpi_rank)
       close(200+mpi_rank)
    else
       if ( i_am_root ) then
          write( PropagatorSuffix, '(a,F7.5)') "k", kappa
          PropagatorFile = trim(OutputPrefix) // PropagatorSuffix // trim(OutputSuffix)
          open (200, file=PropagatorFile, status = "unknown", form = "unformatted", position = "append", action = "write")
       end if

       call WriteFermionField(chi,200)
       if ( i_am_root ) close(200)
    end if

  end subroutine WritePropagatorColumn

  subroutine ReadPropagatorColumn(InputPrefix,kappa,chi,parallel_io)
    ! begin args: InputPrefix, kappa, chi

    character(len=*) :: InputPrefix
    real(DP) :: kappa !quark mass
    type(colour_vector), dimension(:,:,:,:,:) :: chi !solution vector
    logical :: parallel_io

    ! begin local_vars
    character(len=273) :: PropagatorFile
    character(len=8) :: PropagatorSuffix
    character(len=6) :: ProcSuffix

    !Read the propagator out as a sequence of fermion field vectors.
    !The ordering for each propagator column is colour(c) varies first and then spin(s).

    ! begin execution
    if ( parallel_io ) then
       write( PropagatorSuffix, '(a,F7.5)') "k", kappa
       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       PropagatorFile = trim(InputPrefix) // PropagatorSuffix // ".csp" // ProcSuffix

       open (200+mpi_rank, file=PropagatorFile, status = "unknown", form = "unformatted", action = "read", position = "append" )
       call ReadSplitFermionField(chi,200+mpi_rank)
       close(200+mpi_rank)
    else
       if ( i_am_root ) then
          write( PropagatorSuffix, '(a,F7.5)') "k", kappa
          PropagatorFile = trim(InputPrefix) // PropagatorSuffix // ".csp"
          open (200, file=PropagatorFile, status = "unknown", form = "unformatted", position = "append", action = "read")
       end if

       call ReadFermionField(chi,200)
       if ( i_am_root ) close(200)
    end if

  end subroutine ReadPropagatorColumn

  subroutine WritePropagator_csp(PropFile,psipsibar,parallel_io)
    ! begin args: propfile,psipsibar,parallel_io

    character(len=*) :: PropFile
    real(DP) :: kappa !quark mass
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar !solution vector
    logical :: parallel_io

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: MyPropFile
    character(len=6) :: ProcSuffix

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi !solution vector
    integer :: jc, js

    !Write the propagator out as a sequence of fermion field vectors.
    !The ordering for each propagator column is colour(c) varies first and then spin(s).

    allocate(chi(nx,ny,nz,nt,ns))

    ! begin execution
    if ( parallel_io ) then
       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       MyPropFile = trim(PropFile) // ProcSuffix
       open (200+mpi_rank, file=MyPropFile, status = "unknown", form = "unformatted", action = "write")
       do js=1,ns
          do jc=1,nc
             call GetPropColumn(chi,js,jc,psipsibar)
             call WriteSplitFermionField(chi,200+mpi_rank)
          end do
       end do
       close(200+mpi_rank)
    else
       if ( i_am_root ) then
          open (200, file=PropFile, status = "unknown", form = "unformatted", action = "write")
       end if
       do js=1,ns
          do jc=1,nc
             call GetPropColumn(chi,js,jc,psipsibar)
             call WriteFermionField(chi,200)
          end do
       end do
       if ( i_am_root ) close(200)
    end if

    deallocate(chi)

  end subroutine WritePropagator_csp

  subroutine ReadPropagator_csp(PropFile,psipsibar,parallel_io)
    ! begin args: propfile,psipsibar,parallel_io

    character(len=*) :: PropFile
    real(DP) :: kappa !quark mass
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar !solution vector
    logical :: parallel_io

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: MyPropFile
    character(len=6) :: ProcSuffix

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi !solution vector
    integer :: jc, js

    !Read the propagator out as a sequence of fermion field vectors.
    !The ordering for each propagator column is colour(c) varies first and then spin(s).

    allocate(chi(nx,ny,nz,nt,ns))

    ! begin execution
    if ( parallel_io ) then
       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       MyPropFile = trim(PropFile) // ProcSuffix
       open (200+mpi_rank, file=MyPropFile, status = "unknown", form = "unformatted", action = "read")
       do js=1,ns
          do jc=1,nc
             call ReadSplitFermionField(chi,200+mpi_rank)
             call PutPropColumn(chi,js,jc,psipsibar)
          end do
       end do
       close(200+mpi_rank)
    else
       if ( i_am_root ) then
          open (200, file=PropFile, status = "unknown", form = "unformatted", action = "read")
       end if
       do js=1,ns
          do jc=1,nc
             call ReadFermionField(chi,200)
             call PutPropColumn(chi,js,jc,psipsibar)
          end do
       end do
       if ( i_am_root ) close(200)
    end if

    deallocate(chi)

  end subroutine ReadPropagator_csp

  subroutine WritePropagator_soln(PropFile,psipsibar,parallel_io)
    ! begin args: propfile,psipsibar,parallel_io

    character(len=*) :: PropFile
    real(DP) :: kappa !quark mass
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar !solution vector
    logical :: parallel_io

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: MyPropFile
    character(len=6) :: ProcSuffix

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi !solution vector
    integer :: jc, js, i_v
    integer :: FileID

    !Read the propagator out as a sequence of fermion field vectors.
    !The ordering for each propagator column is colour(c) varies first and then spin(s).

    allocate(chi(nx,ny,nz,nt,ns))

    ! begin execution
    if ( parallel_io ) then
       call Abort("WritePropagator: parallel io not yet supported for soln format.")
    end if

#ifdef _chiral_basis_
    call SakuraiBasisToChiral(psipsibar)
#endif

    FileID = 200
    call OpenFermionFieldVectorFile(PropFile,FileID,action="write")
    do js=1,ns
       do jc=1,nc
          i_v = jc + (js-1)*nc
          call GetPropColumn(chi,js,jc,psipsibar)
          call WriteFermionField_bin(chi,FileID,i_v)
       end do
    end do
    call CloseFermionFieldVectorFile(FileID)
    deallocate(chi)

#ifdef _chiral_basis_
    call ChiralBasisToSakurai(psipsibar)
#endif

  end subroutine WritePropagator_soln

  subroutine ReadPropagator_soln(PropFile,psipsibar,parallel_io)
    ! begin args: propfile,psipsibar,parallel_io

    character(len=*) :: PropFile
    real(DP) :: kappa !quark mass
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar !solution vector
    logical :: parallel_io

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: MyPropFile
    character(len=6) :: ProcSuffix

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi !solution vector
    integer :: jc, js, i_v
    integer :: FileID

    !Read the propagator out as a sequence of fermion field vectors.
    !The ordering for each propagator column is colour(c) varies first and then spin(s).

    allocate(chi(nx,ny,nz,nt,ns))

    ! begin execution
    if ( parallel_io ) then
       call Abort("ReadPropagator: parallel io not yet supported for soln format.")
    end if

    FileID = 200
    call OpenFermionFieldVectorFile(PropFile,FileID,action="read")
    do js=1,ns
       do jc=1,nc
          i_v = jc + (js-1)*nc
          call ReadFermionField_bin(chi,FileID,i_v)
          call PutPropColumn(chi,js,jc,psipsibar)
       end do
    end do
    call CloseFermionFieldVectorFile(FileID)
    deallocate(chi)

#ifdef _chiral_basis_
    call ChiralBasisToSakurai(psipsibar)
#endif

  end subroutine ReadPropagator_soln

  subroutine WritePropagator_tsp(OutputFile,psipsibar,parallel_io)
    ! begin args: nkappa, kappa_i, TimeSlicePrefix, psipsibar_k, parallel_io
    character(len=*) :: OutputFile
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar ! the full propagator
    logical :: parallel_io

    ! begin local_vars
    character(len=1024) :: TimeSliceFile
    character(len=8) :: PropagatorSuffix
    character(len=6) :: TimeSliceSuffix

    integer :: ikappa,is,ic,js,jc

    real(dp),dimension(nlt) :: PionCorr, EffMass
    real(dp),dimension(nt)  :: PionCorr_zt, PionCorr_in
    integer :: ix, jx, iy, jy, iz, jz, it, jt, i_xyz, n_xyz
    integer :: irank
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: eta
    real(dp) :: starttime, endtime, totaltime

    ! begin execution

    allocate(eta(nx*ny*nz,nt,nc,ns,nc,ns))

    totaltime = 0.0d0

    do js=1,ns; do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_xyz =  ix + (iy-1)*nx + (iz-1)*nx*ny
          do jc=1,nc; do ic=1,nc
             eta(i_xyz,it,ic,is,jc,js) =  psipsibar(ix,iy,iz,it)%sc(is,js,ic,jc)
          end do; end do
       end do; end do; end do; end do
    end do; end do

    do jt=1,nt
       PionCorr_zt(jt) = 0.5d0*sum(abs(eta(:,jt,:,:,:,:))**2)
    end do

    if ( i_am_root ) then
       PionCorr = 0.0d0
       do irank=0,nproc-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          if ( irank /= mpi_root_rank ) then
             call RecvRealField(PionCorr_in,irank)
          else
             PionCorr_in = PionCorr_zt
          end if

          PionCorr(it:jt) = PionCorr(it:jt) + PionCorr_in(1:nt)
       end do
    else
       call SendRealField(PionCorr_zt,mpi_root_rank)
    end if

    if ( i_am_root ) then

       EffMass(1) = 0.0d0
       do jt = 2,nlt
          EffMass(jt) = log(abs( PionCorr(jt-1)/PionCorr(jt) ))
       end do

       mpiprint *, "TimeSlice  Pion Correlator  Effective Mass "
       do jt = 1,nlt
          mpiprint '(I9, E15.7, E15.7)', jt, PionCorr(jt), EffMass(jt)
       end do

    end if

    TimeSliceFile = trim(OutputFile)

    starttime = mpi_wtime()
    if ( parallel_io ) then
       write( TimeSliceFile, '(a,a,I5.5)' ) trim(OutputFile),".",mpi_rank
       open (200+mpi_rank, file=TimeSliceFile, status = "unknown", form = "unformatted", action = "write", position = "append" )

       call WriteSplitTimeSlicePropagator(eta,200+mpi_rank)
       close(200+mpi_rank)
    else
       if ( i_am_root ) then
          open (200, file=TimeSliceFile, status = "unknown", form = "unformatted", action = "write", position = "append" )
       end if

       call WriteTimeSlicePropagator(eta,200)

       if ( i_am_root ) close(200)
    end if
    endtime = mpi_wtime()

    totaltime = totaltime + ( endtime - starttime )


    mpiprint *, "Propagator IO took ", totaltime, " seconds in total."


  end subroutine WritePropagator_tsp

  subroutine ReadPropagator_tsp(InputFile,psipsibar,parallel_io)
    ! begin args: nkappa, kappa_i, TimeSlicePrefix, psipsibar_k, parallel_io
    character(len=*) :: InputFile
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar ! the full propagator
    logical :: parallel_io

    ! begin local_vars
    character(len=1024) :: TimeSliceFile
    character(len=8) :: PropagatorSuffix
    character(len=6) :: TimeSliceSuffix

    integer :: ikappa,is,ic,js,jc

    real(dp),dimension(nlt) :: PionCorr, EffMass
    real(dp),dimension(nt)  :: PionCorr_zt, PionCorr_in
    integer :: ix, jx, iy, jy, iz, jz, it, jt, i_xyz, n_xyz
    integer :: irank
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: eta
    real(dp) :: starttime, endtime, totaltime

    ! begin execution

    allocate(eta(nx*ny*nz,nt,nc,ns,nc,ns))

    totaltime = 0.0d0

    TimeSliceFile = trim(InputFile)

    starttime = mpi_wtime()
    if ( parallel_io ) then
       write( TimeSliceFile, '(a,a,I5.5)' ) trim(InputFile),".",mpi_rank
       open (200+mpi_rank, file=TimeSliceFile, status = "unknown", form = "unformatted", action = "read")

       call ReadSplitTimeSlicePropagator(eta,200+mpi_rank)
       close(200+mpi_rank)
    else
       if ( i_am_root ) then
          open (200, file=TimeSliceFile, status = "unknown", form = "unformatted", action = "read")
       end if

       call ReadTimeSlicePropagator(eta,200)

       if ( i_am_root ) close(200)
    end if
    endtime = mpi_wtime()

    totaltime = totaltime + ( endtime - starttime )

    mpiprint *, "Verifying read in propagator:"

    do jt=1,nt
       PionCorr_zt(jt) = 0.5d0*sum(abs(eta(:,jt,:,:,:,:))**2)
    end do

    if ( i_am_root ) then
       PionCorr = 0.0d0
       do irank=0,nproc-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          if ( irank /= mpi_root_rank ) then
             call RecvRealField(PionCorr_in,irank)
          else
             PionCorr_in = PionCorr_zt
          end if

          PionCorr(it:jt) = PionCorr(it:jt) + PionCorr_in(1:nt)
       end do
    else
       call SendRealField(PionCorr_zt,mpi_root_rank)
    end if

    if ( i_am_root ) then
       EffMass(1) = 0.0d0
       do jt = 2,nlt
          EffMass(jt) = log(abs( PionCorr(jt-1)/PionCorr(jt) ))
       end do

       mpiprint *, "TimeSlice  Pion Correlator  Effective Mass "
       do jt = 1,nlt
          mpiprint '(I9, E15.7, E15.7)', jt, PionCorr(jt), EffMass(jt)
       end do
    end if

    do js=1,ns; do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_xyz =  ix + (iy-1)*nx + (iz-1)*nx*ny
          do jc=1,nc; do ic=1,nc
             psipsibar(ix,iy,iz,it)%sc(is,js,ic,jc) = eta(i_xyz,it,ic,is,jc,js)
          end do; end do
       end do; end do; end do; end do
    end do; end do

    mpiprint *, "Propagator IO took ", totaltime, " seconds in total."

    deallocate(eta)

  end subroutine ReadPropagator_tsp

  subroutine WritePropagator_prop(OutputFile,psipsibar,parallel_io)

    character(len=*) :: OutputFile
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar
    logical :: parallel_io

    ! begin local_vars
    character(len=273) :: PropFile
    character(len=6) :: ProcSuffix

    integer :: irec  !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: OutFileID
    integer :: ix,iy,iz,it ! Loop variables
    integer(long) :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)
    logical :: avoid_2gb_limit

    !type(spin_colour_matrix), dimension(:,:,:,:), allocatable :: chi
    complex(sc), dimension(:,:,:,:,:), allocatable :: chi
    type(colour_spin_matrix) :: chi_x

    allocate(chi(nc*nc*ns*ns,nx,ny,nz,nt))

    PropFile = trim(OutputFile)

    matrix_len = 8*nc*nc*ns*ns !complex type=8 bytes
    irecl = matrix_len*nx*ny*nz*nt

    ! Try to avoid the 2GB record limit for 32-bit integer based systems.
    avoid_2gb_limit =  ( irecl > 2**31-1 )
    if ( avoid_2gb_limit ) irecl = matrix_len*nx*ny*nz

    OutfileId = 201

    if ( parallel_io ) then
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          chi_x%cs = reshape(psipsibar(ix,iy,iz,it)%sc,(/ nc,nc,ns,ns /), order = (/ 3,4,1,2 /))
          chi(:,ix,iy,iz,it) = reshape(chi_x%cs, (/ nc*nc*ns*ns /) ) 
       end do; end do; end do; end do
          
       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       PropFile = trim(PropFile) // ProcSuffix
       OutFileId = OutFileId + mpi_rank

       !Write out my portion of the propagator as a single record, for better IO performance.

       open(OutFileId,file=PropFile,form='unformatted',access='direct',status='replace',action='write',recl=irecl)
       irec = 1
       if ( avoid_2gb_limit ) then
          do it=1,nt
             write(OutFileID,rec=irec) chi(:,:,:,:,it)
             irec = irec + 1
          end do
       else
          write(OutFileID,rec=irec) chi(:,:,:,:,:)
       end if
       close(OutFileId)

    else
       call AssertSublatticeIsContiguous()

       if ( i_am_root ) then
          open(OutFileId,file=PropFile,form='unformatted',access='direct',status='replace',action='write',recl=irecl)
          irec = 1
          do iproct=1,nproct
             do iprocz=1,nprocz
                do iprocy=1,nprocy
                   coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                   irank = coords_to_rank(coords)

                   if ( irank /= mpi_root_rank ) then
                      call RecvComplexField(chi, irank)
                   else
                      do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                         chi_x%cs = reshape(psipsibar(ix,iy,iz,it)%sc,(/ nc,nc,ns,ns /), order = (/ 3,4,1,2 /))
                         chi(:,ix,iy,iz,it) = reshape(chi_x%cs, (/ nc*nc*ns*ns /) ) 
                      end do; end do; end do; end do
                   end if

                   ! Write the sub-lattice propagator.
                   if ( avoid_2gb_limit ) then
                      do it=1,nt
                         write(OutFileID,rec=irec) chi(:,:,:,:,it)
                         irec = irec + 1
                      end do
                   else
                      write(OutFileID,rec=irec) chi(:,:,:,:,:)
                      irec = irec + 1
                   end if

                end do
             end do
          end do
          close(OutFileId)

       else
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             chi_x%cs = reshape(psipsibar(ix,iy,iz,it)%sc,(/ nc,nc,ns,ns /), order = (/ 3,4,1,2 /))
             chi(:,ix,iy,iz,it) = reshape(chi_x%cs, (/ nc*nc*ns*ns /) ) 
          end do; end do; end do; end do
          call SendComplexField(chi,mpi_root_rank)
       end if
    end if

    deallocate(chi)

  end subroutine WritePropagator_prop

  subroutine ReadPropagator_prop(InputFile,psipsibar,parallel_io)

    character(len=*) :: InputFile
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar
    logical :: parallel_io

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: PropFile
    character(len=6) :: ProcSuffix

    integer :: irec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: InFileID
    integer :: ix,iy,iz,it ! Loop variables
    integer(long) :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)
    logical :: avoid_2gb_limit

    complex(sc), dimension(:,:,:,:,:), allocatable :: chi

    allocate(chi(nc*nc*ns*ns,nx,ny,nz,nt))

    PropFile = trim(InputFile)

    matrix_len = 8*nc*nc*ns*ns
    irecl = matrix_len*nx*ny*nz*nt

    ! Try to avoid the 2GB record limit for 32-bit integer based systems.
    avoid_2gb_limit =  ( irecl > 2**31-1 )
    if ( avoid_2gb_limit ) irecl = matrix_len*nx*ny*nz

    InFileId = 201

    if ( parallel_io ) then
       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       PropFile = trim(PropFile) // ProcSuffix
       InFileId = InFileId + mpi_rank

       !Read my portion of the propagator as a single record, for better IO performance.
       open(InFileId,file=PropFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)
       irec = 1
       if ( avoid_2gb_limit ) then
          do it=1,nt
             read(InFileID,rec=irec) chi(:,:,:,:,it)
             irec = irec + 1
          end do
       else
          read(InFileID,rec=irec) chi(:,:,:,:,:)
       end if
       close(InFileId)

       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          psipsibar(ix,iy,iz,it)%sc = reshape(chi(:,ix,iy,iz,it),(/ ns,ns,nc,nc /), order = (/ 3,4,1,2 /) )
       end do; end do; end do; end do

    else
       call AssertSublatticeIsContiguous()

       if ( i_am_root ) then

          open(InFileID,file=PropFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)

          irec = 1
          do iproct=1,nproct
             do iprocz=1,nprocz
                do iprocy=1,nprocy
                   coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                   irank = coords_to_rank(coords)

                   ! Read in the sub-lattice propagator.
                   if ( avoid_2gb_limit ) then
                      do it=1,nt
                         read(InFileID,rec=irec) chi(:,:,:,:,it)
                         irec = irec + 1
                      end do
                   else
                      read(InFileID,rec=irec) chi(:,:,:,:,:)
                      irec = irec + 1
                   end if

                   if ( irank /= mpi_root_rank ) then
                      call SendComplexField(chi, irank)
                   else
                      do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                         psipsibar(ix,iy,iz,it)%sc = reshape(chi(:,ix,iy,iz,it),(/ ns,ns,nc,nc /), order = (/3,4,1,2/))
                      end do; end do; end do; end do
                   end if

                end do
             end do
          end do

          close(InFileId)
       else
          call RecvComplexField(chi,mpi_root_rank)
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             psipsibar(ix,iy,iz,it)%sc = reshape(chi(:,ix,iy,iz,it),(/ ns,ns,nc,nc /), order = (/3,4,1,2/))
          end do; end do; end do; end do
       end if
    end if

    deallocate(chi)

  end subroutine ReadPropagator_prop

  subroutine WritePropagator_prop_dp(OutputFile,psipsibar,parallel_io)

    character(len=*) :: OutputFile
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar
    logical :: parallel_io

    ! begin local_vars
    character(len=273) :: PropFile
    character(len=6) :: ProcSuffix

    integer :: irec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: OutFileID
    integer :: ix,iy,iz,it ! Loop variables
    integer(long) :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)
    logical :: avoid_2gb_limit

    !type(spin_colour_matrix), dimension(:,:,:,:), allocatable :: chi
    complex(dc), dimension(:,:,:,:,:), allocatable :: chi
    type(colour_spin_matrix) :: chi_x

    allocate(chi(nc*nc*ns*ns,nx,ny,nz,nt))

    PropFile = trim(OutputFile)

    matrix_len = 16*nc*nc*ns*ns !complex type=8 bytes
    irecl = matrix_len*nx*ny*nz*nt

    ! Try to avoid the 2GB record limit for 32-bit integer based systems.
    avoid_2gb_limit =  ( irecl > 2**31-1 )
    if ( avoid_2gb_limit ) irecl = matrix_len*nx*ny*nz

    OutfileId = 201

    if ( parallel_io ) then
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          chi_x%cs = reshape(psipsibar(ix,iy,iz,it)%sc,(/ nc,nc,ns,ns /), order = (/ 3,4,1,2 /))
          chi(:,ix,iy,iz,it) = reshape(chi_x%cs, (/ nc*nc*ns*ns /) ) 
       end do; end do; end do; end do

       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       PropFile = trim(PropFile) // ProcSuffix
       OutFileId = OutFileId + mpi_rank

       !Write out my portion of the propagator as a single record, for better IO performance.

       open(OutFileId,file=PropFile,form='unformatted',access='direct',status='replace',action='write',recl=irecl)
       irec = 1
       if ( avoid_2gb_limit ) then
          do it=1,nt
             write(OutFileID,rec=irec) chi(:,:,:,:,it)
             irec = irec + 1
          end do
       else
          write(OutFileID,rec=irec) chi(:,:,:,:,:)
          irec = irec + 1
       end if
       close(OutFileId)
    else
       call AssertSublatticeIsContiguous()

       if ( i_am_root ) then
          open(OutFileId,file=PropFile,form='unformatted',access='direct',status='replace',action='write',recl=irecl)
          irec = 1
          do iproct=1,nproct
             do iprocz=1,nprocz
                do iprocy=1,nprocy
                   coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                   irank = coords_to_rank(coords)

                   if ( irank /= mpi_root_rank ) then
                      call RecvComplexField(chi, irank)
                   else
                      do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                         chi_x%cs = reshape(psipsibar(ix,iy,iz,it)%sc,(/ nc,nc,ns,ns /), order = (/ 3,4,1,2 /))
                         chi(:,ix,iy,iz,it) = reshape(chi_x%cs, (/ nc*nc*ns*ns /) ) 
                      end do; end do; end do; end do
                   end if

                   ! Write the sub-lattice propagator.
                   if ( avoid_2gb_limit ) then
                      do it=1,nt
                         write(OutFileID,rec=irec) chi(:,:,:,:,it)
                         irec = irec + 1
                      end do
                   else
                      write(OutFileID,rec=irec) chi(:,:,:,:,:)
                      irec = irec + 1
                   end if

                end do
             end do
          end do
          close(OutFileId)

       else
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             chi_x%cs = reshape(psipsibar(ix,iy,iz,it)%sc,(/ nc,nc,ns,ns /), order = (/ 3,4,1,2 /))
             chi(:,ix,iy,iz,it) = reshape(chi_x%cs, (/ nc*nc*ns*ns /) ) 
          end do; end do; end do; end do
          call SendComplexField(chi,mpi_root_rank)
       end if
    end if

    deallocate(chi)

  end subroutine WritePropagator_prop_dp

  subroutine ReadPropagator_prop_dp(InputFile,psipsibar,parallel_io)

    character(len=*) :: InputFile
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar
    logical :: parallel_io

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: PropFile
    character(len=6) :: ProcSuffix

    integer :: irec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: InFileID
    integer :: ix,iy,iz,it ! Loop variables
    integer(long) :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)
    logical :: avoid_2gb_limit

    complex(dc), dimension(:,:,:,:,:), allocatable :: chi

    allocate(chi(nc*nc*ns*ns,nx,ny,nz,nt))

    PropFile = trim(InputFile)

    matrix_len = 16*nc*nc*ns*ns
    irecl = matrix_len*nx*ny*nz*nt

    ! Try to avoid the 2GB record limit for 32-bit integer based systems.
    avoid_2gb_limit =  ( irecl > 2**31-1 )
    if ( avoid_2gb_limit ) irecl = matrix_len*nx*ny*nz

    InFileId = 201

    if ( parallel_io ) then
       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       PropFile = trim(PropFile) // ProcSuffix
       InFileId = InFileId + mpi_rank

       !Read my portion of the propagator as a single record, for better IO performance.
       open(InFileId,file=PropFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)
       irec = 1
       if ( avoid_2gb_limit ) then
          do it=1,nt
             read(InFileID,rec=irec) chi(:,:,:,:,it)
             irec = irec + 1
          end do
       else
          read(InFileID,rec=irec) chi(:,:,:,:,:)
          irec = irec + 1
       end if
       close(InFileId)

       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          psipsibar(ix,iy,iz,it)%sc = reshape(chi(:,ix,iy,iz,it),(/ ns,ns,nc,nc /), order = (/3,4,1,2/) )
       end do; end do; end do; end do

    else
       call AssertSublatticeIsContiguous()

       if ( i_am_root ) then
          open(InFileID,file=PropFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)

          irec = 1
          do iproct=1,nproct
             do iprocz=1,nprocz
                do iprocy=1,nprocy
                   coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                   irank = coords_to_rank(coords)

                   ! Read in the sub-lattice propagator.
                   if ( avoid_2gb_limit ) then
                      do it=1,nt
                         read(InFileID,rec=irec) chi(:,:,:,:,it)
                         irec = irec + 1
                      end do
                   else
                      read(InFileID,rec=irec) chi(:,:,:,:,:)
                      irec = irec + 1
                   end if

                   if ( irank /= mpi_root_rank ) then
                      call SendComplexField(chi, irank)
                   else
                      do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                         psipsibar(ix,iy,iz,it)%sc = reshape(chi(:,ix,iy,iz,it),(/ ns,ns,nc,nc /), order = (/3,4,1,2/))
                      end do; end do; end do; end do
                   end if

                end do
             end do
          end do

          close(InFileId)
       else
          call RecvComplexField(chi,mpi_root_rank)
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             psipsibar(ix,iy,iz,it)%sc = reshape(chi(:,ix,iy,iz,it),(/ ns,ns,nc,nc /), order = (/3,4,1,2/))
          end do; end do; end do; end do
       end if
    end if

    deallocate(chi)

  end subroutine ReadPropagator_prop_dp

  subroutine WriteInverseFMSoln_EO(OutputFile,InvFMS_eo,parallel_io)
    
    character(len=*) :: OutputFile
    type(dirac_fermion), dimension(:,:) :: InvFMS_eo
    logical :: parallel_io

    ! begin local_vars
    character(len=273) :: PropFile
    character(len=6) :: ProcSuffix

    integer :: irec, jrec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: OutFileID
    integer :: i_xeo,ic,is ! Loop variables
    integer(long) :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)
    integer :: i_v, n_v

    complex(sc), dimension(:,:,:,:), allocatable :: chi

    n_v = size(InvFMS_eo,dim=2)
    allocate(chi(nc,ns,n_xeo,n_v))

    PropFile = trim(OutputFile)

    matrix_len = 8*nc*ns !complex type=8 bytes

    OutfileId = 201

    if ( parallel_io ) then
       irecl = matrix_len*n_xeo*n_v
       do i_v=1,n_v; do i_xeo=1,n_xeo
          chi(:,:,i_xeo,i_v) = InvFMS_eo(i_xeo,i_v)%cs(:,:)
       end do; end do

       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       PropFile = trim(PropFile) // ProcSuffix
       OutFileId = OutFileId + mpi_rank

       !Write out my portion of the propagator as a single record, for better IO performance.

       open(OutFileId,file=PropFile,form='unformatted',access='direct',status='replace',action='write',recl=irecl)
       irec = 1
       write(OutFileId,rec=irec) chi
       close(OutFileId)
    else
       irecl = matrix_len*n_xeo
       call AssertSublatticeIsContiguous()

       if ( i_am_root ) then
          open(OutFileId,file=PropFile,form='unformatted',access='direct',status='replace',action='write',recl=irecl)
          jrec = 1
          do iproct=1,nproct
             do iprocz=1,nprocz
                do iprocy=1,nprocy
                   coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                   irank = coords_to_rank(coords)

                   if ( irank /= mpi_root_rank ) then
                      call RecvComplexField(chi, irank)
                   else
                      do i_v=1,n_v; do i_xeo=1,n_xeo
                         chi(:,:,i_xeo,i_v) = InvFMS_eo(i_xeo,i_v)%cs(:,:)
                      end do; end do
                   end if

                   ! Write the sub-lattice propagator.
                   do i_v=1,n_v
                      irec = jrec + (i_v-1)*nproc
                      write(OutFileID,rec=irec) chi(:,:,:,i_v)
                   end do

                   jrec = jrec + 1
                end do
             end do
          end do
          close(OutFileId)

       else
          do i_v=1,n_v; do i_xeo=1,n_xeo
             chi(:,:,i_xeo,i_v) = InvFMS_eo(i_xeo,i_v)%cs(:,:)
          end do; end do
          call SendComplexField(chi,mpi_root_rank)
       end if
    end if

    deallocate(chi)

  end subroutine WriteInverseFMSoln_EO

  subroutine ReadInverseFMSoln_EO(InputFile,InvFMS_eo,parallel_io)

    character(len=*) :: InputFile
    type(dirac_fermion), dimension(:,:) :: InvFMS_eo
    logical :: parallel_io

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: PropFile
    character(len=6) :: ProcSuffix

    integer :: irec, jrec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: InFileID
    integer :: i_xeo,i_v,n_v
    integer(long) :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)

    complex(sc), dimension(:,:,:,:), allocatable :: chi

    n_v = size(InvFMS_eo,dim=2)
    allocate(chi(nc,ns,n_xeo,n_v))

    PropFile = trim(InputFile)

    matrix_len = 8*nc*ns !complex type=8 bytes

    InFileId = 201

    if ( parallel_io ) then
       irecl = matrix_len*n_xeo*n_v
       write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
       PropFile = trim(PropFile) // ProcSuffix
       InFileId = InFileId + mpi_rank

       !Read my portion of the propagator as a single record, for better IO performance.
       open(InFileId,file=PropFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)
       irec = 1
       read(InFileId,rec=irec) chi
       close(InFileId)

       do i_v=1,n_v; do i_xeo=1,n_xeo
          InvFMS_eo(i_xeo,i_v)%cs(:,:) = chi(:,:,i_xeo,i_v)
       end do; end do

    else
       irecl = matrix_len*n_xeo
       call AssertSublatticeIsContiguous()

       if ( i_am_root ) then

          open(InFileID,file=PropFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)

          jrec = 1
          do iproct=1,nproct
             do iprocz=1,nprocz
                do iprocy=1,nprocy
                   coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                   irank = coords_to_rank(coords)
                   ! Read in the sub-lattice propagator.
                   do i_v=1,n_v
                      irec = jrec + (i_v-1)*nproc
                      read(InFileID,rec=irec) chi(:,:,:,i_v)
                   end do

                   if ( irank /= mpi_root_rank ) then
                      call SendComplexField(chi, irank)
                   else
                      do i_v=1,n_v; do i_xeo=1,n_xeo
                         InvFMS_eo(i_xeo,i_v)%cs(:,:) = chi(:,:,i_xeo,i_v)
                      end do; end do
                   end if

                   jrec = jrec + 1
                end do
             end do
          end do

          close(InFileId)
       else
          call RecvComplexField(chi,mpi_root_rank)
          do i_v=1,n_v; do i_xeo=1,n_xeo
             InvFMS_eo(i_xeo,i_v)%cs(:,:) = chi(:,:,i_xeo,i_v)
          end do; end do
       end if
    end if

    deallocate(chi)

  end subroutine ReadInverseFMSoln_EO

  subroutine ReadPropagator_flic6(InputFile,psipsibar,parallel_io)
    ! begin args: nkappa, kappa_i, PropPrefix, psipsibar_k, parallel_io
    character(len=*) :: InputFile
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar ! the full propagator
    logical :: parallel_io

    ! begin local_vars


    integer :: OutFileId
    ! begin local_vars
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: chi
    real(dp), dimension(:,:,:,:,:,:), allocatable :: phir, phii
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    integer :: ic, jc, is, js
    integer :: kx,ky,kz,kt, i_xyz, k_xyz
    integer :: irank

    ! begin execution

    if ( parallel_io ) then
       call Abort("ReadPropagator: parallel IO not supported for FLIC6 format.")
    end if

    allocate(chi(nx*ny*nz,nt,nc,ns,nc,ns))

    call MPIBarrier

    if ( i_am_root ) then

       allocate(phir(nlx*nly*nlz,nlt,nc,ns,nc,ns))
       allocate(phii(nlx*nly*nlz,nlt,nc,ns,nc,ns))

       open (200, file=InputFile, status = "unknown", form = "unformatted", action = "read")
       do is=1,ns
          do ic=1,nc
             read(200) phir(:,:,:,:,is,ic), phii(:,:,:,:,is,ic)
          end do
       end do
       close(200)

       do irank=0,mpi_size-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do kt=1,nt; do kz=1,nz; do ky=1,ny; do kx=1,nx
             k_xyz =  kx + (ky-1)*nx + (kz-1)*nx*ny
             i_xyz = (ix+(kx-1)) + (iy-1+(ky-1))*nlx + (iz-1+(kz-1))*nlx*nly
             chi(k_xyz,kt,:,:,:,:) = cmplx(phir(i_xyz,it+kt-1,:,:,:,:),phii(i_xyz,it+kt-1,:,:,:,:),dc)
          end do; end do; end do; end do

          if ( irank == mpi_root_rank ) then
             do js=1,ns; do is=1,ns
                do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                   i_xyz =  ix + (iy-1)*nx + (iz-1)*nx*ny
                   do jc=1,nc; do ic=1,nc
                      psipsibar(ix,iy,iz,it)%sc(is,js,ic,jc) = chi(i_xyz,it,ic,is,jc,js)
                   end do; end do
                end do; end do; end do; end do
             end do; end do
          else
             call SendComplexField(chi,irank)
          end if
       end do

       deallocate(phir)
       deallocate(phii)
    else
       call RecvComplexField(chi,mpi_root_rank)
       do js=1,ns; do is=1,ns
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             i_xyz =  ix + (iy-1)*nx + (iz-1)*nx*ny
             do jc=1,nc; do ic=1,nc
                psipsibar(ix,iy,iz,it)%sc(is,js,ic,jc) = chi(i_xyz,it,ic,is,jc,js)
             end do; end do
          end do; end do; end do; end do
       end do; end do
    end if

    deallocate(chi)

  end subroutine ReadPropagator_flic6


end module PropagatorIO
