#include "defines.h"

module StoutLinks

  use MPIInterface
  use FatLinks
  use MatrixAlgebra
  use GaugeFieldMPIComms
  use LatticeSize
  use Kinds
  use ColourTypes
  implicit none
  private

  integer :: stout_sweeps = 2 !smearing sweeps

  public :: StoutSmearLinks
  public :: StoutSmear
  public :: GetExpIQ
  public :: dU_prbydU_stout
  public :: dQbydt

contains

  subroutine StoutSmearLinks(U_xd,Usm_xd,alpha,sweeps,smear_time)
    ! begin args: U_xd, Usm_xd, alpha, sweeps, smear_time

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd, Usm_xd !Thin links
    real(dp) :: alpha
    integer :: sweeps
    logical, optional :: smear_time ! smear in the time direction

    ! begin local_vars
    integer :: isweeps

    ! begin execution

    Usm_xd(1:nxs,1:nys,1:nzs,1:nts,:) = U_xd(1:nxs,1:nys,1:nzs,1:nts,:)

    do isweeps =1,sweeps
       call StoutSmear(Usm_xd,alpha,smear_time)
       call ShadowGaugeField(Usm_xd,1)
    end do


  end subroutine StoutSmearLinks

  subroutine StoutSmear(U_xd,alpha,smear_time)
    ! begin args: U_xd, alpha, smear_time

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: alpha
    logical, optional :: smear_time

    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: V_xd !Smeared links
    real(dp) :: alpha_sm
    integer :: mu,nu,id, nmu
    integer :: ix,iy,iz,it
    type(colour_matrix) :: A_x, Adag_x, Q_x, V_x
    complex(dc) :: TrQ
    logical :: smear_t
    logical :: NaN

    NaN=.false.
    ! begin execution
    allocate(V_xd(nxp,nyp,nzp,ntp,nd))

    smear_t = .true.
    if ( present(smear_time) ) smear_t = smear_time

    if ( smear_t ) then
       nmu = nd
    else
       nmu = nd-1
    end if

    alpha_sm = alpha
    V_xd = zero_matrix
    write(*,*) "Begin Smearing"
    call eNaN(U_xd)

    write(*,*) "APE Blocking Links"
    do mu=1,nmu
       do id=1,nmu-1
          nu = modc(mu+id,nmu)
          call GetAPEBlockedLinks(V_xd,U_xd,mu,nu)
          write(*,*) mu,id
          call eNaN(V_xd)
       end do
    end do
    write(*,*) "Naughty Corner"
    write(*,*) V_xd(10,1,1,1,1)%cl
    write(*,*) U_xd(10,1,1,1,1)%cl

    do mu=1,nmu
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
         
          V_x%Cl = alpha_sm*V_xd(ix,iy,iz,it,mu)%Cl
          write(*,*) "Constant Multiplication"
          call eNaNl(V_x,NaN)
!          if(NaN) then
!             write(*,*) ix,iy,iz,it,mu
!          endif

          call MultiplyMatMatDag(A_x,V_x,U_xd(ix,iy,iz,it,mu))
          write(*,*) "Define Omega"
          call eNaNl(A_x,NaN)
!          if(NaN) then
!             write(*,*) ix,iy,iz,it,mu
!          endif
          call MatDag(Adag_x,A_x)
          write(*,*) "Omega Dag"
          call eNaNl(Adag_x,NaN)
          Q_x%Cl = Adag_x%Cl - A_x%Cl
          write(*,*) "Omega dif"
          call eNaNl(Q_x,NaN)
          TrQ = trace(Q_x)
          write(*,*) "Trace dif"
          if(isnan(real(TrQ))) then
             write(*,*) "NaN detected"
             NaN=.true.
          endif

          Q_x%Cl(1,1) = Q_x%Cl(1,1) - (1.0d0/3.0d0)*TrQ
          Q_x%Cl(2,2) = Q_x%Cl(2,2) - (1.0d0/3.0d0)*TrQ
          Q_x%Cl(3,3) = Q_x%Cl(3,3) - (1.0d0/3.0d0)*TrQ

          Q_x%Cl = 0.5d0*i*Q_x%Cl
          write(*,*) "Define Q"
!          write(*,*) Q_x
          call eNaNl(Q_x,NaN)
!          if(ix.eq.10 .and. iy.eq.1 .and. iz.eq.1 .and. it.eq.1 .and. mu.eq.1) then
!             write(*,*) "Problem Entry"
!             write(*,*) "Omega Dagger"
!             write(*,*) Adag_x
!             write(*,*) "Omega"
!             write(*,*) A_x
!             write(*,*) "Trace Dif"
!             write(*,*) TrQ
!             write(*,*) "Q"
!             write(*,*) Q_x
!          endif

          call GetExpIQ(V_x,Q_x)
          write(*,*) "Exp Q"
          call eNaNl(V_x,NaN)
          if(NaN) then
             write(*,*) V_x
             write(*,*) Q_x
          endif

          A_x = U_xd(ix,iy,iz,it,mu)
          call MultiplyMatMat(U_xd(ix,iy,iz,it,mu),V_x,A_x)
          write(*,*) "Get Smeared Link"
          call eNaNl(U_xd(ix,iy,iz,it,mu),NaN)

          if(NaN) then
             write(*,*) "NaN at"
             write(*,*) ix,iy,iz,it,mu
             write(*,*) "Problem Entry"
             write(*,*) "Omega Dagger"
             write(*,*) Adag_x
             write(*,*) "Omega"
             write(*,*) A_x
             write(*,*) "Trace Dif"
             write(*,*) TrQ
             write(*,*) "Q"
             write(*,*) Q_x
             write(*,*) "ExpQ"
             write(*,*) V_x
          endif

       end do; end do; end do; end do
    end do

    deallocate(V_xd)

  end subroutine StoutSmear

  subroutine GetExpIQ(V_x,Q_x)
    ! begin args: V_x, Q_x

    ! Exponentiates the hermitian traceless matrix Q, V = exp(iQ)

    type(colour_matrix), intent(out) :: V_x
    type(colour_matrix), intent(in) :: Q_x
    ! begin local_vars
    type(colour_matrix) :: Qsq_x, Qcu_x, I_x

    real(dp) :: c0, c1, c0max, theta, u, w, u_sq, w_sq, xi0, pm
    complex(dc) :: h(0:2), f(0:2)
    integer :: j

    logical :: NaN

    ! begin execution

    I_x = unit_matrix

    call MultiplyMatMat(Qsq_x,Q_x,Q_x)
    write(*,*) "Qsq_x"
    call eNaNl(Qsq_x,NaN)
    call MultiplyMatMat(Qcu_x,Qsq_x,Q_x)
    write(*,*) "Qcu_x"
    call eNaNl(Qcu_x,NaN)
    c0 = trace(Qcu_x)/3.0d0
    c1 = 0.5d0*trace(Qsq_x)

    pm = sign(1.0d0,c0)
    c0 = abs(c0)

    c0max = 2*(c1/3)**(1.5d0)
    if(isnan(c0max) .or. isnan(c0)) then
       write(*,*) "c0"
       write(*,*) c0max,c0
    endif
    !c1max = (1.0d0/32.0d0)*(69.0d0 + 11*sqrt(33.0d0))*(12*alpha)**2

    if ( c0max .le. 1e-40) then
       write(*,*) "Tiny c0Max"
       write(*,*) c0max,c0
       write(*,*) Qcu_x
       write(*,*) Qsq_x
!       c0max = 0
    endif
    if ( c0max == 0 ) then
       theta = 0.0d0
    else
       theta = acos(c0/c0max)
    end if
    
    if(isnan(theta)) then
       write(*,*) "Theta NaN"
       write(*,*) theta,c0,c0max,c0/c0max
    endif
    u = sqrt(c1/3.0d0)*cos(theta/3.0d0)
    w = sqrt(c1)*sin(theta/3.0d0)
    if(isnan(u) .or. isnan(w)) then
       write(*,*) "uw NaN"
       write(*,*) u,w
    endif

    u_sq = u**2
    w_sq = w**2

    if ( abs(w) < 0.05 ) then
       xi0 = 1 - (1.0d0/6.0d0)*w_sq*(1 - (1.0d0/20.0d0)*w_sq*(1 - (1.0d0/42.0d0)*w_sq))
    else
       xi0 = sin(w)/w
    end if

    h(0) = (u_sq - w_sq)*exp(2*i*u) + (8*u_sq*cos(w) + 2*i*u*(3*u_sq + w_sq)*xi0)*exp(-i*u)
    h(1) = 2*u*exp(2*i*u) - (2*u*cos(w) - i*(3*u_sq-w_sq)*xi0)*exp(-i*u)
    h(2) = exp(2*i*u) - (cos(w)+3*i*u*xi0)*exp(-i*u)

    if ( c0max == 0 ) then
       f(0) = 1.0d0
       f(1:2) = 0.0d0
    else
       f(:) = h(:)/(9*u_sq-w_sq)
    end if

    if ( pm < 0 ) then
       do j=0,2
          f(j) = (-1)**j*conjg(f(j))
       end do
    end if

    V_x%Cl = f(0)*I_x%Cl + f(1)*Q_x%Cl + f(2)*Qsq_x%Cl


  end subroutine GetExpIQ

  subroutine dU_prbydU_stout(dSbydU,U_xd,alpha)
    ! begin args: dSbydU, U_xd, alpha

    !Accepts dSbydU' and returns dSbydU in the same array.

    type(colour_matrix), dimension(:,:,:,:,:) :: dSbydU, U_xd
    real(dp) :: alpha
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: V_xd, dSbydV
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: Lambda_xd

    type(colour_matrix) :: A_x, Adag_x, B1_x, B2_x, C_x, I_x, Q_x, Qsq_x, T_x, USigma_x
    integer :: mu,nu, inu

    integer, dimension(nd) :: dmu, dnu
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt
    integer :: lx,ly,lz,lt
    integer :: mx,my,mz,mt

    integer :: j, id
    complex(dc) :: TrA, TrQ
    real(dp) :: c0, c1
    complex(dc) :: f(0:2), b(0:2,1:2)

    logical :: verbose

#define U_mux        U_xd(ix,iy,iz,it,mu)
#define U_nux        U_xd(ix,iy,iz,it,nu)
#define U_nuxpmu     U_xd(jx,jy,jz,jt,nu)
#define U_muxpnu     U_xd(kx,ky,kz,kt,mu)
#define U_nuxmnu     U_xd(lx,ly,lz,lt,nu)
#define U_muxmnu     U_xd(lx,ly,lz,lt,mu)
#define U_nuxpmumnu  U_xd(mx,my,mz,mt,nu)

#define Lambda_mux        Lambda_xd(ix,iy,iz,it,mu)
#define Lambda_nux        Lambda_xd(ix,iy,iz,it,nu)
#define Lambda_nuxpmu     Lambda_xd(jx,jy,jz,jt,nu)
#define Lambda_muxpnu     Lambda_xd(kx,ky,kz,kt,mu)
#define Lambda_nuxmnu     Lambda_xd(lx,ly,lz,lt,nu)
#define Lambda_muxmnu     Lambda_xd(lx,ly,lz,lt,mu)
#define Lambda_nuxpmumnu  Lambda_xd(mx,my,mz,mt,nu)

#define dSbydU_mux        dSbydU(ix,iy,iz,it,mu)
#define dSbydV_mux        dSbydV(ix,iy,iz,it,mu)

    ! begin execution
    allocate(V_xd(nxp,nyp,nzp,ntp,nd))
    allocate( dSbydV(nxp,nyp,nzp,ntp,nd))
    allocate(Lambda_xd(nxs,nys,nzs,nts,nd))

    dSbydV(1:nx,1:ny,1:nz,1:nt,:) = dSbydU(1:nx,1:ny,1:nz,1:nt,:) ! dSbydV now stores dSbydU_pr = dSbydU(i)
    dSbydU = zero_matrix ! dSbydU will contain dSbydU(i-1)
    I_x = unit_matrix

    !Create the staple field
    V_xd = zero_matrix
    do mu=1,nd
       do id=1,nd-1
          nu = modc(mu+id,nd)
          call GetAPEBlockedLinks(V_xd,U_xd,mu,nu)
       end do
    end do

    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          ! Construct C
          C_x%Cl = alpha*V_xd(ix,iy,iz,it,mu)%Cl

          ! Construct Q
          call MultiplyMatMatDag(A_x,C_x,U_xd(ix,iy,iz,it,mu))
          call MatDag(Adag_x,A_x)
          Q_x%Cl = Adag_x%Cl - A_x%Cl
          TrQ = trace(Q_x)

          Q_x%Cl(1,1) = Q_x%Cl(1,1) - (1.0d0/3.0d0)*TrQ
          Q_x%Cl(2,2) = Q_x%Cl(2,2) - (1.0d0/3.0d0)*TrQ
          Q_x%Cl(3,3) = Q_x%Cl(3,3) - (1.0d0/3.0d0)*TrQ

          Q_x%Cl = 0.5d0*i*Q_x%Cl

          ! Construct B_i

          call MultiplyMatMat(Qsq_x,Q_x,Q_x)
          call MultiplyMatMat(T_x,Qsq_x,Q_x)

          c0 = trace(T_x)/3.0d0
          c1 = 0.5d0*trace(Qsq_x)

          call dQbydt(c0,c1,f,b)

          B1_x%Cl = b(0,1)*I_x%Cl + b(1,1)*Q_x%Cl + b(2,1)*Qsq_x%Cl
          B2_x%Cl = b(0,2)*I_x%Cl + b(1,2)*Q_x%Cl + b(2,2)*Qsq_x%Cl


          ! Construct the Lambda field
          call MultiplyMatMat(USigma_x,U_mux,dSbydV_mux)
          A_x%Cl = f(1)*USigma_x%Cl

          call MultiplyMatMat(T_x,USigma_x,Q_x)
          A_x%Cl = A_x%Cl + f(2)*T_x%Cl

          call MultiplyMatMat(T_x,Q_x,USigma_x)
          A_x%Cl = A_x%Cl + f(2)*T_x%Cl

          call MultiplyMatMat(T_x,B1_x,USigma_x)
          A_x%Cl = A_x%Cl + trace(T_x)*Q_x%Cl

          call MultiplyMatMat(T_x,B2_x,USigma_x)
          A_x%Cl = A_x%Cl + trace(T_x)*Qsq_x%Cl

          call MatDag(Adag_x,A_x)
          A_x%Cl = A_x%Cl + Adag_x%Cl
          TrA = trace(A_x)

          A_x%Cl(1,1) = A_x%Cl(1,1) - (1.0d0/3.0d0)*TrA
          A_x%Cl(2,2) = A_x%Cl(2,2) - (1.0d0/3.0d0)*TrA
          A_x%Cl(3,3) = A_x%Cl(3,3) - (1.0d0/3.0d0)*TrA

          Lambda_mux%Cl = 0.5d0*A_x%Cl

          ! Begin to construct dSbydU

          T_x%Cl = f(0)*I_x%Cl + f(1)*Q_x%Cl + f(2)*Qsq_x%Cl
          call MultiplyMatMat(dSbydU_mux,dSbydV_mux,T_x)

          call MultiplyMatDagMat(T_x,C_x,Lambda_mux)

          dSbydU_mux%Cl = dSbydU_mux%Cl + i*T_x%Cl

       end do; end do; end do; end do
    end do

    call ShadowGaugeField(Lambda_xd,1)

    dSbydV = zero_matrix

    ! Contruct the staple derivative terms (dOmegabydt)
    do mu=1,nd
       nu = mu
       do inu=1,nd-1
          nu = modulo(nu,nd)+1
          dmu = 0
          dnu = 0

          dmu(mu) = 1
          dnu(nu) = 1

          do it=1,nt
             jt = mapt(it + dmu(4))
             kt = mapt(it + dnu(4))
             lt = mapt(it - dnu(4))
             mt = mapt(it - dnu(4) + dmu(4))
             do iz=1,nz
                jz = mapz(iz + dmu(3))
                kz = mapz(iz + dnu(3))
                lz = mapz(iz - dnu(3))
                mz = mapz(iz - dnu(3) + dmu(3))
                do iy=1,ny
                   jy = mapy(iy + dmu(2))
                   ky = mapy(iy + dnu(2))
                   ly = mapy(iy - dnu(2))
                   my = mapy(iy - dnu(2) + dmu(2))
                   do ix=1,nx
                      jx = mapx(ix + dmu(1))
                      kx = mapx(ix + dnu(1))
                      lx = mapx(ix - dnu(1))
                      mx = mapx(ix - dnu(1) + dmu(1))

                      call MultiplyMatMatDag(T_x,U_nuxpmu,U_muxpnu)
                      call MultiplyMatDagMat(A_x,U_nux,Lambda_nux)
                      call MatPlusMatTimesMat(dSbydV_mux,T_x,A_x)

                      call MultiplyMatMatDag(A_x,Lambda_muxpnu,U_nux)
                      call MatPlusMatTimesMat(dSbydV_mux,T_x,A_x)

                      call MultiplyMatDagMatDag(T_x,U_nuxpmumnu,U_muxmnu)
                      call MultiplyMatMat(A_x,Lambda_muxmnu,U_nuxmnu)
                      call MatPlusMatTimesMat(dSbydV_mux,T_x,A_x)

                      call MultiplyMatMat(A_x,Lambda_nuxmnu,U_nuxmnu)
                      call MatMinusMatTimesMat(dSbydV_mux,T_x,A_x)

                      call MultiplyMatDagMat(T_x,U_nuxpmumnu,Lambda_nuxpmumnu)
                      call MultiplyMatDagMat(A_x,U_muxmnu,U_nuxmnu)
                      call MatPlusMatTimesMat(dSbydV_mux,T_x,A_x)

                      call MultiplyMatMat(T_x,Lambda_nuxpmu,U_nuxpmu)
                      call MultiplyMatDagMatDag(A_x,U_muxpnu,U_nux)
                      call MatMinusMatTimesMat(dSbydV_mux,T_x,A_x)

                   end do
                end do
             enddo
          end do

       end do
    end do

    ! Finish by adding the staple derivative term.
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dSbydU_mux%Cl = dSbydU_mux%Cl - i*alpha*dSbydV_mux%Cl
       end do; end do; end do; end do
    end do

#undef dSbydU_mux
#undef dSbydV_mux

#undef U_mux
#undef U_nuxpmu
#undef U_muxpnu
#undef U_nuxpmumnu
#undef U_muxmnu
#undef U_nuxmnu

#undef Lambda_mux
#undef Lambda_nux
#undef Lambda_nuxpmu
#undef Lambda_muxpnu
#undef Lambda_nuxpmumnu
#undef Lambda_muxmnu
#undef Lambda_nuxmnu

    deallocate(V_xd)
    deallocate( dSbydV)
    deallocate(Lambda_xd)

  end subroutine dU_prbydU_stout

  subroutine dQbydt(c0,c1,f,b)
    ! begin args: c0, c1, f, b

    real(dp) :: c0, c1
    complex(dc) :: f(0:2), b(0:2,1:2)

    ! begin local_vars
    real(dp) :: c0max, theta, u, w, u_sq, w_sq, xi0, xi1, pm
    complex(dc) :: h(0:2), r(0:2,1:2)
    integer :: j, k

    ! begin execution

    pm = sign(1.0d0,c0)
    c0 = abs(c0)

    c0max = 2.0d0*(c1/3.0d0)**(1.5d0)

    if ( c0max == 0 ) then
       theta = 0.0d0
    else
       theta = acos(c0/c0max)
    end if

    u = sqrt(c1/3.0d0)*cos(theta/3.0d0)
    w = sqrt(c1)*sin(theta/3.0d0)

    u_sq = u**2
    w_sq = w**2

    if ( abs(w) < 0.05 ) then
       xi0 = 1 - (1.0d0/6.0d0)*w_sq*(1 - (1.0d0/20.0d0)*w_sq*(1 - (1.0d0/42.0d0)*w_sq))
       xi1 = (-1.0_dp)*(1.0_dp/3.0_dp - (1.0_dp/30.0_dp)*w_sq*(1 - (1.0_dp/28.0_dp)*w_sq*(1 - (1.0_dp/54.0_dp)*w_sq) ))
    else
       xi0 = sin(w)/w
       xi1 = cos(w)/w_sq - sin(w)/(w_sq*w)
    end if

    h(0) = (u_sq - w_sq)*exp(2*i*u) + (8*u_sq*cos(w) + 2*i*u*(3*u_sq + w_sq)*xi0)*exp(-i*u)
    h(1) = 2*u*exp(2*i*u) - (2*u*cos(w) - i*(3*u_sq-w_sq)*xi0)*exp(-i*u)
    h(2) = exp(2*i*u) - (cos(w)+3*i*u*xi0)*exp(-i*u)

    if ( c0max == 0 ) then
       f(0) = 1.0d0
       f(1:2) = 0.0d0
    else
       f(:) = h(:)/(9*u_sq-w_sq)
    end if

    r(0,1) = 2*(u+i*(u_sq - w_sq))*exp(2*i*u) + 2*(4*u*(2-i*u)*cos(w) + i*(9*u_sq + w_sq - i*u*(3*u_sq+w_sq))*xi0)*exp(-i*u)
    r(1,1) = 2*(1+2*i*u)*exp(2*i*u) + (-2*(1-i*u)*cos(w) + i*(6*u + i*(w_sq - 3*u_sq))*xi0)*exp(-i*u)
    r(2,1) = 2*i*exp(2*i*u) + i*(cos(w) - 3*(1-i*u)*xi0)*exp(-i*u)

    r(0,2) = -2*exp(2*i*u) + 2*i*u*(cos(w)+(1+4*i*u)*xi0+3*u_sq*xi1)*exp(-i*u)
    r(1,2) = -i*(cos(w) + (1+2*i*u)*xi0 - 3*u_sq*xi1)*exp(-i*u)
    r(2,2) = (xi0 - 3*i*u*xi1)*exp(-i*u)

    if ( c0max == 0 ) then
       b(:,:) = 0.0d0
    else
       b(:,1) = (2*u*r(:,1) + (3*u_sq - w_sq)*r(:,2) - 2*(15*u_sq+w_sq)*f(:))/(2*(9*u_sq - w_sq)**2)
       b(:,2) = (r(:,1) - 3*u*r(:,2) - 24*u*f(:))/(2*(9*u_sq - w_sq)**2)
    end if

    if ( pm < 0 ) then
       do j=0,2
          f(j) = (-1)**j*conjg(f(j))
       end do

       do k=1,2
          do j=0,2
             b(j,k) = (-1)**(k+j+1)*conjg(b(j,k))
          end do
       end do
    end if


  end subroutine dQbydt

  subroutine eNaN(U_xd)
    type(colour_matrix), dimension(:,:,:,:,:),intent(in) :: U_xd

    integer :: ix,iy,iz,it,imu,ic,jc

    do ix=1,nlx;do iy=1,nly; do iz=1,nlz;do it=1,nlt;do imu=1,4;do ic=1,3;do jc=1,3
       if (isnan(real(U_xd(ix,iy,iz,it,imu)%cl(ic,jc)))) then
          write(*,*) "NaN Detected"
          write(*,*) ix,iy,iz,it,imu,ic,jc
       endif
       
    enddo;enddo;enddo;enddo;enddo;enddo;enddo


  end subroutine eNaN

  subroutine eNaNl(U_x,NaN)
    type(colour_matrix),intent(in) :: U_x
    logical,intent(out) :: NaN
    integer :: ic,jc
    
    NaN=.false.
    do ic=1,3;do jc=1,3
       if(isnan(real(U_x%cl(ic,jc)))) then
          write(*,*) "NaN Detected"
          write(*,*) ic,jc
          NaN=.true.
       endif
    enddo;enddo

  end subroutine eNaNl

end module StoutLinks

