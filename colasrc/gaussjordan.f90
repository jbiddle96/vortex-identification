
module GaussJordan

  use Kinds
  use nrutil, only : assert_eq,nrerror,outerand,outerprod,swap,imaxloc
  implicit none
  private

  public :: GaussJordanInv
  public :: CG_n
  public :: CR_n
  public :: HouseholderQR
  public :: det
  public :: ludcmp

contains

  subroutine GaussJordanInv(n,b,a)
    ! begin args: n, b, a
    integer :: n
    complex(dc), dimension(n,n) :: b, a
    !Invert matrix by Gauss-Jordan elimination, equation (2.1.1). b is an NxN input
    !coefficient matrix. a is its inverse.

    ! begin local_vars
    integer, dimension(n) :: ipiv,indxr,indxc
    !These arrays are used for bookkeeping on the pivoting.
    logical, dimension(n) :: lpiv
    complex(dc) :: pivinv
    complex(dc), dimension(n) :: dumc
    integer, target :: irc(2)
    integer :: i,l
    integer, pointer :: irow,icol

    ! begin execution

    a = b

    irow => irc(1)
    icol => irc(2)
    ipiv=0
    do i=1,n !Main loop over columns to be reduced.
       lpiv = (ipiv == 0) !Begin search for a pivot element.
       irc=maxloc(abs(a),outerand(lpiv,lpiv))
       ipiv(icol)=ipiv(icol)+1
       if (ipiv(icol) > 1) call nrerror('gaussj: singular matrix (1)')
       !We now have the pivot element, so we interchange rows, if needed, to put the pivot
       !element on the diagonal. The columns are not physically interchanged, only relabeled:
       !indxc(i), the column of the ith pivot element, is the ith column that is reduced, while
       !indxr(i) is the row in which that pivot element was originally located. If indxr(i) =
       !indxc(i) there is an implied column interchange. With this form of bookkeeping, the
       !solution b's will end up in the correct order, and the inverse matrix will be scrambled
       !by columns.

       if (irow /= icol) then
          call swap(a(irow,:),a(icol,:))
       end if

       indxr(i)=irow !We are now ready to divide the pivot row by the pivot element, located at irow and icol.
       indxc(i)=icol

       if (a(icol,icol) == 0.0) call nrerror('gaussj: singular matrix (2)')
       pivinv=1.0_dp/a(icol,icol)
       a(icol,icol)=1.0d0
       a(icol,:)=a(icol,:)*pivinv
       dumc=a(:,icol) !Next, we reduce the rows, except for the pivot one, of course.
       a(:,icol)=0.0d0

       a(icol,icol)=pivinv
       a(1:icol-1,:)=a(1:icol-1,:)-outerprod(dumc(1:icol-1),a(icol,:))
       a(icol+1:,:)=a(icol+1:,:)-outerprod(dumc(icol+1:),a(icol,:))
    end do

    !It only remains to unscramble the solution in view of the column interchanges. We do this
    !by interchanging pairs of columns in the reverse order that the permutation was built up.
    do l=n,1,-1
       call swap(a(:,indxr(l)),a(:,indxc(l)))
    end do


  end subroutine GaussJordanInv

  subroutine CG_n(n, b, x, M, tolerance, iterations)
    ! begin args: n, b, x, M, tolerance, iterations

    !A routine to solve the matrix equation M x = b, using an initial guess x
    !M _must_ be hermitian and positive definite.

    integer :: n
    complex(dc), dimension(n) :: b, x
    complex(dc), dimension(n,n) :: M
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required

    ! begin local_vars
    complex(dc), dimension(n) :: r
    complex(dc), dimension(n) :: p, Mp
    real(dp) :: delta, prevdelta, norm_b, omega, theta, pdotMp

    ! begin execution

    norm_b = sqrt(sum(abs(b)**2))

    Mp = matmul(M,x)

    r = b - Mp
    p = r

    delta = sum(abs(r)**2)

    Mp = matmul(M,p)

    pdotMp = real(sum(conjg(p)*Mp),dp)

    iterations = 0

    do
       if (sqrt(delta)/norm_b < tolerance) exit
       print '(I4,4F20.16)', iterations, sqrt(delta)/norm_b !, theta, omega, pdotMp

       iterations = iterations + 1

       omega = delta/pdotMp

       r = r - omega*Mp

       x = x + omega*p

       prevdelta = delta
       delta = sum(abs(r)**2)

       theta = -delta/prevdelta

       p = r - theta*p

       Mp = matmul(M,p)

       pdotMp = real(sum(conjg(p)*Mp),dp)

    end do


  end subroutine CG_n

  subroutine CR_n(n, b, x, M, tolerance, iterations)
    ! begin args: n, b, x, M, tolerance, iterations

    !A routine to solve the matrix equation M x = b, using an initial guess x
    !M _must_ be hermitian and positive definite.

    integer :: n
    complex(dc), dimension(n) :: b, x
    complex(dc), dimension(n,n) :: M
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required

    ! begin local_vars
    complex(dc), dimension(n) :: r, Mr
    complex(dc), dimension(n) :: p, Mp
    real(dp) :: delta, prevdelta, norm_b, omega, theta, pdotMp, deltaM, prevdeltaM

    ! begin execution

    norm_b = sqrt(sum(abs(b)**2))

    Mp = matmul(M,x)

    r = b - Mp
    p = r

    delta = sum(abs(r)**2)
    Mr = matmul(M,r)
    deltaM = real(sum(conjg(r)*Mr),dp)

    Mp = matmul(M,p)

    pdotMp = real(sum(conjg(Mp)*Mp),dp)

    iterations = 0

    do
       if (sqrt(delta)/norm_b < tolerance) exit
       !print '(I4,4F20.16)', iterations, sqrt(delta)/norm_b !, theta, omega, pdotMp

       iterations = iterations + 1
       if ( iterations > 5000 ) exit

       omega = deltaM/pdotMp

       r = r - omega*Mp

       x = x + omega*p

       Mr = matmul(M,r)
       prevdeltaM = deltaM
       delta = sum(abs(r)**2)
       deltaM = real(sum(conjg(r)*Mr),dp)

       theta = -deltaM/prevdeltaM

       p = r - theta*p

       Mp = matmul(M,p)

       pdotMp = real(sum(conjg(Mp)*Mp),dp)

    end do


  end subroutine CR_n

  subroutine HouseholderQR(n,A,R,Q)
    ! begin args: n, A, R, Q

    integer :: n
    complex(dc), dimension(n,n) :: A, R
    complex(dc), dimension(n,n), optional :: Q
    ! begin local_vars
    complex(dc), dimension(n,n) :: P, I, Qdag_h, Qdag
    complex(dc), dimension(n) :: x, v, e1
    complex(dc) :: alpha, beta
    real(dp) :: norm_x, norm_v, abs_x1
    integer :: k,l,m

    ! Computes upper triangular R such that A = QR
    ! begin execution

    R = A

    e1 = 0.0d0
    e1(1) = 1.0d0

    if ( present(Q) ) then
       I = 0.0d0
       do k=1,n
          I(k,k) = 1.0d0
       end do
       Qdag = I
    end if

    do k=1,n
       x = 0.0d0
       v = 0.0d0
       m = n - k + 1

       x(1:m) = R(k:n,k)
       norm_x = sqrt(sum(abs(x(1:m))**2))
       abs_x1 = abs(x(1))
       alpha = -norm_x*x(1)/abs_x1 ! ||x|| sgn(x_1)
       v(1:m) = x(1:m) - alpha*e1(1:m)

       beta = 1.0d0/(norm_x*(norm_x + abs_x1))
       !norm_v = sqrt(sum(abs(v(1:m))**2))
       !v(1:m) = v(1:m)/norm_v

       ! Apply H = I - 2 Re(alpha) v v^dag
       do l=1,m
          !P(l,1:m) = 2.0d0*real(alpha,dp)*v(l)*conjg(v(1:m))
          P(l,1:m) = beta*v(l)*conjg(v(1:m))
       end do

       ! R has partial zeros, so we can update like so:

       R(k:n,k:n) = R(k:n,k:n) - matmul(P(1:m,1:m),R(k:n,k:n))
       if ( present(Q) ) then
          ! Q^dag does not have partial zeros so we update in the usual way
          Qdag_h = I
          Qdag_h(k:n,k:n) = I(1:m,1:m) - P(1:m,1:m)
          Qdag = matmul(Qdag_h,Qdag)
       end if

    end do

    if ( present(Q) ) then
       Q = conjg(transpose(Qdag))
    end if


  end subroutine HouseholderQR

  recursive function det(A) result (z)
    ! begin args: A

    complex(dc), dimension(:,:) :: A

    ! begin local_vars
    complex(dc) :: z

    integer :: n,j,k,l
    real(dp) :: pm
    complex(dc), dimension(size(A,dim=1)-1,size(A,dim=2)-1) :: A_j

    ! begin execution

    n = size(A,dim=1)
    if ( n == 2 ) then
       z = A(1,1)*A(2,2) - A(1,2)*A(2,1)
       !print *, n, z
    else
       z= 0.0d0
       pm = 1.0d0
       do j=1,n
          l = 1
          do k=1,n
             if ( k /= j ) then
                A_j(1:n-1,l) = A(2:n,k)
                l = l + 1
             end if
          end do
          z = z + pm*det(A_j)
          !pm = -pm
       end do
       !print *, n, z

    end if

  end function det

  subroutine ludcmp(m,a,indx,d)
    ! begin args: m, a, indx, d
    complex(dc), dimension(:,:), intent(INOUT) :: m, a
    integer, dimension(:), intent(OUT) :: indx
    real(dp), intent(OUT) :: d
    !Given an N x N input matrix a, this routine replaces it by the LU decomposition of a
    !rowwise permutation of itself. On output, a is arranged as in equation (2.3.14); indx is an
    !output vector of length N that records the row permutation effected by the partial pivoting;
    !d is output as +/- 1 depending on whether the number of row interchanges was even or odd,
    !respectively. This routine is used in combination with lubksb to solve linear equations or
    !invert a matrix.
    ! begin local_vars
    real(dp), dimension(size(a,1)) :: vv !vv stores the implicit scaling of each row.
    integer :: j,n,imax
    ! begin execution

    n=assert_eq(size(a,1),size(a,2),size(indx),'ludcmp')
    a = m
    d=1.0_dp !No row interchanges yet.
    vv=maxval(abs(a),dim=2) !Loop over rows to get the implicit scaling information.
    if (any(vv == 0.0d0)) call nrerror('singular matrix in ludcmp') !There is a row of zeros.
    vv=1.0_dp/vv !save the scaling.
    do j=1,n
       imax=(j-1)+imaxloc(vv(j:n)*abs(a(j:n,j))) !Find the pivot row.
       if (j /= imax) then !do we need to interchange rows?
          call swap(a(imax,:),a(j,:)) !Yes, do so...
          d=-d !...and change the parity of d.
          vv(imax)=vv(j) !Also interchange the scale factor.
       end if
       indx(j)=imax

       !if (a(j,j) == 0.0_dp) !Singular matrix
       !if the pivot element is zero the matrix is singular (at least to the precision of the algorithm).

       a(j+1:n,j)=a(j+1:n,j)/a(j,j) !Divide by the pivot element.
       a(j+1:n,j+1:n)=a(j+1:n,j+1:n)-outerprod(a(j+1:n,j),a(j,j+1:n)) !Reduce remaining submatrix.

    end do

  end subroutine ludcmp

end module GaussJordan


