!! Filename : $Source: /home/tsmat/waseem/cocacola/include/RCS/hybridmontecarlo.f90,v $
!! Author : Waseem Kamleh
!! Created On : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author: waseem $
!! Last Modified On: $Date: 2005/11/17 11:57:15 $
!! Branch Tag : $Name: $
!! Revision : $Revision: 1.1 $
!! Update History : $Log: hybridmontecarlo.f90,v $
!! Update History : Revision 1.1 2005/11/17 11:57:15 waseem
!! Update History : Initial revision
!! Update History :
/* wilson.h */



module wilsonHybridMonteCarlo

  use AccMinEVCG
  use ConjGradSolvers
  use GaugeAction
  use GL3Diag
  use FatLinks



  use CloverFmunu
  use wilsonFermionMD
  use wilsonPolyHMC
  use wilsonMatrix



  use ZolotarevApprox
  use Timer
  use SpinorTypes
  use FermionTypes
  use ReduceOps
  use MatrixAlgebra
  use CollectiveOps
  use FermionField
  use MPIInterface
  use GaugeFieldMPIComms
  use LatticeSize
  use GaugeField
  use GaugeFieldIO
  use ColourTypes
  use Kinds
  use ColourFieldOps
  implicit none
  private
  logical, public :: quenched = .false., two_flavour = .true., three_flavour = .false., multijob = .false., benchmarking = .false.
  real(dp), public :: dt = 0.02d0, dt_2pf = 0.02d0, dt_1pf = 0.02d0, dt_p2f = 0.02d0, dt_p1f = 0.02d0, dt_q2f = 0.02d0, dt_q1f = 0.02d0 !Use multiple time steps, (shorter for the gauge action)
  integer, public :: n_hmc = 10, n_md = 50, n_md2pf = 50, n_md1pf = 50, n_mdp2f = 50, n_mdp1f = 50, n_mdq2f = 50, n_mdq1f = 50
  real(dp), public :: beta_md !The beta value to use in updating the configuration.
  !Some global variables to track quantities along the HMC trajectory
  integer, parameter, public :: nupdate_max = 100000, ntraj_max = 150000, nwindow = 50
  integer, public :: ntraj = 0, nupdates = 0, ntherm_hmc, nfixuzero, ngen_hmc, njob_hmc
  real(dp), dimension(nupdate_max), public :: plaquette, topQ, plaq_fl
  real(dp), dimension(ntraj_max), public :: dH
  logical, public :: thermalising = .false.
  integer , public :: count_hmc=0, count_md=0, count_gf=0, count_gm=0, count_2fm=0, count_pm2f=0, count_1fm=0, count_pm1f=0, count_io=0
  real(dp), public :: t_hmc, t_md, t_gf, t_gm, t_2fm, t_pm2f, t_1fm, t_pm1f, t_io
  real(dp), public :: maxt_hmc,maxt_md, maxt_gf, maxt_gm, maxt_2fm, maxt_pm2f, maxt_1fm, maxt_pm1f, maxt_io
  real(dp), public :: mint_hmc,mint_md, mint_gf, mint_gm, mint_2fm, mint_pm2f, mint_1fm, mint_pm1f, mint_io
  integer , public :: count_i2fm=0, count_2pfm=0, count_s2pf=0, count_i1fm=0, count_1pfm=0, count_s1pf=0, count_P2fm=0, count_sP2f=0, count_P1fm=0, count_sP1f=0
  real(dp), public :: t_i2fm=0, t_2pfm=0, t_s2pf=0, t_i1fm=0, t_1pfm=0, t_s1pf=0, t_P2fm=0, t_sP2f=0, t_P1fm=0, t_sP1f=0
  real(dp), public :: maxt_i2fm=0, maxt_2pfm=0, maxt_s2pf=0, maxt_i1fm=0, maxt_1pfm=0, maxt_s1pf=0, maxt_P2fm=0, maxt_sP2f=0, maxt_P1fm=0, maxt_sP1f=0
  real(dp), public :: mint_i2fm=0, mint_2pfm=0, mint_s2pf=0, mint_i1fm=0, mint_1pfm=0, mint_s1pf=0, mint_P2fm=0, mint_sP2f=0, mint_P1fm=0, mint_sP1f=0
  integer, public :: count_qm2f=0, count_Q2fm=0, count_sQ2f=0
  real(dp), public :: t_qm2f, t_Q2fm, t_sQ2f, maxt_qm2f=0, maxt_Q2fm=0, maxt_sQ2f=0, mint_qm2f, mint_Q2fm, mint_sQ2f
  integer, public :: count_qm1f=0, count_Q1fm=0, count_sQ1f=0
  real(dp), public :: t_qm1f, t_Q1fm, t_sQ1f, maxt_qm1f=0, maxt_Q1fm=0, maxt_sQ1f=0, mint_qm1f, mint_Q1fm, mint_sQ1f
  character(len=256), public :: traj_report_name, hmc_report_name, cg_report_name, force_report_name
  character(len=256), public :: F_x_report_name
  integer, public :: traj_report_file = 121, hmc_report_file = 122, cg_report_file=123, force_report_file = 124, traj_data_file=130
  integer, public :: F_x_report_file=125
  integer, public :: updates,trajectories
  real(dp), public :: F_g, F_gmin, F_gmax, F_2pf, F_2pfmin, F_2pfmax, F_p2f, F_p2fmin, F_p2fmax, F_1pf, F_1pfmin, F_1pfmax, F_p1f, F_p1fmin, F_p1fmax
  real(dp), public :: F_q2f, F_q2fmin, F_q2fmax, F_q1f, F_q1fmin, F_q1fmax
  integer, public :: nF_g=0, nF_2pf=0, nF_1pf=0, nF_p2f=0, nF_p1f=0, nF_q2f=0, nF_q1f=0
  real(dp), dimension(1:3), public :: F_xg, F_x2pf, F_xP2f, F_x1pf, F_xP1f, F_xQ2f, F_xQ1f
  integer, public :: nF_xg=0, nF_x2pf=0, nF_xP2f=0, nF_x1pf=0, nF_xP1f=0, nF_xQ2f=0, nF_xQ1f=0
  type(colour_matrix), dimension(:,:,:,:,:), private, allocatable :: P_xd !conjugate momenta
  type(colour_matrix), dimension(:,:,:,:,:), private, allocatable :: Upr_xd !updated gauge field, U'
  type(colour_matrix), dimension(:,:,:,:,:), private, allocatable :: Ppr_xd !updated momenta, P'
  type(colour_vector), dimension(:,:,:,:,:), private, allocatable :: phi2f, eta2f, eta2f_pr
  type(colour_vector), dimension(:,:,:,:,:), private, allocatable :: phi1f, eta1f, eta1f_pr
  type(colour_vector), dimension(:,:,:,:,:), private, allocatable :: phiP2f, etaP2f, etaP2f_pr
  type(colour_vector), dimension(:,:,:,:,:), private, allocatable :: phiP1f, etaP1f, etaP1f_pr
  type(colour_vector), dimension(:,:,:,:,:), private, allocatable :: phiQ2f, etaQ2f, etaQ2f_pr
  type(colour_vector), dimension(:,:,:,:,:), private, allocatable :: phiQ1f, etaQ1f, etaQ1f_pr
  !! type(colour_matrix), dimension(:,:,:,:,:), private, allocatable :: U0_xd, Upr2_xd !replay gauge field, U'_2
  character(len=256), public :: traj_data_name, hmc_cfg_name
  real(dp), public :: sqrt_error = 0.0d0, poly_error = 0.0d0
  real(dp), public :: md_cg_tol = 1.0d-7
  integer, public :: ireplay = 0
  logical, public :: UpdateMatrixFields = .true.
  public :: HMCTrajectory
  public :: IntegratedAutoCorrelation
  public :: AutoCorrelation
  public :: UpdateGaugeField
  public :: UpdateMomenta
  public :: UpdateGaugeMomenta
  public :: UpdateFermionicMomenta_2f
  public :: UpdateFermionicMomenta_1f
  public :: UpdatePolynomialMomenta_2f
  public :: UpdatePolynomialMomenta_1f
  public :: UpdatePseudofermionField_2f
  public :: UpdatePseudofermionField_1f
  public :: UpdatePolynomialField_2f
  public :: UpdatePolynomialField_1f
  public :: GetKE
  public :: GL3GaussianField
  public :: ReadHMCTrajData
  public :: WriteHMCTrajData
  public :: InitialiseHMC
  public :: FinaliseHMC
contains
  subroutine HMCTrajectory(U_xd,icfg)
    ! begin args: U_xd, icfg
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd !gauge field
    integer :: icfg
    ! begin local_vars
    real(DP) :: rho_accept, accept_prob, delta_H, delta_g, delta_KE, delta_pf
    integer :: i_md, i_md2, ic, is, iterations
    integer :: cgiter(n_md), mcgiter(n_md), eviter(n_md)
    real(DP) :: tau_int, KEbar, S_gbar, S_pfbar, expdHbar, dHbar, p_acc, tau_intQ
    real(DP) :: S_gU, S_gUpr, S_pf, S_pfpr, KE_P, KE_Ppr, H, Hpr, deltau0, Q
    real(dp) :: S_2pf, S_1pf, S_2pfpr, S_1pfpr, S_2pfbar, S_1pfbar, delta_2pf, delta_1pf
    real(dp) :: S_P2f, S_P2fpr, S_P2fbar, delta_P2f, S_P1f, S_P1fpr, S_P1fbar, delta_P1f
    real(dp) :: S_Q2f, S_Q2fpr, S_Q2fbar, delta_Q2f, S_Q1f, S_Q1fpr, S_Q1fbar, delta_Q1f
    real(dp) :: S_det2fU, S_det2fUpr, delta_det2f, S_det1fU, S_det1fUpr, delta_det1f
    character(len=6) :: yesno
    real(dp) :: t0, t1, t_in, t_out
    integer :: iaccept
    integer :: ix,iy,iz,it,mu
    integer :: n_xf ! number of fermion lattice sites
    !Perform a single HMC trajectory, that is
    !perform n_hmc (accepted) HMC updates of the gauge field
    !such that the final gauge field is uncorrelated with the starting gauge field
    ! begin execution
    beta_md = beta
    iterations = 0
    call GetUZero(U_xd,uzero)
    if (i_am_root) print *, uzero
    phi2f = zero_vector
    phi1f = zero_vector
    phiP2f = zero_vector
    phiP1f = zero_vector
    phiQ2f = zero_vector
    Upr_xd(1:nxs,1:nys,1:nzs,1:nts,:) = U_xd
    call ShadowGaugeField(Upr_xd,2)
    if ( i_am_root ) then
       open(file=hmc_report_name,unit=hmc_report_file,form="formatted",action="write",status="unknown",position="append")
       open(file=traj_report_name,unit=traj_report_file,form="formatted",action="write",status="unknown",position="append")
       open(file=cg_report_name,unit=cg_report_file,form="formatted",action="write",status="unknown",position="append")
       open(file=force_report_name,unit=force_report_file,form="formatted",action="write",status="unknown",position="append")
       open(file=F_x_report_name,unit=F_x_report_file,form="formatted",action="write",status="unknown",position="append")
       write(*,'(2A6,4A10,A12,A8,2A12)') "#Traj", "#acc", "uzero", "u0bar","u0fl_bar", "Q", "delta H", "accept?", "<dH>","<e^-dH>"
       write(hmc_report_file,'(2A6,4A10,A12,A8,2A12)') "#Traj", "#acc", "uzero", "u0bar","u0fl_bar", "Q", "delta H", "accept?", &
            & "<dH>","<e^-dH>"
       write(traj_report_file,'(2A6,7A12,5A10)') "#Traj", "#acc", "delta KE", "delta S_g", "delta S_2pf", "delta S_1pf","delta S_P2f", "delta_SP1f", "delta H", &
            & "ev_min", "ev_max", "proj_min", "proj_max", "sqrt err"
       write(cg_report_file,'(2A6,13A7)') "#Traj", "#acc", "2fcg-", "2fcg+", "2fcg", "1fcg-", "1fcg+", "1fcg", &
            & "evcg-", "evcg+", "evcg", "zmin", "zmax", "pole-","pole+"
       write(force_report_file,'(12A10)') "dt*F_g", "dt*F_P2f", "dt*F_P1f", "dt*F_Q2f", "dt*F_2pf", "dt*F_1pf", "F_g", "F_P2f", "F_P1f", "F_Q2f", "F_2pf", "F_1pf"
       write(F_x_report_file,'(18A7)') "", "F_xg", "", "", "F_x2pf", "", "", "F_x1pf", "", "", "F_xP2f", "", "", "F_xP1f", "", "", "F_xQ2f", ""
       close(hmc_report_file)
       close(traj_report_file)
       close(cg_report_file)
       close(force_report_file)
       close(F_x_report_file)
    end if
    do
       t_in = mpi_wtime()
       ev_calls = 0
       ev_min = 0.0d0
       ev_max = 0.0d0
       traj_minev = 0.0d0
       traj_maxev = 0.0d0
       iter_cg = 0
       iter_multicg = 0
       iter_ev = 0
       min_z = 0
       max_z = 0
       pole_min = 0
       pole_max = 0
       nF_xg=0; nF_x2pf=0; nF_xP2f=0; nF_x1pf=0; nF_xP1f=0; nF_xQ2f=0
       if ( ireplay <= 1 ) then
          !Refresh the momenta, for ergodicity, for normal HMC when we are not replaying a trajectory.
          call GL3GaussianField(P_xd)
       end if
       Ppr_xd = P_xd
       S_gU = S_gauge(Upr_xd,beta)
       !Perform n_md Molecular Dynamics updates of the coordinates and conjugate momenta
       !To ensure reversibility, we use a leapfrog algorithm, which requires
       !that we perform an initial and final half-step on the momenta.
       UpdateMatrixFields = .true. ! IMPORTANT: This flag controls when the fermion link fields need updating.
       tolerance_cg = 1.0d-14
       eta2f = zero_vector
       etaP2f = zero_vector
       etaQ2f = zero_vector
       S_det2fU = 0.0d0
       S_det1fU = 0.0d0
       if ( two_flavour ) then
          call UpdatePseudofermionField_2f(Upr_xd,phi2f,eta2f,.true.)
          if ( poly_filter ) then
             call UpdatePolynomialField_2f(Upr_xd,phiP2f,etaP2f,c_p2f,z_p2f,.true.)
             if ( n_q2f > 0 ) call UpdatePolynomialField_2f(Upr_xd,phiQ2f,etaQ2f,c_q2f,z_q2f,.true.)
          end if
       end if
       eta1f = zero_vector
       etaP1f = zero_vector
       if ( three_flavour ) then
          call UpdatePseudoFermionField_1f(Upr_xd,phi1f,eta1f,.true.)
          if ( poly_filter ) then
             call UpdatePolynomialField_1f(Upr_xd,phiP1f,etaP1f,c_p1f,z_p1f,.true.)
             !if ( n_q1f > 0 ) call UpdatePolynomialField_1f(Upr_xd,phiQ1f,etaQ1f,c_q1f,z_q1f,.true.)
          end if
       end if
       tolerance_cg = md_cg_tol
       if ( three_flavour ) call UpdateFermionicMomenta_1f(Upr_xd, Ppr_xd, phi1f, dt_1pf/2.0d0)
       if ( two_flavour ) call UpdateFermionicMomenta_2f(Upr_xd, Ppr_xd, phi2f, dt_2pf/2.0d0)
       if ( poly_filter .and. (n_q2f > 0) ) call UpdatePolynomialMomenta_2f(Upr_xd, Ppr_xd, phiQ2f, c_q2f, z_q2f, dt_q2f/2.0d0 )
       if ( three_flavour .and. poly_filter ) call UpdatePolynomialMomenta_1f(Upr_xd, Ppr_xd, phiP1f, c_p1f, z_p1f, dt_p1f/2.0d0 )
       if ( poly_filter ) call UpdatePolynomialMomenta_2f(Upr_xd, Ppr_xd, phiP2f, c_p2f, z_p2f, dt_p2f/2.0d0 )
       call UpdateGaugeMomenta(Upr_xd, Ppr_xd, dt/2.0d0)
       do i_md=1,n_md-1
          t0 = mpi_wtime()
          call UpdateGaugeField(Upr_xd, Ppr_xd, dt)
          if ( modulo(i_md,n_md/n_md1pf) == 0 .and. three_flavour ) call UpdateFermionicMomenta_1f(Upr_xd, Ppr_xd, phi1f, dt_1pf)
          if ( modulo(i_md,n_md/n_md2pf) == 0 .and. two_flavour ) call UpdateFermionicMomenta_2f(Upr_xd, Ppr_xd, phi2f, dt_2pf)
          if ( modulo(i_md,n_md/n_mdq2f) == 0 .and. poly_filter .and. (n_q2f > 0) ) call UpdatePolynomialMomenta_2f(Upr_xd, Ppr_xd, phiQ2f, c_q2f, z_q2f, dt_q2f )
          if ( modulo(i_md,n_md/n_mdp1f) == 0 .and. three_flavour .and. poly_filter ) call UpdatePolynomialMomenta_1f(Upr_xd, Ppr_xd, phiP1f, c_p1f, z_p1f, dt_p1f )
          if ( modulo(i_md,n_md/n_mdp2f) == 0 .and. poly_filter ) call UpdatePolynomialMomenta_2f(Upr_xd, Ppr_xd, phiP2f, c_p2f, z_p2f, dt_p2f )
          call UpdateGaugeMomenta(Upr_xd, Ppr_xd, dt)
          cgiter(i_md) = iter_cg
          mcgiter(i_md) = iter_multicg
          eviter(i_md) = iter_ev
          t1 = mpi_wtime()
          call TimingUpdate(count_md,t1,t0,mint_md,maxt_md,t_md)
       end do
       call UpdateGaugeField(Upr_xd, Ppr_xd, dt)
       t0 = mpi_wtime()
       call UpdateGaugeMomenta(Upr_xd, Ppr_xd, dt/2)
       if ( poly_filter ) call UpdatePolynomialMomenta_2f(Upr_xd, Ppr_xd, phiP2f, c_p2f, z_p2f, dt_p2f/2.0d0 )
       if ( three_flavour .and. poly_filter ) call UpdatePolynomialMomenta_1f(Upr_xd, Ppr_xd, phiP1f, c_p1f, z_p1f, dt_p1f/2.0d0 )
       if ( poly_filter .and. (n_q2f > 0) ) call UpdatePolynomialMomenta_2f(Upr_xd, Ppr_xd, phiQ2f, c_q2f, z_q2f, dt_q2f/2.0d0 )
       if ( two_flavour ) call UpdateFermionicMomenta_2f(Upr_xd, Ppr_xd, phi2f, dt_2pf/2.0d0)
       if ( three_flavour ) call UpdateFermionicMomenta_1f(Upr_xd, Ppr_xd, phi1f, dt_1pf/2.0d0)
       cgiter(n_md) = iter_cg
       mcgiter(n_md) = iter_multicg
       eviter(n_md) = iter_ev
       t1 = mpi_wtime()
       call TimingUpdate(count_md,t1,t0,mint_md,maxt_md,t_md)
       !At the end of each trajectory, enforce the unitarity of U_xd
       call FixGaugeField(Upr_xd)
       call ShadowGaugeField(Upr_xd,1)
       call ShadowGaugeField(Upr_xd,2)
       tolerance_cg = 1.0d-14
       eta2f_pr = zero_vector
       etaP2f_pr = zero_vector
       etaQ2f_pr = zero_vector
       if ( two_flavour ) then
          call UpdatePseudofermionField_2f(Upr_xd,phi2f,eta2f_pr,.false.)
          if ( poly_filter ) then
             call UpdatePolynomialField_2f(Upr_xd,phiP2f,etaP2f_pr,c_p2f,z_p2f,.false.)
             if ( n_q2f > 0 ) call UpdatePolynomialField_2f(Upr_xd,phiQ2f,etaQ2f_pr,c_q2f,z_q2f,.false.)
          end if
       end if
       eta1f_pr = zero_vector
       etaP1f_pr = zero_vector
       if ( three_flavour ) then
          call UpdatePseudoFermionField_1f(Upr_xd,phi1f,eta1f_pr,.false.)
          if ( poly_filter ) then
             call UpdatePolynomialField_1f(Upr_xd,phiP1f,etaP1f_pr,c_p1f,z_p1f,.false.)
             !if ( n_q1f > 0 ) call UpdatePolynomialField_1f(Upr_xd,phiQ1f,etaQ1f_pr,c_q1f,z_q1f,.false.)
          end if
       end if
       KE_P = GetKE(P_xd)
       KE_Ppr = GetKE(Ppr_xd)
       !Average the kinetic energy over the momenta matrices
       KEbar = KE_Ppr/(nlattice*nd)
       delta_KE = KE_Ppr - KE_P
       S_gUpr = S_gauge(Upr_xd,beta)
       !Average the action over the 3 unique plaquettes associated with each link matrix.
       S_gbar = S_gUpr/(nlattice*nd*(nd-1))
       delta_g = S_gUpr - S_gU
       S_det2fUpr = 0.0d0
       S_det1fUpr = 0.0d0
       delta_det2f = S_det2fUpr - S_det2fU
       delta_det1f = S_det1fUpr - S_det1fU
       !The contribution from the determinant term for EO-preconditioned clover
       n_xf = nlattice
       S_2pf = real_inner_product(phi2f,eta2f)
       S_2pfpr = real_inner_product(phi2f,eta2f_pr)
       S_1pf = real_inner_product(phi1f,eta1f)
       S_1pfpr = real_inner_product(phi1f,eta1f_pr)
       !Average the pseudo fermionic action over each pseudofermionic vector.
       S_2pfbar = S_2pfpr/n_xf
       S_1pfbar = S_1pfpr/n_xf
       delta_2pf = S_2pfpr - S_2pf
       delta_1pf = S_1pfpr - S_1pf
       S_P2f = real_inner_product(phiP2f,etaP2f)
       S_P2fpr = real_inner_product(phiP2f,etaP2f_pr)
       S_P1f = real_inner_product(phiP1f,etaP1f)
       S_P1fpr = real_inner_product(phiP1f,etaP1f_pr)
       !Average the polynomial pseudo fermionic action over each pseudofermionic vector.
       S_P2fbar = S_P2fpr/n_xf
       delta_P2f = S_P2fpr - S_P2f
       S_P1fbar = S_P1fpr/n_xf
       delta_P1f = S_P1fpr - S_P1f
       S_Q2f = real_inner_product(phiQ2f,etaQ2f)
       S_Q2fpr = real_inner_product(phiQ2f,etaQ2f_pr)
       !Average the polynomial pseudo fermionic action over each pseudofermionic vector.
       S_Q2fbar = S_Q2fpr/n_xf
       delta_Q2f = S_Q2fpr - S_Q2f
       !if (i_am_root) print '(5F10.5)', S_2pf/n_xf, S_P2f/n_xf, S_Q2f/n_xf, S_1pf/n_xf, S_P1f/n_xf
       !if (i_am_root) print '(5F10.5)', S_2pfbar, S_P2fbar, S_Q2fbar, S_1pfbar, S_P1fbar
       Hpr = KE_Ppr + S_gUpr + S_2pfpr + S_1pfpr + S_P2fpr + S_P1fpr + S_Q2fpr + S_det2fUpr + S_det1fUpr
       H = KE_P + S_gU + S_2pf + S_1pf + S_P2f + S_P1f + S_Q2f + S_det2fU + S_det1fU
       delta_H = Hpr - H
       if ( i_am_root ) call random_number(rho_accept)
       call Broadcast(rho_accept,mpi_root_rank)
       if ( delta_H <= 0 ) then
          accept_prob = 1.0d0
       else
          accept_prob = exp(-delta_H)
       end if
       if ( benchmarking ) accept_prob = 1.0d0
       !Calculate the (gauge) topological charge.
       ! Fix this: broken uzerofl will be reported.
       call GetUZero(Upr_xd,uzero)
       call APESmearLinks(Upr_xd, UFL_xd, 0.7d0, 4)
       !call StoutSmearLinks(Upr_xd, UFL_xd, 0.1d0, 4)
       call GetUZero(UFL_xd,uzerofl)
       call CalculateFmunuClover(F_munu,UFL_xd)
       Q = GetTopQ(F_munu)/(uzerofl**4)
       if ( rho_accept < accept_prob ) then
          ! Accept the proposed configuration
          yesno = "accept"
          iaccept = 1
       else
          ! Reject the proposed configuration
          yesno = "reject"
          iaccept = 0
       end if
!! ! ireplay = 0: normal HMC
!! ! ireplay = 1: HMC with replay trick, standard dt
!! ! ireplay = 2: replay previous trajectory, reduced dt
!! ! ireplay = 3: reverse previous trajectory, standard dt
!! ! reference: hep-lat/0409167
!!
!!
!! if ( ireplay == 0 ) then
!! if ( rho_accept < accept_prob ) then
!! ! Accept the proposed configuration
!! yesno = "accept"
!! iaccept = 1
!! else
!! ! Reject the proposed configuration
!! yesno = "reject"
!! iaccept = 0
!! end if
!! else if ( ireplay == 1 ) then
!! if ( abs(delta_H) < 1.0d0 ) then
!! if ( rho_accept < accept_prob ) then
!! ! Accept the proposed configuration
!! yesno = "accept"
!! iaccept = 1
!! else
!! ! Reject the proposed configuration
!! yesno = "reject"
!! iaccept = 0
!! end if
!! else
!! ! Reject the proposed configuration U'_1 and replay the trajectory
!! ! at a smaller step size. Momenta will not be refreshed.
!! ireplay = 2
!! U0_xd(1:nxs,1:nys,1:nzs,1:nts,:) = U_xd(1:nxs,1:nys,1:nzs,1:nts,:)
!! yesno = "replay"
!! iaccept = -3
!!
!! ! Reduce step size
!! n_md2pf = 2*n_md2pf
!! n_md1pf = 2*n_md1pf
!! dt_2pf = (dt*n_md)/n_md2pf
!! dt_1pf = (dt*n_md)/n_md1pf
!! end if
!! else if ( ireplay == 2 ) then
!! if ( rho_accept < accept_prob ) then
!! ! Tentatively accept the proposed configuration U'_2, but we must
!! ! perform a return trajectory at normal stepsize to ensure reversibility.
!! ireplay = 3
!! yesno = "return"
!! iaccept = 2
!!
!! do mu=1,nd
!! do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
!! P_xd(ix,iy,iz,it,mu)%Cl = -Ppr_xd(ix,iy,iz,it,mu)%Cl
!! end do; end do; end do; end do
!! end do
!! else
!! ! Reject the proposed configuration U'_2 and return to normal step size.
!! ireplay = 1
!! yesno = "reject"
!! iaccept = 0
!! end if
!! ! Increase step size
!! n_md2pf = n_md2pf/2
!! n_md1pf = n_md1pf/2
!! dt_2pf = (dt*n_md)/n_md2pf
!! dt_1pf = (dt*n_md)/n_md1pf
!! else if ( ireplay == 3) then
!! ireplay = 1
!! if ( abs(delta_H) > 1.0d0 ) then
!! ! Reversibility test passed, accept the proposed configuration U'_2.
!! yesno = "accept"
!! ! revert to U_2.
!! iaccept = -1
!! nupdates = nupdates + 1
!! else
!! ! Reversibility test failed, reject the proposed configuration U'_2,
!! yesno = "reject"
!! ! revert to U_0.
!! U_xd(1:nxs,1:nys,1:nzs,1:nts,:) = U0_xd(1:nxs,1:nys,1:nzs,1:nts,:)
!! iaccept = -2
!! end if
!! end if
       ! iaccept >= 1 : Keep the proposed configuration.
       ! iaccept == 0 : Reject the proposed configuration.
       ! iaccept <= -1 : Trajectory is either being replayed or was a reverse trajectory, ignore proposed configuration.
       if ( iaccept >= 1 ) then
          U_xd = Upr_xd(1:nxs,1:nys,1:nzs,1:nts,:)
          nupdates = nupdates + 1
          plaquette(nupdates) = uzero**4
          topQ(nupdates) = Q
          plaq_fl(nupdates) = uzerofl**4
          if ( thermalising ) then
             !if ( nupdates == nfixuzero/3 ) alpha_smear = 0.7d0
             if ( nupdates < nfixuzero ) then
                u0_bar = uzero
                u0fl_bar = uzerofl
             else if ( nupdates == nfixuzero ) then
                u0_bar = sum(plaquette(nupdates-9:nupdates)**0.25d0)/10.0d0
                u0fl_bar = sum(plaq_fl(nupdates-9:nupdates)**0.25d0)/10.0d0
             end if
          end if
          !if ( iaccept == 2 ) nupdates = nupdates - 1
       else if ( iaccept <= 0 ) then
          Upr_xd(1:nxs,1:nys,1:nzs,1:nts,:) = U_xd(1:nxs,1:nys,1:nzs,1:nts,:)
          call ShadowGaugeField(Upr_xd,2)
       end if
       !if (i_am_root) print '(8F12.5,A8)', delta_KE, delta_g, delta_2pf, delta_P, delta_H, F_g, F_pf, F_poly, yesno
       if ( (iaccept >= -2) .and. (iaccept <= 1) ) trajectories = trajectories + 1
       if ( (iaccept == 1) .or. (iaccept == -1) ) updates = updates + 1
       if (iaccept >= 0) then
          ntraj = ntraj + 1
          dH(ntraj) = delta_H
          dHbar = sum(dH(max(1,ntraj-nwindow+1):ntraj))/min(ntraj,nwindow)
          dH(ntraj) = max(dH(ntraj),-20.0d0)
          expdHbar = sum(exp(-dH(max(1,ntraj-nwindow+1):ntraj)))/min(ntraj,nwindow)
       end if
       iterations = iterations + 1
       if (i_am_root) then
          open(file=hmc_report_name,unit=hmc_report_file,form="formatted",action="write",status="unknown",position="append")
          open(file=traj_report_name,unit=traj_report_file,form="formatted",action="write",status="unknown",position="append")
          open(file=cg_report_name,unit=cg_report_file,form="formatted",action="write",status="unknown",position="append")
          open(file=force_report_name,unit=force_report_file,form="formatted",action="write",status="unknown",position="append")
          open(file=F_x_report_name,unit=F_x_report_file,form="formatted",action="write",status="unknown",position="append")
          write(*,'(2I6,3F10.7,F10.5,F12.5,A8,2F12.7)') ntraj,nupdates,uzero,u0_bar,u0fl_bar, Q, delta_H, yesno,dHbar, expdHbar
          write(hmc_report_file,'(2I6,3F10.7,F10.5,F12.5,A8,2F12.7)') ntraj,nupdates,uzero,u0_bar,u0fl_bar, Q,delta_H, yesno, dHbar, expdHbar
          write(traj_report_file,'(2I6,6F12.4,F12.7,4F10.7,2E10.3)') ntraj,nupdates, &
               & delta_KE,delta_g,delta_2pf,delta_1pf,delta_P2f,delta_P1f,delta_H, ev_min, ev_max, traj_minev, traj_maxev, sqrt_error, poly_error
          write(cg_report_file,'(2I6,13I7)') ntraj,nupdates, minval(cgiter), maxval(cgiter), sum(cgiter)/n_md, &
               & minval(mcgiter), maxval(mcgiter), sum(mcgiter)/n_md, &
               & minval(eviter), maxval(eviter), sum(eviter)/n_md, &
               & min_z, max_z, pole_min, pole_max
          write(force_report_file,'(6F10.4,6F10.2)') dt*F_g, dt_p2f*F_p2f, dt_p1f*F_p1f, dt_q2f*F_q2f, dt_2pf*F_2pf, dt_1pf*F_1pf, F_g, F_p2f, F_p1f, F_q2f, F_2pf, F_1pf
          write(F_x_report_file,'(18F7.3)') F_xg, F_x2pf, F_x1pf, F_xP2f, F_xP1f, F_xQ2f
          close(hmc_report_file)
          close(traj_report_file)
          close(cg_report_file)
          close(force_report_file)
          close(F_x_report_file)
       end if
       !if ( multijob .and. (nupdates > 0) .and. (modulo(nupdates,n_hmc)==0) ) exit
       !if ( iterations >= 3*n_hmc ) exit
       lastPlaq = m_f
       plaqbarAvg = u0fl_bar
       uzero = u0_bar
       t0 = mpi_wtime()
       if ( .not. benchmarking ) then
          call WriteHMCTrajData(traj_data_name,icfg)
          call WriteGaugeField(hmc_cfg_name,U_xd,icfg,'unknown')
       end if
       t1 = mpi_wtime()
       call TimingUpdate(count_io,t1,t0,mint_io,maxt_io,t_io)
       t_out = mpi_wtime()
       call TimingUpdate(count_hmc,t_out,t_in,mint_hmc,maxt_hmc,t_hmc)
       if ( updates >= n_hmc) exit
       if ( multijob .and. (trajectories >= njob_hmc) ) exit
    end do
    !Output some info about the HMC trajectory
    p_acc = real(nupdates)/ntraj
    if ( ntraj > nfixuzero ) then
       dHbar = sum(dH(1+nfixuzero:ntraj))/(ntraj - nfixuzero)
       expdHbar = sum(exp(-dH(1+nfixuzero:ntraj)))/(ntraj - nfixuzero)
    else
       dHbar = sum(dH(1:ntraj))/ntraj
       expdHbar = sum(exp(-dH(1:ntraj)))/ntraj
    end if
    tau_int = IntegratedAutoCorrelation(nupdates,plaquette(1:nupdates))
    tau_intQ = IntegratedAutoCorrelation(nupdates,topQ(1:nupdates))
    deltau0 = maxval(abs(u0_bar - plaquette(1:nupdates)**0.25d0))
    if (i_am_root) print '(2A6,7A10)', "traj", "acc", "p_acc", "<dH>", "<e^-dH>", "tau_int^P", "tau_int^Q","u0_bar", "delta u0"
    if (i_am_root) print '(2I6,5F10.5,2F10.7)', ntraj, nupdates, p_acc, dHbar, expdHbar, tau_int, tau_intQ, u0_bar, deltau0
  end subroutine HMCTrajectory
  function IntegratedAutoCorrelation(t_mc,curlyO) result (tau_int)
    ! begin args: t_mc, curlyO
    integer :: t_mc
    real(dp), dimension(t_mc) :: curlyO
    ! begin local_vars
    real(dp) :: tau_int
    real(dp) :: C_t, C_0
    integer :: t
    ! begin execution
    C_0 = AutoCorrelation(t_mc,curlyO,0)
    tau_int = 0.5d0
    do t=1,t_mc-1
       C_t = AutoCorrelation(t_mc,curlyO,t)
       tau_int = tau_int + C_t/C_0
    end do
  end function IntegratedAutoCorrelation
  function AutoCorrelation(t_mc,curlyO,t) result (C_t)
    ! begin args: t_mc, curlyO, t
    integer :: t_mc
    real(dp), dimension(t_mc) :: curlyO
    integer :: t
    ! begin local_vars
    real(dp) :: C_t
    real(dp) :: Osq_av, Oav_sq
    ! begin execution
    Osq_av = sum(curlyO(1:t_mc-t)*curlyO(t+1:t_mc))/(t_mc-t)
    Oav_sq = (sum(curlyO(1:t_mc-t))*sum(curlyO(t+1:t_mc)))/(t_mc-t)**2
    C_t = Osq_av - Oav_sq
  end function AutoCorrelation
  subroutine UpdateGaugeField(U_xd, P_xd, dt)
    ! begin args: U_xd, P_xd, dt
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: P_xd
    real(DP), intent(in) :: dt
    ! begin local_vars
    type(colour_matrix) :: V_xd, W_xd, X_xd, I_xd
    type(real_vector) :: lambda_xd
    type(colour_vector) :: theta_xd
    integer :: ix,iy,iz,it,mu
    real(dp) :: nonU
    real(dp) :: t_in, t_out
    ! begin execution
    t_in = mpi_wtime()
    !P_xd is a Hermitian field that is the sum of SU(3) generator matrices.
    !To update U_xd, we must multiply by an SU(3) matrix to stay within SU(3).
    !We exponentiate I*dt*P_xd/2 by diagonalising, exponentiating its eigenvalues,
    !lambda, and then undiagonalising the diagonal matrix D[lambda].
    !This exponential is then an SU(3) matrix.
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          call DiagonaliseMat(P_xd(ix,iy,iz,it,mu), lambda_xd, V_xd)
          theta_xd%Cl = exp((I*dt)*lambda_xd%Cl)
          !Undiagonalise back to P_xd basis, by setting W_xd = V_xd*D[theta_xd]*V_xd^dag,
          !where V_xd is the matrix of eigenvectors.
          !Note: [M*D]_ij = M_ij*theta_j
          call MultiplyMatDiagMat(W_xd,V_xd,theta_xd)
          call MultiplyMatMatDag(X_xd,W_xd,V_xd)
          V_xd = U_xd(ix,iy,iz,it,mu)
          call MultiplyMatMat(U_xd(ix,iy,iz,it,mu),X_xd,V_xd)
       end do; end do; end do; end do
    end do
    call ShadowGaugeField(U_xd,1)
    call ShadowGaugeField(U_xd,2)
    UpdateMatrixFields = .true.
    t_out = mpi_wtime()
    call TimingUpdate(count_gf,t_out,t_in,mint_gf,maxt_gf,t_gf)
  end subroutine UpdateGaugeField
  subroutine UpdateMomenta(U_xd, P_xd, dSbydU, dt, iF_x, F_sum)
    ! begin args: U_xd, P_xd, dSbydU, dt, iF_x, F_sum
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd, P_xd, dSbydU
    real(DP), intent(in) :: dt
    real(dp) :: iF_x(1:3), F_sum
    ! begin local_vars
    type(colour_matrix) :: V_xd,Pdot_xd
    integer :: ix,iy,iz,it,mu
    integer :: ic, jc
    real(dp) :: TrPdot
    real(dp) :: F_pp, F_barpp, F_minpp, F_maxpp
    real(dp) :: F_bar, F_min, F_max
    real(dp), dimension(:,:,:,:,:), allocatable :: F_x
    ! begin execution
    allocate(F_x(nx,ny,nz,nt,nd))
    F_pp = 0.0d0
    F_sum = 0.0d0
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          !P_xd is a Hermitian field that is the sum of SU(3) generator matrices (i.e. traceless).
          !To update P_xd, we must add an Hermitian, traceless matrix, Pdot_xd.
          !Set Pdot_xd = i * U *(dSbydU - U^dag * dSbydU^dag U^dag)
          ! = i *(U * dSbydU - h.c. )
          !F_pp = F_pp + matrix_normsq(dSbydU(ix,iy,iz,it,mu))
          call MultiplyMatMat(V_xd,U_xd(ix,iy,iz,it,mu),dSbydU(ix,iy,iz,it,mu))
          do jc=1,nc; do ic=1,nc
             Pdot_xd%Cl(ic,jc) = I*( V_xd%Cl(ic,jc) - conjg(V_xd%Cl(jc,ic)) )
          end do; end do
          TrPdot = real_trace(Pdot_xd)
          Pdot_xd%Cl(1,1) = Pdot_xd%Cl(1,1) - TrPdot/3.0d0
          Pdot_xd%Cl(2,2) = Pdot_xd%Cl(2,2) - TrPdot/3.0d0
          Pdot_xd%Cl(3,3) = Pdot_xd%Cl(3,3) - TrPdot/3.0d0
          P_xd(ix,iy,iz,it,mu)%Cl = P_xd(ix,iy,iz,it,mu)%Cl - dt*Pdot_xd%Cl
          F_x(ix,iy,iz,it,mu) = lie_norm(Pdot_xd)
          F_pp = F_pp + sum(abs(Pdot_xd%Cl)**2)
       end do; end do; end do; end do
    end do
    call AllSum(F_pp,F_sum)
    F_sum = sqrt(F_sum)/(nlattice*nd)
    F_minpp = minval(F_x)
    F_maxpp = maxval(F_x)
    F_barpp = sum(F_x)
    call AllSum(F_barpp,F_bar)
    call AllMin(F_minpp,F_min)
    call AllMax(F_maxpp,F_max)
    F_bar = F_bar/(nlattice*nd)
    iF_x(1) = F_min
    iF_x(2) = F_bar
    iF_x(3) = F_max
    deallocate(F_x)
  end subroutine UpdateMomenta
  subroutine UpdateGaugeMomenta(U_xd, P_xd, dt)
    ! begin args: U_xd, P_xd, dt
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: P_xd
    real(DP), intent(in) :: dt
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dS_gbydU
    integer :: t0, t1
    real(dp) :: t_in, t_out
    real(dp) :: iF_x(1:3), F_sum
    ! begin execution
    allocate(dS_gbydU(nxp,nyp,nzp,ntp,nd))
    t_in = mpi_wtime()
    call GetdS_gbydU(dS_gbydU,U_xd,beta_md)
    call UpdateMomenta(U_xd,P_xd,dS_gbydU,dt,iF_x,F_sum)
    call TimingUpdate(nF_g,F_sum,0.0d0,F_gmin,F_gmax,F_g)
    call UpdateStatistic(nF_xg,iF_x,F_xg)
    t_out = mpi_wtime()
    call TimingUpdate(count_gm,t_out,t_in,mint_gm,maxt_gm,t_gm)
    deallocate(dS_gbydU)
  end subroutine UpdateGaugeMomenta
  subroutine UpdateFermionicMomenta_2f(U_xd,P_xd,phi2f,dt)
    ! begin args: U_xd, P_xd, phi2f, dt
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: P_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phi2f
    real(DP), intent(in) :: dt
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dS_pfbydU
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: eta2f, P_eta2f
    type(colour_matrix) :: V_xd,Pdot_xd
    integer :: ix,iy,iz,it,mu
    integer :: ic, jc
    real(dp) :: TrPdot
    real(dp) :: t0, t1
    real(dp) :: t_in, t_out
    real(dp) :: F_pp, F_sum, F_barpp, F_minpp, F_maxpp
    real(dp) :: F_bar, F_min, F_max, iF_x(1:3)
    real(dp), dimension(:,:,:,:,:), allocatable :: F_x
    real(dp) :: err, err_pp
    ! begin execution
    allocate(eta2f(nxp,nyp,nzp,ntp,ns))
    allocate(dS_pfbydU(nxp,nyp,nzp,ntp,nd))
    allocate(P_eta2f(nxp,nyp,nzp,ntp,ns))
    allocate(F_x(nx,ny,nz,nt,nd))
    t_in = mpi_wtime()
    dS_pfbydU = zero_matrix
    t0 = mpi_wtime()
    call InitialiseTwoFlavourMatrix(U_xd,UpdateMatrixFields)
    t1 = mpi_wtime()
    call TimingUpdate(count_i2fm,t1,t0,mint_i2fm,maxt_i2fm,t_i2fm)
    t0 = mpi_wtime()
    t1 = mpi_wtime()
    call TimingUpdate(count_2pfm,t1,t0,mint_2pfm,maxt_2pfm,t_2pfm)
    t0 = mpi_wtime()
    if ( poly_filter ) then
       call GetdS_twopfbydU_poly(dS_pfbydU,U_xd,phi2f,c_poly2f,z_poly2f)
    else
       call GetdS_twopfbydU(dS_pfbydU,U_xd,phi2f)
    end if
    t1 = mpi_wtime()
    call TimingUpdate(count_s2pf,t1,t0,mint_s2pf,maxt_s2pf,t_s2pf)
    call UpdateMomenta(U_xd,P_xd,dS_pfbydU,dt,iF_x,F_sum)
    call TimingUpdate(nF_2pf,F_sum,0.0d0,F_2pfmin,F_2pfmax,F_2pf)
    call UpdateStatistic(nF_x2pf,iF_x,F_x2pf)
    t_out = mpi_wtime()
    call TimingUpdate(count_2fm,t_out,t_in,mint_2fm,maxt_2fm,t_2fm)
    deallocate(eta2f)
    deallocate(dS_pfbydU)
    deallocate(P_eta2f)
    deallocate(F_x)
  end subroutine UpdateFermionicMomenta_2f
  subroutine UpdateFermionicMomenta_1f(U_xd,P_xd,phi1f,dt)
    ! begin args: U_xd, P_xd, phi1f, dt
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: P_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phi1f
    real(DP), intent(in) :: dt
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dS_pfbydU
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: eta1f, chi1f
    type(colour_matrix) :: V_xd,Pdot_xd
    integer :: ix,iy,iz,it,mu
    integer :: ic, jc
    real(dp) :: TrPdot
    real(dp) :: t0, t1
    real(dp) :: t_in, t_out
    real(dp) :: F_pp, F_sum, F_barpp, F_minpp, F_maxpp
    real(dp) :: F_bar, F_min, F_max, iF_x(1:3)
    real(dp), dimension(:,:,:,:,:), allocatable :: F_x
    ! begin execution
    allocate(eta1f(nxp,nyp,nzp,ntp,ns))
    allocate(dS_pfbydU(nxp,nyp,nzp,ntp,nd))
    allocate(chi1f(nxp,nyp,nzp,ntp,ns))
    allocate(F_x(nx,ny,nz,nt,nd))
    t_in = mpi_wtime()
    dS_pfbydU = zero_matrix
    t0 = mpi_wtime()
    call InitialiseOneFlavourMatrix(U_xd,UpdateMatrixFields)
    t1 = mpi_wtime()
    call TimingUpdate(count_i1fm,t1,t0,mint_i1fm,maxt_i1fm,t_i1fm)
    t0 = mpi_wtime()
    t1 = mpi_wtime()
    call TimingUpdate(count_1pfm,t1,t0,mint_1pfm,maxt_1pfm,t_1pfm)
    t0 = mpi_wtime()
    if ( poly_filter ) then
       call GetdS_onepfbydU_poly(dS_pfbydU,U_xd,phi1f,c_poly1f,z_poly1f,d_n1f,a_k1f,c_k1f)
    else
       call GetdS_onepfbydU(dS_pfbydU,U_xd,phi1f,d_n1f,a_k1f,c_k1f)
    end if
    t1 = mpi_wtime()
    call TimingUpdate(count_s1pf,t1,t0,mint_s1pf,maxt_s1pf,t_s1pf)
    call UpdateMomenta(U_xd,P_xd,dS_pfbydU,dt,iF_x,F_sum)
    call TimingUpdate(nF_1pf,F_sum,0.0d0,F_1pfmin,F_1pfmax,F_1pf)
    call UpdateStatistic(nF_x1pf,iF_x,F_x1pf)
    t_out = mpi_wtime()
    call TimingUpdate(count_1fm,t_out,t_in,mint_1fm,maxt_1fm,t_1fm)
    deallocate(eta1f)
    deallocate(dS_pfbydU)
    deallocate(chi1f)
    deallocate(F_x)
  end subroutine UpdateFermionicMomenta_1f
  subroutine UpdatePolynomialMomenta_2f(U_xd,P_xd,phiP2f,c_n,z_k,dt)
    ! begin args: U_xd, P_xd, phiP2f, c_n, z_k, dt
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: P_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phiP2f
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    real(DP), intent(in) :: dt
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dS_polybydU
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: P_phi
    type(colour_matrix) :: V_xd,Pdot_xd
    integer :: ix,iy,iz,it,mu
    integer :: ic, jc
    real(dp) :: TrPdot
    real(dp) :: t0, t1
    real(dp) :: t_in, t_out
    real(dp) :: F_pp, F_sum, F_barpp, F_minpp, F_maxpp
    real(dp) :: F_bar, F_min, F_max, iF_x(1:3)
    real(dp), dimension(:,:,:,:,:), allocatable :: F_x
    integer :: n
    ! begin execution
    allocate(dS_polybydU(nxp,nyp,nzp,ntp,nd))
    allocate(P_phi(nxp,nyp,nzp,ntp,ns))
    allocate(F_x(nx,ny,nz,nt,nd))
    t_in = mpi_wtime()
    dS_polybydU = zero_matrix
    m_f = m_ud
    n = size(z_k)
    t0 = mpi_wtime()
    call InitialiseTwoFlavourMatrix(U_xd,UpdateMatrixFields)
    t1 = mpi_wtime()
    call TimingUpdate(count_i2fm,t1,t0,mint_i2fm,maxt_i2fm,t_i2fm)
    t0 = mpi_wtime()
    call PolyMultiply(phiP2f,P_phi,sqrt(c_n),z_k(1:n/2))
    t1 = mpi_wtime()
    if ( n == n_p2f ) then
       call TimingUpdate(count_P2fm,t1,t0,mint_P2fm,maxt_P2fm,t_P2fm)
    else
       call TimingUpdate(count_Q2fm,t1,t0,mint_Q2fm,maxt_Q2fm,t_Q2fm)
    end if
    t0 = mpi_wtime()
    call GetdS_polybydU_2f(dS_polybydU,U_xd,phiP2f,P_phi,sqrt(c_n),z_k(1:n/2))
    t1 = mpi_wtime()
    if ( n == n_p2f ) then
       call TimingUpdate(count_sP2f,t1,t0,mint_sP2f,maxt_sP2f,t_sP2f)
    else
       call TimingUpdate(count_sQ2f,t1,t0,mint_sQ2f,maxt_sQ2f,t_sQ2f)
    end if
    call UpdateMomenta(U_xd,P_xd,dS_polybydU,dt,iF_x,F_sum)
    t_out = mpi_wtime()
    if ( n == n_p2f ) then
       call TimingUpdate(nF_p2f,F_sum,0.0d0,F_p2fmin,F_p2fmax,F_p2f)
       call UpdateStatistic(nF_xP2f,iF_x,F_xP2f)
       call TimingUpdate(count_pm2f,t_out,t_in,mint_pm2f,maxt_pm2f,t_pm2f)
    else
       call TimingUpdate(nF_q2f,F_sum,0.0d0,F_q2fmin,F_q2fmax,F_q2f)
       call UpdateStatistic(nF_xQ2f,iF_x,F_xQ2f)
       call TimingUpdate(count_qm2f,t_out,t_in,mint_qm2f,maxt_qm2f,t_qm2f)
    end if
    deallocate(dS_polybydU)
    deallocate(P_phi)
    deallocate(F_x)
  end subroutine UpdatePolynomialMomenta_2f
  subroutine UpdatePolynomialMomenta_1f(U_xd,P_xd,phiP1f,c_n,z_k,dt)
    ! begin args: U_xd, P_xd, phiP1f, c_n, z_k, dt
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: P_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phiP1f
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    real(DP), intent(in) :: dt
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dS_polybydU
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: P_phi
    type(colour_matrix) :: V_xd,Pdot_xd
    integer :: ix,iy,iz,it,mu
    integer :: ic, jc
    real(dp) :: TrPdot
    real(dp) :: t0, t1
    real(dp) :: t_in, t_out
    real(dp) :: F_pp, F_sum, F_barpp, F_minpp, F_maxpp
    real(dp) :: F_bar, F_min, F_max, iF_x(1:3)
    real(dp), dimension(:,:,:,:,:), allocatable :: F_x
    integer :: n
    ! begin execution
    allocate(dS_polybydU(nxp,nyp,nzp,ntp,nd))
    allocate(P_phi(nxp,nyp,nzp,ntp,ns))
    allocate(F_x(nx,ny,nz,nt,nd))
    t_in = mpi_wtime()
    dS_polybydU = zero_matrix
    m_f = m_s
    n = size(z_k)
    t0 = mpi_wtime()
    call InitialiseOneFlavourMatrix(U_xd,UpdateMatrixFields)
    t1 = mpi_wtime()
    call TimingUpdate(count_i1fm,t1,t0,mint_i1fm,maxt_i1fm,t_i1fm)
    t0 = mpi_wtime()
    call PolyMultiply(phiP1f,P_phi,sqrt(c_n),z_k(1:n/2))
    t1 = mpi_wtime()
    call TimingUpdate(count_P1fm,t1,t0,mint_P1fm,maxt_P1fm,t_P1fm)
    t0 = mpi_wtime()
    call GetdS_polybydU_1f(dS_polybydU,U_xd,phiP1f,P_phi,sqrt(c_n),z_k(1:n/2))
    t1 = mpi_wtime()
    call TimingUpdate(count_sP1f,t1,t0,mint_sP1f,maxt_sP1f,t_sP1f)
    call UpdateMomenta(U_xd,P_xd,dS_polybydU,dt,iF_x,F_sum)
    t_out = mpi_wtime()
    if ( n == n_p1f ) then
       call TimingUpdate(nF_p1f,F_sum,0.0d0,F_p1fmin,F_p1fmax,F_p1f)
       call UpdateStatistic(nF_xP1f,iF_x,F_xP1f)
       call TimingUpdate(count_pm1f,t_out,t_in,mint_pm1f,maxt_pm1f,t_pm1f)
    else
       call TimingUpdate(nF_q1f,F_sum,0.0d0,F_q1fmin,F_q1fmax,F_q1f)
       call UpdateStatistic(nF_xQ1f,iF_x,F_xQ1f)
       call TimingUpdate(count_qm1f,t_out,t_in,mint_qm1f,maxt_qm1f,t_qm1f)
    end if
    !call TimingUpdate(nF_p1f,F_sum,0.0d0,F_p1fmin,F_p1fmax,F_p1f)
    !call UpdateStatistic(nF_xP1f,iF_x,F_xP1f)
    !call TimingUpdate(count_pm1f,t_out,t_in,mint_pm1f,maxt_pm1f,t_pm1f)
    deallocate(dS_polybydU)
    deallocate(P_phi)
    deallocate(F_x)
  end subroutine UpdatePolynomialMomenta_1f
  subroutine UpdatePseudofermionField_2f(U_xd,phi2f,eta2f,refresh)
    ! begin args: U_xd, phi2f, eta2f, refresh
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phi2f,eta2f
    logical :: refresh
    ! begin execution
    call InitialiseTwoFlavourMatrix(U_xd,UpdateMatrixFields)
    if ( refresh ) then
       if ( poly_filter ) then
          call GetPseudofermionField_twopf_poly(phi2f,c_poly2f,z_poly2f)
       else
          call GetPseudofermionField_twopf(phi2f)
       end if
    end if
    if ( poly_filter ) then
       call MultiplyM_twopf_poly(phi2f,eta2f,c_poly2f,z_poly2f)
    else
       call MultiplyM_twopf(phi2f,eta2f)
    end if
  end subroutine UpdatePseudofermionField_2f
  subroutine UpdatePseudofermionField_1f(U_xd,phi1f,eta1f,refresh)
    ! begin args: U_xd, phi1f, eta1f, refresh
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phi1f,eta1f
    logical :: refresh
    integer :: n
    ! begin execution
    call InitialiseOneFlavourMatrix(U_xd,UpdateMatrixFields)
    if ( refresh ) then
       call Init1PFRationalPoly(n_z1f,1.0d-14)
       n = max(n_z1f + n_poly1f + 1,n_z1f + n_poly2f + 1)
       if ( n_aux /= n ) then
          if ( allocated(chi_i) ) deallocate(chi_i)
          allocate(chi_i(nxp,nyp,nzp,ntp,ns,n))
          n_aux = n
       end if
       if ( poly_filter ) then
          call GetPseudofermionField_onepf_poly(phi1f,d_n1f,a_k1f,c_k1f,c_poly1f,z_poly1f)
       else
          call GetPseudofermionField_onepf(phi1f,d_n1f,a_k1f,c_k1f)
       end if
    end if
    if ( poly_filter ) then
       call MultiplyM_onepf_poly(phi1f,eta1f,d_n1f,a_k1f,c_k1f,c_poly2f,z_poly2f)
    else
       call MultiplyM_onepf(phi1f,eta1f,d_n1f,a_k1f,c_k1f)
    end if
  end subroutine UpdatePseudofermionField_1f
  subroutine UpdatePolynomialField_2f(U_xd,phiP2f,etaP2f,c_p2f,z_p2f,refresh)
    ! begin args: U_xd, phiP2f, etaP2f, c_p2f, z_p2f, refresh
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phiP2f,etaP2f
    real(dp) :: c_p2f
    complex(dc), dimension(:) :: z_p2f
    logical :: refresh
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: xi, Mxi
    integer :: ix,iy,iz,it,is,i_v
    real(dp) :: tolerance = 1.0d-12, norm_phi
    integer :: iter_cg
    ! begin execution
    allocate(xi(nxp,nyp,nzp,ntp,ns))
    allocate(Mxi(nxp,nyp,nzp,ntp,ns))
    call InitialiseTwoFlavourMatrix(U_xd,UpdateMatrixFields)
    if ( refresh ) then
       call GetPolynomialField_2f(phiP2f,c_p2f,z_p2f)
    end if
    call MultiplyM_poly2f(phiP2f,etaP2f,c_p2f,z_p2f)
    deallocate(xi)
    deallocate(Mxi)
  end subroutine UpdatePolynomialField_2f
  subroutine UpdatePolynomialField_1f(U_xd,phiP1f,etaP1f,c_p1f,z_p1f,refresh)
    ! begin args: U_xd, phiP1f, etaP1f, c_p1f, z_p1f, refresh
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phiP1f,etaP1f
    real(dp) :: c_p1f
    complex(dc), dimension(:) :: z_p1f
    logical :: refresh
    ! begin execution
    call InitialiseOneFlavourMatrix(U_xd,UpdateMatrixFields)
    if ( refresh ) then
       call GetPolynomialField_1f(phiP1f,c_p1f,z_p1f)
    end if
    call MultiplyM_poly1f(phiP1f,etaP1f,c_p1f,z_p1f)
  end subroutine UpdatePolynomialField_1f
  function GetKE(P_xd) result (KE)
    ! begin args: P_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: P_xd
    ! begin local_vars
    real(dp) :: KE
    real(dp) :: TrPsq, TrPsqpp
    integer :: ix,iy,iz,it,mu
    !Calculate the Kinetic energy (in 5D) of the conjugate momenta.
    ! begin execution
    TrPsqpp = 0.0d0
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          call RealTraceMultMatMat(TrPsq,P_xd(ix,iy,iz,it,mu),P_xd(ix,iy,iz,it,mu))
          TrPsqpp = TrPsqpp + TrPsq
       end do; end do; end do; end do
    end do
    call AllSum(TrPsqpp,TrPsq)
    KE = 0.5d0*TrPsq
  end function GetKE
  subroutine GL3GaussianField(P_xd)
    ! begin args: P_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: P_xd
    ! begin local_vars
    real(dp) :: alpha, theta !random number fields
    real(dp), dimension(n_a) :: omega_a
    integer :: ix,iy,iz,it,mu,i_a
    ! begin execution
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
          !Returns real random numbers according to P(tau) = exp(-tau**2)/sqrt(pi)
          do i_a=1,n_a
             call random_number(alpha)
             call random_number(theta)
             alpha = sqrt(-log(alpha))
             theta=two_pi*theta
             omega_a(i_a) = alpha*cos(theta)
          end do
          !Generate an Hermitian colour matrix field based on the Gaussian momenta omega_a
          !Calculate the exponent, sum_a omega_a*lambda_a
          !where lambda_a are the generators of SU(3), the Gellmann matrices
          P_xd(ix,iy,iz,it,mu)%Cl(1,1) = omega_a(3) + omega_a(8)/sqrt(3.0d0)
          P_xd(ix,iy,iz,it,mu)%Cl(1,2) = cmplx(omega_a(1), -omega_a(2),dc)
          P_xd(ix,iy,iz,it,mu)%Cl(1,3) = cmplx(omega_a(4), -omega_a(5),dc)
          P_xd(ix,iy,iz,it,mu)%Cl(2,1) = cmplx(omega_a(1), omega_a(2),dc)
          P_xd(ix,iy,iz,it,mu)%Cl(2,2) = -omega_a(3) + omega_a(8)/sqrt(3.0d0)
          P_xd(ix,iy,iz,it,mu)%Cl(2,3) = cmplx(omega_a(6), -omega_a(7),dc)
          P_xd(ix,iy,iz,it,mu)%Cl(3,1) = cmplx(omega_a(4), omega_a(5),dc)
          P_xd(ix,iy,iz,it,mu)%Cl(3,2) = cmplx(omega_a(6), omega_a(7),dc)
          P_xd(ix,iy,iz,it,mu)%Cl(3,3) = -2.0d0*omega_a(8)/sqrt(3.0d0)
       end do; end do; end do; end do
    end do
  end subroutine GL3GaussianField
  subroutine ReadHMCTrajData(traj_data_name,icfg)
    ! begin args: traj_data_name, icfg
    character(len=*) :: traj_data_name
    integer :: icfg
    ! begin execution
    if ( i_am_root ) then
       open(file=traj_data_name,unit=traj_data_file,status='unknown',action='read',form='unformatted')
       read(traj_data_file) thermalising
       read(traj_data_file) ntraj, nupdates, updates, icfg
       read(traj_data_file) plaquette(1:nupdates)
       read(traj_data_file) topQ(1:nupdates)
       read(traj_data_file) plaq_fl(1:nupdates)
       read(traj_data_file) dH(1:ntraj)
       close(traj_data_file)
    end if
    call Broadcast(thermalising)
    call Broadcast(ntraj)
    call Broadcast(nupdates)
    call Broadcast(updates)
    call Broadcast(icfg)
    call Broadcast(plaquette(1:nupdates))
    call Broadcast(topQ(1:nupdates))
    call Broadcast(plaq_fl(1:nupdates))
    call Broadcast(dH(1:ntraj))
  end subroutine ReadHMCTrajData
  subroutine WriteHMCTrajData(traj_data_name,icfg)
    ! begin args: traj_data_name, icfg
    character(len=*) :: traj_data_name
    integer :: icfg
    ! begin execution
    if ( i_am_root ) then
       open(file=traj_data_name,unit=traj_data_file,status='unknown',action='write',form='unformatted')
       write(traj_data_file) thermalising
       write(traj_data_file) ntraj, nupdates, updates, icfg
       write(traj_data_file) plaquette(1:nupdates)
       write(traj_data_file) topQ(1:nupdates)
       write(traj_data_file) plaq_fl(1:nupdates)
       write(traj_data_file) dH(1:ntraj)
       close(traj_data_file)
    end if
  end subroutine WriteHMCTrajData
  subroutine InitialiseHMC
    ! begin local_vars
    integer :: ierror
    ! begin execution
    if (i_am_root) print *, " Initialising "
    !TODO: Clean up memory usage here.
    call CreateGaugeField(P_xd,1)
    call CreateGaugeField(Ppr_xd,1)
    call CreateGaugeField(Upr_xd,2)
    call CreateFermionField(phi2f,1)
    call CreateFermionField(eta2f,1)
    call CreateFermionField(eta2f_pr,1)
    call CreateFermionField(phi1f,1)
    call CreateFermionField(eta1f,1)
    call CreateFermionField(eta1f_pr,1)
    call CreateFermionField(phiP2f,1)
    call CreateFermionField(etaP2f,1)
    call CreateFermionField(etaP2f_pr,1)
    call CreateFermionField(phiQ2f,1)
    call CreateFermionField(etaQ2f,1)
    call CreateFermionField(etaQ2f_pr,1)
    call CreateFermionField(phiP1f,1)
    call CreateFermionField(etaP1f,1)
    call CreateFermionField(etaP1f_pr,1)
    call CreateFermionField(phiQ1f,1)
    call CreateFermionField(etaQ1f,1)
    call CreateFermionField(etaQ1f_pr,1)
    if (quenched) then
       m_ud = 0.0d0
       m_s = 0.0d0
    else
       m_ud = 4.0d0 - 0.5d0/kappa_ud
       m_s = 4.0d0 - 0.5d0/kappa_s
    end if
    if ( dt <= 0.0d0 ) then
       dt = 1.0d0/n_md
    end if
    dt_2pf = (dt*n_md)/n_md2pf
    dt_1pf = (dt*n_md)/n_md1pf
    dt_p2f = (dt*n_md)/n_mdp2f
    dt_p1f = (dt*n_md)/n_mdp1f
    dt_q2f = (dt*n_md)/n_mdq2f
    if ( (modulo(n_md,n_md2pf) /= 0) .or. (modulo(n_md,n_md1pf) /= 0) .or. (modulo(n_md,n_mdp2f) /= 0) .or. (modulo(n_md,n_mdp1f) /= 0) .or. (modulo(n_md,n_mdq2f) /= 0) ) then
       if (i_am_root) print *, "Invalid n_md choice: n_md must be a multiple of (n_md2pf,n_md1pf,n_mdp2f,n_mdp1f) "
       stop
    end if
    ! Test for validity of replay trick
    !if ( (modulo(n_md,2*n_md2pf) /= 0) .or. (modulo(n_md,2*n_md1pf) /= 0) ) then
    ! if (i_am_root) print *, "Invalid n_md choice: n_md must be a multiple of (2*n_md2pf,2*n_md1pf)"
    ! stop
    !end if
    if ( eps_poly2f < 0.0d0 ) then
       eps_poly2f = (1.0d0 - 4.0d0*kappa_ud)/(1.0d0 + 8.0d0*kappa_ud)
    end if
    !eps_zolo = 1.0d0 - 4*kappa_s
    if (i_am_root) print *, " Initialising filters"
    if ( poly_filter ) then
       n_poly2f = n_p2f + n_q2f
       if ( modulo(n_poly2f+1,n_p2f+1) /= 0 ) then
          if (i_am_root) print *, "Invalid n_q2f choice: (n_q2f+n_p2f+1) must be a multiple of (n_p2f+1)"
          stop
       end if
       allocate(z_poly2f(n_poly2f),zz_poly2f(n_poly2f+1))
       allocate(z_p2f(n_p2f))
       if ( n_q2f > 0 ) allocate(z_q2f(n_q2f))
       call Init2PFPolyFilter(n_poly2f,n_p2f,n_q2f,mu_p,nu_p,kappa_ud)
       if ( three_flavour ) then
          if (i_am_root) print *, " Three flavour poly filter"
          n_poly1f = n_p1f + n_q1f
          allocate(z_poly1f(n_poly1f),z_p1f(n_p1f))
          if ( n_q1f > 0 ) allocate(z_q1f(n_q1f))
          call Init1PFPolyFilter(n_poly1f,n_p1f,n_q1f,mu_p,nu_p,kappa_s)
          n_aux = max(n_poly2f+1,n_poly1f+1)
       else
          if (i_am_root) print *, " Two flavour poly filter", n_poly2f, n_p2f, n_q2f
          !allocate(eta_i(nxp,nyp,nzp,ntp,ns,n_poly2f+1))
          n_aux = n_poly2f+1
       end if
       allocate(chi_i(nxp,nyp,nzp,ntp,ns,n_aux))
    end if
    if ( three_flavour ) then
       if (i_am_root) print *, " Three flavour" !, eps_zolo, 1.0d0+64*kappa_s**2
       allocate(v_min(nxp,nyp,nzp,ntp,ns,1))
       allocate(v_max(nxp,nyp,nzp,ntp,ns,1))
       allocate(Dv_i(nxp,nyp,nzp,ntp,ns,1))
       !call Init1PFRationalPoly(n_poles,kappa_s,eps_zolo,1.0d0+64*kappa_s**2,1.0d-12)
       !allocate(chi_i(nxp,nyp,nzp,ntp,ns,n_poles))
       !if (i_am_root) print *, n_poles
    end if
    !Debugging flags
    VerboseAccCGEV = .false.
    !DebugCGInvert = .true.
  end subroutine InitialiseHMC
  subroutine FinaliseHMC
    ! begin execution
    call DestroyGaugeField(P_xd)
    call DestroyGaugeField(Ppr_xd)
    call DestroyGaugeField(Upr_xd)
    call DestroyFermionField(phi2f)
    call DestroyFermionField(eta2f)
    call DestroyFermionField(eta2f_pr)
    call DestroyFermionField(phi1f)
    call DestroyFermionField(eta1f)
    call DestroyFermionField(eta1f_pr)
    call DestroyFermionField(phiP2f)
    call DestroyFermionField(etaP2f)
    call DestroyFermionField(etaP2f_pr)
    call DestroyFermionField(phiQ2f)
    call DestroyFermionField(etaQ2f)
    call DestroyFermionField(etaQ2f_pr)
    call DestroyFermionField(phiP1f)
    call DestroyFermionField(etaP1f)
    call DestroyFermionField(etaP1f_pr)
    call DestroyFermionField(phiQ1f)
    call DestroyFermionField(etaQ1f)
    call DestroyFermionField(etaQ1f_pr)
    if ( poly_filter ) then
       deallocate(z_poly2f,z_p2f)
       if ( n_q2f > 0 ) deallocate(z_q2f)
    end if
    if ( three_flavour .or. poly_filter ) deallocate(chi_i)
    if ( three_flavour ) then
       !deallocate(a_l,b_l,c_l,b_k,c_k)
       deallocate(v_min)
       deallocate(v_max)
       deallocate(Dv_i)
    end if
  end subroutine FinaliseHMC
end module wilsonHybridMonteCarlo
