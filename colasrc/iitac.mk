#F90C = mpif90
#F90C = /usr/support/mvapich/mvapich-0.9.7-ipoib/pathscale-2.2.1/bin/mpif90
#F90C = /usr/support/mvapich/mvapich-0.9.7/pathscale-2.2.1/bin/mpif90
#F90C = /usr/support/mvapich/mvapich-0.9.7-waseem/pathscale-2.2.1/bin/mpif90
F90C = /usr/support/mvapich/mvapich-0.9.8/pathscale-2.2.1/bin/mpif90
#F90C = /usr/support/mvapich/mvapich-0.9.7-waseem/pathscale-2.4/bin/mpif90
#F90C = /usr/voltaire/mpi.pathcc.rsh/bin/mpif90
OPTS = -march=opteron -mcpu=opteron -O2
#OPTS = -O2 -g2
F90FLAGS = -freeform -cpp -D_iitac_ $(OPTS) 

.SUFFIXES:
.SUFFIXES: .f90 .o .f
.f90.o:
	$(F90C) $(F90FLAGS) -o $@ -I$(COMDIR) -module $(@D)/ -c $<
