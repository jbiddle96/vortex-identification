
module Strings

  use Kinds
  implicit none
  private

  interface str
     module procedure logical_to_str
     module procedure integer_to_str
     module procedure integer_1_to_str
     module procedure real_to_str_sp
     module procedure real_to_str_dp
     module procedure complex_to_str_sc
     module procedure complex_to_str_dc
  end interface

  public :: str

contains

  pure function logical_to_str(a,width,fmt) result (s)
    logical, intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function logical_to_str

  pure function integer_to_str(a,width,fmt) result (s)
    integer, intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function integer_to_str

  pure function integer_1_to_str(a,width,fmt) result (s)
    integer, dimension(:), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function integer_1_to_str

  pure function real_to_str_sp(a,width,fmt) result (s)
    real(sp), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function real_to_str_sp

  pure function real_to_str_dp(a,width,fmt) result (s)
    real(dp), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function real_to_str_dp

  pure function complex_to_str_sc(a,width,fmt) result (s)
    complex(sc), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if

  end function complex_to_str_sc

  pure function complex_to_str_dc(a,width,fmt) result (s)
    complex(dc), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function complex_to_str_dc
  
end module Strings
