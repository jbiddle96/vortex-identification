/* eoflic.h */


module eoflicMatrix

  use Timer
  use MPIInterface
  use FermionField
  use FermionFieldMPIComms
  use FermionAlgebra

  use GaussJordan

  use SpinorTypes
  use FermionTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  use ColourFieldOps
  implicit none
  private
  integer, dimension(:,:,:), allocatable :: i_xpmu, i_xmmu
  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:), public, allocatable :: Up_xd
  type(colour_matrix), dimension(:,:,:), public, allocatable :: Um_xd
  type(colour_weyl_spin_matrix), dimension(:,:), allocatable, public :: SigmaFp, SigmaFm, InvSigmaFp, InvSigmaFm
  logical, target, public :: fm_initialised_eoflic = .false.
  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean
  !real(dp), public :: bcx_eoflic = 1.0d0, bcy_eoflic = 1.0d0, bcz_eoflic = 1.0d0, bct_eoflic = 1.0d0
  public :: Deoflic
  public :: Deoflic_dag
  public :: Hsqeoflic
  public :: Heoflic
  public :: InitialiseeoflicOperator
  public :: FinaliseeoflicOperator
  public :: InitialiseFermionMatrix_eoflic
  public :: FinaliseFermionMatrix_eoflic
  public :: phi_x2phi_eo
  public :: phi_eo2phi_x
  public :: EOWilsonDag_Split
  public :: EOWilson_Split
  public :: LeftILUMatrix_eoflic
  public :: RightILUMatrix_eoflic
  public :: LeftILUMatrixDag_eoflic
  public :: RightILUMatrixDag_eoflic
  public :: InvLeftILUMatrix_eoflic
  public :: InvRightILUMatrix_eoflic
  public :: AddEOClover
  public :: EOCloverInv
  public :: DefineSpinField
  public :: CalcCloverTerms
  interface FinaliseeoflicOperator
     module procedure FinaliseFermionMatrix_eoflic
  end interface
  interface InitialiseeoflicOperator
     module procedure InitialiseFermionMatrix_eoflic
  end interface
contains
  subroutine InitialiseFermionMatrix_eoflic(U_xd, Usm_xd , F_munu, kappa, c_sw, u0, u0_sm)
    ! begin args: U_xd, Usm_xd , F_munu, kappa, c_sw, u0, u0_sm
    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd, Usm_xd , F_munu
    real(dp) :: kappa, c_sw
    real(dp), optional :: u0, u0_sm
    ! begin local_vars
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfic_sw, mfir, mfir_sm !coefficents to be absorbed.
    type(colour_matrix) :: U_mux, Usm_mux
    integer :: i_eo, i_xeo, j_xeo
    integer, dimension(nd) :: i_x, j_x
    if ( .not. allocated(i_xpmu) ) allocate(i_xpmu(n_xeo,nd,0:1))
    if ( .not. allocated(i_xmmu) ) allocate(i_xmmu(n_xeo,nd,0:1))
    !EO preconditioning is in the hopping parameter formalism.
    ! begin execution
    if ( .not. allocated(Up_xd) ) call CreateGaugeField(Up_xd,1)
    if ( .not. allocated(Um_xd) ) call CreateGaugeField(Um_xd,1)
    mfir = kappa
    if ( present(u0) ) mfir = mfir/u0
    mfir_sm = kappa
    if ( present(u0_sm) ) mfir_sm = mfir_sm/u0_sm
    !Absorb the mean field improvement into the gauge fields.
    !Additional factor of a half due to split link trick.
    mfir = 0.5d0*mfir
    mfir_sm = 0.5d0*mfir_sm
    !Construct the forward and backward hop indices.
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       i_x = (/ ix,iy,iz,it /)
       i_eo = modulo(sum(i_x)+proc_eo,2)
       i_xeo = x2eo(i_x)
       do mu=1,nd
          j_x = i_x
          j_x(mu) = j_x(mu)+1
          j_xeo = x2eo(j_x)
          i_xpmu(i_xeo,mu,i_eo) = j_xeo
          j_x = i_x
          j_x(mu) = j_x(mu)-1
          j_xeo = x2eo(j_x)
          i_xmmu(i_xeo,mu,i_eo) = j_xeo
       end do
    end do; end do; end do; end do
    !Absorb the mean field improvement into the gauge fields.
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x)+proc_eo,2)
          i_xeo = x2eo(i_x)
          U_mux%Cl = mfir*U_xd(ix,iy,iz,it,mu)%Cl
          Usm_mux%Cl = mfir_sm*Usm_xd(ix,iy,iz,it,mu)%Cl
          Up_xd(i_xeo,mu,i_eo)%Cl = Usm_mux%Cl + U_mux%Cl
          Um_xd(i_xeo,mu,i_eo)%Cl = Usm_mux%Cl - U_mux%Cl
       end do; end do; end do; end do
    end do
    call SetBoundaryConditions(Up_xd,ff_bcx, ff_bcy, ff_bcz, ff_bct)
    call ShadowGaugeField(Up_xd,1)
    call SetBoundaryConditions(Um_xd,ff_bcx, ff_bcy, ff_bcz, ff_bct)
    call ShadowGaugeField(Um_xd,1)
    mfic_sw = kappa*c_sw
    if ( present(u0_sm) ) mfic_sw = mfic_sw/(u0_sm**4)
    if ( .not. allocated(SigmaFp) ) allocate(SigmaFp(n_xeo,0:1))
    if ( .not. allocated(SigmaFm) ) allocate(SigmaFm(n_xeo,0:1))
    if ( .not. allocated(InvSigmaFp) ) allocate(InvSigmaFp(n_xeo,0:1))
    if ( .not. allocated(InvSigmaFm) ) allocate(InvSigmaFm(n_xeo,0:1))
    call CalcCloverTerms(F_munu,mfic_sw)
    fm_initialised_eoflic = .true.
  end subroutine InitialiseFermionMatrix_eoflic
  subroutine FinaliseFermionMatrix_eoflic
    fm_initialised_eoflic = .false.
    if ( allocated(Up_xd) ) call DestroyGaugeField(Up_xd)
    if ( allocated(Um_xd) ) call DestroyGaugeField(Um_xd)
    if ( allocated(SigmaFp) ) deallocate(SigmaFp)
    if ( allocated(SigmaFm) ) deallocate(SigmaFm)
    if ( allocated(InvSigmaFp) ) deallocate(InvSigmaFp)
    if ( allocated(InvSigmaFm) ) deallocate(InvSigmaFm)
  end subroutine FinaliseFermionMatrix_eoflic
  subroutine phi_x2phi_eo(psi,psi_e,psi_o)
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(dirac_fermion), dimension(:) :: psi_e, psi_o
    integer :: ix,iy,iz,it,is,i_eo,i_xeo,i_x(nd)
    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x)+proc_eo,2)
          i_xeo = x2eo(i_x)
          if ( i_eo == 0 ) then
             psi_e(i_xeo)%cs(:,is) = psi(ix,iy,iz,it,is)%cl(:)
          else
             psi_o(i_xeo)%cs(:,is) = psi(ix,iy,iz,it,is)%cl(:)
          end if
       end do; end do; end do; end do
    end do
  end subroutine phi_x2phi_eo
  subroutine phi_eo2phi_x(psi_e,psi_o,psi)
    type(dirac_fermion), dimension(:) :: psi_e, psi_o
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    integer :: ix,iy,iz,it,is,i_eo,i_xeo,i_x(nd)
    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x)+proc_eo,2)
          i_xeo = x2eo(i_x)
          if ( i_eo == 0 ) then
             psi(ix,iy,iz,it,is)%cl(:) = psi_e(i_xeo)%cs(:,is)
          else
             psi(ix,iy,iz,it,is)%cl(:) = psi_o(i_xeo)%cs(:,is)
          end if
       end do; end do; end do; end do
    end do
  end subroutine phi_eo2phi_x
  subroutine LeftILUMatrix_eoflic(psi_e,psi_o,psi)
    !! L = [ (1+T_ee) 0 ]
    !! [ +D_oe 1 ]
    ! begin args: psi_e, psi_o, psi_pr
    type(dirac_fermion), dimension(:) :: psi_e,psi_o
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dpsi_o,Dpsi_e
    integer :: i_xeo
    ! begin execution
    allocate(Dpsi_e(n_xpeo))
    allocate(Dpsi_o(n_xpeo))
    call EOWilson_Split(psi_e,Dpsi_o,1)
    do i_xeo=1,n_xeo
       Dpsi_o(i_xeo)%cs(:,:) = psi_o(i_xeo)%cs(:,:) + Dpsi_o(i_xeo)%cs(:,:)
    end do
    Dpsi_e = psi_e
    call AddEOClover(Dpsi_e,psi_e,0)
    call phi_eo2phi_x(Dpsi_e,Dpsi_o,psi)
    deallocate(Dpsi_e)
    deallocate(Dpsi_o)
  end subroutine LeftILUMatrix_eoflic
  subroutine RightILUMatrix_eoflic(psi,psi_e,psi_o)
    !! R = [ 1 (1+T_ee)^-1 D_eo ]
    !! [ 0 1 ]
    ! begin args: psi, psi_pr
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(dirac_fermion), dimension(:) :: psi_e,psi_o
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dpsi_o, Dpsi_e
    integer :: i_xeo
    ! begin execution
    allocate(Dpsi_o(n_xpeo))
    allocate(Dpsi_e(n_xpeo))
    call phi_x2phi_eo(psi,psi_e,psi_o)
    call EOWilson_Split(psi_o,Dpsi_o,0)
    Dpsi_e = Dpsi_o
    call EOCloverInv(Dpsi_e,Dpsi_o,0)
    do i_xeo=1,n_xeo
       psi_e(i_xeo)%cs(:,:) = psi_e(i_xeo)%cs(:,:) + Dpsi_o(i_xeo)%cs(:,:)
    end do
    deallocate(Dpsi_o)
    deallocate(Dpsi_e)
  end subroutine RightILUMatrix_eoflic
  subroutine LeftILUMatrixDag_eoflic(psi,psi_e,psi_o)
    !! L^dag = [ (1+T_ee) +D_oe^dag ]
    !! [ 0 1 ]
    ! begin args: psi_e, psi_o, psi_pr
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(dirac_fermion), dimension(:) :: psi_e,psi_o
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dpsi_o
    integer :: i_xeo
    ! begin execution
    allocate(Dpsi_o(n_xpeo))
    call phi_x2phi_eo(psi,psi_e,psi_o)
    Dpsi_o = psi_e
    call AddEOClover(psi_e,Dpsi_o,0)
    call EOWilsonDag_Split(psi_o,Dpsi_o,1)
    do i_xeo=1,n_xeo
       psi_e(i_xeo)%cs(:,:) = psi_e(i_xeo)%cs(:,:) + Dpsi_o(i_xeo)%cs(:,:)
    end do
    deallocate(Dpsi_o)
  end subroutine LeftILUMatrixDag_eoflic
  subroutine RightILUMatrixDag_eoflic(psi_e,psi_o,psi)
    !! R^dag = [ 1 0 ]
    !! [ +D_eo^dag(1+T_ee)^-1 1 ]
    ! begin args: psi_e, psi_o, psi
    type(dirac_fermion), dimension(:) :: psi_e,psi_o
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dpsi_o, Dpsi_e
    integer :: i_xeo
    ! begin execution
    allocate(Dpsi_o(n_xpeo))
    allocate(Dpsi_e(n_xpeo))
    call EOCloverInv(psi_e,Dpsi_e,0)
    call EOWilsonDag_Split(Dpsi_e,Dpsi_o,0)
    do i_xeo=1,n_xeo
       Dpsi_o(i_xeo)%cs(:,:) = psi_o(i_xeo)%cs(:,:) + Dpsi_o(i_xeo)%cs(:,:)
    end do
    call phi_eo2phi_x(psi_e,Dpsi_o,psi)
    deallocate(Dpsi_o)
    deallocate(Dpsi_e)
  end subroutine RightILUMatrixDag_eoflic
  subroutine InvLeftILUMatrix_eoflic(psi,psi_e,psi_o)
    !! L^-1 = [ (1+T_ee)^-1 0 ]
    !! [ -D_oe(1+T_ee)^-1 1 ]
    ! begin args: psi, psi_o, psi_e
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(dirac_fermion), dimension(:) :: psi_e,psi_o
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dpsi_o
    integer :: i_xeo
    ! begin execution
    allocate(Dpsi_o(n_xpeo))
    call phi_x2phi_eo(psi,psi_e,psi_o)
    call EOCloverInv(psi_e,Dpsi_o,0)
    psi_e = Dpsi_o
    call EOWilson_Split(psi_e,Dpsi_o,1)
    do i_xeo=1,n_xeo
       psi_o(i_xeo)%cs(:,:) = psi_o(i_xeo)%cs(:,:) - Dpsi_o(i_xeo)%cs(:,:)
    end do
    deallocate(Dpsi_o)
  end subroutine InvLeftILUMatrix_eoflic
  subroutine InvRightILUMatrix_eoflic(psi_e,psi_o,psi)
    !! R^-1 = [ 1 -(1+T_ee)^-1 D_eo ]
    !! [ 0 1 ]
    ! begin args: psi_o, psi_e, psi
    type(dirac_fermion), dimension(:) :: psi_e,psi_o
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dpsi_o, Dpsi_e
    integer :: i_xeo
    ! begin execution
    allocate(Dpsi_o(n_xpeo))
    allocate(Dpsi_e(n_xpeo))
    call EOWilson_Split(psi_o,Dpsi_o,0)
    call EOCloverInv(Dpsi_o,Dpsi_e,0)
    Dpsi_o = Dpsi_e
    do i_xeo=1,n_xeo
       Dpsi_e(i_xeo)%cs(:,:) = psi_e(i_xeo)%cs(:,:) - Dpsi_o(i_xeo)%cs(:,:)
    end do
    call phi_eo2phi_x(Dpsi_e,psi_o,psi)
    deallocate(Dpsi_o)
    deallocate(Dpsi_e)
  end subroutine InvRightILUMatrix_eoflic
  subroutine Deoflic(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi
    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: phi_e,phi_o
    integer :: i_xeo
    ! begin execution
    allocate(phi_e(n_xpeo))
    allocate(phi_o(n_xpeo))
    call EOWilson_Split(phi,phi_e,0)
    call EOCloverInv(phi_e,Dphi,0)
    phi_e = Dphi
    call EOWilson_Split(phi_e,phi_o,1)
    Dphi = phi
    call AddEOClover(Dphi,phi,1)
    do i_xeo=1,n_xeo
       Dphi(i_xeo)%cs(:,:) = Dphi(i_xeo)%cs(:,:) - phi_o(i_xeo)%cs(:,:)
    end do
    deallocate(phi_e)
    deallocate(phi_o)
  end subroutine Deoflic
  subroutine Deoflic_dag(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi
    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: phi_e, phi_o
    integer :: i_xeo
    ! begin execution
    allocate(phi_e(n_xpeo))
    allocate(phi_o(n_xpeo))
    call EOWilsonDag_Split(phi,phi_e,1)
    call EOCloverInv(phi_e,Dphi,0)
    phi_e = Dphi
    call EOWilsonDag_Split(phi_e,phi_o,0)
    Dphi = phi
    call AddEOClover(Dphi,phi,1)
    do i_xeo=1,n_xeo
       Dphi(i_xeo)%cs(:,:) = Dphi(i_xeo)%cs(:,:) - phi_o(i_xeo)%cs(:,:)
    end do
    deallocate(phi_e)
    deallocate(phi_o)
  end subroutine Deoflic_dag
  subroutine Hsqeoflic(phi,Hsqphi)
    ! begin args: phi, Hsqphi
    type(dirac_fermion), dimension(:) :: phi, Hsqphi
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dphi
    ! begin execution
    allocate(Dphi(n_xpeo))
    call Heoflic(phi,Dphi)
    call Heoflic(Dphi,Hsqphi)
    deallocate(Dphi)
  end subroutine Hsqeoflic
  subroutine Heoflic(phi,Hphi)
    ! begin args: phi, Hphi
    type(dirac_fermion), dimension(:) :: phi, Hphi
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    call Deoflic(phi,Hphi)
    do i_xeo=1,n_xeo
       Hphi(i_xeo)%cs(:,3) = -Hphi(i_xeo)%cs(:,3)
       Hphi(i_xeo)%cs(:,4) = -Hphi(i_xeo)%cs(:,4)
    end do
  end subroutine Heoflic
  subroutine AddEOClover(Dphi, phi, i_eo)
    ! begin args: Dphi, phi, i_eo
    type(dirac_fermion), dimension(:) :: Dphi, phi
    integer :: i_eo
    ! i_eo = 0 => applies (1+T_ee)
    ! i_eo = 1 => applies (1+T_oo)
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo = 1,n_xeo
       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) + SigmaFp(i_xeo,i_eo)%cs(1,1,1,1)*phi(i_xeo)%cs(1,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,2,1,1)*phi(i_xeo)%cs(2,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,3,1,1)*phi(i_xeo)%cs(3,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,1,1,2)*phi(i_xeo)%cs(1,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,2,1,2)*phi(i_xeo)%cs(2,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,3,1,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) + SigmaFp(i_xeo,i_eo)%cs(2,1,1,1)*phi(i_xeo)%cs(1,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,2,1,1)*phi(i_xeo)%cs(2,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,3,1,1)*phi(i_xeo)%cs(3,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,1,1,2)*phi(i_xeo)%cs(1,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,2,1,2)*phi(i_xeo)%cs(2,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,3,1,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) + SigmaFp(i_xeo,i_eo)%cs(3,1,1,1)*phi(i_xeo)%cs(1,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,2,1,1)*phi(i_xeo)%cs(2,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,3,1,1)*phi(i_xeo)%cs(3,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,1,1,2)*phi(i_xeo)%cs(1,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,2,1,2)*phi(i_xeo)%cs(2,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,3,1,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) + SigmaFp(i_xeo,i_eo)%cs(1,1,2,1)*phi(i_xeo)%cs(1,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,2,2,1)*phi(i_xeo)%cs(2,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,3,2,1)*phi(i_xeo)%cs(3,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,1,2,2)*phi(i_xeo)%cs(1,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,2,2,2)*phi(i_xeo)%cs(2,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(1,3,2,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) + SigmaFp(i_xeo,i_eo)%cs(2,1,2,1)*phi(i_xeo)%cs(1,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,2,2,1)*phi(i_xeo)%cs(2,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,3,2,1)*phi(i_xeo)%cs(3,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,1,2,2)*phi(i_xeo)%cs(1,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,2,2,2)*phi(i_xeo)%cs(2,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(2,3,2,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) + SigmaFp(i_xeo,i_eo)%cs(3,1,2,1)*phi(i_xeo)%cs(1,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,2,2,1)*phi(i_xeo)%cs(2,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,3,2,1)*phi(i_xeo)%cs(3,1) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,1,2,2)*phi(i_xeo)%cs(1,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,2,2,2)*phi(i_xeo)%cs(2,2) + &
            & SigmaFp(i_xeo,i_eo)%cs(3,3,2,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + SigmaFm(i_xeo,i_eo)%cs(1,1,1,1)*phi(i_xeo)%cs(1,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,2,1,1)*phi(i_xeo)%cs(2,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,3,1,1)*phi(i_xeo)%cs(3,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,1,1,2)*phi(i_xeo)%cs(1,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,2,1,2)*phi(i_xeo)%cs(2,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,3,1,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + SigmaFm(i_xeo,i_eo)%cs(2,1,1,1)*phi(i_xeo)%cs(1,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,2,1,1)*phi(i_xeo)%cs(2,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,3,1,1)*phi(i_xeo)%cs(3,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,1,1,2)*phi(i_xeo)%cs(1,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,2,1,2)*phi(i_xeo)%cs(2,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,3,1,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + SigmaFm(i_xeo,i_eo)%cs(3,1,1,1)*phi(i_xeo)%cs(1,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,2,1,1)*phi(i_xeo)%cs(2,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,3,1,1)*phi(i_xeo)%cs(3,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,1,1,2)*phi(i_xeo)%cs(1,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,2,1,2)*phi(i_xeo)%cs(2,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,3,1,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + SigmaFm(i_xeo,i_eo)%cs(1,1,2,1)*phi(i_xeo)%cs(1,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,2,2,1)*phi(i_xeo)%cs(2,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,3,2,1)*phi(i_xeo)%cs(3,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,1,2,2)*phi(i_xeo)%cs(1,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,2,2,2)*phi(i_xeo)%cs(2,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(1,3,2,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + SigmaFm(i_xeo,i_eo)%cs(2,1,2,1)*phi(i_xeo)%cs(1,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,2,2,1)*phi(i_xeo)%cs(2,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,3,2,1)*phi(i_xeo)%cs(3,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,1,2,2)*phi(i_xeo)%cs(1,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,2,2,2)*phi(i_xeo)%cs(2,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(2,3,2,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + SigmaFm(i_xeo,i_eo)%cs(3,1,2,1)*phi(i_xeo)%cs(1,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,2,2,1)*phi(i_xeo)%cs(2,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,3,2,1)*phi(i_xeo)%cs(3,3) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,1,2,2)*phi(i_xeo)%cs(1,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,2,2,2)*phi(i_xeo)%cs(2,4) + &
            & SigmaFm(i_xeo,i_eo)%cs(3,3,2,2)*phi(i_xeo)%cs(3,4)
    end do
  end subroutine AddEOClover
  subroutine EOCloverInv(phi, Dphi, i_eo)
    ! begin args: phi, Dphi, i_eo
    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi
    integer :: i_eo
    ! i_eo = 0 => applies (1+T_ee)^{-1}
    ! i_eo = 1 => applies (1+T_oo)^{-1}
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo = 1,n_xeo
       Dphi(i_xeo)%cs(1,1) = InvSigmaFp(i_xeo,i_eo)%cs(1,1,1,1)*phi(i_xeo)%cs(1,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,2,1,1)*phi(i_xeo)%cs(2,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,3,1,1)*phi(i_xeo)%cs(3,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,1,1,2)*phi(i_xeo)%cs(1,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,2,1,2)*phi(i_xeo)%cs(2,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,3,1,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(2,1) = InvSigmaFp(i_xeo,i_eo)%cs(2,1,1,1)*phi(i_xeo)%cs(1,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,2,1,1)*phi(i_xeo)%cs(2,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,3,1,1)*phi(i_xeo)%cs(3,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,1,1,2)*phi(i_xeo)%cs(1,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,2,1,2)*phi(i_xeo)%cs(2,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,3,1,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(3,1) = InvSigmaFp(i_xeo,i_eo)%cs(3,1,1,1)*phi(i_xeo)%cs(1,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,2,1,1)*phi(i_xeo)%cs(2,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,3,1,1)*phi(i_xeo)%cs(3,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,1,1,2)*phi(i_xeo)%cs(1,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,2,1,2)*phi(i_xeo)%cs(2,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,3,1,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(1,2) = InvSigmaFp(i_xeo,i_eo)%cs(1,1,2,1)*phi(i_xeo)%cs(1,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,2,2,1)*phi(i_xeo)%cs(2,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,3,2,1)*phi(i_xeo)%cs(3,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,1,2,2)*phi(i_xeo)%cs(1,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,2,2,2)*phi(i_xeo)%cs(2,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(1,3,2,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(2,2) = InvSigmaFp(i_xeo,i_eo)%cs(2,1,2,1)*phi(i_xeo)%cs(1,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,2,2,1)*phi(i_xeo)%cs(2,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,3,2,1)*phi(i_xeo)%cs(3,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,1,2,2)*phi(i_xeo)%cs(1,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,2,2,2)*phi(i_xeo)%cs(2,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(2,3,2,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(3,2) = InvSigmaFp(i_xeo,i_eo)%cs(3,1,2,1)*phi(i_xeo)%cs(1,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,2,2,1)*phi(i_xeo)%cs(2,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,3,2,1)*phi(i_xeo)%cs(3,1) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,1,2,2)*phi(i_xeo)%cs(1,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,2,2,2)*phi(i_xeo)%cs(2,2) + &
            & InvSigmaFp(i_xeo,i_eo)%cs(3,3,2,2)*phi(i_xeo)%cs(3,2)
       Dphi(i_xeo)%cs(1,3) = InvSigmaFm(i_xeo,i_eo)%cs(1,1,1,1)*phi(i_xeo)%cs(1,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,2,1,1)*phi(i_xeo)%cs(2,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,3,1,1)*phi(i_xeo)%cs(3,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,1,1,2)*phi(i_xeo)%cs(1,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,2,1,2)*phi(i_xeo)%cs(2,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,3,1,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(2,3) = InvSigmaFm(i_xeo,i_eo)%cs(2,1,1,1)*phi(i_xeo)%cs(1,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,2,1,1)*phi(i_xeo)%cs(2,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,3,1,1)*phi(i_xeo)%cs(3,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,1,1,2)*phi(i_xeo)%cs(1,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,2,1,2)*phi(i_xeo)%cs(2,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,3,1,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(3,3) = InvSigmaFm(i_xeo,i_eo)%cs(3,1,1,1)*phi(i_xeo)%cs(1,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,2,1,1)*phi(i_xeo)%cs(2,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,3,1,1)*phi(i_xeo)%cs(3,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,1,1,2)*phi(i_xeo)%cs(1,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,2,1,2)*phi(i_xeo)%cs(2,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,3,1,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(1,4) = InvSigmaFm(i_xeo,i_eo)%cs(1,1,2,1)*phi(i_xeo)%cs(1,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,2,2,1)*phi(i_xeo)%cs(2,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,3,2,1)*phi(i_xeo)%cs(3,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,1,2,2)*phi(i_xeo)%cs(1,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,2,2,2)*phi(i_xeo)%cs(2,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(1,3,2,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(2,4) = InvSigmaFm(i_xeo,i_eo)%cs(2,1,2,1)*phi(i_xeo)%cs(1,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,2,2,1)*phi(i_xeo)%cs(2,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,3,2,1)*phi(i_xeo)%cs(3,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,1,2,2)*phi(i_xeo)%cs(1,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,2,2,2)*phi(i_xeo)%cs(2,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(2,3,2,2)*phi(i_xeo)%cs(3,4)
       Dphi(i_xeo)%cs(3,4) = InvSigmaFm(i_xeo,i_eo)%cs(3,1,2,1)*phi(i_xeo)%cs(1,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,2,2,1)*phi(i_xeo)%cs(2,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,3,2,1)*phi(i_xeo)%cs(3,3) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,1,2,2)*phi(i_xeo)%cs(1,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,2,2,2)*phi(i_xeo)%cs(2,4) + &
            & InvSigmaFm(i_xeo,i_eo)%cs(3,3,2,2)*phi(i_xeo)%cs(3,4)
    end do
  end subroutine EOCloverInv
  subroutine DefineSpinField(delta_ns, gamma_mu, gamma_5, sigma_munu, chiral_basis)
    ! begin args: delta_ns, gamma_mu, gamma_5, sigma_munu, chiral_basis
    complex(dc), dimension(ns,ns) :: delta_ns, gamma_5
    complex(dc), dimension(nd,ns,ns) :: gamma_mu ! Dirac matrices
    complex(dc), dimension(nd,nd,ns,ns) :: sigma_munu
    logical, optional :: chiral_basis
    ! begin local_vars
    integer :: mu, nu, is
    complex(dc) :: J = (1.0d0,0.0d0)
    complex(dc), parameter :: O = (0.0d0,0.0d0)
    ! begin execution
    delta_ns =0
    do is = 1, ns
       delta_ns(is,is)=1.0d0
    end do
    !Define the Gamma matrices(Non-relativistic or Sakurai notation)
    gamma_mu(1,:,:) = RESHAPE( (/O,O,O,I,O,O,I,O,O,-I,O,O,-I,O,O,O/), (/4,4/) )
    gamma_mu(2,:,:) = RESHAPE( (/O,O,O,-J,O,O,J,O,O,J,O,O,-J,O,O,O/), (/4,4/) )
    gamma_mu(3,:,:) = RESHAPE( (/O,O,I,O,O,O,O,-I,-I,O,O,O,O,I,O,O/), (/4,4/) )
    if (present(chiral_basis)) then
       if (chiral_basis) then
          gamma_mu(4,:,:) = RESHAPE( (/O,O,J,O,O,O,O,J,J,O,O,O,O,J,O,O/), (/4,4/) )
          gamma_5(:,:) = RESHAPE( (/J,O,O,O,O,J,O,O,O,O,-J,O,O,O,O,-J/), (/4,4/) )
       else
          gamma_mu(4,:,:) = RESHAPE( (/J,O,O,O,O,J,O,O,O,O,-J,O,O,O,O,-J/), (/4,4/) )
          gamma_5(:,:) = RESHAPE( (/O,O,-J,O,O,O,O,-J,-J,O,O,O,O,-J,O,O/), (/4,4/) )
       end if
    else
       !Sakurai basis
       gamma_mu(4,:,:) = RESHAPE( (/J,O,O,O,O,J,O,O,O,O,-J,O,O,O,O,-J/), (/4,4/) )
       gamma_5(:,:) = RESHAPE( (/O,O,-J,O,O,O,O,-J,-J,O,O,O,O,-J,O,O/), (/4,4/) )
    end if
    !Define Sigma_\mu\nu=(1/4)[Gamma_\mu, Gamma_\nu]
    do mu=1,nd; do nu=1,nd
       sigma_munu(mu,nu,:,:) = 0.5d0 * MatMul(gamma_mu(mu,:,:), gamma_mu(nu,:,:)) - &
            & 0.5d0 * MatMul(gamma_mu(nu,:,:), gamma_mu(mu,:,:))
    end do; end do
  end subroutine DefineSpinField
  subroutine CalcCloverTerms(F_munu,mfic_sw)
    ! begin args: F_munu, mfic_sw
    type(colour_matrix), dimension(:,:,:,:,:) :: F_munu !Clover based field strength tensor
    ! begin local_vars
    integer :: ix,iy,iz,it,is,js,ic,jc
    integer :: i_xeo, i_eo, i_x(nd)
    integer :: mu,nu,ip
    real(dp) :: kappa,c_sw,u0
    complex(dc), dimension(6,6) :: sFp, sFm, InvsFp, InvsFm
    complex(dc), dimension(ns,ns) :: delta_ns, gamma_5
    complex(dc), dimension(nd,ns,ns) :: gamma_mu ! Dirac matrices
    complex(dc), dimension(nd,nd,ns,ns) :: sigma_munu
    real(dp) :: mfic_sw
    ! begin execution
    call DefineSpinField(delta_ns, gamma_mu, gamma_5, sigma_munu, chiral_basis = .true. )
    SigmaFp = zero_colour_weyl_spin_matrix
    SigmaFm = zero_colour_weyl_spin_matrix
    do mu=1,nd
       do nu=mu+1,nd
          ip = mu+nu-1-1/mu
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
             i_x = (/ ix,iy,iz,it /)
             i_eo = modulo(sum(i_x)+proc_eo,2)
             i_xeo = x2eo(i_x)
             do js=1,2; do is=1,2
                !Include the minus sign for the clover term (-sigma.F) here.
                SigmaFp(i_xeo,i_eo)%cs(:,:,is,js) = SigmaFp(i_xeo,i_eo)%cs(:,:,is,js) - mfic_sw*sigma_munu(mu,nu,is,js)*F_munu(ix,iy,iz,it,ip)%Cl(:,:)
                SigmaFm(i_xeo,i_eo)%cs(:,:,is,js) = SigmaFm(i_xeo,i_eo)%cs(:,:,is,js) - mfic_sw*sigma_munu(mu,nu,is+2,js+2)*F_munu(ix,iy,iz,it,ip)%Cl(:,:)
             end do; end do
          end do; end do; end do; end do
       end do
    end do
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       i_x = (/ ix,iy,iz,it /)
       i_eo = modulo(sum(i_x)+proc_eo,2)
       i_xeo = x2eo(i_x)
       sFp = 0.0d0
       sFm = 0.0d0
       do js=1,2; do is=1,2
          do jc=1,nc; do ic=1,nc
             sFp(ic + (is-1)*nc,jc + (js-1)*nc) = SigmaFp(i_xeo,i_eo)%cs(ic,jc,is,js)
             sFm(ic + (is-1)*nc,jc + (js-1)*nc) = SigmaFm(i_xeo,i_eo)%cs(ic,jc,is,js)
          end do; end do
       end do; end do
       do is=1,6
          sFp(is,is) = 1.0d0 + sFp(is,is)
          sFm(is,is) = 1.0d0 + sFm(is,is)
       end do
       call GaussJordanInv(6,sFp,InvsFp)
       call GaussJordanInv(6,sFm,InvsFm)
       do js=1,2; do is=1,2
          do jc=1,nc; do ic=1,nc
             InvSigmaFp(i_xeo,i_eo)%cs(ic,jc,is,js) = InvsFp(ic + (is-1)*nc,jc + (js-1)*nc)
             InvSigmaFm(i_xeo,i_eo)%cs(ic,jc,is,js) = InvsFm(ic + (is-1)*nc,jc + (js-1)*nc)
          end do; end do
       end do; end do
    end do; end do; end do; end do
  end subroutine CalcCloverTerms
  subroutine EOWilsonDag_Split(phi, Dphi, i_eo)
    ! begin args: phi, Dphi, i_eo
    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi
    integer :: i_eo
    integer :: i_xeo
    ! begin local_vars
    ! begin execution
    do i_xeo=1,n_xeo
       phi(i_xeo)%cs(:,3) = -phi(i_xeo)%cs(:,3)
       phi(i_xeo)%cs(:,4) = -phi(i_xeo)%cs(:,4)
    end do
    call EOWilson_Split(phi,Dphi,1-i_eo)
    do i_xeo=1,n_xeo
       phi(i_xeo)%cs(:,3) = -phi(i_xeo)%cs(:,3)
       phi(i_xeo)%cs(:,4) = -phi(i_xeo)%cs(:,4)
       Dphi(i_xeo)%cs(:,3) = -Dphi(i_xeo)%cs(:,3)
       Dphi(i_xeo)%cs(:,4) = -Dphi(i_xeo)%cs(:,4)
    end do
  end subroutine EOWilsonDag_Split
  subroutine GeteoflicShadow(phi)
    ! begin args: phi, ishdw
    type(dirac_fermion), dimension(:) :: phi
    ! begin local_vars
    integer :: ishdw
    integer :: ierror, intime, outtime
    integer :: it, src_rank, dest_rank, nxd, nyd, nzd, ntd
    integer :: sendrecv_reqz(nt,2),sendrecv_reqt(2), sendrecv_reqzs(nts,2), ierr
    integer :: sendrecvz_status(nmpi_status,nt*2), sendrecvt_status(nmpi_status,2), sendrecvzs_status(nmpi_status,nts*2)
    real(dp) :: t_in, t_out
    integer :: i_xeo, j_xeo
    ! begin execution
    nxd = nx
    nyd = ny
    nzd = nz
    ntd = nt
    ishdw = 1
    if ( nprocz > 1 ) then
       dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
       src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)
       ! Get the forward shadow.
       ! # of sites = (nx*ny*/2)*nt (even field or odd field)
       do it=1,nt
          i_xeo = x2eo((/1,1,1,it/))
          j_xeo = x2eo((/1,1,nz+1,it/))
          call MPI_ISSend(phi(i_xeo), nc*ns*(nx*ny)/2, mpi_dc,dest_rank,it,mpi_comm,sendrecv_reqz(it,1),ierr)
          call MPI_IRecv (phi(j_xeo), nc*ns*(nx*ny)/2, mpi_dc, src_rank,it,mpi_comm,sendrecv_reqz(it,2),ierr)
       end do
       call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
       dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)
       src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
       ! Get the backward shadow.
       ! # of sites = (nx*ny*/2)*nt (even field or odd field)
       do it=1,nt
          i_xeo = x2eo((/1,1,nz,it/))
          j_xeo = x2eo((/1,1,0,it/))
          call MPI_ISSend(phi(i_xeo), nc*ns*(nx*ny)/2, mpi_dc,dest_rank,it,mpi_comm,sendrecv_reqz(it,1),ierr)
          call MPI_IRecv (phi(j_xeo), nc*ns*(nx*ny)/2, mpi_dc, src_rank,it,mpi_comm,sendrecv_reqz(it,2),ierr)
       end do
       call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
       nzd = nz + 2*ishdw
    end if
    if ( nproct > 1 ) then
       dest_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
       src_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)
       ! Get the forward shadow.
       i_xeo = x2eo((/1,1,1,1/))
       j_xeo = x2eo((/1,1,1,nt+1/))
       ! # of sites = nx*ny*nz/2 (even field or odd field)
       call MPI_ISSend(phi(i_xeo), nc*ns*(nx*ny*nz)/2, mpi_dc, dest_rank, 1, mpi_comm, sendrecv_reqt(1), ierr)
       call MPI_IRecv (phi(j_xeo), nc*ns*(nx*ny*nz)/2, mpi_dc, src_rank, 1, mpi_comm, sendrecv_reqt(2), ierr)
       call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
       dest_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)
       src_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
       ! Get the backward shadow.
       i_xeo = x2eo((/1,1,1,nt/))
       j_xeo = x2eo((/1,1,1,0/))
       call MPI_ISSend(phi(i_xeo), nc*ns*(nx*ny*nz)/2, mpi_dc, dest_rank, 1, mpi_comm, sendrecv_reqt(1), ierr)
       call MPI_IRecv (phi(j_xeo), nc*ns*(nx*ny*nz)/2, mpi_dc, src_rank, 1, mpi_comm, sendrecv_reqt(2), ierr)
       call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
    end if
  end subroutine GeteoflicShadow
  subroutine EOWilson_Split(phi, Dphi, i_eo)
    ! begin args: phi, Dphi, i_eo
    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi
    integer :: i_eo
    ! i_eo = 0 => takes odd sites to even sites.
    ! i_eo = 1 => takes even sites to odd sites.
    ! begin local_vars
    integer :: i_xeo, j_xeo, j_eo
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt
    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)
    ! begin execution
    !if (timing) intime = mpi_wtime()
    !commtime = 0.0d0
    call GeteoflicShadow(phi)
    j_eo = 1-i_eo
    Dphi = zero_dirac_fermion
    do i_xeo=1,n_xeo
      ! mu = 1
      j_xeo = i_xpmu(i_xeo,1,i_eo)
      ! G_1^- Up phi_xpmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)
      psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)
      psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! G_1^+ Um phi_xpmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)
      psi_x%Cl(1) = Um_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)
      psi_x%Cl(1) = Um_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 2
      j_xeo = i_xpmu(i_xeo,2,i_eo)
      ! G_2^- Up phi_xpmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + phi(j_xeo)%cs(1,4)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + phi(j_xeo)%cs(2,4)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + phi(j_xeo)%cs(3,4)
      psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - phi(j_xeo)%cs(1,3)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - phi(j_xeo)%cs(2,3)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - phi(j_xeo)%cs(3,3)
      psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + psi_x%Cl(3)
      ! G_2^+ Um phi_xpmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - phi(j_xeo)%cs(1,4)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - phi(j_xeo)%cs(2,4)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - phi(j_xeo)%cs(3,4)
      psi_x%Cl(1) = Um_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + phi(j_xeo)%cs(1,3)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + phi(j_xeo)%cs(2,3)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + phi(j_xeo)%cs(3,3)
      psi_x%Cl(1) = Um_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - psi_x%Cl(3)
      ! mu = 3
      j_xeo = i_xpmu(i_xeo,3,i_eo)
      ! G_3^- Up phi_xpmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)
      psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)
      psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! G_3^+ Um phi_xpmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)
      psi_x%Cl(1) = Um_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)
      psi_x%Cl(1) = Um_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 4
      j_xeo = i_xpmu(i_xeo,4,i_eo)
      ! G_4^- Up phi_xpmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - phi(j_xeo)%cs(1,3)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - phi(j_xeo)%cs(2,3)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - phi(j_xeo)%cs(3,3)
      psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - phi(j_xeo)%cs(1,4)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - phi(j_xeo)%cs(2,4)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - phi(j_xeo)%cs(3,4)
      psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + psi_x%Cl(3)
      ! G_4^+ Um phi_xpmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + phi(j_xeo)%cs(1,3)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + phi(j_xeo)%cs(2,3)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + phi(j_xeo)%cs(3,3)
      psi_x%Cl(1) = Um_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + phi(j_xeo)%cs(1,4)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + phi(j_xeo)%cs(2,4)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + phi(j_xeo)%cs(3,4)
      psi_x%Cl(1) = Um_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - psi_x%Cl(3)
      ! mu = 1
      j_xeo = i_xmmu(i_xeo,1,i_eo)
      ! G_1^+ Up phi_xmmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)
      psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)
      psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! G_1^- Um phi_xmmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)
      psi_x%Cl(1) = conjg(Um_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)
      psi_x%Cl(1) = conjg(Um_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 2
      j_xeo = i_xmmu(i_xeo,2,i_eo)
      ! G_2^+ Up phi_xmmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - phi(j_xeo)%cs(1,4)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - phi(j_xeo)%cs(2,4)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - phi(j_xeo)%cs(3,4)
      psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + phi(j_xeo)%cs(1,3)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + phi(j_xeo)%cs(2,3)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + phi(j_xeo)%cs(3,3)
      psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - psi_x%Cl(3)
      ! G_2^- Um phi_xmmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + phi(j_xeo)%cs(1,4)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + phi(j_xeo)%cs(2,4)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + phi(j_xeo)%cs(3,4)
      psi_x%Cl(1) = conjg(Um_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - phi(j_xeo)%cs(1,3)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - phi(j_xeo)%cs(2,3)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - phi(j_xeo)%cs(3,3)
      psi_x%Cl(1) = conjg(Um_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + psi_x%Cl(3)
      ! mu = 3
      j_xeo = i_xmmu(i_xeo,3,i_eo)
      ! G_3^+ Up phi_xmmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)
      psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)
      psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! G_3^- Um phi_xmmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)
      psi_x%Cl(1) = conjg(Um_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)
      psi_x%Cl(1) = conjg(Um_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 4
      j_xeo = i_xmmu(i_xeo,4,i_eo)
      ! G_4^+ Up phi_xmmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + phi(j_xeo)%cs(1,3)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + phi(j_xeo)%cs(2,3)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + phi(j_xeo)%cs(3,3)
      psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + phi(j_xeo)%cs(1,4)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + phi(j_xeo)%cs(2,4)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + phi(j_xeo)%cs(3,4)
      psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - psi_x%Cl(3)
      ! G_4^- Um phi_xmmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - phi(j_xeo)%cs(1,3)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - phi(j_xeo)%cs(2,3)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - phi(j_xeo)%cs(3,3)
      psi_x%Cl(1) = conjg(Um_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - phi(j_xeo)%cs(1,4)
      Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - phi(j_xeo)%cs(2,4)
      Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - phi(j_xeo)%cs(3,4)
      psi_x%Cl(1) = conjg(Um_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)
      Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + psi_x%Cl(1)
      Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + psi_x%Cl(2)
      Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + psi_x%Cl(3)
    end do
    !if (timing) then
    ! outtime = mpi_wtime()
    ! call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    !end if
    !call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
    !waste = commtime/(outtime - intime)
    !call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)
  end subroutine EOWilson_Split
end module eoflicMatrix
