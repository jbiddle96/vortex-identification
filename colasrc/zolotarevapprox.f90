!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/zolotarevapprox.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: zolotarevapprox.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

module ZolotarevApprox

  use ColourTypes
  use Kinds
  implicit none
  private

  integer, public :: n_poles, n_zolo = 0

  real(dp), public :: x_min, x_max, delta_z
  real(dp), public :: c_2n, d_n
  real(dp), dimension(:), allocatable, public :: b_l, c_l
  real(dp), dimension(:), allocatable, public :: a_l
  complex(dc), dimension(:), allocatable, public :: p_l, q_l
  complex(dc), public :: p_2n

  integer, public :: n_lambda !number of eigenvectors to project out

  real(dp), dimension(:), allocatable, public :: epsilon_lambda_i !eigenvalues
  type(colour_vector), dimension(:,:,:,:,:,:), allocatable, public :: v_lambda, Dv_lambda !eigenvectors to project out
  real(DP), public :: delta_lambda !the precision to which the eigenvalues are known

  public :: GetZolotarevEstError
  public :: GetZolotarevEstOrder
  public :: GetZolotarevCoeffs
  public :: FactorZolotarevCoeffs
  public :: sncndnK
  public :: ArithGeom

contains

  function GetZolotarevEstError(n,lambda_min,lambda_max) result (delta)
    ! begin args: n, lambda_min, lambda_max
    integer :: n
    real(dp) :: lambda_min,lambda_max
    ! begin local_vars
    real(dp) :: delta
    real(dp), parameter :: cc = 0.982d0*9.17d0, ce = -0.774d0, ac = 0.465d0, ae = 0.596d0
    real(dp) :: kappa_c, c_k, a_k

    ! begin execution

    kappa_c = (lambda_max/lambda_min)**2
    c_k = cc*log(kappa_c)**ce
    a_k = ac*log(kappa_c)**ae

    delta = 2.0d0*a_k*exp(-c_k*n)


  end function GetZolotarevEstError

  function GetZolotarevEstOrder(delta,lambda_min,lambda_max) result (n)
    ! begin args: delta, lambda_min, lambda_max
    real(dp) :: delta
    real(dp) :: lambda_min,lambda_max
    ! begin local_vars
    integer :: n
    real(dp), parameter :: cc = 0.982d0*9.17d0, ce = -0.774d0, ac = 0.465d0, ae = 0.596d0
    real(dp) :: kappa_c, c_k, a_k

    ! begin execution

    kappa_c = (lambda_max/lambda_min)**2
    c_k = cc*log(kappa_c)**ce
    a_k = ac*log(kappa_c)**ae

    n = ceiling(-log(0.5d0*delta/a_k)/c_k)


  end function GetZolotarevEstOrder

  subroutine GetZolotarevCoeffs(n,a_l,b_l,c_l,c_2n,d_n,lambda_min,lambda_max,delta)
    ! begin args: n, a_l, b_l, c_l, c_2n, d_n, lambda_min, lambda_max, delta
    integer :: n
    real(dp), dimension(n) :: a_l,b_l, c_l
    real(dp) :: c_2n,d_n,lambda_min,lambda_max,delta

    ! begin local_vars
    real(dp), dimension(n) :: c_m, cp_m
    real(dp), dimension(n) :: a_m, ap_m
    real(dp) :: kappa, kappap, Kp, Kpp, sn, cn, dn, xi, z, lambda, M
    integer :: i,j

    ! begin execution

    kappa = abs(lambda_min/lambda_max)
    kappap = sqrt(1.0d0 - kappa**2)

    call sncndnK(0.0d0,kappap,sn,cn,dn,Kp)

    call sncndnK(Kp/(2*n+1),kappap,sn,cn,dn,Kpp)
    xi = 1.0d0/dn

    do j=1,n
       z = (2*j*Kp)/(2*n+1)
       call sncndnK(z,kappap,sn,cn,dn,Kpp)
       c_m(j) = -(cn/sn)**2
       a_m(j) = (sn/cn)**2

       z = ((2*j-1)*Kp)/(2*n+1)
       call sncndnK(z,kappap,sn,cn,dn,Kpp)
       cp_m(j) =  -(cn/sn)**2
       ap_m(j) = (sn/cn)**2
    end do

    M = 1.0d0
    do j=1,n
       M = M*(1.0d0 - c_m(j))/(1.0d0 - cp_m(j))
    end do

    lambda = (M/xi)
    do j=1,n
       lambda = lambda*(1.0d0 - cp_m(j)*xi**2)/(1.0d0 - c_m(j)*xi**2)
    end do

    !The error delta = max|epsilon(x) - epsilon_n(x)|
    delta = (1.0d0 - lambda)/(1.0d0 + lambda)

    !Coefficients of epsilon_n(x) = d_n*x*(x^2+c_2n)*sum(b_l/(x**2+c_l)), x\in[lambda_min,lambda_max]
    d_n = 2.0d0/(abs(lambda_min)*(1.0d0+1.0d0/lambda))
    do j=1,n
       d_n = d_n*c_m(j)*(1.0d0 - cp_m(j))/(cp_m(j)*(1.0d0 - c_m(j)))
    end do

    do i=1,n
       b_l(i) = 1.0d0
       do j=1,n
          if ( j /= n ) b_l(i) = b_l(i)*(a_m(j)-ap_m(i))
          if ( j /= i ) b_l(i) = b_l(i)/(ap_m(j)-ap_m(i))
       end do
    end do
    a_l = a_m*(lambda_min**2)
    c_l = ap_m*(lambda_min**2)
    c_2n = a_m(n)*(lambda_min**2)

    !Rescale so that epsilon_n(x) <= 1.0
    d_n = d_n/(1.0d0+delta)
    delta = 2.0d0*delta


  end subroutine GetZolotarevCoeffs

  subroutine FactorZolotarevCoeffs(n,a_l,c_l,d_n,p_l,q_l,p_2n)
    ! begin args: n, a_l, c_l, d_n, p_l, q_l, p_2n

    integer :: n
    real(dp), dimension(n) :: a_l,c_l
    real(dp) :: d_n
    complex(dc), dimension(n) :: p_l,q_l
    complex(dc) :: p_2n

    ! begin local_vars
    complex(dc), dimension(n) :: r_l
    integer :: j,k

    ! begin execution

    q_l = i*sqrt(a_l)
    r_l = i*sqrt(c_l)

    do k=1,n
       p_l(k) = 1.0d0
       do j=1,n
          if ( j /= n ) p_l(k) = p_l(k)*(r_l(j)-q_l(k))
          if ( j /= k ) p_l(k) = p_l(k)/(q_l(j)-q_l(k))
       end do
    end do
    p_2n = r_l(n)


  end subroutine FactorZolotarevCoeffs

  subroutine sncndnK(z,k,sn,cn,dn,K_k)
    ! begin args: z, k, sn, cn, dn, K_k
    real(dp) :: z,k
    real(dp) :: sn,cn,dn,K_k

    ! begin local_vars
    real(dp) :: agm,pb
    integer :: sgn

    ! begin execution

    pb = -1.0d0
    sn = ArithGeom(z,1.0d0,sqrt(1.0d0-k*k),agm,pb)
    K_k = pi/(2.0d0 * agm)

    sgn = mod(int(abs(z)/K_k),4)! sgn = 0, 1, 2, 3
    sgn = min(1,mod(sgn,3))     ! sgn = 0, 1, 1, 0
    sgn = 1 - 2*sgn             ! sgn = 1,-1,-1, 1

    cn = sgn*sqrt(1.0d0 - sn**2)
    dn = sqrt(1.0d0 - (k*sn)**2)


  end subroutine sncndnK

  recursive function ArithGeom(z,a,b,agm,pb) result (sn)
    ! begin args: z, a, b, agm, pb
    real(dp) :: z,a,b
    real(dp) :: agm,pb
    ! begin local_vars
    real(dp) :: sn

    real(dp) :: xi

    ! Convergence: if b is less than its previous value (pb),
    ! then the algorithm has converged (i.e. b has ceased to increase).

    ! begin execution

    if (b <= pb) then
       pb = -1.0d0
       agm = a
       sn = sin(z*a)
    else
       pb = b
       xi = ArithGeom(z, 0.5d0*(a+b), sqrt(a*b), agm, pb)
       sn = (2.0d0*a*xi)/((a+b)+(a-b)*xi*xi)
    end if


  end function ArithGeom

end module ZolotarevApprox

