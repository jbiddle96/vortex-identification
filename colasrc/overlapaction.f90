!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

#include "defines.h"
#include "fermionfields.h"

module OverlapAction

  use AccMinEVCG
  use ColourTypes
  use SignFunction
  use FermionAction
  use FermionTypes
  use FermActTypes
  use SpinorTypes
  use EVTypes
  use ZolotarevApprox
  use Timer
  use LatticeSize
  use FatLinks
  use GaugeField
  use FermionField
  use FermionFieldIO
  use CollectiveOps
  use MPIInterface
  use Kinds
  use TextIO
  use Strings
  implicit none
  private

  type, public :: SignFunctionType
     integer :: n_lambda
     character(len=_FILENAME_LEN_) :: LoModeFile
     real(dp) :: delta_z
     logical :: DynamicSignFunc
     integer :: n_poles
     real(dp) :: x_min
     real(dp) :: x_max
     real(dp) :: c_2n
     real(dp) :: d_n
     real(dp), dimension(:), allocatable :: b_l
     real(dp), dimension(:), allocatable :: c_l
   contains
     procedure :: Get => Get_signfunc
!     procedure :: Put => Put_signfunc
!     procedure :: Print => Print_signfunc
  end type SignFunctionType

  type(SignFunctionType) :: sign_func

  real(dp), public :: epsilon_tol = 1.0d-8 !sign function tolerance
  integer, dimension(:), allocatable, public :: epsilon_iter_i !sign function iterations

  integer, dimension(:), allocatable, public :: miniter_i, maxiter_i
  real, dimension(:), allocatable, public :: meaniter_i

  logical, parameter, public :: VerboseOverlap = .true., DebugOverlap = .false.

  integer, public :: dummy

  integer, public :: ncalls2 = 0
  real(dp), public :: maxtime2 = 0, mintime2
  real(dp), public :: meantime2 = 0.0

  character(len=256), public :: ZoloFile, InputHiMode, InputLoMode
  logical, public :: DynamicSignFunc
  character(len=_FILENAME_LEN_), public :: LoModeFile, HiModeFile
  real(dp) :: kappa_kernel
  character(len=32) :: KernelAction
  class(wilsonFermAct), pointer :: kernelFermAct
  
  character(len=32) :: EVFileFormat

  public :: FindEigenSpace
  public :: GetEigenSpace
  public :: GetOverlapParams
  public :: GetZolotarevParams
  public :: InitialiseOverlapAction
  public :: FinaliseOverlapAction
  public :: Doverlap
  public :: Doverlap_dag
  public :: Hoverlap
  public :: Hsqoverlap
  public :: ChiralHsqoverlap

  !Assumes we are working in the chiral gamma matrix basis.
  
contains

  subroutine Get_signfunc(this,file_unit)
    class(SignFunctionType) :: this
    integer :: file_unit

    call Get(this%n_lambda,file_unit)
    call Get(this%LoModeFile,file_unit)
    call Get(this%DynamicSignFunc,file_unit)
    call Get(this%n_poles,file_unit)
    call Get(this%x_min,file_unit)
    call Get(this%x_max,file_unit)
    call Get(this%c_2n,file_unit)
    call Get(this%d_n,file_unit)
    !call Get(this%b_l,file_unit)
    !call Get(this%c_l,file_unit)

  end subroutine Get_signfunc

  subroutine GetOverlapParams(file_stub)

    character(len=*) :: file_stub

    character(len=_FILENAME_LEN_) :: overlapfile
    integer :: file_unit

    overlapfile = trim(file_stub)//".overlap"

    call OpenTextfile(trim(overlapfile),file_unit,action='read')
    !call Get(KernelAction,file_unit)
    !call Get(kappa_kernel,file_unit)
    call Get(epsilon_tol,file_unit)
    call Get(delta_z,file_unit)
    call Get(DynamicSignFunc,file_unit)
    call Get(n_lambda,file_unit)
    if ( n_lambda > 0 ) then
       call Get(LoModeFile,file_unit)
       call Get(HiModeFile,file_unit)
       call Get(EVFileFormat,file_unit)
    end if
    call CloseTextfile(file_unit)

    call Put("Overlap parameters:")
    !call Put("Kernel fermion action: "//trim(KernelAction))
    !call Put("Kernel mass parameter")
    !m_f = 4.0d0 - 0.5d0/kappa_kernel
    !call Put("m_w="//str(m_f,24)//" kappa_kernel= "//str(kappa_kernel,24))

    call Put("Sign function CG Tolerance:"//str(epsilon_tol,24))
    call Put("Sign function precision:"//str(delta_z,24))
    call Put("Use a dynamic sign function approximation?:"//str(DynamicSignFunc,6))
    call Put("Number of eigenvectors to project, n_lambda:"//str(n_lambda,12))
    if ( n_lambda > 0 ) then
       call Put("Low eigenmode file:"//trim(LoModeFile))
       call Put("Hi eigenmode file:"//trim(HiModeFile))
       call Put("Eigenmode file format:"//trim(EVFileFormat))
    end if

  end subroutine GetOverlapParams

  subroutine FindEigenSpace(n_lambda,epsilon_lambda_i,v_i,lambda_min,lambda_max,tol_loev, tol_hiev)

    integer :: n_lambda
    real(dp), dimension(:) :: epsilon_lambda_i
    _fermion_eigenspace_type_ :: v_i
    real(dp) :: lambda_min, lambda_max, tol_loev, tol_hiev

    ! begin local_vars
    real(dp), dimension(:), allocatable :: lambda_i
    _fermion_eigenspace_type_, allocatable :: Dv_i, vprime_i
    integer, dimension(:), allocatable :: CGIter_i !Total eigenvector CG iterations
    integer :: i_ev,n_ev,n_auxev
    real(dp) :: lambda_scale

    n_ev = 2
    n_auxev = 1
    lambda_scale = -1.0_dp

    allocate(CGIter_i(n_ev))
    allocate(lambda_i(n_ev))
    allocate(vprime_i(_fermion_eigenspace_extents_(n_ev)))
    allocate(Dv_i(_fermion_eigenspace_extents_(n_ev)))

    call Put("Calculating Hi Modes")
    call MinEVSpectrum(n_ev, lambda_i, vprime_i, Dv_i, tol_hiev, CGIter_i, .true., Hsq_fm, n_auxev, lambda_scale )
    lambda_max = sqrt(abs(lambda_i(1)))
    call Put("# CG Iterations"//str(sum(CGIter_i),14)//str(RecalcDpsiCount,14))

    deallocate(CGIter_i)
    deallocate(lambda_i)
    deallocate(Dv_i)
    deallocate(vprime_i)

    n_auxev = max(1,n_lambda/20)
    n_ev = n_lambda + n_auxev
    lambda_scale = 1.0_dp

    allocate(CGIter_i(n_ev))
    allocate(lambda_i(n_ev))
    allocate(vprime_i(_fermion_eigenspace_extents_(n_ev)))
    allocate(Dv_i(_fermion_eigenspace_extents_(n_ev)))

    call Put("Calculating Lo Modes")
    call MinEVSpectrum(n_ev, lambda_i, vprime_i, Dv_i, tol_loev, CGIter_i, .true., Hsq_fm, n_auxev, lambda_scale )
    call Put("# CG Iterations"//str(sum(CGIter_i),14)//str(RecalcDpsiCount,14))

    v_i = vprime_i
    call DiagonaliseBasis(n_lambda, lambda_i, v_i, Dv_i, vprime_i, H_fm, 1.0d0, .false. )
    lambda_min = abs(lambda_i(n_lambda))

    mpiprint '(a6,a22,a8)', "i_ev","lambda_i", "iter"
    do i_ev=1,n_lambda
       mpiprint '(I6,F22.17,I8,2F15.10)' , i_ev, lambda_i(i_ev), CGIter_i(i_ev)
    end do

    epsilon_lambda_i = sign(1.0d0,lambda_i(1:n_lambda))

    deallocate(CGIter_i)
    deallocate(Dv_i)
    deallocate(vprime_i)

  end subroutine FindEigenSpace

  subroutine GetEigenSpace(lambda_min,lambda_max)
    ! begin args: lambda_min, lambda_max

    real(dp) :: lambda_min, lambda_max

    ! begin local_vars
    real(dp) :: m_w, bct, error
    integer :: m_ev, m_dummy, i_lambda

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: psi, Dpsi
    integer :: ix,iy,iz,it,is

    ! begin execution
    allocate(psi(nxp,nyp,nzp,ntp,ns))
    allocate(Dpsi(nxp,nyp,nzp,ntp,ns))

    mpiprint *, "Reading Hi Modes"
    if ( i_am_root ) then
       open (100, file=InputHiMode, status = "old", form = "unformatted", action = "read")
       read (100) m_ev, m_dummy, delta_lambda, m_w, bct
       read (100) lambda_max
       close(100)
    end if

    call Broadcast(lambda_max,mpi_root_rank)

    mpiprint *, "Reading Lo Modes"
    !call ReadEigenspace(n_lambda,m_dummy,epsilon_lambda_i,v_lambda,delta_lambda,m_w,bct,InputLoMode)

    do i_lambda=1,n_lambda
       call Broadcast(epsilon_lambda_i(i_lambda),mpi_root_rank)
    end do
    call Broadcast(delta_lambda,mpi_root_rank)

    lambda_min = epsilon_lambda_i(n_lambda)

    epsilon_lambda_i = sign(1.0d0,epsilon_lambda_i)

    deallocate(psi)
    deallocate(Dpsi)

  end subroutine GetEigenSpace

  subroutine GetZolotarevParams(file_stub)

    character(len=*) :: file_stub

    ! begin local_vars
    integer :: ipole
    character(len=_FILENAME_LEN_) :: Zolofile
    integer :: file_unit

    ! begin execution

    zolofile = trim(file_stub)//".zolo"

    call OpenTextfile(trim(Zolofile),file_unit,action='read')

    call Get(n_poles)
    call Get(x_min)
    call Get(x_max)
    call Get(c_2n)
    call Get(d_n)
    allocate(b_l(n_poles))
    allocate(c_l(n_poles))
    do ipole=1,n_poles
       call Get(b_l(ipole))
    end do
    do ipole=1,n_poles
       call Get(c_l(ipole))
    end do
    call CloseTextfile(file_unit)

  end subroutine GetZolotarevParams

  subroutine InitialiseOverlapAction(kappa_w)
    real(dp), optional :: kappa_w
    !type(colour_matrix), dimension(:,:,:,:,:) :: U_xd

    ! begin local_vars
    real(DP) :: lambda_min, lambda_max

    type(EVMetaDataType) :: lo_modes, hi_modes
    real(DP) :: kappa, delta, dx, x, err, err2
    integer :: i_pole
    type(dirac_fermion), dimension(:,:), allocatable :: FMEV
    real(dp) :: m_w

    !call InitialiseFermionAction(U_xd,kappa_kernel)

    ! begin execution
    project_eigenspace = (n_lambda > 0)

    if ( n_lambda > 0 ) then

       call lo_modes%Read(trim(LoModeFile)//".metadata")
       call hi_modes%Read(trim(HiModeFile)//".metadata")

       if ( n_lambda > lo_modes%n_ev ) call Abort("InitialiseOverlap: n_lambda exceeds lo_modes%n_ev")
       lambda_min = abs(lo_modes%lambda_i(n_lambda))
       lambda_max = abs(hi_modes%lambda_i(1))

       allocate(epsilon_lambda_i(n_lambda))
       allocate(v_lambda(_fermion_eigenspace_extents_(n_lambda)))
       
       delta_lambda = lo_modes%tolerance
       epsilon_lambda_i = sign(1.0d0,lo_modes%lambda_i(1:n_lambda))
       print '(10f10.5)', lo_modes%lambda_i(1:n_lambda)
       print '(10f10.5)', epsilon_lambda_i(1:n_lambda)

       !call FindEigenSpace(n_lambda,epsilon_lambda_i,v_lambda,lambda_min,lambda_max,delta_lambda,1.0d-7)
       allocate(FMEV(lo_modes%n_x,n_lambda))
       call ReadFermionMatrixEigenVectors(LoModeFile,FMEV)
       call FMEVToFermionField(FMEV,v_lambda)
       deallocate(FMEV)

    else
       m_w = 4.0_dp - 1.0_dp/(2.0_dp*kappa_w)
       lambda_max = (8.0d0 - m_w)*(2.0_dp*kappa_w)
       lambda_min = (1.0d0 - abs(m_w - 1.0d0))*(2.0_dp*kappa_w)
    end if

    n_poles = GetZolotarevEstOrder(delta_z,lambda_min,lambda_max)
       
    allocate(a_l(n_poles))
    allocate(b_l(n_poles))
    allocate(c_l(n_poles))
       
    call GetZolotarevCoeffs(n_poles,a_l,b_l,c_l,c_2n,d_n,lambda_min,lambda_max,delta_z)

    !deallocate(a_l)
    call Put("lambda_min= "//str(lambda_min,24))
    call Put("lambda_max= "//str(lambda_max,24))
    call Put("n_poles= "//str(n_poles,24))
    call Put("delta_z= "//str(delta_z,24))

    !else
       !call GetZolotarevParams(file_stub)
    !end if

    allocate(epsilon_iter_i(n_poles))
    allocate(miniter_i(n_poles))
    allocate(maxiter_i(n_poles))
    allocate(meaniter_i(n_poles))

    !if ( project_eigenspace ) then
    !   call GetEigenSpace(lambda_min,lambda_max)
    !else
    !   lambda_max = 8.0d0 - m_w
    !   lambda_min = 1.0d0 - abs(m_w - 1.0d0)
    !end if

    !call ReadZolotarevApprox
    !lambda_min = x_min
    !lambda_max = x_max
    !zolotarev approximation is good between [1.0d0,kappa]

    kappa = abs(lambda_max/lambda_min)

    delta = delta_z

    if (VerboseOverlap) then
       mpiprint *, "kappa", kappa
       mpiprint *, "lambda_min", lambda_min
       mpiprint *, "min_error", 1.0d0 - zolotarev(abs(lambda_min))
       mpiprint *, "lambda_max", lambda_max
       mpiprint *, "max_error", 1.0d0 - zolotarev(abs(lambda_max))
       mpiprint *, "n_poles", n_poles
       mpiprint *, "polar order needed",  -0.25d0*sqrt(kappa)*log(delta/2.0d0)
       dx = abs(lambda_max-lambda_min)/(nlx*nly*nlz*nlt*ns*nc)

       err = 0.0d0
       err2 = 0.0d0
       x = abs(lambda_min)
       do while ( x <= abs(lambda_max) )
          err = err + abs(1.0d0 - zolotarev(x))
          err2 = max(err2,abs(zolotarev(x) - 1.0d0))
          x = x + dx
       end do
       err = err/(nlx*nly*nlz*nlt*ns*nc)
       mpiprint *, "mean error", err, "max error", err2

    end if

  contains

    function zolotarev(x)
      ! begin args: x
      ! begin local_vars
      real(DP) :: zolotarev
      real(DP) :: x

      ! begin execution

      zolotarev = d_n*x*(x**2+c_2n)*(sum(b_l/(x**2+c_l)))

    end function zolotarev


  end subroutine InitialiseOverlapAction

  subroutine FinaliseOverlapAction

    ! begin execution

    deallocate(epsilon_iter_i)
    deallocate(miniter_i)
    deallocate(maxiter_i)
    deallocate(meaniter_i)

    deallocate(epsilon_lambda_i)
    deallocate(v_lambda)

    deallocate(b_l)
    deallocate(c_l)
  end subroutine FinaliseOverlapAction

#define phi_x(is) phi(ix,iy,iz,it,is)
#define Dphi_x(is) Dphi(ix,iy,iz,it,is)
#define epsilon_Hphi_x(is) epsilon_Hphi(ix,iy,iz,it,is)

  subroutine Doverlap(phi, Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: epsilon_Hphi

    integer :: ix,iy,iz,it,is
    real(dp) :: intime, outtime

    ! begin execution
    allocate(epsilon_Hphi(nxp,nyp,nzp,ntp,ns))

    if (timing) intime = mpi_wtime()

    !Evaluate the sign function
    call MatrixSignFunctionOperate(n_poles, phi, epsilon_Hphi, epsilon_tol, epsilon_iter_i, &
         & H_fm, Hsq_fm, n_lambda, epsilon_lambda_i, v_lambda, delta_lambda )

    !Multiply by gamma_5
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       epsilon_Hphi_x(3)%Cl = -epsilon_Hphi_x(3)%Cl
       epsilon_Hphi_x(4)%Cl = -epsilon_Hphi_x(4)%Cl
    end do; end do; end do; end do


    !Dphi = (1/2)*(1 + gamma_5*epsilon(H))*phi
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       Dphi_x(is)%Cl = 0.5d0*(phi_x(is)%Cl + epsilon_Hphi_x(is)%Cl)
    end do; end do; end do; end do; end do

    if (timing) then
       ncalls2 = ncalls2 + 1
       outtime = mpi_wtime()
       call TimingUpdate(ncalls2,outtime,intime,mintime2,maxtime2,meantime2)
       where ( epsilon_iter_i == 0 ) epsilon_iter_i = epsilon_iter_i(1)
       where ( epsilon_iter_i < miniter_i ) miniter_i = epsilon_iter_i
       where ( epsilon_iter_i > maxiter_i ) maxiter_i = epsilon_iter_i
       meaniter_i = (ncalls2 - 1)*meaniter_i/ncalls2 + epsilon_iter_i/real(ncalls2)
    end if

    deallocate(epsilon_Hphi)

  end subroutine Doverlap

  subroutine ChiralHsqoverlap(phi, Dphi, lambda_pm)
    ! begin args: phi, Dphi, lambda_pm

    !assumes phi is chiral, with chirality lambda_pm
    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    integer :: lambda_pm

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi_s, epsilon_Hphi

    integer :: ix,iy,iz,it
    real(dp) :: intime, outtime
    integer :: is,js,n_spinor

    ! begin execution
    allocate(phi_s(nxp,nyp,nzp,ntp,ns))
    allocate(epsilon_Hphi(nxp,nyp,nzp,ntp,ns))

    if (timing) intime = mpi_wtime()
    !Evaluate the sign function

    n_spinor = size(phi,dim=5)

    is = 2 - lambda_pm
    js = 2 + lambda_pm
    
    if ( n_spinor == 2 ) then
       phi_s(:,:,:,:,is  ) = phi(:,:,:,:,1)
       phi_s(:,:,:,:,is+1) = phi(:,:,:,:,2)
       phi_s(:,:,:,:,js  ) = zero_vector
       phi_s(:,:,:,:,js+1) = zero_vector
    else
       phi_s = phi
    end if

    call MatrixSignFunctionOperate(n_poles, phi_s, epsilon_Hphi, epsilon_tol, epsilon_iter_i, &
         & H_fm, Hsq_fm, n_lambda, epsilon_lambda_i, v_lambda, delta_lambda )

    !Dphi = (1/2)*phi + (1/4)*(gamma_5*epsilon(H)*phi + lambda_pm*epsilon(H)*phi)

    if ( n_spinor == 2 ) then
       is = 1
    else
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Dphi_x(js)%Cl   = 0.0d0
          Dphi_x(js+1)%Cl = 0.0d0
       end do; end do; end do; end do
    end if

    if ( lambda_pm == 1 ) then
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Dphi_x(is)%Cl   = 0.5d0*(phi_x(is)%Cl   + epsilon_Hphi_x(1)%Cl)
          Dphi_x(is+1)%Cl = 0.5d0*(phi_x(is+1)%Cl + epsilon_Hphi_x(2)%Cl)
       end do; end do; end do; end do
    else
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Dphi_x(is)%Cl   = 0.5d0*(phi_x(is)%Cl   - epsilon_Hphi_x(3)%Cl)
          Dphi_x(is+1)%Cl = 0.5d0*(phi_x(is+1)%Cl - epsilon_Hphi_x(4)%Cl)
       end do; end do; end do; end do
    end if

    if (timing) then
       ncalls2 = ncalls2 + 1
       outtime = mpi_wtime()
       call TimingUpdate(ncalls2,outtime,intime,mintime2,maxtime2,meantime2)
       where ( epsilon_iter_i == 0 ) epsilon_iter_i = epsilon_iter_i(1)
       where ( epsilon_iter_i < miniter_i ) miniter_i = epsilon_iter_i
       where ( epsilon_iter_i > maxiter_i ) maxiter_i = epsilon_iter_i
       meaniter_i = (ncalls2 - 1)*meaniter_i/ncalls2 + epsilon_iter_i/real(ncalls2)
    end if

    deallocate(phi_s)
    deallocate(epsilon_Hphi)

  end subroutine ChiralHsqoverlap

  subroutine Doverlap_dag(phi, Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    ! begin local_vars
    integer :: ix,iy,iz,it,is
    ! begin execution

    !Multiply by gamma_5
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       phi_x(3)%Cl = -phi_x(3)%Cl
       phi_x(4)%Cl = -phi_x(4)%Cl
    end do; end do; end do; end do

    call Doverlap(phi, Dphi)

    !Multiply by gamma_5
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       phi_x(3)%Cl = -phi_x(3)%Cl
       phi_x(4)%Cl = -phi_x(4)%Cl
       Dphi_x(3)%Cl = -Dphi_x(3)%Cl
       Dphi_x(4)%Cl = -Dphi_x(4)%Cl
    end do; end do; end do; end do


  end subroutine Doverlap_dag

  subroutine Hoverlap(phi, Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    ! begin local_vars
    integer :: ix,iy,iz,it,is
    ! begin execution

    call Doverlap(phi, Dphi)

    !Multiply by gamma_5
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       Dphi_x(3)%Cl = -Dphi_x(3)%Cl
       Dphi_x(4)%Cl = -Dphi_x(4)%Cl
    end do; end do; end do; end do


  end subroutine Hoverlap

  subroutine Hsqoverlap(phi, Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Dsqphi

    integer :: ix,iy,iz,it,is

    ! begin execution
    allocate(Dsqphi(nxp,nyp,nzp,ntp,ns))

    call Doverlap(phi, Dphi)

    !Multiply by gamma_5
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       Dphi_x(3)%Cl = -Dphi_x(3)%Cl
       Dphi_x(4)%Cl = -Dphi_x(4)%Cl
    end do; end do; end do; end do

    call Doverlap(Dphi, Dsqphi)

    !Multiply by gamma_5
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       Dphi_x(3)%Cl = -Dphi_x(3)%Cl
       Dphi_x(4)%Cl = -Dphi_x(4)%Cl
    end do; end do; end do; end do

    Dphi = Dsqphi

    deallocate(Dsqphi)

  end subroutine Hsqoverlap

end module OverlapAction


