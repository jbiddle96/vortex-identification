!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

#include "defines.h"

module AccMinEVCG

  use ColourTypes
  use FermionTypes
  use SpinorTypes
  use CmplxJacobiDiag
  use FermionField
  use MPIInterface
  use ReduceOps
  use LatticeSize
  use Kinds
  implicit none
  private

  !!Eigenvalue routines for lattice shaped (nx,ny,nz,nt) fermion fields.

  !The only procedure needed to be called is MinEvSpectrum, after which lambda_i and v_i will contain
  !the lowest n_ev eigenvalues and their corresponding eigenvectors, respectively.

  logical, public :: VerboseAccCGEV = .true. !More or less output
  integer, public :: CGIter_Max = 100 !Maximum CG Iterations per cycle per eigenvalue
  integer, public :: StateRenormCount = 0
  integer, public :: RecalcDpsiCount = 0
  integer, public :: CGCycles = 0
  real(dp) :: tau = 0.1d0 !Determines the criterion for stopping the CG search and performing an Diagonalisation

  logical, dimension(:), allocatable, public :: converged_i !eigenvectors that have converged
  logical, dimension(:), allocatable, public  :: normg_i !||g|| < tolerance?
  logical, dimension(:), allocatable, public :: temple_i !temple < tolerance?
  logical, dimension(:), allocatable, public :: deltaC_i !deltacycle < tolerance?
  logical, dimension(:), allocatable, public:: TauCycleExit_i !Did we improve by tau in a CG cycle?
  real(dp), dimension(:), allocatable, public :: deltaTau_i !ritz functional values
  real(dp), dimension(:), allocatable, public :: lower_i !lower bounds
  real(dp), dimension(:), allocatable, public :: deltaGrad_i, deltaTemple_i, deltaCycle_i !Gradient, Temple and Cycle error estimates
  logical, dimension(:), allocatable, public :: UseTemple_i !temple < tolerance?
  logical, dimension(:), allocatable, public :: UseDeltaCycle_i !Is delta_cycle safe?

  public :: MinEVSpectrum
  public :: ConjGradEigenMode
  public :: OrthogonaliseEigenspace
  public :: ScaleVector
  public :: DiagonaliseBasis

  interface MinEVSpectrum
     module procedure MinEVSpectrum_4x
     module procedure MinEVSpectrum_eo
  end interface

  interface ConjGradEigenMode
     module procedure ConjGradEigenMode_4x
     module procedure ConjGradEigenMode_eo
  end interface

  interface OrthogonaliseEigenspace
     module procedure OrthogonaliseEigenspace_4x
     module procedure OrthogonaliseEigenspace_eo
  end interface

  interface ScaleVector
     module procedure ScaleVector_4x
     module procedure ScaleVector_eo
  end interface

  interface DiagonaliseBasis
     module procedure DiagonaliseBasis_4x
     module procedure DiagonaliseBasis_eo
  end interface

contains

  subroutine MinEVSpectrum_4x(n_ev,lambda_i, v_i, Dv_i, tolerance, CGIter_i,  InitVectors, MatrixOperate, n_dummy, lambda_scale )
    ! begin args: n_ev, lambda_i, v_i, Dv_i, tolerance, CGIter_i, InitVectors, MatrixOperate, n_dummy, lambda_scale

    integer, intent(in) :: n_ev !number of eigenvectors to calculate
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i, Dv_i !eigenvectors and their derivatives
    real(dp), dimension(n_ev) :: lambda_i !eigenvalues
    integer, dimension(n_ev) :: CGIter_i !Total eigenvector CG iterations
    real(dp), intent(inout) :: tolerance !Desired Precision of eigenvectors
    logical :: InitVectors !Use a random guess to start (initialise the eigenvectors)
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         implicit none
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface
    integer :: n_dummy !Do not demand convergence for the last n_dummy eigenvectors, n_dummy small
    real(dp) :: lambda_scale !Scale the operator by lambda_scale during MinEVSpectrum.
    !Note: Eigenvalues returned are unscaled.
    !lambda_scale = 1.0d0 : unscaled
    !lambda_scale = 0.0d0 : find only the zero eigenvalues
    !lambda_scale = -1.0d0 : highest eigenvalues found
    !lambda_scale = 1/||M|| : use normalised operator during search
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable  :: vprime_i

    real(dp), dimension(n_ev) :: mu_i, mu1_i, mu2_i !ritz functional values from 0,1,2 previous diagonalizations.

    integer :: iterations
    real(dp) :: norm_grad_mu, norm_grad_mu_pp

    integer :: i_ev, k_ev
    integer :: ix,iy,iz,it,is,ns

    ! begin execution
    ns = size(v_i,5)

    allocate(vprime_i(size(v_i,1),size(v_i,2),size(v_i,3),size(v_i,4),ns,n_ev))

    !if (abs(lambda_scale) /= 0.0d0) then
    !   tolerance = tolerance*abs(lambda_scale)
    !end if

    allocate(converged_i(n_ev))
    allocate(normg_i(n_ev))
    allocate(temple_i(n_ev))
    allocate(deltaC_i(n_ev))
    allocate(TauCycleExit_i(n_ev))

    allocate(deltaTau_i(n_ev))
    allocate(lower_i(n_ev))
    allocate(deltaGrad_i(n_ev))
    allocate(deltaTemple_i(n_ev))
    allocate(deltaCycle_i(n_ev))
    allocate(UseTemple_i(n_ev))
    allocate(UseDeltaCycle_i(n_ev))

    VerboseAccCGEV = VerboseAccCGEV .and. i_am_root

    CGCycles = 0

    CGIter_i=0
    converged_i = .false.
    normg_i = .false.
    temple_i = .false.
    deltaC_i = .false.
    TauCycleExit_i = .false.

    mu_i=0.0d0
    mu1_i=0.0d0
    mu2_i=0.0d0
    deltaTau_i=0.0d0
    lower_i=0.0d0
    UseTemple_i = .false.
    UseDeltaCycle_i = .false.


!!  Commented out to allow starting with less initial eigenvectors than the number of desired eigenvectors.
!!    if (.not. InitVectors ) then
!!       if (VerboseAccCGEV ) print *, "Rotating Eigenvectors"
!!       vprime_i = v_i
!!       call DiagonaliseBasis(n_ev, mu_i, v_i, Dv_i, vprime_i, MatrixOperate, lambda_scale, .true. )
!!    end if

    do i_ev=0,n_ev-1
       call ConjGradEigenMode(i_ev,n_ev,lambda_i,v_i,Dv_i,tolerance,iterations,InitVectors,MatrixOperate,lambda_scale)
       CGIter_i(i_ev+1) = iterations
       if (VerboseAccCGEV ) print *, "i_ev=", i_ev+1, "lambda=", lambda_i(i_ev+1), "iterations=", iterations
    end do

    CGCycles = 1

    do
       if ( all(converged_i(1:n_ev-n_dummy)) ) exit

       if (VerboseAccCGEV ) print *, "Cycle #:", CGCycles

       if (CGCycles == 1) then
          mu1_i = lambda_i
       end if

       mu2_i = mu1_i
       mu1_i = lambda_i

       if (VerboseAccCGEV ) print *, "Rotating Eigenvectors"
       vprime_i = v_i
       call DiagonaliseBasis(n_ev, mu_i, v_i, Dv_i, vprime_i, MatrixOperate, lambda_scale, .true. )

       !update the ritz functional values
       if (CGCycles > 1) then
          !Calculate delta cycle
          deltaTau_i = abs(mu2_i - mu_i)/(1.0d0-tau)
          UseDeltaCycle_i = ( mu2_i >= mu_i - tolerance ) .and. ( TauCycleExit_i ) .and. (CGCycles > 3)
       end if

       !Determine whether the Temple Inequality is safe
       do k_ev=1,n_ev
          if (k_ev<n_ev) then
             lower_i(k_ev) = minval(mu_i(k_ev+1:n_ev), mask = mu_i(k_ev+1:n_ev) - mu_i(k_ev) > tolerance)
          end if

          norm_grad_mu_pp = 0.0d0

          do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             norm_grad_mu_pp = norm_grad_mu_pp + sum(abs(Dv_i(ix,iy,iz,it,is,k_ev)%Cl - mu_i(k_ev)*v_i(ix,iy,iz,it,is,k_ev)%Cl)**2)
          end do; end do; end do; end do; end do

          call AllSum(norm_grad_mu_pp,norm_grad_mu)
          norm_grad_mu = sqrt(norm_grad_mu)

          UseTemple_i(k_ev) = abs(lower_i(k_ev)-mu_i(k_ev)) < norm_grad_mu
          UseTemple_i(k_ev) = any(mu_i(k_ev+1:n_ev) - mu_i(k_ev) > tolerance) .and. UseTemple_i(k_ev) .and. (CGCycles > 3 )
       end do
       UseTemple_i(n_ev) = .false. !The temple inequality is not valid for the highest eigenvalue.

       do i_ev=0,n_ev-1
          !if (VerboseAccCGEV ) print *, "Searching for Eigenvector ", i_ev+1
          call ConjGradEigenMode(i_ev,n_ev,lambda_i,v_i,Dv_i,tolerance,iterations,.false.,MatrixOperate,lambda_scale)
          CGIter_i(i_ev+1) = CGIter_i(i_ev+1)+iterations
          if (VerboseAccCGEV ) print *, "i_ev=", i_ev+1, "lambda=", lambda_i(i_ev+1), "iterations=", iterations
       end do
       CGCycles = CGCycles + 1

    end do

    if (lambda_scale /= 0.0d0) then
       lambda_i = lambda_i / lambda_scale
    end if

    if (VerboseAccCGEV) then
       do i_ev=1,n_ev
          mpiprint *, "||g||", normg_i(i_ev), "temple", temple_i(i_ev),"delta_c", deltaC_i(i_ev)
       end do

       do i_ev=1,n_ev
          mpiprint '(3(a,ES23.15))', "||g||", deltaGrad_i(i_ev), " temple", deltaTemple_i(i_ev), " delta_c", deltaCycle_i(i_ev)
       end do

       mpiprint '(a6,a22,a8,3a15)', "i_ev","lambda_i", "iter", "norm_g","delta_t","delta_c"

       do i_ev=1,n_ev
          mpiprint '(I6,F22.17,I8,3F15.10 )' , i_ev, lambda_i(i_ev), CGIter_i(i_ev), &
               & deltaGrad_i(i_ev), deltaTemple_i(i_ev), deltaCycle_i(i_ev)
       end do

    end if

    deallocate(converged_i)
    deallocate(normg_i)
    deallocate(temple_i)
    deallocate(deltaC_i)
    deallocate(TauCycleExit_i)

    deallocate(deltaTau_i)
    deallocate(lower_i)
    deallocate(deltaGrad_i)
    deallocate(deltaTemple_i)
    deallocate(deltaCycle_i)
    deallocate(UseTemple_i)
    deallocate(UseDeltaCycle_i)

    deallocate(vprime_i)

  end subroutine MinEVSpectrum_4x

  subroutine ConjGradEigenMode_4x(i_ev,n_ev,lambda_i,v_i,Dv_i,tolerance,iterations,InitVectors,MatrixOperate,lambda_scale)
    ! begin args: i_ev, n_ev, lambda_i, v_i, Dv_i, tolerance, iterations, InitVectors, MatrixOperate, lambda_scale

    integer :: i_ev, n_ev
    real(dp), dimension(n_ev) :: lambda_i
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i, Dv_i
    real(dp)  :: tolerance
    integer :: iterations
    logical, intent(in) :: InitVectors
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         implicit none
         type(colour_vector), dimension(:,:,:,:,:) :: phi, Dphi
       end subroutine MatrixOperate
    end interface
    real(dp) :: lambda_scale

    ! begin local_vars
    real(dp) :: mu_psi !the ritz functional
    real(dp) :: normsq_grad_mu, normsq_grad_mu_old, normsq_grad_mu_0, norm_search, norm_psi
    real(dp) :: alpha, beta, delta, a1, a2, a3, cos_delta, sin_delta, cos_theta, sin_theta

    type(colour_vector), dimension(:,:,:,:,:), allocatable  :: psi, Dpsi, grad_mu, search, Dsearch

    complex(DC) :: psidotDpsi, searchdotDsearch, psidotsearch, grad_mudotsearch

    integer :: t_cycle
    real(dp) :: stop_cycle, recalc_tol
    logical :: RenormState, CheckState, RecalcState, RecalcTest
    integer :: RenormFrequency, CheckFrequency, LastRecalc
    integer :: ix,iy,iz,it,is,ns

    ! begin execution
    ns = size(v_i,5)

    allocate(psi(nxp,nyp,nzp,ntp,ns))
    allocate(Dpsi(nxp,nyp,nzp,ntp,ns))
    allocate(grad_mu(nxp,nyp,nzp,ntp,ns))
    allocate(search(nxp,nyp,nzp,ntp,ns))
    allocate(Dsearch(nxp,nyp,nzp,ntp,ns))

    recalc_tol = min(1.0d-8,tolerance)


    if (InitVectors) then
       call random_fermion_field(psi)
    else
       psi(1:nx,1:ny,1:nz,1:nt,:) = v_i(:,:,:,:,:,i_ev+1)
    end if

    call ProjectVectorSpace(i_ev,v_i,psi)

    norm_psi = fermion_norm(psi)
    call normalise(psi,norm_psi)

    call MatrixOperate(psi, Dpsi)
    call ScaleVector_4x(Dpsi,lambda_scale)
    call ProjectVectorSpace(i_ev,v_i,Dpsi)

    mu_psi = real_inner_product(psi,Dpsi)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       grad_mu(ix,iy,iz,it,is)%Cl = Dpsi(ix,iy,iz,it,is)%Cl - mu_psi*psi(ix,iy,iz,it,is)%Cl
    end do; end do; end do; end do; end do

    normsq_grad_mu = fermion_normsq(grad_mu)
    normsq_grad_mu_0 = normsq_grad_mu

    search = grad_mu
    norm_search = fermion_norm(search)
    call MatrixOperate(search, Dsearch)
    call ScaleVector_4x(Dsearch, lambda_scale)
    call ProjectVectorSpace(i_ev,v_i,Dsearch)

    t_cycle = CGCycles + 1
    stop_cycle = tau**t_cycle
    !if ( lambda_scale /= 0.0d0 ) stop_cycle = abs(lambda_scale)*stop_cycle

    CheckFrequency = min(CGIter_Max/10, CGCycles + max(0,int(-log(normsq_grad_mu))) + max(0,20-i_ev) )
    RenormFrequency = min(50, 10 + CGCycles + max(0,int(-log(normsq_grad_mu))) + max(0,20-i_ev) )
    RecalcState = .true.

    TauCycleExit_i(i_ev+1) = .false.
    converged_i(i_ev+1) = .false.
    normg_i(i_ev+1) = .false.
    temple_i(i_ev+1) = .false.
    deltaC_i(i_ev+1) = .false.

    iterations = 0
    do
       deltaGrad_i(i_ev+1) = sqrt(normsq_grad_mu)
       if (  deltaGrad_i(i_ev+1) < tolerance) then
          converged_i(i_ev+1) = .true.
          normg_i(i_ev+1) = .true.
       end if

       if ( lambda_scale == 0.0d0 ) then
          if (  deltaGrad_i(i_ev+1) < tau*mu_psi ) then
             converged_i(i_ev+1) = .true.
          end if
       end if

       deltaTemple_i(i_ev+1) = normsq_grad_mu/(lower_i(i_ev+1) - mu_psi)

       if ( UseTemple_i(i_ev+1) .and. ( lower_i(i_ev+1) - mu_psi > 0 ) ) then
          if ( deltaTemple_i(i_ev+1) < tolerance ) then
             converged_i(i_ev+1) = .true.
             temple_i(i_ev+1) = .true.
          end if
       end if

       deltaCycle_i(i_ev+1) = deltaTau_i(i_ev+1)*(normsq_grad_mu/normsq_grad_mu_0)

       if ( (CGCycles > 2) .and. UseDeltaCycle_i(i_ev+1) ) then
          if ( deltaCycle_i(i_ev+1) < tolerance ) then
             converged_i(i_ev+1) = .true.
             deltaC_i(i_ev+1) = .true.
             TauCycleExit_i(i_ev+1) = .true.
          end if
       end if

       if (converged_i(i_ev+1)) exit

       if ( (iterations >= CGIter_Max) .and. (normsq_grad_mu < normsq_grad_mu_0) ) exit

       if ( iterations >= 3*CGIter_Max/2 ) exit

       if ( (CGCycles <= 3) .and. sqrt(abs(normsq_grad_mu)) < stop_cycle) exit

       if (CGCycles > 1 .and. iterations > 10 ) then
          if ( normsq_grad_mu/normsq_grad_mu_0 < tau) then
             TauCycleExit_i(i_ev+1) = .true.
             exit
          end if
       end if

       iterations = iterations + 1

       CheckState = .false.
       if (RecalcState) then
          StateRenormCount = StateRenormCount + 1
          CheckState = .true.
          LastRecalc = 1
       else
          LastRecalc = LastRecalc + 1
       end if
       if (LastRecalc >= CheckFrequency) then
          CheckState = .true.
          LastRecalc = 1
       end if

       RecalcState = .false.
       RenormState = (modulo(iterations,RenormFrequency)==0)

       normsq_grad_mu_old = normsq_grad_mu
       psidotDpsi = real_inner_product(psi,Dpsi)
       searchdotDsearch = real_inner_product(search,Dsearch)
       searchdotDsearch = searchdotDsearch/(norm_search**2)

       a1 = 0.5d0*(psidotDpsi + searchdotDsearch)
       a2 = 0.5d0*(psidotDpsi - searchdotDsearch)
       a3 = normsq_grad_mu/norm_search
       alpha = sqrt( a2**2 + a3**2)

       cos_delta = a2/alpha
       sin_delta = a3/alpha

       if (cos_delta <= 0.0d0 ) then
          cos_theta = sqrt(0.5*(1.0-cos_delta))
          sin_theta = -0.5*sin_delta/cos_theta
       else
          sin_theta = -sqrt(0.5*(1.0+cos_delta))
          cos_theta = -0.5*sin_delta/sin_theta
       end if

       mu_psi = psidotDpsi

       !mpiprint *, iterations, mu_psi, norm_search, checkstate, spacing(norm_search), recalc_tol

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          psi(ix,iy,iz,it,is)%Cl = cos_theta*psi(ix,iy,iz,it,is)%Cl + (sin_theta/norm_search)*search(ix,iy,iz,it,is)%Cl
       end do; end do; end do; end do; end do

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Dpsi(ix,iy,iz,it,is)%Cl = cos_theta*Dpsi(ix,iy,iz,it,is)%Cl + (sin_theta/norm_search)*Dsearch(ix,iy,iz,it,is)%Cl
       end do; end do; end do; end do; end do

       if (RenormState) then
          call ProjectVectorSpace(i_ev,v_i,psi)
          norm_psi = fermion_norm(psi)
          call normalise(psi,norm_psi)

          call MatrixOperate(psi, Dpsi)
          call ScaleVector_4x(Dpsi,lambda_scale)
          call ProjectVectorSpace(i_ev,v_i,Dpsi)
       else if (CheckState) then
          call OrthogonaliseEigenspace_4x(i_ev,v_i,psi,Dv_i,Dpsi)
          call ProjectVectorSpace(i_ev,v_i,Dpsi)
          norm_psi = fermion_norm(psi)
          RecalcTest = abs(norm_psi-1.0d0) > recalc_tol
          RecalcState = RecalcState .or. RecalcTest
          if (RecalcTest) then
             call normalise(psi,norm_psi)
             call normalise(Dpsi,norm_psi)
          end if
       end if

       mu_psi = mu_psi - 2*alpha*(sin_theta**2)

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          grad_mu(ix,iy,iz,it,is)%Cl = Dpsi(ix,iy,iz,it,is)%Cl - mu_psi*psi(ix,iy,iz,it,is)%Cl
       end do; end do; end do; end do; end do

       normsq_grad_mu = fermion_normsq(grad_mu)

       if (CheckState .or. RenormState) then
          grad_mudotsearch = inner_product(grad_mu,search)
          RecalcTest = (abs(grad_mudotsearch)/max(norm_search,1.0d0) > recalc_tol)
          RecalcState = RecalcState .or. RecalcTest
          if ( RenormState .or. RecalcTest ) then
             call orthogonalise(search,grad_mu,grad_mudotsearch/normsq_grad_mu)
          end if
       end if

       beta = cos_theta*(normsq_grad_mu/normsq_grad_mu_old)
       psidotsearch = inner_product(psi,search)

       beta = min(beta,100.0d0)

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          search(ix,iy,iz,it,is)%Cl = grad_mu(ix,iy,iz,it,is)%Cl + beta*( search(ix,iy,iz,it,is)%Cl - &
               & psi(ix,iy,iz,it,is)%Cl*psidotsearch)
       end do; end do; end do; end do; end do

       if (CheckState) then
          psidotsearch = inner_product(psi,search)
          RecalcTest = abs(psidotsearch)/max(norm_search,1.0d0) > recalc_tol
          RecalcState = RecalcState .or. RecalcTest
          if (RenormState .or. RecalcTest) then
             norm_psi = fermion_normsq(psi)
             call orthogonalise(search,psi,psidotsearch/norm_psi)
          end if
       end if

       norm_search = fermion_norm(search)

       if (CheckState) then
          !RecalcTest =  spacing(norm_search**2) > recalc_tol
          RecalcTest =  1.0d0/(norm_search**2) < recalc_tol
          RecalcState = RecalcState .or. RecalcTest
          if ( RecalcTest ) then
             !   search = grad_mu
             call normalise(search,norm_search)
             psidotsearch = inner_product(psi,search)
             grad_mudotsearch = inner_product(grad_mu,search)/normsq_grad_mu - 1.0d0
             !search = search - psidotsearch*psi - searchdotgrad_mu*grad_mu/normsq_grad_mu
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                search(ix,iy,iz,it,is)%Cl = search(ix,iy,iz,it,is)%Cl - grad_mu(ix,iy,iz,it,is)%Cl*grad_mudotsearch - &
                     & psi(ix,iy,iz,it,is)%Cl*psidotsearch
             end do; end do; end do; end do; end do
          end if
       end if

       call MatrixOperate(search, Dsearch)
       call ScaleVector_4x(Dsearch,lambda_scale)
       if (CheckState) then
          call ProjectVectorSpace(i_ev,v_i,Dsearch)
       end if

    end do

    call MatrixOperate(psi, Dpsi)
    call ScaleVector_4x(Dpsi,lambda_scale)
    call ProjectVectorSpace(i_ev,v_i,Dpsi)

    v_i(:,:,:,:,:,i_ev+1) = psi(1:nx,1:ny,1:nz,1:nt,:)
    Dv_i(:,:,:,:,:,i_ev+1) = Dpsi(1:nx,1:ny,1:nz,1:nt,:)
    lambda_i(i_ev+1) = mu_psi

    deallocate(psi)
    deallocate(Dpsi)
    deallocate(grad_mu)
    deallocate(search)
    deallocate(Dsearch)

  end subroutine ConjGradEigenMode_4x

  subroutine OrthogonaliseEigenspace_4x(n_v,v_i,phi,Dv_i,Dphi)
    ! begin args: n_v, v_i, phi, Dv_i, Dphi

    integer, intent(in) :: n_v
    type(colour_vector), dimension(:,:,:,:,:) :: phi, Dphi
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i,Dv_i

    ! begin local_vars
    complex(dc) :: v_idotphi
    integer :: ix,iy,iz,it,is,ns,i_v

    ! begin execution

    ns = size(phi,5)

    do i_v = 1,n_v
       v_idotphi = inner_product(v_i(:,:,:,:,:,i_v),phi)
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl - v_i(ix,iy,iz,it,is,i_v)%Cl*v_idotphi
          Dphi(ix,iy,iz,it,is)%Cl = Dphi(ix,iy,iz,it,is)%Cl - Dv_i(ix,iy,iz,it,is,i_v)%Cl*v_idotphi
       end do; end do; end do; end do; end do
    end do


  end subroutine OrthogonaliseEigenspace_4x

  subroutine ScaleVector_4x(Dphi, lambda_scale)
    ! begin args: Dphi, lambda_scale

    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    real(dp) :: lambda_scale
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns

    ! begin execution

    ns = size(Dphi,5)

    if ( (lambda_scale /= 1.0d0) .and. (lambda_scale /= 0.0d0) ) then
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Dphi(ix,iy,iz,it,is)%Cl = lambda_scale * Dphi(ix,iy,iz,it,is)%Cl
       end do; end do; end do; end do; end do
    end if


  end subroutine ScaleVector_4x

  subroutine DiagonaliseBasis_4x(n_ev, lambda_i, v_i, Dv_i, vprime_i, MatrixOperate, lambda_scale, renormalise )
    ! begin args: n_ev, lambda_i, v_i, Dv_i, vprime_i, MatrixOperate, lambda_scale, renormalise

    !Given a set of eigenvectors vprime_i of one operator (eg. H^2), diagonalise another operator (eg. H)
    !on the span of vprime_i, and return the new eigenvectors v_i, and also their derivatives Dv_i.
    !The eigenvalues/Ritz function values are returned in lambda_i
    !The vprime_i are renormalised and orthogonalised if renormalise == .true.

    integer, intent(in) :: n_ev !number of eigenvectors to calculate
    real(dp), dimension(n_ev) :: lambda_i !eigenvalues
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i, Dv_i !new eigenvectors
    type(colour_vector), dimension(:,:,:,:,:,:) :: vprime_i  !old eigenvectors
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         implicit none
         type(colour_vector), dimension(:,:,:,:,:) :: phi, Dphi
       end subroutine MatrixOperate
    end interface
    real(dp) :: lambda_scale
    logical :: renormalise

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable  :: phi, Dphi
    complex(DC), dimension(n_ev,n_ev) :: M_kl !Sub-matrix to be diagonalised
    complex(DC), dimension(n_ev,n_ev) :: V_kl !Matrix of sub-eigenvectors

    real(dp) :: swap, norm_v, norm_phi
    real(dp), dimension(n_ev) :: swap_l
    integer :: k_ev, l_ev, sweeps
    integer :: ix,iy,iz,it,is,ns

    ! begin execution
    ns = size(v_i,5)

    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate(Dphi(nxp,nyp,nzp,ntp,ns))

    do k_ev=1,n_ev
       phi(1:nx,1:ny,1:nz,1:nt,:) = vprime_i(:,:,:,:,:,k_ev)
       if ( renormalise ) then
          call ProjectVectorSpace(k_ev-1,v_i,phi)
          norm_phi = fermion_norm(phi)
          call normalise(phi,norm_phi)
       end if

       call MatrixOperate(phi,Dphi)
       call ScaleVector_4x(Dphi,lambda_scale)

       v_i(:,:,:,:,:,k_ev) = phi(1:nx,1:ny,1:nz,1:nt,:)
       Dv_i(:,:,:,:,:,k_ev) = Dphi(1:nx,1:ny,1:nz,1:nt,:)
    end do

    do k_ev=1,n_ev
       M_kl(k_ev,k_ev) = real_inner_product(v_i(:,:,:,:,:,k_ev),Dv_i(:,:,:,:,:,k_ev))
       do l_ev=k_ev+1,n_ev
           M_kl(k_ev,l_ev) = inner_product(v_i(:,:,:,:,:,k_ev),Dv_i(:,:,:,:,:,l_ev))
           M_kl(l_ev,k_ev) = conjg(M_kl(k_ev,l_ev))
       end do
    end do

    !Sort sub-eigenvectors into increasing eigenvalue order
    do k_ev=1,n_ev
       do l_ev=k_ev+1,n_ev
          if ( real(M_kl(l_ev,l_ev)) < real(M_kl(k_ev,k_ev)) ) then
             swap = M_kl(k_ev,k_ev)
             swap_l = V_kl(:,k_ev)
             M_kl(k_ev,k_ev) = M_kl(l_ev,l_ev)
             V_kl(:,k_ev) = V_kl(:,l_ev)
             M_kl(l_ev,l_ev) = swap
             V_kl(:,l_ev) = swap_l
          end if
       end do
    end do

    !Perform intermediate diagonalisation
    call ComplexDiag(n_ev, M_kl, V_kl, sweeps, JacobiPrecision)

    !Normalise sub-eigenvectors
    do l_ev=1,n_ev
       V_kl(:,l_ev) = V_kl(:,l_ev)/sqrt(sum(abs(V_kl(:,l_ev))**2 ))
    end do

    !Rotate eigenvectors and their derivatives
    vprime_i(:,:,:,:,:,1:n_ev) = Dv_i(:,:,:,:,:,1:n_ev)
    Dv_i(:,:,:,:,:,1:n_ev) = zero_vector

    do l_ev=1,n_ev; do k_ev=1,n_ev
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Dv_i(ix,iy,iz,it,is,l_ev)%Cl =  Dv_i(ix,iy,iz,it,is,l_ev)%Cl + &
               & V_kl(k_ev,l_ev)*vprime_i(ix,iy,iz,it,is,k_ev)%Cl
       end do; end do; end do; end do; end do
    end do; end do

    vprime_i(:,:,:,:,:,1:n_ev) = v_i(:,:,:,:,:,1:n_ev)
    v_i(:,:,:,:,:,1:n_ev) = zero_vector

    do l_ev=1,n_ev; do k_ev=1,n_ev
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          v_i(ix,iy,iz,it,is,l_ev)%Cl =  v_i(ix,iy,iz,it,is,l_ev)%Cl + &
               & V_kl(k_ev,l_ev)*vprime_i(ix,iy,iz,it,is,k_ev)%Cl
       end do; end do; end do; end do; end do
    end do; end do


    do k_ev=1,n_ev
       !lambda_i(k_ev) = lambda_scale*real_inner_product(v_i(:,:,:,:,:,k_ev),Dv_i(:,:,:,:,:,k_ev))
       lambda_i(k_ev) = real_inner_product(v_i(:,:,:,:,:,k_ev),Dv_i(:,:,:,:,:,k_ev))
    end do

    deallocate(phi)
    deallocate(Dphi)

  end subroutine DiagonaliseBasis_4x

  !! Subroutines for even-odd shaped fermions.

  subroutine MinEVSpectrum_eo(n_ev,lambda_i, v_i, Dv_i, tolerance, CGIter_i,  InitVectors, MatrixOperate, n_dummy, lambda_scale )
    ! begin args: n_ev, lambda_i, v_i, Dv_i, tolerance, CGIter_i, InitVectors, MatrixOperate, n_dummy, lambda_scale

    integer, intent(in) :: n_ev !number of eigenvectors to calculate
    type(dirac_fermion), dimension(:,:) :: v_i, Dv_i !eigenvectors and their derivatives
    real(dp), dimension(n_ev) :: lambda_i !eigenvalues
    integer, dimension(n_ev) :: CGIter_i !Total eigenvector CG iterations
    real(dp), intent(inout) :: tolerance !Desired Precision of eigenvectors
    logical :: InitVectors !Use a random guess to start (initialise the eigenvectors)
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         implicit none
         type(dirac_fermion), dimension(:) :: phi
         type(dirac_fermion), dimension(:) :: Dphi
       end subroutine MatrixOperate
    end interface
    integer :: n_dummy !Do not demand convergence for the last n_dummy eigenvectors, n_dummy small
    real(dp) :: lambda_scale !Scale the operator by lambda_scale during MinEVSpectrum.
    !Note: Eigenvalues returned are unscaled.
    !lambda_scale = 1.0d0 : unscaled
    !lambda_scale = 0.0d0 : find only the zero eigenvalues
    !lambda_scale = -1.0d0 : highest eigenvalues found
    !lambda_scale = 1/||M|| : use normalised operator during search
    ! begin local_vars
    type(dirac_fermion), dimension(:,:), allocatable  :: vprime_i

    real(dp), dimension(n_ev) :: mu_i, mu1_i, mu2_i !ritz functional values from 0,1,2 previous diagonalizations.

    integer :: iterations
    real(dp) :: norm_grad_mu, norm_grad_mu_pp

    integer :: i_ev, k_ev
    integer :: i_xeo

    ! begin execution

    allocate(vprime_i(size(v_i,1),n_ev))

    !if (abs(lambda_scale) /= 0.0d0) then
    !   tolerance = tolerance*abs(lambda_scale)
    !end if

    allocate(converged_i(n_ev))
    allocate(normg_i(n_ev))
    allocate(temple_i(n_ev))
    allocate(deltaC_i(n_ev))
    allocate(TauCycleExit_i(n_ev))

    allocate(deltaTau_i(n_ev))
    allocate(lower_i(n_ev))
    allocate(deltaGrad_i(n_ev))
    allocate(deltaTemple_i(n_ev))
    allocate(deltaCycle_i(n_ev))
    allocate(UseTemple_i(n_ev))
    allocate(UseDeltaCycle_i(n_ev))

    VerboseAccCGEV = VerboseAccCGEV .and. i_am_root

    CGCycles = 0

    CGIter_i=0
    converged_i = .false.
    normg_i = .false.
    temple_i = .false.
    deltaC_i = .false.
    TauCycleExit_i = .false.

    mu_i=0.0d0
    mu1_i=0.0d0
    mu2_i=0.0d0
    deltaTau_i=0.0d0
    lower_i=0.0d0
    UseTemple_i = .false.
    UseDeltaCycle_i = .false.

    if (.not. InitVectors ) then
       if (VerboseAccCGEV ) print *, "Rotating Eigenvectors"
       vprime_i = v_i
       call DiagonaliseBasis_eo(n_ev, mu_i, v_i, Dv_i, vprime_i, MatrixOperate, lambda_scale, .true. )
    end if

    do i_ev=0,n_ev-1
       call ConjGradEigenMode_eo(i_ev,n_ev,lambda_i,v_i,Dv_i,tolerance,iterations,InitVectors,MatrixOperate,lambda_scale)
       CGIter_i(i_ev+1) = iterations
       if (VerboseAccCGEV ) print *, "i_ev=", i_ev+1, "lambda=", lambda_i(i_ev+1), "iterations=", iterations
    end do

    CGCycles = 1

    do
       if ( all(converged_i(1:n_ev-n_dummy)) ) exit

       if (VerboseAccCGEV ) print *, "Cycle #:", CGCycles

       if (CGCycles == 1) then
          mu1_i = lambda_i
       end if

       mu2_i = mu1_i
       mu1_i = lambda_i

       if (VerboseAccCGEV ) print *, "Rotating Eigenvectors"
       vprime_i = v_i
       call DiagonaliseBasis_eo(n_ev, mu_i, v_i, Dv_i, vprime_i, MatrixOperate, lambda_scale, .true. )

       !update the ritz functional values
       if (CGCycles > 1) then
          !Calculate delta cycle
          deltaTau_i = abs(mu2_i - mu_i)/(1.0d0-tau)
          UseDeltaCycle_i = ( mu2_i >= mu_i - tolerance ) .and. ( TauCycleExit_i ) .and. (CGCycles > 3)
       end if

       !Determine whether the Temple Inequality is safe
       do k_ev=1,n_ev
          if (k_ev<n_ev) then
             lower_i(k_ev) = minval(mu_i(k_ev+1:n_ev), mask = mu_i(k_ev+1:n_ev) - mu_i(k_ev) > tolerance)
          end if

          norm_grad_mu_pp = 0.0d0

          do i_xeo =1,n_xeo
             norm_grad_mu_pp = norm_grad_mu_pp + sum(abs(Dv_i(i_xeo,k_ev)%cs - mu_i(k_ev)*v_i(i_xeo,k_ev)%cs)**2)
          end do

          call AllSum(norm_grad_mu_pp,norm_grad_mu)
          norm_grad_mu = sqrt(norm_grad_mu)

          UseTemple_i(k_ev) = abs(lower_i(k_ev)-mu_i(k_ev)) < norm_grad_mu
          UseTemple_i(k_ev) = any(mu_i(k_ev+1:n_ev) - mu_i(k_ev) > tolerance) .and. UseTemple_i(k_ev) .and. (CGCycles > 3 )
       end do
       UseTemple_i(n_ev) = .false. !The temple inequality is not valid for the highest eigenvalue.

       do i_ev=0,n_ev-1
          if (VerboseAccCGEV ) print *, "Searching for Eigenvector ", i_ev+1
          call ConjGradEigenMode_eo(i_ev,n_ev,lambda_i,v_i,Dv_i,tolerance,iterations,.false.,MatrixOperate,lambda_scale)
          CGIter_i(i_ev+1) = CGIter_i(i_ev+1)+iterations
          if (VerboseAccCGEV ) print *, "i_ev=", i_ev+1, "lambda=", lambda_i(i_ev+1), "iterations=", iterations
       end do
       CGCycles = CGCycles + 1

    end do

    if (lambda_scale /= 0.0d0) then
       lambda_i = lambda_i / lambda_scale
    end if

    if (VerboseAccCGEV) then
       do i_ev=1,n_ev
          mpiprint *, "||g||", normg_i(i_ev), "temple", temple_i(i_ev),"delta_c", deltaC_i(i_ev)
       end do

       do i_ev=1,n_ev
          mpiprint '(3(a,ES23.15))', "||g||", deltaGrad_i(i_ev), " temple", deltaTemple_i(i_ev), " delta_c", deltaCycle_i(i_ev)
       end do

       mpiprint '(a6,a22,a8,3a15)', "i_ev","lambda_i", "iter", "norm_g","delta_t","delta_c"

       do i_ev=1,n_ev
          mpiprint '(I6,F22.17,I8,3F15.10 )' , i_ev, lambda_i(i_ev), CGIter_i(i_ev), &
               & deltaGrad_i(i_ev), deltaTemple_i(i_ev), deltaCycle_i(i_ev)
       end do

    end if

    deallocate(converged_i)
    deallocate(normg_i)
    deallocate(temple_i)
    deallocate(deltaC_i)
    deallocate(TauCycleExit_i)

    deallocate(deltaTau_i)
    deallocate(lower_i)
    deallocate(deltaGrad_i)
    deallocate(deltaTemple_i)
    deallocate(deltaCycle_i)
    deallocate(UseTemple_i)
    deallocate(UseDeltaCycle_i)

    deallocate(vprime_i)

  end subroutine MinEVSpectrum_eo

  subroutine ConjGradEigenMode_eo(i_ev,n_ev,lambda_i,v_i,Dv_i,tolerance,iterations,InitVectors,MatrixOperate,lambda_scale)
    ! begin args: i_ev, n_ev, lambda_i, v_i, Dv_i, tolerance, iterations, InitVectors, MatrixOperate, lambda_scale

    integer :: i_ev, n_ev
    real(dp), dimension(n_ev) :: lambda_i
    type(dirac_fermion), dimension(:,:) :: v_i, Dv_i
    real(dp)  :: tolerance
    integer :: iterations
    logical, intent(in) :: InitVectors
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         implicit none
         type(dirac_fermion), dimension(:) :: phi, Dphi
       end subroutine MatrixOperate
    end interface
    real(dp) :: lambda_scale

    ! begin local_vars
    real(dp) :: mu_psi !the ritz functional
    real(dp) :: normsq_grad_mu, normsq_grad_mu_old, normsq_grad_mu_0, norm_search, norm_psi
    real(dp) :: alpha, beta, delta, a1, a2, a3, cos_delta, sin_delta, cos_theta, sin_theta

    type(dirac_fermion), dimension(:), allocatable  :: psi, Dpsi, grad_mu, search, Dsearch

    complex(DC) :: psidotDpsi, searchdotDsearch, psidotsearch, grad_mudotsearch

    integer :: t_cycle
    real(dp) :: stop_cycle, recalc_tol
    logical :: RenormState, CheckState, RecalcState, RecalcTest
    integer :: RenormFrequency, CheckFrequency, LastRecalc
    integer :: i_xeo

    ! begin execution

    allocate(psi(n_xpeo))
    allocate(Dpsi(n_xpeo))
    allocate(grad_mu(n_xpeo))
    allocate(search(n_xpeo))
    allocate(Dsearch(n_xpeo))

    recalc_tol = min(1.0d-8,tolerance)


    if (InitVectors) then
       call random_fermion_field(psi)
    else
       psi(1:n_xeo) = v_i(:,i_ev+1)
    end if

    call ProjectVectorSpace(i_ev,v_i,psi)

    norm_psi = fermion_norm(psi)
    call normalise(psi,norm_psi)

    call MatrixOperate(psi, Dpsi)
    call ScaleVector_eo(Dpsi,lambda_scale)
    call ProjectVectorSpace(i_ev,v_i,Dpsi)

    mu_psi = real_inner_product(psi,Dpsi)
    do i_xeo =1,n_xeo
       grad_mu(i_xeo)%cs = Dpsi(i_xeo)%cs - mu_psi*psi(i_xeo)%cs
    end do

    normsq_grad_mu = fermion_normsq(grad_mu)
    normsq_grad_mu_0 = normsq_grad_mu

    search = grad_mu
    norm_search = fermion_norm(search)
    call MatrixOperate(search, Dsearch)
    call ScaleVector_eo(Dsearch, lambda_scale)
    call ProjectVectorSpace(i_ev,v_i,Dsearch)

    t_cycle = CGCycles + 1
    stop_cycle = tau**t_cycle
    !if ( lambda_scale /= 0.0d0 ) stop_cycle = abs(lambda_scale)*stop_cycle

    CheckFrequency = min(CGIter_Max/10, CGCycles + max(0,int(-log(normsq_grad_mu))) + max(0,20-i_ev) )
    RenormFrequency = min(50, 10 + CGCycles + max(0,int(-log(normsq_grad_mu))) + max(0,20-i_ev) )
    RecalcState = .true.

    TauCycleExit_i(i_ev+1) = .false.
    converged_i(i_ev+1) = .false.
    normg_i(i_ev+1) = .false.
    temple_i(i_ev+1) = .false.
    deltaC_i(i_ev+1) = .false.

    iterations = 0
    do
       deltaGrad_i(i_ev+1) = sqrt(normsq_grad_mu)
       if (  deltaGrad_i(i_ev+1) < tolerance) then
          converged_i(i_ev+1) = .true.
          normg_i(i_ev+1) = .true.
       end if

       if ( lambda_scale == 0.0d0 ) then
          if (  deltaGrad_i(i_ev+1) < tau*mu_psi ) then
             converged_i(i_ev+1) = .true.
          end if
       end if

       deltaTemple_i(i_ev+1) = normsq_grad_mu/(lower_i(i_ev+1) - mu_psi)

       if ( UseTemple_i(i_ev+1) .and. ( lower_i(i_ev+1) - mu_psi > 0 ) ) then
          if ( deltaTemple_i(i_ev+1) < tolerance ) then
             converged_i(i_ev+1) = .true.
             temple_i(i_ev+1) = .true.
          end if
       end if

       deltaCycle_i(i_ev+1) = deltaTau_i(i_ev+1)*(normsq_grad_mu/normsq_grad_mu_0)

       if ( (CGCycles > 2) .and. UseDeltaCycle_i(i_ev+1) ) then
          if ( deltaCycle_i(i_ev+1) < tolerance ) then
             converged_i(i_ev+1) = .true.
             deltaC_i(i_ev+1) = .true.
             TauCycleExit_i(i_ev+1) = .true.
          end if
       end if

       if (converged_i(i_ev+1)) exit

       if ( (iterations >= CGIter_Max) .and. (normsq_grad_mu < normsq_grad_mu_0) ) exit

       if ( iterations >= 3*CGIter_Max/2 ) exit

       if ( (CGCycles <= 3) .and. sqrt(abs(normsq_grad_mu)) < stop_cycle) exit

       if (CGCycles > 1 .and. iterations > 10 ) then
          if ( normsq_grad_mu/normsq_grad_mu_0 < tau) then
             TauCycleExit_i(i_ev+1) = .true.
             exit
          end if
       end if

       iterations = iterations + 1

       CheckState = .false.
       if (RecalcState) then
          StateRenormCount = StateRenormCount + 1
          CheckState = .true.
          LastRecalc = 1
       else
          LastRecalc = LastRecalc + 1
       end if
       if (LastRecalc >= CheckFrequency) then
          CheckState = .true.
          LastRecalc = 1
       end if

       RecalcState = .false.
       RenormState = (modulo(iterations,RenormFrequency)==0)

       normsq_grad_mu_old = normsq_grad_mu
       psidotDpsi = real_inner_product(psi,Dpsi)
       searchdotDsearch = real_inner_product(search,Dsearch)
       searchdotDsearch = searchdotDsearch/(norm_search**2)

       a1 = 0.5d0*(psidotDpsi + searchdotDsearch)
       a2 = 0.5d0*(psidotDpsi - searchdotDsearch)
       a3 = normsq_grad_mu/norm_search
       alpha = sqrt( a2**2 + a3**2)

       cos_delta = a2/alpha
       sin_delta = a3/alpha

       if (cos_delta <= 0.0d0 ) then
          cos_theta = sqrt(0.5*(1.0-cos_delta))
          sin_theta = -0.5*sin_delta/cos_theta
       else
          sin_theta = -sqrt(0.5*(1.0+cos_delta))
          cos_theta = -0.5*sin_delta/sin_theta
       end if

       mu_psi = psidotDpsi

       !mpiprint *, iterations, mu_psi, norm_search, checkstate, spacing(norm_search), recalc_tol

       do i_xeo =1,n_xeo
          psi(i_xeo)%cs = cos_theta*psi(i_xeo)%cs + (sin_theta/norm_search)*search(i_xeo)%cs
       end do

       do i_xeo =1,n_xeo
          Dpsi(i_xeo)%cs = cos_theta*Dpsi(i_xeo)%cs + (sin_theta/norm_search)*Dsearch(i_xeo)%cs
       end do

       if (RenormState) then
          call ProjectVectorSpace(i_ev,v_i,psi)
          norm_psi = fermion_norm(psi)
          call normalise(psi,norm_psi)

          call MatrixOperate(psi, Dpsi)
          call ScaleVector_eo(Dpsi,lambda_scale)
          call ProjectVectorSpace(i_ev,v_i,Dpsi)
       else if (CheckState) then
          call OrthogonaliseEigenspace_eo(i_ev,v_i,psi,Dv_i,Dpsi)
          call ProjectVectorSpace(i_ev,v_i,Dpsi)
          norm_psi = fermion_norm(psi)
          RecalcTest = abs(norm_psi-1.0d0) > recalc_tol
          RecalcState = RecalcState .or. RecalcTest
          if (RecalcTest) then
             call normalise(psi,norm_psi)
             call normalise(Dpsi,norm_psi)
          end if
       end if

       mu_psi = mu_psi - 2*alpha*(sin_theta**2)

       do i_xeo =1,n_xeo
          grad_mu(i_xeo)%cs = Dpsi(i_xeo)%cs - mu_psi*psi(i_xeo)%cs
       end do

       normsq_grad_mu = fermion_normsq(grad_mu)

       if (CheckState .or. RenormState) then
          grad_mudotsearch = inner_product(grad_mu,search)
          RecalcTest = (abs(grad_mudotsearch)/max(norm_search,1.0d0) > recalc_tol)
          RecalcState = RecalcState .or. RecalcTest
          if ( RenormState .or. RecalcTest ) then
             call orthogonalise(search,grad_mu,grad_mudotsearch/normsq_grad_mu)
          end if
       end if

       beta = cos_theta*(normsq_grad_mu/normsq_grad_mu_old)
       psidotsearch = inner_product(psi,search)

       beta = min(beta,100.0d0)

       do i_xeo =1,n_xeo
          search(i_xeo)%cs = grad_mu(i_xeo)%cs + beta*( search(i_xeo)%cs - &
               & psi(i_xeo)%cs*psidotsearch)
       end do

       if (CheckState) then
          psidotsearch = inner_product(psi,search)
          RecalcTest = abs(psidotsearch)/max(norm_search,1.0d0) > recalc_tol
          RecalcState = RecalcState .or. RecalcTest
          if (RenormState .or. RecalcTest) then
             norm_psi = fermion_normsq(psi)
             call orthogonalise(search,psi,psidotsearch/norm_psi)
          end if
       end if

       norm_search = fermion_norm(search)

       if (CheckState) then
          !RecalcTest =  spacing(norm_search**2) > recalc_tol
          RecalcTest =  1.0d0/(norm_search**2) < recalc_tol
          RecalcState = RecalcState .or. RecalcTest
          if ( RecalcTest ) then
             !   search = grad_mu
             call normalise(search,norm_search)
             psidotsearch = inner_product(psi,search)
             grad_mudotsearch = inner_product(grad_mu,search)/normsq_grad_mu - 1.0d0
             !search = search - psidotsearch*psi - searchdotgrad_mu*grad_mu/normsq_grad_mu
             do i_xeo =1,n_xeo
                search(i_xeo)%cs = search(i_xeo)%cs - grad_mu(i_xeo)%cs*grad_mudotsearch - &
                     & psi(i_xeo)%cs*psidotsearch
             end do
          end if
       end if

       call MatrixOperate(search, Dsearch)
       call ScaleVector_eo(Dsearch,lambda_scale)
       if (CheckState) then
          call ProjectVectorSpace(i_ev,v_i,Dsearch)
       end if

    end do

    call MatrixOperate(psi, Dpsi)
    call ScaleVector_eo(Dpsi,lambda_scale)
    call ProjectVectorSpace(i_ev,v_i,Dpsi)

    v_i(:,i_ev+1) = psi(1:n_xeo)
    Dv_i(:,i_ev+1) = Dpsi(1:n_xeo)
    lambda_i(i_ev+1) = mu_psi

    deallocate(psi)
    deallocate(Dpsi)
    deallocate(grad_mu)
    deallocate(search)
    deallocate(Dsearch)

  end subroutine ConjGradEigenMode_eo

  subroutine OrthogonaliseEigenspace_eo(n_v,v_i,phi,Dv_i,Dphi)
    ! begin args: n_v, v_i, phi, Dv_i, Dphi

    integer, intent(in) :: n_v
    type(dirac_fermion), dimension(:) :: phi, Dphi
    type(dirac_fermion), dimension(:,:) :: v_i,Dv_i

    ! begin local_vars
    complex(dc) :: v_idotphi
    integer :: i_xeo,i_v

    ! begin execution


    do i_v = 1,n_v
       v_idotphi = inner_product(v_i(:,i_v),phi)
       do i_xeo =1,n_xeo
          phi(i_xeo)%cs = phi(i_xeo)%cs - v_i(i_xeo,i_v)%cs*v_idotphi
          Dphi(i_xeo)%cs = Dphi(i_xeo)%cs - Dv_i(i_xeo,i_v)%cs*v_idotphi
       end do
    end do


  end subroutine OrthogonaliseEigenspace_eo

  subroutine ScaleVector_eo(Dphi, lambda_scale)
    ! begin args: Dphi, lambda_scale

    type(dirac_fermion), dimension(:) :: Dphi
    real(dp) :: lambda_scale
    ! begin local_vars
    integer :: i_xeo

    ! begin execution


    if ( (lambda_scale /= 1.0d0) .and. (lambda_scale /= 0.0d0) ) then
       do i_xeo =1,n_xeo
          Dphi(i_xeo)%cs = lambda_scale * Dphi(i_xeo)%cs
       end do
    end if


  end subroutine ScaleVector_eo

  subroutine DiagonaliseBasis_eo(n_ev, lambda_i, v_i, Dv_i, vprime_i, MatrixOperate, lambda_scale, renormalise )
    ! begin args: n_ev, lambda_i, v_i, Dv_i, vprime_i, MatrixOperate, lambda_scale, renormalise

    !Given a set of eigenvectors vprime_i of one operator (eg. H^2), diagonalise another operator (eg. H)
    !on the span of vprime_i, and return the new eigenvectors v_i, and also their derivatives Dv_i.
    !The eigenvalues/Ritz function values are returned in lambda_i
    !The vprime_i are renormalised and orthogonalised if renormalise == .true.

    integer, intent(in) :: n_ev !number of eigenvectors to calculate
    real(dp), dimension(n_ev) :: lambda_i !eigenvalues
    type(dirac_fermion), dimension(:,:) :: v_i, Dv_i !new eigenvectors
    type(dirac_fermion), dimension(:,:) :: vprime_i  !old eigenvectors
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         implicit none
         type(dirac_fermion), dimension(:) :: phi, Dphi
       end subroutine MatrixOperate
    end interface
    real(dp) :: lambda_scale
    logical :: renormalise

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable  :: phi, Dphi
    complex(DC), dimension(n_ev,n_ev) :: M_kl !Sub-matrix to be diagonalised
    complex(DC), dimension(n_ev,n_ev) :: V_kl !Matrix of sub-eigenvectors

    real(dp) :: swap, norm_v, norm_phi
    real(dp), dimension(n_ev) :: swap_l
    integer :: k_ev, l_ev, sweeps
    integer :: i_xeo

    ! begin execution

    allocate(phi(n_xpeo))
    allocate(Dphi(n_xpeo))

    do k_ev=1,n_ev
       phi(1:n_xeo) = vprime_i(:,k_ev)
       if ( renormalise ) then
          call ProjectVectorSpace(k_ev-1,v_i,phi)
          norm_phi = fermion_norm(phi)
          call normalise(phi,norm_phi)
       end if

       call MatrixOperate(phi,Dphi)
       call ScaleVector_eo(Dphi,lambda_scale)

       v_i(:,k_ev) = phi(1:n_xeo)
       Dv_i(:,k_ev) = Dphi(1:n_xeo)
    end do

    do k_ev=1,n_ev
       M_kl(k_ev,k_ev) = real_inner_product(v_i(:,k_ev),Dv_i(:,k_ev))
       do l_ev=k_ev+1,n_ev
           M_kl(k_ev,l_ev) = inner_product(v_i(:,k_ev),Dv_i(:,l_ev))
           M_kl(l_ev,k_ev) = conjg(M_kl(k_ev,l_ev))
       end do
    end do

    !Sort sub-eigenvectors into increasing eigenvalue order
    do k_ev=1,n_ev
       do l_ev=k_ev+1,n_ev
          if ( real(M_kl(l_ev,l_ev)) < real(M_kl(k_ev,k_ev)) ) then
             swap = M_kl(k_ev,k_ev)
             swap_l = V_kl(:,k_ev)
             M_kl(k_ev,k_ev) = M_kl(l_ev,l_ev)
             V_kl(:,k_ev) = V_kl(:,l_ev)
             M_kl(l_ev,l_ev) = swap
             V_kl(:,l_ev) = swap_l
          end if
       end do
    end do

    !Perform intermediate diagonalisation
    call ComplexDiag(n_ev, M_kl, V_kl, sweeps, JacobiPrecision)

    !Normalise sub-eigenvectors
    do l_ev=1,n_ev
       V_kl(:,l_ev) = V_kl(:,l_ev)/sqrt(sum(abs(V_kl(:,l_ev))**2 ))
    end do

    !Rotate eigenvectors and their derivatives
    vprime_i = Dv_i(:,1:n_ev)
    Dv_i(:,1:n_ev) = zero_dirac_fermion

    do l_ev=1,n_ev; do k_ev=1,n_ev
       do i_xeo =1,n_xeo
          Dv_i(i_xeo,l_ev)%cs =  Dv_i(i_xeo,l_ev)%cs + &
               & V_kl(k_ev,l_ev)*vprime_i(i_xeo,k_ev)%cs
       end do
    end do; end do

    vprime_i = v_i(:,1:n_ev)
    v_i(:,1:n_ev) = zero_dirac_fermion

    do l_ev=1,n_ev; do k_ev=1,n_ev
       do i_xeo =1,n_xeo
          v_i(i_xeo,l_ev)%cs =  v_i(i_xeo,l_ev)%cs + &
               & V_kl(k_ev,l_ev)*vprime_i(i_xeo,k_ev)%cs
       end do
    end do; end do


    do k_ev=1,n_ev
       !lambda_i(k_ev) = lambda_scale*real_inner_product(v_i(:,k_ev),Dv_i(:,k_ev))
       lambda_i(k_ev) = real_inner_product(v_i(:,k_ev),Dv_i(:,k_ev))
    end do

    deallocate(phi)
    deallocate(Dphi)

  end subroutine DiagonaliseBasis_eo

end module AccMinEVCG


