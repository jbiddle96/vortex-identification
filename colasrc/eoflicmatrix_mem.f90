module eoflicMatrix_Mem

  use Timer
  use MPIInterface
  use FermionField
  use FermionAlgebra
  use MatrixAlgebra

  use GaussJordan

  use SpinorTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  use ColourFieldOps
  implicit none

  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: Up_xd
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: Um_xd



  type(colour_matrix), dimension(:,:,:,:,:,:), allocatable, public :: InvSigmaFp, InvSigmaFm


  integer :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp) :: maxtime, mintime, meantime
  real(dp) :: kappa_eoflic

  real(dp) :: c_sw_eoflic

  real(dp) :: commmaxtime, commmintime, commmeantime
  real(dp) :: wastemax, wastemin, wastemean

  real(dp) :: bcx_eoflic = 1.0d0, bcy_eoflic = 1.0d0, bcz_eoflic = 1.0d0, bct_eoflic = 1.0d0

contains
  subroutine InitialiseeoflicOperator(U_xd, Usm_xd, kappa, c_sw, u0, u0_sm)

    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd, Usm_xd
    real(dp) :: kappa, c_sw
    real(dp), optional :: u0, u0_sm

    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfic_sw, mfir, mfir_sm !coefficents to be absorbed.

    type(colour_matrix) :: U_mux, Usm_mux
    type(colour_matrix) :: Up_x, Um_x


    mfic_sw = -kappa*c_sw

    if ( present(u0_sm) ) mfic_sw = mfic_sw/(u0_sm**4)
    call CalcCloverInverse(Usm_xd,mfic_sw)

    !EO preconditioning is in the hopping parameter formalism.
    mfir = kappa
    if ( present(u0) ) mfir = mfir/u0

    mfir_sm = kappa
    if ( present(u0_sm) ) mfir_sm = mfir_sm/u0_sm

    kappa_eoflic = kappa

    !Absorb the mean field improvement into the gauge fields.

    !Additional factor of a half due to split link trick.
    mfir = 0.5d0*mfir
    mfir_sm = 0.5d0*mfir_sm


    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          U_mux%Cl = mfir*U_xd(ix,iy,iz,it,mu)%Cl
          Usm_mux%Cl = mfir_sm*Usm_xd(ix,iy,iz,it,mu)%Cl

          ! Intermediate step for memory reuse (if Up_xd = U_xd, Um_xd = Usm_xd)
          Up_x%Cl = U_mux%Cl + Usm_mux%Cl
          Um_x%Cl = U_mux%Cl - Usm_mux%Cl

          Up_xd(ix,iy,iz,it,mu)%Cl = Up_x%Cl
          Um_xd(ix,iy,iz,it,mu)%Cl = Um_x%Cl

       end do; end do; end do; end do
    end do

    call SetBoundaryConditions(Up_xd,bcx_eoflic, bcy_eoflic, bcz_eoflic, bct_eoflic)
    call ShadowGaugeField(Up_xd,0)

    call SetBoundaryConditions(Um_xd,bcx_eoflic, bcy_eoflic, bcz_eoflic, bct_eoflic)
    call ShadowGaugeField(Um_xd,0)

  end subroutine InitialiseeoflicOperator

  subroutine InvLeftILUMatrix_eoflic(psi,psi_pr)

    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(colour_vector), dimension(:,:,:,:,:) :: psi_pr

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Dpsi_o

    allocate(Dpsi_o(nxp,nyp,nzp,ntp,ns))

    call EOCloverInv(psi,psi_pr,0)

    call EOWilson_Split(psi_pr,Dpsi_o,1)

    call PsiMinusPhi(psi_pr,Dpsi_o)

    Dpsi_o = psi_pr
    call EOCloverInv(Dpsi_o,psi_pr,1)

    deallocate(Dpsi_o)

  end subroutine InvLeftILUMatrix_eoflic

  subroutine InvRightILUMatrix_eoflic(psi,psi_pr)

    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(colour_vector), dimension(:,:,:,:,:) :: psi_pr

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Dpsi_o

    allocate(Dpsi_o(nxp,nyp,nzp,ntp,ns))

    call EOWilson_Split(psi,Dpsi_o,0)

    psi_pr = Dpsi_o
    call EOCloverInv(psi_pr,Dpsi_o,0)

    psi_pr = psi
    call PsiMinusPhi(psi_pr,Dpsi_o)

    deallocate(Dpsi_o)

  end subroutine InvRightILUMatrix_eoflic



  subroutine Deoflic(phi,Dphi)

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi_e,phi_o

    allocate(phi_e(nxp,nyp,nzp,ntp,ns))
    allocate(phi_o(nxp,nyp,nzp,ntp,ns))

    call EOWilson_Split(phi,phi_e,0)

    call EOCloverInv(phi_e,phi_o,0)

    call EOWilson_Split(phi_o,phi_e,1)

    call EOCloverInv(phi_e,phi_o,1)

    Dphi = phi

    call PsiMinusPhi(Dphi,phi_o)

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine Deoflic

  subroutine Deoflic_dag(phi,Dphi)

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi_e, phi_o
    integer :: ix,iy,iz,it,is

    allocate(phi_e(nxp,nyp,nzp,ntp,ns))
    allocate(phi_o(nxp,nyp,nzp,ntp,ns))

    call EOCloverInv(phi,phi_o,1)

    call EOWilsonDag_Split(phi_o,phi_e,1)

    call EOCloverInv(phi_e,phi_o,0)

    call EOWilsonDag_Split(phi_o,phi_e,0)

    Dphi = phi

    call PsiMinusPhi(Dphi,phi_o)

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine Deoflic_dag

  subroutine Hsqeoflic(phi,Hsqphi)

    type(colour_vector), dimension(:,:,:,:,:) :: phi, Hsqphi

    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Dphi

    allocate(Dphi(nxp,nyp,nzp,ntp,ns))

    call Heoflic(phi,Dphi)
    call Heoflic(Dphi,Hsqphi)

    deallocate(Dphi)

  end subroutine Hsqeoflic

  subroutine Heoflic(phi,Hphi)

    type(colour_vector), dimension(:,:,:,:,:) :: phi, Hphi
    integer :: ix,iy,iz,it

    call Deoflic(phi,Hphi)


    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       Hphi(ix,iy,iz,it,3)%Cl = -Hphi(ix,iy,iz,it,3)%Cl
       Hphi(ix,iy,iz,it,4)%Cl = -Hphi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do
  end subroutine Heoflic

  subroutine EOWilsonDag_Split(phi,Dphi,op_parity)

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    integer :: op_parity
    integer :: ix,iy,iz,it


    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       phi(ix,iy,iz,it,3)%Cl = -phi(ix,iy,iz,it,3)%Cl
       phi(ix,iy,iz,it,4)%Cl = -phi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do
    call EOWilson_Split(phi,Dphi,1-op_parity)


    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       phi(ix,iy,iz,it,3)%Cl = -phi(ix,iy,iz,it,3)%Cl
       phi(ix,iy,iz,it,4)%Cl = -phi(ix,iy,iz,it,4)%Cl

       Dphi(ix,iy,iz,it,3)%Cl = -Dphi(ix,iy,iz,it,3)%Cl
       Dphi(ix,iy,iz,it,4)%Cl = -Dphi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do
  end subroutine EOWilsonDag_Split

  subroutine EOWilson_Split(phi, Dphi, op_parity)

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    integer :: op_parity
    ! op_parity = 0 => takes odd sites to even sites.
    ! op_parity = 1 => takes even sites to odd sites.

    integer :: ix,iy,iz,it,is
    integer :: jx,jy,jz,jt,js
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    if (timing) intime = mpi_wtime()
    commtime = 0.0d0
    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0

    Dphi = zero_vector

    p_xyzt = modulo(i_nx+i_ny+i_nz+i_nt,2) ! Offset due to processor sub-lattice

    !mu = 1




    !G_1^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; ix=nx; do jx=1,nx
          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = Up_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Up_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Up_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

          end if
          ix = jx
       end do; end do; end do; end do
    end do




    !G_1^+ Um phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; ix=nx; do jx=1,nx
          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = Um_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Um_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Um_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

          end if
          ix = jx
       end do; end do; end do; end do
    end do







    !mu = 2




    !G_2^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; iy=ny; do jy=1,ny; do ix=1,nx
          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) + pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) + pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) + pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

             psi_x%Cl(1) = Up_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Up_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Up_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*psi_x%Cl(3)

          end if
       end do; iy=jy; end do; end do; end do
    end do




    !G_2^+ Um phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; iy=ny; do jy=1,ny; do ix=1,nx
          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) - pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) - pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) - pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

             psi_x%Cl(1) = Um_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Um_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Um_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*psi_x%Cl(3)

          end if
       end do; iy=jy; end do; end do; end do
    end do
    !mu = 3




    !G_3^- Up phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; do iz=1,nz; jz=mapz(iz+1); do iy=1,ny; do ix=1,nx


          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = Up_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Up_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Up_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

          end if
       end do; end do; end do; end do
    end do




    !G_3^+ Um phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; do iz=1,nz; jz=mapz(iz+1); do iy=1,ny; do ix=1,nx


          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = Um_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Um_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Um_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

          end if
       end do; end do; end do; end do
    end do
    !mu = 4




    !G_4^- Up phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jt=mapt(it+1); do iz=1,nz; do iy=1,ny; do ix=1,nx


          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then


             Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) - phi(ix,iy,iz,jt,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) - phi(ix,iy,iz,jt,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) - phi(ix,iy,iz,jt,js)%Cl(3)
             psi_x%Cl(1) = Up_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Up_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Up_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)


             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + psi_x%Cl(3)


          end if
       end do; end do; end do; end do
    end do




    !G_4^+ Um phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jt=mapt(it+1); do iz=1,nz; do iy=1,ny; do ix=1,nx


          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then


             Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) + phi(ix,iy,iz,jt,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) + phi(ix,iy,iz,jt,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) + phi(ix,iy,iz,jt,js)%Cl(3)
             psi_x%Cl(1) = Um_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Um_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Um_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Um_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Um_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x%Cl(3)


             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + psi_x%Cl(3)


          end if
       end do; end do; end do; end do
    end do
    !mu = 1




    !G_1^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jx=nx; do ix=1,nx

          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

          end if
          jx = ix

       end do; end do; end do; end do
    end do




    !G_1^- Um phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jx=nx; do ix=1,nx

          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

          end if
          jx = ix

       end do; end do; end do; end do
    end do







    !mu = 2




    !G_2^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; jy=ny; do iy=1,ny; do ix=1,nx

          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) - pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) - pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) - pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

             psi_x%Cl(1) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*psi_x%Cl(3)

          end if
       end do; jy=iy; end do; end do; end do
    end do




    !G_2^- Um phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; jy=ny; do iy=1,ny; do ix=1,nx

          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) + pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) + pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) + pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

             psi_x%Cl(1) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*psi_x%Cl(3)

          end if
       end do; jy=iy; end do; end do; end do
    end do
    !mu = 3




    !G_3^+ Up phi_xmmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jz=nz; do iz=1,nz; do iy=1,ny; do ix=1,nx


          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

          end if
       end do; end do; jz=iz; end do; end do
    end do




    !G_3^- Um phi_xmmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jz=nz; do iz=1,nz; do iy=1,ny; do ix=1,nx


          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then

             Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

          end if
       end do; end do; jz=iz; end do; end do
    end do
    !mu = 4




    !G_4^+ Up phi_xmmu

    do is=1,nsp
       js = is+2



       jt=nt

       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then


             Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) + phi(ix,iy,iz,jt,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) + phi(ix,iy,iz,jt,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) + phi(ix,iy,iz,jt,js)%Cl(3)
             psi_x%Cl(1) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)


             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - psi_x%Cl(3)


          end if
       end do; end do; end do; jt=it; end do
    end do




    !G_4^- Um phi_xmmu

    do is=1,nsp
       js = is+2



       jt=nt

       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then


             Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) - phi(ix,iy,iz,jt,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) - phi(ix,iy,iz,jt,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) - phi(ix,iy,iz,jt,js)%Cl(3)
             psi_x%Cl(1) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x%Cl(3)


             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - psi_x%Cl(3)


          end if
       end do; end do; end do; jt=it; end do
    end do







    if (timing) then
       outtime = mpi_wtime()
       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    end if

    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
    waste = commtime/(outtime - intime)
    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)


  end subroutine EOWilson_Split

  subroutine EOCloverInv(phi, Dphi, op_parity)

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    integer :: op_parity
    ! op_parity = 0 => applies (1+T_ee)^{-1}
    ! op_parity = 1 => applies (1+T_oo)^{-1}

    integer :: ix,iy,iz,it,is,ic,js,jc
    integer :: site_parity, p_xyzt

    p_xyzt = modulo(i_nx+i_ny+i_nz+i_nt,2) ! Offset due to processor sub-lattice

    Dphi = zero_vector

    do js=1,nsp; do is=1,nsp
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity == op_parity ) then
             do jc=1,nc; do ic=1,nc
                Dphi(ix,iy,iz,it,is)%Cl(ic) = Dphi(ix,iy,iz,it,is)%Cl(ic) + InvSigmaFp(ix,iy,iz,it,is,js)%Cl(ic,jc)*phi(ix,iy,iz,it,js)%Cl(jc)
                Dphi(ix,iy,iz,it,is+2)%Cl(ic) = Dphi(ix,iy,iz,it,is+2)%Cl(ic) + InvSigmaFm(ix,iy,iz,it,is,js)%Cl(ic,jc)*phi(ix,iy,iz,it,js+2)%Cl(jc)
             end do; end do
          end if
       end do; end do; end do; end do
    end do; end do

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
          site_parity = modulo(ix+iy+iz+it+p_xyzt,2)
          if ( site_parity /= op_parity ) Dphi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl
       end do; end do; end do; end do
    end do

  end subroutine EOCloverInv

  subroutine DefineSpinField(delta_ns, gamma_mu, gamma_5, sigma_munu, chiral_basis)

    complex(dc), dimension(ns,ns) :: delta_ns, gamma_5
    complex(dc), dimension(nd,ns,ns) :: gamma_mu ! Dirac matrices
    complex(dc), dimension(nd,nd,ns,ns) :: sigma_munu
    logical, optional :: chiral_basis
    integer :: mu, nu, is

    complex(dc) :: J = (1.0d0,0.0d0)
    complex(dc), parameter :: O = (0.0d0,0.0d0)

    delta_ns =0
    do is = 1, ns
       delta_ns(is,is)=1.0d0
    end do

    !Define the Gamma matrices(Non-relativistic or Sakurai notation)
    gamma_mu(1,:,:) = RESHAPE( (/O,O,O,I,O,O,I,O,O,-I,O,O,-I,O,O,O/), (/4,4/) )
    gamma_mu(2,:,:) = RESHAPE( (/O,O,O,-J,O,O,J,O,O,J,O,O,-J,O,O,O/), (/4,4/) )
    gamma_mu(3,:,:) = RESHAPE( (/O,O,I,O,O,O,O,-I,-I,O,O,O,O,I,O,O/), (/4,4/) )
    if (present(chiral_basis)) then
       if (chiral_basis) then
          gamma_mu(4,:,:) = RESHAPE( (/O,O,J,O,O,O,O,J,J,O,O,O,O,J,O,O/), (/4,4/) )
          gamma_5(:,:) = RESHAPE( (/J,O,O,O,O,J,O,O,O,O,-J,O,O,O,O,-J/), (/4,4/) )
       else
          gamma_mu(4,:,:) = RESHAPE( (/J,O,O,O,O,J,O,O,O,O,-J,O,O,O,O,-J/), (/4,4/) )
          gamma_5(:,:) = RESHAPE( (/O,O,-J,O,O,O,O,-J,-J,O,O,O,O,-J,O,O/), (/4,4/) )
       end if
    else
       !Sakurai basis
       gamma_mu(4,:,:) = RESHAPE( (/J,O,O,O,O,J,O,O,O,O,-J,O,O,O,O,-J/), (/4,4/) )
       gamma_5(:,:) = RESHAPE( (/O,O,-J,O,O,O,O,-J,-J,O,O,O,O,-J,O,O/), (/4,4/) )
    end if

    !Define Sigma_\mu\nu=(1/4)[Gamma_\mu, Gamma_\nu]
    do mu=1,nd; do nu=1,nd
       sigma_munu(mu,nu,:,:) = 0.5d0 * MatMul(gamma_mu(mu,:,:), gamma_mu(nu,:,:)) - &
            & 0.5d0 * MatMul(gamma_mu(nu,:,:), gamma_mu(mu,:,:))
    end do; end do

  end subroutine DefineSpinField

  subroutine CalcCloverInverse(U_xd,mfic_sw)

    type(colour_matrix) :: F_munux
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: ix,iy,iz,it,is,js,ic,jc
    integer :: mu,nu,ip
    real(dp) :: kappa,c_sw,u0

    complex(dc), dimension(6,6) :: sFp, sFm, InvsFp, InvsFm

    type(colour_matrix), dimension(nsp,nsp) :: SigmaFp, SigmaFm
    complex(dc), dimension(ns,ns) :: delta_ns, gamma_5
    complex(dc), dimension(nd,ns,ns) :: gamma_mu ! Dirac matrices
    complex(dc), dimension(nd,nd,ns,ns) :: sigma_munu

    real(dp) :: mfic_sw


    call DefineSpinField(delta_ns, Gamma_mu, gamma_5, Sigma_munu, chiral_basis = .true. )

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       SigmaFp = zero_matrix
       SigmaFm = zero_matrix

       do mu=1,nd
          do nu=mu+1,nd
             call GetCloverTerm_xdd(F_munux,U_xd,ix,iy,iz,it,mu,nu)

             do js=1,2; do is=1,2
                do jc=1,nc; do ic=1,nc
                   SigmaFp(is,js)%Cl(ic,jc) = SigmaFp(is,js)%Cl(ic,jc) + mfic_sw*Sigma_munu(mu,nu,is,js)*F_munux%Cl(ic,jc)
                   SigmaFm(is,js)%Cl(ic,jc) = SigmaFm(is,js)%Cl(ic,jc) + mfic_sw*Sigma_munu(mu,nu,is+2,js+2)*F_munux%Cl(ic,jc)
                end do; end do
             end do; end do
          end do
       end do

       sFp = 0.0d0
       sFm = 0.0d0
       do js=1,2; do is=1,2
          do jc=1,nc; do ic=1,nc
             sFp(ic + (is-1)*nc,jc + (js-1)*nc) = SigmaFp(is,js)%Cl(ic,jc)
             sFm(ic + (is-1)*nc,jc + (js-1)*nc) = SigmaFm(is,js)%Cl(ic,jc)
          end do; end do
       end do; end do
       do is=1,6
          sFp(is,is) = 1.0d0 + sFp(is,is)
          sFm(is,is) = 1.0d0 + sFm(is,is)
       end do
       call GaussJordanInv(6,sFp,InvsFp)
       call GaussJordanInv(6,sFm,InvsFm)
       do js=1,2; do is=1,2
          do jc=1,nc; do ic=1,nc
             InvSigmaFp(ix,iy,iz,it,is,js)%Cl(ic,jc) = InvsFp(ic + (is-1)*nc,jc + (js-1)*nc)
             InvSigmaFm(ix,iy,iz,it,is,js)%Cl(ic,jc) = InvsFm(ic + (is-1)*nc,jc + (js-1)*nc)
          end do; end do
       end do; end do

    end do; end do; end do; end do

  end subroutine CalcCloverInverse

  subroutine GetCloverTerm_xdd(F_munux,U_xd,ix,iy,iz,it,mu,nu)

    type(colour_matrix) :: F_munux 
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: ix,iy,iz,it
    integer :: mu,nu

    type(colour_matrix) :: U_munu, UmuUnu, UmudagUnudag, UnuUmudag, UnudagUmu !Gauge field products

    integer, dimension(nd) :: dmu, dnu
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt
    integer :: ax,ay,az,at
    integer :: bx,by,bz,bt
    integer :: cx,cy,cz,ct
    integer :: dx,dy,dz,dt
    integer :: ex,ey,ez,et

#define U_mux U_xd(ix,iy,iz,it,mu)
#define U_nux U_xd(ix,iy,iz,it,nu)

#define U_nuxpmu U_xd(jx,jy,jz,jt,nu)
#define U_muxpnu U_xd(kx,ky,kz,kt,mu)

#define U_muxmmu U_xd(ax,ay,az,at,mu)
#define U_nuxmmu U_xd(ax,ay,az,at,nu)

#define U_muxmnu U_xd(bx,by,bz,bt,mu)
#define U_nuxmnu U_xd(bx,by,bz,bt,nu)

#define U_nuxmmumnu U_xd(cx,cy,cz,ct,nu)
#define U_muxmmumnu U_xd(cx,cy,cz,ct,mu)

#define U_nuxmnupmu U_xd(dx,dy,dz,dt,nu)
#define U_muxpnummu U_xd(ex,ey,ez,et,mu)

    dmu = 0
    dnu = 0
    dmu(mu) = 1
    dnu(nu) = 1

    ! t indices
    jt = mapt(it + dmu(4))
    kt = mapt(it + dnu(4))
    at = mapt(it - dmu(4))
    bt = mapt(it - dnu(4))
    ct = mapt(it - dmu(4) - dnu(4))
    dt = mapt(it + dmu(4) - dnu(4))
    et = mapt(it - dmu(4) + dnu(4))

    ! z indices
    jz = mapz(iz + dmu(3))
    kz = mapz(iz + dnu(3))
    az = mapz(iz - dmu(3))
    bz = mapz(iz - dnu(3))
    cz = mapz(iz - dmu(3) - dnu(3))
    dz = mapz(iz + dmu(3) - dnu(3))
    ez = mapz(iz - dmu(3) + dnu(3))

    ! y indices
    jy = mapy(iy + dmu(2))
    ky = mapy(iy + dnu(2))
    ay = mapy(iy - dmu(2))
    by = mapy(iy - dnu(2))
    cy = mapy(iy - dmu(2) - dnu(2))
    dy = mapy(iy + dmu(2) - dnu(2))
    ey = mapy(iy - dmu(2) + dnu(2))

    ! x indices
    jx = mapx(ix + dmu(1))
    kx = mapx(ix + dnu(1))
    ax = mapx(ix - dmu(1))
    bx = mapx(ix - dnu(1))
    cx = mapx(ix - dmu(1) - dnu(1))
    dx = mapx(ix + dmu(1) - dnu(1))
    ex = mapx(ix - dmu(1) + dnu(1))

    F_munux = zero_matrix

    !U+mu+nu(x)
    call MultiplyMatMat(UmuUnu,U_mux,U_nuxpmu)
    call MultiplyMatdagMatdag(UmudagUnudag,U_muxpnu,U_nux)
    call MultiplyMatMat(U_munu,UmuUnu,UmudagUnudag)

    call AddCloverLeaf(F_munux,U_munu)

    !U-nu+mu(x+a_nu)
    call MultiplyMatdagMat(UnudagUmu,U_nuxmnu,U_muxmnu)
    call MultiplyMatMatdag(UnuUmudag,U_nuxmnupmu,U_mux)
    call MultiplyMatMat(U_munu,UnudagUmu,UnuUmudag)

    call AddCloverLeaf(F_munux,U_munu)

    !U+nu-mu(x+a_mu)
    call MultiplyMatMatdag(UnuUmudag,U_nux,U_muxpnummu)
    call MultiplyMatdagMat(UnudagUmu,U_nuxmmu,U_muxmmu)
    call MultiplyMatMat(U_munu,UnuUmudag,UnudagUmu)

    call AddCloverLeaf(F_munux,U_munu)

    !U-mu-nu(x+a_mu+a_nu)
    call MultiplyMatdagMatdag(UmudagUnudag,U_muxmmu,U_nuxmmumnu)
    call MultiplyMatMat(UmuUnu,U_muxmmumnu,U_nuxmnu)
    call MultiplyMatMat(U_munu,UmudagUnudag,UmuUnu)

    call AddCloverLeaf(F_munux,U_munu)

    F_munux%Cl = 0.125d0*F_munux%Cl

#undef U_mux
#undef U_nux
#undef U_nuxpmu
#undef U_muxpnu

#undef F_munux

  contains

    subroutine AddCloverLeaf(F_munux,U_munux)

      type(colour_matrix) :: F_munux, U_munux

      F_munux%Cl(1,1) = F_munux%Cl(1,1) + U_munux%Cl(1,1) - conjg(U_munux%Cl(1,1))
      F_munux%Cl(2,1) = F_munux%Cl(2,1) + U_munux%Cl(2,1) - conjg(U_munux%Cl(1,2))
      F_munux%Cl(3,1) = F_munux%Cl(3,1) + U_munux%Cl(3,1) - conjg(U_munux%Cl(1,3))
      F_munux%Cl(1,2) = F_munux%Cl(1,2) + U_munux%Cl(1,2) - conjg(U_munux%Cl(2,1))
      F_munux%Cl(2,2) = F_munux%Cl(2,2) + U_munux%Cl(2,2) - conjg(U_munux%Cl(2,2))
      F_munux%Cl(3,2) = F_munux%Cl(3,2) + U_munux%Cl(3,2) - conjg(U_munux%Cl(2,3))
      F_munux%Cl(1,3) = F_munux%Cl(1,3) + U_munux%Cl(1,3) - conjg(U_munux%Cl(3,1))
      F_munux%Cl(2,3) = F_munux%Cl(2,3) + U_munux%Cl(2,3) - conjg(U_munux%Cl(3,2))
      F_munux%Cl(3,3) = F_munux%Cl(3,3) + U_munux%Cl(3,3) - conjg(U_munux%Cl(3,3))

    end subroutine AddCloverLeaf

  end subroutine GetCloverTerm_xdd

end module eoflicMatrix_Mem
