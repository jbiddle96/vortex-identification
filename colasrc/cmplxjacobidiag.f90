!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

#include "defines.h"

module CmplxJacobiDiag

  use Kinds
  implicit none
  private

  real(DP), public :: JacobiPrecision = epsilon(1.0d0)
  !Use Jacobi Precision as the precision_factor argument to the routines below
  !Recommended value range: not greater than 1.0d0 and not less than 1.0d-16
  !the smaller the value the more precise the routine

  public :: ComplexDiag
  public :: JacobiDiag

contains

  subroutine ComplexDiag(ndim, M_ij, V_ij, sweeps, precision_factor)
    ! begin args: ndim, M_ij, V_ij, sweeps, precision_factor

    integer, intent(in) :: ndim !Matrix dimensions
    complex(DC), dimension(ndim, ndim), intent(inout) :: M_ij !Matrix to be diagonalised
    complex(DC), dimension(ndim, ndim), intent(inout) :: V_ij !Matrix of eigenvectors
    integer, intent(out) :: sweeps
    real(DP), intent(in)  :: precision_factor

    ! begin local_vars
    real(DP), dimension(ndim, ndim) :: ReM, ImM !Matrix to be diagonalised
    real(DP), dimension(ndim, ndim) :: ReV, ImV !Matrix of eigenvectors

    ! begin execution

    ReM = real(M_ij)
    ImM = aimag(M_ij)

    call JacobiDiag(ndim, ReM, ImM, ReV, ImV, sweeps, precision_factor)

    M_ij = cmplx(ReM, ImM, dc)
    V_ij = cmplx(ReV, ImV, dc)


  end subroutine ComplexDiag

  subroutine JacobiDiag(ndim, ReM, ImM, ReV, ImV, sweeps, precision_factor)
    ! begin args: ndim, ReM, ImM, ReV, ImV, sweeps, precision_factor

    integer, intent(in) :: ndim !Matrix dimensions
    real(DP), dimension(ndim, ndim), intent(inout) :: ReM, ImM !Matrix to be diagonalised
    real(DP), dimension(ndim, ndim), intent(inout) :: ReV, ImV !Matrix of eigenvectors
    integer , intent(out) :: sweeps
    real(DP), intent(in)  :: precision_factor

    !Upon exit, ReM and ImM contain the real and imaginary parts of the eigenvalues as
    !their diagonal elements, ReV and ImV contain the real and imaginary parts of the
    !eigenvectors, and sweeps contains the number of Jacobi sweeps needed

    !Reference: Wilkinson & Reinsch, Handbook for Automatic Computation
    ! v. 2 Linear Algebra, II/17, by P.J. Eberlein

    ! begin local_vars
    real(DP) :: eps, tau
    real(DP) :: ImH, ReH, ModH, G
    real(DP) :: ReB, ImB, ReE, ImE, ReD, ImD
    real(DP) :: isw, c, s, d, de, b, e, nd, root, root1, root2, eta
    real(DP) :: sig, sa, ca, sx, cx, cotx, cot2x, sin2a, cos2a, cb, ch, sb, sh
    real(DP) :: htan, nc, c1r, s1r, c2r, s2r, c1i, s1i, c2i, s2i
    real(DP) :: tik, tim, uik, uim, aki, ami, aik, aim, zik, zki, zim, zmi
    integer  :: i,j,k,m
    logical  :: mark
    logical, dimension(ndim,ndim) :: OffDiag !sum mask

    ! begin execution

    eps = spacing(precision_factor)
    mark = .false.

    !set V=identity matrix, set OffDiag mask
    ReV=0.0d0
    ImV=0.0d0
    OffDiag = .true.
    do i=1,ndim
       ReV(i,i) = 1.0d0
       OffDiag(i,i) = .false.
    end do

    sweeps = 0
    do
       !calculate tau=norm of off-diagonal elements
       tau = sum(abs(ReM), mask=OffDiag)+sum(abs(ImM), mask=OffDiag)

       !print *, "Jacobi Diag", sweeps, tau**2, eps, mark
       !stopping criterion
       if (tau**2 < eps) exit
       !if (tau == 0.0d0) exit
       if (mark) exit

       !begin Jacobi sweep
       sweeps = sweeps + 1

       mark = .true.
       KLoop: do k=1,ndim-1
          MLoop: do m=k+1,ndim
             ReH = 0
             ImH = 0
             ModH = 0
             G = 0
             do i=1,ndim
                if ( (i/=k) .and. (i/=m) ) then
                   ReH = ReH + ReM(k,i)*ReM(m,i) + ImM(k,i)*ImM(m,i) &
                        & - ReM(i,k)*ReM(i,m) - ImM(i,k)*ImM(i,m)
                   ImH = ImH + ImM(k,i)*ReM(m,i) - ReM(k,i)*ImM(m,i) &
                        & - ReM(i,k)*ImM(i,m) + ImM(i,k)*ReM(i,m)
                   ModH = ModH - ReM(i,k)**2 - ImM(i,k)**2 &
                        & - ReM(m,i)**2 - ImM(m,i)**2 &
                        & + ReM(i,m)**2 + ImM(i,m)**2 &
                        & + ReM(k,i)**2 + ImM(k,i)**2
                   G = G + ReM(i,k)**2 + ImM(i,k)**2 &
                        & + ReM(m,i)**2 + ImM(m,i)**2 &
                        & + ReM(i,m)**2 + ImM(i,m)**2 &
                        & + ReM(k,i)**2 + ImM(k,i)**2
                end if
             end do
             ReB = ReM(k,m) + ReM(m,k)
             ImB = ImM(k,m) + ImM(m,k)
             ReE = ReM(k,m) - ReM(m,k)
             ImE = ImM(k,m) - ImM(m,k)
             ReD = ReM(k,k) - ReM(m,m)
             ImD = ImM(k,k) - ImM(m,m)
             if ( (ReB**2+ImE**2+ReD**2) >= (ImB**2+ReE**2+ImD**2) ) then
                isw = 1.0d0
                c = ReB
                s = ImE
                d = ReD
                de = ImD
                root2 = sqrt(ReB**2+ImE**2+ReD**2)
             else
                isw = -1.0d0
                c = ImB
                s = -ReE
                d = ImD
                de = ReD
                root2 = sqrt(ImB**2+ReE**2+ImD**2)
             end if
             root1 = sqrt(s**2 + c**2)
             sig = sign(1.0d0,d)
             sa = 0.0d0
             ca = sign(1.0d0,c)
             if (root1 < eps) then
                sx = 0.0d0
                sa = 0.0d0
                cx = 1.0d0
                ca = 1.0d0
                if (isw > 0.0d0) then
                   e = ReE
                   b = ImB
                else
                   e = ImE
                   b = -ReB
                end if
                nd = d**2 + de**2
             else
                if (abs(s) > eps) then
                   ca = c/root1
                   sa = s/root1
                end if
                cot2x = d/root1
                cotx = cot2x + sig*sqrt(1.0d0 + cot2x**2)
                sx = sig/sqrt(1.0d0 + cotx**2)
                cx = sx*cotx
                !find rotated elements
                eta = (ReE*ReB+ImB*ImE)/root1
                nd = root2**2 + ((d*de+ReB*ImB-ReE*ImE)/root2)**2
                sin2a = 2.0d0*ca*sa
                cos2a = ca**2 - sa**2
                ReH = ReH*cx**2 - (ReH*cos2a+ImH*sin2a)*sx**2 - ca*(ModH*cx*sx)
                ImH = ImH*cx**2 + (ImH*cos2a-ReH*sin2a)*sx**2 - sa*(ModH*cx*sx)
                b = isw*(sig*(-root1*de+d*(ReB*ImB-ReE*ImE)/root1)/root2)*ca + eta*sa
                e = ca*eta - isw*(sig*(-root1*de+d*(ReB*ImB-ReE*ImE)/root1)/root2)*sa
             end if
             s = ReH - sig*root2*e
             c = ImH - sig*root2*b
             root = sqrt(c**2+s**2)
             if (root < eps) then
                cb = 1.0d0
                ch = 1.0d0
                sb = 0.0d0
                sh = 0.0d0
             else
                cb = -c/root
                sb = s/root
                nc = (cb*b-e*sb)**2
                htan = root/(G+2.0d0*(nc+nd))
                ch = 1.0d0/sqrt(1.0d0 - htan**2)
                sh = ch*htan
             end if
             !prepare for transformation
             c1r = cx*ch - sx*sh*(sa*cb-sb*ca)
             c2r = cx*ch + sx*sh*(sa*cb-sb*ca)
             c1i = -sx*sh*(ca*cb+sa*sb)
             c2i = c1i
             s1r = sx*ch*ca - cx*sh*sb
             s2r = -sx*ch*ca - cx*sh*sb
             s1i = sx*ch*sa + cx*sh*cb
             s2i = sx*ch*sa - cx*sh*cb
             !decide whether to make transformation
             if ( sqrt(s1r**2+s1i**2) > eps .or. sqrt(s2r**2+s2i**2) > eps) then
                mark = .false.
                !transformation on left
                do i=1,ndim
                   aki = ReM(k,i)
                   ami = ReM(m,i)
                   zki = ImM(k,i)
                   zmi = ImM(m,i)
                   ReM(k,i) = c1r*aki - c1i*zki + s1r*ami - s1i*zmi
                   ImM(k,i) = c1r*zki + c1i*aki + s1r*zmi + s1i*ami
                   ReM(m,i) = s2r*aki - s2i*zki + c2r*ami - c2i*zmi
                   ImM(m,i) = s2r*zki + s2i*aki + c2r*zmi + c2i*ami
                end do
                !transformation on right
                do i=1,ndim
                   aik = ReM(i,k)
                   aim = ReM(i,m)
                   zik = ImM(i,k)
                   zim = ImM(i,m)
                   ReM(i,k) = c2r*aik - c2i*zik - s2r*aim + s2i*zim
                   ImM(i,k) = c2r*zik + c2i*aik - s2r*zim - s2i*aim
                   ReM(i,m) = -s1r*aik + s1i*zik + c1r*aim - c1i*zim
                   ImM(i,m) = -s1r*zik - s1i*aik + c1r*zim + c1i*aim
                   tik = ReV(i,k)
                   tim = ReV(i,m)
                   uik = ImV(i,k)
                   uim = ImV(i,m)
                   ReV(i,k) = c2r*tik - c2i*uik - s2r*tim + s2i*uim
                   ImV(i,k) = c2r*uik + c2i*tik - s2r*uim - s2i*tim
                   ReV(i,m) = -s1r*tik + s1i*uik + c1r*tim - c1i*uim
                   ImV(i,m) = -s1r*uik - s1i*tik + c1r*uim + c1i*tim
                end do

             end if
          end do MLoop
       end do KLoop
    end do


  end subroutine JacobiDiag

end module CmplxJacobiDiag

