module rootfinding

  use Kinds 
  use nrutil, only : nrerror,poly,poly_term,assert_eq, arth,swap, indexx
  implicit none
  private

  public :: polish
  public :: zroots
  public :: laguer

contains

  subroutine polish(a,roots)
    complex(dc), dimension(:), intent(IN) :: a
    complex(dc), dimension(:), intent(OUT) :: roots
    real(dp) :: xacc
    complex(dc) :: x
    integer, parameter :: MAXIT=20
    !Using the Newton-Raphson method, find the root of a function known to lie in the interval
    ![x1, x2]. The root rtnewt will be refined until its accuracy is known within \pm xacc.
    !Parameter: MAXIT is the maximum number of iterations.
    integer :: i,j,k,n
    complex(dc) :: df,dx,f
    n = size(roots)

    do i=1,n
       x = roots(i)
       xacc = spacing(abs(x))
       do j=1,MAXIT
          f = a(n+1)*x + a(n)
          df = a(n+1)
          do k=n-1,1,-1
             df = f+df*x;
             f = a(k) + f*x
          end do
          dx=f/df
          !print '(I4,5ES15.5)', j, real(x), abs(dx), xacc, abs(f), abs(df)
          if (abs(dx) < xacc) exit !Convergence.
          x = x - dx
       end do
       roots(i) = x
    end do
    !call nrerror('rtnewt exceeded maximum iterations')
  end subroutine polish

  subroutine zroots(a,roots,polish)
    complex(dc), dimension(:), intent(IN) :: a
    complex(dc), dimension(:), intent(OUT) :: roots
    logical(LGT), intent(IN) :: polish
    real(dp), parameter :: EPS=1.0e-14_dp
    !Given the array of M + 1 complex coefficients a of the polynomial \sum 
    !i=1..M+1 a(i)x^(i-1), this
    !routine successively calls laguer and finds all M complex roots. The logical variable
    !polish should be input as .true. if polishing (also by Laguerre's method) is desired,
    !.false. if the roots will be subsequently polished by other means.
    !Parameter: EPS is a small number.
    integer :: j,its,m
    integer, dimension(size(roots)) :: indx
    complex(dc) :: x
    complex(dc), dimension(size(a)) :: ad
    m=assert_eq(size(roots),size(a)-1,'zroots')
    ad(:)=a(:) !Copy of coefficients for successive deflation.
    do j=m,1,-1 !Loop over each root to be found.
       x=cmplx(0.0_dp,kind=dc)
       !Start at zero to favor convergence to smallest remaining root.
       call laguer(ad(1:j+1),x,its) !Find the root.
       if (abs(aimag(x)) <= 2.0_dp*EPS**2*abs(real(x))) &
            x=cmplx(real(x),kind=dc)
       roots(j)=x
       print '(I4,2F20.10)', j, real(x), aimag(x)
       ad(j:1:-1)=poly_term(ad(j+1:2:-1),x) !Forward deflation.
    end do
    if (polish) then
       do j=1,m !Polish the roots using the undeflated coefficients.
          call laguer(a(:),roots(j),its)
       end do
    end if
    call indexx(real(roots),indx) !Sort roots by their real parts.
    roots=roots(indx)
  end subroutine zroots

  subroutine laguer(a,x,its)
    integer, intent(OUT) :: its
    complex(dc), intent(INOUT) :: x
    complex(dc), dimension(:), intent(IN) :: a
    real(dp), parameter :: EPS=epsilon(1.0_dp)
    integer, parameter :: MR=8,MT=20,MAXIT=MT*MR
    !Given an array of M + 1 complex coefficients a of the polynomial M+1
    !i=1 a(i)x^(i-1), and
    !given a complex value x, this routine improves x by Laguerre's method until it converges,
    !within the achievable roundoff limit, to a root of the given polynomial. The number of
    !iterations taken is returned as its.
    !Parameters: EPS is the estimated fractional roundoff error. We try to break (rare) limit
    !cycles with MR different fractional values, once every MT steps, for MAXIT total allowed
    !iterations.
    integer :: iter,m
    real(dp) :: abx,abp,abm,err
    complex(dc) :: dx,x1,f,g,h,sq,gp,gm,g2
    complex(dc), dimension(size(a)) :: b,d
    real(dp), dimension(MR) :: frac = &
         (/ 0.5_dp,0.25_dp,0.75_dp,0.13_dp,0.38_dp,0.62_dp,0.88_dp,1.0_dp /)
    !Fractions used to break a limit cycle.
    m=size(a)-1
    do iter=1,MAXIT !Loop over iterations up to allowed maximum.
       its=iter
       abx=abs(x)
       b(m+1:1:-1)=poly_term(a(m+1:1:-1),x) !Efficient computation of the polynomial
       d(m:1:-1)=poly_term(b(m+1:2:-1),x) !and its first two derivatives.
       f=poly(x,d(2:m)) !f stores P''/2.
       err=EPS*poly(abx,abs(b(1:m+1))) !Estimate of roundoff in evaluating polynomial.
       if (abs(b(1)) <= err) return !We are on the root.
       g=d(1)/b(1) !The generic case: Use Laguerre's formula.
       g2=g*g
       h=g2-2.0_dp*f/b(1)
       sq=sqrt((m-1)*(m*h-g2))
       gp=g+sq
       gm=g-sq
       abp=abs(gp)
       abm=abs(gm)
       if (abp < abm) gp=gm
       if (max(abp,abm) > 0.0) then
          dx=m/gp
       else
          dx=exp(cmplx(log(1.0_dp+abx),iter,kind=dc))
       end if
       x1=x-dx
       if (x == x1) return !Converged.
       if (mod(iter,MT) /= 0) then
          x=x1
       else !Every so often we take a fractional step, to break any limit cycle (itself a rare occurrence).
          x=x-dx*frac(iter/MT)
       end if
    end do
    call nrerror('laguer: too many iterations')
  end subroutine laguer

end module rootfinding
