!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/cloverfmunu.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: cloverfmunu.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:00  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module CloverFmunu

  use MatrixAlgebra
  use MPIInterface
  use ReduceOps
  use LatticeSize
  use Kinds
  use ColourTypes
  implicit none
  private

  type(colour_matrix), dimension(:,:,:,:,:), allocatable , public :: F_munu !Clover based field strength tensor

  public :: GetTopQ
  public :: CalculateFmunuClover
  public :: GetCloverTerm

contains

  function GetTopQ(F_munu) result (Q)
    ! begin args: F_munu

    type(colour_matrix), dimension(:,:,:,:,:) :: F_munu
    ! begin local_vars
    real(dp) :: Q, Qpp, TrF12F34, TrF13F24, TrF14F23
    integer :: ix,iy,iz,it

    ! begin execution

    Qpp = 0.0d0

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       call RealTraceMultMatMat(TrF12F34,F_munu(ix,iy,iz,it,1),F_munu(ix,iy,iz,it,6))
       call RealTraceMultMatMat(TrF13F24,F_munu(ix,iy,iz,it,2),F_munu(ix,iy,iz,it,5))
       call RealTraceMultMatMat(TrF14F23,F_munu(ix,iy,iz,it,3),F_munu(ix,iy,iz,it,4))
       Qpp = Qpp + 8.0d0*(TrF12F34 - TrF13F24 + TrF14F23)
    end do; end do; end do; end do

    call AllSum(Qpp,Q)

    Q = Q/(32.0d0*(pi**2))


  end function GetTopQ

  subroutine CalculateFmunuClover(F_munu,U_xd)
    ! begin args: F_munu, U_xd

    type(colour_matrix), dimension(:,:,:,:,:) :: F_munu
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd

    ! begin local_vars
    integer :: mu, nu, iplaq
    integer :: ix,iy,iz,it

    ! begin execution

    do mu=1,nd
       do nu=mu+1,nd
          iplaq = mu+nu-1-1/mu

          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             F_munu(ix,iy,iz,it,iplaq)%Cl = 0.0d0
          end do; end do; end do; end do

          call GetCloverTerm(F_munu,U_xd,mu,nu,iplaq)

          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             F_munu(ix,iy,iz,it,iplaq)%Cl = 0.125d0*F_munu(ix,iy,iz,it,iplaq)%Cl
          end do; end do; end do; end do

       end do
    end do


  end subroutine CalculateFmunuClover


  subroutine GetCloverTerm(F_munu,U_xd,mu,nu,iplaq)
    ! begin args: F_munu, U_xd, mu, nu, iplaq

    type(colour_matrix), dimension(:,:,:,:,:) :: F_munu
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: mu, nu, iplaq

    ! begin local_vars
    type(colour_matrix) :: U_munu, UmuUnu, UmudagUnudag, UnuUmudag, UnudagUmu !Gauge field products

    integer, dimension(nd) :: dmu, dnu
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt
    integer :: ax,ay,az,at
    integer :: bx,by,bz,bt
    integer :: cx,cy,cz,ct
    integer :: dx,dy,dz,dt
    integer :: ex,ey,ez,et

#define U_mux U_xd(ix,iy,iz,it,mu)
#define U_nux U_xd(ix,iy,iz,it,nu)

#define U_nuxpmu U_xd(jx,jy,jz,jt,nu)
#define U_muxpnu U_xd(kx,ky,kz,kt,mu)

#define U_muxmmu U_xd(ax,ay,az,at,mu)
#define U_nuxmmu U_xd(ax,ay,az,at,nu)

#define U_muxmnu U_xd(bx,by,bz,bt,mu)
#define U_nuxmnu U_xd(bx,by,bz,bt,nu)

#define U_nuxmmumnu U_xd(cx,cy,cz,ct,nu)
#define U_muxmmumnu U_xd(cx,cy,cz,ct,mu)

#define U_nuxmnupmu U_xd(dx,dy,dz,dt,nu)
#define U_muxpnummu U_xd(ex,ey,ez,et,mu)

#define F_munux F_munu(ix,iy,iz,it,iplaq)

    ! begin execution

    dmu = 0
    dnu = 0
    dmu(mu) = 1
    dnu(nu) = 1

    do it=1,nt
       jt = mapt(it + dmu(4))
       kt = mapt(it + dnu(4))
       at = mapt(it - dmu(4))
       bt = mapt(it - dnu(4))
       ct = mapt(it - dmu(4) - dnu(4))
       dt = mapt(it + dmu(4) - dnu(4))
       et = mapt(it - dmu(4) + dnu(4))
       do iz=1,nz
          jz = mapz(iz + dmu(3))
          kz = mapz(iz + dnu(3))
          az = mapz(iz - dmu(3))
          bz = mapz(iz - dnu(3))
          cz = mapz(iz - dmu(3) - dnu(3))
          dz = mapz(iz + dmu(3) - dnu(3))
          ez = mapz(iz - dmu(3) + dnu(3))
          do iy=1,ny
             jy = mapy(iy + dmu(2))
             ky = mapy(iy + dnu(2))
             ay = mapy(iy - dmu(2))
             by = mapy(iy - dnu(2))
             cy = mapy(iy - dmu(2) - dnu(2))
             dy = mapy(iy + dmu(2) - dnu(2))
             ey = mapy(iy - dmu(2) + dnu(2))
             do ix=1,nx
                jx = mapx(ix + dmu(1))
                kx = mapx(ix + dnu(1))
                ax = mapx(ix - dmu(1))
                bx = mapx(ix - dnu(1))
                cx = mapx(ix - dmu(1) - dnu(1))
                dx = mapx(ix + dmu(1) - dnu(1))
                ex = mapx(ix - dmu(1) + dnu(1))

                !U+mu+nu(x)
                call MultiplyMatMat(UmuUnu,U_mux,U_nuxpmu)
                call MultiplyMatdagMatdag(UmudagUnudag,U_muxpnu,U_nux)
                call MultiplyMatMat(U_munu,UmuUnu,UmudagUnudag)

                call AddCloverLeaf(F_munux,U_munu)

                !U-nu+mu(x+a_nu)
                call MultiplyMatdagMat(UnudagUmu,U_nuxmnu,U_muxmnu)
                call MultiplyMatMatdag(UnuUmudag,U_nuxmnupmu,U_mux)
                call MultiplyMatMat(U_munu,UnudagUmu,UnuUmudag)

                call AddCloverLeaf(F_munux,U_munu)

                !U+nu-mu(x+a_mu)
                call MultiplyMatMatdag(UnuUmudag,U_nux,U_muxpnummu)
                call MultiplyMatdagMat(UnudagUmu,U_nuxmmu,U_muxmmu)
                call MultiplyMatMat(U_munu,UnuUmudag,UnudagUmu)

                call AddCloverLeaf(F_munux,U_munu)

                !U-mu-nu(x+a_mu+a_nu)
                call MultiplyMatdagMatdag(UmudagUnudag,U_muxmmu,U_nuxmmumnu)
                call MultiplyMatMat(UmuUnu,U_muxmmumnu,U_nuxmnu)
                call MultiplyMatMat(U_munu,UmudagUnudag,UmuUnu)

                call AddCloverLeaf(F_munux,U_munu)


             end do
          end do
       enddo
    end do

#undef U_mux
#undef U_nux
#undef U_nuxpmu
#undef U_muxpnu

#undef F_munux

  contains

    subroutine AddCloverLeaf(F_munux,U_munux)
      ! begin args: F_munux, U_munux

      type(colour_matrix) :: F_munux, U_munux

      ! begin execution

      F_munux%Cl(1,1) = F_munux%Cl(1,1) + U_munux%Cl(1,1) - conjg(U_munux%Cl(1,1))
      F_munux%Cl(2,1) = F_munux%Cl(2,1) + U_munux%Cl(2,1) - conjg(U_munux%Cl(1,2))
      F_munux%Cl(3,1) = F_munux%Cl(3,1) + U_munux%Cl(3,1) - conjg(U_munux%Cl(1,3))
      F_munux%Cl(1,2) = F_munux%Cl(1,2) + U_munux%Cl(1,2) - conjg(U_munux%Cl(2,1))
      F_munux%Cl(2,2) = F_munux%Cl(2,2) + U_munux%Cl(2,2) - conjg(U_munux%Cl(2,2))
      F_munux%Cl(3,2) = F_munux%Cl(3,2) + U_munux%Cl(3,2) - conjg(U_munux%Cl(2,3))
      F_munux%Cl(1,3) = F_munux%Cl(1,3) + U_munux%Cl(1,3) - conjg(U_munux%Cl(3,1))
      F_munux%Cl(2,3) = F_munux%Cl(2,3) + U_munux%Cl(2,3) - conjg(U_munux%Cl(3,2))
      F_munux%Cl(3,3) = F_munux%Cl(3,3) + U_munux%Cl(3,3) - conjg(U_munux%Cl(3,3))


    end subroutine AddCloverLeaf


  end subroutine GetCloverTerm

end module CloverFmunu

