#include "defines.h"

module QuarkPropTypes
  use Kinds
  use LatticeSize
  use ColourTypes
  use SpinorTypes
  use SourceTypes
  use FermActTypes
  use MPIInterface
  use QuarkPropagator
  use FermionAction
  use FermionField
  use TextIO
  use GaugeField
  use GaugeFieldMPIComms
  use BackgroundField
  use ColourFieldOps
  use StoutLinks
  implicit none
  private

  type, public :: QPParamType
     character(len=64) :: FermActName
     integer, dimension(nd) :: gfshift
     real(dp) :: kappa
     integer :: kBFStrength
     real(dp) :: tolerance
     integer :: iSourceType
   contains
     procedure :: Get => GetQPParamType
     procedure :: Put => PutQPParamType
  end type QPParamType
  
  type, extends(QPParamType), public :: QPMetaDataType
     character(len=128) :: header
     integer, dimension(nd) :: nl_d ! lattice dimensions
     character(len=_FILENAME_LEN_) :: GaugeFieldFile
     real(dp) :: u0check
     class(SourceType), pointer :: qpsrc
     class(wilsonFermAct), pointer :: fermact
   contains
     procedure :: Get => Get_qpmetadata
     procedure :: Put => Put_qpmetadata
     procedure :: GetSource => GetSource_qpmetadata
     procedure :: InitialiseFermact => InitialiseFermact_qpmetadata
     procedure :: Read => ReadQPMetadataType
     procedure :: Write => WriteQPMetadataType
  end type QPMetaDataType

  type, extends(QPMetaDataType), public :: IFMSMetaDataType
     integer :: n_x
     integer :: n_v
   contains
     procedure :: Get => GetIFMSMetadataType
     procedure :: Put => PutIFMSMetadataType
     procedure :: Read => ReadIFMSMetadataType
     procedure :: Write => WriteIFMSMetadataType
     procedure :: GetIFMSSource => GetIFMSSource_ifms
     procedure :: SetExtents => SetExtents_ifms
  end type IFMSMetaDataType

!!  type, extends(QPMetaDataType), public :: MetaDataType
!!     integer :: n_x
!!     integer :: n_v
!!   contains
!!     procedure :: Get => GetIFMSMetadataType
!!     procedure :: Put => PutIFMSMetadataType
!!     procedure :: Read => ReadIFMSMetadataType
!!     procedure :: Write => WriteIFMSMetadataType
!!  end type MetaDataType
  public :: GetFileFormatFromMetaDataFile

contains

  subroutine GetQPParamType(this,file_unit)
    class(QPParamType) :: this
    integer :: file_unit

    call Get(this%FermActName,file_unit)
    call Get(this%kappa,file_unit)
    call Get(this%gfshift,file_unit)
    call Get(this%kBFStrength,file_unit)
    call Get(this%tolerance,file_unit)
    call Get(this%iSourceType,file_unit)
    
  end subroutine GetQPParamType

  subroutine PutQPParamType(this,file_unit)
    class(QPParamType) :: this
    integer :: file_unit

    if ( i_am_root ) then
       write(file_unit,'(a)') this%FermActName
       write(file_unit,'(f20.10,a)') this%kappa, " !qp%kappa"
       write(file_unit,'(4i5,a)') this%gfshift, " !qp%gfshift"
       write(file_unit,'(i20,a)') this%kBFStrength, " !qp%kBFStrength"
       write(file_unit,'(es20.10,a)') this%tolerance, " !qp%tolerance"
       write(file_unit,'(i20,a)') this%iSourceType, " !qp%iSourceType"
    end if

  end subroutine PutQPParamType

  subroutine Get_qpmetadata(this,file_unit)
    class(QPMetadataType) :: this
    integer :: file_unit

    call Get(this%nl_d,file_unit)
    if ( .not. all( this%nl_d == (/ nlx, nly, nlz, nlt /) ) ) then
       call Abort("QuarkProp%ReadMetaData: Lattice size mismatch.")
    end if
    call Get(this%GaugeFieldFile,file_unit)
    call Get(this%u0check,file_unit)
    call this%QPParamType%Get(file_unit)
    call SelectSourceType(this%qpsrc,this%iSourceType)
    call this%qpsrc%Get(file_unit)
    call SetFermActType(this%fermact,this%FermActName)
    call this%fermact%Get(file_unit)
    
  end subroutine Get_qpmetadata

  subroutine Put_qpmetadata(this,file_unit)
    class(QPMetaDataType) :: this
    integer :: file_unit

    if ( i_am_root ) then
       write(file_unit,'(4i5,a)') this%nl_d, " !qp%nl_d"
       write(file_unit,'(a)') trim(this%GaugeFieldFile)
       write(file_unit,'(f20.10,a)') this%u0check, " !qp%u0check"
    end if
    call this%QPParamType%Put(file_unit)
    call this%qpsrc%Put(file_unit)
    call this%fermact%Put(file_unit)

  end subroutine Put_qpmetadata

  subroutine GetSource_qpmetadata(this,rho,js,jc,U_xd)
    class(QPMetaDataType) :: this
    type(colour_vector), dimension(:,:,:,:,:) :: rho !source vector
    integer :: js,jc
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: jx,jy,jz,jt
    real(dp) :: u0
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: Usm_xd
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi

    !! ToDo: Remove the option of mean field improvement in the source and sink smearing?
    select type (qpsrc => this%qpsrc)
    type is (PointSourceType)
       jx = qpsrc%jx
       jy = qpsrc%jy
       jz = qpsrc%jz
       jt = qpsrc%jt
       call GetPointSource(rho,jx,jy,jz,jt,js,jc)
    type is (WallSourceType)
       jt = qpsrc%jt
       call GetWallSource(rho,jt,js,jc)
    type is (SmearedSourceType)
       jx = qpsrc%jx
       jy = qpsrc%jy
       jz = qpsrc%jz
       jt = qpsrc%jt
       call GetPointSource(rho,jx,jy,jz,jt,js,jc)

       call CreateGaugeField(Usm_xd,1)
       if ( qpsrc%UseStoutLinks ) then
          call StoutSmearLinks(U_xd,Usm_xd,qpsrc%alpha_stout,qpsrc%n_stout)
       else
          Usm_xd(:,:,:,:,:) = U_xd(1:nx,1:ny,1:nz,1:nt,:)
       end if
       if ( this%kBFStrength /= 0 ) call MultiplyBF(this%kBFStrength,Usm_xd)
       u0 = 1.0d0
       if ( qpsrc%UseUzero ) then
          u0 = qpsrc%u0_smsrc
          if ( u0 < 0.0d0 ) call GetUzero(Usm_xd,u0)
       end if
       associate ( bcx => this%fermact%bcx, bcy => this%fermact%bcy, &
            &      bcz => this%fermact%bcz, bct => this%fermact%bct)
         call SetBoundaryConditions(Usm_xd,bcx,bcy,bcz,bct)
       end associate
       call ShadowGaugeField(Usm_xd,1)
       call SmearSource(rho,jt,js,qpsrc%alpha_smsrc,qpsrc%n_smsrc,Usm_xd,u0)
       call DestroyGaugeField(Usm_xd)
    type is (NoiseSourceType)
       !! Noise Source
       !! Do nothing here - handled externally at the moment.
    type is (SSTSourceType)
       !! SST Source.
       !! Read in the regular propagator column.
       call CreateFermionField(chi,1)
       call GetPropColumn(chi,js,jc,qp_sst_source_prop)
       rho = zero_vector
       call AddSSTSource(rho,chi,U_xd,this%kappa,qpsrc%j_x,qpsrc%j_mu,qpsrc%it_s,qpsrc%p_x,qpsrc%currentType)
       call DestroyFermionField(chi)
    end select

  end subroutine GetSource_qpmetadata

  subroutine ReadQPMetaDataType(this,filename)
    class(QPMetaDataType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='read')
    call Get(this%header,file_unit)
    call this%Get(file_unit)
    call CloseTextfile(file_unit)

  end subroutine ReadQPMetaDataType

  subroutine WriteQPMetaDataType(this,filename)
    class(QPMetaDataType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='write')
    call Put(trim(this%header),file_unit)
    call this%Put(file_unit)
    call CloseTextfile(file_unit)

  end subroutine WriteQPMetaDataType

  subroutine ReadIFMSMetaDataType(this,filename)
    class(IFMSMetaDataType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='read')
    call Get(this%header,file_unit)
    call this%Get(file_unit)
    call CloseTextfile(file_unit)

  end subroutine ReadIFMSMetaDataType

  subroutine WriteIFMSMetaDataType(this,filename)
    class(IFMSMetaDataType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='write')
    call Put(trim(this%header),file_unit)
    call this%Put(file_unit)
    call CloseTextfile(file_unit)

  end subroutine WriteIFMSMetaDataType

  subroutine GetIFMSMetaDataType(this,file_unit)
    class(IFMSMetaDataType) :: this
    integer :: file_unit

    call Get(this%n_x,file_unit)
    call Get(this%n_v,file_unit)
    call this%QPMetaDataType%Get(file_unit)

  end subroutine GetIFMSMetaDataType

  subroutine PutIFMSMetaDataType(this,file_unit)
    class(IFMSMetaDataType) :: this
    integer :: file_unit

    if ( i_am_root ) then
       write(file_unit,'(i20,a)') this%n_x, " !ifms_size%n_x"
       write(file_unit,'(i20,a)') this%n_v, " !ifms_size%n_v"
    end if
    call this%QPMetaDataType%Put(file_unit)

  end subroutine PutIFMSMetaDataType

  subroutine InitialiseFermAct_qpmetadata(this,file_stub,even_odd)
    class(QPMetaDataType) :: this
    character(len=*) :: file_stub
    logical :: even_odd

    call SetFermActType(this%fermact,this%FermActName)
    call SelectFermionAction(this%FermActName)
    call this%fermact%Read(trim(file_stub)//".fm_"//trim(this%FermActName))
    call SetFermionActionParams(this%fermact)
    call InitialiseFermionAction(U_xd,this%kappa,this%kBFStrength,even_odd)
    
  end subroutine InitialiseFermAct_qpmetadata

  subroutine GetIFMSSource_ifms(this,rho,i_v,U_xd)
    class(IFMSMetaDataType) :: this
    type(colour_vector), dimension(:,:,:,:,:) :: rho !source vector
    integer :: i_v
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: jc,js,jt

    select type (qpsrc => this%qpsrc)
    type is (NoiseSourceType)
       associate(nv_c => qpsrc%diln%nv_c,nv_s => qpsrc%diln%nv_s,nv_t => qpsrc%diln%nv_t)
         jc = modulo((i_v-1),nv_c)+1
         js = modulo((i_v-1)/nv_c,nv_s)+1
         jt = ((i_v-1)/(nv_c*nv_s))+1
         call qpsrc%diln%GetSource(rho,jc,js,jt,qp_noise_field)
       end associate
    class default
       jc = modulo(i_v-1,nc)+1
       js = ((i_v-1)/nc)+1
       call this%GetSource(rho,js,jc,U_xd)
    end select

  end subroutine GetIFMSSource_ifms

  subroutine SetExtents_ifms(this)
    class(IFMSMetaDataType) :: this

    this%nl_d = (/ nlx, nly, nlz, nlt /)
    this%n_x = nlattice_eo
    select type (qpsrc => this%qpsrc)
    type is (NoiseSourceType)
       associate(nv_c => qpsrc%diln%nv_c,nv_s => qpsrc%diln%nv_s,nv_t => qpsrc%diln%nv_t)
         this%n_v = nv_c*nv_s*nv_t
       end associate
    class default
       this%n_v = nc*ns
    end select

  end subroutine SetExtents_ifms

  subroutine GetFileFormatFromMetaDataFile(FileFormat,MetaDataFile)
    character(len=*) :: FileFormat, MetaDataFile

    character(len=128) :: header
    integer :: file_unit, pos, ipos, jpos
    character :: char

    call OpenTextfile(MetaDataFile,file_unit,action='read')
    call Get(header,file_unit)
    call CloseTextfile(file_unit)

    FileFormat = ""
    pos = index(header,"binary file format = ")
    if ( pos == 0 ) call Abort("GetFileFormatFromMetaDataFile: failed to find format code.")
    ipos = pos + len("binary file format = ")
    jpos = 1
    char = header(ipos:ipos)
    do while (char /= ',')
       FileFormat(jpos:jpos) = char
       ipos = ipos + 1
       jpos = jpos + 1
       if ( ipos > len(header) ) call Abort("GetFileFormatFromMetaDataFile: failed to find format code.")
       char = header(ipos:ipos)
    end do

  end subroutine GetFileFormatFromMetaDataFile

end module QuarkPropTypes
