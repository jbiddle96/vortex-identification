!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$


#include "defines.h"

module ConjGradSolvers

  use FermionField
  use fermionAlgebra
  use Timer
  use MPIInterface
  use FermionAlgebra
  use LatticeSize
  use SpinorTypes
  use FermionTypes
  use ColourTypes
  use Kinds
  implicit none
  private

  integer , public :: nbicgstab = 0, max_cgsolve_iter = 200000
  real(dp), public :: cgmaxtime, cgmintime, cgmeantime
  logical, public :: DebugCGInvert = .false.

  public :: CGInvert
  public :: ConjResInvert
  public :: MinResInvert
  public :: BiCGStabInvert
  public :: CGSInvert
  public :: MultiCGInvert
  public :: MultiCGInvert_z
  public :: MultiCRInvert
  public :: MultiBiCGStabInvert

  interface CGInvert
     module procedure CGInvert_4x
     module procedure CGInvert_eo
  end interface

  interface ConjResInvert
     module procedure ConjResInvert_4x
     module procedure ConjResInvert_eo
  end interface
  
  interface BiCGStabInvert
     module procedure BiCGStabInvert_4x
     module procedure BiCGStabInvert_eo
  end interface

  interface MultiCGInvert
     module procedure MultiCGInvert_4x
     module procedure MultiCGInvert_eo
  end interface

  interface MultiCGInvert_z
     module procedure MultiCGInvert_z_4x
     module procedure MultiCGInvert_z_eo
  end interface

  interface MultiCRInvert
     module procedure MultiCRInvert_4x
     module procedure MultiCRInvert_eo
  end interface

contains

#define residue_x(is) residue(ix,iy,iz,it,is)
#define psi_x(is) psi(ix,iy,iz,it,is)
#define phi_x(is) phi(ix,iy,iz,it,is)
#define Mphi_x(is) Mphi(ix,iy,iz,it,is)
#define chi_x(is) chi(ix,iy,iz,it,is)
#define xi_x(is) xi(ix,iy,iz,it,is)
#define Mxi_x(is) Mxi(ix,iy,iz,it,is)
#define phi_ix(is) phi_i(ix,iy,iz,it,is,i_v)
#define chi_ix(is) chi_i(ix,iy,iz,it,is,i_v)
#define xi_ix(is) xi_i(ix,iy,iz,it,is,i_v)
#define prevres_x(is) prevres(ix,iy,iz,it,is)
#define phi_prx(is) phi_pr(ix,iy,iz,it,is)
#define xi_prx(is) xi_pr(ix,iy,iz,it,is)
#define Mxi_prx(is) Mxi_pr(ix,iy,iz,it,is)
#define phi_iprx(is) phi_ipr(ix,iy,iz,it,is,i_v)

  subroutine CGInvert_4x(psi, chi, tolerance, iterations, MatrixOperate)
    ! begin args: psi, chi, tolerance, iterations, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi
    !M _must_ be hermitian and positive definite.

    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:) :: chi !solution vector and initial guess
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, Mphi
    real(dp) :: delta, prevdelta, norm_psi, omega, theta, phidotMphi
    integer :: ix,iy,iz,it,is
    complex(dc) :: test

    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate(Mphi(nxp,nyp,nzp,ntp,ns))

    norm_psi = fermion_norm(psi)
    !mpiprint *, "norm ", norm_psi

    call MatrixOperate(chi, Mphi)

    !residue = psi - Mphi
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       residue_x(is)%Cl = psi_x(is)%Cl - Mphi_x(is)%Cl
    end do; end do; end do; end do; end do

    phi = residue

    delta = fermion_normsq(residue)

    call MatrixOperate(phi,Mphi)

    phidotMphi = real_inner_product(phi,Mphi)
    test = inner_product(phi,Mphi)

    iterations = 0

    do
       if (sqrt(delta)/norm_psi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit

       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(delta), theta, omega, phidotMphi
       end if

       iterations = iterations + 1

       omega = delta/phidotMphi

       !residue = residue - omega*Mphi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = residue_x(is)%Cl - omega*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do

       !chi = chi + omega*phi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          chi_x(is)%Cl = chi_x(is)%Cl + omega*phi_x(is)%Cl
       end do; end do; end do; end do; end do

       prevdelta = delta
       delta = fermion_normsq(residue)

       theta = -delta/prevdelta

       !phi = residue - theta*phi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi_x(is)%Cl = residue_x(is)%Cl - theta*phi_x(is)%Cl
       end do; end do; end do; end do; end do

       call MatrixOperate(phi,Mphi)

       phidotMphi = real_inner_product(phi,Mphi)
       test = inner_product(phi,Mphi)

    end do

    deallocate(residue)
    deallocate(phi)
    deallocate( Mphi)

  end subroutine CGInvert_4x

  subroutine ConjResInvert_4x(psi, chi, tolerance, iterations, MatrixOperate)
    ! begin args: psi, chi, tolerance, iterations, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi
    !M _must_ be hermitian.

    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:) :: chi !solution vector and initial guess
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue, Mres
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, Mphi
    real(dp) :: delta, prevdelta, norm_psi, omega, theta, phidotMphi, deltaM, prevdeltaM
    integer :: ix,iy,iz,it,is
    complex(dc) :: test

    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate( Mres(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate( Mphi(nxp,nyp,nzp,ntp,ns))

    norm_psi = fermion_norm(psi)

    call MatrixOperate(chi, Mphi)

    !residue = psi - Mphi
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       residue_x(is)%Cl = psi_x(is)%Cl - Mphi_x(is)%Cl
    end do; end do; end do; end do; end do

    phi = residue

    delta = fermion_normsq(residue)

    call MatrixOperate(residue,Mres)
    deltaM = real_inner_product(residue,Mres)

    call MatrixOperate(phi,Mphi)

    phidotMphi = real_inner_product(Mphi,Mphi)

    iterations = 0

    do
       if (sqrt(delta)/norm_psi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit

       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(delta), theta, omega, phidotMphi
       end if

       iterations = iterations + 1

       omega = deltaM/phidotMphi

       !residue = residue - omega*Mphi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = residue_x(is)%Cl - omega*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do

       !chi = chi + omega*phi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          chi_x(is)%Cl = chi_x(is)%Cl + omega*phi_x(is)%Cl
       end do; end do; end do; end do; end do

       delta = fermion_normsq(residue)

       prevdeltaM = deltaM
       call MatrixOperate(residue,Mres)
       deltaM = real_inner_product(residue,Mres)

       theta = -deltaM/prevdeltaM

       !phi = residue - theta*phi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi_x(is)%Cl = residue_x(is)%Cl - theta*phi_x(is)%Cl
       end do; end do; end do; end do; end do

       call MatrixOperate(phi,Mphi)

       phidotMphi = real_inner_product(Mphi,Mphi)

    end do

    deallocate(residue)
    deallocate( Mres)
    deallocate(phi)
    deallocate( Mphi)

  end subroutine ConjResInvert_4x

  subroutine MinResInvert(psi, chi, tolerance, iterations, MatrixOperate)
    ! begin args: psi, chi, tolerance, iterations, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi
    !M _must_ be hermitian.

    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:) :: chi !solution vector and initial guess
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi,prevphi,phi_pr,xi,prevxi
    real(dp) :: alpha, beta, eta, mu, nu, nextbeta
    real(dp) :: alpha_pr, beta_pr, gamma_pr, eta_pr, nextbeta_pr
    real(dp) :: alpha_temp, beta_temp, c1, s1, c2, s2
    real(dp) :: norm_phi

    ! begin execution
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate(prevphi(nxp,nyp,nzp,ntp,ns))
    allocate(phi_pr(nxp,nyp,nzp,ntp,ns))
    allocate(xi(nxp,nyp,nzp,ntp,ns))
    allocate(prevxi(nxp,nyp,nzp,ntp,ns))

    phi = psi

    call MatrixOperate(chi,phi_pr)

    call PsiMinusPhi(phi,phi_pr)

    norm_phi = fermion_norm(phi)
    call normalise(phi,norm_phi)

    prevphi = zero_vector
    prevxi = zero_vector
    xi = zero_vector

    eta_pr = 1.0d0

    beta = 0.0d0
    gamma_pr = 0.0d0
    beta_pr = 0.0d0

    c2 = 1.0d0
    s2 = 0.0d0

    c1 = 1.0d0
    s1 = 0.0d0

    iterations = 0.0d0

    do
       if (abs(eta_pr) < tolerance ) exit
       if ( iterations > max_cgsolve_iter ) exit

       iterations = iterations + 1

       !phi = (Mphi - alpha*phi - beta*prevphi)/nextbeta
       call MatrixOperate(phi,phi_pr)

       alpha = inner_product(phi,phi_pr)
       call PsiMinusAlphaPhi(phi_pr,alpha,phi)
       call PsiMinusAlphaPhi(phi_pr,beta,prevphi)

       prevphi = phi
       nextbeta = fermion_norm(phi_pr)
       call MultiplyAlphaPhi(phi,1.0d0/nextbeta,phi_pr)

       alpha_pr = alpha
       beta = nextbeta
       nextbeta_pr = beta

       ! if iterations = 1, beta_pr  = 0, gamma_pr = 0
       ! if iterations = 2, beta_pr /= 0, gamma_pr = 0

       gamma_pr = s2*beta_pr
       beta_pr = c2*beta_pr

       alpha_temp = alpha_pr
       beta_temp = beta_pr
       beta_pr = c1*beta_temp + s1*alpha_temp
       alpha_pr = -s1*beta_temp + c1*alpha_temp

       c2 = c1
       s2 = s1

       mu = alpha_pr
       nu = beta

       if ( mu /= 0.0d0 ) then
          c1 = abs(mu)/sqrt(mu**2 + nu**2)
          s1 = c1*nu/mu
       else
          c1 = 0.0d0
          s1 = 1.0d0
       end if

       eta = c1*eta_pr
       eta_pr = -s1*eta_pr
       alpha_pr = c1*mu + s1*nu

       !xi = (phi - beta_pr*xi - gamma_pr*prevxi)/alpha_pr
       phi_pr = prevphi
       call PsiMinusAlphaPhi(phi_pr,beta_pr,xi)
       call PsiMinusAlphaPhi(phi_pr,gamma_pr,prevxi)
       prevxi = xi
       call MultiplyAlphaPhi(xi,1.0d0/alpha_pr,phi_pr)

       !chi = chi + eta*xi
       call PsiPlusAlphaPhi(chi,eta,xi)

       beta = nextbeta
       beta_pr = nextbeta_pr

    end do

    deallocate(phi)
    deallocate(prevphi)
    deallocate(phi_pr)
    deallocate(xi)
    deallocate(prevxi)

  end subroutine MinResInvert

  subroutine BiCGStabInvert_4x(psi, chi, tolerance, iterations, MatrixOperate)
    ! begin args: psi, chi, tolerance, iterations, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi

    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:) :: chi !solution vector and initial guess
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue, r0
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, Mphi, xi, Mxi
    real(dp) :: epsilon, norm_psi
    complex(dc) :: delta, delta_prime, omega, beta, theta
    integer :: ix,iy,iz,it,is
    real(dp) :: t_in, t_out

    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate( r0(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate( Mphi(nxp,nyp,nzp,ntp,ns))
    allocate( xi(nxp,nyp,nzp,ntp,ns))
    allocate( Mxi(nxp,nyp,nzp,ntp,ns))

    norm_psi = fermion_norm(psi)

    call MatrixOperate(chi,Mphi)

    !residue = psi - Mphi
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       residue_x(is)%Cl = psi_x(is)%Cl - Mphi_x(is)%Cl
    end do; end do; end do; end do; end do

    epsilon = fermion_normsq(residue)

    r0 = residue
    delta = inner_product(r0,residue)

    xi = residue
    call MatrixOperate(xi,Mxi)

    delta_prime = inner_product(r0,Mxi)

    iterations = 0

    do
       if (sqrt(epsilon)/norm_psi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit

       iterations = iterations + 1

       !res1(iterations) = sqrt(epsilon)/norm_psi
       !if ( iterations >= 500 ) exit

       !print '(I4,ES20.10,3(F15.10,F15.10))', iterations, sqrt(epsilon)/norm_psi !, omega, theta, beta
       if ( DebugCGInvert ) then
          mpiprint '(I4,F20.10,3(F15.10,F15.10))', iterations, sqrt(epsilon), omega, theta, beta
       end if

       omega = delta/delta_prime

       !phi = residue - omega*Mxi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi_x(is)%Cl = residue_x(is)%Cl - omega*Mxi_x(is)%Cl
       end do; end do; end do; end do; end do

       call MatrixOperate(phi,Mphi)

       theta = inner_product(Mphi,phi)/fermion_normsq(Mphi)

       !residue = phi - theta*Mphi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = phi_x(is)%Cl - theta*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do

       epsilon = fermion_normsq(residue)

       !chi = chi + omega*xi + theta*phi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          chi_x(is)%Cl = chi_x(is)%Cl + omega*xi_x(is)%Cl + theta*phi_x(is)%Cl
       end do; end do; end do; end do; end do

       delta = inner_product(r0,residue)

       beta = -delta/(delta_prime*theta)

       !xi = residue - beta*(xi - theta*Mxi)
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          xi_x(is)%Cl = residue_x(is)%Cl - beta*(xi_x(is)%Cl - theta*Mxi_x(is)%Cl)
       end do; end do; end do; end do; end do

       call MatrixOperate(xi,Mxi)

       delta_prime = inner_product(r0,Mxi)

    end do

    deallocate(residue)
    deallocate( r0)
    deallocate(phi)
    deallocate( Mphi)
    deallocate( xi)
    deallocate( Mxi)

  end subroutine BiCGStabInvert_4x

  subroutine CGSInvert(psi, chi, tolerance, iterations, MatrixOperate)
    ! begin args: psi, chi, tolerance, iterations, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi

    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:) :: chi !solution vector and initial guess
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue, r0
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, phi_pr, xi, Mxi, xi_pr, Mxi_pr
    real(dp) :: epsilon, norm_psi
    complex(dc) :: delta, prevdelta, delta_prime, alpha, beta
    integer :: ix,iy,iz,it,is
    real(dp) :: t_in, t_out

    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate( r0(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate( phi_pr(nxp,nyp,nzp,ntp,ns))
    allocate( xi(nxp,nyp,nzp,ntp,ns))
    allocate( Mxi(nxp,nyp,nzp,ntp,ns))
    allocate( xi_pr(nxp,nyp,nzp,ntp,ns))
    allocate( Mxi_pr(nxp,nyp,nzp,ntp,ns))

    norm_psi = fermion_norm(psi)

    call MatrixOperate(chi,Mxi)

    !residue = psi - Mchi
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       residue_x(is)%Cl = psi_x(is)%Cl - Mxi_x(is)%Cl
    end do; end do; end do; end do; end do

    epsilon = fermion_normsq(residue)

    r0 = residue

    delta = inner_product(r0,residue)

    phi = residue
    xi_pr = residue
    call MatrixOperate(xi_pr,Mxi_pr)

    delta_prime = inner_product(r0,Mxi_pr)

    iterations = 0

    do
       if (sqrt(epsilon)/norm_psi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit

       iterations = iterations + 1

       if ( DebugCGInvert ) then
          mpiprint '(I4,ES20.10,3(F15.10,F15.10))', iterations, sqrt(epsilon)/norm_psi 
       end if

       beta = delta/delta_prime

       !phi_pr = phi - beta*Mxi_pr
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi_prx(is)%Cl = phi_x(is)%Cl - beta*Mxi_prx(is)%Cl
       end do; end do; end do; end do; end do

       !xi = phi + phi_pr
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          xi_x(is)%Cl = phi_x(is)%Cl + phi_prx(is)%Cl
       end do; end do; end do; end do; end do
       call MatrixOperate(xi,Mxi)

       !residue = residue - beta*Mxi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = residue_x(is)%Cl - beta*Mxi_x(is)%Cl
       end do; end do; end do; end do; end do

       !chi = chi + beta*xi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          chi_x(is)%Cl = chi_x(is)%Cl + beta*xi_x(is)%Cl
       end do; end do; end do; end do; end do

       epsilon = fermion_normsq(residue)

       prevdelta = delta

       delta = inner_product(r0,residue)

       alpha = -delta/prevdelta

       !phi = residue - alpha*phi_pr
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi_x(is)%Cl = residue_x(is)%Cl - alpha*phi_prx(is)%Cl
       end do; end do; end do; end do; end do

       !xi_pr = phi - alpha*(phi_pr - alpha*xi_pr)
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          xi_prx(is)%Cl = phi_x(is)%Cl - alpha*(phi_prx(is)%Cl - alpha*xi_prx(is)%Cl)
       end do; end do; end do; end do; end do

       call MatrixOperate(xi_pr,Mxi_pr)
       delta_prime = inner_product(r0,Mxi_pr)
    end do

    deallocate(residue)
    deallocate( r0)
    deallocate(phi)
    deallocate( phi_pr)
    deallocate( xi)
    deallocate( Mxi)
    deallocate( xi_pr)
    deallocate( Mxi_pr)

  end subroutine CGSInvert

  subroutine MultiCGInvert_4x(n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate)
    ! begin args: n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate

    !A routine to solve the matrix equation (M+sigma)v = psi for a system of shifts sigma
    !sigma must be ordered such that sigma(1) contains the least convergent system
    ! (i.e. the system that converges slowest).

    !M _must_ be hermitian and positive definite.
    !Assumed: tolerance >= epsilon(1.0d0)

    integer, intent(in) :: n_v !number of shifts
    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:,:) :: chi_i !solution vectors
    real(dp), dimension(n_v) :: sigma_i
    real(DP) :: tolerance !the precision desired for the solution
    integer, dimension(n_v) :: iterations_i !convergence information
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    integer :: iterations !the total number of iterations required
    real(DP) :: sigmaprime_0, sigmaprime_i(n_v) !relative matrix shifts
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, Mphi
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: phi_i
    real(DP) :: normsq_r, prevnormsq_r, beta, prevbeta, alpha, phidotMphi, offset, tau = 1.0d-1, normpsi, normphi
    real(DP), dimension(n_v) :: alpha_i, beta_i, zeta_i, prevzeta_i, nextzeta_i
    logical, dimension(n_v) :: converged_i, prevconverged_i
    integer :: ix,iy,iz,it,is,i_v

    !translate the sigma shifts so that sigmaprime_i(1)=0

    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate( Mphi(nxp,nyp,nzp,ntp,ns))
    allocate(phi_i(nxp,nyp,nzp,ntp,ns,n_v))

    sigmaprime_0 = sigma_i(1)
    sigmaprime_i = sigma_i - sigmaprime_0

    residue = psi

    chi_i = zero_vector

    do i_v=1,n_v
       phi_i(:,:,:,:,:,i_v) = residue
    end do

    beta = 1.0d0
    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    alpha_i = 0
    alpha = 0

    normsq_r = fermion_normsq(residue)
    normpsi = sqrt(normsq_r)

    phi = residue

    call MatrixOperate(phi,Mphi)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       Mphi_x(is)%Cl = Mphi_x(is)%Cl + sigmaprime_0*phi_x(is)%Cl
    end do; end do; end do; end do; end do
    phidotMphi = real_inner_product(phi,Mphi)

    iterations = 0
    iterations_i = 0
    converged_i = .false.

    do
       !If the slowest converegence is extremely slow compared to the other shifts, we need to have a "safety net"
       !that prevents underflow in zeta_i for the already converged values.
       prevconverged_i = converged_i
       converged_i = (sqrt(normsq_r)*zeta_i/normpsi < tau*tolerance )
       do i_v=1,n_v
          if (converged_i(i_v)) zeta_i(i_v) = epsilon(1.0d0)
          if (converged_i(i_v) .and. .not. prevconverged_i(i_v)) iterations_i(i_v) = iterations
       end do

       if (sqrt(normsq_r)/normpsi < tolerance) iterations_i(1) = iterations
       if (sqrt(normsq_r)/normpsi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit
       
       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(normsq_r), alpha, beta, phidotMphi
       end if

       iterations = iterations + 1

       beta = -normsq_r/phidotMphi

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-sigmaprime_i*beta) )
       beta_i = beta*nextzeta_i/zeta_i

       do i_v=1,n_v
          if (.not. converged_i(i_v)) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                chi_ix(is)%Cl = chi_ix(is)%Cl - beta_i(i_v)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = residue_x(is)%Cl + beta*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do

       prevnormsq_r = normsq_r
       normsq_r = fermion_normsq(residue)

       alpha = normsq_r/prevnormsq_r
       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       do i_v=1,n_v
          if (.not. converged_i(i_v) ) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                phi_ix(is)%Cl = nextzeta_i(i_v)*residue_x(is)%Cl + alpha_i(i_v)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       phi = phi_i(:,:,:,:,:,1)
       call MatrixOperate(phi,Mphi)
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Mphi_x(is)%Cl = Mphi_x(is)%Cl + sigmaprime_0*phi_x(is)%Cl
       end do; end do; end do; end do; end do
       phidotMphi = real_inner_product(phi,Mphi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

    end do

    deallocate(residue)
    deallocate(phi)
    deallocate( Mphi)
    deallocate(phi_i)

  end subroutine MultiCGInvert_4x

  subroutine MultiCGInvert_z_4x(n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate)
    ! begin args: n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate

    !A routine to solve the matrix equation (M+sigma)v = psi for a system of shifts sigma
    !sigma must be ordered such that sigma(1) contains the least convergent system
    ! (i.e. the system that converges slowest).

    !M _must_ be hermitian and positive definite.
    !Assumed: tolerance >= epsilon(1.0d0)

    integer, intent(in) :: n_v !number of shifts
    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:,:) :: chi_i !solution vectors
    complex(dc), dimension(n_v) :: sigma_i
    real(DP) :: tolerance !the precision desired for the solution
    integer, dimension(n_v) :: iterations_i !convergence information
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    integer :: iterations !the total number of iterations required
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, Mphi, Mchi
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: phi_i
    real(DP) :: normsq_r, prevnormsq_r, beta, prevbeta, alpha, phidotMphi, normpsi, normphi
    complex(dc), dimension(n_v) :: alpha_i, beta_i, zeta_i, prevzeta_i, nextzeta_i
    logical, dimension(n_v) :: converged_i, prevconverged_i
    integer :: ix,iy,iz,it,is,i_v
    real(dp) :: err

    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate( Mphi(nxp,nyp,nzp,ntp,ns))
    allocate( Mchi(nxp,nyp,nzp,ntp,ns))
    allocate(phi_i(nxp,nyp,nzp,ntp,ns,n_v))

    residue = psi

    chi_i = zero_vector

    phi = residue
    do i_v=1,n_v
       phi_i(:,:,:,:,:,i_v) = residue
    end do

    beta = 1.0d0
    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    alpha_i = 0
    alpha = 0.0d0

    normsq_r = fermion_normsq(residue)
    normpsi = sqrt(normsq_r)

    call MatrixOperate(phi,Mphi)
    phidotMphi = real_inner_product(phi,Mphi)

    iterations = 0
    iterations_i = 0
    converged_i = .false.

    do
       !If the slowest converegence is extremely slow compared to the other shifts, we need to have a "safety net"
       !that prevents underflow in zeta_i for the already converged values.
       prevconverged_i = converged_i
       converged_i = (sqrt(normsq_r)*abs(zeta_i)/normpsi < tolerance )
       do i_v=1,n_v
          if (converged_i(i_v)) zeta_i(i_v) = epsilon(1.0d0)
          if (converged_i(i_v) .and. .not. prevconverged_i(i_v)) iterations_i(i_v) = iterations
       end do

       if (all(converged_i)) exit
       if ( iterations > max_cgsolve_iter ) then
          mpiprint *, "MultiCG: Failed to converge", tolerance
          do i_v=1,n_v
             mpiprint '(I5,4F20.15)', i_v, abs(zeta_i(i_v))*sqrt(normsq_r)/normpsi, normpsi, sigma_i(i_v)
          end do

          call MPIBarrier
          call Abort("MultiCG failed.")
       end if

       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(normsq_r), alpha, beta, phidotMphi
       end if
       !mpiprint '(I4,F20.16,5ES10.2)', iterations, sqrt(normsq_r)/normpsi, sqrt(normsq_r)*abs(zeta_i)/normpsi

       iterations = iterations + 1

       beta = -normsq_r/phidotMphi

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-sigma_i*beta) )
       beta_i = beta*nextzeta_i/zeta_i

       do i_v=1,n_v
          if (.not. converged_i(i_v)) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                chi_ix(is)%Cl = chi_ix(is)%Cl - beta_i(i_v)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = residue_x(is)%Cl + beta*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do

       prevnormsq_r = normsq_r
       normsq_r = fermion_normsq(residue)

       alpha = normsq_r/prevnormsq_r
       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi_x(is)%Cl = residue_x(is)%Cl + alpha*phi_x(is)%Cl
       end do; end do; end do; end do; end do

       do i_v=1,n_v
          if (.not. converged_i(i_v) ) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                phi_ix(is)%Cl = nextzeta_i(i_v)*residue_x(is)%Cl + alpha_i(i_v)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       call MatrixOperate(phi,Mphi)
       phidotMphi = real_inner_product(phi,Mphi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

    end do

    !mpiprint '(16I5)', iterations, iterations_i

    !do i_v=1,n_v
    !   phi = chi_i(:,:,:,:,:,i_v)
    !   call MatrixOperate(phi,Mphi)
    !   call PsiPlusAlphaPhi(Mphi,sigma_i(i_v),phi)
    !   call PsiMinusPhi(Mphi,psi)
    !   err = fermion_norm(Mphi)
    !   mpiprint '(2I5,F20.15)', i_v, iterations_i(i_v), err/normpsi
    !end do
    deallocate(residue)
    deallocate(phi)
    deallocate( Mphi)
    deallocate( Mchi)
    deallocate(phi_i)

  end subroutine MultiCGInvert_z_4x

  subroutine MultiCRInvert_4x(n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate)
    ! begin args: n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate

    !A routine to solve the matrix equation (M+sigma)v = psi for a system of shifts sigma
    !sigma must be ordered such that sigma(1) contains the least convergent system
    ! (i.e. the system that converges slowest).

    !M _must_ be hermitian.
    !Assumed: tolerance >= epsilon(1.0d0)

    integer, intent(in) :: n_v !number of shifts
    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:,:) :: chi_i !solution vectors
    complex(dc), dimension(n_v) :: sigma_i
    real(DP) :: tolerance !the precision desired for the solution
    integer, dimension(n_v) :: iterations_i !convergence information
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    integer :: iterations !the total number of iterations required
    real(DP) :: sigmaprime_0, sigmaprime_i(n_v) !relative matrix shifts
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue, Mres
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, Mphi
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: phi_i
    real(DP) :: normsq_r, beta, prevbeta, alpha, phidotMphi, offset, tau = 1.0d-1, normpsi, normphi
    complex(dc), dimension(n_v) :: alpha_i, beta_i, zeta_i, prevzeta_i, nextzeta_i
    logical, dimension(n_v) :: converged_i, prevconverged_i
    integer :: ix,iy,iz,it,is,i_v
    real(dp) :: deltaM, prevdeltaM

    !translate the sigma shifts so that sigmaprime_i(1)=0

    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate( Mres(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate( Mphi(nxp,nyp,nzp,ntp,ns))
    allocate(phi_i(nxp,nyp,nzp,ntp,ns,n_v))

    residue = psi

    chi_i = zero_vector

    do i_v=1,n_v
       phi_i(:,:,:,:,:,i_v) = residue
    end do

    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    alpha_i = 0

    normsq_r = fermion_normsq(residue)
    normpsi = sqrt(normsq_r)

    call MatrixOperate(residue,Mres)
    deltaM = real_inner_product(residue,Mres)

    phi = residue

    call MatrixOperate(phi,Mphi)
    phidotMphi = real_inner_product(Mphi,Mphi)

    iterations = 0
    iterations_i = 0
    converged_i = .false.

    do
       !If the slowest converegence is extremely slow compared to the other shifts, we need to have a "safety net"
       !that prevents underflow in zeta_i for the already converged values.
       prevconverged_i = converged_i
       converged_i = (sqrt(normsq_r)*abs(zeta_i)/normpsi < tolerance )
       do i_v=1,n_v
          if (converged_i(i_v)) zeta_i(i_v) = epsilon(1.0d0)
          if (converged_i(i_v) .and. .not. prevconverged_i(i_v)) iterations_i(i_v) = iterations
       end do

       if ( all(converged_i) ) exit

       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(normsq_r), alpha, beta, phidotMphi
       end if
       if ( iterations > max_cgsolve_iter ) exit

       iterations = iterations + 1

       beta = -deltaM/phidotMphi

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-sigma_i*beta) )
       beta_i = beta*nextzeta_i/zeta_i

       do i_v=1,n_v
          if (.not. converged_i(i_v)) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                chi_ix(is)%Cl = chi_ix(is)%Cl - beta_i(i_v)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = residue_x(is)%Cl + beta*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do

       normsq_r = fermion_normsq(residue)

       prevdeltaM = deltaM
       call MatrixOperate(residue,Mres)
       deltaM = real_inner_product(residue,Mres)

       alpha = deltaM/prevdeltaM
       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       do i_v=1,n_v
          if (.not. converged_i(i_v) ) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                phi_ix(is)%Cl = nextzeta_i(i_v)*residue_x(is)%Cl + alpha_i(i_v)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi_x(is)%Cl = residue_x(is)%Cl +alpha*phi_x(is)%Cl
       end do; end do; end do; end do; end do

       call MatrixOperate(phi,Mphi)
       phidotMphi = real_inner_product(Mphi,Mphi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

    end do

    deallocate(residue)
    deallocate( Mres)
    deallocate(phi)
    deallocate( Mphi)
    deallocate(phi_i)

  end subroutine MultiCRInvert_4x

  subroutine MultiBiCGStabInvert(n_v, psi, chi_i, sigma_i, sigma_0, tolerance, iterations_i, MatrixOperate)
    ! begin args: n_v, psi, chi_i, sigma_i, sigma_0, tolerance, iterations_i, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi

    integer, intent(in) :: n_v !number of shifts
    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:,:) :: chi_i !solution vector and initial guess
    complex(dc), dimension(n_v) :: sigma_i
    complex(dc) :: sigma_0
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations_i(n_v) !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue, r0, prevres
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, Mphi, xi, Mxi
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: xi_i !solution vector and initial guess
    complex(dc) :: sigmaprime_0, sigmaprime_i(n_v) !relative matrix shifts
    real(dp) :: normsq_r, norm_psi
    complex(dc) :: delta, delta_prime, omega, beta, theta, prevbeta, alpha
    complex(dc), dimension(n_v) :: prevzeta_i, zeta_i,  nextzeta_i, prevrho_i, rho_i, nextrho_i, alpha_i, beta_i
    complex(dc), dimension(n_v) :: mu_i, nu_i, omega_i, theta_i
    integer :: ix,iy,iz,it,is,i_v
    real(dp) :: t_in, t_out
    integer :: iterations
    logical, dimension(n_v) :: converged_i, prevconverged_i


    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate( r0(nxp,nyp,nzp,ntp,ns))
    allocate( prevres(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate( Mphi(nxp,nyp,nzp,ntp,ns))
    allocate( xi(nxp,nyp,nzp,ntp,ns))
    allocate( Mxi(nxp,nyp,nzp,ntp,ns))
    allocate(xi_i(nxp,nyp,nzp,ntp,ns,n_v))

    residue = psi
    prevres = residue

    chi_i = zero_vector

    xi = residue
    do i_v=1,n_v
       xi_i(:,:,:,:,:,i_v) = residue
    end do

    beta = 1.0d0
    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    prevrho_i = 1.0d0
    rho_i = 1.0d0
    alpha = 0.0d0
    alpha_i = 0.0d0
    theta = 1.0d0
    theta_i = 1.0d0

    normsq_r = fermion_normsq(residue)
    norm_psi = sqrt(normsq_r)

    r0 = residue
    delta = inner_product(r0,residue)

    call MatrixOperate(xi,Mxi)
    call PsiPlusAlphaPhi(Mxi,sigma_0,xi)

    delta_prime = inner_product(r0,Mxi)

    iterations = 0
    iterations_i = 0
    converged_i = .false.

    do
       prevconverged_i = converged_i
       converged_i = (sqrt(normsq_r)*abs(rho_i)*abs(zeta_i)/norm_psi < tolerance )

       do i_v=1,n_v
          if (converged_i(i_v)) zeta_i(i_v) = epsilon(1.0d0)
          if (converged_i(i_v) .and. .not. prevconverged_i(i_v)) iterations_i(i_v) = iterations
       end do

       if (all(converged_i)) exit
       if ( iterations > max_cgsolve_iter ) then
          mpiprint *, "MultiBiCGStab: Failed to converge", tolerance
          do i_v=1,n_v
             mpiprint '(I5,F20.15)', i_v, abs(zeta_i(i_v))*abs(rho_i(i_v))*sqrt(normsq_r)/norm_psi
          end do

          call MPIBarrier
          call Abort("MultiBiCGStab failed.")
       end if

       !mpiprint '(I4,16ES10.2)', iterations, sqrt(normsq_r), abs(zeta_i), abs(rho_i),  sqrt(normsq_r)*abs(rho_i)*abs(zeta_i)/norm_psi !, omega, theta, beta

       !mpiprint '(I4,65ES10.2)', iterations, sqrt(normsq_r)/norm_psi !, sqrt(normsq_r)*abs(rho_i)*abs(zeta_i)/norm_psi
       !mpiprint '(I4,2ES20.10,3(F15.10,F15.10))', iterations, sqrt(normsq_r),  sqrt(normsq_r)/norm_psi !, omega, theta, beta
       if ( DebugCGInvert ) then
          mpiprint '(I4,F15.10,3(F15.10,F15.10))', iterations, sqrt(normsq_r), beta, theta, alpha
       end if

       iterations = iterations + 1
       !iterations_i = iterations

       !res2(iterations) = sqrt(normsq_r)/norm_psi
       !res3(iterations) = sqrt(normsq_r)*abs(rho_i(5))*abs(zeta_i(5))/norm_psi
       !if ( iterations >= 10 ) exit


       beta = - delta/delta_prime

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-(sigma_i-sigma_0)*beta) )
       beta_i = beta*nextzeta_i/zeta_i

       !phi = residue + beta*Mxi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi_x(is)%Cl = residue_x(is)%Cl + beta*Mxi_x(is)%Cl
       end do; end do; end do; end do; end do

       call MatrixOperate(phi,Mphi)
       call PsiPlusAlphaPhi(Mphi,sigma_0,phi)

       theta = inner_product(Mphi,phi)/fermion_normsq(Mphi)

       theta_i = theta/(1.0d0+theta*(sigma_i-sigma_0))
       nextrho_i = rho_i/(1.0d0+theta*(sigma_i-sigma_0))

       !residue = phi - theta*Mphi
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = phi_x(is)%Cl - theta*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do

       normsq_r = fermion_normsq(residue)

       omega_i = theta_i*nextzeta_i*rho_i

       !chi_i = chi_i - beta_i*xi_i + theta_i*nextzeta_i*rho_i*phi
       do i_v=1,n_v
          if ( .not. converged_i(i_v) ) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                chi_ix(is)%Cl = chi_ix(is)%Cl - beta_i(i_v)*xi_ix(is)%Cl + omega_i(i_v)*phi_x(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       delta = inner_product(r0,residue)

       alpha = delta/(delta_prime*theta)

       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       !xi = residue + alpha*(xi - theta*Mxi)
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          xi_x(is)%Cl = residue_x(is)%Cl + alpha*(xi_x(is)%Cl - theta*Mxi_x(is)%Cl)
       end do; end do; end do; end do; end do

       call MatrixOperate(xi,Mxi)
       call PsiPlusAlphaPhi(Mxi,sigma_0,xi)

       mu_i = nextzeta_i * nextrho_i
       omega_i = omega_i/beta_i
       nu_i = theta_i*zeta_i*rho_i/beta_i
       !xi_i = mu_i*residue + alpha_i*(xi_i - omega_i*phi + nu_i*prevresidue)
       do i_v=1,n_v
          if ( .not. converged_i(i_v) ) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                xi_ix(is)%Cl = mu_i(i_v)*residue_x(is)%Cl + alpha_i(i_v)*(xi_ix(is)%Cl - &
                     & omega_i(i_v)*phi_x(is)%Cl + nu_i(i_v)*prevres_x(is)%Cl)
             end do; end do; end do; end do; end do
          end if
       end do

       delta_prime = inner_product(r0,Mxi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

       prevrho_i = rho_i
       rho_i = nextrho_i

       prevres = residue
    end do

    !mpiprint '(A,65I4)', "Converged ", iterations_i

    deallocate(residue)
    deallocate( r0)
    deallocate( prevres)
    deallocate(phi)
    deallocate( Mphi)
    deallocate( xi)
    deallocate( Mxi)
    deallocate(xi_i)

  end subroutine MultiBiCGStabInvert

#undef residue_x
#undef psi_x
#undef phi_x
#undef Mphi_x
#undef chi_x
#undef xi_x
#undef Mxi_x
#undef phi_ix
#undef chi_ix
#undef xi_ix
#undef prevres_x
#undef phi_prx
#undef xi_prx
#undef Mxi_prx
#undef phi_iprx

#define residue_x residue(i_xeo)
#define psi_x psi(i_xeo)
#define phi_x phi(i_xeo)
#define Mphi_x Mphi(i_xeo)
#define chi_x chi(i_xeo)
#define xi_x xi(i_xeo)
#define Mxi_x Mxi(i_xeo)
#define phi_ix phi_i(i_xeo,i_v)
#define chi_ix chi_i(i_xeo,i_v)
#define xi_ix xi_i(i_xeo,i_v)
#define prevres_x prevres(i_xeo)
#define phi_prx phi_pr(i_xeo)
#define xi_prx xi_pr(i_xeo)
#define Mxi_prx Mxi_pr(i_xeo)
#define phi_iprx phi_ipr(i_xeo,i_v)

  subroutine CGInvert_eo(psi, chi, tolerance, iterations, MatrixOperate)
    ! begin args: psi, chi, tolerance, iterations, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi
    !M _must_ be hermitian and positive definite.

    type(dirac_fermion), dimension(:) :: psi !source vector
    type(dirac_fermion), dimension(:) :: chi !solution vector and initial guess
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         type(dirac_fermion), dimension(:) :: phi
         type(dirac_fermion), dimension(:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: residue
    type(dirac_fermion), dimension(:), allocatable :: phi, Mphi
    real(dp) :: delta, prevdelta, norm_psi, omega, theta, phidotMphi
    integer :: i_xeo
    complex(dc) :: test

    ! begin execution
    allocate(residue(n_xpeo))
    allocate(phi(n_xpeo))
    allocate(Mphi(n_xpeo))

    norm_psi = fermion_norm(psi)
    !mpiprint *, "norm ", norm_psi

    call MatrixOperate(chi, Mphi)

    !residue = psi - Mphi
    do i_xeo=1,n_xeo
       residue_x%cs(:,:) = psi_x%cs(:,:) - Mphi_x%cs(:,:)
    end do

    phi = residue

    delta = fermion_normsq(residue)

    call MatrixOperate(phi,Mphi)

    phidotMphi = real_inner_product(phi,Mphi)
    test = inner_product(phi,Mphi)

    iterations = 0

    do
       if (sqrt(delta)/norm_psi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit

       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(delta), theta, omega, phidotMphi
       end if

       iterations = iterations + 1

       omega = delta/phidotMphi

       !residue = residue - omega*Mphi
       do i_xeo=1,n_xeo
          residue_x%cs(:,:) = residue_x%cs(:,:) - omega*Mphi_x%cs(:,:)
       end do

       !chi = chi + omega*phi
       do i_xeo=1,n_xeo
          chi_x%cs(:,:) = chi_x%cs(:,:) + omega*phi_x%cs(:,:)
       end do

       prevdelta = delta
       delta = fermion_normsq(residue)

       theta = -delta/prevdelta

       !phi = residue - theta*phi
       do i_xeo=1,n_xeo
          phi_x%cs(:,:) = residue_x%cs(:,:) - theta*phi_x%cs(:,:)
       end do

       call MatrixOperate(phi,Mphi)

       phidotMphi = real_inner_product(phi,Mphi)
       test = inner_product(phi,Mphi)

    end do

    deallocate(residue)
    deallocate(phi)
    deallocate(Mphi)

  end subroutine CGInvert_eo

  subroutine ConjResInvert_eo(psi, chi, tolerance, iterations, MatrixOperate)
    ! begin args: psi, chi, tolerance, iterations, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi
    !M _must_ be hermitian.

    type(dirac_fermion), dimension(:) :: psi !source vector
    type(dirac_fermion), dimension(:) :: chi !solution vector and initial guess
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         type(dirac_fermion), dimension(:) :: phi
         type(dirac_fermion), dimension(:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: residue, Mres
    type(dirac_fermion), dimension(:), allocatable :: phi, Mphi
    real(dp) :: delta, prevdelta, norm_psi, omega, theta, phidotMphi, deltaM, prevdeltaM
    integer :: i_xeo
    complex(dc) :: test

    ! begin execution
    allocate(residue(n_xpeo))
    allocate(Mres(n_xpeo))
    allocate(phi(n_xpeo))
    allocate(Mphi(n_xpeo))

    norm_psi = fermion_norm(psi)

    call MatrixOperate(chi, Mphi)

    !residue = psi - Mphi
    do i_xeo=1,n_xeo
       residue_x%cs(:,:) = psi_x%cs(:,:) - Mphi_x%cs(:,:)
    end do

    phi = residue

    delta = fermion_normsq(residue)

    call MatrixOperate(residue,Mres)
    deltaM = real_inner_product(residue,Mres)

    call MatrixOperate(phi,Mphi)

    phidotMphi = real_inner_product(Mphi,Mphi)

    iterations = 0

    do
       if (sqrt(delta)/norm_psi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit

       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(delta), theta, omega, phidotMphi
       end if

       iterations = iterations + 1

       omega = deltaM/phidotMphi

       !residue = residue - omega*Mphi
       do i_xeo=1,n_xeo
          residue_x%cs(:,:) = residue_x%cs(:,:) - omega*Mphi_x%cs(:,:)
       end do

       !chi = chi + omega*phi
       do i_xeo=1,n_xeo
          chi_x%cs(:,:) = chi_x%cs(:,:) + omega*phi_x%cs(:,:)
       end do

       delta = fermion_normsq(residue)

       prevdeltaM = deltaM
       call MatrixOperate(residue,Mres)
       deltaM = real_inner_product(residue,Mres)

       theta = -deltaM/prevdeltaM

       !phi = residue - theta*phi
       do i_xeo=1,n_xeo
          phi_x%cs(:,:) = residue_x%cs(:,:) - theta*phi_x%cs(:,:)
       end do

       call MatrixOperate(phi,Mphi)

       phidotMphi = real_inner_product(Mphi,Mphi)

    end do

    deallocate(residue)
    deallocate(Mres)
    deallocate(phi)
    deallocate(Mphi)

  end subroutine ConjResInvert_eo

  subroutine BiCGStabInvert_eo(psi, chi, tolerance, iterations, MatrixOperate)
    ! begin args: psi, chi, tolerance, iterations, MatrixOperate

    !A routine to solve the matrix equation M chi = psi, using an initial guess chi

    type(dirac_fermion), dimension(:) :: psi !source vector
    type(dirac_fermion), dimension(:) :: chi !solution vector and initial guess
    real(dp) :: tolerance !the precision desired for the solution
    integer :: iterations !the total number of iterations required
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         type(dirac_fermion), dimension(:) :: phi
         type(dirac_fermion), dimension(:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: residue, r0
    type(dirac_fermion), dimension(:), allocatable :: phi, Mphi, xi, Mxi
    real(dp) :: epsilon, norm_psi
    complex(dc) :: delta, delta_prime, omega, beta, theta
    integer :: i_xeo
    real(dp) :: t_in, t_out

    ! begin execution
    allocate(residue(n_xpeo))
    allocate(r0(n_xpeo))
    allocate(phi(n_xpeo))
    allocate(Mphi(n_xpeo))
    allocate(xi(n_xpeo))
    allocate(Mxi(n_xpeo))

    norm_psi = fermion_norm(psi)

    call MatrixOperate(chi,Mphi)

    !residue = psi - Mphi
    do i_xeo=1,n_xeo
       residue_x%cs(:,:) = psi_x%cs(:,:) - Mphi_x%cs(:,:)
    end do

    epsilon = fermion_normsq(residue)

    r0 = residue
    delta = inner_product(r0,residue)

    xi = residue
    call MatrixOperate(xi,Mxi)

    delta_prime = inner_product(r0,Mxi)

    iterations = 0

    do
       if (sqrt(epsilon)/norm_psi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit

       iterations = iterations + 1

       !res1(iterations) = sqrt(epsilon)/norm_psi
       !if ( iterations >= 500 ) exit

       !print '(I4,ES20.10,3(F15.10,F15.10))', iterations, sqrt(epsilon)/norm_psi !, omega, theta, beta
       if ( DebugCGInvert ) then
          mpiprint '(I4,F20.10,3(F15.10,F15.10))', iterations, sqrt(epsilon), omega, theta, beta
       end if

       omega = delta/delta_prime

       !phi = residue - omega*Mxi
       do i_xeo=1,n_xeo
          phi_x%cs(:,:) = residue_x%cs(:,:) - omega*Mxi_x%cs(:,:)
       end do

       call MatrixOperate(phi,Mphi)

       theta = inner_product(Mphi,phi)/fermion_normsq(Mphi)

       !residue = phi - theta*Mphi
       do i_xeo=1,n_xeo
          residue_x%cs(:,:) = phi_x%cs(:,:) - theta*Mphi_x%cs(:,:)
       end do

       epsilon = fermion_normsq(residue)

       !chi = chi + omega*xi + theta*phi
       do i_xeo=1,n_xeo
          chi_x%cs(:,:) = chi_x%cs(:,:) + omega*xi_x%cs(:,:) + theta*phi_x%cs(:,:)
       end do

       delta = inner_product(r0,residue)

       beta = -delta/(delta_prime*theta)

       !xi = residue - beta*(xi - theta*Mxi)
       do i_xeo=1,n_xeo
          xi_x%cs(:,:) = residue_x%cs(:,:) - beta*(xi_x%cs(:,:) - theta*Mxi_x%cs(:,:))
       end do

       call MatrixOperate(xi,Mxi)

       delta_prime = inner_product(r0,Mxi)

    end do

    deallocate(residue)
    deallocate(r0)
    deallocate(phi)
    deallocate(Mphi)
    deallocate(xi)
    deallocate(Mxi)

  end subroutine BiCGStabInvert_eo

  subroutine MultiCGInvert_eo(n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate)
    ! begin args: n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate

    !A routine to solve the matrix equation (M+sigma)v = psi for a system of shifts sigma
    !sigma must be ordered such that sigma(1) contains the least convergent system
    ! (i.e. the system that converges slowest).

    !M _must_ be hermitian and positive definite.
    !Assumed: tolerance >= epsilon(1.0d0)

    integer, intent(in) :: n_v !number of shifts
    type(dirac_fermion), dimension(:) :: psi !source vector
    type(dirac_fermion), dimension(:,:) :: chi_i !solution vectors
    real(dp), dimension(n_v) :: sigma_i
    real(DP) :: tolerance !the precision desired for the solution
    integer, dimension(n_v) :: iterations_i !convergence information
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         type(dirac_fermion), dimension(:) :: phi
         type(dirac_fermion), dimension(:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    integer :: iterations !the total number of iterations required
    real(DP) :: sigmaprime_0, sigmaprime_i(n_v) !relative matrix shifts
    type(dirac_fermion), dimension(:), allocatable :: residue
    type(dirac_fermion), dimension(:), allocatable :: phi, Mphi
    type(dirac_fermion), dimension(:,:), allocatable :: phi_i
    real(DP) :: normsq_r, prevnormsq_r, beta, prevbeta, alpha, phidotMphi, offset, tau = 1.0d-1, normpsi, normphi
    real(DP), dimension(n_v) :: alpha_i, beta_i, zeta_i, prevzeta_i, nextzeta_i
    logical, dimension(n_v) :: converged_i, prevconverged_i
    integer :: i_xeo,i_v

    !translate the sigma shifts so that sigmaprime_i(1)=0

    ! begin execution
    allocate(residue(n_xpeo))
    allocate(phi(n_xpeo))
    allocate(Mphi(n_xpeo))
    allocate(phi_i(n_xpeo,n_v))

    sigmaprime_0 = sigma_i(1)
    sigmaprime_i = sigma_i - sigmaprime_0

    residue = psi

    chi_i = zero_dirac_fermion

    do i_v=1,n_v
       phi_i(:,i_v) = residue
    end do

    beta = 1.0d0
    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    alpha_i = 0
    alpha = 0

    normsq_r = fermion_normsq(residue)
    normpsi = sqrt(normsq_r)

    phi = residue

    call MatrixOperate(phi,Mphi)
    do i_xeo=1,n_xeo
       Mphi_x%cs(:,:) = Mphi_x%cs(:,:) + sigmaprime_0*phi_x%cs(:,:)
    end do
    phidotMphi = real_inner_product(phi,Mphi)

    iterations = 0
    iterations_i = 0
    converged_i = .false.

    do
       !If the slowest converegence is extremely slow compared to the other shifts, we need to have a "safety net"
       !that prevents underflow in zeta_i for the already converged values.
       prevconverged_i = converged_i
       converged_i = (sqrt(normsq_r)*zeta_i/normpsi < tau*tolerance )
       do i_v=1,n_v
          if (converged_i(i_v)) zeta_i(i_v) = epsilon(1.0d0)
          if (converged_i(i_v) .and. .not. prevconverged_i(i_v)) iterations_i(i_v) = iterations
       end do

       if (sqrt(normsq_r)/normpsi < tolerance) iterations_i(1) = iterations
       if (sqrt(normsq_r)/normpsi < tolerance) exit
       if ( iterations > max_cgsolve_iter ) exit

       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(normsq_r), alpha, beta, phidotMphi
       end if

       iterations = iterations + 1

       beta = -normsq_r/phidotMphi

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-sigmaprime_i*beta) )
       beta_i = beta*nextzeta_i/zeta_i

       do i_v=1,n_v
          if (.not. converged_i(i_v)) then
             do i_xeo=1,n_xeo
                chi_ix%cs(:,:) = chi_ix%cs(:,:) - beta_i(i_v)*phi_ix%cs(:,:)
             end do
          end if
       end do

       do i_xeo=1,n_xeo
          residue_x%cs(:,:) = residue_x%cs(:,:) + beta*Mphi_x%cs(:,:)
       end do

       prevnormsq_r = normsq_r
       normsq_r = fermion_normsq(residue)

       alpha = normsq_r/prevnormsq_r
       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       do i_v=1,n_v
          if (.not. converged_i(i_v) ) then
             do i_xeo=1,n_xeo
                phi_ix%cs(:,:) = nextzeta_i(i_v)*residue_x%cs(:,:) + alpha_i(i_v)*phi_ix%cs(:,:)
             end do
          end if
       end do

       phi = phi_i(:,1)
       call MatrixOperate(phi,Mphi)
       do i_xeo=1,n_xeo
          Mphi_x%cs(:,:) = Mphi_x%cs(:,:) + sigmaprime_0*phi_x%cs(:,:)
       end do
       phidotMphi = real_inner_product(phi,Mphi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

    end do

    deallocate(residue)
    deallocate(phi)
    deallocate(Mphi)
    deallocate(phi_i)

  end subroutine MultiCGInvert_eo

  subroutine MultiCGInvert_z_eo(n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate)
    ! begin args: n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate

    !A routine to solve the matrix equation (M+sigma)v = psi for a system of shifts sigma
    !sigma must be ordered such that sigma(1) contains the least convergent system
    ! (i.e. the system that converges slowest).

    !M _must_ be hermitian and positive definite.
    !Assumed: tolerance >= epsilon(1.0d0)

    integer, intent(in) :: n_v !number of shifts
    type(dirac_fermion), dimension(:) :: psi !source vector
    type(dirac_fermion), dimension(:,:) :: chi_i !solution vectors
    complex(dc), dimension(n_v) :: sigma_i
    real(DP) :: tolerance !the precision desired for the solution
    integer, dimension(n_v) :: iterations_i !convergence information
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         type(dirac_fermion), dimension(:) :: phi
         type(dirac_fermion), dimension(:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    integer :: iterations !the total number of iterations required
    type(dirac_fermion), dimension(:), allocatable :: residue
    type(dirac_fermion), dimension(:), allocatable :: phi, Mphi, Mchi
    type(dirac_fermion), dimension(:,:), allocatable :: phi_i
    real(DP) :: normsq_r, prevnormsq_r, beta, prevbeta, alpha, phidotMphi, normpsi, normphi
    complex(dc), dimension(n_v) :: alpha_i, beta_i, zeta_i, prevzeta_i, nextzeta_i
    logical, dimension(n_v) :: converged_i, prevconverged_i
    integer :: i_xeo,i_v
    real(dp) :: err

    ! begin execution
    allocate(residue(n_xpeo))
    allocate(phi(n_xpeo))
    allocate(Mphi(n_xpeo))
    allocate(Mchi(n_xpeo))
    allocate(phi_i(n_xpeo,n_v))

    residue = psi

    chi_i = zero_dirac_fermion

    phi = residue
    do i_v=1,n_v
       phi_i(:,i_v) = residue
    end do

    beta = 1.0d0
    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    alpha_i = 0
    alpha = 0.0d0

    normsq_r = fermion_normsq(residue)
    normpsi = sqrt(normsq_r)

    call MatrixOperate(phi,Mphi)
    phidotMphi = real_inner_product(phi,Mphi)

    iterations = 0
    iterations_i = 0
    converged_i = .false.

    do
       !If the slowest converegence is extremely slow compared to the other shifts, we need to have a "safety net"
       !that prevents underflow in zeta_i for the already converged values.
       prevconverged_i = converged_i
       converged_i = (sqrt(normsq_r)*abs(zeta_i)/normpsi < tolerance )
       do i_v=1,n_v
          if (converged_i(i_v)) zeta_i(i_v) = epsilon(1.0d0)
          if (converged_i(i_v) .and. .not. prevconverged_i(i_v)) iterations_i(i_v) = iterations
       end do

       if (all(converged_i)) exit
       if ( iterations > max_cgsolve_iter ) then
          mpiprint *, "MultiCG: Failed to converge", tolerance
          do i_v=1,n_v
             mpiprint '(I5,4F20.15)', i_v, abs(zeta_i(i_v))*sqrt(normsq_r)/normpsi, normpsi, sigma_i(i_v)
          end do

          call MPIBarrier
          call Abort("MultiCG failed.")
       end if

       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(normsq_r), alpha, beta, phidotMphi
       end if
       !mpiprint '(I4,F20.16,5ES10.2)', iterations, sqrt(normsq_r)/normpsi, sqrt(normsq_r)*abs(zeta_i)/normpsi

       iterations = iterations + 1

       beta = -normsq_r/phidotMphi

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-sigma_i*beta) )
       beta_i = beta*nextzeta_i/zeta_i

       do i_v=1,n_v
          if (.not. converged_i(i_v)) then
             do i_xeo=1,n_xeo
                chi_ix%cs(:,:) = chi_ix%cs(:,:) - beta_i(i_v)*phi_ix%cs(:,:)
             end do
          end if
       end do

       do i_xeo=1,n_xeo
          residue_x%cs(:,:) = residue_x%cs(:,:) + beta*Mphi_x%cs(:,:)
       end do

       prevnormsq_r = normsq_r
       normsq_r = fermion_normsq(residue)

       alpha = normsq_r/prevnormsq_r
       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       do i_xeo=1,n_xeo
          phi_x%cs(:,:) = residue_x%cs(:,:) + alpha*phi_x%cs(:,:)
       end do

       do i_v=1,n_v
          if (.not. converged_i(i_v) ) then
             do i_xeo=1,n_xeo
                phi_ix%cs(:,:) = nextzeta_i(i_v)*residue_x%cs(:,:) + alpha_i(i_v)*phi_ix%cs(:,:)
             end do
          end if
       end do

       call MatrixOperate(phi,Mphi)
       phidotMphi = real_inner_product(phi,Mphi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

    end do

    !mpiprint '(16I5)', iterations, iterations_i

    !do i_v=1,n_v
    !   phi = chi_i(:,i_v)
    !   call MatrixOperate(phi,Mphi)
    !   call PsiPlusAlphaPhi(Mphi,sigma_i(i_v),phi)
    !   call PsiMinusPhi(Mphi,psi)
    !   err = fermion_norm(Mphi)
    !   mpiprint '(2I5,F20.15)', i_v, iterations_i(i_v), err/normpsi
    !end do
    deallocate(residue)
    deallocate(phi)
    deallocate(Mphi)
    deallocate(Mchi)
    deallocate(phi_i)

  end subroutine MultiCGInvert_z_eo

  subroutine MultiCRInvert_eo(n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate)
    ! begin args: n_v, psi, chi_i, sigma_i, tolerance, iterations_i, MatrixOperate

    !A routine to solve the matrix equation (M+sigma)v = psi for a system of shifts sigma
    !sigma must be ordered such that sigma(1) contains the least convergent system
    ! (i.e. the system that converges slowest).

    !M _must_ be hermitian.
    !Assumed: tolerance >= epsilon(1.0d0)

    integer, intent(in) :: n_v !number of shifts
    type(dirac_fermion), dimension(:) :: psi !source vector
    type(dirac_fermion), dimension(:,:) :: chi_i !solution vectors
    complex(dc), dimension(n_v) :: sigma_i
    real(DP) :: tolerance !the precision desired for the solution
    integer, dimension(n_v) :: iterations_i !convergence information
    interface
       subroutine MatrixOperate(phi, Dphi)
         use FermionTypes
         type(dirac_fermion), dimension(:) :: phi
         type(dirac_fermion), dimension(:) :: Dphi
       end subroutine MatrixOperate
    end interface

    ! begin local_vars
    integer :: iterations !the total number of iterations required
    real(DP) :: sigmaprime_0, sigmaprime_i(n_v) !relative matrix shifts
    type(dirac_fermion), dimension(:), allocatable :: residue, Mres
    type(dirac_fermion), dimension(:), allocatable :: phi, Mphi
    type(dirac_fermion), dimension(:,:), allocatable :: phi_i
    real(DP) :: normsq_r, beta, prevbeta, alpha, phidotMphi, offset, tau = 1.0d-1, normpsi, normphi
    complex(dc), dimension(n_v) :: alpha_i, beta_i, zeta_i, prevzeta_i, nextzeta_i
    logical, dimension(n_v) :: converged_i, prevconverged_i
    integer :: i_xeo,i_v
    real(dp) :: deltaM, prevdeltaM

    !translate the sigma shifts so that sigmaprime_i(1)=0

    ! begin execution
    allocate(residue(n_xpeo))
    allocate(Mres(n_xpeo))
    allocate(phi(n_xpeo))
    allocate(Mphi(n_xpeo))
    allocate(phi_i(n_xpeo,n_v))

    residue = psi

    chi_i = zero_dirac_fermion

    do i_v=1,n_v
       phi_i(:,i_v) = residue
    end do

    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    alpha_i = 0

    normsq_r = fermion_normsq(residue)
    normpsi = sqrt(normsq_r)

    call MatrixOperate(residue,Mres)
    deltaM = real_inner_product(residue,Mres)

    phi = residue

    call MatrixOperate(phi,Mphi)
    phidotMphi = real_inner_product(Mphi,Mphi)

    iterations = 0
    iterations_i = 0
    converged_i = .false.

    do
       !If the slowest converegence is extremely slow compared to the other shifts, we need to have a "safety net"
       !that prevents underflow in zeta_i for the already converged values.
       prevconverged_i = converged_i
       converged_i = (sqrt(normsq_r)*abs(zeta_i)/normpsi < tolerance )
       do i_v=1,n_v
          if (converged_i(i_v)) zeta_i(i_v) = epsilon(1.0d0)
          if (converged_i(i_v) .and. .not. prevconverged_i(i_v)) iterations_i(i_v) = iterations
       end do

       if ( all(converged_i) ) exit
       if ( iterations > max_cgsolve_iter ) exit
       if ( DebugCGInvert ) then
          mpiprint '(I4,4F20.16)', iterations, sqrt(normsq_r), alpha, beta, phidotMphi
       end if

       iterations = iterations + 1

       beta = -deltaM/phidotMphi

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-sigma_i*beta) )
       beta_i = beta*nextzeta_i/zeta_i

       do i_v=1,n_v
          if (.not. converged_i(i_v)) then
             do i_xeo=1,n_xeo
                chi_ix%cs(:,:) = chi_ix%cs(:,:) - beta_i(i_v)*phi_ix%cs(:,:)
             end do
          end if
       end do

       do i_xeo=1,n_xeo
          residue_x%cs(:,:) = residue_x%cs(:,:) + beta*Mphi_x%cs(:,:)
       end do

       normsq_r = fermion_normsq(residue)

       prevdeltaM = deltaM
       call MatrixOperate(residue,Mres)
       deltaM = real_inner_product(residue,Mres)

       alpha = deltaM/prevdeltaM
       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       do i_v=1,n_v
          if (.not. converged_i(i_v) ) then
             do i_xeo=1,n_xeo
                phi_ix%cs(:,:) = nextzeta_i(i_v)*residue_x%cs(:,:) + alpha_i(i_v)*phi_ix%cs(:,:)
             end do
          end if
       end do

       do i_xeo=1,n_xeo
          phi_x%cs(:,:) = residue_x%cs(:,:) +alpha*phi_x%cs(:,:)
       end do

       call MatrixOperate(phi,Mphi)
       phidotMphi = real_inner_product(Mphi,Mphi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

    end do

    deallocate(residue)
    deallocate(Mres)
    deallocate(phi)
    deallocate(Mphi)
    deallocate(phi_i)

  end subroutine MultiCRInvert_eo

#undef residue_x
#undef psi_x
#undef phi_x
#undef Mphi_x
#undef chi_x
#undef xi_x
#undef Mxi_x
#undef phi_ix
#undef chi_ix
#undef xi_ix
#undef prevres_x
#undef phi_prx
#undef xi_prx
#undef Mxi_prx
#undef phi_iprx

end module ConjGradSolvers

