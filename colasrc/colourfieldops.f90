module ColourFieldOps

  use Kinds
  use ColourTypes
  use SpinorTypes
  use FermionTypes
  use LatticeSize
  use MPIInterface
  implicit none
  private

  public :: AllocMatrixField
  public :: DeallocMatrixField
  public :: SendMatrixField
  public :: RecvMatrixField
  public :: AllocVectorField
  public :: DeallocVectorField
  public :: SendVectorField
  public :: RecvVectorField
  public :: CreateGaugeField
  public :: DestroyGaugeField
  public :: CreateFermionField
  public :: DestroyFermionField

  interface AllocMatrixField
    module procedure AllocMatrixField_1
    module procedure AllocMatrixField_2
    module procedure AllocMatrixField_3
    module procedure AllocMatrixField_4
    module procedure AllocMatrixField_5
    module procedure AllocMatrixField_6
    module procedure AllocMatrixField_7
  end interface 

  interface DeallocMatrixField
    module procedure DeallocMatrixField_1
    module procedure DeallocMatrixField_2
    module procedure DeallocMatrixField_3
    module procedure DeallocMatrixField_4
    module procedure DeallocMatrixField_5
    module procedure DeallocMatrixField_6
    module procedure DeallocMatrixField_7
  end interface 

  interface SendMatrixField
    module procedure SendMatrixField_1
    module procedure SendMatrixField_2
    module procedure SendMatrixField_3
    module procedure SendMatrixField_4
    module procedure SendMatrixField_5
    module procedure SendMatrixField_6
    module procedure SendMatrixField_7
  end interface 

  interface RecvMatrixField
    module procedure RecvMatrixField_1
    module procedure RecvMatrixField_2
    module procedure RecvMatrixField_3
    module procedure RecvMatrixField_4
    module procedure RecvMatrixField_5
    module procedure RecvMatrixField_6
    module procedure RecvMatrixField_7
  end interface 

  interface AllocVectorField
    module procedure AllocVectorField_1
    module procedure AllocVectorField_2
    module procedure AllocVectorField_3
    module procedure AllocVectorField_4
    module procedure AllocVectorField_5
    module procedure AllocVectorField_6
    module procedure AllocVectorField_7
  end interface 

  interface DeallocVectorField
    module procedure DeallocVectorField_1
    module procedure DeallocVectorField_2
    module procedure DeallocVectorField_3
    module procedure DeallocVectorField_4
    module procedure DeallocVectorField_5
    module procedure DeallocVectorField_6
    module procedure DeallocVectorField_7
  end interface 

  interface SendVectorField
    module procedure SendVectorField_1
    module procedure SendVectorField_2
    module procedure SendVectorField_3
    module procedure SendVectorField_4
    module procedure SendVectorField_5
    module procedure SendVectorField_6
    module procedure SendVectorField_7
  end interface 

  interface RecvVectorField
    module procedure RecvVectorField_1
    module procedure RecvVectorField_2
    module procedure RecvVectorField_3
    module procedure RecvVectorField_4
    module procedure RecvVectorField_5
    module procedure RecvVectorField_6
    module procedure RecvVectorField_7
  end interface 

  interface CreateGaugeField
     module procedure CreateGaugeField_4x
     module procedure CreateGaugeField_eo
  end interface

  interface DestroyGaugeField
     module procedure DestroyGaugeField_4x
     module procedure DestroyGaugeField_eo
  end interface

  interface CreateFermionField
     module procedure CreateFermionField_4x
     module procedure CreateFermionField_eo
  end interface

  interface DestroyFermionField
     module procedure DestroyFermionField_4x
     module procedure DestroyFermionField_eo
  end interface

contains

  subroutine CreateGaugeField_4x(U_xd,nshdw)
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: U_xd
    integer :: nshdw

    integer :: nxd,nyd,nzd,ntd

    nxd = nx
    nyd = ny
    nzd = nz
    ntd = nt

    if ( nprocx > 1 ) nxd = nxd+2*nshdw
    if ( nprocy > 1 ) nyd = nyd+2*nshdw
    if ( nprocz > 1 ) nzd = nzd+2*nshdw
    if ( nproct > 1 ) ntd = ntd+2*nshdw

    if ( .not. allocated(U_xd) ) allocate(U_xd(nxd,nyd,nzd,ntd,nd))

  end subroutine CreateGaugeField_4x

  subroutine CreateGaugeField_eo(U_xd,nshdw)
    type(colour_matrix), dimension(:,:,:), allocatable :: U_xd
    integer :: nshdw

    integer :: nld_eo
    integer :: nxd, nyd, nzd, ntd

    ! shadowing in t direction requires 2 timeslices to be stored,
    ! so additional space of 2*(nx*ny*nz) in total,
    ! but this is split between even and odd lattices, so 
    ! only (nx*ny*nz) extra space for each parity.
    ! Similarly for x,y,z directions.

    nxd = nx
    nyd = ny
    nzd = nz
    ntd = nt

    if ( nprocx > 1 ) nxd = nxd+2*nshdw
    if ( nprocy > 1 ) nyd = nyd+2*nshdw
    if ( nprocz > 1 ) nzd = nzd+2*nshdw
    if ( nproct > 1 ) ntd = ntd+2*nshdw

    nld_eo = (nxd*nyd*nzd*ntd)/2

    if ( .not. allocated(U_xd) ) allocate(U_xd(nld_eo,nd,0:1))

  end subroutine CreateGaugeField_eo

  subroutine DestroyGaugeField_4x(U_xd)
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: U_xd

    if ( allocated(U_xd) ) deallocate(U_xd)
  end subroutine DestroyGaugeField_4x

  subroutine DestroyGaugeField_eo(U_xd)
    type(colour_matrix), dimension(:,:,:), allocatable :: U_xd

    if ( allocated(U_xd) ) deallocate(U_xd)
  end subroutine DestroyGaugeField_eo

  subroutine CreateFermionField_4x(phi,nshdw)
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi
    integer :: nshdw

    integer :: nxd,nyd,nzd,ntd

    nxd = nx
    nyd = ny
    nzd = nz
    ntd = nt

    if ( nprocx > 1 ) nxd = nxd+2*nshdw
    if ( nprocy > 1 ) nyd = nyd+2*nshdw
    if ( nprocz > 1 ) nzd = nzd+2*nshdw
    if ( nproct > 1 ) ntd = ntd+2*nshdw

    if ( .not. allocated(phi) ) allocate(phi(nxd,nyd,nzd,ntd,ns))

  end subroutine CreateFermionField_4x

  subroutine CreateFermionField_eo(phi,nshdw)
    type(dirac_fermion), dimension(:), allocatable :: phi
    integer :: nshdw

    integer :: nld_eo
    integer :: nxd,nyd,nzd,ntd

    ! shadowing in t direction requires 2 timeslices to be stored,
    ! so additional space of 2*(nx*ny*nz) in total,
    ! but this is split between even and odd lattices, so 
    ! only (nx*ny*nz) extra space for each parity.
    ! Similarly for x,y,z directions.

    nxd = nx
    nyd = ny
    nzd = nz
    ntd = nt

    if ( nprocx > 1 ) nxd = nxd+2*nshdw
    if ( nprocy > 1 ) nyd = nyd+2*nshdw
    if ( nprocz > 1 ) nzd = nzd+2*nshdw
    if ( nproct > 1 ) ntd = ntd+2*nshdw

    nld_eo = (nxd*nyd*nzd*ntd)/2

    if ( .not. allocated(phi) ) allocate(phi(nld_eo))

  end subroutine CreateFermionField_eo

  subroutine DestroyFermionField_4x(phi)
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DestroyFermionField_4x

  subroutine DestroyFermionField_eo(phi)
    type(dirac_fermion), dimension(:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DestroyFermionField_eo

  subroutine AllocMatrixField_1(U_x,sdim,idim)
    type(colour_matrix), dimension(:), allocatable :: U_x
    character(len=1) :: sdim
    integer, dimension(1) :: idim

    integer, dimension(1) :: n
    integer :: j

    do j=1,1
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(U_x) ) allocate(U_x(n(1)))
  end subroutine AllocMatrixField_1

  subroutine DeallocMatrixField_1(U_x)
    type(colour_matrix), dimension(:), allocatable :: U_x

    if ( allocated(U_x) ) deallocate(U_x)
  end subroutine DeallocMatrixField_1

  subroutine SendMatrixField_1(U_x, dest_rank)

    type(colour_matrix), dimension(:) :: U_x
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(U_x)

    call MPI_SSend(U_x, size(U_x)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendMatrixField_1

  subroutine RecvMatrixField_1(U_x, src_rank)

    type(colour_matrix), dimension(:) :: U_x
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(U_x)

    call MPI_Recv(U_x, size(U_x)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvMatrixField_1

  subroutine AllocVectorField_1(phi,sdim,idim)
    type(colour_vector), dimension(:), allocatable :: phi
    character(len=1) :: sdim
    integer, dimension(1) :: idim

    integer, dimension(1) :: n
    integer :: j

    do j=1,1
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(phi) ) allocate(phi(n(1)))
  end subroutine AllocVectorField_1

  subroutine DeallocVectorField_1(phi)
    type(colour_vector), dimension(:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DeallocVectorField_1

  subroutine SendVectorField_1(phi, dest_rank)

    type(colour_vector), dimension(:) :: phi
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendVectorField_1

  subroutine RecvVectorField_1(phi, src_rank)

    type(colour_vector), dimension(:) :: phi
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvVectorField_1

  subroutine AllocMatrixField_2(U_x,sdim,idim)
    type(colour_matrix), dimension(:,:), allocatable :: U_x
    character(len=2) :: sdim
    integer, dimension(2) :: idim

    integer, dimension(2) :: n
    integer :: j

    do j=1,2
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(U_x) ) allocate(U_x(n(1),n(2)))
  end subroutine AllocMatrixField_2

  subroutine DeallocMatrixField_2(U_x)
    type(colour_matrix), dimension(:,:), allocatable :: U_x

    if ( allocated(U_x) ) deallocate(U_x)
  end subroutine DeallocMatrixField_2

  subroutine SendMatrixField_2(U_x, dest_rank)

    type(colour_matrix), dimension(:,:) :: U_x
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(U_x)

    call MPI_SSend(U_x, size(U_x)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendMatrixField_2

  subroutine RecvMatrixField_2(U_x, src_rank)

    type(colour_matrix), dimension(:,:) :: U_x
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(U_x)

    call MPI_Recv(U_x, size(U_x)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvMatrixField_2

  subroutine AllocVectorField_2(phi,sdim,idim)
    type(colour_vector), dimension(:,:), allocatable :: phi
    character(len=2) :: sdim
    integer, dimension(2) :: idim

    integer, dimension(2) :: n
    integer :: j

    do j=1,2
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(phi) ) allocate(phi(n(1),n(2)))
  end subroutine AllocVectorField_2

  subroutine DeallocVectorField_2(phi)
    type(colour_vector), dimension(:,:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DeallocVectorField_2

  subroutine SendVectorField_2(phi, dest_rank)

    type(colour_vector), dimension(:,:) :: phi
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendVectorField_2

  subroutine RecvVectorField_2(phi, src_rank)

    type(colour_vector), dimension(:,:) :: phi
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvVectorField_2

  subroutine AllocMatrixField_3(U_x,sdim,idim)
    type(colour_matrix), dimension(:,:,:), allocatable :: U_x
    character(len=3) :: sdim
    integer, dimension(3) :: idim

    integer, dimension(3) :: n
    integer :: j

    do j=1,3
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(U_x) ) allocate(U_x(n(1),n(2),n(3)))
  end subroutine AllocMatrixField_3

  subroutine DeallocMatrixField_3(U_x)
    type(colour_matrix), dimension(:,:,:), allocatable :: U_x

    if ( allocated(U_x) ) deallocate(U_x)
  end subroutine DeallocMatrixField_3

  subroutine SendMatrixField_3(U_x, dest_rank)

    type(colour_matrix), dimension(:,:,:) :: U_x
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(U_x)

    call MPI_SSend(U_x, size(U_x)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendMatrixField_3

  subroutine RecvMatrixField_3(U_x, src_rank)

    type(colour_matrix), dimension(:,:,:) :: U_x
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(U_x)

    call MPI_Recv(U_x, size(U_x)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvMatrixField_3

  subroutine AllocVectorField_3(phi,sdim,idim)
    type(colour_vector), dimension(:,:,:), allocatable :: phi
    character(len=3) :: sdim
    integer, dimension(3) :: idim

    integer, dimension(3) :: n
    integer :: j

    do j=1,3
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(phi) ) allocate(phi(n(1),n(2),n(3)))
  end subroutine AllocVectorField_3

  subroutine DeallocVectorField_3(phi)
    type(colour_vector), dimension(:,:,:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DeallocVectorField_3

  subroutine SendVectorField_3(phi, dest_rank)

    type(colour_vector), dimension(:,:,:) :: phi
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendVectorField_3

  subroutine RecvVectorField_3(phi, src_rank)

    type(colour_vector), dimension(:,:,:) :: phi
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvVectorField_3

  subroutine AllocMatrixField_4(U_x,sdim,idim)
    type(colour_matrix), dimension(:,:,:,:), allocatable :: U_x
    character(len=4) :: sdim
    integer, dimension(4) :: idim

    integer, dimension(4) :: n
    integer :: j

    do j=1,4
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(U_x) ) allocate(U_x(n(1),n(2),n(3),n(4)))
  end subroutine AllocMatrixField_4

  subroutine DeallocMatrixField_4(U_x)
    type(colour_matrix), dimension(:,:,:,:), allocatable :: U_x

    if ( allocated(U_x) ) deallocate(U_x)
  end subroutine DeallocMatrixField_4

  subroutine SendMatrixField_4(U_x, dest_rank)

    type(colour_matrix), dimension(:,:,:,:) :: U_x
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(U_x)

    call MPI_SSend(U_x, size(U_x)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendMatrixField_4

  subroutine RecvMatrixField_4(U_x, src_rank)

    type(colour_matrix), dimension(:,:,:,:) :: U_x
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(U_x)

    call MPI_Recv(U_x, size(U_x)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvMatrixField_4

  subroutine AllocVectorField_4(phi,sdim,idim)
    type(colour_vector), dimension(:,:,:,:), allocatable :: phi
    character(len=4) :: sdim
    integer, dimension(4) :: idim

    integer, dimension(4) :: n
    integer :: j

    do j=1,4
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(phi) ) allocate(phi(n(1),n(2),n(3),n(4)))
  end subroutine AllocVectorField_4

  subroutine DeallocVectorField_4(phi)
    type(colour_vector), dimension(:,:,:,:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DeallocVectorField_4

  subroutine SendVectorField_4(phi, dest_rank)

    type(colour_vector), dimension(:,:,:,:) :: phi
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendVectorField_4

  subroutine RecvVectorField_4(phi, src_rank)

    type(colour_vector), dimension(:,:,:,:) :: phi
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvVectorField_4

  subroutine AllocMatrixField_5(U_x,sdim,idim)
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: U_x
    character(len=5) :: sdim
    integer, dimension(5) :: idim

    integer, dimension(5) :: n
    integer :: j

    do j=1,5
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(U_x) ) allocate(U_x(n(1),n(2),n(3),n(4),n(5)))
  end subroutine AllocMatrixField_5

  subroutine DeallocMatrixField_5(U_x)
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: U_x

    if ( allocated(U_x) ) deallocate(U_x)
  end subroutine DeallocMatrixField_5

  subroutine SendMatrixField_5(U_x, dest_rank)

    type(colour_matrix), dimension(:,:,:,:,:) :: U_x
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(U_x)

    call MPI_SSend(U_x, size(U_x)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendMatrixField_5

  subroutine RecvMatrixField_5(U_x, src_rank)

    type(colour_matrix), dimension(:,:,:,:,:) :: U_x
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(U_x)

    call MPI_Recv(U_x, size(U_x)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvMatrixField_5

  subroutine AllocVectorField_5(phi,sdim,idim)
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi
    character(len=5) :: sdim
    integer, dimension(5) :: idim

    integer, dimension(5) :: n
    integer :: j

    do j=1,5
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(phi) ) allocate(phi(n(1),n(2),n(3),n(4),n(5)))
  end subroutine AllocVectorField_5

  subroutine DeallocVectorField_5(phi)
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DeallocVectorField_5

  subroutine SendVectorField_5(phi, dest_rank)

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendVectorField_5

  subroutine RecvVectorField_5(phi, src_rank)

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvVectorField_5

  subroutine AllocMatrixField_6(U_x,sdim,idim)
    type(colour_matrix), dimension(:,:,:,:,:,:), allocatable :: U_x
    character(len=6) :: sdim
    integer, dimension(6) :: idim

    integer, dimension(6) :: n
    integer :: j

    do j=1,6
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(U_x) ) allocate(U_x(n(1),n(2),n(3),n(4),n(5),n(6)))
  end subroutine AllocMatrixField_6

  subroutine DeallocMatrixField_6(U_x)
    type(colour_matrix), dimension(:,:,:,:,:,:), allocatable :: U_x

    if ( allocated(U_x) ) deallocate(U_x)
  end subroutine DeallocMatrixField_6

  subroutine SendMatrixField_6(U_x, dest_rank)

    type(colour_matrix), dimension(:,:,:,:,:,:) :: U_x
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(U_x)

    call MPI_SSend(U_x, size(U_x)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendMatrixField_6

  subroutine RecvMatrixField_6(U_x, src_rank)

    type(colour_matrix), dimension(:,:,:,:,:,:) :: U_x
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(U_x)

    call MPI_Recv(U_x, size(U_x)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvMatrixField_6

  subroutine AllocVectorField_6(phi,sdim,idim)
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: phi
    character(len=6) :: sdim
    integer, dimension(6) :: idim

    integer, dimension(6) :: n
    integer :: j

    do j=1,6
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(phi) ) allocate(phi(n(1),n(2),n(3),n(4),n(5),n(6)))
  end subroutine AllocVectorField_6

  subroutine DeallocVectorField_6(phi)
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DeallocVectorField_6

  subroutine SendVectorField_6(phi, dest_rank)

    type(colour_vector), dimension(:,:,:,:,:,:) :: phi
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendVectorField_6

  subroutine RecvVectorField_6(phi, src_rank)

    type(colour_vector), dimension(:,:,:,:,:,:) :: phi
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvVectorField_6

  subroutine AllocMatrixField_7(U_x,sdim,idim)
    type(colour_matrix), dimension(:,:,:,:,:,:,:), allocatable :: U_x
    character(len=7) :: sdim
    integer, dimension(7) :: idim

    integer, dimension(7) :: n
    integer :: j

    do j=1,7
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(U_x) ) allocate(U_x(n(1),n(2),n(3),n(4),n(5),n(6),n(7)))
  end subroutine AllocMatrixField_7

  subroutine DeallocMatrixField_7(U_x)
    type(colour_matrix), dimension(:,:,:,:,:,:,:), allocatable :: U_x

    if ( allocated(U_x) ) deallocate(U_x)
  end subroutine DeallocMatrixField_7

  subroutine SendMatrixField_7(U_x, dest_rank)

    type(colour_matrix), dimension(:,:,:,:,:,:,:) :: U_x
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(U_x)

    call MPI_SSend(U_x, size(U_x)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendMatrixField_7

  subroutine RecvMatrixField_7(U_x, src_rank)

    type(colour_matrix), dimension(:,:,:,:,:,:,:) :: U_x
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(U_x)

    call MPI_Recv(U_x, size(U_x)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvMatrixField_7

  subroutine AllocVectorField_7(phi,sdim,idim)
    type(colour_vector), dimension(:,:,:,:,:,:,:), allocatable :: phi
    character(len=7) :: sdim
    integer, dimension(7) :: idim

    integer, dimension(7) :: n
    integer :: j

    do j=1,7
      if ( sdim(j:j) == 'x' ) then
        n(j) = nx
        if ( nprocx > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'y' ) then
        n(j) = ny
        if ( nprocy > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 'z' ) then
        n(j) = nz
        if ( nprocz > 1 ) n(j) = n(j)+2*idim(j)
      else if ( sdim(j:j) == 't' ) then
        n(j) = nt
        if ( nproct > 1 ) n(j) = n(j)+2*idim(j)
      else
        n(j) = idim(j)
      end if
    end do

    if ( .not. allocated(phi) ) allocate(phi(n(1),n(2),n(3),n(4),n(5),n(6),n(7)))
  end subroutine AllocVectorField_7

  subroutine DeallocVectorField_7(phi)
    type(colour_vector), dimension(:,:,:,:,:,:,:), allocatable :: phi

    if ( allocated(phi) ) deallocate(phi)
  end subroutine DeallocVectorField_7

  subroutine SendVectorField_7(phi, dest_rank)

    type(colour_vector), dimension(:,:,:,:,:,:,:) :: phi
    integer :: dest_rank
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine SendVectorField_7

  subroutine RecvVectorField_7(phi, src_rank)

    type(colour_vector), dimension(:,:,:,:,:,:,:) :: phi
    integer :: src_rank
    integer :: mpierror, tag    
    integer, dimension(nmpi_status) :: mpi_status
    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine RecvVectorField_7


end module ColourFieldOps
