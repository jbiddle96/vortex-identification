
module FermionAction
  use FermionField
  use FermActTypes
  use WilsonMatrix
  use CloverMatrix
  use FLWMatrix
  use FLCMatrix
  use FLIWMatrix
  use FLICMatrix
  use EOWilsonMatrix
  use EOCloverMatrix
  use EOFLWMatrix
  use EOFLCMatrix
  use EOFLIWMatrix
  use EOFLICMatrix
  use WilsonPropagator
  use CloverPropagator
  use FLWPropagator
  use FLCPropagator
  use FLIWPropagator
  use FLICPropagator
  implicit none
  private

  abstract interface 
     subroutine f_eo2x(psi_e,psi_o,psi)
       use ColourTypes
       use FermionTypes
       implicit none
       type(dirac_fermion), dimension(:) :: psi_e,psi_o
       type(colour_vector), dimension(:,:,:,:,:) :: psi
     end subroutine f_eo2x
  end interface

  abstract interface 
     subroutine f_x2eo(psi,psi_e,psi_o)
       use ColourTypes
       use FermionTypes
       implicit none
       type(colour_vector), dimension(:,:,:,:,:) :: psi
       type(dirac_fermion), dimension(:) :: psi_e,psi_o
     end subroutine f_x2eo
  end interface

  procedure(f_eo2x), pointer, public :: LeftILUMatrix, InvRightILUMatrix
  procedure(f_x2eo), pointer, public :: RightILUMatrix, InvLeftILUMatrix

  abstract interface
     subroutine fm(phi,Dphi)
       use ColourTypes
       use FermionTypes
       implicit none
       type(colour_vector), dimension(:,:,:,:,:) :: phi
       type(colour_vector), dimension(:,:,:,:,:) :: Dphi
     end subroutine fm
  end interface

  abstract interface
     subroutine fm_eo(phi,Dphi)
       use ColourTypes
       use FermionTypes
       implicit none
       type(dirac_fermion), dimension(:) :: phi
       type(dirac_fermion), dimension(:) :: Dphi
     end subroutine fm_eo
  end interface

  procedure(fm), pointer, public :: D_fm, Ddag_fm, H_fm, Hsq_fm
  procedure(fm_eo), pointer , public :: D_eo, Ddag_eo, H_eo, Hsq_eo

  abstract interface
     subroutine get_params(filename)
       character(len=*) :: filename
     end subroutine get_params
  end interface
  procedure(get_params), pointer, public :: GetFermionActionParams

  abstract interface
     subroutine init_fermact(U_xd,kappa,kBFStrength,even_odd)
       use Kinds
       use ColourTypes
       type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
       real(dp) :: kappa
       integer, optional :: kBFStrength
       logical, optional :: even_odd
     end subroutine init_fermact
  end interface
  procedure(init_fermact), pointer, public :: InitialiseFermionAction

  abstract interface
     subroutine fin_fermact(even_odd)
       logical, optional :: even_odd
     end subroutine fin_fermact
  end interface
  procedure(fin_fermact), pointer, public :: FinaliseFermionAction

  abstract interface
     subroutine param_io(file_unit)
       integer :: file_unit
     end subroutine param_io
  end interface
  procedure(param_io), pointer, public :: ReadFermionActionParams
  procedure(param_io), pointer, public  :: WriteFermionActionParams

  abstract interface 
     subroutine add_sst(chi,psi,U_xd,kappa,j_x,j_mu,it_s,p_x,currentType) 
       use Kinds
       use LatticeSize
       use ColourTypes
       implicit none
       type(colour_vector), dimension(:,:,:,:,:) :: chi, psi
       type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
       real(dp) :: kappa
       integer  :: j_x(nd-1)  !! spatial source position
       integer  :: j_mu !! Lorentz index for the current.
       integer :: it_s !! time slice to insert the current.
       integer  :: p_x(nd-1) !! momentum
       integer :: currentType
     end subroutine add_sst
  end interface
  procedure(add_sst), pointer, public :: AddSSTSource

  !! Warning: Don't use function pointers for procedure with class type arguments (internal compiler error).

  logical, pointer, public :: fm_initialised, eo_initialised

  public :: SelectFermionAction, SetFermionActionParams

contains

  subroutine SelectFermionAction(fermact)
    character(len=*) :: fermact

    select case(trim(fermact))
    case("wilson")
       D_fm => Dwilson
       Ddag_fm => Dwilson_dag
       H_fm => Hwilson
       Hsq_fm => Hsqwilson
       GetFermionActionParams => GetFermionActionParams_wilson
       InitialiseFermionAction => InitialiseFermionAction_wilson
       FinaliseFermionAction => FinaliseFermionAction_wilson
       ReadFermionActionParams => ReadFermionActionParams_wilson
       WriteFermionActionParams => WriteFermionActionParams_wilson
       AddSSTSource => AddSSTSource_wilson
       fm_initialised => fm_initialised_wilson

       D_eo => Deowilson
       Ddag_eo => Deowilson_dag
       H_eo => Heowilson
       Hsq_eo => Hsqeowilson
       eo_initialised => fm_initialised_eowilson
       LeftILUMatrix => LeftILUMatrix_eowilson
       RightILUMatrix => RightILUMatrix_eowilson
       InvLeftILUMatrix => InvLeftILUMatrix_eowilson
       InvRightILUMatrix => InvRightILUMatrix_eowilson
    case("clover")
       D_fm => Dclover
       Ddag_fm => Dclover_dag
       H_fm => Hclover
       Hsq_fm => Hsqclover
       GetFermionActionParams => GetFermionActionParams_clover
       InitialiseFermionAction => InitialiseFermionAction_clover
       FinaliseFermionAction => FinaliseFermionAction_clover
       ReadFermionActionParams => ReadFermionActionParams_clover
       WriteFermionActionParams => WriteFermionActionParams_clover
       AddSSTSource => AddSSTSource_clover
       fm_initialised => fm_initialised_clover

       D_eo => Deoclover
       Ddag_eo => Deoclover_dag
       H_eo => Heoclover
       Hsq_eo => Hsqeoclover
       eo_initialised => fm_initialised_eoclover
       LeftILUMatrix => LeftILUMatrix_eoclover
       RightILUMatrix => RightILUMatrix_eoclover
       InvLeftILUMatrix => InvLeftILUMatrix_eoclover
       InvRightILUMatrix => InvRightILUMatrix_eoclover
    case("flw")
       D_fm => Dflw
       Ddag_fm => Dflw_dag
       H_fm => Hflw
       Hsq_fm => Hsqflw
       GetFermionActionParams => GetFermionActionParams_flw
       InitialiseFermionAction => InitialiseFermionAction_flw
       FinaliseFermionAction => FinaliseFermionAction_flw
       ReadFermionActionParams => ReadFermionActionParams_flw
       WriteFermionActionParams => WriteFermionActionParams_flw
       AddSSTSource => AddSSTSource_flw
       fm_initialised => fm_initialised_flw

       D_eo => Deoflw
       Ddag_eo => Deoflw_dag
       H_eo => Heoflw
       Hsq_eo => Hsqeoflw
       eo_initialised => fm_initialised_eoflw
       LeftILUMatrix => LeftILUMatrix_eoflw
       RightILUMatrix => RightILUMatrix_eoflw
       InvLeftILUMatrix => InvLeftILUMatrix_eoflw
       InvRightILUMatrix => InvRightILUMatrix_eoflw
    case("flc")
       D_fm => Dflc
       Ddag_fm => Dflc_dag
       H_fm => Hflc
       Hsq_fm => Hsqflc
       GetFermionActionParams => GetFermionActionParams_flc
       InitialiseFermionAction => InitialiseFermionAction_flc
       FinaliseFermionAction => FinaliseFermionAction_flc
       ReadFermionActionParams => ReadFermionActionParams_flc
       WriteFermionActionParams => WriteFermionActionParams_flc
       AddSSTSource => AddSSTSource_flc
       fm_initialised => fm_initialised_flc

       D_eo => Deoflc
       Ddag_eo => Deoflc_dag
       H_eo => Heoflc
       Hsq_eo => Hsqeoflc
       eo_initialised => fm_initialised_eoflc
       LeftILUMatrix => LeftILUMatrix_eoflc
       RightILUMatrix => RightILUMatrix_eoflc
       InvLeftILUMatrix => InvLeftILUMatrix_eoflc
       InvRightILUMatrix => InvRightILUMatrix_eoflc
    case("fliw")
       D_fm => Dfliw
       Ddag_fm => Dfliw_dag
       H_fm => Hfliw
       Hsq_fm => Hsqfliw
       GetFermionActionParams => GetFermionActionParams_fliw
       InitialiseFermionAction => InitialiseFermionAction_fliw
       FinaliseFermionAction => FinaliseFermionAction_fliw
       ReadFermionActionParams => ReadFermionActionParams_fliw
       WriteFermionActionParams => WriteFermionActionParams_fliw
       AddSSTSource => AddSSTSource_fliw
       fm_initialised => fm_initialised_fliw

       D_eo => Deofliw
       Ddag_eo => Deofliw_dag
       H_eo => Heofliw
       Hsq_eo => Hsqeofliw
       eo_initialised => fm_initialised_eofliw

       LeftILUMatrix => LeftILUMatrix_eofliw
       RightILUMatrix => RightILUMatrix_eofliw
       InvLeftILUMatrix => InvLeftILUMatrix_eofliw
       InvRightILUMatrix => InvRightILUMatrix_eofliw
    case("flic")
       D_fm => Dflic
       Ddag_fm => Dflic_dag
       H_fm => Hflic
       Hsq_fm => Hsqflic
       GetFermionActionParams => GetFermionActionParams_flic
       InitialiseFermionAction => InitialiseFermionAction_flic
       FinaliseFermionAction => FinaliseFermionAction_flic
       ReadFermionActionParams => ReadFermionActionParams_flic
       WriteFermionActionParams => WriteFermionActionParams_flic
       AddSSTSource => AddSSTSource_flic
       fm_initialised => fm_initialised_flic

       D_eo => Deoflic
       Ddag_eo => Deoflic_dag
       H_eo => Heoflic
       Hsq_eo => Hsqeoflic
       eo_initialised => fm_initialised_eoflic
       LeftILUMatrix => LeftILUMatrix_eoflic
       RightILUMatrix => RightILUMatrix_eoflic
       InvLeftILUMatrix => InvLeftILUMatrix_eoflic
       InvRightILUMatrix => InvRightILUMatrix_eoflic
    case default
       call Abort("SelectFermionAction: Unrecognised fermion action: "//trim(fermact))
    end select

  end subroutine SelectFermionAction

  subroutine SetFermionActionParams(fermact)
    class(wilsonFermAct) :: fermact

    ff_bcx = fermact%bcx
    ff_bcy = fermact%bcy
    ff_bcz = fermact%bcz
    ff_bct = fermact%bct

    select type (fermact)
    type is (wilsonFermAct)
       u0_wilson = fermact%u0
    type is (cloverFermAct)
       u0_clover = fermact%u0
       c_sw_clover = fermact%c_sw
    type is (flwFermAct)
       u0_flw = fermact%u0
       alpha_smear_flw = fermact%alpha_smear
       n_sweeps_flw = fermact%n_sweeps
       u0sm_flw = fermact%u0sm
    type is (flcFermAct)
       u0_flc = fermact%u0
       c_sw_flc = fermact%c_sw
       alpha_smear_flc = fermact%alpha_smear
       n_sweeps_flc = fermact%n_sweeps
       u0sm_flc = fermact%u0sm
    type is (fliwFermAct)
       u0_fliw = fermact%u0
       alpha_smear_fliw = fermact%alpha_smear
       n_sweeps_fliw = fermact%n_sweeps
       u0sm_fliw = fermact%u0sm
    type is (flicFermAct)
       u0_flic = fermact%u0
       c_sw_flic = fermact%c_sw
       alpha_smear_flic = fermact%alpha_smear
       n_sweeps_flic = fermact%n_sweeps
       u0sm_flic = fermact%u0sm
    end select

  end subroutine SetFermionActionParams

end module FermionAction
