
module EOWilsonMatrix_EO

  use Timer
  use MPIInterface
  use FermionField
  use FermionAlgebra



  use SpinorTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  use ColourFieldOps
  implicit none
  private

  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:), private, allocatable :: Up_xd
  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), public :: kappa_EOWilson



  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean

  real(dp), public :: bcx_EOWilson = 1.0d0, bcy_EOWilson = 1.0d0, bcz_EOWilson = 1.0d0, bct_EOWilson = 1.0d0
  public :: DEOWilson_ex
  public :: EOWilson_ex


  public :: DEOWilson_eo
  public :: DEOWilson_dag_eo
  public :: HsqEOWilson_eo
  public :: HEOWilson_eo
  public :: InitialiseEOWilsonOperator_eo
  public :: FinaliseEOWilsonOperator_eo

  public :: EOWilsonDag_eo
  public :: EOWilson_eo
  public :: phi_x2phi_eo
  public :: phi_eo2phi_x
  
  integer, dimension(:,:,:), allocatable :: i_xpmu, i_xmmu

contains

  subroutine InitialiseEOWilsonOperator_eo(U_xd , kappa, u0)
    ! begin args: U_xd , kappa, u0

    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: kappa
    real(dp), optional :: u0

    ! begin local_vars
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfic_sw, mfir, mfir_sm !coefficents to be absorbed.
    integer :: i_eo, i_xeo, j_xeo
    integer, dimension(nd) :: i_x, j_x, n_x

    !EO preconditioning is in the hopping parameter formalism.
    ! begin execution

    if ( .not. allocated(Up_xd) ) allocate(Up_xd(n_xpeo,nd,0:1))
    if ( .not. allocated(i_xpmu) ) allocate(i_xpmu(n_xpeo,nd,0:1))
    if ( .not. allocated(i_xmmu) ) allocate(i_xmmu(n_xpeo,nd,0:1))

    mfir = kappa
    if ( present(u0) ) mfir = mfir/u0

    kappa_EOWilson = kappa

    !Construct the forward and backward hop indices.
    n_x = (/ nx, ny, nz, nt /)
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       i_x = (/ ix,iy,iz,it /)
       i_eo = modulo(sum(i_x),2)
       i_xeo = x2eo(i_x)
       do mu=1,nd
          j_x = i_x

          j_x(mu) = modc(j_x(mu)+1,n_x(mu))
          j_xeo = x2eo(j_x)
          i_xpmu(i_xeo,mu,i_eo) = j_xeo

          j_x = i_x

          j_x(mu) = modc(j_x(mu)-1,n_x(mu))
          j_xeo = x2eo(j_x)
          i_xmmu(i_xeo,mu,i_eo) = j_xeo

       end do
    end do; end do; end do; end do

    !Absorb the mean field improvement into the gauge fields.
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          Up_xd(i_xeo,mu,i_eo)%Cl = mfir*U_xd(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do

    !call SetBoundaryConditions(Up_xd,bcx_EOWilson, bcy_EOWilson, bcz_EOWilson, bct_EOWilson)
    !call ShadowGaugeField(Up_xd,0)
  end subroutine InitialiseEOWilsonOperator_eo

  subroutine FinaliseEOWilsonOperator_eo

    if ( allocated(Up_xd) ) deallocate(Up_xd)
    if ( allocated(i_xpmu) ) deallocate(i_xpmu)
    if ( allocated(i_xmmu) ) deallocate(i_xmmu)
  end subroutine FinaliseEOWilsonOperator_eo

  subroutine phi_x2phi_eo(psi,psi_e,psi_o)
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(colour_vector), dimension(:,:) :: psi_e, psi_o

    integer :: ix,iy,iz,it,is,i_eo,i_xeo,i_x(nd)

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          if ( i_eo == 0 ) then
             psi_e(i_xeo,is) = psi(ix,iy,iz,it,is) 
          else
             psi_o(i_xeo,is) = psi(ix,iy,iz,it,is) 
          end if
       end do; end do; end do; end do
    end do
  end subroutine phi_x2phi_eo

  subroutine phi_eo2phi_x(psi_e,psi_o,psi)
    type(colour_vector), dimension(:,:) :: psi_e, psi_o
    type(colour_vector), dimension(:,:,:,:,:) :: psi

    integer :: ix,iy,iz,it,is,i_eo,i_xeo,i_x(nd)

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          if ( i_eo == 0 ) then
             psi(ix,iy,iz,it,is) = psi_e(i_xeo,is) 
          else
             psi(ix,iy,iz,it,is) = psi_o(i_xeo,is)
          end if
       end do; end do; end do; end do
    end do
  end subroutine phi_eo2phi_x

  subroutine DEOWilson_eo(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:) :: phi
    type(colour_vector), dimension(:,:) :: Dphi

    ! begin local_vars
    type(colour_vector), dimension(:,:), allocatable :: phi_e,phi_o
    integer :: i_xeo,is

    ! begin execution
    allocate(phi_e(n_xpeo,ns))
    allocate(phi_o(n_xpeo,ns))

    call EOWilson_eo(phi,phi_e,0)




    call EOWilson_eo(phi_e,phi_o,1)

    Dphi = phi

    do is=1,ns
       do i_xeo=1,n_xeo
          Dphi(i_xeo,is)%Cl = Dphi(i_xeo,is)%Cl - phi_o(i_xeo,is)%Cl
       end do
    end do

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine DEOWilson_eo

  subroutine DEOWilson_dag_eo(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:) :: phi
    type(colour_vector), dimension(:,:) :: Dphi

    ! begin local_vars
    type(colour_vector), dimension(:,:), allocatable :: phi_e, phi_o
    integer :: i_xeo,is

    ! begin execution
    allocate(phi_e(n_xpeo,ns))
    allocate(phi_o(n_xpeo,ns))

    call EOWilsonDag_eo(phi,phi_e,1)

    call EOWilsonDag_eo(phi_e,phi_o,0)

    Dphi = phi

    do is=1,ns
       do i_xeo=1,n_xeo
          Dphi(i_xeo,is)%Cl = Dphi(i_xeo,is)%Cl - phi_o(i_xeo,is)%Cl
       end do
    end do

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine DEOWilson_dag_eo

  subroutine HsqEOWilson_eo(phi,Hsqphi)
    ! begin args: phi, Hsqphi

    type(colour_vector), dimension(:,:) :: phi, Hsqphi

    ! begin local_vars
    type(colour_vector), dimension(:,:), allocatable :: Dphi

    ! begin execution
    allocate(Dphi(n_xpeo,ns))

    call HEOWilson_eo(phi,Dphi)
    call HEOWilson_eo(Dphi,Hsqphi)

    deallocate(Dphi)

  end subroutine HsqEOWilson_eo

  subroutine HEOWilson_eo(phi,Hphi)
    ! begin args: phi, Hphi

    type(colour_vector), dimension(:,:) :: phi, Hphi
    ! begin local_vars
    integer :: i_xeo

    ! begin execution

    call DEOWilson_eo(phi,Hphi)

    do i_xeo=1,n_xeo
       Hphi(i_xeo,3)%Cl = -Hphi(i_xeo,3)%Cl
       Hphi(i_xeo,4)%Cl = -Hphi(i_xeo,4)%Cl
    end do

  end subroutine HEOWilson_eo

  subroutine EOWilsonDag_eo(phi, Dphi, op_parity)
    ! begin args: phi, Dphi, op_parity

    type(colour_vector), dimension(:,:) :: phi
    type(colour_vector), dimension(:,:) :: Dphi

    integer :: op_parity

    ! begin local_vars
    integer :: i_xeo


    ! begin execution

    do i_xeo=1,n_xeo
       phi(i_xeo,3)%Cl = -phi(i_xeo,3)%Cl
       phi(i_xeo,4)%Cl = -phi(i_xeo,4)%Cl
    end do
    call EOWilson_eo(phi,Dphi,1-op_parity)

    do i_xeo=1,n_xeo
       phi(i_xeo,3)%Cl = -phi(i_xeo,3)%Cl
       phi(i_xeo,4)%Cl = -phi(i_xeo,4)%Cl

       Dphi(i_xeo,3)%Cl = -Dphi(i_xeo,3)%Cl
       Dphi(i_xeo,4)%Cl = -Dphi(i_xeo,4)%Cl
    end do
  end subroutine EOWilsonDag_eo

  subroutine EOWilson_eo(phi, Dphi,i_eo)
    ! begin args: phi, Dphi, op_parity

    type(colour_vector), dimension(:,:) :: phi
    type(colour_vector), dimension(:,:) :: Dphi

    integer :: i_eo

    ! op_parity = 0 => takes odd sites to even sites.
    ! op_parity = 1 => takes even sites to odd sites.

    ! begin local_vars
    integer :: i_xeo, j_xeo, is, js, j_eo
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

!!    if (timing) intime = mpi_wtime()

    commtime = 0.0d0
    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0

    j_eo = 1 - i_eo
    p_xyzt = modulo(i_nx+i_ny+i_nz+i_nt,2) ! Offset due to processor sub-lattice

    Dphi = zero_vector
    !mu = 1

    !G_1^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do i_xeo=1,n_xeo
          j_xeo = i_xpmu(i_xeo,1,i_eo)

          Gammaphi%Cl(1) = phi(j_xeo,is)%Cl(1) + cmplx(-aimag(phi(j_xeo,js)%Cl(1)),real(phi(j_xeo,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(j_xeo,is)%Cl(2) + cmplx(-aimag(phi(j_xeo,js)%Cl(2)),real(phi(j_xeo,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(j_xeo,is)%Cl(3) + cmplx(-aimag(phi(j_xeo,js)%Cl(3)),real(phi(j_xeo,js)%Cl(3)),dc)

          psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo,is)%Cl(1) = Dphi(i_xeo,is)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,is)%Cl(2) = Dphi(i_xeo,is)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,is)%Cl(3) = Dphi(i_xeo,is)%Cl(3) - psi_x%Cl(3)

          Dphi(i_xeo,js)%Cl(1) = Dphi(i_xeo,js)%Cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo,js)%Cl(2) = Dphi(i_xeo,js)%Cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo,js)%Cl(3) = Dphi(i_xeo,js)%Cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)


       end do
    end do
    !mu = 2


    !G_2^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do i_xeo=1,n_xeo
          j_xeo = i_xpmu(i_xeo,2,i_eo)

          Gammaphi%Cl(1) = phi(j_xeo,is)%Cl(1) + pm(is)*phi(j_xeo,js)%Cl(1)
          Gammaphi%Cl(2) = phi(j_xeo,is)%Cl(2) + pm(is)*phi(j_xeo,js)%Cl(2)
          Gammaphi%Cl(3) = phi(j_xeo,is)%Cl(3) + pm(is)*phi(j_xeo,js)%Cl(3)

          psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo,is)%Cl(1) = Dphi(i_xeo,is)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,is)%Cl(2) = Dphi(i_xeo,is)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,is)%Cl(3) = Dphi(i_xeo,is)%Cl(3) - psi_x%Cl(3)

          Dphi(i_xeo,js)%Cl(1) = Dphi(i_xeo,js)%Cl(1) + pm(js)*psi_x%Cl(1)
          Dphi(i_xeo,js)%Cl(2) = Dphi(i_xeo,js)%Cl(2) + pm(js)*psi_x%Cl(2)
          Dphi(i_xeo,js)%Cl(3) = Dphi(i_xeo,js)%Cl(3) + pm(js)*psi_x%Cl(3)

       end do
    end do
    !mu = 3





    !G_3^- Up phi_xpmu

    do is=1,nsp
       js = is+2

       do i_xeo=1,n_xeo
          j_xeo = i_xpmu(i_xeo,3,i_eo)

          Gammaphi%Cl(1) = phi(j_xeo,is)%Cl(1) + pm(is)*cmplx(-aimag(phi(j_xeo,js)%Cl(1)),real(phi(j_xeo,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(j_xeo,is)%Cl(2) + pm(is)*cmplx(-aimag(phi(j_xeo,js)%Cl(2)),real(phi(j_xeo,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(j_xeo,is)%Cl(3) + pm(is)*cmplx(-aimag(phi(j_xeo,js)%Cl(3)),real(phi(j_xeo,js)%Cl(3)),dc)

          psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo,is)%Cl(1) = Dphi(i_xeo,is)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,is)%Cl(2) = Dphi(i_xeo,is)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,is)%Cl(3) = Dphi(i_xeo,is)%Cl(3) - psi_x%Cl(3)

          Dphi(i_xeo,js)%Cl(1) = Dphi(i_xeo,js)%Cl(1) + pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo,js)%Cl(2) = Dphi(i_xeo,js)%Cl(2) + pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo,js)%Cl(3) = Dphi(i_xeo,js)%Cl(3) + pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
    end do
    !mu = 4




    !G_4^- Up phi_xpmu

    do is=1,nsp
       js = is+2

       do i_xeo=1,n_xeo
          j_xeo = i_xpmu(i_xeo,4,i_eo)

          Gammaphi%Cl(1) = phi(j_xeo,is)%Cl(1) - phi(j_xeo,js)%Cl(1)
          Gammaphi%Cl(2) = phi(j_xeo,is)%Cl(2) - phi(j_xeo,js)%Cl(2)
          Gammaphi%Cl(3) = phi(j_xeo,is)%Cl(3) - phi(j_xeo,js)%Cl(3)
          psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo,is)%Cl(1) = Dphi(i_xeo,is)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,is)%Cl(2) = Dphi(i_xeo,is)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,is)%Cl(3) = Dphi(i_xeo,is)%Cl(3) - psi_x%Cl(3)


          Dphi(i_xeo,js)%Cl(1) = Dphi(i_xeo,js)%Cl(1) + psi_x%Cl(1)
          Dphi(i_xeo,js)%Cl(2) = Dphi(i_xeo,js)%Cl(2) + psi_x%Cl(2)
          Dphi(i_xeo,js)%Cl(3) = Dphi(i_xeo,js)%Cl(3) + psi_x%Cl(3)

       end do
    end do
    !mu = 1




    !G_1^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is

       do i_xeo=1,n_xeo
          j_xeo = i_xmmu(i_xeo,1,i_eo)

          Gammaphi%Cl(1) = phi(j_xeo,is)%Cl(1) - cmplx(-aimag(phi(j_xeo,js)%Cl(1)),real(phi(j_xeo,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(j_xeo,is)%Cl(2) - cmplx(-aimag(phi(j_xeo,js)%Cl(2)),real(phi(j_xeo,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(j_xeo,is)%Cl(3) - cmplx(-aimag(phi(j_xeo,js)%Cl(3)),real(phi(j_xeo,js)%Cl(3)),dc)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo,is)%Cl(1) = Dphi(i_xeo,is)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,is)%Cl(2) = Dphi(i_xeo,is)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,is)%Cl(3) = Dphi(i_xeo,is)%Cl(3) - psi_x%Cl(3)

          Dphi(i_xeo,js)%Cl(1) = Dphi(i_xeo,js)%Cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo,js)%Cl(2) = Dphi(i_xeo,js)%Cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo,js)%Cl(3) = Dphi(i_xeo,js)%Cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
    end do
    !mu = 2




    !G_2^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is

       do i_xeo=1,n_xeo
          j_xeo = i_xmmu(i_xeo,2,i_eo)

          Gammaphi%Cl(1) = phi(j_xeo,is)%Cl(1) - pm(is)*phi(j_xeo,js)%Cl(1)
          Gammaphi%Cl(2) = phi(j_xeo,is)%Cl(2) - pm(is)*phi(j_xeo,js)%Cl(2)
          Gammaphi%Cl(3) = phi(j_xeo,is)%Cl(3) - pm(is)*phi(j_xeo,js)%Cl(3)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo,is)%Cl(1) = Dphi(i_xeo,is)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,is)%Cl(2) = Dphi(i_xeo,is)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,is)%Cl(3) = Dphi(i_xeo,is)%Cl(3) - psi_x%Cl(3)

          Dphi(i_xeo,js)%Cl(1) = Dphi(i_xeo,js)%Cl(1) - pm(js)*psi_x%Cl(1)
          Dphi(i_xeo,js)%Cl(2) = Dphi(i_xeo,js)%Cl(2) - pm(js)*psi_x%Cl(2)
          Dphi(i_xeo,js)%Cl(3) = Dphi(i_xeo,js)%Cl(3) - pm(js)*psi_x%Cl(3)


       end do
    end do

    !mu = 3




    !G_3^+ Up phi_xmmu

    do is=1,nsp
       js = is+2

       do i_xeo=1,n_xeo
          j_xeo = i_xmmu(i_xeo,3,i_eo)

          Gammaphi%Cl(1) = phi(j_xeo,is)%Cl(1) - pm(is)*cmplx(-aimag(phi(j_xeo,js)%Cl(1)),real(phi(j_xeo,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(j_xeo,is)%Cl(2) - pm(is)*cmplx(-aimag(phi(j_xeo,js)%Cl(2)),real(phi(j_xeo,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(j_xeo,is)%Cl(3) - pm(is)*cmplx(-aimag(phi(j_xeo,js)%Cl(3)),real(phi(j_xeo,js)%Cl(3)),dc)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo,is)%Cl(1) = Dphi(i_xeo,is)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,is)%Cl(2) = Dphi(i_xeo,is)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,is)%Cl(3) = Dphi(i_xeo,is)%Cl(3) - psi_x%Cl(3)

          Dphi(i_xeo,js)%Cl(1) = Dphi(i_xeo,js)%Cl(1) - pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo,js)%Cl(2) = Dphi(i_xeo,js)%Cl(2) - pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo,js)%Cl(3) = Dphi(i_xeo,js)%Cl(3) - pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
    end do
    !mu = 4




    !G_4^+ Up phi_xmmu

    do is=1,nsp
       js = is+2

       do i_xeo=1,n_xeo
          j_xeo = i_xmmu(i_xeo,4,i_eo)

          Gammaphi%Cl(1) = phi(j_xeo,is)%Cl(1) + phi(j_xeo,js)%Cl(1)
          Gammaphi%Cl(2) = phi(j_xeo,is)%Cl(2) + phi(j_xeo,js)%Cl(2)
          Gammaphi%Cl(3) = phi(j_xeo,is)%Cl(3) + phi(j_xeo,js)%Cl(3)
          psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo,is)%Cl(1) = Dphi(i_xeo,is)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,is)%Cl(2) = Dphi(i_xeo,is)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,is)%Cl(3) = Dphi(i_xeo,is)%Cl(3) - psi_x%Cl(3)


          Dphi(i_xeo,js)%Cl(1) = Dphi(i_xeo,js)%Cl(1) - psi_x%Cl(1)
          Dphi(i_xeo,js)%Cl(2) = Dphi(i_xeo,js)%Cl(2) - psi_x%Cl(2)
          Dphi(i_xeo,js)%Cl(3) = Dphi(i_xeo,js)%Cl(3) - psi_x%Cl(3)

       end do
    end do

!!    if (timing) then
!!       outtime = mpi_wtime()
!!       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
!!    end if
!!
!!    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
!!    waste = commtime/(outtime - intime)
!!    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)

  end subroutine EOWilson_eo

  subroutine DEOWilson_ex(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:) :: phi
    type(colour_vector), dimension(:,:) :: Dphi

    ! begin local_vars
    type(colour_vector), dimension(:,:), allocatable :: phi_e,phi_o
    integer :: i_xeo,is

    ! begin execution
    allocate(phi_e(n_xpeo,ns))
    allocate(phi_o(n_xpeo,ns))

    call EOWilson_ex(phi,phi_e,0)

    call EOWilson_ex(phi_e,phi_o,1)

    Dphi = phi

    do is=1,ns
       do i_xeo=1,n_xeo
          Dphi(i_xeo,is)%cl = Dphi(i_xeo,is)%cl - phi_o(i_xeo,is)%cl
       end do
    end do

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine DEOWilson_ex

  subroutine EOWilson_ex(phi, Dphi,i_eo)
    ! begin args: phi, Dphi, op_parity

    type(colour_vector), dimension(:,:) :: phi
    type(colour_vector), dimension(:,:) :: Dphi

    integer :: i_eo

    ! op_parity = 0 => takes odd sites to even sites.
    ! op_parity = 1 => takes even sites to odd sites.

    ! begin local_vars
    integer :: i_xeo, j_xeo, j_eo
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt

    real(dp) :: intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

!!    if (timing) intime = mpi_wtime()
!!    commtime = 0.0d0

    j_eo = 1 - i_eo
    p_xyzt = modulo(i_nx+i_ny+i_nz+i_nt,2) ! Offset due to processor sub-lattice

    Dphi = zero_vector
    !mu = 1

    !G_1^- Up phi_xpmu

    do i_xeo=1,n_xeo

       j_xeo = i_xpmu(i_xeo,1,i_eo)

       !i_s = 1
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo,1)%cl(1) + cmplx(-aimag(phi(j_xeo,4)%cl(1)),real(phi(j_xeo,4)%cl(1)),dc)
       Gammaphi%Cl(2) = phi(j_xeo,1)%cl(2) + cmplx(-aimag(phi(j_xeo,4)%cl(2)),real(phi(j_xeo,4)%cl(2)),dc)
       Gammaphi%Cl(3) = phi(j_xeo,1)%cl(3) + cmplx(-aimag(phi(j_xeo,4)%cl(3)),real(phi(j_xeo,4)%cl(3)),dc)

       psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo,1)%cl(1) = Dphi(i_xeo,1)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,1)%cl(2) = Dphi(i_xeo,1)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,1)%cl(3) = Dphi(i_xeo,1)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,4)%cl(1) = Dphi(i_xeo,4)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo,4)%cl(2) = Dphi(i_xeo,4)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo,4)%cl(3) = Dphi(i_xeo,4)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !i_s = 2
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo,2)%cl(1) + cmplx(-aimag(phi(j_xeo,3)%cl(1)),real(phi(j_xeo,3)%cl(1)),dc)
       Gammaphi%Cl(2) = phi(j_xeo,2)%cl(2) + cmplx(-aimag(phi(j_xeo,3)%cl(2)),real(phi(j_xeo,3)%cl(2)),dc)
       Gammaphi%Cl(3) = phi(j_xeo,2)%cl(3) + cmplx(-aimag(phi(j_xeo,3)%cl(3)),real(phi(j_xeo,3)%cl(3)),dc)

       psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo,2)%cl(1) = Dphi(i_xeo,2)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,2)%cl(2) = Dphi(i_xeo,2)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,2)%cl(3) = Dphi(i_xeo,2)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,3)%cl(1) = Dphi(i_xeo,3)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo,3)%cl(2) = Dphi(i_xeo,3)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo,3)%cl(3) = Dphi(i_xeo,3)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)


       !mu = 2

       !G_2^- Up phi_xpmu

       j_xeo = i_xpmu(i_xeo,2,i_eo)

       !i_s = 1
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo,1)%cl(1) + phi(j_xeo,4)%cl(1)
       Gammaphi%Cl(2) = phi(j_xeo,1)%cl(2) + phi(j_xeo,4)%cl(2)
       Gammaphi%Cl(3) = phi(j_xeo,1)%cl(3) + phi(j_xeo,4)%cl(3)

       psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo,1)%cl(1) = Dphi(i_xeo,1)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,1)%cl(2) = Dphi(i_xeo,1)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,1)%cl(3) = Dphi(i_xeo,1)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,4)%cl(1) = Dphi(i_xeo,4)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,4)%cl(2) = Dphi(i_xeo,4)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,4)%cl(3) = Dphi(i_xeo,4)%cl(3) - psi_x%Cl(3)

       !i_s = 2
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo,2)%cl(1) - phi(j_xeo,3)%cl(1)
       Gammaphi%Cl(2) = phi(j_xeo,2)%cl(2) - phi(j_xeo,3)%cl(2)
       Gammaphi%Cl(3) = phi(j_xeo,2)%cl(3) - phi(j_xeo,3)%cl(3)

       psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo,2)%cl(1) = Dphi(i_xeo,2)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,2)%cl(2) = Dphi(i_xeo,2)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,2)%cl(3) = Dphi(i_xeo,2)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,3)%cl(1) = Dphi(i_xeo,3)%cl(1) + psi_x%Cl(1)
       Dphi(i_xeo,3)%cl(2) = Dphi(i_xeo,3)%cl(2) + psi_x%Cl(2)
       Dphi(i_xeo,3)%cl(3) = Dphi(i_xeo,3)%cl(3) + psi_x%Cl(3)

       !mu = 3




       !G_3^- Up phi_xpmu
       j_xeo = i_xpmu(i_xeo,3,i_eo)

       !i_s = 1
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo,1)%cl(1) + cmplx(-aimag(phi(j_xeo,3)%cl(1)),real(phi(j_xeo,3)%cl(1)),dc)
       Gammaphi%Cl(2) = phi(j_xeo,1)%cl(2) + cmplx(-aimag(phi(j_xeo,3)%cl(2)),real(phi(j_xeo,3)%cl(2)),dc)
       Gammaphi%Cl(3) = phi(j_xeo,1)%cl(3) + cmplx(-aimag(phi(j_xeo,3)%cl(3)),real(phi(j_xeo,3)%cl(3)),dc)

       psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo,1)%cl(1) = Dphi(i_xeo,1)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,1)%cl(2) = Dphi(i_xeo,1)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,1)%cl(3) = Dphi(i_xeo,1)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,3)%cl(1) = Dphi(i_xeo,3)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo,3)%cl(2) = Dphi(i_xeo,3)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo,3)%cl(3) = Dphi(i_xeo,3)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !i_s = 2
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo,2)%cl(1) - cmplx(-aimag(phi(j_xeo,4)%cl(1)),real(phi(j_xeo,4)%cl(1)),dc)
       Gammaphi%Cl(2) = phi(j_xeo,2)%cl(2) - cmplx(-aimag(phi(j_xeo,4)%cl(2)),real(phi(j_xeo,4)%cl(2)),dc)
       Gammaphi%Cl(3) = phi(j_xeo,2)%cl(3) - cmplx(-aimag(phi(j_xeo,4)%cl(3)),real(phi(j_xeo,4)%cl(3)),dc)

       psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo,2)%cl(1) = Dphi(i_xeo,2)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,2)%cl(2) = Dphi(i_xeo,2)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,2)%cl(3) = Dphi(i_xeo,2)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,4)%cl(1) = Dphi(i_xeo,4)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo,4)%cl(2) = Dphi(i_xeo,4)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo,4)%cl(3) = Dphi(i_xeo,4)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !mu = 4




       !G_4^- Up phi_xpmu
       j_xeo = i_xpmu(i_xeo,4,i_eo)

       !i_s = 1
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo,1)%cl(1) - phi(j_xeo,3)%cl(1)
       Gammaphi%Cl(2) = phi(j_xeo,1)%cl(2) - phi(j_xeo,3)%cl(2)
       Gammaphi%Cl(3) = phi(j_xeo,1)%cl(3) - phi(j_xeo,3)%cl(3)
       psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo,1)%cl(1) = Dphi(i_xeo,1)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,1)%cl(2) = Dphi(i_xeo,1)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,1)%cl(3) = Dphi(i_xeo,1)%cl(3) - psi_x%Cl(3)


       Dphi(i_xeo,3)%cl(1) = Dphi(i_xeo,3)%cl(1) + psi_x%Cl(1)
       Dphi(i_xeo,3)%cl(2) = Dphi(i_xeo,3)%cl(2) + psi_x%Cl(2)
       Dphi(i_xeo,3)%cl(3) = Dphi(i_xeo,3)%cl(3) + psi_x%Cl(3)

       !i_s = 2
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo,2)%cl(1) - phi(j_xeo,4)%cl(1)
       Gammaphi%Cl(2) = phi(j_xeo,2)%cl(2) - phi(j_xeo,4)%cl(2)
       Gammaphi%Cl(3) = phi(j_xeo,2)%cl(3) - phi(j_xeo,4)%cl(3)
       psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo,2)%cl(1) = Dphi(i_xeo,2)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,2)%cl(2) = Dphi(i_xeo,2)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,2)%cl(3) = Dphi(i_xeo,2)%cl(3) - psi_x%Cl(3)


       Dphi(i_xeo,4)%cl(1) = Dphi(i_xeo,4)%cl(1) + psi_x%Cl(1)
       Dphi(i_xeo,4)%cl(2) = Dphi(i_xeo,4)%cl(2) + psi_x%Cl(2)
       Dphi(i_xeo,4)%cl(3) = Dphi(i_xeo,4)%cl(3) + psi_x%Cl(3)

       !mu = 1




       !G_1^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,1,i_eo)

       !i_s = 1
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo,1)%cl(1) - cmplx(-aimag(phi(j_xeo,4)%cl(1)),real(phi(j_xeo,4)%cl(1)),dc)
       Gammaphi%Cl(2) = phi(j_xeo,1)%cl(2) - cmplx(-aimag(phi(j_xeo,4)%cl(2)),real(phi(j_xeo,4)%cl(2)),dc)
       Gammaphi%Cl(3) = phi(j_xeo,1)%cl(3) - cmplx(-aimag(phi(j_xeo,4)%cl(3)),real(phi(j_xeo,4)%cl(3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo,1)%cl(1) = Dphi(i_xeo,1)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,1)%cl(2) = Dphi(i_xeo,1)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,1)%cl(3) = Dphi(i_xeo,1)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,4)%cl(1) = Dphi(i_xeo,4)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo,4)%cl(2) = Dphi(i_xeo,4)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo,4)%cl(3) = Dphi(i_xeo,4)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !i_s = 2
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo,2)%cl(1) - cmplx(-aimag(phi(j_xeo,3)%cl(1)),real(phi(j_xeo,3)%cl(1)),dc)
       Gammaphi%Cl(2) = phi(j_xeo,2)%cl(2) - cmplx(-aimag(phi(j_xeo,3)%cl(2)),real(phi(j_xeo,3)%cl(2)),dc)
       Gammaphi%Cl(3) = phi(j_xeo,2)%cl(3) - cmplx(-aimag(phi(j_xeo,3)%cl(3)),real(phi(j_xeo,3)%cl(3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo,2)%cl(1) = Dphi(i_xeo,2)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,2)%cl(2) = Dphi(i_xeo,2)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,2)%cl(3) = Dphi(i_xeo,2)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,3)%cl(1) = Dphi(i_xeo,3)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo,3)%cl(2) = Dphi(i_xeo,3)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo,3)%cl(3) = Dphi(i_xeo,3)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !mu = 2




       !G_2^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,2,i_eo)

       !i_s = 1
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo,1)%cl(1) - phi(j_xeo,4)%cl(1)
       Gammaphi%Cl(2) = phi(j_xeo,1)%cl(2) - phi(j_xeo,4)%cl(2)
       Gammaphi%Cl(3) = phi(j_xeo,1)%cl(3) - phi(j_xeo,4)%cl(3)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo,1)%cl(1) = Dphi(i_xeo,1)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,1)%cl(2) = Dphi(i_xeo,1)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,1)%cl(3) = Dphi(i_xeo,1)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,4)%cl(1) = Dphi(i_xeo,4)%cl(1) + psi_x%Cl(1)
       Dphi(i_xeo,4)%cl(2) = Dphi(i_xeo,4)%cl(2) + psi_x%Cl(2)
       Dphi(i_xeo,4)%cl(3) = Dphi(i_xeo,4)%cl(3) + psi_x%Cl(3)

       !i_s = 2
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo,2)%cl(1) + phi(j_xeo,3)%cl(1)
       Gammaphi%Cl(2) = phi(j_xeo,2)%cl(2) + phi(j_xeo,3)%cl(2)
       Gammaphi%Cl(3) = phi(j_xeo,2)%cl(3) + phi(j_xeo,3)%cl(3)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo,2)%cl(1) = Dphi(i_xeo,2)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,2)%cl(2) = Dphi(i_xeo,2)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,2)%cl(3) = Dphi(i_xeo,2)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,3)%cl(1) = Dphi(i_xeo,3)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,3)%cl(2) = Dphi(i_xeo,3)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,3)%cl(3) = Dphi(i_xeo,3)%cl(3) - psi_x%Cl(3)

       !mu = 3




       !G_3^+ Up phi_xmmu

       j_xeo = i_xmmu(i_xeo,3,i_eo)

       !i_s = 1
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo,1)%cl(1) - cmplx(-aimag(phi(j_xeo,3)%cl(1)),real(phi(j_xeo,3)%cl(1)),dc)
       Gammaphi%Cl(2) = phi(j_xeo,1)%cl(2) - cmplx(-aimag(phi(j_xeo,3)%cl(2)),real(phi(j_xeo,3)%cl(2)),dc)
       Gammaphi%Cl(3) = phi(j_xeo,1)%cl(3) - cmplx(-aimag(phi(j_xeo,3)%cl(3)),real(phi(j_xeo,3)%cl(3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo,1)%cl(1) = Dphi(i_xeo,1)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,1)%cl(2) = Dphi(i_xeo,1)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,1)%cl(3) = Dphi(i_xeo,1)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,3)%cl(1) = Dphi(i_xeo,3)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo,3)%cl(2) = Dphi(i_xeo,3)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo,3)%cl(3) = Dphi(i_xeo,3)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !i_s = 2
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo,2)%cl(1) + cmplx(-aimag(phi(j_xeo,4)%cl(1)),real(phi(j_xeo,4)%cl(1)),dc)
       Gammaphi%Cl(2) = phi(j_xeo,2)%cl(2) + cmplx(-aimag(phi(j_xeo,4)%cl(2)),real(phi(j_xeo,4)%cl(2)),dc)
       Gammaphi%Cl(3) = phi(j_xeo,2)%cl(3) + cmplx(-aimag(phi(j_xeo,4)%cl(3)),real(phi(j_xeo,4)%cl(3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo,2)%cl(1) = Dphi(i_xeo,2)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,2)%cl(2) = Dphi(i_xeo,2)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,2)%cl(3) = Dphi(i_xeo,2)%cl(3) - psi_x%Cl(3)

       Dphi(i_xeo,4)%cl(1) = Dphi(i_xeo,4)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo,4)%cl(2) = Dphi(i_xeo,4)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo,4)%cl(3) = Dphi(i_xeo,4)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !mu = 4




       !G_4^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,4,i_eo)

       !i_s = 1
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo,1)%cl(1) + phi(j_xeo,3)%cl(1)
       Gammaphi%Cl(2) = phi(j_xeo,1)%cl(2) + phi(j_xeo,3)%cl(2)
       Gammaphi%Cl(3) = phi(j_xeo,1)%cl(3) + phi(j_xeo,3)%cl(3)
       psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo,1)%cl(1) = Dphi(i_xeo,1)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,1)%cl(2) = Dphi(i_xeo,1)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,1)%cl(3) = Dphi(i_xeo,1)%cl(3) - psi_x%Cl(3)

       !i_s = 2
       !j_s = 4

       Dphi(i_xeo,3)%cl(1) = Dphi(i_xeo,3)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,3)%cl(2) = Dphi(i_xeo,3)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,3)%cl(3) = Dphi(i_xeo,3)%cl(3) - psi_x%Cl(3)

       Gammaphi%Cl(1) = phi(j_xeo,2)%cl(1) + phi(j_xeo,4)%cl(1)
       Gammaphi%Cl(2) = phi(j_xeo,2)%cl(2) + phi(j_xeo,4)%cl(2)
       Gammaphi%Cl(3) = phi(j_xeo,2)%cl(3) + phi(j_xeo,4)%cl(3)
       psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo,2)%cl(1) = Dphi(i_xeo,2)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,2)%cl(2) = Dphi(i_xeo,2)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,2)%cl(3) = Dphi(i_xeo,2)%cl(3) - psi_x%Cl(3)


       Dphi(i_xeo,4)%cl(1) = Dphi(i_xeo,4)%cl(1) - psi_x%Cl(1)
       Dphi(i_xeo,4)%cl(2) = Dphi(i_xeo,4)%cl(2) - psi_x%Cl(2)
       Dphi(i_xeo,4)%cl(3) = Dphi(i_xeo,4)%cl(3) - psi_x%Cl(3)

    end do

!!    if (timing) then
!!       outtime = mpi_wtime()
!!       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
!!    end if
!!
!!    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
!!    waste = commtime/(outtime - intime)
!!    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)

  end subroutine EOWilson_ex

end module EOWilsonMatrix_EO
