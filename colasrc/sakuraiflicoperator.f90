

!! Filename : $Source$
!! Author : Waseem Kamleh
!! Created On : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag : $Name$
!! Revision : $Revision$
!! Update History : $Log$

!! Filename : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/chiralflicoperator.f90,v $
!! Author : Waseem Kamleh
!! Created On : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag : $Name: $
!! Revision : $Revision: 0.1 $
!! Update History : $Log: chiralflicoperator.f90,v $
!! Update History : Revision 0.1 2004/07/21 07:15:33 wkamleh
!! Update History : Orion version
!! Update History :
!! Update History : Revision 1.1.1.2 2004/06/20 07:42:00 wkamleh
!! Update History : Hydra version
!! Update History :




module SakuraiFLICOperator

  use Timer
  use MPIInterface
  use FatLinks
  use CloverFmunu
  use FermionField
  use SpinorTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  use ColourFieldOps
  implicit none
  private

  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: Up_xd
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: Um_xd
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: F_clmunu

  !MFI Fat Link Clover-Wilson fermions using negative mass term, in Sakurai basis
  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), public :: c_sw = 1.0d0, kappa, mfic_sw
  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean

  integer, dimension(:), allocatable, private :: mapz_1, mapt_1

  public :: InitialiseFLICSakurai
  public :: FinaliseFLICSakurai
  public :: FLICSakurai

contains

  subroutine InitialiseFLICSakurai(U_xd, UFL_xd, F_munu, u0, u0fl, mass, c_sw, bct)

    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd, UFL_xd, F_munu

    real(DP) :: mass, c_sw, u0, u0fl, bct, mfic_sw
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfir, mfir_fl !coefficents to be absorbed.

    type(colour_matrix) :: U_mu, Ufl_mu ! temporary storage
    type(colour_matrix), dimension(nplaq) :: F_x

    if ( .not. allocated(Up_xd) ) call CreateGaugeField(Up_xd,1)
    if ( .not. allocated(Um_xd) ) call CreateGaugeField(Um_xd,1)
    if ( .not. allocated(F_clmunu) ) allocate(F_clmunu(nxp,nyp,nzp,ntp,nplaq))

    Up_xd(1:nx,1:ny,1:nz,1:nt,:) = U_xd(1:nx,1:ny,1:nz,1:nt,:)
    Um_xd(1:nx,1:ny,1:nz,1:nt,:) = UFL_xd(1:nx,1:ny,1:nz,1:nt,:)

    m_f = mass

    kappa = 1.0d0/(8.0d0 - 2.0d0*m_f)

    !call CalculateFmunuClover(F_munu,UFL_xd)

    !if ( use_hopping_parameter ) then
    mfic_sw = kappa*c_sw/(u0fl**4)
    !else
    ! mfic_sw = 0.5d0*c_sw/(u0fl**4)
    !end if

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       F_clmunu(ix,iy,iz,it,1)%Cl = -i*mfic_sw*F_munu(ix,iy,iz,it,1)%Cl
       F_clmunu(ix,iy,iz,it,2)%Cl = mfic_sw*F_munu(ix,iy,iz,it,2)%Cl
       F_clmunu(ix,iy,iz,it,3)%Cl = -i*mfic_sw*F_munu(ix,iy,iz,it,3)%Cl
       F_clmunu(ix,iy,iz,it,4)%Cl = -i*mfic_sw*F_munu(ix,iy,iz,it,4)%Cl
       F_clmunu(ix,iy,iz,it,5)%Cl = -mfic_sw*F_munu(ix,iy,iz,it,5)%Cl
       F_clmunu(ix,iy,iz,it,6)%Cl = -i*mfic_sw*F_munu(ix,iy,iz,it,6)%Cl
    end do; end do; end do; end do

    !if ( use_hopping_parameter ) then
    mfir = (kappa*0.5d0/u0)
    mfir_fl = (kappa*0.5d0/u0fl)
    !else
    ! mfir = (0.25d0/u0)
    ! mfir_fl = (0.25d0/u0fl)
    !end if
    !Absorb the mean field improvement into the gauge fields.
    !Define symmetrised and antisymmetrised gauge fields for efficiency (halve multiplies)

    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          U_mu%Cl = mfir*Up_xd(ix,iy,iz,it,mu)%Cl
          Ufl_mu%Cl = mfir_fl*Um_xd(ix,iy,iz,it,mu)%Cl

          Up_xd(ix,iy,iz,it,mu)%Cl = U_mu%Cl + Ufl_mu%Cl

          Um_xd(ix,iy,iz,it,mu)%Cl = U_mu%Cl - Ufl_mu%Cl

       end do; end do; end do; end do
    end do

    if ( bct/=1.0d0 ) then
       call SetBoundaryConditions(Up_xd,1.0d0,1.0d0,1.0d0,bct)
       call SetBoundaryConditions(Um_xd,1.0d0,1.0d0,1.0d0,bct)
    end if

    call ShadowGaugeField(Up_xd,0)
    call ShadowGaugeField(Um_xd,0)

    if ( .not. allocated(mapz_1) ) allocate(mapz_1(0:nz+1))
    if ( .not. allocated(mapt_1) ) allocate(mapt_1(0:nt+1))

    if ( nprocz > 1 ) then
       mapz_1(0) = nz+1
    else
       mapz_1(0) = nz
    end if
    mapz_1(1:nz+1) = mapz(1:nz+1)
    if ( (nproct > 1) ) then
       mapt_1(0) = nt+1
    else
       mapt_1(0) = nt
    end if
    mapt_1(1:nt+1) = mapt(1:nt+1)

  end subroutine InitialiseFLICSakurai

  subroutine FinaliseFLICSakurai

    if ( allocated(Up_xd) ) call DestroyGaugeField(Up_xd)
    if ( allocated(Um_xd) ) call DestroyGaugeField(Um_xd)
    if ( allocated(F_clmunu) ) deallocate(F_clmunu)
    if ( allocated(mapz_1) ) deallocate(mapz_1)
    if ( allocated(mapt_1) ) deallocate(mapt_1)

  end subroutine FinaliseFLICSakurai

  subroutine FLICSakurai(phi, Dphi)

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    integer :: ix,iy,iz,it,is
    integer :: jx,jy,jz,jt,js
    complex(dc) :: psi_x
    type(colour_vector) :: Gammaphi

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    if (timing) intime = mpi_wtime()
    commtime = 0.0d0





    if (nprocz > 1) then
       t0 = mpi_wtime()
       mrank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
       prank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)

       do is=1,ns
          do it=1,nt
             call MPI_ISSend(phi(1,1,1 ,it,is), nc*nxp*ny, mpi_dc, mrank, it+ns*is, mpi_comm, sendrecv_reqz(it,is,1), mpierror)
             call MPI_IRecv (phi(1,1,nz+1,it,is), nc*nxp*ny, mpi_dc, prank, it+ns*is, mpi_comm, sendrecv_reqz(it,is,2), mpierror)
          end do
       end do
       t1 = mpi_wtime()
       commtime = commtime + (t1 - t0)
    end if

    Dphi = phi

    pm = (/ 1.0d0, -1.0d0, 1.0d0, -1.0d0 /)
    ! iplaq = 1

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          psi_x = F_clmunu(ix,iy,iz,it,1)%Cl(1,1)*phi(ix,iy,iz,it,is)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,1)%Cl(1,2)*phi(ix,iy,iz,it,is)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,1)%Cl(1,3)*phi(ix,iy,iz,it,is)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + pm(is)*psi_x

          psi_x = F_clmunu(ix,iy,iz,it,1)%Cl(2,1)*phi(ix,iy,iz,it,is)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,1)%Cl(2,2)*phi(ix,iy,iz,it,is)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,1)%Cl(2,3)*phi(ix,iy,iz,it,is)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + pm(is)*psi_x

          psi_x = F_clmunu(ix,iy,iz,it,1)%Cl(3,1)*phi(ix,iy,iz,it,is)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,1)%Cl(3,2)*phi(ix,iy,iz,it,is)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,1)%Cl(3,3)*phi(ix,iy,iz,it,is)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + pm(is)*psi_x

       end do; end do; end do; end do
    end do

    ! iplaq = 2

    do is=1,ns
       js = 3 - is + (is/3)*4
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          psi_x = F_clmunu(ix,iy,iz,it,2)%Cl(1,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,2)%Cl(1,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,2)%Cl(1,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + pm(is)*psi_x

          psi_x = F_clmunu(ix,iy,iz,it,2)%Cl(2,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,2)%Cl(2,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,2)%Cl(2,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + pm(is)*psi_x

          psi_x = F_clmunu(ix,iy,iz,it,2)%Cl(3,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,2)%Cl(3,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,2)%Cl(3,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + pm(is)*psi_x

       end do; end do; end do; end do
    end do

    ! iplaq = 3
    do is=1,ns
       js = 5 - is
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          psi_x = F_clmunu(ix,iy,iz,it,3)%Cl(1,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,3)%Cl(1,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,3)%Cl(1,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x

          psi_x = F_clmunu(ix,iy,iz,it,3)%Cl(2,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,3)%Cl(2,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,3)%Cl(2,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x

          psi_x = F_clmunu(ix,iy,iz,it,3)%Cl(3,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,3)%Cl(3,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,3)%Cl(3,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x

       end do; end do; end do; end do
    end do

    ! iplaq = 4
    do is=1,ns
       js = 3 - is + (is/3)*4
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          psi_x = F_clmunu(ix,iy,iz,it,4)%Cl(1,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,4)%Cl(1,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,4)%Cl(1,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x

          psi_x = F_clmunu(ix,iy,iz,it,4)%Cl(2,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,4)%Cl(2,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,4)%Cl(2,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x

          psi_x = F_clmunu(ix,iy,iz,it,4)%Cl(3,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,4)%Cl(3,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,4)%Cl(3,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x

       end do; end do; end do; end do
    end do

    ! iplaq = 5
    do is=1,ns
       js = 5 - is
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          psi_x = F_clmunu(ix,iy,iz,it,5)%Cl(1,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,5)%Cl(1,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,5)%Cl(1,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + pm(is)*psi_x

          psi_x = F_clmunu(ix,iy,iz,it,5)%Cl(2,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,5)%Cl(2,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,5)%Cl(2,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + pm(is)*psi_x

          psi_x = F_clmunu(ix,iy,iz,it,5)%Cl(3,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,5)%Cl(3,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,5)%Cl(3,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + pm(is)*psi_x

       end do; end do; end do; end do
    end do

    ! iplaq = 6
    do is=1,ns
       js = is + 2 - (is/3)*4
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          psi_x = F_clmunu(ix,iy,iz,it,6)%Cl(1,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,6)%Cl(1,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,6)%Cl(1,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + pm(is)*psi_x

          psi_x = F_clmunu(ix,iy,iz,it,6)%Cl(2,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,6)%Cl(2,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,6)%Cl(2,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + pm(is)*psi_x

          psi_x = F_clmunu(ix,iy,iz,it,6)%Cl(3,1)*phi(ix,iy,iz,it,js)%Cl(1) + &
               & F_clmunu(ix,iy,iz,it,6)%Cl(3,2)*phi(ix,iy,iz,it,js)%Cl(2) + &
               & F_clmunu(ix,iy,iz,it,6)%Cl(3,3)*phi(ix,iy,iz,it,js)%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + pm(is)*psi_x

       end do; end do; end do; end do
    end do

    if (nproct > 1) then
       t0 = mpi_wtime()
       mrank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
       prank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)

       do is=1,ns
          call MPI_ISSend(phi(1,1,1,1 ,is), nc*nxp*nyp*nz, mpi_dc, mrank, is, mpi_comm, sendrecv_reqt(is,1), mpierror)
          call MPI_IRecv (phi(1,1,1,nt+1,is), nc*nxp*nyp*nz, mpi_dc, prank, is, mpi_comm, sendrecv_reqt(is,2), mpierror)
       end do
       t1 = mpi_wtime()
       commtime = commtime + (t1 - t0)
    end if

    !mu = 1





    !G_1^+ Um phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; ix=nx; do jx=1,nx

          Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

          psi_x = Um_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Um_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Um_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + cmplx(-aimag(psi_x),real(psi_x),dc)

          ix = jx

       end do; end do; end do; end do
    end do

    !G_1^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; ix=nx; do jx=1,nx

          Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

          psi_x = Up_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Up_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Up_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + cmplx(-aimag(psi_x),real(psi_x),dc)

          ix = jx

       end do; end do; end do; end do
    end do

    !mu = 2





    !G_2^+ Um phi_xpmu

    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; iy=ny; do jy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) - pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) - pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) - pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

          psi_x = Um_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*psi_x

          psi_x = Um_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*psi_x

          psi_x = Um_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*psi_x

       end do; iy=jy; end do; end do; end do
    end do

    !G_2^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; iy=ny; do jy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) + pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) + pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) + pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

          psi_x = Up_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*psi_x

          psi_x = Up_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*psi_x

          psi_x = Up_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*psi_x

       end do; iy=jy; end do; end do; end do
    end do

    if (nprocz > 1) then
       t0 = mpi_wtime()
       call MPI_WaitAll(nt*ns*2,sendrecv_reqz,sendrecvz_status,mpierror)
       t1 = mpi_wtime()
       commtime = commtime + (t1 - t0)
    end if

    !mu = 3





    !G_3^+ Um phi_xpmu

    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0

    do is=1,nsp
       js = is+2
       do it=1,nt; do iz=1,nz; jz=mapz_1(iz+1); do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

          psi_x = Um_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Um_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Um_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

       end do; end do; end do; end do
    end do

    !G_3^- Up phi_xpmu

    do is=1,nsp
       js = is+2
       do it=1,nt; do iz=1,nz; jz=mapz_1(iz+1); do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

          psi_x = Up_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Up_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Up_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

       end do; end do; end do; end do
    end do

    if (nprocz > 1) then
       t0 = mpi_wtime()
       mrank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
       prank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)

       do is=1,ns
          do it=1,nt
             call MPI_ISSend(phi(1,1,nz ,it,is), nc*nxp*ny, mpi_dc, prank, it+ns*is, mpi_comm, sendrecv_reqz(it,is,1), mpierror)
             call MPI_IRecv (phi(1,1,nz+1,it,is), nc*nxp*ny, mpi_dc, mrank, it+ns*is, mpi_comm, sendrecv_reqz(it,is,2), mpierror)
          end do
       end do
       t1 = mpi_wtime()
       commtime = commtime + (t1 - t0)
    end if

    if (nproct > 1) then
       t0 = mpi_wtime()
       call MPI_WaitAll(2*ns,sendrecv_reqt,sendrecvt_status,mpierror)
       t1 = mpi_wtime()
       commtime = commtime + (t1 - t0)
    end if

    !mu = 4





    !G_4^+ Um phi_xpmu

    do is=1,nsp
       js = is+2
       do it=1,nt; jt=mapt_1(it+1); do iz=1,nz; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = 2.0d0*phi(ix,iy,iz,jt,is)%Cl(1)
          Gammaphi%Cl(2) = 2.0d0*phi(ix,iy,iz,jt,is)%Cl(2)
          Gammaphi%Cl(3) = 2.0d0*phi(ix,iy,iz,jt,is)%Cl(3)

          psi_x = Um_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          !Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + psi_x

          psi_x = Um_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          !Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + psi_x

          psi_x = Um_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          !Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + psi_x

       end do; end do; end do; end do
    end do

    !G_4^- Up phi_xpmu

    do is=1,nsp
       js = is+2
       do it=1,nt; jt=mapt_1(it+1); do iz=1,nz; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = 2.0d0*phi(ix,iy,iz,jt,js)%Cl(1)
          Gammaphi%Cl(2) = 2.0d0*phi(ix,iy,iz,jt,js)%Cl(2)
          Gammaphi%Cl(3) = 2.0d0*phi(ix,iy,iz,jt,js)%Cl(3)

          psi_x = Up_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
          !Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - psi_x

          psi_x = Up_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
          !Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - psi_x

          psi_x = Up_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)
          !Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - psi_x

       end do; end do; end do; end do
    end do

    if (nproct > 1) then
       t0 = mpi_wtime()
       mrank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
       prank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)

       do is=1,ns
          call MPI_ISSend(phi(1,1,1,nt ,is), nc*nxp*nyp*nz, mpi_dc, prank, is, mpi_comm, sendrecv_reqt(is,1), mpierror)
          call MPI_IRecv (phi(1,1,1,nt+1,is), nc*nxp*nyp*nz, mpi_dc, mrank, is, mpi_comm, sendrecv_reqt(is,2), mpierror)
       end do
       t1 = mpi_wtime()
       commtime = commtime + (t1 - t0)
    end if

    !mu = 1





    !G_1^- Um phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jx=nx; do ix=1,nx

          Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

          psi_x = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - cmplx(-aimag(psi_x),real(psi_x),dc)

          jx = ix

       end do; end do; end do; end do
    end do

    !G_1^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jx=nx; do ix=1,nx

          Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

          psi_x = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - cmplx(-aimag(psi_x),real(psi_x),dc)

          jx = ix

       end do; end do; end do; end do
    end do

    !mu = 2





    !G_2^- Um phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; jy=ny; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) + pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) + pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) + pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

          psi_x = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*psi_x

          psi_x = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*psi_x

          psi_x = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*psi_x

       end do; jy=iy; end do; end do; end do
    end do

    !G_2^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; jy=ny; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) - pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) - pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) - pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

          psi_x = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*psi_x

          psi_x = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*psi_x

          psi_x = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*psi_x

       end do; jy=iy; end do; end do; end do
    end do

    if (nprocz > 1) then
       t0 = mpi_wtime()
       call MPI_WaitAll(nt*ns*2,sendrecv_reqz,sendrecvz_status,mpierror)
       t1 = mpi_wtime()
       commtime = commtime + (t1 - t0)
    end if
    !mu = 3





    !G_3^- Um phi_xmmu

    do is=1,nsp
       js = is+2
       do it=1,nt; do iz=1,nz; jz=mapz_1(iz-1); do iy=1,ny; do ix=1,nx
          Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

          psi_x = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

       end do; end do; end do; end do
    end do

    !G_3^+ Up phi_xmmu

    do is=1,nsp
       js = is+2
       do it=1,nt; do iz=1,nz; jz = mapz_1(iz-1); do iy=1,ny; do ix=1,nx


          Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

          psi_x = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

       end do; end do; end do; end do
    end do

    if (nproct > 1) then
       t0 = mpi_wtime()
       call MPI_WaitAll(2*ns,sendrecv_reqt,sendrecvt_status,mpierror)
       t1 = mpi_wtime()
       commtime = commtime + (t1 - t0)
    end if

    !mu = 4





    !G_4^- Um phi_xmmu

    do is=1,nsp
       js = is+2
       do it=1,nt; jt=mapt_1(it-1); do iz=1,nz; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = 2.0d0*phi(ix,iy,iz,jt,js)%Cl(1)
          Gammaphi%Cl(2) = 2.0d0*phi(ix,iy,iz,jt,js)%Cl(2)
          Gammaphi%Cl(3) = 2.0d0*phi(ix,iy,iz,jt,js)%Cl(3)

          psi_x = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
          !Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + psi_x

          psi_x = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
          !Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + psi_x

          psi_x = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)
          !Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + psi_x

       end do; end do; end do; end do
    end do

    !G_4^+ Up phi_xmmu

    do is=1,nsp
       js = is+2
       do it=1,nt; jt=mapt_1(it-1); do iz=1,nz; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = 2.0d0*phi(ix,iy,iz,jt,is)%Cl(1)
          Gammaphi%Cl(2) = 2.0d0*phi(ix,iy,iz,jt,is)%Cl(2)
          Gammaphi%Cl(3) = 2.0d0*phi(ix,iy,iz,jt,is)%Cl(3)

          psi_x = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          !Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - psi_x

          psi_x = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          !Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - psi_x

          psi_x = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          !Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - psi_x

       end do; end do; end do; end do
    end do

    if (timing) then
       outtime = mpi_wtime()
       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    end if
    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
    waste = commtime/(outtime - intime)
    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)

  end subroutine FLICSakurai

end module SakuraiFLICOperator
