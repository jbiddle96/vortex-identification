#include "defines.h"

module SourceTypes
  use Kinds
  use LatticeSize
  use TextIO
  use MPIInterface
  use Dilution
  use ColourTypes
  implicit none
  private

  type, abstract, public :: SourceType
   contains
     procedure(txt_io), deferred :: Get
     procedure(txt_io), deferred :: Put
     procedure :: Read => ReadSourceType
     procedure :: Write => WriteSourceType
  end type SourceType

  abstract interface
     subroutine txt_io(this,file_unit)
       import :: SourceType
       class(SourceType) :: this
       integer :: file_unit
     end subroutine txt_io
  end interface

  type, extends(SourceType), public :: PointSourceType
     integer :: jx
     integer :: jy
     integer :: jz
     integer :: jt
   contains
     procedure :: Get => Get_Point
     procedure :: Put => Put_Point
  end type PointSourceType

  type, extends(SourceType), public :: WallSourceType
     integer :: jt
   contains
     procedure :: Get => Get_Wall
     procedure :: Put => Put_Wall
  end type WallSourceType

  type, extends(PointSourceType), public :: SmearedSourceType
     !integer  :: jx, jy, jz, jt
     integer  :: n_smsrc
     real(dp) :: alpha_smsrc
     logical  :: UseUzero
     real(dp) :: u0_smsrc
     logical  :: UseStoutLinks
     real(dp) :: alpha_stout
     integer  :: n_stout
   contains
     procedure :: Get => Get_Smeared
     procedure :: Put => Put_Smeared
  end type SmearedSourceType

  type, extends(SourceType), public :: NoiseSourceType
     character(len=_FILENAME_LEN_) :: NoiseFile
     integer :: iColourDilution
     integer :: iSpinDilution
     integer :: iTimeDilution
     type(DilutionType) :: diln
   contains
     procedure :: Get => Get_Noise
     procedure :: Put => Put_Noise
  end type NoiseSourceType
  
  type, extends(SourceType), public :: SSTSourceType
     real(dp) :: kappa
     integer :: j_mu
     integer :: it_s
     integer :: j_x(nd-1)
     integer :: p_x(nd-1)
     integer :: currentType
     character(len=_FILENAME_LEN_) :: SourcePropFile
   contains
     procedure :: Get => Get_SST
     procedure :: Put => Put_SST
  end type SSTSourceType

  public :: SelectSourceType

contains

  subroutine ReadSourceType(this,filename)
    class(SourceType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='read')
    call this%Get(file_unit)
    call CloseTextfile(file_unit)

  end subroutine ReadSourceType

  subroutine WriteSourceType(this,filename)
    class(SourceType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='write')
    call this%Put(file_unit)
    call CloseTextfile(file_unit)

  end subroutine WriteSourceType

  subroutine SelectSourceType(qpsrc,iSourceType)
    class(SourceType), pointer :: qpsrc
    integer :: iSourceType

    select case (iSourceType)
    case(1)
       allocate(PointSourceType :: qpsrc)
    case(2)
       allocate(WallSourceType :: qpsrc)
    case(3)
       allocate(SmearedSourceType :: qpsrc)
    case(4)
       allocate(NoiseSourceType :: qpsrc)
    case(5)
       allocate(SSTSourceType :: qpsrc)
    end select

  end subroutine SelectSourceType

  subroutine Get_point(this,file_unit)
    class(PointSourceType) :: this
    integer :: file_unit
    call Get(this%jx,file_unit)
    call Get(this%jy,file_unit)
    call Get(this%jz,file_unit)
    call Get(this%jt,file_unit)
  end subroutine Get_point

  subroutine Get_wall(this,file_unit)
    class(WallSourceType) :: this
    integer :: file_unit
    call Get(this%jt,file_unit)
  end subroutine Get_wall

  subroutine Get_smeared(this,file_unit)
    class(SmearedSourceType) :: this
    integer :: file_unit
    call Get(this%jx,file_unit)
    call Get(this%jy,file_unit)
    call Get(this%jz,file_unit)
    call Get(this%jt,file_unit)

    call Get(this%n_smsrc,file_unit)
    call Get(this%alpha_smsrc,file_unit)
    call Get(this%UseUzero,file_unit)
    call Get(this%u0_smsrc,file_unit)
    call Get(this%UseStoutLinks,file_unit)
    call Get(this%alpha_stout,file_unit)
    call Get(this%n_stout,file_unit)

  end subroutine Get_smeared

  subroutine Get_noise(this,file_unit)
    class(NoiseSourceType) :: this
    integer :: file_unit
    call Get(this%NoiseFile,file_unit)
    call Get(this%iColourDilution,file_unit)
    call Get(this%iSpinDilution,file_unit)
    call Get(this%iTimeDilution,file_unit)
  end subroutine Get_noise

  subroutine Get_sst(this,file_unit)
    class(SSTSourceType) :: this
    integer :: file_unit
    call Get(this%SourcePropFile,file_unit)
    call Get(this%kappa,file_unit)
    call Get(this%j_mu,file_unit)
    call Get(this%it_s,file_unit)
    call Get(this%j_x,file_unit)
    call Get(this%p_x,file_unit)
    call Get(this%currentType,file_unit)
  end subroutine Get_sst

  subroutine Put_point(this,file_unit)
    class(PointSourceType) :: this
    integer :: file_unit
    if (i_am_root) then
       write(file_unit,'(i20,a)') this%jx, " !qpsrc_pt%jx"
       write(file_unit,'(i20,a)') this%jy, " !qpsrc_pt%jy"
       write(file_unit,'(i20,a)') this%jz, " !qpsrc_pt%jz"
       write(file_unit,'(i20,a)') this%jt, " !qpsrc_pt%jt"
    end if
  end subroutine Put_point

  subroutine Put_wall(this,file_unit)
    class(WallSourceType) :: this
    integer :: file_unit
    if (i_am_root) then
       write(file_unit,'(i20,a)') this%jt, " !qpsrc_wl%jt"
    end if
  end subroutine Put_wall

  subroutine Put_smeared(this,file_unit)
    class(SmearedSourceType) :: this
    integer :: file_unit
    if (i_am_root) then
       write(file_unit,'(i20,a)') this%jx, " !qpsrc_sm%jx"
       write(file_unit,'(i20,a)') this%jy, " !qpsrc_sm%jy"
       write(file_unit,'(i20,a)') this%jz, " !qpsrc_sm%jz"
       write(file_unit,'(i20,a)') this%jt, " !qpsrc_sm%jt"

       write(file_unit,'(i20,a)') this%n_smsrc, " !qpsrc_sm%n_smsrc"
       write(file_unit,'(f20.10,a)') this%alpha_smsrc, " !qpsrc_sm%alpha_smsrc"
       write(file_unit,'(l20,a)') this%UseUzero, " !qpsrc_sm%UseUzero"
       write(file_unit,'(f20.10,a)') this%u0_smsrc, " !qpsrc_sm%u0_smsrc"
       write(file_unit,'(l20,a)') this%UseStoutLinks, " !qpsrc_sm%UseStoutLinks"
       write(file_unit,'(f20.10,a)') this%alpha_stout, " !qpsrc_sm%alpha_stout"
       write(file_unit,'(i20,a)') this%n_stout, " !qpsrc_sm%n_stout"
    end if
  end subroutine Put_smeared

  subroutine Put_noise(this,file_unit)
    class(NoiseSourceType) :: this
    integer :: file_unit
    if (i_am_root) then
       write(file_unit,'(a)') trim(this%NoiseFile)
       write(file_unit,'(i20,a)') this%iColourDilution, " !qpsrc_ns%ColourDilution"
       write(file_unit,'(i20,a)') this%iSpinDilution, " !qpsrc_ns%iSpin_dilution"
       write(file_unit,'(i20,a)') this%iTimeDilution, " !qpsrc_ns%iTime_dilution"
    end if
  end subroutine Put_noise

  subroutine Put_sst(this,file_unit)
    class(SSTSourceType) :: this
    integer :: file_unit
    if (i_am_root) then
       write(file_unit,'(a)') trim(this%SourcePropFile)
       write(file_unit,'(f20.10,a)') this%kappa, " !qpsrc_sst%kappa"
       write(file_unit,'(i20,a)') this%j_mu, " !qpsrc_sst%j_mu"
       write(file_unit,'(i20,a)') this%it_s, " !qpsrc_sst%it_s"
       write(file_unit,'(a5,3i5,a)') "", this%j_x, " !qpsrc_sst%j_x"
       write(file_unit,'(a5,3i5,a)') "", this%p_x, " !qpsrc_sst%p_x"
       write(file_unit,'(i20,a)') this%currentType, " !qpsrc_sst%currentType"
    end if
  end subroutine Put_sst


end module SourceTypes
