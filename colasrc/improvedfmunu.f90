!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/improvedfmunu.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: improvedfmunu.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:02  wkamleh
!! Update History  : Hydra version
!! Update History  :

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/improvedfmunu.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:02:06 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: improvedfmunu.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:02  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module ImprovedFmunu

  use ColourTypes
  use GaugeField
  use CloverFmunu
  use GaugeFieldMPIComms
  use LatticeSize
  use Kinds
  use MatrixAlgebra
  implicit none
  private

  logical, public :: ImprovedClover = .false.
  integer, public :: CloverLoops = 1

  public :: operator(+), matrix_plus_matrix

  interface operator(+)
     module procedure matrix_plus_matrix
  end interface

  public :: CalculateImprovedFmunu
  public :: CalculateCloverTerm
  public :: UU
  public :: UUdag
  public :: UdagU
  public :: UdagUdag

contains

  subroutine CalculateImprovedFmunu(F_munu,U_xd,CloverLoops,u0)
    ! begin args: F_munu, U_xd, CloverLoops, u0

    !The 3-loop improved F_munu is just really the same calculation of the usual clover term,
    !but performed 3 times, once for each set of "links". The first set is the standard links,
    !and the second and third set are the 2-link and 3-link analogies.

    type(colour_matrix), dimension(:,:,:,:,:) :: F_munu !Clover based field strength tensor
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: CloverLoops
    real(dp) :: u0

    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: F_clmunu !Clover based field strength tensor
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: U1_xd,U2_xd, U3_xd
    type(colour_matrix), dimension(:,:,:,:), allocatable  :: U_muxpmu
    type(colour_matrix), dimension(:,:,:,:), allocatable  :: C_munu
    complex(DC), dimension(:,:,:,:), allocatable  :: TrF_munu
#define Ci_munu U_muxpmu

    integer :: mu, nu, ic, jc, iplaq
    real(DP) :: k1,k2,k3,k5 ! 1x1, 2x2, 2x1+1x2, 3x3 loop coefficients

    ! begin execution
    allocate(F_clmunu(nx,ny,nz,nt,nplaq))
    allocate(U1_xd(nx,ny,nz,nt,nd))
    allocate(U2_xd(nx,ny,nz,nt,nd))
    allocate( U3_xd(nx,ny,nz,nt,nd))
    allocate(U_muxpmu(nx,ny,nz,nt))
    allocate(C_munu(nx,ny,nz,nt))
    allocate(TrF_munu(nx,ny,nz,nt))

    U1_xd = U_xd(1:nx,1:ny,1:nz,1:nt,:)

    if ( CloverLoops == 1 ) then
       k1 = 1.0d0
       k2 = 0.0d0
       k3 = 0.0d0
       k5 = 0.0d0
    else if ( CloverLoops == 2) then
       k1 = 5.0d0/3.0d0
       k2 = 0.0d0
       k3 = -1.0d0/(6.0d0*u0**2)
       k5 = 0.0d0
       !Calculate the 2-link sets
       do mu=1,nd
          U_muxpmu = CShiftGaugePlane(U1_xd(:,:,:,:,mu),mu,1)
          call UU(U2_xd(:,:,:,:,mu),U1_xd(:,:,:,:,mu),U_muxpmu)
       end do
    else if ( CloverLoops == 3) then
       k5 = (1.0d0/90.0d0)/u0**8
       k1 = 19.0d0/9.0d0 - 55.0d0*k5
       k2 = (1.0d0/36.0d0 - 16.0*k5)/u0**4
       k3 = 0.0d0
       !Calculate the 2-link and 3-link sets
       do mu=1,nd
          U_muxpmu = CShiftGaugePlane(U1_xd(:,:,:,:,mu),mu,1)
          call UU(U2_xd(:,:,:,:,mu),U1_xd(:,:,:,:,mu),U_muxpmu)
          U_muxpmu = CShiftGaugePlane(U_muxpmu,mu,1)
          call UU(U3_xd(:,:,:,:,mu),U2_xd(:,:,:,:,mu),U_muxpmu)
       end do
    end if

    !Calculate the clover terms, and combine with appropriate coefficients.
    do mu=1,nd
       do nu=mu+1,nd
          iplaq = mu+nu-1-1/mu

          C_munu = zero_matrix
          if ( CloverLoops == 1) then
             call CalculateCloverTerm(U1_xd,U1_xd,mu,nu,1,1,C_munu)
          else if ( CloverLoops == 2) then
             call CalculateCloverTerm(U1_xd,U1_xd,mu,nu,1,1,Ci_munu)
             C_munu = C_munu + k1*Ci_munu

             call CalculateCloverTerm(U2_xd,U1_xd,mu,nu,2,1,Ci_munu)
             C_munu = C_munu + k3*Ci_munu

             call CalculateCloverTerm(U1_xd,U2_xd,mu,nu,1,2,Ci_munu)
             C_munu = C_munu + k3*Ci_munu
          else if ( CloverLoops == 3) then
             call CalculateCloverTerm(U1_xd,U1_xd,mu,nu,1,1,Ci_munu)
             C_munu = C_munu + k1*Ci_munu

             call CalculateCloverTerm(U2_xd,U2_xd,mu,nu,2,2,Ci_munu)
             C_munu = C_munu + k2*Ci_munu

             call CalculateCloverTerm(U3_xd,U3_xd,mu,nu,3,3,Ci_munu)
             C_munu = C_munu + k5*Ci_munu
          end if

          do ic=1,nc; do jc=1,nc
             F_clmunu(:,:,:,:,iplaq)%Cl(ic,jc) = (C_munu(:,:,:,:)%Cl(ic,jc)-conjg(C_munu(:,:,:,:)%Cl(jc,ic)))/8.0d0
          end do; end do

          if ( CloverLoops >= 2 ) then
             !Make F_munu traceless, like the Gellmann matrices.
             TrF_munu = 0.0d0
             do ic=1,nc
                TrF_munu(:,:,:,:) = TrF_munu(:,:,:,:) + F_clmunu(:,:,:,:,iplaq)%Cl(ic,ic)
             end do

             do ic=1,nc
                F_clmunu(:,:,:,:,iplaq)%Cl(ic,ic) = F_clmunu(:,:,:,:,iplaq)%Cl(ic,ic) - TrF_munu(:,:,:,:)/3.0d0
             end do
          end if

       end do
    end do

    F_munu(1:nx,1:ny,1:nz,1:nt,:) = F_clmunu

#undef Ci_munu

    deallocate(F_clmunu)
    deallocate(U1_xd)
    deallocate(U2_xd)
    deallocate( U3_xd)
    deallocate(U_muxpmu)
    deallocate(C_munu)
    deallocate(TrF_munu)

  end subroutine CalculateImprovedFmunu

  subroutine CalculateCloverTerm(Umu_xd,Unu_xd,mu,nu,musize,nusize,C_munu)
    ! begin args: Umu_xd, Unu_xd, mu, nu, musize, nusize, C_munu

    type(colour_matrix), dimension(:,:,:,:,:) :: Umu_xd,Unu_xd
    integer :: mu, nu, musize, nusize
    type(colour_matrix), dimension(:,:,:,:) :: C_munu

    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:), allocatable  :: U_nuxpmu, U_muxpnu
    type(colour_matrix), dimension(:,:,:,:), allocatable  :: UmuUnu, UmudagUnudag, UnuUmudag, UnudagUmu !Gauge field products
#define U_munu U_muxpnu
    integer :: ilink


    ! begin execution
    allocate(U_nuxpmu(nx,ny,nz,nt))
    allocate( U_muxpnu(nx,ny,nz,nt))
    allocate(UmuUnu(nx,ny,nz,nt))
    allocate( UmudagUnudag(nx,ny,nz,nt))
    allocate( UnuUmudag(nx,ny,nz,nt))
    allocate( UnudagUmu(nx,ny,nz,nt))

    U_nuxpmu = CShiftGaugePlane(Unu_xd(:,:,:,:,nu),mu,musize)
    U_muxpnu = CShiftGaugePlane(Umu_xd(:,:,:,:,mu),nu,nusize)

    UmuUnu = zero_matrix
    UnuUmudag = zero_matrix
    UnudagUmu = zero_matrix
    UmudagUnudag = zero_matrix

    !Calculate link products in a way to maximise reuse
    call UU(UmuUnu,Umu_xd(:,:,:,:,mu),U_nuxpmu)

    call UUdag(UnuUmudag,U_nuxpmu,U_muxpnu)

    call UdagU(UnudagUmu,Unu_xd(:,:,:,:,nu),Umu_xd(:,:,:,:,mu))

    call UdagUdag(UmudagUnudag,U_muxpnu,Unu_xd(:,:,:,:,nu))

    !Assign plaquette values, shifting where necessary

    C_munu = zero_matrix
    U_munu = zero_matrix

    !U+mu+nu(x)
    call UU(U_munu,UmuUnu,UmudagUnudag)
    C_munu = C_munu + U_munu

    !U+nu-mu(x+a_mu)
    call UU(U_munu,UnuUmudag,UnudagUmu)
    U_munu =  CShiftGaugePlane(U_munu, mu,-musize)
    C_munu = C_munu + U_munu

    !U-nu+mu(x+a_nu)
    call UU(U_munu,UnudagUmu,UnuUmudag)
    U_munu =  CShiftGaugePlane(U_munu, nu,-nusize)
    C_munu = C_munu + U_munu

    !U-mu-nu(x+a_mu+a_nu)
    call UU(U_munu,UmudagUnudag,UmuUnu)
    U_munu = CShiftGaugePlane(U_munu, mu,-musize)
    U_munu = CShiftGaugePlane(U_munu, nu,-nusize)
    C_munu = C_munu + U_munu

#undef U_munu

    deallocate(U_nuxpmu)
    deallocate( U_muxpnu)
    deallocate(UmuUnu)
    deallocate( UmudagUnudag)
    deallocate( UnuUmudag)
    deallocate( UnudagUmu)

  end subroutine CalculateCloverTerm

#define M_x M(ix,iy,iz,it)
#define L_x L(ix,iy,iz,it)
#define R_x R(ix,iy,iz,it)

  pure subroutine UU(M,L,R)
    ! begin args: M, L, R

    type(colour_matrix), dimension(:,:,:,:), intent(in) :: L
    type(colour_matrix), dimension(:,:,:,:), intent(in) :: R
    type(colour_matrix), dimension(:,:,:,:), intent(out) :: M
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       M_x%Cl(1,1) = L_x%Cl(1,1)*R_x%Cl(1,1) + L_x%Cl(1,2)*R_x%Cl(2,1) + L_x%Cl(1,3)*R_x%Cl(3,1)
       M_x%Cl(2,1) = L_x%Cl(2,1)*R_x%Cl(1,1) + L_x%Cl(2,2)*R_x%Cl(2,1) + L_x%Cl(2,3)*R_x%Cl(3,1)
       M_x%Cl(3,1) = L_x%Cl(3,1)*R_x%Cl(1,1) + L_x%Cl(3,2)*R_x%Cl(2,1) + L_x%Cl(3,3)*R_x%Cl(3,1)

       M_x%Cl(1,2) = L_x%Cl(1,1)*R_x%Cl(1,2) + L_x%Cl(1,2)*R_x%Cl(2,2) + L_x%Cl(1,3)*R_x%Cl(3,2)
       M_x%Cl(2,2) = L_x%Cl(2,1)*R_x%Cl(1,2) + L_x%Cl(2,2)*R_x%Cl(2,2) + L_x%Cl(2,3)*R_x%Cl(3,2)
       M_x%Cl(3,2) = L_x%Cl(3,1)*R_x%Cl(1,2) + L_x%Cl(3,2)*R_x%Cl(2,2) + L_x%Cl(3,3)*R_x%Cl(3,2)

       M_x%Cl(1,3) = L_x%Cl(1,1)*R_x%Cl(1,3) + L_x%Cl(1,2)*R_x%Cl(2,3) + L_x%Cl(1,3)*R_x%Cl(3,3)
       M_x%Cl(2,3) = L_x%Cl(2,1)*R_x%Cl(1,3) + L_x%Cl(2,2)*R_x%Cl(2,3) + L_x%Cl(2,3)*R_x%Cl(3,3)
       M_x%Cl(3,3) = L_x%Cl(3,1)*R_x%Cl(1,3) + L_x%Cl(3,2)*R_x%Cl(2,3) + L_x%Cl(3,3)*R_x%Cl(3,3)
    end do; end do; end do; end do


  end subroutine UU

  pure subroutine UUdag(M,L,R)
    ! begin args: M, L, R

    type(colour_matrix), dimension(:,:,:,:), intent(in) :: L
    type(colour_matrix), dimension(:,:,:,:), intent(in) :: R
    type(colour_matrix), dimension(:,:,:,:), intent(out) :: M
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       M_x%Cl(1,1) = L_x%Cl(1,1)*conjg(R_x%Cl(1,1)) + L_x%Cl(1,2)*conjg(R_x%Cl(1,2)) + L_x%Cl(1,3)*conjg(R_x%Cl(1,3))
       M_x%Cl(2,1) = L_x%Cl(2,1)*conjg(R_x%Cl(1,1)) + L_x%Cl(2,2)*conjg(R_x%Cl(1,2)) + L_x%Cl(2,3)*conjg(R_x%Cl(1,3))
       M_x%Cl(3,1) = L_x%Cl(3,1)*conjg(R_x%Cl(1,1)) + L_x%Cl(3,2)*conjg(R_x%Cl(1,2)) + L_x%Cl(3,3)*conjg(R_x%Cl(1,3))

       M_x%Cl(1,2) = L_x%Cl(1,1)*conjg(R_x%Cl(2,1)) + L_x%Cl(1,2)*conjg(R_x%Cl(2,2)) + L_x%Cl(1,3)*conjg(R_x%Cl(2,3))
       M_x%Cl(2,2) = L_x%Cl(2,1)*conjg(R_x%Cl(2,1)) + L_x%Cl(2,2)*conjg(R_x%Cl(2,2)) + L_x%Cl(2,3)*conjg(R_x%Cl(2,3))
       M_x%Cl(3,2) = L_x%Cl(3,1)*conjg(R_x%Cl(2,1)) + L_x%Cl(3,2)*conjg(R_x%Cl(2,2)) + L_x%Cl(3,3)*conjg(R_x%Cl(2,3))

       M_x%Cl(1,3) = L_x%Cl(1,1)*conjg(R_x%Cl(3,1)) + L_x%Cl(1,2)*conjg(R_x%Cl(3,2)) + L_x%Cl(1,3)*conjg(R_x%Cl(3,3))
       M_x%Cl(2,3) = L_x%Cl(2,1)*conjg(R_x%Cl(3,1)) + L_x%Cl(2,2)*conjg(R_x%Cl(3,2)) + L_x%Cl(2,3)*conjg(R_x%Cl(3,3))
       M_x%Cl(3,3) = L_x%Cl(3,1)*conjg(R_x%Cl(3,1)) + L_x%Cl(3,2)*conjg(R_x%Cl(3,2)) + L_x%Cl(3,3)*conjg(R_x%Cl(3,3))
    end do; end do; end do; end do


  end subroutine UUdag

  pure subroutine UdagU(M,L,R)
    ! begin args: M, L, R

    type(colour_matrix), dimension(:,:,:,:), intent(in) :: L
    type(colour_matrix), dimension(:,:,:,:), intent(in) :: R
    type(colour_matrix), dimension(:,:,:,:), intent(out) :: M
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       M_x%Cl(1,1) = conjg(L_x%Cl(1,1))*R_x%Cl(1,1) + conjg(L_x%Cl(2,1))*R_x%Cl(2,1) + conjg(L_x%Cl(3,1))*R_x%Cl(3,1)
       M_x%Cl(2,1) = conjg(L_x%Cl(1,2))*R_x%Cl(1,1) + conjg(L_x%Cl(2,2))*R_x%Cl(2,1) + conjg(L_x%Cl(3,2))*R_x%Cl(3,1)
       M_x%Cl(3,1) = conjg(L_x%Cl(1,3))*R_x%Cl(1,1) + conjg(L_x%Cl(2,3))*R_x%Cl(2,1) + conjg(L_x%Cl(3,3))*R_x%Cl(3,1)

       M_x%Cl(1,2) = conjg(L_x%Cl(1,1))*R_x%Cl(1,2) + conjg(L_x%Cl(2,1))*R_x%Cl(2,2) + conjg(L_x%Cl(3,1))*R_x%Cl(3,2)
       M_x%Cl(2,2) = conjg(L_x%Cl(1,2))*R_x%Cl(1,2) + conjg(L_x%Cl(2,2))*R_x%Cl(2,2) + conjg(L_x%Cl(3,2))*R_x%Cl(3,2)
       M_x%Cl(3,2) = conjg(L_x%Cl(1,3))*R_x%Cl(1,2) + conjg(L_x%Cl(2,3))*R_x%Cl(2,2) + conjg(L_x%Cl(3,3))*R_x%Cl(3,2)

       M_x%Cl(1,3) = conjg(L_x%Cl(1,1))*R_x%Cl(1,3) + conjg(L_x%Cl(2,1))*R_x%Cl(2,3) + conjg(L_x%Cl(3,1))*R_x%Cl(3,3)
       M_x%Cl(2,3) = conjg(L_x%Cl(1,2))*R_x%Cl(1,3) + conjg(L_x%Cl(2,2))*R_x%Cl(2,3) + conjg(L_x%Cl(3,2))*R_x%Cl(3,3)
       M_x%Cl(3,3) = conjg(L_x%Cl(1,3))*R_x%Cl(1,3) + conjg(L_x%Cl(2,3))*R_x%Cl(2,3) + conjg(L_x%Cl(3,3))*R_x%Cl(3,3)
    end do; end do; end do; end do


  end subroutine UdagU

  pure subroutine UdagUdag(M,L,R)
    ! begin args: M, L, R

    type(colour_matrix), dimension(:,:,:,:), intent(in) :: L
    type(colour_matrix), dimension(:,:,:,:), intent(in) :: R
    type(colour_matrix), dimension(:,:,:,:), intent(out) :: M
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       M_x%Cl(1,1) = conjg(L_x%Cl(1,1)*R_x%Cl(1,1) + L_x%Cl(2,1)*R_x%Cl(1,2) + L_x%Cl(3,1)*R_x%Cl(1,3))
       M_x%Cl(2,1) = conjg(L_x%Cl(1,2)*R_x%Cl(1,1) + L_x%Cl(2,2)*R_x%Cl(1,2) + L_x%Cl(3,2)*R_x%Cl(1,3))
       M_x%Cl(3,1) = conjg(L_x%Cl(1,3)*R_x%Cl(1,1) + L_x%Cl(2,3)*R_x%Cl(1,2) + L_x%Cl(3,3)*R_x%Cl(1,3))

       M_x%Cl(1,2) = conjg(L_x%Cl(1,1)*R_x%Cl(2,1) + L_x%Cl(2,1)*R_x%Cl(2,2) + L_x%Cl(3,1)*R_x%Cl(2,3))
       M_x%Cl(2,2) = conjg(L_x%Cl(1,2)*R_x%Cl(2,1) + L_x%Cl(2,2)*R_x%Cl(2,2) + L_x%Cl(3,2)*R_x%Cl(2,3))
       M_x%Cl(3,2) = conjg(L_x%Cl(1,3)*R_x%Cl(2,1) + L_x%Cl(2,3)*R_x%Cl(2,2) + L_x%Cl(3,3)*R_x%Cl(2,3))

       M_x%Cl(1,3) = conjg(L_x%Cl(1,1)*R_x%Cl(3,1) + L_x%Cl(2,1)*R_x%Cl(3,2) + L_x%Cl(3,1)*R_x%Cl(3,3))
       M_x%Cl(2,3) = conjg(L_x%Cl(1,2)*R_x%Cl(3,1) + L_x%Cl(2,2)*R_x%Cl(3,2) + L_x%Cl(3,2)*R_x%Cl(3,3))
       M_x%Cl(3,3) = conjg(L_x%Cl(1,3)*R_x%Cl(3,1) + L_x%Cl(2,3)*R_x%Cl(3,2) + L_x%Cl(3,3)*R_x%Cl(3,3))
    end do; end do; end do; end do


  end subroutine UdagUdag

  elemental function matrix_plus_matrix(left,right) result (plus)
    ! begin args: left, right

    type(colour_matrix), intent(in) :: left, right
    ! begin local_vars
    type(colour_matrix)             :: plus

    ! begin execution

    plus%Cl = left%Cl + right%Cl


  end function matrix_plus_matrix

end module ImprovedFmunu

