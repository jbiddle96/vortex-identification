!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/fatlinks.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: fatlinks.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.2  2004/06/21 16:57:25  wkamleh
!! Update History  : Bugs fixed. Traces removed
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module FatLinks

  use MPIInterface
  use GaugeField
  use GL3Diag
  use ZolotarevApprox
  use ReduceOps
  use MatrixAlgebra
  use GaugeFieldMPIComms
  use LatticeSize
  use Kinds
  use ColourTypes
  implicit none
  private

  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: UFL_xd !Fat links
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: Usm_xd !Fat links

  real(DP), public :: alpha_smear = 0.7d0 !smearing fraction
  integer, public :: ape_sweeps = 4 !smearing sweeps
  integer, public :: smear_sweeps = 4 !smearing sweeps
  real(dp), public :: u0fl_bar = 1.0d0, uzerofl = 1.0d0

  integer, public :: pole_min = 0, pole_max = 0
  real(dp), public :: traj_minev = 1.0d0, traj_maxev = 0.0d0

  public :: APESmearLinks
  public :: APESmear
  public :: GetSmearedLinks
  public :: APESmearing
  public :: SU3Project
  public :: GetAPEBlockedLinks
  public :: dU_prbydU_ape
  public :: dSbydU_nStardSbydV
  public :: dSbydVstardVbydU

contains

  subroutine APESmearLinks(U_xd, UFL_xd, alpha, sweeps, that, smear_x, smear_t)
    ! begin args: U_xd, UFL_xd, alpha, sweeps, that, smear_x, smear_t

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd !Thin links
    type(colour_matrix), dimension(:,:,:,:,:) :: UFL_xd !Fat links
    real(dp) :: alpha
    integer :: sweeps
    integer, optional :: that
    logical, optional :: smear_x, smear_t

    ! begin local_vars
    integer :: isweeps

    ! begin execution

    UFL_xd(1:nxs,1:nys,1:nzs,1:nts,:) = U_xd(1:nxs,1:nys,1:nzs,1:nts,:)

    do isweeps =1,sweeps
       call APESmear(UFL_xd,alpha,that,smear_x,smear_t)
!       if ( isweeps == sweeps) call FixGaugeField(UFL_xd)
       call ShadowGaugeField(UFL_xd,1)
    end do


  end subroutine APESmearLinks

  subroutine APESmear(U_xd,alpha,that,smear_x,smear_t)
    ! begin args: U_xd, alpha, that, smear_x, smear_t

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd !Thin links
    real(dp) :: alpha
    integer, optional :: that
    logical, optional :: smear_x, smear_t

    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: V_xd !Smeared links
    integer :: mu,id
    integer :: xhat(nd-1)

    ! begin execution
    allocate(V_xd(nxp,nyp,nzp,ntp,nd))

    if (.not. present(that)) then
       !Standard APE Smearing
       do mu=1,nd
          call APESmearing(V_xd,U_xd,alpha,mu)
       end do

    else
       !Spatial APE smearing
       do id=1,nd-1
          xhat(id) = modc(that+id,nd)
       end do

       do id=1,nd-1
          mu = xhat(id)
          if (smear_x) then
             call APESmearing(V_xd,U_xd,alpha,mu,xhat)
          else
             V_xd(1:nx,1:ny,1:nz,1:nt,mu) = U_xd(1:nx,1:ny,1:nz,1:nt,mu)
          end if
       end do

       mu = that
       if (smear_t) then
          call APESmearing(V_xd,U_xd,alpha,mu)
       else
          V_xd(1:nx,1:ny,1:nz,1:nt,mu) = U_xd(1:nx,1:ny,1:nz,1:nt,mu)
        end if
    end if

    U_xd(1:nx,1:ny,1:nz,1:nt,:) = V_xd(1:nx,1:ny,1:nz,1:nt,:)

    call SU3Project(U_xd)

    deallocate(V_xd)

  end subroutine APESmear

  subroutine GetSmearedLinks(V_xd,U_xd,alpha)
    ! begin args: V_xd, U_xd, alpha

    type(colour_matrix), dimension(:,:,:,:,:) :: V_xd !Smeared links
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd !Thin links
    real(dp) :: alpha
    ! begin local_vars
    integer :: mu

    ! begin execution

    do mu=1,nd
       call APESmearing(V_xd,U_xd,alpha,mu)
    end do


  end subroutine GetSmearedLinks

  subroutine APESmearing(V_xd,U_xd,alpha,mu,xhat)
    ! begin args: V_xd, U_xd, alpha, mu, xhat

    type(colour_matrix), dimension(:,:,:,:,:) :: V_xd !Smeared links
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd !Thin links
    real(dp) :: alpha
    integer :: mu
    integer, optional :: xhat(nd-1)

    ! begin local_vars
    integer :: ix,iy,iz,it,id,nu,nd_sm,yhat(nd-1)
    real(dp) :: alpha_d

    ! begin execution

    if ( present(xhat) ) then
       !Spatial Smearing
       nd_sm = nd-2
       yhat = xhat
       do while ( yhat(nd-1) /= mu )
          yhat = cshift(yhat,shift=1)
       end do
    else
       !Standard or Temporal Smearing
       nd_sm = nd-1
       do id=1,nd_sm
          yhat(id) = modc(mu+id,nd)
       end do
    end if

    alpha_d = alpha / (2*nd_sm)

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       V_xd(ix,iy,iz,it,mu)%Cl = 0.0d0
    end do; end do; end do; end do

    do id=1,nd_sm
       nu = yhat(id)
       call GetAPEBlockedLinks(V_xd,U_xd,mu,nu)
    end do

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       V_xd(ix,iy,iz,it,mu)%Cl = (1.0d0 - alpha)*U_xd(ix,iy,iz,it,mu)%Cl + alpha_d*V_xd(ix,iy,iz,it,mu)%Cl
    end do; end do; end do; end do


  end subroutine APESmearing

  subroutine SU3Project(U_xd)
    ! begin args: U_xd

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    ! begin local_vars
    type(colour_matrix) :: H_xd, V_xd, W_xd !Hermitian matrix

    type(real_vector) :: lambda_xd !eigenvalues

    type(colour_vector) :: WxW !cross product
    complex(dc) :: detW
    integer :: ix,iy,iz,it,mu

    ! begin execution

    do mu=1,nd

       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          call MultiplyMatDagMat(H_xd,U_xd(ix,iy,iz,it,mu),U_xd(ix,iy,iz,it,mu))

          call DiagonaliseMat(H_xd,lambda_xd,V_xd)

          lambda_xd%Cl = 1.0d0/sqrt(lambda_xd%Cl)

          call MultiplyMatRealDiagMat(W_xd,V_xd,lambda_xd)

          !Undiagonalise H_xd back to the U_xd basis
          call MultiplyMatMatdag(H_xd,W_xd,V_xd)
          call MultiplyMatMat(W_xd,U_xd(ix,iy,iz,it,mu),H_xd)

          WxW%Cl(1) = W_xd%Cl(2,2)*W_xd%Cl(3,3) - W_xd%Cl(2,3)*W_xd%Cl(3,2)
          WxW%Cl(2) = W_xd%Cl(2,3)*W_xd%Cl(3,1) - W_xd%Cl(2,1)*W_xd%Cl(3,3)
          WxW%Cl(3) = W_xd%Cl(2,1)*W_xd%Cl(3,2) - W_xd%Cl(2,2)*W_xd%Cl(3,1)

          detW = W_xd%Cl(1,1)*WxW%Cl(1) + W_xd%Cl(1,2)*WxW%Cl(2) + W_xd%Cl(1,3)*WxW%Cl(3)

          detW = 1.0d0/(detW**(1.0d0/3.0d0))

          U_xd(ix,iy,iz,it,mu)%Cl(:,1) = detW*W_xd%Cl(:,1)
          U_xd(ix,iy,iz,it,mu)%Cl(:,2) = detW*W_xd%Cl(:,2)
          U_xd(ix,iy,iz,it,mu)%Cl(:,3) = detW*W_xd%Cl(:,3)

       end do; end do; end do; end do

    end do


  end subroutine SU3Project


  subroutine GetAPEBlockedLinks(V_xd,U_xd,mu,nu)
    ! begin args: V_xd, U_xd, mu, nu

    type(colour_matrix), dimension(:,:,:,:,:) :: V_xd
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: mu, nu

    ! begin local_vars
    type(colour_matrix) :: UnuUmu, UnudagUmu, V_nup, V_num
    integer, dimension(nd) :: dmu, dnu
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt
    integer :: lx,ly,lz,lt
    integer :: mx,my,mz,mt

#define U_nux U_xd(ix,iy,iz,it,nu)
#define V_munux V_xd(ix,iy,iz,it,mu)
#define U_nuxpmu    U_xd(jx,jy,jz,jt,nu)
#define U_nuxmnu    U_xd(lx,ly,lz,lt,nu)
#define U_muxpnu    U_xd(kx,ky,kz,kt,mu)
#define U_muxmnu    U_xd(lx,ly,lz,lt,mu)
#define U_nuxmnupmu U_xd(mx,my,mz,mt,nu)


    ! begin execution

    dmu = 0
    dnu = 0

    dmu(mu) = 1
    dnu(nu) = 1

    do it=1,nt
       jt = mapt(it + dmu(4))
       kt = mapt(it + dnu(4))
       lt = mapt(it - dnu(4))
       mt = mapt(it - dnu(4) + dmu(4))
       do iz=1,nz
          jz = mapz(iz + dmu(3))
          kz = mapz(iz + dnu(3))
          lz = mapz(iz - dnu(3))
          mz = mapz(iz - dnu(3) + dmu(3))
          do iy=1,ny
             jy = mapy(iy + dmu(2))
             ky = mapy(iy + dnu(2))
             ly = mapy(iy - dnu(2))
             my = mapy(iy - dnu(2) + dmu(2))
             do ix=1,nx
                jx = mapx(ix + dmu(1))
                kx = mapx(ix + dnu(1))
                lx = mapx(ix - dnu(1))
                mx = mapx(ix - dnu(1) + dmu(1))

                call MultiplyMatMat(UnuUmu,U_nux,U_muxpnu)
                call MultiplyMatMatdag(V_nup,UnuUmu,U_nuxpmu)

                call MultiplyMatdagMat(UnudagUmu,U_nuxmnu,U_muxmnu)
                call MultiplyMatMat(V_num,UnudagUmu,U_nuxmnupmu)

                V_munux%Cl = V_munux%Cl + V_nup%Cl + V_num%Cl

             end do
          end do
       enddo
    end do

#undef U_nux
#undef V_munux
#undef U_nuxpmu
#undef U_nuxmnu
#undef U_muxpnu
#undef U_muxmnu
#undef U_nuxmnupmu


  end subroutine GetAPEBlockedLinks

  subroutine dU_prbydU_ape(dSbydU,U_xd,alpha)
    ! begin args: dSbydU, U_xd, alpha

    !Accepts dSbydU' and returns dSbydU in the same array.

    type(colour_matrix), dimension(:,:,:,:,:) :: dSbydU, U_xd
    real(dp) :: alpha
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: V_xd

    ! begin execution
    allocate(V_xd(nxs,nys,nzs,nts,nd))

    call GetSmearedLinks(V_xd,U_xd,alpha)

    call dSbydU_nStardSbydV(dSbydU,V_xd)

    V_xd(1:nx,1:ny,1:nz,1:nt,:) = dSbydU(1:nx,1:ny,1:nz,1:nt,:)
    call ShadowGaugeField(V_xd,1)

    call dSbydVstardVbydU(dSbydU,U_xd,V_xd,alpha)

    deallocate(V_xd)

  end subroutine dU_prbydU_ape

  subroutine dSbydU_nStardSbydV(dSbydU,V_xd)
    ! begin args: dSbydU, V_xd

    !Accepts dSbydU_n and applies multiple chain rules to return
    !dSbydV reusing the same array.

    type(colour_matrix), dimension(:,:,:,:,:) :: dSbydU,V_xd

    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: D_xd
    type(real_vector), dimension(:,:,:,:,:), allocatable :: lambda_xd

    type(colour_matrix) :: W_xd, H_xd, G_xd
    complex(dc) :: alpha
    complex(dc) :: detW, ddetW
    type(colour_matrix) :: ddetWbydW
    type(colour_matrix) :: L_xd, R_xd

    type(real_vector) :: mu_xd, tau_xd
    type(colour_matrix) :: A_xd, B_xd

    type(real_vector), allocatable :: kappa_xd(:)
    type(colour_matrix), allocatable :: M_xd(:)

    !Zolotarev approximation to 1/sqrt(x)
    real(dp) :: lambda_min, lambda_max, local_min, local_max
    integer :: npole
    real(dp) :: c_2n, d_n, delta
    real(dp), dimension(:), allocatable :: a_l,b_l,c_l
    integer :: ix,iy,iz,it,mu,ipole

    real(dp) :: t0,t1

#define dSbydU_mux dSbydU(ix,iy,iz,it,mu)
#define dSbydW_mux dSbydU(ix,iy,iz,it,mu)
#define dSbydV_mux dSbydU(ix,iy,iz,it,mu)

    ! begin execution
    allocate(D_xd(nxp,nyp,nzp,ntp,nd))
    allocate(lambda_xd(nxp,nyp,nzp,ntp,nd))

    local_min = 1.0d0
    local_max = 0.0d0

    t0 = mpi_wtime()

    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          call MultiplyMatDagMat(H_xd,V_xd(ix,iy,iz,it,mu),V_xd(ix,iy,iz,it,mu))
          call DiagonaliseMat(H_xd,lambda_xd(ix,iy,iz,it,mu),D_xd(ix,iy,iz,it,mu))

          local_min = min(local_min,lambda_xd(ix,iy,iz,it,mu)%Cl(1))
          local_max = max(local_max,lambda_xd(ix,iy,iz,it,mu)%Cl(3))

       end do; end do; end do; end do
    end do

    call AllMin(local_min,lambda_min)
    call AllMax(local_max,lambda_max)

    npole = GetZolotarevEstOrder(1.0d-12,lambda_min,lambda_max)

    allocate(a_l(npole))
    allocate(b_l(npole))
    allocate(c_l(npole))
    allocate(kappa_xd(0:npole))
    allocate(M_xd(0:npole))

    call GetZolotarevCoeffs(npole,a_l,b_l,c_l,c_2n,d_n,lambda_min,lambda_max,delta)

    if ( traj_minev == 0.0d0 ) traj_minev = lambda_min
    traj_minev = min(traj_minev,lambda_min)
    traj_maxev = max(traj_maxev,lambda_max)
    if ( pole_min == 0 ) pole_min = npole
    pole_min = min(pole_min,npole)
    pole_max = max(pole_max,npole)

    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          G_xd = D_xd(ix,iy,iz,it,mu)

          tau_xd%Cl = 1.0d0/sqrt(lambda_xd(ix,iy,iz,it,mu)%Cl)

          !Undiagonalise H_xd back to the V_xd basis
          call MultiplyMatRealDiagMat(W_xd,G_xd,tau_xd)
          call MultiplyMatMatdag(H_xd,W_xd,G_xd)

          !Set W_xd = U H \in U(3)
          call MultiplyMatMat(W_xd,V_xd(ix,iy,iz,it,mu),H_xd)

          ! U_n = detW^(-1/3)*W
          ! First Chain rule: dS/dW = dS/dU_n * dU_n/dW + dS/dU_n^dag * dU_n^dag/dW
          ! U_n^dag = conjg(detW)*W^dag => dU_n^dag/dW = 0

          !Note ddetWbydW[j,i] = (d detW)/(d W[i,j])

          ddetWbydW%Cl(1,1) = W_xd%Cl(2,2)*W_xd%Cl(3,3) - W_xd%Cl(2,3)*W_xd%Cl(3,2)
          ddetWbydW%Cl(1,2) = W_xd%Cl(1,3)*W_xd%Cl(3,2) - W_xd%Cl(1,2)*W_xd%Cl(3,3)
          ddetWbydW%Cl(1,3) = W_xd%Cl(1,2)*W_xd%Cl(2,3) - W_xd%Cl(1,3)*W_xd%Cl(2,2)

          ddetWbydW%Cl(2,1) = W_xd%Cl(2,3)*W_xd%Cl(3,1) - W_xd%Cl(2,1)*W_xd%Cl(3,3)
          ddetWbydW%Cl(2,2) = W_xd%Cl(1,1)*W_xd%Cl(3,3) - W_xd%Cl(1,3)*W_xd%Cl(3,1)
          ddetWbydW%Cl(2,3) = W_xd%Cl(1,3)*W_xd%Cl(2,1) - W_xd%Cl(1,1)*W_xd%Cl(2,3)

          ddetWbydW%Cl(3,1) = W_xd%Cl(2,1)*W_xd%Cl(3,2) - W_xd%Cl(2,2)*W_xd%Cl(3,1)
          ddetWbydW%Cl(3,2) = W_xd%Cl(1,2)*W_xd%Cl(3,1) - W_xd%Cl(1,1)*W_xd%Cl(3,2)
          ddetWbydW%Cl(3,3) = W_xd%Cl(1,1)*W_xd%Cl(2,2) - W_xd%Cl(1,2)*W_xd%Cl(2,1)

          detW = W_xd%Cl(1,1)*ddetWbydW%Cl(1,1) + W_xd%Cl(1,2)*ddetWbydW%Cl(2,1) + W_xd%Cl(1,3)*ddetWbydW%Cl(3,1)

          !Get detW^(-1/3)
          detW = 1.0d0/(detW**(1.0d0/3.0d0))

          !Get -(1/3)*detW^(-4/3)
          ddetW = -(1.0d0/3.0d0)*detW**4

          L_xd%Cl = detW*dSbydU_mux%Cl

          !StarInnerProduct(alpha,dSbydU_mux,W_xd)
          call TraceMultMatMat(alpha,dSbydU_mux,W_xd)

          R_xd%Cl = (ddetW*alpha)*ddetWbydW%Cl

          dSbydW_mux%Cl = L_xd%Cl + R_xd%Cl

          ! W = V H, W^dag = H V^dag, H= h^(-1/2), h = [V^dag V]
          ! Second Chain rule: dS/dV = dS/dW * dW/dV + dS/dW^dag * dW^dag/dV
          !                          = dS/dW * ( I x H + V dH/dV) + dS/dW^dag * ( dH/dV V^dag )
          ! Intermediate Chain rule: dH/dV = dH/dh * dh/dV

          call MultiplyMatMat(L_xd,H_xd,dSbydW_mux)

          call MultiplyMatMat(A_xd,dSbydW_mux,V_xd(ix,iy,iz,it,mu))
          call MatDag(B_xd,A_xd)

          !Set W_xd = dS/dH
          W_xd%Cl = A_xd%Cl + B_xd%Cl

          kappa_xd(0)%Cl = d_n*(lambda_xd(ix,iy,iz,it,mu)%Cl + c_2n)
          do ipole=1,npole
             kappa_xd(ipole)%Cl = 1.0d0/(lambda_xd(ix,iy,iz,it,mu)%Cl + c_l(ipole))
          end do

          !Undiagonalise H_xd back to the V_xd basis
          do ipole=0,npole
             call MultiplyMatRealDiagMat(A_xd,G_xd,kappa_xd(ipole))
             call MultiplyMatMatdag(M_xd(ipole),A_xd,G_xd)
          end do

          B_xd%Cl = 0.0d0
          do ipole=1,npole
             B_xd%Cl = B_xd%Cl + b_l(ipole)*M_xd(ipole)%Cl
          end do

          A_xd%Cl = 0.0d0

          A_xd%Cl(1,1) = d_n
          A_xd%Cl(2,2) = d_n
          A_xd%Cl(3,3) = d_n

          ! T * (L x R) = R T L
          !StarOuterProduct(R_xd,W_xd,A_xd,B_xd)
          call MultiplyMatMat(G_xd,B_xd,W_xd)
          call MultiplyMatMat(R_xd,G_xd,A_xd)

          call MultiplyMatMat(A_xd,W_xd,M_xd(0))
          do ipole=1,npole
             ! T * (L x R) = R T L
             !StarOuterProduct(B_xd,A_xd,M_xd(ipole),M_xd(ipole))
             call MultiplyMatMat(G_xd,M_xd(ipole),A_xd)
             call MultiplyMatMat(B_xd,G_xd,M_xd(ipole))

             R_xd%Cl = R_xd%Cl - b_l(ipole)*B_xd%Cl
          end do

          call MultiplyMatMatDag(dSbydV_mux,R_xd,V_xd(ix,iy,iz,it,mu))

          dSbydV_mux%Cl = L_xd%Cl + dSbydV_mux%Cl

       end do; end do; end do; end do
    end do

    deallocate(b_l)
    deallocate(c_l)
    deallocate(kappa_xd)
    deallocate(M_xd)

#undef dSbydU_mux
#undef dSbydV_mux
#undef dSbydW_mux

    deallocate(D_xd)
    deallocate(lambda_xd)

  end subroutine dSbydU_nStardSbydV

  subroutine dSbydVstardVbydU(dSbydU,U_xd,dSbydV,alpha)
    ! begin args: dSbydU, U_xd, dSbydV, alpha

    !Accepts dSbydV and applies dS/dU[mu,x] = dS/dV[nu,y] * dV[nu,y]/dU[mu,x] + dS/dV^dag[nu,y] * dV^dag[nu,y]/dU[mu,x],
    !returns the result in dSbyDu.

    type(colour_matrix), dimension(:,:,:,:,:) :: dSbydU, U_xd, dSbydV
    real(dp) :: alpha

    ! begin local_vars
    type(colour_matrix) :: V_x, W_xpmu, T_x
    integer :: mu,nu, inu

    integer, dimension(nd) :: dmu, dnu
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt
    integer :: lx,ly,lz,lt
    integer :: mx,my,mz,mt

#define U_nux        U_xd(ix,iy,iz,it,nu)
#define U_nuxpmu     U_xd(jx,jy,jz,jt,nu)
#define U_muxpnu     U_xd(kx,ky,kz,kt,mu)
#define U_nuxmnu     U_xd(lx,ly,lz,lt,nu)
#define U_muxmnu     U_xd(lx,ly,lz,lt,mu)
#define U_nuxpmumnu  U_xd(mx,my,mz,mt,nu)

#define dSbydU_mux        dSbydU(ix,iy,iz,it,mu)
#define dSbydV_mux        dSbydV(ix,iy,iz,it,mu)
#define dSbydV_nux        dSbydV(ix,iy,iz,it,nu)
#define dSbydV_nuxpmu     dSbydV(jx,jy,jz,jt,nu)
#define dSbydV_muxpnu     dSbydV(kx,ky,kz,kt,mu)
#define dSbydV_nuxpnu     dSbydV(kx,ky,kz,kt,nu)
#define dSbydV_muxmnu     dSbydV(lx,ly,lz,lt,mu)
#define dSbydV_nuxmnu     dSbydV(lx,ly,lz,lt,nu)
#define dSbydV_nuxpmumnu  dSbydV(mx,my,mz,mt,nu)

    ! begin execution

    dSbydU = zero_matrix

    do mu=1,nd
       nu = mu
       do inu=1,nd-1
          nu = modulo(nu,nd)+1
          dmu = 0
          dnu = 0

          dmu(mu) = 1
          dnu(nu) = 1

          do it=1,nt
             jt = mapt(it + dmu(4))
             kt = mapt(it + dnu(4))
             lt = mapt(it - dnu(4))
             mt = mapt(it - dnu(4) + dmu(4))
             do iz=1,nz
                jz = mapz(iz + dmu(3))
                kz = mapz(iz + dnu(3))
                lz = mapz(iz - dnu(3))
                mz = mapz(iz - dnu(3) + dmu(3))
                do iy=1,ny
                   jy = mapy(iy + dmu(2))
                   ky = mapy(iy + dnu(2))
                   ly = mapy(iy - dnu(2))
                   my = mapy(iy - dnu(2) + dmu(2))
                   do ix=1,nx
                      jx = mapx(ix + dmu(1))
                      kx = mapx(ix + dnu(1))
                      lx = mapx(ix - dnu(1))
                      mx = mapx(ix - dnu(1) + dmu(1))

                      V_x = U_nuxmnu
                      call MatDag(W_xpmu,U_nuxpmumnu)

                      ! T * (L x R) = R T L
                      call MultiplyMatMat(T_x,W_xpmu,dSbydV_muxmnu)
                      call MatPlusMatTimesMat(dSbydU_mux,T_x,V_x)

                      call Matdag(V_x,U_nux)
                      W_xpmu = U_nuxpmu

                      ! T * (L x R) = R T L
                      call MultiplyMatMat(T_x,W_xpmu,dSbydV_muxpnu)
                      call MatPlusMatTimesMat(dSbydU_mux,T_x,V_x)

                      call MultiplyMatMatdag(W_xpmu,U_nuxpmu,U_muxpnu)

                      ! T * (I x R) = R A
                      call MatPlusMatTimesMat(dSbydU_mux,W_xpmu,dSbydV_nux)

                      call MultiplyMatdagMat(V_x,U_muxmnu,U_nuxmnu)

                      ! T * (L x I) = T L
                      call MatPlusMatTimesMat(dSbydU_mux,dSbydV_nuxpmumnu,V_x)

                      !Vdag

                      call MultiplyMatdagMatdag(W_xpmu,U_nuxpmumnu,U_muxmnu)

                      ! T * (I x R) = R T
                      call MatPlusMatTimesMatDag(dSbydU_mux,W_xpmu,dSbydV_nuxmnu)

                      call MultiplyMatdagMatdag(V_x,U_muxpnu,U_nux)

                      ! T * (L x I) = T L
                      call MatPlusMatDagTimesMat(dSbydU_mux,dSbydV_nuxpmu,V_x)

                   end do
                end do
             enddo
          end do

       end do
    end do

    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dSbydU_mux%Cl = (1.0d0 - alpha)*dSbydV_mux%Cl + (alpha/6.0d0)*dSbydU_mux%Cl
       end do; end do; end do; end do
    end do

#undef dSbydU_mux

#undef U_nux
#undef U_nuxpmu
#undef U_muxpnu
#undef U_nuxpmumnu
#undef U_muxmnu
#undef U_nuxmnu


  end subroutine dSbydVstardVbydU

end module FatLinks

