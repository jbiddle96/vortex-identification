
module CollectiveOps

  use Kinds
  use MPIInterface
  private

  interface Broadcast
     module procedure BroadcastString
     module procedure BroadcastLogical
     module procedure BroadcastInteger
     module procedure BroadcastInteger_1
     module procedure BroadcastReal
     module procedure BroadcastReal_1
     module procedure BroadcastComplex
     module procedure BroadcastReal_sp
     module procedure BroadcastComplex_sc
  end interface

  public :: Broadcast

contains

  subroutine BroadcastLogical(l,root_rank)

    logical :: l
    integer, optional :: root_rank
    integer :: mpierror, irank

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    call MPI_BCast(l,1,mpi_logical,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine BroadcastLogical

  subroutine BroadcastInteger(n,root_rank)

    integer :: n
    integer, optional :: root_rank
    integer :: mpierror, irank

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    call MPI_BCast(n,1,mpi_integer,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine BroadcastInteger

  subroutine BroadcastInteger_1(n,root_rank)

    integer, dimension(:) :: n
    integer, optional :: root_rank
    integer :: mpierror, irank

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    call MPI_BCast(n,size(n),mpi_integer,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)
    
  end subroutine BroadcastInteger_1

  subroutine BroadcastReal_sp(x,root_rank)

    real(sp) :: x
    integer, optional :: root_rank
    integer :: mpierror, irank

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    call MPI_BCast(x,1,mpi_sp,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine BroadCastReal_sp

  subroutine BroadcastReal(x,root_rank)

    real(DP) :: x
    integer, optional :: root_rank
    integer :: mpierror, irank

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    call MPI_BCast(x,1,mpi_dp,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine BroadCastReal

  subroutine BroadcastReal_1(x,root_rank)

    real(DP), dimension(:) :: x
    integer, optional :: root_rank
    integer :: mpierror, irank, isize

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    isize = size(x)

    call MPI_BCast(x,isize,mpi_dp,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine BroadcastReal_1

  subroutine BroadcastComplex_sc(z,root_rank)

    complex(sc) :: z
    integer, optional :: root_rank
    integer :: mpierror, irank

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    call MPI_BCast(z,1,mpi_sc,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine BroadcastComplex_sc

  subroutine BroadcastComplex(z,root_rank)

    complex(DC) :: z
    integer, optional :: root_rank
    integer :: mpierror, irank

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    call MPI_BCast(z,1,mpi_dc,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine BroadCastComplex

  subroutine BroadcastString(s,root_rank)

    character(len=*) :: s
    integer, optional :: root_rank
    integer :: mpierror, irank

    if ( present(root_rank) ) then
       irank = root_rank
    else
       irank = mpi_root_rank
    end if

    call MPI_BCast(s,len(s),mpi_character,irank,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine BroadcastString

end module CollectiveOps
