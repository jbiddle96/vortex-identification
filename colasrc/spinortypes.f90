#include "defines.h"

module SpinorTypes

  use Kinds
  implicit none
  private

  integer, parameter, public :: ns = 4 !! The size of a Dirac spinor.
  integer, parameter, public :: ns_weyl = 2 !! The size of a Weyl spinor.
  integer, parameter, public :: nsp = 2 !! *To Do: Remove this.*

  !! Define a (Dirac) spinor type.
  type spinor
     sequence
     complex(wc), dimension(ns) :: sp
  end type spinor

  !! Define a (Dirac) spin matrix.
  type spin_matrix
     sequence
     complex(wc), dimension(ns,ns) :: sp
  end type spin_matrix

  !! Define a Weyl spinor type.
  type weyl_spinor
     sequence
     complex(wc), dimension(ns_weyl) :: sp
  end type weyl_spinor

  !! Define a Weyl spin matrix.
  type weyl_spin_matrix
     sequence
     complex(wc), dimension(ns_weyl,ns_weyl) :: sp
  end type weyl_spin_matrix

  public :: spinor, spin_matrix
  public :: weyl_spinor, weyl_spin_matrix
  public :: random_spinor, random_spin_matrix
  public :: random_weyl_spinor, random_weyl_spin_matrix

contains

  !! Return a random Dirac spinor.
  subroutine random_spinor(z)
    
    type(spinor), intent(out) :: z
    real(wp) :: zr(ns), zi(ns)
    
    call random_number(zr)
    call random_number(zi)

    z%sp = cmplx(zr,zi,wc)

  end subroutine random_spinor

  !! Return a random Weyl spinor.
  subroutine random_weyl_spinor(z)
    
    type(weyl_spinor), intent(out) :: z
    real(wp) :: zr(ns_weyl), zi(ns_weyl)
    
    call random_number(zr)
    call random_number(zi)

    z%sp = cmplx(zr,zi,wc)

  end subroutine random_weyl_spinor

  !! Return a random Dirac spin matrix.
  subroutine random_spin_matrix(z)
    
    type(spin_matrix), intent(out) :: z
    real(wp) :: zr(ns,ns), zi(ns,ns)
    
    call random_number(zr)
    call random_number(zi)

    z%sp = cmplx(zr,zi,wc)

  end subroutine random_spin_matrix

  !! Return a random Weyl spin matrix.
  subroutine random_weyl_spin_matrix(z)
    
    type(weyl_spin_matrix), intent(out) :: z
    real(wp) :: zr(ns_weyl,ns_weyl), zi(ns_weyl,ns_weyl)
    
    call random_number(zr)
    call random_number(zi)

    z%sp = cmplx(zr,zi,wc)

  end subroutine random_weyl_spin_matrix

end module SpinorTypes
