#include "defines.h"

!Author: Waseem Kamleh
!Date: November, 2009

module GaugeFieldIO

  use GaugeField
  use GaugeFieldMPIComms
  use VectorAlgebra
  use ColourFieldOps
  use ReduceOps
  use CollectiveOps
  use LatticeSize
  use MPIInterface
  use Kinds
  use ColourTypes
  use TextIO
  use Strings
  implicit none
  private

  character(len=32), public :: DefaultGaugeFieldFormat = "cssm"
  !! Currently supported file formats: "cssm", "ildg", "ildg-split"

  public :: InitialiseGaugeField
  public :: FinaliseGaugeField
  public :: ReadGaugeField
  public :: WriteGaugeField

contains

  subroutine InitialiseGaugeField(file_stub)
    character(len=*) :: file_stub
    
    character(len=_FILENAME_LEN_) :: gffile
    integer :: file_unit
    character(len=32) :: GaugeFieldFormat
    character(len=_FILENAME_LEN_)  :: InputConfig

    gffile = trim(file_stub)//".gf"
    
    call OpenTextfile(trim(gffile),file_unit,action='read')
    call Get(InputConfig,file_unit)
    call Get(GaugeFieldFormat,file_unit)
    call CloseTextfile(file_unit)

    call Put("Input configuration:"//trim(InputConfig))
    call Put("Gaugefield file format:"//trim(GaugeFieldFormat))

    call CreateGaugeField(U_xd,1)

    call Put("Reading Gauge Field")
    if ( trim(GaugeFieldFormat) == "U=1" ) then
       U_xd = unit_matrix
    else if ( trim(GaugeFieldFormat) == "U=random" ) then
       call RandomGaugeField(U_xd)
    else
       call ReadGaugeField(InputConfig, U_xd, file_format = GaugeFieldFormat)
    end if

  end subroutine InitialiseGaugeField
  
  subroutine FinaliseGaugeField

    call DestroyGaugeField(U_xd)
    
  end subroutine FinaliseGaugeField

  subroutine ReadGaugeField(filename, U_xd, shift, file_format)
    ! begin args: filename, U_xd

    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:), intent(out) :: U_xd
    integer, dimension(4), optional :: shift !! Parameter for shifted gauge fields.
    character(len=*), optional  :: file_format 

    ! begin local_vars
    character(len=32) :: the_format 

    ! begin execution

    if ( present(file_format) ) then
       the_format = trim(file_format)
    else
       the_format = trim(DefaultGaugeFieldFormat)
    end if
       
    if ( trim(the_format) == "cssm" ) then
       call ReadGaugeField_cssm(filename,U_xd,shift)
    else if ( trim(the_format) == "ildg" ) then
       call ReadGaugeField_ildg(filename,U_xd,shift)
    else if ( trim(the_format) == "ildg-split" ) then
       call ReadGaugeField_split(filename,U_xd,shift)
    else if ( trim(the_format) == "U=1" ) then
       U_xd = unit_matrix
       beta = 6.0_dp
       uzero = 1.0_dp
    else if ( trim(the_format) == "U=random" ) then
       call RandomGaugeField(U_xd)
    else
       call Abort("Unrecognised gauge field format code:"//trim(the_format))
    end if

    call ShadowGaugeField(U_xd,1)

  end subroutine ReadGaugeField

  subroutine WriteGaugeField(filename, U_xd, icfg, file_status, file_format)
    ! begin args: filename, U_xd, icfg, file_status

    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: icfg
    character(len=*) :: file_status
    character(len=*), optional  :: file_format

    character(len=32) :: the_format 

    ! begin execution
    if ( present(file_format) ) then
       the_format = trim(file_format)
    else
       the_format = trim(DefaultGaugeFieldFormat)
    end if

    if ( trim(the_format) == "cssm" ) then
       call WriteGaugeField_cssm(filename,U_xd, icfg, file_status)
    else if ( trim(the_format) == "ildg" ) then
       call WriteGaugeField_ildg(filename,U_xd, icfg, file_status)
    else if ( trim(the_format) == "ildg-split" ) then
       call WriteGaugeField_split(filename,U_xd, icfg, file_status)
    else
       call Abort("Unrecognised gauge field format code:"//trim(the_format))
    end if

  end subroutine WriteGaugeField

  subroutine ReadGaugeField_cssm(filename,U_xd, shift)
    ! begin args: filename, U_xd

    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd !! Global gauge field to be read in.
    integer, dimension(4), optional :: shift !! Parameter for shifted gauge fields.

    ! begin local_vars
    real(DP), dimension(:,:,:,:,:,:,:), allocatable ::  ReU, ImU
    type(colour_vector) :: v1,v2,v3
    integer  :: nconfig,nxdim,nydim,nzdim,ntdim

    integer :: ic, jc, irank, mu
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: V_xd !! Temporary storage.
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: U_lxd !! Global gauge to be read in.


    ! begin execution

    allocate(V_xd(nx,ny,nz,nt,nd)) 
    if ( i_am_root ) allocate(U_lxd(nlx,nly,nlz,nlt,nd))

    if ( i_am_root ) then

       allocate(ReU(nlx,nly,nlz,nlt,nd,nc-1,nc))
       allocate(ImU(nlx,nly,nlz,nlt,nd,nc-1,nc))

       ReU = 0.0d0
       ImU = 0.0d0

       open(101,file=filename,form='unformatted',status='old',action='read')

       read (101) nconfig,beta,nxdim,nydim,nzdim,ntdim
       write(*,*) nconfig,beta,nxdim,nydim,nzdim,ntdim

       if ( (nxdim /= nlx) .or. (nydim /= nly) .or. (nzdim /= nlz) .or. (ntdim /= nlt) ) then
          call Abort("ReadGaugeField: Lattice size mismatch")
       end if

       do ic = 1, nc-1
          read (101) ReU(:,:,:,:,:,ic,:)
          read (101) ImU(:,:,:,:,:,ic,:)
       end do

       read (101) lastPlaq, plaqbarAvg, uzero
       close(101)

       ! begin execution
       do mu=1,nd
          do it=1,nlt; do iz=1,nlz; do iy=1,nly; do ix=1,nlx

             v1%Cl(:) = cmplx(ReU(ix,iy,iz,it,mu,1,:),ImU(ix,iy,iz,it,mu,1,:),dc)
             v2%Cl(:) = cmplx(ReU(ix,iy,iz,it,mu,2,:),ImU(ix,iy,iz,it,mu,2,:),dc)

             call orthogonalise_vectors(v2,v1)
             call vector_product(v3,v1,v2)

             U_lxd(ix,iy,iz,it,mu)%Cl(1,:) = v1%Cl(:)
             U_lxd(ix,iy,iz,it,mu)%Cl(2,:) = v2%Cl(:)
             U_lxd(ix,iy,iz,it,mu)%Cl(3,:) = v3%Cl(:)

          end do; end do; end do; end do
       end do

       deallocate(ReU)
       deallocate(ImU)

    end if

    call Broadcast(beta,mpi_root_rank)
    call Broadcast(lastPlaq,mpi_root_rank)
    call Broadcast(plaqbarAvg,mpi_root_rank)
    call Broadcast(uzero,mpi_root_rank)

    if ( present(shift) ) then
       do mu=1,4
          if (shift(mu) /= 0 ) U_lxd = cshift(U_lxd,dim=mu,shift=shift(mu))
       end do
    end if

    if ( i_am_root ) then
       do irank=0,nproc-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          V_xd(1:nx,1:ny,1:nz,1:nt,:) = U_lxd(ix:jx,iy:jy,iz:jz,it:jt,:)

          if ( irank /= mpi_root_rank ) then
             call SendMatrixField(V_xd, irank)
          else
             U_xd(1:nx,1:ny,1:nz,1:nt,:)  = V_xd
          end if
       end do

    else
       call RecvMatrixField(V_xd, mpi_root_rank)
       U_xd(1:nx,1:ny,1:nz,1:nt,:)  = V_xd
    end if

    deallocate(V_xd)
    if ( i_am_root ) deallocate(U_lxd)

  end subroutine ReadGaugeField_cssm

  subroutine ReadGaugeField_trin(filename, U_xd, shift)

    ! begin args: filename, U_xd

    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:)  :: U_xd !! Global gauge field to be read in.
    integer, dimension(4), optional :: shift !! Parameter for shifted gauge fields.

    ! begin local_vars
    integer :: irec
    character(len=8), dimension(5) :: header
    real(dp) :: ReZ, ImZ

    integer :: ic, jc, irank, mu
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: V_xd !! Temporary storage.
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: U_lxd !! Global gauge to be read in.
    ! begin execution

    allocate(V_xd(nx,ny,nz,nt,nd)) 
    if ( i_am_root ) allocate(U_lxd(nlx,nly,nlz,nlt,nd))

    if ( i_am_root ) then

       open(101,file=filename,form='unformatted',access='direct',status='old',action='read',recl=8)

       irec = 1
       do ic=1,5
          read (101,rec=irec) header(ic)
          irec = irec + 1
       end do

       do it=1,nlt
          do mu=1,nd
             do iz=1,nlz; do iy=1,nly; do ix=1,nlx
                do ic=1,nc; do jc=1,nc
                   read(101,rec=irec) ReZ
                   irec = irec + 1
                   read(101,rec=irec) ImZ
                   irec = irec + 1
                   U_lxd(ix,iy,iz,it,mu)%Cl(ic,jc) = cmplx(ReZ,ImZ,dc)
                end do; end do
             end do; end do; end do
          end do
       end do
       close(101)

    end if

    uzero = 1.0_dp ! No uzero value is read, so set u_0 = 1

    call MPIBarrier

    if ( present(shift) ) then
       do mu=1,4
          if (shift(mu) /= 0 ) U_lxd = cshift(U_lxd,dim=mu,shift=shift(mu))
       end do
    end if

    if ( i_am_root ) then
       do irank=0,nproc-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          V_xd(1:nx,1:ny,1:nz,1:nt,:) = U_lxd(ix:jx,iy:jy,iz:jz,it:jt,:)

          if ( irank /= mpi_root_rank ) then
             call SendMatrixField(V_xd, irank)
          else
             U_xd(1:nx,1:ny,1:nz,1:nt,:)  = V_xd
          end if
       end do

    else
       call RecvMatrixField(V_xd, mpi_root_rank)
       U_xd(1:nx,1:ny,1:nz,1:nt,:)  = V_xd
    end if

    deallocate(V_xd)
    if ( i_am_root ) deallocate(U_lxd)

  end subroutine ReadGaugeField_trin

  subroutine ReadGaugeField_ildg(filename, U_xd, shift)

    ! begin args: filename, U_xd
    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:)  :: U_xd !! Global gauge field to be read in.
    integer, dimension(4), optional :: shift !! Parameter for shifted gauge fields.

    ! begin local_vars
    integer :: irec

    integer :: ic, jc, irank, mu
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: V_xd !! Temporary storage.
    complex(dc), dimension(:,:,:,:,:,:,:), allocatable  :: U_lxd !! Global gauge to be read in.
    integer :: matrix_len, irecl
    ! begin execution

    allocate(V_xd(nx,ny,nz,nt,nd)) 
    matrix_len = 16*nc*nc
    irecl = matrix_len*nd*nlx*nly*nlz*nlt

    if ( i_am_root ) then
       allocate(U_lxd(nc,nc,nd,nlx,nly,nlz,nlt))

       open(101,file=filename,form='unformatted',access='direct',status='old',action='read',recl=irecl)

       !do it=1,nlt; do iz=1,nlz; do iy=1,nly; do ix=1,nlx
       !   do mu=1,nd
       !      do ic=1,nc; do jc=1,nc
       !         read(101,rec=irec) U_lxd(ix,iy,iz,it,mu)%Cl(ic,jc)
       !         irec = irec + 1
       !      end do; end do
       !   end do
       !end do; end do; end do; end do

       irec = 1
       read(101,rec=irec) U_lxd
       close(101)

       if ( present(shift) ) then
          do mu=1,4
             if (shift(mu) /= 0 ) U_lxd = cshift(U_lxd,dim=mu+3,shift=shift(mu))
          end do
       end if

       do irank=0,nproc-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do mu=1,nd
             do jc=1,nc; do ic=1,nc
                ! Take the matrix transpose here.
                V_xd(1:nx,1:ny,1:nz,1:nt,mu)%cl(ic,jc) = U_lxd(jc,ic,mu,ix:jx,iy:jy,iz:jz,it:jt)
             end do; end do
          end do

          if ( irank /= mpi_root_rank ) then
             call SendMatrixField(V_xd, irank)
          else
             do mu=1,nd
                U_xd(1:nx,1:ny,1:nz,1:nt,:)  = V_xd
             end do
          end if
       end do

       deallocate(U_lxd)

    else
       call RecvMatrixField(V_xd, mpi_root_rank)
       U_xd(1:nx,1:ny,1:nz,1:nt,:)  = V_xd
    end if

    uzero = 1.0_dp ! No uzero value is read, so set u_0 = 1
    
    call MPIBarrier

    deallocate(V_xd)

  end subroutine ReadGaugeField_ildg

  subroutine ReadGaugeField_split(filename, U_xd, shift)
    ! begin args: filename, U_xd
    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:)  :: U_xd !! Global gauge field to be read in.
    integer, dimension(4), optional :: shift !! Parameter for shifted gauge fields.

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: CfgFile
    character(len=6) :: ProcSuffix
    integer :: irec
    integer :: ic, jc, irank, mu
    integer :: matrix_len, irecl
    integer :: FileID
    complex(dc), dimension(:,:,:,:,:,:,:), allocatable :: U_split
    ! begin execution

    matrix_len = 16*nc*nc
    irecl = matrix_len*nd*nx*ny*nz*nt

    allocate(U_split(nc,nc,nd,nx,ny,nz,nt))

    write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
    CfgFile = trim(filename) // ProcSuffix
    FileID = 101 + mpi_rank

    open(FileID,file=CfgFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)
    read(FileID,rec=1) U_split
    close(FileID)

    do mu=1,nd
       do jc=1,nc; do ic=1,nc
          U_xd(1:nx,1:ny,1:nz,1:nt,mu)%cl(ic,jc) = U_split(jc,ic,mu,:,:,:,:)
       end do; end do
    end do

    deallocate(U_split)

    if ( present(shift) ) then
       do mu=1,4
          if (shift(mu) /= 0 ) U_xd(:,:,:,:,mu) = CShiftGaugePlane(U_xd(:,:,:,:,mu),mu,shift(mu))
       end do
    end if

    uzero = 1.0_dp ! No uzero value is read, so set u_0 = 1
    
    call MPIBarrier

  end subroutine ReadGaugeField_split

  subroutine WriteGaugeField_cssm(filename, U_xd, icfg, file_status)

    ! begin args: filename, U_xd, icfg, file_status
    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: icfg
    character(len=*) :: file_status

    ! begin local_vars
    integer :: irank
    integer :: ic, jc
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: V_xd
    real(DP), dimension(:,:,:,:,:,:,:), allocatable ::  ReU, ImU

    ! begin execution
    allocate(V_xd(nx,ny,nz,nt,nd))

    if ( i_am_root ) then
       allocate(ReU(nlx,nly,nlz,nlt,nd,nc,nc))
       allocate(ImU(nlx,nly,nlz,nlt,nd,nc,nc))

       do irank=0,nproc-1
          if ( irank /= mpi_root_rank ) then
             call RecvMatrixField(V_xd, irank)
          else
             V_xd = U_xd(1:nx,1:ny,1:nz,1:nt,:)
          end if
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do jc=1,nc; do ic=1,nc
             ReU(ix:jx,iy:jy,iz:jz,it:jt,:,ic,jc) =  real(V_xd(:,:,:,:,:)%Cl(ic,jc))
             ImU(ix:jx,iy:jy,iz:jz,it:jt,:,ic,jc) = aimag(V_xd(:,:,:,:,:)%Cl(ic,jc))
          end do; end do
       end do

       open(101,file=filename,form='unformatted',status=file_status,action='write')
       
       write (101) icfg,beta,nlx,nly,nlz,nlt
       
       do ic = 1, nc-1
          write (101) ReU(:,:,:,:,:,ic,:)
          write (101) ImU(:,:,:,:,:,ic,:)
       end do

       write (101) lastPlaq, plaqbarAvg, uzero
       close(101)

       deallocate(ReU)
       deallocate(ImU)
    else
       V_xd = U_xd(1:nx,1:ny,1:nz,1:nt,:)
       call SendMatrixField(V_xd, mpi_root_rank)
    end if

    deallocate(V_xd)

    call MPIBarrier

  end subroutine WriteGaugeField_cssm

  subroutine WriteGaugeField_ildg(filename, U_xd, icfg, file_status)

    ! begin args: filename, U_xd, icfg, file_status
    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: icfg !ignored at the moment
    character(len=*) :: file_status

    ! begin local_vars
    integer :: irank, irec
    integer :: ic, jc
    integer :: ix, jx, iy, jy, iz, jz, it, jt, mu
    integer :: matrix_len, irecl

    type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: V_xd
    complex(dc), dimension(:,:,:,:,:,:,:), allocatable  :: U_lxd !! Global gauge to be read in.


    ! begin execution
    allocate(V_xd(nx,ny,nz,nt,nd))
    matrix_len = 16*nc*nc
    irecl = matrix_len*nd*nlx*nly*nlz*nlt

    if ( i_am_root ) then
       allocate(U_lxd(nc,nc,nd,nlx,nly,nlz,nlt))
       
       do irank=0,nproc-1
          if ( irank /= mpi_root_rank ) then
             call RecvMatrixField(V_xd, irank)
          else
             V_xd = U_xd(1:nx,1:ny,1:nz,1:nt,:)
          end if
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do mu=1,nd
             do jc=1,nc; do ic=1,nc
                U_lxd(jc,ic,mu,ix:jx,iy:jy,iz:jz,it:jt) = V_xd(1:nx,1:ny,1:nz,1:nt,mu)%cl(ic,jc)
             end do; end do
          end do
       end do

       open(101,file=filename,form='unformatted',access='direct',status=file_status,action='write',recl=irecl)
       irec = 1
       write(101,rec=irec) U_lxd
       close(101)

    else
       V_xd = U_xd(1:nx,1:ny,1:nz,1:nt,:)
       call SendMatrixField(V_xd, mpi_root_rank)
    end if

    deallocate(V_xd)

    call MPIBarrier

  end subroutine WriteGaugeField_ildg

  subroutine WriteGaugeField_split(filename, U_xd, icfg, file_status)

    ! begin args: filename, U_xd
    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:)  :: U_xd !! Global gauge field to be read in.
    integer :: icfg !ignored at the moment
    character(len=*) :: file_status

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: CfgFile
    character(len=6) :: ProcSuffix
    integer :: irec
    integer :: ic, jc, irank, mu
    integer :: matrix_len, irecl
    integer :: FileID
    complex(dc), dimension(:,:,:,:,:,:,:), allocatable :: U_split
    ! begin execution

    matrix_len = 16*nc*nc
    irecl = matrix_len*nd*nx*ny*nz*nt

    allocate(U_split(nc,nc,nd,nx,ny,nz,nt))

    do mu=1,nd
       do jc=1,nc; do ic=1,nc
          U_split(jc,ic,mu,:,:,:,:) = U_xd(1:nx,1:ny,1:nz,1:nt,mu)%cl(ic,jc)
       end do; end do
    end do

    write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
    CfgFile = trim(filename) // ProcSuffix
    FileID = 101 + mpi_rank

    open(FileID,file=CfgFile,form='unformatted',access='direct',status=file_status,action='write',recl=irecl)
    write(FileID,rec=1) U_split
    close(FileID)

    deallocate(U_split)

    call MPIBarrier

  end subroutine WriteGaugeField_split

end module GaugeFieldIO
