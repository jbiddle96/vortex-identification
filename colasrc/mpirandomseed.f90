!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/mpirandomseed.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: mpirandomseed.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :


!Author (in HPF): Paul Coddington,
!Modified to MPI by Waseem Kamleh

module MPIRandomSeed

  use Kinds
  use MPIInterface
#ifdef _hydra_
  use IFLPort
#endif
  implicit none
  private

  public :: mpi_random_seed
  public :: bitrand_seeder

contains

  !----------------------------------------------------------------------!
  !                                                                      !
  !     Initialize the intrinsic Fortran random number generator         !
  !     using a single integer seed.                                     !
  !                                                                      !
  !                                                                      !
  !     Fortran 90/95 has an intrinsic function RANDOM_NUMBER for        !
  !     generating pseudo-random numbers. There is also an intrinsic     !
  !     function RANDOM_SEED for initializing the random number          !
  !     generator.                                                       !
  !                                                                      !
  !     The problem with the RANDOM_SEED intrinsic function is that      !
  !     the number of seeds it takes as input parameters depends on      !
  !     how the random number generator is implemented, so it will       !
  !     vary with different Fortran compilers. It is up to the user      !
  !     to check how many seeds are required and then to provide the     !
  !     appropriate number of *random* seeds -- choosing correlated      !
  !     seeds such as seed, seed+1, seed+2, etc will affect the          !
  !     randomness properties of most random number generators.          !
  !                                                                      !
  !     Of course this means that you need to use a random number        !
  !     generator in order to initialize the random number generator,    !
  !     and that is what is provided here.                               !
  !                                                                      !
  !     The routine HPF_RANDOM_SEED takes a single seed as input,        !
  !     checks the Fortran 90 intrinsic RANDOM_SEED to see how many      !
  !     seeds are required, and then calls the routine bitrand_seeder,   !
  !     which uses a simple linear congruential random number            !
  !     generator to create the appropriate number of random seeds,      !
  !     using random values for each bit of each seed. The seeds are     !
  !     passed to RANDOM_SEED to initialize the RANDOM_NUMBER            !
  !     intrinsic function.                                              !
  !                                                                      !
  !----------------------------------------------------------------------!


  subroutine mpi_random_seed(iseed)
    ! begin args: iseed

    integer :: iseed
    ! begin local_vars
    integer :: i,N
    integer, dimension(:), allocatable :: seeder_array

    !--- Find out the size of the seed array used in the Fortran 90
    !--- random number generator
    ! begin execution

    call random_seed(size=N)

    !--- Initialize the seed array by setting each bit using a
    !--- simple linear congruential generator (in bitrand_seeder)
    allocate ( seeder_array(N) )

    !Modification to MPI:
    !This code executes identically in each process,
    !so we must include a rank dependent seed to make sure that
    !we get different random numbers on each process.

#ifndef _hydra_
    call bitrand_seeder(iseed+mpi_rank,N,seeder_array)
#else
    call seed(2**31 - 2*iseed - 4096*mpi_rank - 1)
    do i=1,N
       seeder_array(i) = irand()
    end do
#endif



    call random_seed( put=seeder_array(1:N) )
    deallocate( seeder_array )


  end subroutine mpi_random_seed


  !----------------------------------------------------------------------!
  !                                                                      !
  !     Initialize each bit of the 32-bit integer seed array using a     !
  !     simple linear congruential generator.                            !
  !                                                                      !
  !----------------------------------------------------------------------!

  subroutine bitrand_seeder(seed,N,seed_array)
    ! begin args: seed, N, seed_array
    integer :: seed,N
    integer :: seed_array(N)
    ! begin local_vars
    integer :: i,j,k
    integer :: s,t,randx
    real(dp), parameter :: TWOTO31 = 2147483648.0   ! 2^{31}
    real(dp), parameter :: TWOTONEG31=(1.0/TWOTO31)
    integer, parameter :: A=1103515245, C=12345
    integer :: M
    ! begin execution

    data  M   /Z'7FFFFFFF'/

    !--- Initialize the linear congruential generator RAND.
    randx = seed

    do i = 1, N
       s = 0
       t = 1
       do j = 0, 30
          randx = iand((A * randx + C),M)
          if((randx * TWOTONEG31) .lt. 0.5) then
             s = s + t
          endif
          t = 2 * t
          !------- Throw away a few random numbers just to foil any
          !------- possible correlations that might affect RAND.
          do k=1,5
             randx = iand((A * randx + C),M)
          enddo
       enddo
       !------- Throw away a few random numbers just to foil any
       !------- possible correlations that might affect RAND.
       do k=1,13
          randx = iand((A * randx + C),M)
       enddo

       seed_array(i) = s

    enddo

    return

  end subroutine bitrand_seeder

end module MPIRandomSeed

