!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/matrixalgebra.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: matrixalgebra.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module MatrixAlgebra

  use Kinds
  use ColourTypes
  implicit none
  private

  interface operator(*)
     module procedure real_times_matrix
  end interface

  interface operator(-)
     module procedure matrix_minus_matrix
  end interface

  public :: operator(*)
  public :: operator(-)
  public :: real_trace
  public :: trace
  public :: lie_norm
  public :: matrix_normsq
  public :: GetIdentityMatrix
  public :: MatDag
  public :: MultiplyMatMat
  public :: MultiplyMatMatdag
  public :: MultiplyMatdagMat
  public :: MultiplyMatdagMatdag
  public :: RealTraceMultMatMat
  public :: TraceMultMatMat
  public :: MultiplyMatDiagMat
  public :: MultiplyMatRealDiagMat
  public :: MatPlusMatTimesMat
  public :: MatPlusMatTimesMatdag
  public :: MatPlusMatdagTimesMat
  public :: MatPlusMatdagTimesMatdag
  public :: MatMinusMatTimesMat
  public :: MatMinusMatTimesMatdag
  public :: MatMinusMatdagTimesMat
  public :: MatMinusMatdagTimesMatdag

contains

  elemental function real_times_matrix(left,right) result (times)

    real(dp), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix) :: times

    times%Cl = left*Right%Cl

  end function real_times_matrix

  elemental function matrix_minus_matrix(left,right) result (diff)

    type(colour_matrix), intent(in) :: left, right
    type(colour_matrix) :: diff

    diff%Cl = left%Cl - right%Cl

  end function matrix_minus_matrix

  elemental function real_trace(right) result (Tr)

    real(dp) :: Tr
    type(colour_matrix), intent(in) :: right

    Tr = real(right%Cl(1,1) + right%Cl(2,2) + right%Cl(3,3)) 

  end function real_trace

  elemental function trace(right) result (Tr)

    complex(dc) :: Tr
    type(colour_matrix), intent(in) :: right

    Tr = right%Cl(1,1) + right%Cl(2,2) + right%Cl(3,3)

  end function trace

  elemental function lie_norm(right) result (Tr)

    real(dp) :: Tr
    type(colour_matrix), intent(in) :: right
    complex(dc) :: cTr

    cTr = 2*(right%Cl(1,1)*right%Cl(1,1) + right%Cl(1,2)*right%Cl(2,1) + right%Cl(1,3)*right%Cl(3,1) &
         & + right%Cl(2,1)*right%Cl(1,2) + right%Cl(2,2)*right%Cl(2,2) + right%Cl(2,3)*right%Cl(3,2) &
         & + right%Cl(3,1)*right%Cl(1,3) + right%Cl(3,2)*right%Cl(2,3) + right%Cl(3,3)*right%Cl(3,3))

    Tr = sqrt(real(cTr,dp))

  end function lie_norm

  elemental function matrix_normsq(right) result (norm)

    real(dp) :: norm
    type(colour_matrix), intent(in) :: right

    norm = sum(real(conjg(right%Cl)*right%Cl))

  end function matrix_normsq

  elemental subroutine GetIdentityMatrix(I_x)

    type(colour_matrix), intent(out) :: I_x

    I_x%Cl = 0.0d0

    I_x%Cl(1,1) = 1.0d0
    I_x%Cl(2,2) = 1.0d0
    I_x%Cl(3,3) = 1.0d0

  end subroutine GetIdentityMatrix

  elemental subroutine MatDag(dag,left)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(out) :: dag

    dag%Cl(1,1) =  conjg(left%Cl(1,1))
    dag%Cl(2,1) =  conjg(left%Cl(1,2))
    dag%Cl(3,1) =  conjg(left%Cl(1,3))
    dag%Cl(1,2) =  conjg(left%Cl(2,1))
    dag%Cl(2,2) =  conjg(left%Cl(2,2))
    dag%Cl(3,2) =  conjg(left%Cl(2,3))
    dag%Cl(1,3) =  conjg(left%Cl(3,1))
    dag%Cl(2,3) =  conjg(left%Cl(3,2))
    dag%Cl(3,3) =  conjg(left%Cl(3,3))

  end subroutine MatDag

  elemental subroutine MultiplyMatMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(out) :: MM

    MM%Cl(1,1) = left%Cl(1,1)*right%Cl(1,1) + left%Cl(1,2)*right%Cl(2,1) + left%Cl(1,3)*right%Cl(3,1)
    MM%Cl(2,1) = left%Cl(2,1)*right%Cl(1,1) + left%Cl(2,2)*right%Cl(2,1) + left%Cl(2,3)*right%Cl(3,1)
    MM%Cl(3,1) = left%Cl(3,1)*right%Cl(1,1) + left%Cl(3,2)*right%Cl(2,1) + left%Cl(3,3)*right%Cl(3,1)

    MM%Cl(1,2) = left%Cl(1,1)*right%Cl(1,2) + left%Cl(1,2)*right%Cl(2,2) + left%Cl(1,3)*right%Cl(3,2)
    MM%Cl(2,2) = left%Cl(2,1)*right%Cl(1,2) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(2,3)*right%Cl(3,2)
    MM%Cl(3,2) = left%Cl(3,1)*right%Cl(1,2) + left%Cl(3,2)*right%Cl(2,2) + left%Cl(3,3)*right%Cl(3,2)

    MM%Cl(1,3) = left%Cl(1,1)*right%Cl(1,3) + left%Cl(1,2)*right%Cl(2,3) + left%Cl(1,3)*right%Cl(3,3)
    MM%Cl(2,3) = left%Cl(2,1)*right%Cl(1,3) + left%Cl(2,2)*right%Cl(2,3) + left%Cl(2,3)*right%Cl(3,3)
    MM%Cl(3,3) = left%Cl(3,1)*right%Cl(1,3) + left%Cl(3,2)*right%Cl(2,3) + left%Cl(3,3)*right%Cl(3,3)

  end subroutine MultiplyMatMat

  elemental subroutine MultiplyMatMatdag(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(out) :: MM

    MM%Cl(1,1) = left%Cl(1,1)*conjg(right%Cl(1,1)) + left%Cl(1,2)*conjg(right%Cl(1,2)) + left%Cl(1,3)*conjg(right%Cl(1,3))
    MM%Cl(2,1) = left%Cl(2,1)*conjg(right%Cl(1,1)) + left%Cl(2,2)*conjg(right%Cl(1,2)) + left%Cl(2,3)*conjg(right%Cl(1,3))
    MM%Cl(3,1) = left%Cl(3,1)*conjg(right%Cl(1,1)) + left%Cl(3,2)*conjg(right%Cl(1,2)) + left%Cl(3,3)*conjg(right%Cl(1,3))

    MM%Cl(1,2) = left%Cl(1,1)*conjg(right%Cl(2,1)) + left%Cl(1,2)*conjg(right%Cl(2,2)) + left%Cl(1,3)*conjg(right%Cl(2,3))
    MM%Cl(2,2) = left%Cl(2,1)*conjg(right%Cl(2,1)) + left%Cl(2,2)*conjg(right%Cl(2,2)) + left%Cl(2,3)*conjg(right%Cl(2,3))
    MM%Cl(3,2) = left%Cl(3,1)*conjg(right%Cl(2,1)) + left%Cl(3,2)*conjg(right%Cl(2,2)) + left%Cl(3,3)*conjg(right%Cl(2,3))

    MM%Cl(1,3) = left%Cl(1,1)*conjg(right%Cl(3,1)) + left%Cl(1,2)*conjg(right%Cl(3,2)) + left%Cl(1,3)*conjg(right%Cl(3,3))
    MM%Cl(2,3) = left%Cl(2,1)*conjg(right%Cl(3,1)) + left%Cl(2,2)*conjg(right%Cl(3,2)) + left%Cl(2,3)*conjg(right%Cl(3,3))
    MM%Cl(3,3) = left%Cl(3,1)*conjg(right%Cl(3,1)) + left%Cl(3,2)*conjg(right%Cl(3,2)) + left%Cl(3,3)*conjg(right%Cl(3,3))

  end subroutine MultiplyMatMatdag

  elemental subroutine MultiplyMatdagMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(out) :: MM

    MM%Cl(1,1) = conjg(left%Cl(1,1))*right%Cl(1,1) + conjg(left%Cl(2,1))*right%Cl(2,1) + conjg(left%Cl(3,1))*right%Cl(3,1)
    MM%Cl(2,1) = conjg(left%Cl(1,2))*right%Cl(1,1) + conjg(left%Cl(2,2))*right%Cl(2,1) + conjg(left%Cl(3,2))*right%Cl(3,1)
    MM%Cl(3,1) = conjg(left%Cl(1,3))*right%Cl(1,1) + conjg(left%Cl(2,3))*right%Cl(2,1) + conjg(left%Cl(3,3))*right%Cl(3,1)

    MM%Cl(1,2) = conjg(left%Cl(1,1))*right%Cl(1,2) + conjg(left%Cl(2,1))*right%Cl(2,2) + conjg(left%Cl(3,1))*right%Cl(3,2)
    MM%Cl(2,2) = conjg(left%Cl(1,2))*right%Cl(1,2) + conjg(left%Cl(2,2))*right%Cl(2,2) + conjg(left%Cl(3,2))*right%Cl(3,2)
    MM%Cl(3,2) = conjg(left%Cl(1,3))*right%Cl(1,2) + conjg(left%Cl(2,3))*right%Cl(2,2) + conjg(left%Cl(3,3))*right%Cl(3,2)

    MM%Cl(1,3) = conjg(left%Cl(1,1))*right%Cl(1,3) + conjg(left%Cl(2,1))*right%Cl(2,3) + conjg(left%Cl(3,1))*right%Cl(3,3)
    MM%Cl(2,3) = conjg(left%Cl(1,2))*right%Cl(1,3) + conjg(left%Cl(2,2))*right%Cl(2,3) + conjg(left%Cl(3,2))*right%Cl(3,3)
    MM%Cl(3,3) = conjg(left%Cl(1,3))*right%Cl(1,3) + conjg(left%Cl(2,3))*right%Cl(2,3) + conjg(left%Cl(3,3))*right%Cl(3,3)

  end subroutine MultiplyMatdagMat

  elemental subroutine MultiplyMatdagMatdag(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(out) :: MM

    MM%Cl(1,1) = conjg(left%Cl(1,1)*right%Cl(1,1) + left%Cl(2,1)*right%Cl(1,2) + left%Cl(3,1)*right%Cl(1,3))
    MM%Cl(2,1) = conjg(left%Cl(1,2)*right%Cl(1,1) + left%Cl(2,2)*right%Cl(1,2) + left%Cl(3,2)*right%Cl(1,3))
    MM%Cl(3,1) = conjg(left%Cl(1,3)*right%Cl(1,1) + left%Cl(2,3)*right%Cl(1,2) + left%Cl(3,3)*right%Cl(1,3))

    MM%Cl(1,2) = conjg(left%Cl(1,1)*right%Cl(2,1) + left%Cl(2,1)*right%Cl(2,2) + left%Cl(3,1)*right%Cl(2,3))
    MM%Cl(2,2) = conjg(left%Cl(1,2)*right%Cl(2,1) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(3,2)*right%Cl(2,3))
    MM%Cl(3,2) = conjg(left%Cl(1,3)*right%Cl(2,1) + left%Cl(2,3)*right%Cl(2,2) + left%Cl(3,3)*right%Cl(2,3))

    MM%Cl(1,3) = conjg(left%Cl(1,1)*right%Cl(3,1) + left%Cl(2,1)*right%Cl(3,2) + left%Cl(3,1)*right%Cl(3,3))
    MM%Cl(2,3) = conjg(left%Cl(1,2)*right%Cl(3,1) + left%Cl(2,2)*right%Cl(3,2) + left%Cl(3,2)*right%Cl(3,3))
    MM%Cl(3,3) = conjg(left%Cl(1,3)*right%Cl(3,1) + left%Cl(2,3)*right%Cl(3,2) + left%Cl(3,3)*right%Cl(3,3))

  end subroutine MultiplyMatdagMatdag

  elemental subroutine RealTraceMultMatMat(TrMM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    real(dp), intent(out) :: TrMM

    TrMM = real(left%Cl(1,1)*right%Cl(1,1) + left%Cl(1,2)*right%Cl(2,1) + left%Cl(1,3)*right%Cl(3,1) + &
         &      left%Cl(2,1)*right%Cl(1,2) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(2,3)*right%Cl(3,2) + &
         &      left%Cl(3,1)*right%Cl(1,3) + left%Cl(3,2)*right%Cl(2,3) + left%Cl(3,3)*right%Cl(3,3))

  end subroutine RealTraceMultMatMat

  elemental subroutine TraceMultMatMat(TrMM,left,right)

    complex(dc), intent(out) :: TrMM
    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right

    TrMM = left%Cl(1,1)*right%Cl(1,1) + left%Cl(1,2)*right%Cl(2,1) + left%Cl(1,3)*right%Cl(3,1) + &
         & left%Cl(2,1)*right%Cl(1,2) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(2,3)*right%Cl(3,2) + &
         & left%Cl(3,1)*right%Cl(1,3) + left%Cl(3,2)*right%Cl(2,3) + left%Cl(3,3)*right%Cl(3,3)

  end subroutine TraceMultMatMat

  elemental subroutine MultiplyMatDiagMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_vector), intent(in) :: right
    type(colour_matrix), intent(out) :: MM

    MM%Cl(1,1) = left%Cl(1,1)*right%Cl(1)
    MM%Cl(2,1) = left%Cl(2,1)*right%Cl(1)
    MM%Cl(3,1) = left%Cl(3,1)*right%Cl(1)

    MM%Cl(1,2) = left%Cl(1,2)*right%Cl(2)
    MM%Cl(2,2) = left%Cl(2,2)*right%Cl(2)
    MM%Cl(3,2) = left%Cl(3,2)*right%Cl(2)

    MM%Cl(1,3) = left%Cl(1,3)*right%Cl(3)
    MM%Cl(2,3) = left%Cl(2,3)*right%Cl(3)
    MM%Cl(3,3) = left%Cl(3,3)*right%Cl(3)

  end subroutine MultiplyMatDiagMat

  elemental subroutine MultiplyMatRealDiagMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(real_vector), intent(in) :: right
    type(colour_matrix), intent(out) :: MM

    MM%Cl(1,1) = left%Cl(1,1)*right%Cl(1)
    MM%Cl(2,1) = left%Cl(2,1)*right%Cl(1)
    MM%Cl(3,1) = left%Cl(3,1)*right%Cl(1)

    MM%Cl(1,2) = left%Cl(1,2)*right%Cl(2)
    MM%Cl(2,2) = left%Cl(2,2)*right%Cl(2)
    MM%Cl(3,2) = left%Cl(3,2)*right%Cl(2)

    MM%Cl(1,3) = left%Cl(1,3)*right%Cl(3)
    MM%Cl(2,3) = left%Cl(2,3)*right%Cl(3)
    MM%Cl(3,3) = left%Cl(3,3)*right%Cl(3)

  end subroutine MultiplyMatRealDiagMat

  elemental subroutine MatPlusMatTimesMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(inout) :: MM

    MM%Cl(1,1) = MM%Cl(1,1) + left%Cl(1,1)*right%Cl(1,1) + left%Cl(1,2)*right%Cl(2,1) + left%Cl(1,3)*right%Cl(3,1)
    MM%Cl(2,1) = MM%Cl(2,1) + left%Cl(2,1)*right%Cl(1,1) + left%Cl(2,2)*right%Cl(2,1) + left%Cl(2,3)*right%Cl(3,1)
    MM%Cl(3,1) = MM%Cl(3,1) + left%Cl(3,1)*right%Cl(1,1) + left%Cl(3,2)*right%Cl(2,1) + left%Cl(3,3)*right%Cl(3,1)

    MM%Cl(1,2) = MM%Cl(1,2) + left%Cl(1,1)*right%Cl(1,2) + left%Cl(1,2)*right%Cl(2,2) + left%Cl(1,3)*right%Cl(3,2)
    MM%Cl(2,2) = MM%Cl(2,2) + left%Cl(2,1)*right%Cl(1,2) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(2,3)*right%Cl(3,2)
    MM%Cl(3,2) = MM%Cl(3,2) + left%Cl(3,1)*right%Cl(1,2) + left%Cl(3,2)*right%Cl(2,2) + left%Cl(3,3)*right%Cl(3,2)

    MM%Cl(1,3) = MM%Cl(1,3) + left%Cl(1,1)*right%Cl(1,3) + left%Cl(1,2)*right%Cl(2,3) + left%Cl(1,3)*right%Cl(3,3)
    MM%Cl(2,3) = MM%Cl(2,3) + left%Cl(2,1)*right%Cl(1,3) + left%Cl(2,2)*right%Cl(2,3) + left%Cl(2,3)*right%Cl(3,3)
    MM%Cl(3,3) = MM%Cl(3,3) + left%Cl(3,1)*right%Cl(1,3) + left%Cl(3,2)*right%Cl(2,3) + left%Cl(3,3)*right%Cl(3,3)

  end subroutine MatPlusMatTimesMat

  elemental subroutine MatPlusMatTimesMatdag(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(inout) :: MM

    MM%Cl(1,1) = MM%Cl(1,1) + left%Cl(1,1)*conjg(right%Cl(1,1)) + left%Cl(1,2)*conjg(right%Cl(1,2)) + &
         &                    left%Cl(1,3)*conjg(right%Cl(1,3))
    MM%Cl(2,1) = MM%Cl(2,1) + left%Cl(2,1)*conjg(right%Cl(1,1)) + left%Cl(2,2)*conjg(right%Cl(1,2)) + &
         &                    left%Cl(2,3)*conjg(right%Cl(1,3))
    MM%Cl(3,1) = MM%Cl(3,1) + left%Cl(3,1)*conjg(right%Cl(1,1)) + left%Cl(3,2)*conjg(right%Cl(1,2)) + &
         &                    left%Cl(3,3)*conjg(right%Cl(1,3))

    MM%Cl(1,2) = MM%Cl(1,2) + left%Cl(1,1)*conjg(right%Cl(2,1)) + left%Cl(1,2)*conjg(right%Cl(2,2)) + &
         &                    left%Cl(1,3)*conjg(right%Cl(2,3))
    MM%Cl(2,2) = MM%Cl(2,2) + left%Cl(2,1)*conjg(right%Cl(2,1)) + left%Cl(2,2)*conjg(right%Cl(2,2)) + &
         &                    left%Cl(2,3)*conjg(right%Cl(2,3))
    MM%Cl(3,2) = MM%Cl(3,2) + left%Cl(3,1)*conjg(right%Cl(2,1)) + left%Cl(3,2)*conjg(right%Cl(2,2)) + &
         &                    left%Cl(3,3)*conjg(right%Cl(2,3))

    MM%Cl(1,3) = MM%Cl(1,3) + left%Cl(1,1)*conjg(right%Cl(3,1)) + left%Cl(1,2)*conjg(right%Cl(3,2)) + &
         &                    left%Cl(1,3)*conjg(right%Cl(3,3))
    MM%Cl(2,3) = MM%Cl(2,3) + left%Cl(2,1)*conjg(right%Cl(3,1)) + left%Cl(2,2)*conjg(right%Cl(3,2)) + &
         &                    left%Cl(2,3)*conjg(right%Cl(3,3))
    MM%Cl(3,3) = MM%Cl(3,3) + left%Cl(3,1)*conjg(right%Cl(3,1)) + left%Cl(3,2)*conjg(right%Cl(3,2)) + &
         &                    left%Cl(3,3)*conjg(right%Cl(3,3))

  end subroutine MatPlusMatTimesMatdag

  elemental subroutine MatPlusMatdagTimesMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(inout) :: MM

    MM%Cl(1,1) = MM%Cl(1,1) + conjg(left%Cl(1,1))*right%Cl(1,1) + conjg(left%Cl(2,1))*right%Cl(2,1) + &
         &                    conjg(left%Cl(3,1))*right%Cl(3,1)
    MM%Cl(2,1) = MM%Cl(2,1) + conjg(left%Cl(1,2))*right%Cl(1,1) + conjg(left%Cl(2,2))*right%Cl(2,1) + &
         &                    conjg(left%Cl(3,2))*right%Cl(3,1)
    MM%Cl(3,1) = MM%Cl(3,1) + conjg(left%Cl(1,3))*right%Cl(1,1) + conjg(left%Cl(2,3))*right%Cl(2,1) + &
         &                    conjg(left%Cl(3,3))*right%Cl(3,1)

    MM%Cl(1,2) = MM%Cl(1,2) + conjg(left%Cl(1,1))*right%Cl(1,2) + conjg(left%Cl(2,1))*right%Cl(2,2) + &
         &                    conjg(left%Cl(3,1))*right%Cl(3,2)
    MM%Cl(2,2) = MM%Cl(2,2) + conjg(left%Cl(1,2))*right%Cl(1,2) + conjg(left%Cl(2,2))*right%Cl(2,2) + &
         &                    conjg(left%Cl(3,2))*right%Cl(3,2)
    MM%Cl(3,2) = MM%Cl(3,2) + conjg(left%Cl(1,3))*right%Cl(1,2) + conjg(left%Cl(2,3))*right%Cl(2,2) + &
         &                    conjg(left%Cl(3,3))*right%Cl(3,2)

    MM%Cl(1,3) = MM%Cl(1,3) + conjg(left%Cl(1,1))*right%Cl(1,3) + conjg(left%Cl(2,1))*right%Cl(2,3) + &
         &                    conjg(left%Cl(3,1))*right%Cl(3,3)
    MM%Cl(2,3) = MM%Cl(2,3) + conjg(left%Cl(1,2))*right%Cl(1,3) + conjg(left%Cl(2,2))*right%Cl(2,3) + &
         &                    conjg(left%Cl(3,2))*right%Cl(3,3)
    MM%Cl(3,3) = MM%Cl(3,3) + conjg(left%Cl(1,3))*right%Cl(1,3) + conjg(left%Cl(2,3))*right%Cl(2,3) + &
         &                    conjg(left%Cl(3,3))*right%Cl(3,3)

  end subroutine MatPlusMatdagTimesMat

  elemental subroutine MatPlusMatdagTimesMatdag(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(inout) :: MM

    MM%Cl(1,1) = MM%Cl(1,1) + conjg(left%Cl(1,1)*right%Cl(1,1) + left%Cl(2,1)*right%Cl(1,2) + left%Cl(3,1)*right%Cl(1,3))
    MM%Cl(2,1) = MM%Cl(2,1) + conjg(left%Cl(1,2)*right%Cl(1,1) + left%Cl(2,2)*right%Cl(1,2) + left%Cl(3,2)*right%Cl(1,3))
    MM%Cl(3,1) = MM%Cl(3,1) + conjg(left%Cl(1,3)*right%Cl(1,1) + left%Cl(2,3)*right%Cl(1,2) + left%Cl(3,3)*right%Cl(1,3))

    MM%Cl(1,2) = MM%Cl(1,2) + conjg(left%Cl(1,1)*right%Cl(2,1) + left%Cl(2,1)*right%Cl(2,2) + left%Cl(3,1)*right%Cl(2,3))
    MM%Cl(2,2) = MM%Cl(2,2) + conjg(left%Cl(1,2)*right%Cl(2,1) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(3,2)*right%Cl(2,3))
    MM%Cl(3,2) = MM%Cl(3,2) + conjg(left%Cl(1,3)*right%Cl(2,1) + left%Cl(2,3)*right%Cl(2,2) + left%Cl(3,3)*right%Cl(2,3))

    MM%Cl(1,3) = MM%Cl(1,3) + conjg(left%Cl(1,1)*right%Cl(3,1) + left%Cl(2,1)*right%Cl(3,2) + left%Cl(3,1)*right%Cl(3,3))
    MM%Cl(2,3) = MM%Cl(2,3) + conjg(left%Cl(1,2)*right%Cl(3,1) + left%Cl(2,2)*right%Cl(3,2) + left%Cl(3,2)*right%Cl(3,3))
    MM%Cl(3,3) = MM%Cl(3,3) + conjg(left%Cl(1,3)*right%Cl(3,1) + left%Cl(2,3)*right%Cl(3,2) + left%Cl(3,3)*right%Cl(3,3))

  end subroutine MatPlusMatdagTimesMatdag

  elemental subroutine MatMinusMatTimesMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(inout) :: MM

    MM%Cl(1,1) = MM%Cl(1,1) - (left%Cl(1,1)*right%Cl(1,1) + left%Cl(1,2)*right%Cl(2,1) + left%Cl(1,3)*right%Cl(3,1))
    MM%Cl(2,1) = MM%Cl(2,1) - (left%Cl(2,1)*right%Cl(1,1) + left%Cl(2,2)*right%Cl(2,1) + left%Cl(2,3)*right%Cl(3,1))
    MM%Cl(3,1) = MM%Cl(3,1) - (left%Cl(3,1)*right%Cl(1,1) + left%Cl(3,2)*right%Cl(2,1) + left%Cl(3,3)*right%Cl(3,1))

    MM%Cl(1,2) = MM%Cl(1,2) - (left%Cl(1,1)*right%Cl(1,2) + left%Cl(1,2)*right%Cl(2,2) + left%Cl(1,3)*right%Cl(3,2))
    MM%Cl(2,2) = MM%Cl(2,2) - (left%Cl(2,1)*right%Cl(1,2) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(2,3)*right%Cl(3,2))
    MM%Cl(3,2) = MM%Cl(3,2) - (left%Cl(3,1)*right%Cl(1,2) + left%Cl(3,2)*right%Cl(2,2) + left%Cl(3,3)*right%Cl(3,2))

    MM%Cl(1,3) = MM%Cl(1,3) - (left%Cl(1,1)*right%Cl(1,3) + left%Cl(1,2)*right%Cl(2,3) + left%Cl(1,3)*right%Cl(3,3))
    MM%Cl(2,3) = MM%Cl(2,3) - (left%Cl(2,1)*right%Cl(1,3) + left%Cl(2,2)*right%Cl(2,3) + left%Cl(2,3)*right%Cl(3,3))
    MM%Cl(3,3) = MM%Cl(3,3) - (left%Cl(3,1)*right%Cl(1,3) + left%Cl(3,2)*right%Cl(2,3) + left%Cl(3,3)*right%Cl(3,3))

  end subroutine MatMinusMatTimesMat

  elemental subroutine MatMinusMatTimesMatdag(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(inout) :: MM

    MM%Cl(1,1) = MM%Cl(1,1) - (left%Cl(1,1)*conjg(right%Cl(1,1)) + left%Cl(1,2)*conjg(right%Cl(1,2)) + &
         &                    left%Cl(1,3)*conjg(right%Cl(1,3)))
    MM%Cl(2,1) = MM%Cl(2,1) - (left%Cl(2,1)*conjg(right%Cl(1,1)) + left%Cl(2,2)*conjg(right%Cl(1,2)) + &
         &                    left%Cl(2,3)*conjg(right%Cl(1,3)))
    MM%Cl(3,1) = MM%Cl(3,1) - (left%Cl(3,1)*conjg(right%Cl(1,1)) + left%Cl(3,2)*conjg(right%Cl(1,2)) + &
         &                    left%Cl(3,3)*conjg(right%Cl(1,3)))

    MM%Cl(1,2) = MM%Cl(1,2) - (left%Cl(1,1)*conjg(right%Cl(2,1)) + left%Cl(1,2)*conjg(right%Cl(2,2)) + &
         &                    left%Cl(1,3)*conjg(right%Cl(2,3)))
    MM%Cl(2,2) = MM%Cl(2,2) - (left%Cl(2,1)*conjg(right%Cl(2,1)) + left%Cl(2,2)*conjg(right%Cl(2,2)) + &
         &                    left%Cl(2,3)*conjg(right%Cl(2,3)))
    MM%Cl(3,2) = MM%Cl(3,2) - (left%Cl(3,1)*conjg(right%Cl(2,1)) + left%Cl(3,2)*conjg(right%Cl(2,2)) + &
         &                    left%Cl(3,3)*conjg(right%Cl(2,3)))

    MM%Cl(1,3) = MM%Cl(1,3) - (left%Cl(1,1)*conjg(right%Cl(3,1)) + left%Cl(1,2)*conjg(right%Cl(3,2)) + &
         &                    left%Cl(1,3)*conjg(right%Cl(3,3)))
    MM%Cl(2,3) = MM%Cl(2,3) - (left%Cl(2,1)*conjg(right%Cl(3,1)) + left%Cl(2,2)*conjg(right%Cl(3,2)) + &
         &                    left%Cl(2,3)*conjg(right%Cl(3,3)))
    MM%Cl(3,3) = MM%Cl(3,3) - (left%Cl(3,1)*conjg(right%Cl(3,1)) + left%Cl(3,2)*conjg(right%Cl(3,2)) + &
         &                    left%Cl(3,3)*conjg(right%Cl(3,3)))

  end subroutine MatMinusMatTimesMatdag

  elemental subroutine MatMinusMatdagTimesMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(inout) :: MM

    MM%Cl(1,1) = MM%Cl(1,1) - (conjg(left%Cl(1,1))*right%Cl(1,1) + conjg(left%Cl(2,1))*right%Cl(2,1) + &
         &                    conjg(left%Cl(3,1))*right%Cl(3,1))
    MM%Cl(2,1) = MM%Cl(2,1) - (conjg(left%Cl(1,2))*right%Cl(1,1) + conjg(left%Cl(2,2))*right%Cl(2,1) + &
         &                    conjg(left%Cl(3,2))*right%Cl(3,1))
    MM%Cl(3,1) = MM%Cl(3,1) - (conjg(left%Cl(1,3))*right%Cl(1,1) + conjg(left%Cl(2,3))*right%Cl(2,1) + &
         &                    conjg(left%Cl(3,3))*right%Cl(3,1))

    MM%Cl(1,2) = MM%Cl(1,2) - (conjg(left%Cl(1,1))*right%Cl(1,2) + conjg(left%Cl(2,1))*right%Cl(2,2) + &
         &                    conjg(left%Cl(3,1))*right%Cl(3,2))
    MM%Cl(2,2) = MM%Cl(2,2) - (conjg(left%Cl(1,2))*right%Cl(1,2) + conjg(left%Cl(2,2))*right%Cl(2,2) + &
         &                    conjg(left%Cl(3,2))*right%Cl(3,2))
    MM%Cl(3,2) = MM%Cl(3,2) - (conjg(left%Cl(1,3))*right%Cl(1,2) + conjg(left%Cl(2,3))*right%Cl(2,2) + &
         &                    conjg(left%Cl(3,3))*right%Cl(3,2))

    MM%Cl(1,3) = MM%Cl(1,3) - (conjg(left%Cl(1,1))*right%Cl(1,3) + conjg(left%Cl(2,1))*right%Cl(2,3) + &
         &                    conjg(left%Cl(3,1))*right%Cl(3,3))
    MM%Cl(2,3) = MM%Cl(2,3) - (conjg(left%Cl(1,2))*right%Cl(1,3) + conjg(left%Cl(2,2))*right%Cl(2,3) + &
         &                    conjg(left%Cl(3,2))*right%Cl(3,3))
    MM%Cl(3,3) = MM%Cl(3,3) - (conjg(left%Cl(1,3))*right%Cl(1,3) + conjg(left%Cl(2,3))*right%Cl(2,3) + &
         &                    conjg(left%Cl(3,3))*right%Cl(3,3))

  end subroutine MatMinusMatdagTimesMat

  elemental subroutine MatMinusMatdagTimesMatdag(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(colour_matrix), intent(inout) :: MM

    MM%Cl(1,1) = MM%Cl(1,1) - conjg(left%Cl(1,1)*right%Cl(1,1) + left%Cl(2,1)*right%Cl(1,2) + left%Cl(3,1)*right%Cl(1,3))
    MM%Cl(2,1) = MM%Cl(2,1) - conjg(left%Cl(1,2)*right%Cl(1,1) + left%Cl(2,2)*right%Cl(1,2) + left%Cl(3,2)*right%Cl(1,3))
    MM%Cl(3,1) = MM%Cl(3,1) - conjg(left%Cl(1,3)*right%Cl(1,1) + left%Cl(2,3)*right%Cl(1,2) + left%Cl(3,3)*right%Cl(1,3))

    MM%Cl(1,2) = MM%Cl(1,2) - conjg(left%Cl(1,1)*right%Cl(2,1) + left%Cl(2,1)*right%Cl(2,2) + left%Cl(3,1)*right%Cl(2,3))
    MM%Cl(2,2) = MM%Cl(2,2) - conjg(left%Cl(1,2)*right%Cl(2,1) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(3,2)*right%Cl(2,3))
    MM%Cl(3,2) = MM%Cl(3,2) - conjg(left%Cl(1,3)*right%Cl(2,1) + left%Cl(2,3)*right%Cl(2,2) + left%Cl(3,3)*right%Cl(2,3))

    MM%Cl(1,3) = MM%Cl(1,3) - conjg(left%Cl(1,1)*right%Cl(3,1) + left%Cl(2,1)*right%Cl(3,2) + left%Cl(3,1)*right%Cl(3,3))
    MM%Cl(2,3) = MM%Cl(2,3) - conjg(left%Cl(1,2)*right%Cl(3,1) + left%Cl(2,2)*right%Cl(3,2) + left%Cl(3,2)*right%Cl(3,3))
    MM%Cl(3,3) = MM%Cl(3,3) - conjg(left%Cl(1,3)*right%Cl(3,1) + left%Cl(2,3)*right%Cl(3,2) + left%Cl(3,3)*right%Cl(3,3))

  end subroutine MatMinusMatdagTimesMatdag

end module MatrixAlgebra
