
module ReduceOps

  use Kinds
  use MPIInterface
  implicit none 
  private

  public :: AllSum
  public :: AllProduct
  public :: AllMin
  public :: AllMax

  interface AllSum
     module procedure AllSumInteger
     module procedure AllSumReal
     module procedure AllSumComplex
  end interface

  interface AllProduct
     module procedure AllProductInteger
     module procedure AllProductReal
     module procedure AllProductComplex
  end interface

  interface AllMin
     module procedure AllMinInteger
     module procedure AllMinReal
  end interface

  interface AllMax
     module procedure AllMaxInteger
     module procedure AllMaxReal
  end interface

contains

  subroutine AllSumInteger(summand, sum)

    integer :: summand, sum
    integer :: mpierror

    call MPI_AllReduce(summand,sum,1,mpi_integer,MPI_SUM,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllSumInteger

  subroutine AllSumReal(summand, sum)

    real(DP) :: summand, sum
    integer :: mpierror

    call MPI_AllReduce(summand,sum,1,mpi_dp,MPI_SUM,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllSumReal

  subroutine AllSumComplex(summand, sum)

    complex(DC) :: summand, sum
    integer :: mpierror

    call MPI_AllReduce(summand,sum,1,mpi_dc,MPI_SUM,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllSumComplex

  subroutine AllProductInteger(prod_pp, prod)

    integer :: prod_pp, prod
    integer :: mpierror

    call MPI_AllReduce(prod_pp,prod,1,mpi_integer,MPI_PROD,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllProductInteger

  subroutine AllProductReal(prod_pp, prod)

    real(DP) :: prod_pp, prod
    integer :: mpierror

    call MPI_AllReduce(prod_pp,prod,1,mpi_dp,MPI_PROD,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllProductReal

  subroutine AllProductComplex(prod_pp, prod)

    complex(DC) :: prod_pp, prod
    integer :: mpierror
    
    call MPI_AllReduce(prod_pp,prod,1,mpi_dc,MPI_PROD,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)
    
  end subroutine AllProductComplex

  subroutine AllMaxInteger(local_max, global_max)

    integer :: local_max, global_max
    integer :: mpierror

    call MPI_AllReduce(local_max,global_max,1,mpi_integer,MPI_MAX,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllMaxInteger

  subroutine AllMaxReal(local_max, global_max)

    real(DP) :: local_max, global_max
    integer :: mpierror

    call MPI_AllReduce(local_max,global_max,1,mpi_dp,MPI_MAX,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllMaxReal

  subroutine AllMinInteger(local_min, global_min)

    integer :: local_min, global_min
    integer :: mpierror

    call MPI_AllReduce(local_min,global_min,1,mpi_integer,MPI_MIN,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllMinInteger

  subroutine AllMinReal(local_min, global_min)

    real(DP) :: local_min, global_min
    integer :: mpierror

    call MPI_AllReduce(local_min,global_min,1,mpi_dp,MPI_MIN,mpi_comm,mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine AllMinReal

end module ReduceOps
