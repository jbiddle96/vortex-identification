!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/fermionfieldmpicomms.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: fermionfieldmpicomms.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module FermionFieldMPIComms

  use MPIInterface
  use ColourTypes
  use Timer
  use SpinorTypes
  use FermionTypes
  use LatticeSize
  use Kinds
  implicit none
  private

  interface SendFermionField
     module procedure SendFermionField_3
     module procedure SendFermionField_4
     module procedure SendFermionField_5
     module procedure SendFermionField_6
  end interface

  interface RecvFermionField
     module procedure RecvFermionField_3
     module procedure RecvFermionField_4
     module procedure RecvFermionField_5
     module procedure RecvFermionField_6
  end interface

  interface InitSendFermionField
     module procedure InitSendFermionField_1
     module procedure InitSendFermionField_2
     module procedure InitSendFermionField_3
     module procedure InitSendFermionField_4
  end interface

  interface InitRecvFermionField
     module procedure InitRecvFermionField_1
     module procedure InitRecvFermionField_2
     module procedure InitRecvFermionField_3
     module procedure InitRecvFermionField_4
  end interface

  interface SendColourSpinMatrixField
     module procedure SendColourSpinMatrixField_1
     module procedure SendColourSpinMatrixField_2
     module procedure SendColourSpinMatrixField_3
     module procedure SendColourSpinMatrixField_4
  end interface

  interface RecvColourSpinMatrixField
     module procedure RecvColourSpinMatrixField_1
     module procedure RecvColourSpinMatrixField_2
     module procedure RecvColourSpinMatrixField_3
     module procedure RecvColourSpinMatrixField_4
  end interface

  interface SendDiracField
     module procedure SendDiracField_1
     module procedure SendDiracField_2
  end interface

  interface RecvDiracField
     module procedure RecvDiracField_1
     module procedure RecvDiracField_2
  end interface

  interface SendSpinColourMatrixField
     module procedure SendSpinColourMatrixField_1
     module procedure SendSpinColourMatrixField_2
     module procedure SendSpinColourMatrixField_3
     module procedure SendSpinColourMatrixField_4
  end interface

  interface RecvSpinColourMatrixField
     module procedure RecvSpinColourMatrixField_1
     module procedure RecvSpinColourMatrixField_2
     module procedure RecvSpinColourMatrixField_3
     module procedure RecvSpinColourMatrixField_4
  end interface

  integer, public  :: nshdwff = 0
  real(dp), public :: maxt_shdwff, mint_shdwff, t_shdwff

  public :: SendFermionField
  public :: RecvFermionField
  public :: InitSendFermionField
  public :: InitRecvFermionField
  public :: ShadowFermionField
  public :: InitSendFermionVector
  public :: InitRecvFermionVector
  public :: SendColourSpinMatrixField
  public :: RecvColourSpinMatrixField
  public :: SendSpinColourMatrixField
  public :: RecvSpinColourMatrixField
  public :: SendDiracField
  public :: RecvDiracField

  interface ShadowFermionField
     module procedure ShadowFermionField_4x
     module procedure ShadowFermionField_eo
  end interface

contains

  subroutine ShadowFermionField_4x(phi,ishdw)
    ! begin args: phi, ishdw

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: ishdw

    ! begin local_vars
    integer :: ierror, intime, outtime
    integer :: is, it, src_rank, dest_rank, nxd, nyd, nzd, ntd
    integer :: sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2), sendrecv_reqzs(nts,ns,2), ierr
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2), sendrecvzs_status(nmpi_status,nts*ns*2)
    real(dp) :: t_in, t_out

    ! begin execution

    t_in = mpi_wtime()

    nxd = size(phi,1)
    nyd = size(phi,2)
    nzd = size(phi,3)
    ntd = size(phi,4)

    select case(ishdw)
    case(1)

       if ( nprocz > 1 ) then
          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)

          do is=1,ns
             do it=1,nt
                call MPI_ISSend(phi(1,1,mapz(1)   ,it,is), nc*nxd*nyd,mpi_dc,dest_rank,it+ns*is,&
                     &                                     mpi_comm,sendrecv_reqz(it,is,1),ierr)
                call MPI_IRecv (phi(1,1,mapz(nz+1),it,is), nc*nxd*nyd, mpi_dc,  src_rank, it+ns*is, &
                     &                                     mpi_comm,sendrecv_reqz(it,is,2),ierr)
             end do
          end do

          call MPI_WaitAll(nt*ns*2,sendrecv_reqz,sendrecvz_status,ierr)
          
          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
          
          do is=1,ns
             do it=1,nt
                call MPI_ISSend(phi(1,1,mapz(nz),it,is), nc*nxd*nyd, mpi_dc, dest_rank, it+ns*is, &
                     &                                  mpi_comm, sendrecv_reqz(it,is,1), ierr)
                call MPI_IRecv (phi(1,1,mapz(0) ,it,is), nc*nxd*nyd, mpi_dc,  src_rank, it+ns*is, &
                     &                                  mpi_comm, sendrecv_reqz(it,is,2), ierr)
             end do
          end do

          call MPI_WaitAll(nt*ns*2,sendrecv_reqz,sendrecvz_status,ierr)
       end if

       if ( nproct > 1 ) then
          dest_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)

          do is=1,ns
             call MPI_ISSend(phi(1,1,1,mapt(1)   ,is), nc*nxd*nyd*nzs, mpi_dc, dest_rank, is, &
                  &                                    mpi_comm, sendrecv_reqt(is,1), ierr)
             call MPI_IRecv (phi(1,1,1,mapt(nt+1),is), nc*nxd*nyd*nzs, mpi_dc,  src_rank, is, &
                  &                                    mpi_comm, sendrecv_reqt(is,2), ierr)
          end do

          call MPI_WaitAll(nd*2,sendrecv_reqt,sendrecvt_status,ierr)

          dest_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)

          do is=1,ns
             call MPI_ISSend(phi(1,1,1,mapt(nt),is), nc*nxd*nyd*nzs, mpi_dc, dest_rank, is, &
                  &                                 mpi_comm, sendrecv_reqt(is,1), ierr)
             call MPI_IRecv (phi(1,1,1,mapt(0) ,is), nc*nxd*nyd*nzs, mpi_dc,  src_rank, is, &
                  &                                 mpi_comm, sendrecv_reqt(is,2), ierr)
          end do

          call MPI_WaitAll(ns*2,sendrecv_reqt,sendrecvt_status,ierr)
       end if
    end select

    t_out = mpi_wtime()
    call TimingUpdate(nshdwff,t_out,t_in,mint_shdwff,maxt_shdwff,t_shdwff)


  end subroutine ShadowFermionField_4x


  subroutine ShadowFermionField_eo(phi,ishdw)
    ! begin args: phi, ishdw

    type(dirac_fermion), dimension(:) :: phi
    integer :: ishdw

    ! begin local_vars
    integer :: ierror, intime, outtime
    integer :: it, src_rank, dest_rank, nxd, nyd, nzd, ntd
    integer :: sendrecv_reqz(nt,2),sendrecv_reqt(2), sendrecv_reqzs(nts,2), ierr
    integer :: sendrecvz_status(nmpi_status,nt*2), sendrecvt_status(nmpi_status,2), sendrecvzs_status(nmpi_status,nts*2)
    real(dp) :: t_in, t_out
    integer :: i_xeo, j_xeo

    ! begin execution

    nxd = nx
    nyd = ny
    nzd = nz
    ntd = nt

    select case(ishdw)
    case(1)

       if ( nprocz > 1 ) then
          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)

          ! Get the forward shadow.
          ! # of sites = (nx*ny*/2)*nt (even field or odd field)
          do it=1,nt
             i_xeo = x2eo((/1,1,1,it/))
             j_xeo = x2eo((/1,1,nz+1,it/))

             call MPI_ISSend(phi(i_xeo), nc*ns*(nx*ny)/2, mpi_dc,dest_rank,it,mpi_comm,sendrecv_reqz(it,1),ierr)
             call MPI_IRecv (phi(j_xeo), nc*ns*(nx*ny)/2, mpi_dc, src_rank,it,mpi_comm,sendrecv_reqz(it,2),ierr)
          end do

          call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
          
          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
          
          ! Get the backward shadow.
          ! # of sites = (nx*ny*/2)*nt (even field or odd field)
          do it=1,nt
             i_xeo = x2eo((/1,1,nz,it/))
             j_xeo = x2eo((/1,1,0,it/))

             call MPI_ISSend(phi(i_xeo), nc*ns*(nx*ny)/2, mpi_dc,dest_rank,it,mpi_comm,sendrecv_reqz(it,1),ierr)
             call MPI_IRecv (phi(j_xeo), nc*ns*(nx*ny)/2, mpi_dc, src_rank,it,mpi_comm,sendrecv_reqz(it,2),ierr)
          end do

          call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)

          nzd = nz + 2*ishdw
       end if

       if ( nproct > 1 ) then
          dest_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)

          ! Get the forward shadow.
          i_xeo = x2eo((/1,1,1,1/))
          j_xeo = x2eo((/1,1,1,nt+1/))

          ! # of sites = nx*ny*nz/2 (even field or odd field)
          call MPI_ISSend(phi(i_xeo), nc*ns*(nx*ny*nz)/2, mpi_dc, dest_rank, 1, mpi_comm, sendrecv_reqt(1), ierr)
          call MPI_IRecv (phi(j_xeo), nc*ns*(nx*ny*nz)/2, mpi_dc,  src_rank, 1, mpi_comm, sendrecv_reqt(2), ierr)

          call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)

          if ( nprocz > 1 ) then
             i_xeo = x2eo((/1,1,nz+1,1/))
             j_xeo = x2eo((/1,1,nz+1,nt+1/))

             ! Get the forward shadow "corner".
             ! # of sites = nx*ny*2/2 (even field or odd field)
             call MPI_ISSend(phi(i_xeo), nc*ns*nxd*nyd, mpi_dc, dest_rank, 1, mpi_comm, sendrecv_reqt(1), ierr)
             call MPI_IRecv (phi(j_xeo), nc*ns*nxd*nyd, mpi_dc,  src_rank, 1, mpi_comm, sendrecv_reqt(2), ierr)

             call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
          end if

          dest_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)

          ! Get the backward shadow.
          i_xeo = x2eo((/1,1,1,nt/))
          j_xeo = x2eo((/1,1,1,0/))

          call MPI_ISSend(phi(i_xeo), nc*ns*(nx*ny*nz)/2, mpi_dc, dest_rank, 1, mpi_comm, sendrecv_reqt(1), ierr)
          call MPI_IRecv (phi(j_xeo), nc*ns*(nx*ny*nz)/2, mpi_dc,  src_rank, 1, mpi_comm, sendrecv_reqt(2), ierr)

          call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)

          if ( nprocz > 1 ) then
             i_xeo = x2eo((/1,1,nz+1,nt/))
             j_xeo = x2eo((/1,1,nz+1,0/))

             call MPI_ISSend(phi(i_xeo), nc*ns*nxd*nyd, mpi_dc, dest_rank, 1, mpi_comm, sendrecv_reqt(1), ierr)
             call MPI_IRecv (phi(j_xeo), nc*ns*nxd*nyd, mpi_dc,  src_rank, 1, mpi_comm, sendrecv_reqt(2), ierr)

             call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
          end if

          ntd = nz + 2*ishdw
       end if

    end select

  end subroutine ShadowFermionField_eo
  
  subroutine InitSendFermionVector(phi, dest_rank, request)
    ! begin args: phi, dest_rank, request

    type(colour_vector) :: phi
    integer :: dest_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = 1

    call MPI_SSend_Init(phi, nc, mpi_dc, dest_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitSendFermionVector

  subroutine InitRecvFermionVector(phi, src_rank, request)
    ! begin args: phi, src_rank, request

    type(colour_vector) :: phi
    integer :: src_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = 1

    call MPI_Recv_Init(phi, nc, mpi_dc, src_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitRecvFermionVector

  subroutine InitSendFermionField_1(phi, dest_rank, request)
    ! begin args: phi, dest_rank, request

    type(colour_vector),  dimension(:) :: phi
    integer :: dest_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend_Init(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitSendFermionField_1

  subroutine InitRecvFermionField_1(phi, src_rank, request)
    ! begin args: phi, src_rank, request

    type(colour_vector),  dimension(:) :: phi
    integer :: src_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_Recv_Init(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitRecvFermionField_1

  subroutine InitSendFermionField_2(phi, dest_rank, request)
    ! begin args: phi, dest_rank, request

    type(colour_vector),  dimension(:,:) :: phi
    integer :: dest_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend_Init(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitSendFermionField_2

  subroutine InitRecvFermionField_2(phi, src_rank, request)
    ! begin args: phi, src_rank, request

    type(colour_vector),  dimension(:,:) :: phi
    integer :: src_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_Recv_Init(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitRecvFermionField_2

  subroutine InitSendFermionField_3(phi, dest_rank, request)
    ! begin args: phi, dest_rank, request

    type(colour_vector),  dimension(:,:,:) :: phi
    integer :: dest_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend_Init(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitSendFermionField_3

  subroutine InitRecvFermionField_3(phi, src_rank, request)
    ! begin args: phi, src_rank, request

    type(colour_vector),  dimension(:,:,:) :: phi
    integer :: src_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_Recv_Init(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitRecvFermionField_3

  subroutine InitSendFermionField_4(phi, dest_rank, request)
    ! begin args: phi, dest_rank, request

    type(colour_vector), dimension(:,:,:,:) :: phi
    integer :: dest_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend_Init(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitSendFermionField_4

  subroutine InitRecvFermionField_4(phi, src_rank, request)
    ! begin args: phi, src_rank, request

    type(colour_vector), dimension(:,:,:,:) :: phi
    integer :: src_rank, request
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_Recv_Init(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine InitRecvFermionField_4

  subroutine SendFermionField_3(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(colour_vector),  dimension(:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendFermionField_3

  subroutine RecvFermionField_3(phi, src_rank)
    ! begin args: phi, src_rank

    type(colour_vector),  dimension(:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvFermionField_3

  subroutine SendFermionField_4(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(colour_vector), dimension(:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendFermionField_4

  subroutine RecvFermionField_4(phi, src_rank)
    ! begin args: phi, src_rank

    type(colour_vector), dimension(:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvFermionField_4

  subroutine SendFermionField_5(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendFermionField_5

  subroutine RecvFermionField_5(phi, src_rank)
    ! begin args: phi, src_rank

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvFermionField_5

  subroutine SendFermionField_6(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(colour_vector), dimension(:,:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendFermionField_6

  subroutine RecvFermionField_6(phi, src_rank)
    ! begin args: phi, src_rank

    type(colour_vector), dimension(:,:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvFermionField_6

  subroutine SendColourSpinMatrixField_1(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(colour_spin_matrix), dimension(:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc*nc*ns*ns, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendColourSpinMatrixField_1

  subroutine RecvColourSpinMatrixField_1(phi, src_rank)
    ! begin args: phi, src_rank

    type(colour_spin_matrix), dimension(:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc*nc*ns*ns, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvColourSpinMatrixField_1

  subroutine SendColourSpinMatrixField_2(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(colour_spin_matrix), dimension(:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc*nc*ns*ns, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendColourSpinMatrixField_2

  subroutine RecvColourSpinMatrixField_2(phi, src_rank)
    ! begin args: phi, src_rank

    type(colour_spin_matrix), dimension(:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc*nc*ns*ns, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvColourSpinMatrixField_2

  subroutine SendColourSpinMatrixField_3(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(colour_spin_matrix), dimension(:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc*nc*ns*ns, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendColourSpinMatrixField_3

  subroutine RecvColourSpinMatrixField_3(phi, src_rank)
    ! begin args: phi, src_rank

    type(colour_spin_matrix), dimension(:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc*nc*ns*ns, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvColourSpinMatrixField_3

  subroutine SendColourSpinMatrixField_4(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(colour_spin_matrix), dimension(:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc*nc*ns*ns, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendColourSpinMatrixField_4

  subroutine RecvColourSpinMatrixField_4(phi, src_rank)
    ! begin args: phi, src_rank

    type(colour_spin_matrix), dimension(:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc*nc*ns*ns, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvColourSpinMatrixField_4

  subroutine SendDiracField_1(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(dirac_fermion), dimension(:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc*ns, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendDiracField_1

  subroutine RecvDiracField_1(phi, src_rank)
    ! begin args: phi, src_rank

    type(dirac_fermion), dimension(:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc*ns, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvDiracField_1

  subroutine SendDiracField_2(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(dirac_fermion), dimension(:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*nc*ns, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendDiracField_2

  subroutine RecvDiracField_2(phi, src_rank)
    ! begin args: phi, src_rank

    type(dirac_fermion), dimension(:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*nc*ns, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvDiracField_2

  subroutine SendSpinColourMatrixField_1(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(spin_colour_matrix), dimension(:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*ns*ns*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendSpinColourMatrixField_1

  subroutine RecvSpinColourMatrixField_1(phi, src_rank)
    ! begin args: phi, src_rank

    type(spin_colour_matrix), dimension(:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*ns*ns*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvSpinColourMatrixField_1

  subroutine SendSpinColourMatrixField_2(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(spin_colour_matrix), dimension(:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*ns*ns*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendSpinColourMatrixField_2

  subroutine RecvSpinColourMatrixField_2(phi, src_rank)
    ! begin args: phi, src_rank

    type(spin_colour_matrix), dimension(:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*ns*ns*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvSpinColourMatrixField_2

  subroutine SendSpinColourMatrixField_3(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(spin_colour_matrix), dimension(:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*ns*ns*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendSpinColourMatrixField_3

  subroutine RecvSpinColourMatrixField_3(phi, src_rank)
    ! begin args: phi, src_rank

    type(spin_colour_matrix), dimension(:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*ns*ns*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvSpinColourMatrixField_3

  subroutine SendSpinColourMatrixField_4(phi, dest_rank)
    ! begin args: phi, dest_rank

    type(spin_colour_matrix), dimension(:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag
    ! begin execution

    tag = size(phi)

    call MPI_SSend(phi, size(phi)*ns*ns*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendSpinColourMatrixField_4

  subroutine RecvSpinColourMatrixField_4(phi, src_rank)
    ! begin args: phi, src_rank

    type(spin_colour_matrix), dimension(:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag
    integer, dimension(nmpi_status) :: mpi_status
    ! begin execution

    tag = size(phi)

    call MPI_Recv(phi, size(phi)*ns*ns*nc*nc, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvSpinColourMatrixField_4

end module FermionFieldMPIComms

