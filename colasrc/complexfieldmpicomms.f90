!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/complexfieldmpicomms.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: complexfieldmpicomms.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :


module ComplexFieldMPIComms

  use MPIInterface
  implicit none
  private

  interface SendComplexField
     module procedure SendComplexField_sc_1
     module procedure SendComplexField_sc_2
     module procedure SendComplexField_sc_3
     module procedure SendComplexField_sc_4
     module procedure SendComplexField_sc_5
     module procedure SendComplexField_sc_6
     module procedure SendComplexField_sc_7
     module procedure SendComplexField_dc_1
     module procedure SendComplexField_dc_2
     module procedure SendComplexField_dc_3
     module procedure SendComplexField_dc_4
     module procedure SendComplexField_dc_5
     module procedure SendComplexField_dc_6
     module procedure SendComplexField_dc_7
  end interface

  interface RecvComplexField
     module procedure RecvComplexField_sc_1
     module procedure RecvComplexField_sc_2
     module procedure RecvComplexField_sc_3
     module procedure RecvComplexField_sc_4
     module procedure RecvComplexField_sc_5
     module procedure RecvComplexField_sc_6
     module procedure RecvComplexField_sc_7
     module procedure RecvComplexField_dc_1
     module procedure RecvComplexField_dc_2
     module procedure RecvComplexField_dc_3
     module procedure RecvComplexField_dc_4
     module procedure RecvComplexField_dc_5
     module procedure RecvComplexField_dc_6
     module procedure RecvComplexField_dc_7
  end interface

  public :: SendComplexField
  public :: RecvComplexField

contains

  subroutine SendComplexField_dc_1(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(dc), dimension(:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror
    integer :: tag, nphi

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_dc_1

  subroutine RecvComplexField_dc_1(phi, src_rank)
    ! begin args: phi, src_rank

    complex(dc), dimension(:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_dc_1

  subroutine SendComplexField_dc_2(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(dc), dimension(:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_dc_2

  subroutine RecvComplexField_dc_2(phi, src_rank)
    ! begin args: phi, src_rank

    complex(dc), dimension(:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_dc_2

  subroutine SendComplexField_dc_3(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(dc), dimension(:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_dc_3

  subroutine RecvComplexField_dc_3(phi, src_rank)
    ! begin args: phi, src_rank

    complex(dc), dimension(:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_dc_3

  subroutine SendComplexField_dc_4(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(dc), dimension(:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_dc_4

  subroutine RecvComplexField_dc_4(phi, src_rank)
    ! begin args: phi, src_rank

    complex(dc), dimension(:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_dc_4

  subroutine SendComplexField_dc_5(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(dc), dimension(:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_dc_5

  subroutine RecvComplexField_dc_5(phi, src_rank)
    ! begin args: phi, src_rank

    complex(dc), dimension(:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_dc_5

  subroutine SendComplexField_dc_6(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(dc), dimension(:,:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_dc_6

  subroutine RecvComplexField_dc_6(phi, src_rank)
    ! begin args: phi, src_rank

    complex(dc), dimension(:,:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_dc_6

  subroutine SendComplexField_dc_7(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(dc), dimension(:,:,:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_dc_7

  subroutine RecvComplexField_dc_7(phi, src_rank)
    ! begin args: phi, src_rank

    complex(dc), dimension(:,:,:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_dc_7

  subroutine SendComplexField_sc_1(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(sc), dimension(:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror
    integer :: tag, nphi

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_sc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_sc_1

  subroutine RecvComplexField_sc_1(phi, src_rank)
    ! begin args: phi, src_rank

    complex(sc), dimension(:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_sc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_sc_1

  subroutine SendComplexField_sc_2(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(sc), dimension(:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_sc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_sc_2

  subroutine RecvComplexField_sc_2(phi, src_rank)
    ! begin args: phi, src_rank

    complex(sc), dimension(:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_sc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_sc_2

  subroutine SendComplexField_sc_3(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(sc), dimension(:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_sc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_sc_3

  subroutine RecvComplexField_sc_3(phi, src_rank)
    ! begin args: phi, src_rank

    complex(sc), dimension(:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_sc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_sc_3

  subroutine SendComplexField_sc_4(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(sc), dimension(:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_sc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_sc_4

  subroutine RecvComplexField_sc_4(phi, src_rank)
    ! begin args: phi, src_rank

    complex(sc), dimension(:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_sc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_sc_4

  subroutine SendComplexField_sc_5(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(sc), dimension(:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_sc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_sc_5

  subroutine RecvComplexField_sc_5(phi, src_rank)
    ! begin args: phi, src_rank

    complex(sc), dimension(:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_sc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_sc_5

  subroutine SendComplexField_sc_6(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(sc), dimension(:,:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_sc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_sc_6

  subroutine RecvComplexField_sc_6(phi, src_rank)
    ! begin args: phi, src_rank

    complex(sc), dimension(:,:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_sc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_sc_6

  subroutine SendComplexField_sc_7(phi, dest_rank)
    ! begin args: phi, dest_rank

    complex(sc), dimension(:,:,:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_sc, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendComplexField_sc_7

  subroutine RecvComplexField_sc_7(phi, src_rank)
    ! begin args: phi, src_rank

    complex(sc), dimension(:,:,:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_sc, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvComplexField_sc_7

end module ComplexFieldMPIComms

