#include "defines.h"

module FermionFieldIO
  use Kinds
  use LatticeSize
  use MPIInterface
  use ColourTypes
  use SpinorTypes
  use FermionTypes
  use ComplexFieldMPIComms
  use FermionFieldMPIComms
  use FermionAction
  use CollectiveOps
  use QuarkPropTypes
  use GaugeField
  use ColourFieldOps
  implicit none
  private

  public :: WriteFermionField_bin
  public :: ReadFermionField_bin
  public :: WriteFermionField_seq
  public :: ReadFermionField_seq
  public :: WriteFermionField_eo
  public :: ReadFermionField_eo
  public :: WriteFermionMatrixEigenVectors
  public :: ReadFermionMatrixEigenVectors
  public :: WriteFermionMatrixEigenVectors_split
  public :: ReadFermionMatrixEigenVectors_split
  public :: FermionFieldToFMEV
  public :: FMEVToFermionField
  public :: OpenIFMSFile_eo
  public :: CloseIFMSFile
  public :: WriteIFMSVector_eo
  public :: ReadIFMSVector_eo
  public :: OpenFermionFieldVectorFile
  public :: CloseFermionFieldVectorFile
  public :: WriteFermionField
  public :: ReadFermionField
  public :: OpenSolutionVectorFile
  public :: CloseSolutionVectorFile
  public :: ReadSolutionVector
  public :: WriteSplitFermionField
  public :: ReadSplitFermionField
  public :: WriteVectorSpace
  public :: ReadVectorSpace
  public :: WriteEigenSpace
  public :: ReadEigenSpace
  public :: ReadFermionField_sc
  public :: WriteTimeSlicePropagator
  public :: ReadTimeSlicePropagator
  public :: WriteSplitTimeSlicePropagator
  public :: ReadSplitTimeSlicePropagator

  interface WriteFermionField
     module procedure WriteFermionField_bin
     module procedure WriteFermionField_seq 
  end interface

  interface ReadFermionField
     module procedure ReadFermionField_bin
     module procedure ReadFermionField_seq
  end interface

  interface WriteVectorSpace
     module procedure WriteVectorSpace_4x
     module procedure WriteVectorSpace_eo
  end interface

  interface ReadVectorSpace
     module procedure ReadVectorSpace_4x
     module procedure ReadVectorSpace_eo
  end interface

  interface WriteEigenSpace
     module procedure WriteEigenSpace_4x
  end interface

  interface ReadEigenSpace
     module procedure ReadEigenSpace_4x
  end interface

  ! SolutionVectorFile state variables.
  type(dirac_fermion), dimension(:), allocatable :: sv_chi_e, sv_chi_o
  logical :: sv_init_eo = .false.
  logical :: sv_file_open = .false.
  logical :: sv_parallel_io = .false.
  character(len=32) :: sv_FileFormat
  type(IFMSMetaDataType) :: sv_ifms

contains

  subroutine WriteFermionField_bin(phi,OutFileId,irec)
    ! begin args: phi, OutFileId, irec
    

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer, intent(in) :: OutFileId
    integer, intent(in) :: irec ! Starting record position
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: chi_out
    integer :: irank, is, ic
    integer :: ix, jx, iy, jy, iz, jz, it, jt

    ! begin execution
    allocate(chi(nx,ny,nz,nt,ns))

    if ( i_am_root ) then
       allocate(chi_out(nc,ns,nlx,nly,nlz,nlt))
       
       do irank=0,mpi_size-1
          if ( irank == mpi_root_rank ) then
             chi = phi(1:nx,1:ny,1:nz,1:nt,:)
          else
             call RecvFermionField(chi,irank)
          end if

          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do is=1,ns; do ic=1,nc
             chi_out(ic,is,ix:jx,iy:jy,iz:jz,it:jt) = chi(:,:,:,:,is)%Cl(ic)
          end do; end do
       end do

       write(OutFileId,rec=irec) chi_out

       deallocate(chi_out)
    else
       chi = phi(1:nx,1:ny,1:nz,1:nt,:)
       call SendFermionField(chi,mpi_root_rank)
    end if

    deallocate(chi)

  end subroutine WriteFermionField_bin

  subroutine ReadFermionField_bin(phi,InFileId,irec)
    ! begin args: phi, InFileId

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer, intent(in) :: InFileId
    integer, intent(in) :: irec ! Starting record position, returned value is next starting position, or EOF.
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: chi_in
    integer :: irank, is, ic
    integer :: ix, jx, iy, jy, iz, jz, it, jt

    ! begin execution
    allocate(chi(nx,ny,nz,nt,ns))

    if ( i_am_root ) then
       allocate(chi_in(nc,ns,nlx,nly,nlz,nlt))

       read(InFileId,rec=irec) chi_in

       do irank=0,mpi_size-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do is=1,ns; do ic=1,nc
             chi(:,:,:,:,is)%Cl(ic) = chi_in(ic,is,ix:jx,iy:jy,iz:jz,it:jt)
          end do; end do

          if ( irank == mpi_root_rank ) then
             phi(1:nx,1:ny,1:nz,1:nt,:) = chi
          else
             call SendFermionField(chi,irank)
          end if
       end do

       deallocate(chi_in)

    else
       call RecvFermionField(chi,mpi_root_rank)
       phi(1:nx,1:ny,1:nz,1:nt,:) = chi
    end if

    deallocate(chi)

  end subroutine ReadFermionField_bin

  subroutine WriteFermionField_seq(phi,OutFileId)
    ! begin args: phi, OutFileId

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: OutFileId
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: chi_out
    integer :: irank, is, ic
    integer :: ix, jx, iy, jy, iz, jz, it, jt

    ! begin execution
    allocate(chi(nx,ny,nz,nt,ns))

    if ( i_am_root ) then
       allocate(chi_out(nlx,nly,nlz,nlt,ns,nc))

       do irank=0,mpi_size-1
          if ( irank == mpi_root_rank ) then
             chi = phi(1:nx,1:ny,1:nz,1:nt,:)
          else
             call RecvFermionField(chi,irank)
          end if

          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do is=1,ns; do ic=1,nc
             chi_out(ix:jx,iy:jy,iz:jz,it:jt,is,ic) = chi(:,:,:,:,is)%Cl(ic)
          end do; end do
       end do

       write(OutFileId) chi_out(:,:,:,:,:,:)

       deallocate(chi_out)
    else
       chi = phi(1:nx,1:ny,1:nz,1:nt,:)
       call SendFermionField(chi,mpi_root_rank)
    end if

    deallocate(chi)

  end subroutine WriteFermionField_seq

  subroutine ReadFermionField_seq(phi,InFileId)
    ! begin args: phi, InFileId

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: InFileId
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: chi_in
    integer :: irank, is, ic
    integer :: ix, jx, iy, jy, iz, jz, it, jt

    ! begin execution
    allocate(chi(nx,ny,nz,nt,ns))

    if ( i_am_root ) then
       allocate(chi_in(nlx,nly,nlz,nlt,ns,nc))

       read(InFileId) chi_in(:,:,:,:,:,:)

       do irank=0,mpi_size-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do is=1,ns; do ic=1,nc
             chi(:,:,:,:,is)%Cl(ic) = chi_in(ix:jx,iy:jy,iz:jz,it:jt,is,ic)
          end do; end do

          if ( irank == mpi_root_rank ) then
             phi(1:nx,1:ny,1:nz,1:nt,:) = chi
          else
             call SendFermionField(chi,irank)
          end if
       end do

       deallocate(chi_in)

    else
       call RecvFermionField(chi,mpi_root_rank)
       phi(1:nx,1:ny,1:nz,1:nt,:) = chi
    end if

    deallocate(chi)

  end subroutine ReadFermionField_seq

  subroutine WriteFermionField_eo(phi,OutFileId,irec)
    ! begin args: phi, OutFileId, irec

    type(dirac_fermion), dimension(:) :: phi
    integer :: OutFileId
    integer :: irec ! Starting record position, returned value is next starting position, or EOF.
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: chi
    complex(dc), dimension(:,:,:), allocatable :: chi_out
    integer :: irank, i_xeo
    integer :: i_lxeo, j_lxeo

    ! begin execution
    allocate(chi(n_xeo))

    if ( i_am_root ) then
       allocate(chi_out(nc,ns,nlattice_eo))
       
       do irank=0,mpi_size-1
          if ( irank == mpi_root_rank ) then
             chi = phi(1:n_xeo)
          else
             call RecvDiracField(chi,irank)
          end if

          call GetSubLattice_eo(irank,i_lxeo,j_lxeo)
          do i_xeo=1,n_xeo
             chi_out(:,:,i_lxeo+i_xeo-1) = chi(i_xeo)%cs(:,:)
          end do
       end do

       write(OutFileId,rec=irec) chi_out
       irec = irec + 1

       deallocate(chi_out)
    else
       chi = phi(1:n_xeo)
       call SendDiracField(chi,mpi_root_rank)
    end if

    deallocate(chi)

  end subroutine WriteFermionField_eo

  subroutine ReadFermionField_eo(phi,InFileId,irec)
    ! begin args: phi, InFileId

    type(dirac_fermion), dimension(:) :: phi
    integer :: InFileId
    integer :: irec ! Starting record position, returned value is next starting position, or EOF.
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: chi
    complex(dc), dimension(:,:,:), allocatable :: chi_in
    integer :: irank, i_xeo
    integer :: i_lxeo, j_lxeo

    ! begin execution
    allocate(chi(n_xeo))

    if ( i_am_root ) then
       allocate(chi_in(nc,ns,nlattice_eo))

       read(InFileId,rec=irec) chi_in
       irec = irec + 1

       do irank=0,mpi_size-1
          call GetSubLattice_eo(irank,i_lxeo,j_lxeo)
          do i_xeo=1,n_xeo
             chi(i_xeo)%cs(:,:) = chi_in(:,:,i_lxeo+i_xeo-1)
          end do

          if ( irank == mpi_root_rank ) then
             phi(1:n_xeo) = chi
          else
             call SendDiracField(chi,irank)
          end if
       end do

       deallocate(chi_in)

    else
       call RecvDiracField(chi,mpi_root_rank)
       phi(1:n_xeo) = chi
    end if

    deallocate(chi)

  end subroutine ReadFermionField_eo

  subroutine WriteSplitFermionField(phi,OutFileId)
    ! begin args: phi, OutFileId

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: OutFileId
    
    ! begin execution

    write(OutFileId) phi(1:nx,1:ny,1:nz,1:nt,:)

    call MPIBarrier
    
  end subroutine WriteSplitFermionField

  subroutine ReadSplitFermionField(phi,InFileId)
    ! begin args: phi, InFileId

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: InFileId

    ! begin execution

    read(InFileId) phi(1:nx,1:ny,1:nz,1:nt,:)
    
    call MPIBarrier

  end subroutine ReadSplitFermionField

  subroutine WriteFermionMatrixEigenVectors(OutputFile,FMEV)

    character(len=*) :: OutputFile
    type(dirac_fermion), dimension(:,:) :: FMEV

    ! begin local_vars
    character(len=273) :: FMEVFile
    character(len=6) :: ProcSuffix

    integer :: irec, jrec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: OutFileID
    integer :: ic,is ! Loop variables
    integer :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)
    integer :: i_x, i_v, n_x, n_v

    complex(dc), dimension(:,:,:,:), allocatable :: chi

    n_x = nx*ny*nz*nt
    n_v = size(FMEV,dim=2)
    allocate(chi(nc,ns,n_x,n_v))

    FMEVFile = trim(OutputFile)

    matrix_len = 16*nc*ns !complex type=8 bytes

    OutfileId = 201

    irecl = matrix_len*n_x

    call AssertSublatticeIsContiguous()

    if ( i_am_root ) then
       open(OutFileId,file=FMEVFile,form='unformatted',access='direct',status='replace',action='write',recl=irecl)
       jrec = 1
       do iproct=1,nproct
          do iprocz=1,nprocz
             do iprocy=1,nprocy
                coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                irank = coords_to_rank(coords)

                if ( irank /= mpi_root_rank ) then
                   call RecvComplexField(chi, irank)
                else
                   do i_v=1,n_v; do i_x=1,n_x
                      chi(:,:,i_x,i_v) = FMEV(i_x,i_v)%cs(:,:)
                   end do; end do
                end if

                ! Write the sub-lattice propagator.
                do i_v=1,n_v
                   irec = jrec + (i_v-1)*nproc
                   write(OutFileID,rec=irec) chi(:,:,:,i_v)
                end do

                jrec = jrec + 1
             end do
          end do
       end do
       close(OutFileId)

    else
       do i_v=1,n_v; do i_x=1,n_x
          chi(:,:,i_x,i_v) = FMEV(i_x,i_v)%cs(:,:)
       end do; end do
       call SendComplexField(chi,mpi_root_rank)
    end if

    deallocate(chi)

  end subroutine WriteFermionMatrixEigenVectors

  subroutine ReadFermionMatrixEigenVectors(InputFile,FMEV)

    character(len=*) :: InputFile
    type(dirac_fermion), dimension(:,:) :: FMEV

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: FMEVFile
    character(len=6) :: ProcSuffix

    integer :: irec, jrec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: InFileID
    integer :: i_x, i_v, n_x, n_v
    integer :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)

    complex(dc), dimension(:,:,:,:), allocatable :: chi

    n_x = nx*ny*nz*nt
    n_v = size(FMEV,dim=2)
    allocate(chi(nc,ns,n_x,n_v))

    FMEVFile = trim(InputFile)

    matrix_len = 16*nc*ns !complex type=8 bytes

    InFileId = 201

    irecl = matrix_len*n_x
    call AssertSublatticeIsContiguous()

    if ( i_am_root ) then

       open(InFileID,file=FMEVFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)

       jrec = 1
       do iproct=1,nproct
          do iprocz=1,nprocz
             do iprocy=1,nprocy
                coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                irank = coords_to_rank(coords)
                ! Read in the sub-lattice propagator.
                do i_v=1,n_v
                   irec = jrec + (i_v-1)*nproc
                   read(InFileID,rec=irec) chi(:,:,:,i_v)
                end do

                if ( irank /= mpi_root_rank ) then
                   call SendComplexField(chi, irank)
                else
                   do i_v=1,n_v; do i_x=1,n_x
                      FMEV(i_x,i_v)%cs(:,:) = chi(:,:,i_x,i_v)
                   end do; end do
                end if

                jrec = jrec + 1
             end do
          end do
       end do

       close(InFileId)
    else
       call RecvComplexField(chi,mpi_root_rank)
       do i_v=1,n_v; do i_x=1,n_x
          FMEV(i_x,i_v)%cs(:,:) = chi(:,:,i_x,i_v)
       end do; end do
    end if

    deallocate(chi)

  end subroutine ReadFermionMatrixEigenVectors

  subroutine WriteFermionMatrixEigenVectors_split(OutputFile,FMEV)

    character(len=*) :: OutputFile
    type(dirac_fermion), dimension(:,:) :: FMEV

    ! begin local_vars
    character(len=273) :: FMEVFile
    character(len=6) :: ProcSuffix

    integer :: irec, jrec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: OutFileID
    integer :: ic,is ! Loop variables
    integer :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)
    integer :: i_x, i_v, n_x, n_v

    complex(dc), dimension(:,:,:,:), allocatable :: chi

    n_x = nx*ny*nz*nt
    n_v = size(FMEV,dim=2)
    allocate(chi(nc,ns,n_x,n_v))

    FMEVFile = trim(OutputFile)

    matrix_len = 16*nc*ns !complex type=8 bytes

    OutfileId = 201

    irecl = matrix_len*n_x*n_v
    do i_v=1,n_v; do i_x=1,n_x
       chi(:,:,i_x,i_v) = FMEV(i_x,i_v)%cs(:,:)
    end do; end do

    write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
    FMEVFile = trim(FMEVFile) // ProcSuffix
    OutFileId = OutFileId + mpi_rank

    !Write out my portion of the propagator as a single record, for better IO performance.

    open(OutFileId,file=FMEVFile,form='unformatted',access='direct',status='replace',action='write',recl=irecl)
    irec = 1
    write(OutFileId,rec=irec) chi
    close(OutFileId)

    deallocate(chi)

  end subroutine WriteFermionMatrixEigenVectors_split

  subroutine ReadFermionMatrixEigenVectors_split(InputFile,FMEV)

    character(len=*) :: InputFile
    type(dirac_fermion), dimension(:,:) :: FMEV

    ! begin local_vars
    character(len=_FILENAME_LEN_) :: FMEVFile
    character(len=6) :: ProcSuffix

    integer :: irec, jrec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: InFileID
    integer :: i_x, i_v, n_x, n_v
    integer :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)

    complex(dc), dimension(:,:,:,:), allocatable :: chi

    n_x = nx*ny*nz*nt
    n_v = size(FMEV,dim=2)
    allocate(chi(nc,ns,n_x,n_v))

    FMEVFile = trim(InputFile)

    matrix_len = 16*nc*ns !complex type=8 bytes

    InFileId = 201

    irecl = matrix_len*n_x*n_v

    write( ProcSuffix, '(a,I5.5)' ) ".",mpi_rank
    FMEVFile = trim(FMEVFile) // ProcSuffix
    InFileId = InFileId + mpi_rank

    !Read my portion of the propagator as a single record, for better IO performance.
    open(InFileId,file=FMEVFile,form='unformatted',access='direct',status='old',action='read',recl=irecl)
    irec = 1
    read(InFileId,rec=irec) chi
    close(InFileId)

    do i_v=1,n_v; do i_x=1,n_x
       FMEV(i_x,i_v)%cs(:,:) = chi(:,:,i_x,i_v)
    end do; end do

    deallocate(chi)

  end subroutine ReadFermionMatrixEigenVectors_split

  subroutine FermionFieldToFMEV(v_i,FMEV)
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i
    type(dirac_fermion), dimension(:,:) :: FMEV

    integer :: ix,iy,iz,it,is,i_v,i_x
    integer :: n_v

    n_v = size(FMEV,2)

    do i_v=1,n_v
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = ix + (iy-1)*nx + (iz-1)*nx*ny + (it-1)*nx*ny*nz
          do is=1,ns
             FMEV(i_x,i_v)%cs(:,is) = v_i(ix,iy,iz,it,is,i_v)%cl(:)
          end do
       end do; end do; end do; end do
    end do
       
  end subroutine FermionFieldToFMEV

  subroutine FMEVToFermionField(FMEV,v_i)
    type(dirac_fermion), dimension(:,:) :: FMEV
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i

    integer :: ix,iy,iz,it,is,i_v,i_x
    integer :: n_v

    n_v = size(FMEV,2)

    do i_v=1,n_v
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = ix + (iy-1)*nx + (iz-1)*nx*ny + (it-1)*nx*ny*nz
          do is=1,ns
             v_i(ix,iy,iz,it,is,i_v)%cl(:) = FMEV(i_x,i_v)%cs(:,is)
          end do
       end do; end do; end do; end do
    end do
       
  end subroutine FMEVToFermionField

  subroutine OpenIFMSFile_eo(filename,FileID,action,parallel_io)
    character(len=*) :: filename
    integer, intent(inout) :: FileID
    character(len=*) :: action
    logical :: parallel_io

    ! begin local_vars
    character(len=6) :: Suffix
    integer :: matrix_len, irecl

    matrix_len = 8*nc*ns !complex type=8 bytes
    irecl = matrix_len*n_xeo

    if ( parallel_io ) then
       FileId = FileId + mpi_rank
       write( Suffix, '(a,I5.5)' ) ".",mpi_rank
       open(FileId,file=trim(filename)//trim(Suffix),form='unformatted',access='direct',action=action,recl=irecl)
    else
       call AssertSublatticeIsContiguous()

       if (i_am_root) then
          open(FileId,file=filename,form='unformatted',access='direct',action=action,recl=irecl)
       end if
    end if

  end subroutine OpenIFMSFile_eo

  subroutine CloseIFMSFile(FileID,parallel_io)
    integer :: FileID
    logical :: parallel_io

    if ( parallel_io .or. i_am_root ) then
       close(FileId)
    end if

  end subroutine CloseIFMSFile

  subroutine WriteIFMSVector_eo(FileID,chi_o,i_v,parallel_io)
    integer :: FileID
    type(dirac_fermion), dimension(:) :: chi_o
    integer :: i_v
    logical :: parallel_io

    ! begin local_vars
    integer :: irec, jrec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: i_xeo,ic,is ! Loop variables
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)

    complex(sc), dimension(:,:,:), allocatable :: chi

    allocate(chi(nc,ns,n_xeo))

    do i_xeo=1,n_xeo
       chi(:,:,i_xeo) = chi_o(i_xeo)%cs(:,:)
    end do

    if ( parallel_io ) then
       !Write out my portion of the field as a single record, for better IO performance.
       irec = i_v
       write(FileId,rec=irec) chi
    else
       if ( i_am_root ) then
          jrec = 1
          do iproct=1,nproct
             do iprocz=1,nprocz
                do iprocy=1,nprocy
                   coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                   irank = coords_to_rank(coords)

                   if ( irank /= mpi_root_rank ) then
                      call RecvComplexField(chi, irank)
                   else
                      do i_xeo=1,n_xeo
                         chi(:,:,i_xeo) = chi_o(i_xeo)%cs(:,:)
                      end do
                   end if

                   ! Write the sub-lattice field.
                   irec = jrec + (i_v-1)*nproc
                   write(FileID,rec=irec) chi(:,:,:)
                   
                   jrec = jrec + 1
                end do
             end do
          end do

       else
          do i_xeo=1,n_xeo
             chi(:,:,i_xeo) = chi_o(i_xeo)%cs(:,:)
          end do
          call SendComplexField(chi,mpi_root_rank)
       end if
    end if

    deallocate(chi)

  end subroutine WriteIFMSVector_eo

  subroutine ReadIFMSVector_eo(FileID,chi_o,i_v,parallel_io)
    integer :: FileID
    type(dirac_fermion), dimension(:) :: chi_o
    integer :: i_v
    logical :: parallel_io


    integer :: irec, jrec !Record counter for direct IO
    integer :: irank !Loop variable
    integer :: matrix_len ! Length of each colour spin matrix in bytes.
    integer :: i_xeo,n_v
    integer :: irecl
    integer :: iprocx, iprocy, iprocz, iproct, coords(nd)

    complex(sc), dimension(:,:,:), allocatable :: chi

    allocate(chi(nc,ns,n_xeo))

    if ( parallel_io ) then
       irec = i_v
       read(FileId,rec=irec) chi

       do i_xeo=1,n_xeo
          chi_o(i_xeo)%cs(:,:) = chi(:,:,i_xeo)
       end do

    else
       if ( i_am_root ) then
          jrec = 1
          do iproct=1,nproct
             do iprocz=1,nprocz
                do iprocy=1,nprocy
                   coords = (/ 0, iprocy-1, iprocz-1,iproct-1 /) 
                   irank = coords_to_rank(coords)
                   ! Read in the sub-lattice field.

                   irec = jrec + (i_v-1)*nproc
                   read(FileID,rec=irec) chi(:,:,:)

                   if ( irank /= mpi_root_rank ) then
                      call SendComplexField(chi, irank)
                   else
                       do i_xeo=1,n_xeo
                         chi_o(i_xeo)%cs(:,:) = chi(:,:,i_xeo)
                      end do
                   end if

                   jrec = jrec + 1
                end do
             end do
          end do

       else
          call RecvComplexField(chi,mpi_root_rank)
           do i_xeo=1,n_xeo
             chi_o(i_xeo)%cs(:,:) = chi(:,:,i_xeo)
          end do
       end if
    end if

    deallocate(chi)

  end subroutine ReadIFMSVector_eo

  subroutine OpenFermionFieldVectorFile(filename,FileID,action)
    character(len=*) :: filename
    integer, intent(inout) :: FileID
    character(len=*) :: action
    logical :: parallel_io

    ! begin local_vars
    character(len=6) :: Suffix
    integer :: matrix_len, irecl

    matrix_len = 16*nc*ns !double complex type=16 bytes
    irecl = matrix_len*nlx*nly*nlz*nlt

    if (i_am_root) then
       open(FileId,file=trim(filename),form='unformatted',access='direct',action=action,recl=irecl)
    end if

  end subroutine OpenFermionFieldVectorFile

  subroutine CloseFermionFieldVectorFile(FileID)
    integer :: FileID

    if ( i_am_root ) then
       close(FileId)
    end if

  end subroutine CloseFermionFieldVectorFile

  subroutine OpenSolutionVectorFile(filename,FileID,SolnFileFormat,action,parallel_io,ifms)
    character(len=*) :: filename
    integer, intent(inout) :: FileID
    character(len=*) :: SolnFileFormat
    character(len=*) :: action
    logical :: parallel_io
    type(IFMSMetaDataType), optional :: ifms

    if (sv_file_open) then
       call Abort("OpenSolutionVectorFile: cannot open multiple files simultaneously.")
    end if

    sv_file_open = .true.
    sv_FileFormat = trim(SolnFileFormat)
    sv_parallel_io = parallel_io

    select case(trim(sv_FileFormat))
    case("ifms")
       if (present(ifms)) then
          sv_ifms = ifms
       else
          call sv_ifms%Read(trim(filename)//".metadata")
       end if
       if ( .not. allocated(U_xd) ) then
          call Abort("ReadPropagator: Gauge field must be initialised to read propagators in ifms format")
       end if
       call SelectFermionAction(sv_ifms%FermActName)
       sv_init_eo = .not. eo_initialised
       call OpenIFMSFile_eo(filename,FileID,action,parallel_io)
       if (sv_init_eo) then
          call SetFermionActionParams(sv_ifms%fermact)
          call InitialiseFermionAction(U_xd,ifms%kappa,ifms%kBFStrength,even_odd=.true.)
       end if
       call CreateFermionField(sv_chi_e,1)
       call CreateFermionField(sv_chi_o,1)
    case("soln")
       if (sv_parallel_io) then
          call Abort("OpenSolutionVectorFile: parallel io not supported for this format.")
       end if
       
       call OpenFermionFieldVectorFile(filename,FileID,action)
    end select

  end subroutine OpenSolutionVectorFile

  subroutine ReadSolutionVector(chi,FileID,i_v,rho)
    type(colour_vector), dimension(:,:,:,:,:) :: chi
    integer :: FileID
    integer :: i_v
    type(colour_vector), dimension(:,:,:,:,:), optional :: rho

    select case(trim(sv_FileFormat))
    case("ifms")
       if ( .not. present(rho) ) then
          call Abort("ReadSolutionVector: source vector rho required to read solutions in ifms format.")
       end if
       call InvLeftILUMatrix(rho,sv_chi_e,sv_chi_o)
       call ReadIFMSVector_eo(FileId,sv_chi_o,i_v,parallel_io=.false.)
       call InvRightILUMatrix(sv_chi_e,sv_chi_o,chi)
    case("soln")   
       call ReadFermionField_bin(chi,FileID,i_v)
    end select
    
  end subroutine ReadSolutionVector

  subroutine CloseSolutionVectorFile(FileID)
    integer :: FileID
    
    select case(trim(sv_FileFormat))
    case("ifms")
       call DestroyFermionField(sv_chi_e)
       call DestroyFermionField(sv_chi_o)
       if (sv_init_eo) call FinaliseFermionAction(even_odd = .true.)
       call CloseIFMSFile(FileID, sv_parallel_io)
    case("soln")
       call CloseFermionFieldVectorFile(FileID)
    end select
    sv_file_open = .false.
  end subroutine CloseSolutionVectorFile

  subroutine WriteVectorSpace_4x(v_i,OutputFile)
    ! begin args: v_i, OutputFile

    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i
    character(len=*) :: OutputFile

    ! begin local_vars
    integer :: iRec, nRecl, n_v, i_v

    ! begin execution
    nRecl = 16*nc*ns*nlx*nly*nlz*nlt
    n_v = size(v_i,dim=6)

    if ( i_am_root ) then
       open (300, file=OutputFile, status = "unknown", form = "unformatted", & 
            & access="direct", action = "write", recl = nRecl)
    end if

    iRec = 1
    do i_v=1,n_v
       call WriteFermionField_bin(v_i(:,:,:,:,:,i_v),300,iRec)
    end do

    if ( i_am_root ) then
       close(300)
    end if

    call MPIBarrier

  end subroutine WriteVectorSpace_4x

  subroutine ReadVectorSpace_4x(v_i,InputFile)
    ! begin args: v_i, InputFile

    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i
    character(len=*) :: InputFile

    ! begin local_vars
    integer :: iRec, nRecl, n_v, i_v

    ! begin execution
    nRecl = 16*nc*ns*nlx*nly*nlz*nlt
    n_v = size(v_i,dim=6)

    if ( i_am_root ) then
       open (300, file=InputFile, status = "old", form = "unformatted", & 
            & access="direct", action = "read", recl = nRecl)
    end if

    iRec = 1
    do i_v=1,n_v
       call ReadFermionField_bin(v_i(:,:,:,:,:,i_v),300,iRec)
    end do

    if ( i_am_root ) then
       close(300)
    end if

    call MPIBarrier

  end subroutine ReadVectorSpace_4x

  subroutine WriteVectorSpace_eo(v_i,OutputFile)
    ! begin args: v_i, OutputFile

    type(dirac_fermion), dimension(:,:) :: v_i
    character(len=*) :: OutputFile

    ! begin local_vars
    integer :: iRec, nRecl, n_v, i_v

    ! begin execution
    nRecl = 16*nc*ns*nlattice_eo
    n_v = size(v_i,dim=2)

    if ( i_am_root ) then
       open (300, file=OutputFile, status = "unknown", form = "unformatted", & 
            & access="direct", action = "write", recl = nRecl)
    end if

    iRec = 1
    do i_v=1,n_v
       call WriteFermionField_eo(v_i(:,i_v),300,iRec)
    end do

    if ( i_am_root ) then
       close(300)
    end if

    call MPIBarrier

  end subroutine WriteVectorSpace_eo

  subroutine ReadVectorSpace_eo(v_i,InputFile)
    ! begin args: v_i, InputFile

    type(dirac_fermion), dimension(:,:) :: v_i
    character(len=*) :: InputFile

    ! begin local_vars
    integer :: iRec, nRecl, n_v, i_v

    ! begin execution
    nRecl = 16*nc*ns*nlattice_eo
    n_v = size(v_i,dim=2)

    if ( i_am_root ) then
       open (300, file=InputFile, status = "old", form = "unformatted", & 
            & access="direct", action = "read", recl = nRecl)
    end if

    iRec = 1
    do i_v=1,n_v
       call ReadFermionField_eo(v_i(:,i_v),300,iRec)
    end do
    
    if ( i_am_root ) then
       close(300)
    end if

    call MPIBarrier

  end subroutine ReadVectorSpace_eo

  subroutine WriteEigenSpace_4x(n_ev,n_dummy,lambda_i,v_i,tolerance,m_f,bct,OutputFile)
    ! begin args: n_ev, n_dummy, lambda_i, v_i, tolerance, m_f, bct, OutputFile

    integer, intent(in) :: n_ev, n_dummy
    real(dp), dimension(n_ev) :: lambda_i
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i
    real(dp) :: tolerance, m_f, bct
    character(len=*) :: OutputFile

    ! begin local_vars
    integer :: it, i_ev, irank

    ! begin execution

    if ( i_am_root ) then
       open (100, file=OutputFile, status = "unknown", form = "unformatted", action = "write")
       write (100) n_ev, n_dummy, tolerance, m_f, bct
    end if

    do i_ev=1,n_ev
       if ( i_am_root ) then
          write(100) lambda_i(i_ev)
       end if
       call WriteFermionField(v_i(:,:,:,:,:,i_ev),100)
    end do

    if ( i_am_root ) then
       close(100)
    end if

    call MPIBarrier


  end subroutine WriteEigenSpace_4x

  subroutine ReadEigenSpace_4x(n_ev,n_dummy,lambda_i,v_i,tolerance,m_f,bct,InputFile)
    ! begin args: n_ev, n_dummy, lambda_i, v_i, tolerance, m_f, bct, InputFile

    integer, intent(in) :: n_ev
    integer :: n_dummy
    real(dp), dimension(n_ev) :: lambda_i
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i
    real(dp) :: tolerance, m_f, bct
    character(len=*) :: InputFile

    ! begin local_vars
    integer :: it, i_ev, irank, m_ev

    ! begin execution

    if ( i_am_root ) then
       open (100, file=InputFile, status = "old", form = "unformatted", action = "read")
       read (100) m_ev, n_dummy, tolerance, m_f, bct
    end if

    do i_ev=1,n_ev
       if ( i_am_root ) then
          read(100) lambda_i(i_ev)
       end if
       call Broadcast(lambda_i(i_ev),mpi_root_rank)
       call ReadFermionField(v_i(:,:,:,:,:,i_ev),100)
    end do

    if ( i_am_root ) then
       close(100)
    end if

    call Broadcast(n_dummy,mpi_root_rank)
    call Broadcast(tolerance,mpi_root_rank)
    call Broadcast(m_f,mpi_root_rank)
    call Broadcast(bct,mpi_root_rank)


  end subroutine ReadEigenSpace_4x

  subroutine ReadFermionField_sc(phi,InFileId)
    ! begin args: phi, InFileId

    complex(dc), dimension(nx*ny*nz,nt,ns,nc) :: phi
    integer :: InFileId

    ! begin local_vars
    complex(dc), dimension(nx*ny*nz,nt,ns,nc) :: chi
    complex(dc), dimension(:,:,:,:), allocatable :: chi_in
    integer :: irank
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    integer :: kx,ky,kz,kt, i_xyz, k_xyz

    ! begin execution

    if ( i_am_root ) then
       allocate(chi_in(nlx*nly*nlz,nlt,ns,nc))

       read(InFileId) chi_in(:,:,:,:)

       do irank=0,mpi_size-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do kt=1,nt; do kz=1,nz; do ky=1,ny; do kx=1,nx
             k_xyz =  kx + (ky-1)*nx + (kz-1)*nx*ny
             i_xyz = (ix+(kx-1)) + (iy-1+(ky-1))*nlx + (iz-1+(kz-1))*nlx*nly
             chi(k_xyz,kt,:,:) = chi_in(i_xyz,it+kt-1,:,:)
          end do; end do; end do; end do
          if ( irank == mpi_root_rank ) then
             phi = chi
          else
             call SendComplexField(chi,irank)
          end if
       end do

       deallocate(chi_in)

    else
       call RecvComplexField(chi,mpi_root_rank)
       phi = chi
    end if


  end subroutine ReadFermionField_sc

  subroutine ReadTimeSlicePropagator(phi,OutFileId)
    ! begin args: phi, OutFileId

    complex(dc), dimension(:,:,:,:,:,:) :: phi
    integer :: OutFileId
    ! begin local_vars
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: chi
    real(dp), dimension(:,:,:,:,:,:), allocatable :: phir, phii
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    integer :: kx,ky,kz,kt, i_xyz, k_xyz
    integer :: irank

    ! begin execution

    allocate(chi(nx*ny*nz,nt,nc,ns,nc,ns))

    call MPIBarrier

    if ( i_am_root ) then

       allocate(phir(nlx*nly*nlz,nlt,nc,ns,nc,ns))
       allocate(phii(nlx*nly*nlz,nlt,nc,ns,nc,ns))

       do it=1,nlt
          read(OutFileId) phir(:,it,:,:,:,:)
          read(OutFileId) phii(:,it,:,:,:,:)
       end do

       do irank=0,mpi_size-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do kt=1,nt; do kz=1,nz; do ky=1,ny; do kx=1,nx
             k_xyz =  kx + (ky-1)*nx + (kz-1)*nx*ny
             i_xyz = (ix+(kx-1)) + (iy-1+(ky-1))*nlx + (iz-1+(kz-1))*nlx*nly
             chi(k_xyz,kt,:,:,:,:) = cmplx(phir(i_xyz,it+kt-1,:,:,:,:),phii(i_xyz,it+kt-1,:,:,:,:),dc)
          end do; end do; end do; end do

          if ( irank == mpi_root_rank ) then
             phi = chi
          else
             call SendComplexField(chi,irank)
          end if
       end do

       deallocate(phir)
       deallocate(phii)
    else
       call RecvComplexField(chi,mpi_root_rank)
       phi = chi
    end if

    deallocate(chi)

  end subroutine ReadTimeSlicePropagator

  subroutine WriteTimeSlicePropagator(phi,OutFileId)
    ! begin args: phi, OutFileId

    complex(dc), dimension(:,:,:,:,:,:) :: phi
    integer :: OutFileId
    ! begin local_vars
    complex(dc), dimension(:,:,:,:,:,:), allocatable :: chi
    real(dp), dimension(:,:,:,:,:,:), allocatable :: phir, phii
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    integer :: kx,ky,kz,kt, i_xyz, k_xyz
    integer :: irank

    ! begin execution

    allocate(chi(nx*ny*nz,nt,nc,ns,nc,ns))

    call MPIBarrier

    if ( i_am_root ) then

       allocate(phir(nlx*nly*nlz,nlt,nc,ns,nc,ns))
       allocate(phii(nlx*nly*nlz,nlt,nc,ns,nc,ns))

       do irank=0,mpi_size-1
          if ( irank == mpi_root_rank ) then
             chi = phi
          else
             call RecvComplexField(chi,irank)
          end if
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do kt=1,nt; do kz=1,nz; do ky=1,ny; do kx=1,nx
             k_xyz =  kx + (ky-1)*nx + (kz-1)*nx*ny
             i_xyz = (ix+(kx-1)) + (iy-1+(ky-1))*nlx + (iz-1+(kz-1))*nlx*nly
             phir(i_xyz,it+kt-1,:,:,:,:) =  real(chi(k_xyz,kt,:,:,:,:))
             phii(i_xyz,it+kt-1,:,:,:,:) = aimag(chi(k_xyz,kt,:,:,:,:))
          end do; end do; end do; end do

       end do

       do it=1,nlt
          write(OutFileId) phir(:,it,:,:,:,:)
          write(OutFileId) phii(:,it,:,:,:,:)
       end do

       deallocate(phir)
       deallocate(phii)
    else
       chi = phi
       call SendComplexField(chi,mpi_root_rank)
    end if

    deallocate(chi)

  end subroutine WriteTimeSlicePropagator

  subroutine WriteSplitTimeSlicePropagator(phi,OutFileId)
    ! begin args: phi, OutFileId

    complex(dc), dimension(:,:,:,:,:,:) :: phi
    integer :: OutFileId
    ! begin local_vars
    integer :: it

    ! begin execution

    do it=1,nt
       write(OutFileId) phi(:,it,:,:,:,:)
    end do

    call MPIBarrier


  end subroutine WriteSplitTimeSlicePropagator

  subroutine ReadSplitTimeSlicePropagator(phi,InFileId)
    ! begin args: phi, InFileId

    complex(dc), dimension(:,:,:,:,:,:) :: phi
    integer :: InFileId
    ! begin local_vars
    integer :: it

    ! begin execution

    do it=1,nt
       read(InFileId) phi(:,it,:,:,:,:)
    end do

    call MPIBarrier


  end subroutine ReadSplitTimeSlicePropagator

end module FermionFieldIO
