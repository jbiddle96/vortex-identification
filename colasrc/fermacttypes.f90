
module FermActTypes
  use Kinds
  use TextIO
  use MPIInterface
  implicit none
  private

  type, abstract, public :: FermActType
   contains
     procedure(txt_io), deferred :: Get
     procedure(txt_io), deferred :: Put
     procedure :: Read => ReadFermActType
     procedure :: Write => WriteFermActType
  end type FermActType

  abstract interface
     subroutine txt_io(this,file_unit)
       import :: FermActType
       class(FermActType) :: this
       integer :: file_unit
     end subroutine txt_io
  end interface

  type, extends(FermactType), public :: wilsonFermAct
     real(dp) :: bcx, bcy, bcz, bct
     real(dp) :: u0
   contains
     procedure :: Get => Get_wilson
     procedure :: Put => Put_wilson
     procedure :: Print => Print_wilson
  end type wilsonFermAct

  type, extends(wilsonFermAct), public :: cloverFermAct
     real(dp) :: c_sw
   contains
     procedure :: Get => Get_clover
     procedure :: Put => Put_clover
     procedure :: Print => Print_clover
  end type cloverFermAct

  type, extends(wilsonFermAct), public :: flwFermAct
     real(dp) :: alpha_smear
     integer  :: n_sweeps
     real(dp) :: u0sm
   contains
     procedure :: Get => Get_flw
     procedure :: Put => Put_flw
     procedure :: Print => Print_flw
  end type flwFermAct

  type, extends(cloverFermAct), public :: flcFermAct
     real(dp) :: alpha_smear
     integer  :: n_sweeps
     real(dp) :: u0sm
   contains
     procedure :: Get => Get_flc
     procedure :: Put => Put_flc
     procedure :: Print => Print_flc
  end type flcFermAct

  type, extends(wilsonFermAct), public :: fliwFermAct
     real(dp) :: alpha_smear
     integer  :: n_sweeps
     real(dp) :: u0sm
   contains
     procedure :: Get => Get_fliw
     procedure :: Put => Put_fliw
     procedure :: Print => Print_fliw
  end type fliwFermAct

  type, extends(cloverFermAct), public :: flicFermAct
     real(dp) :: alpha_smear
     integer  :: n_sweeps
     real(dp) :: u0sm
   contains
     procedure :: Get => Get_flic
     procedure :: Put => Put_flic
     procedure :: Print => Print_flic
  end type flicFermAct

!  type, extends(FermactType), public :: overlapFermAct
!     real(dp) :: kappa_kernel
!     character(len=32) :: KernelAction
!     class(wilsonFermAct), pointer :: kernelFermAct
!   contains
!     procedure :: Get => Get_overlap
!     procedure :: Put => Put_overlap
!     procedure :: Print => Print_overlap
!  end type overlapFermAct
  
  public :: SetFermActType

contains

  subroutine ReadFermActType(this,filename)
    class(FermActType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='read')
    call this%Get(file_unit)
    call CloseTextfile(file_unit)

  end subroutine ReadFermActType

  subroutine WriteFermActType(this,filename)
    class(FermActType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='write')
    call this%Put(file_unit)
    call CloseTextfile(file_unit)

  end subroutine WriteFermActType

  subroutine Get_wilson(this,file_unit)
    class(wilsonFermAct) :: this
    integer :: file_unit

    call Get(this%bcx ,file_unit)
    call Get(this%bcy ,file_unit)
    call Get(this%bcz ,file_unit)
    call Get(this%bct ,file_unit)
    call Get(this%u0 ,file_unit)
    
  end subroutine Get_wilson

  subroutine Get_clover(this,file_unit)
    class(cloverFermAct) :: this
    integer :: file_unit

    call this%wilsonFermAct%Get(file_unit)
    call Get(this%c_sw,file_unit)
    
  end subroutine Get_clover

  subroutine Get_flw(this,file_unit)
    class(flwFermAct) :: this
    integer :: file_unit

    call this%wilsonFermAct%Get(file_unit)
    call Get(this%alpha_smear ,file_unit)
    call Get(this%n_sweeps,file_unit)
    call Get(this%u0sm,file_unit)
    
  end subroutine Get_flw

  subroutine Get_flc(this,file_unit)
    class(flcFermAct) :: this
    integer :: file_unit

    call this%cloverFermAct%Get(file_unit)
    call Get(this%alpha_smear ,file_unit)
    call Get(this%n_sweeps,file_unit)
    call Get(this%u0sm,file_unit)
    
  end subroutine Get_flc

  subroutine Get_fliw(this,file_unit)
    class(fliwFermAct) :: this
    integer :: file_unit

    call this%wilsonFermAct%Get(file_unit)
    call Get(this%alpha_smear ,file_unit)
    call Get(this%n_sweeps,file_unit)
    call Get(this%u0sm,file_unit)
    
  end subroutine Get_fliw

  subroutine Get_flic(this,file_unit)
    class(flicFermAct) :: this
    integer :: file_unit

    call this%cloverFermAct%Get(file_unit)
    call Get(this%alpha_smear ,file_unit)
    call Get(this%n_sweeps,file_unit)
    call Get(this%u0sm,file_unit)
    
  end subroutine Get_flic

  subroutine Put_wilson(this,file_unit)
    class(wilsonFermAct) :: this
    integer :: file_unit

    if (i_am_root) then
       write(file_unit,'(f20.10,a)') this%bcx , " !fermact%bcx "
       write(file_unit,'(f20.10,a)') this%bcy , " !fermact%bcy "
       write(file_unit,'(f20.10,a)') this%bcz , " !fermact%bcz "
       write(file_unit,'(f20.10,a)') this%bct , " !fermact%bct "
       write(file_unit,'(f20.10,a)') this%u0, " !fermact%u0"
    end if
    
  end subroutine Put_wilson

  subroutine Put_clover(this,file_unit)
    class(cloverFermAct) :: this
    integer :: file_unit

    call this%wilsonFermAct%Put(file_unit)
    if (i_am_root) then
       write(file_unit,'(f20.10,a)') this%c_sw, " !fermact%c_sw"
    end if
    
  end subroutine Put_clover

  subroutine Put_flw(this,file_unit)
    class(flwFermAct) :: this
    integer :: file_unit

    call this%wilsonFermAct%Put(file_unit)
    if (i_am_root) then
       write(file_unit,'(f20.10,a)') this%alpha_smear , " !fermact%alpha_smear "
       write(file_unit,'(i20,a)') this%n_sweeps , " !fermact%n_sweeps "
       write(file_unit,'(f20.10,a)') this%u0sm, " !fermact%u0sm"
    end if
    
  end subroutine Put_flw

  subroutine Put_flc(this,file_unit)
    class(flcFermAct) :: this
    integer :: file_unit
    
    call this%cloverFermAct%Put(file_unit)
    if (i_am_root) then
       write(file_unit,'(f20.10,a)') this%alpha_smear , " !fermact%alpha_smear "
       write(file_unit,'(i20,a)') this%n_sweeps , " !fermact%n_sweeps "
       write(file_unit,'(f20.10,a)') this%u0sm, " !fermact%u0sm"
    end if
    
  end subroutine Put_flc

  subroutine Put_fliw(this,file_unit)
    class(fliwFermAct) :: this
    integer :: file_unit

    call this%wilsonFermAct%Put(file_unit)
    if (i_am_root) then
       write(file_unit,'(f20.10,a)') this%alpha_smear , " !fermact%alpha_smear "
       write(file_unit,'(i20,a)') this%n_sweeps , " !fermact%n_sweeps "
       write(file_unit,'(f20.10,a)') this%u0sm, " !fermact%u0sm"
    end if
    
  end subroutine Put_fliw

  subroutine Put_flic(this,file_unit)
    class(flicFermAct) :: this
    integer :: file_unit
    
    call this%cloverFermAct%Put(file_unit)
    if (i_am_root) then
       write(file_unit,'(f20.10,a)') this%alpha_smear , " !fermact%alpha_smear "
       write(file_unit,'(i20,a)') this%n_sweeps , " !fermact%n_sweeps "
       write(file_unit,'(f20.10,a)') this%u0sm, " !fermact%u0sm"
    end if
    
  end subroutine Put_flic

  subroutine Print_wilson(this)
    class(wilsonFermAct) :: this

    if (i_am_root) then
       print '(a,f20.10)', "fermact%bcx= ", this%bcx
       print '(a,f20.10)', "fermact%bcy= ", this%bcy
       print '(a,f20.10)', "fermact%bcz= ", this%bcz
       print '(a,f20.10)', "fermact%bct= ", this%bct
       print '(a,f20.10)', "fermact%u0= ", this%u0
    end if
    
  end subroutine Print_wilson

  subroutine Print_clover(this)
    class(cloverFermAct) :: this

    call this%wilsonFermAct%Print()
    if (i_am_root) then
       print '(a,f20.10)', "fermact%c_sw= ", this%c_sw
    end if
    
  end subroutine Print_clover

  subroutine Print_flw(this)
    class(flwFermAct) :: this

    call this%wilsonFermAct%Print()
    if (i_am_root) then
       print '(a,f20.10)', "fermact%alpha_smear= ", this%alpha_smear
       print '(a,i20)', "fermact%n_sweeps= ", this%n_sweeps
       print '(a,f20.10)', "fermact%u0sm= ", this%u0sm
    end if
    
  end subroutine Print_flw

  subroutine Print_flc(this)
    class(flcFermAct) :: this
    
    call this%cloverFermAct%Print()
    if (i_am_root) then
       print '(a,f20.10)', "fermact%alpha_smear= ", this%alpha_smear
       print '(a,i20)', "fermact%n_sweeps= ", this%n_sweeps
       print '(a,f20.10)', "fermact%u0sm= ", this%u0sm
    end if
    
  end subroutine Print_flc

  subroutine Print_fliw(this)
    class(fliwFermAct) :: this

    call this%wilsonFermAct%Print()
    if (i_am_root) then
       print '(a,f20.10)', "fermact%alpha_smear= ", this%alpha_smear
       print '(a,i20)', "fermact%n_sweeps= ", this%n_sweeps
       print '(a,f20.10)', "fermact%u0sm= ", this%u0sm
    end if
    
  end subroutine Print_fliw

  subroutine Print_flic(this)
    class(flicFermAct) :: this
    
    call this%cloverFermAct%Print()
    if (i_am_root) then
       print '(a,f20.10)', "fermact%alpha_smear= ", this%alpha_smear
       print '(a,i20)', "fermact%n_sweeps= ", this%n_sweeps
       print '(a,f20.10)', "fermact%u0sm= ", this%u0sm
    end if
    
  end subroutine Print_flic

  subroutine SetFermActType(fermact,FermActName)
    class(wilsonFermAct), pointer :: fermact
    character(len=*) :: FermActName

    select case (trim(FermActName))
    case("wilson")
       allocate(wilsonFermAct :: fermact)
    case("clover")
       allocate(cloverFermAct :: fermact)
    case("flw")
       allocate(flwFermAct :: fermact)
    case("flc")
       allocate(flcFermAct :: fermact)
    case("fliw")
       allocate(fliwFermAct :: fermact)
    case("flic")
       allocate(flicFermAct :: fermact)
    end select

  end subroutine SetFermActType

end module FermActTypes
