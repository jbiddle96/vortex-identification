!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

#include "defines.h"

module MPIInterface

  use Kinds
  implicit none
  public

  include "mpif.h"

  integer, parameter :: mpi_comm = MPI_COMM_WORLD, mpi_root_rank = 0
  integer :: mpi_size, mpi_rank
  logical :: i_am_root

  integer, parameter :: mpi_dp = MPI_DOUBLE_PRECISION, mpi_dc = MPI_DOUBLE_COMPLEX, mpi_sp = MPI_REAL, mpi_sc = MPI_COMPLEX
  integer, parameter :: nmpi_status = MPI_STATUS_SIZE
  integer, parameter :: nmpi_header = MPI_BSEND_OVERHEAD

contains

  subroutine InitialiseMPI

    integer :: mpierror
    integer :: provided
    call MPI_Init(mpierror)
    !!call MPI_Init_Thread(MPI_THREAD_MULTIPLE,provided,mpierror)
    call MPI_ErrorCheck(mpierror)

!!$    if ( provided == MPI_THREAD_SINGLE ) print *, "MPI_THREAD_SINGLE"
!!$    if ( provided == MPI_THREAD_FUNNELED ) print *, "MPI_THREAD_FUNNELED"
!!$    if ( provided == MPI_THREAD_SERIALIZED ) print *, "MPI_THREAD_SERIALIZED"
!!$    if ( provided == MPI_THREAD_MULTIPLE ) print *, "MPI_THREAD_MULTIPLE"

    call MPI_Comm_size(mpi_comm, mpi_size, mpierror)
    call MPI_ErrorCheck(mpierror)

    call MPI_Comm_rank(mpi_comm, mpi_rank, mpierror)
    call MPI_ErrorCheck(mpierror)

    i_am_root = (mpi_rank == mpi_root_rank )

  end subroutine InitialiseMPI

  subroutine Abort(error)

    character(len=*) :: error
    integer :: ierror

    print *, "User abort: ", error
    call MPI_Abort(mpi_comm,MPI_ERR_OTHER,ierror)
    
  end subroutine Abort

  subroutine Wait(mpi_request)

    integer :: mpi_request
    integer, dimension(nmpi_status) :: mpi_status
    integer :: mpierror

    call MPI_Wait(mpi_request, mpi_status, mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine Wait

  subroutine Start(mpi_request)

    integer :: mpi_request
    integer, dimension(nmpi_status) :: mpi_status
    integer :: mpierror

    call MPI_Start(mpi_request, mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine Start

  subroutine MPIBarrier

    integer :: mpierror

    call MPI_Barrier(mpi_comm, mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine MPIBarrier

  subroutine FinaliseMPI

    integer :: mpierror

    call MPI_Finalize(mpierror)
    call MPI_ErrorCheck(mpierror)

  end subroutine FinaliseMPI

  subroutine MPI_ErrorCheck(mpierror)

    integer :: mpierror
    integer :: errorcode, errorclass
    character(len=MPI_MAX_ERROR_STRING) :: errorstring
    integer:: length, ierror

    if (mpierror /= MPI_SUCCESS ) then
       call MPI_ERROR_CLASS(mpierror, errorclass, ierror)
       select case (errorclass)
       case (MPI_ERR_BUFFER) 
          mpiprint *, "MPI_ERR_BUFFER"
       case (MPI_ERR_COUNT) 
          mpiprint *, "MPI_ERR_COUNT"
       case (MPI_ERR_TYPE) 
          mpiprint *, "MPI_ERR_TYPE"
       case (MPI_ERR_TAG) 
          mpiprint *, "MPI_ERR_TAG"
       case (MPI_ERR_COMM) 
          mpiprint *, "MPI_ERR_COMM"
       case (MPI_ERR_RANK) 
          mpiprint *, "MPI_ERR_RANK"
       case (MPI_ERR_REQUEST) 
          mpiprint *, "MPI_ERR_REQUEST"
       case (MPI_ERR_ROOT) 
          mpiprint *, "MPI_ERR_ROOT"
       case (MPI_ERR_GROUP) 
          mpiprint *, "MPI_ERR_GROUP"
       case (MPI_ERR_OP) 
          mpiprint *, "MPI_ERR_OP"
       case (MPI_ERR_TOPOLOGY) 
          mpiprint *, "MPI_ERR_TOPOLOGY"
       case (MPI_ERR_DIMS) 
          mpiprint *, "MPI_ERR_DIMS"
       case (MPI_ERR_ARG) 
          mpiprint *, "MPI_ERR_ARG"
       case (MPI_ERR_OTHER) 
          mpiprint *, "MPI_ERR_OTHER"
       case (MPI_ERR_UNKNOWN) 
          mpiprint *, "MPI_ERR_UNKNOWN"
       case (MPI_ERR_INTERN) 
          mpiprint *, "MPI_ERR_INTERN"
       case (MPI_ERR_IN_STATUS) 
          mpiprint *, "MPI_ERR_IN_STATUS"
       case (MPI_ERR_PENDING) 
          mpiprint *, "MPI_ERR_PENDING"
       case (MPI_ERR_LASTCODE) 
          mpiprint *, "MPI_ERR_LASTCODE"
       end select
       call MPI_ERROR_STRING( mpierror, errorstring, length, ierror ) 
       mpiprint *, "Errorcode", mpierror, errorstring(1:length)
       call MPI_ERROR_STRING( errorclass, errorstring, length, ierror ) 
       mpiprint *, "Errorclass", mpierror, errorstring(1:length)
       call MPI_Abort(mpi_comm, mpierror, ierror)
    end if

  end subroutine MPI_ErrorCheck

end module MPIInterface
