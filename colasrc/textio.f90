
module TextIO

  use Kinds
  use MPIInterface
  use CollectiveOps
  implicit none
  private

  interface Put
     module procedure PutString
     module procedure PutLogical
     module procedure PutInteger
     module procedure PutInteger_1
     module procedure PutReal_sp
     module procedure PutReal_dp
     module procedure PutComplex_sc
     module procedure PutComplex_dc
  end interface

  interface Get
     module procedure GetString
     module procedure GetLogical
     module procedure GetInteger
     module procedure GetInteger_1
     module procedure GetReal_sp
     module procedure GetReal_dp
     module procedure GetComplex_sc
     module procedure GetComplex_dc
  end interface

  public :: Put, Get
  public :: OpenTextfile, CloseTextfile

  integer :: next_unit_number = 100

contains
  
  subroutine OpenTextfile(file,unit,action)
    character(len=*), intent(in) :: file, action
    integer, intent(out) :: unit

    unit = next_unit_number
    next_unit_number = next_unit_number + 1

    if ( i_am_root ) then
       open(unit=unit,file=file,action=action)
    end if

  end subroutine OpenTextfile

  subroutine CloseTextfile(unit)
    integer, intent(in) :: unit
    
    if ( i_am_root ) then
       close(unit=unit)
    end if

    next_unit_number = next_unit_number - 1

  end subroutine CloseTextfile

  subroutine PutString(a,unit)
    character(len=*), intent(in) :: a
    integer, intent(in), optional :: unit

    character(len=8) :: len_str
    character(len=11) :: str_format

    write(len_str,'(I8)') len(a)
    str_format = "(a"//trim(len_str)//")"

    if ( i_am_root ) then
       if ( present(unit) ) then
          write(unit,str_format) a
       else
          write(*,str_format) a
       end if
    end if
  end subroutine PutString

  subroutine PutLogical(a,unit)
    logical, intent(in) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then
          write(unit,*) a
       else
          write(*,*) a
       end if
    end if
  end subroutine PutLogical

  subroutine PutInteger(a,unit)
    integer, intent(in) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then
          write(unit,*) a
       else
          write(*,*) a
       end if
    end if
  end subroutine PutInteger

  subroutine PutInteger_1(a,unit)
    integer, dimension(:), intent(in) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then
          write(unit,*) a
       else
          write(*,*) a
       end if
    end if
  end subroutine PutInteger_1

  subroutine PutReal_sp(a,unit)
    real(sp), intent(in) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then
          write(unit,*) a
       else
          write(*,*) a
       end if
    end if
  end subroutine PutReal_sp

  subroutine PutReal_dp(a,unit)
    real(dp), intent(in) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then
          write(unit,*) a
       else
          write(*,*) a
       end if
    end if
  end subroutine PutReal_dp

  subroutine PutComplex_sc(a,unit)
    complex(sc), intent(in) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then
          write(unit,*) a
       else
          write(*,*) a
       end if
    end if
  end subroutine PutComplex_sc

  subroutine PutComplex_dc(a,unit)
    complex(dc), intent(in) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then
          write(unit,*) a
       else
          write(*,*) a
       end if
    end if
  end subroutine PutComplex_dc

  subroutine GetString(a,unit)
    character(len=*), intent(out) :: a
    integer, intent(in), optional :: unit
    integer :: l

    character(len=8) :: len_str
    character(len=11) :: str_format

    write(len_str,'(I8)') len(a)
    str_format = "(a"//trim(len_str)//")"

    if ( i_am_root ) then
       if ( present(unit) ) then 
          read(unit,str_format) a
       else
          read(*,str_format) a
       end if
    end if
    call Broadcast(a)

  end subroutine GetString

  subroutine GetLogical(a,unit)
    logical, intent(out) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then 
          read(unit,*) a
       else
          read(*,*) a
       end if
    end if
    call Broadcast(a)

  end subroutine GetLogical

  subroutine GetInteger(a,unit)
    integer, intent(out) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then 
          read(unit,*) a
       else
          read(*,*) a
       end if
    end if
    call Broadcast(a)

  end subroutine GetInteger

  subroutine GetInteger_1(a,unit)
    integer, dimension(:), intent(out) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then 
          read(unit,*) a
       else
          read(*,*) a
       end if
    end if
    call Broadcast(a)

  end subroutine GetInteger_1

  subroutine GetReal_sp(a,unit)
    real(sp), intent(out) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then 
          read(unit,*) a
       else
          read(*,*) a
       end if
    end if
    call Broadcast(a)

  end subroutine GetReal_sp

  subroutine GetReal_dp(a,unit)
    real(dp), intent(out) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then 
          read(unit,*) a
       else
          read(*,*) a
       end if
    end if
    call Broadcast(a)

  end subroutine GetReal_dp

  subroutine GetComplex_sc(a,unit)
    integer, intent(in), optional :: unit
    complex(sc), intent(out) :: a

    if ( i_am_root ) then
       if ( present(unit) ) then 
          read(unit,*) a
       else
          read(*,*) a
       end if
    end if
    call Broadcast(a)

  end subroutine GetComplex_sc

  subroutine GetComplex_dc(a,unit)
    complex(dc), intent(out) :: a
    integer, intent(in), optional :: unit

    if ( i_am_root ) then
       if ( present(unit) ) then 
          read(unit,*) a
       else
          read(*,*) a
       end if
    end if
    call Broadcast(a)

  end subroutine GetComplex_dc

end module TextIO
