#include "defines.h"

module OverlapPropagator
  use SignFunction
  use ZolotarevApprox
  use OverlapAction
  use GaugeField
  use FermionField
  use FermionFieldIO
  use ConjGradSolvers
  use MPIInterface
  use LatticeSize
  use SpinorTypes
  use ColourTypes
  use Kinds
  implicit none
  private

  real(dp), public :: alpha_sm
  integer, public :: n_smear, js_start=1, js_end=1, jc_start=1, jc_end = nc
  logical, public :: SmearedSource

  public :: GetOverlapPropagator
  !public :: WritePropagatorColumn
  !public :: ReadHeader
  public :: OverlapMultiMassInvert
  public :: GetOverPropFilename

contains

#define chi_x(is)  chi(ix,iy,iz,it,is)
#define Dchi_x(is) Dchi(ix,iy,iz,it,is)
#define chi_ix(is) chi_i(ix,iy,iz,it,is,i_mu)
#define phi_x(is)  phi(ix,iy,iz,it,is)
#define Mphi_x(is) Mphi(ix,iy,iz,it,is)
#define phi_ix(is) phi_i(ix,iy,iz,it,is,i_mu)
#define residue_x(is) residue(ix,iy,iz,it,is)
#define rho_x(is)  rho(ix,iy,iz,it,is)

  subroutine GetOverPropFilename(PropFile,Prefix,Suffix,mu)

    character(len=*) :: PropFile
    character(len=*) :: Prefix
    character(len=*) :: Suffix
    real(DP) :: mu !quark mass

    character(len=7) :: muString

    write( muString, '(F7.5)') mu
    muString = "mu" // muString(3:7)
    PropFile = trim(Prefix) // trim(muString) // trim(Suffix)

  end subroutine GetOverPropFilename

  subroutine GetOverlapPropagator(n_mu,mu_i,tolerance,rho,chi_i,lambda_pm)
    ! begin args: n_mu, mu_i, tolerance, rho,lambda_pm

    integer, intent(in) :: n_mu !number of shifts
    real(dp), dimension(n_mu), intent(in) :: mu_i !quark masses
    real(dp) :: tolerance !the precision desired for the solution
    type(colour_vector), dimension(:,:,:,:,:) :: rho
    type(colour_vector), dimension(:,:,:,:,:,:) :: chi_i !solution vectors
    integer :: lambda_pm

    ! begin local_vars
    real(dp), dimension(n_mu) :: muprime_i, normchi_i !quark masses
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: rho_sp
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi, Dchi
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: phi_i
    
    real(dp) :: norm_chi, norm_rho
    complex(dc) :: lambda_test

    character(len=273) :: OutputFile, PropagatorFile
    character(len=6) :: OutputSuffix, PropagatorSuffix

    integer :: js,jc
    integer :: ix,iy,iz,it,is,ic,i_mu,iterations
    integer, dimension(n_mu) :: iterations_i
    real(dp) :: starttime, endtime

    ! begin execution
    allocate(rho_sp(nxp,nyp,nzp,ntp,nsp))
    allocate(chi(nxp,nyp,nzp,ntp,ns))
    allocate(Dchi(nxp,nyp,nzp,ntp,ns))
    allocate(phi_i(nx,ny,nz,nt,nsp,n_mu))
    !allocate(chi_i(nx,ny,nz,nt,ns,n_mu))

    norm_rho = fermion_norm(rho)
    call normalise(rho,norm_rho)

    chi = zero_vector

    mpiprint *, " L2 Source norm", norm_rho

    select case(lambda_pm)
    case(1)
       !lambda_pm = 1
       rho_sp = rho(:,:,:,:,1:2)
    case(-1)
       !lambda_pm = -1
       rho_sp = rho(:,:,:,:,3:4)
    end select

    muprime_i = mu_i**2/(1.0d0-mu_i**2)

    starttime = mpi_wtime()
    call OverlapMultiMassInvert(n_mu, muprime_i, rho_sp, lambda_pm, phi_i, tolerance, iterations_i )
    endtime = mpi_wtime()

    mpiprint '(A,F20.2,A,F15.5,A)', "Inversion took ", endtime-starttime, " seconds = ",(endtime-starttime)/3600.0,"hours."

    select case(lambda_pm)
    case(1)
       chi_i(:,:,:,:,1:2,:) = phi_i
       chi_i(:,:,:,:,3:4,:) = zero_vector
    case(-1)
       chi_i(:,:,:,:,1:2,:) = zero_vector
       chi_i(:,:,:,:,3:4,:) = phi_i
    end select


    mpiprint *, " #Mu ", " Mass ", " Convergence "
    do i_mu=1,n_mu
       mpiprint *, i_mu, mu_i(i_mu), iterations_i(i_mu)
    end do

    !Check Solution
    do i_mu = 1,n_mu
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          chi_ix(is)%Cl = chi_ix(is)%Cl/(1.0d0 - mu_i(i_mu)**2)
       end do; end do; end do; end do; end do
    end do

    !Multiply solution by D^\dagger to get inverse of D
    do i_mu = 1,n_mu
       chi(1:nx,1:ny,1:nz,1:nt,:) = chi_i(:,:,:,:,:,i_mu)
       call Doverlap(chi,Dchi)
       if (lambda_pm == 1) then
          !+ve chirality, multiply by +gamma_5
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             Dchi_x(3)%Cl =  -Dchi_x(3)%Cl
             Dchi_x(4)%Cl =  -Dchi_x(4)%Cl
          end do; end do; end do; end do
       else
          !-ve chirality, multiply by -gamma_5
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             Dchi_x(1)%Cl =  -Dchi_x(1)%Cl
             Dchi_x(2)%Cl =  -Dchi_x(2)%Cl
          end do; end do; end do; end do
       end if

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          chi_ix(is)%Cl = (1.0d0-mu_i(i_mu))*Dchi_x(is)%Cl + mu_i(i_mu)*chi_ix(is)%Cl
       end do; end do; end do; end do; end do

    end do

    mpiprint *, " Propagator solution quality"
    mpiprint *, " #Mu ", " Error "
    do i_mu=1,n_mu
       chi(1:nx,1:ny,1:nz,1:nt,:) = chi_i(:,:,:,:,:,i_mu)
       call Doverlap(chi,Dchi)
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Dchi_x(is)%Cl = (1.0d0-mu_i(i_mu))*Dchi_x(is)%Cl + mu_i(i_mu)*chi_ix(is)%Cl - rho_x(is)%Cl
       end do; end do; end do; end do; end do

       norm_chi = fermion_norm(Dchi)
       mpiprint *, i_mu, norm_chi, norm_chi/norm_rho
    end do

    !call WritePropagatorColumn(OutputPrefix,n_mu,mu_i,chi_i)

    deallocate(rho_sp)
    deallocate(chi)
    deallocate(Dchi)
    deallocate(phi_i)
    !deallocate(chi_i)

  end subroutine GetOverlapPropagator

  subroutine OverlapMultiMassInvert(n_mu, mu_a, psi, lambda_pm, chi_i, tolerance, iterations_i)
    ! begin args: n_mu, mu_a, psi, lambda_pm, chi_i, tolerance, iterations_i

    !A routine to solve the matrix equation (M+mu)v = psi for a system of shifts mu
    !mu must be ordered such that mu(1) contains the least convergent system
    ! (i.e. the system that converges slowest).

    !M _must_ be hermitian and positive definite.

    !Assumes a chiral source psi, chirality lambda_pm

    !Assumed: tolerance >= epsilon(1.0d0)

    integer, intent(in) :: n_mu !number of shifts
    real(dp), dimension(n_mu), intent(in) :: mu_a !quark masses
    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    integer :: lambda_pm !the chirality of psi
    type(colour_vector), dimension(:,:,:,:,:,:) :: chi_i !solution vectors
    real(dp) :: tolerance !the precision desired for the solution
    integer, dimension(n_mu) :: iterations_i

    ! begin local_vars
    integer :: iterations !the total number of iterations required
    real(dp), dimension(n_mu) :: mu_i !offset matrix shifts
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue, phi, Mphi
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: phi_i
    real(dp) :: normsq_r, prevnormsq_r, beta, prevbeta, alpha, phidotMphi, offset, normpsi
    real(dp), dimension(n_mu) :: beta_i, prevbeta_i, zeta_i, prevzeta_i, alpha_i, nextzeta_i
    logical, dimension(n_mu) :: converged_i, converged_j
    integer :: ix,iy,iz,it,is,i_mu
    real(dp) :: tau = 1.0d0 !tau = 1.0d-1

    !translate the mu shifts so that mu_i(1)=0
    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,nsp))
    allocate(phi(nxp,nyp,nzp,ntp,nsp))
    allocate(Mphi(nxp,nyp,nzp,ntp,nsp))
    allocate(phi_i(nx,ny,nz,nt,nsp,n_mu))

    offset = mu_a(1)
    if (offset /= 0) then
       mu_i = mu_a - offset
    else
       mu_i = mu_a
    end if

    chi_i = zero_vector
    residue = psi

    do i_mu=1,n_mu
       phi_i(:,:,:,:,:,i_mu) = residue(1:nx,1:ny,1:nz,1:nt,:)
    end do

    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    alpha_i = 0
    beta_i = beta

    normsq_r = fermion_normsq(residue)
    normpsi = sqrt(normsq_r)

    phi(1:nx,1:ny,1:nz,1:nt,:) = phi_i(:,:,:,:,:,1)
    call ChiralHsqoverlap(phi,Mphi, lambda_pm)
    if (offset /= 0) then
       do is=1,nsp; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Mphi_x(is)%Cl = Mphi_x(is)%Cl + offset*phi_x(is)%Cl
       end do; end do; end do; end do; end do
    end if
    phidotMphi = real_inner_product(phi,Mphi)

    iterations = 0
    iterations_i = 0

    converged_i = .false.
    converged_j = .false.

    do
       iterations = iterations + 1

       do i_mu=1,n_mu
          if ( ( sqrt(normsq_r)*zeta_i(i_mu)/normpsi < tolerance ) .and. &
               & .not. converged_j(i_mu) ) iterations_i(i_mu) = iterations
       end do
       if (sqrt(normsq_r)/normpsi < tolerance) exit
       mpiprint *, iterations, sqrt(normsq_r)/normpsi, converged_j, epsilon_iter_i(1)
       !mpiprint *, iterations, sqrt(normsq_r), alpha, beta, phidotMphi

       !If the slowest convergence is extremely slow compared to the other shifts, we need to have a "safety net"
       !that prevents underflow in zeta_i for the already converged values.
       converged_i = (sqrt(normsq_r)*zeta_i/normpsi < epsilon(1.0d0) )
       converged_j = (sqrt(normsq_r)*zeta_i/normpsi < tolerance )
       do i_mu=1,n_mu
          if (converged_i(i_mu)) zeta_i(i_mu) = epsilon(1.0d0)
       end do

       beta = -normsq_r/phidotMphi

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-mu_i*beta))
       beta_i = beta*nextzeta_i/zeta_i

       do i_mu=1,n_mu
          if (.not. converged_i(i_mu)) then
             do is=1,nsp; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                chi_ix(is)%Cl = chi_ix(is)%Cl - beta_i(i_mu)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       do is=1,nsp; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = residue_x(is)%Cl + beta*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do

       prevnormsq_r = normsq_r
       normsq_r = fermion_normsq(residue)

       alpha = normsq_r/prevnormsq_r
       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       do i_mu=1,n_mu
          if (.not. converged_i(i_mu) ) then
             do is=1,nsp; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                phi_ix(is)%Cl = nextzeta_i(i_mu)*residue_x(is)%Cl + alpha_i(i_mu)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       phi(1:nx,1:ny,1:nz,1:nt,:) = phi_i(:,:,:,:,:,1)
       call ChiralHsqoverlap(phi,Mphi,lambda_pm)
       if (offset /= 0) then
          do is=1,nsp; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             Mphi_x(is)%Cl = Mphi_x(is)%Cl + offset*phi_x(is)%Cl
          end do; end do; end do; end do; end do
       end if

       phidotMphi = real_inner_product(phi,Mphi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

       prevbeta_i = beta_i
    end do

    iterations_i(1) = iterations
    where (iterations_i == 0) iterations_i = iterations

    deallocate(residue)
    deallocate( phi)
    deallocate( Mphi)
    deallocate(phi_i)

  end subroutine OverlapMultiMassInvert

#undef chi_x
#undef Dchi_x
#undef chi_ix
#undef phi_x
#undef Mphi_x
#undef residue_x
#undef rho_x

end module OverlapPropagator

