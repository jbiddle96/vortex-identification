/* flic.h */


module flicMatrix

  use Timer
  use MPIInterface
  use FermionField
  use FermionFieldMPIComms
  use FermionAlgebra

  use GaussJordan

  use SpinorTypes
  use FermionTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  use ColourFieldOps
  implicit none
  private
  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:,:,:), public, allocatable :: Up_xd
  type(colour_matrix), dimension(:,:,:,:,:), public, allocatable :: Um_xd
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: Fcl_munu
  logical, target, public :: fm_initialised_flic = .false.
  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean
  !real(dp), public :: bcx_flic = 1.0d0, bcy_flic = 1.0d0, bcz_flic = 1.0d0, bct_flic = 1.0d0
  public :: Dflic
  public :: Dflic_dag
  public :: Hsqflic
  public :: Hflic
  public :: InitialiseflicOperator
  public :: FinaliseflicOperator
  public :: InitialiseFermionMatrix_flic
  public :: FinaliseFermionMatrix_flic
  interface FinaliseflicOperator
     module procedure FinaliseFermionMatrix_flic
  end interface
  interface InitialiseflicOperator
     module procedure InitialiseFermionMatrix_flic
  end interface
contains
  subroutine InitialiseFermionMatrix_flic(U_xd, Usm_xd , F_munu, kappa, c_sw, u0, u0_sm)
    ! begin args: U_xd, Usm_xd , F_munu, kappa, c_sw, u0, u0_sm
    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd, Usm_xd , F_munu
    real(dp) :: kappa, c_sw
    real(dp), optional :: u0, u0_sm
    ! begin local_vars
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfic_sw, mfir, mfir_sm !coefficents to be absorbed.
    type(colour_matrix) :: U_mux, Usm_mux
    type(colour_matrix), dimension(nplaq) :: F_x
    !EO preconditioning is in the hopping parameter formalism.
    ! begin execution
    if ( .not. allocated(Up_xd) ) call CreateGaugeField(Up_xd,1)
    if ( .not. allocated(Um_xd) ) call CreateGaugeField(Um_xd,1)
    mfir = kappa
    if ( present(u0) ) mfir = mfir/u0
    mfir_sm = kappa
    if ( present(u0_sm) ) mfir_sm = mfir_sm/u0_sm
    !Absorb the mean field improvement into the gauge fields.
    !Additional factor of a half due to split link trick.
    mfir = 0.5d0*mfir
    mfir_sm = 0.5d0*mfir_sm
    !Absorb the mean field improvement into the gauge fields.
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          U_mux%Cl = mfir*U_xd(ix,iy,iz,it,mu)%Cl
          Usm_mux%Cl = mfir_sm*Usm_xd(ix,iy,iz,it,mu)%Cl
          Up_xd(ix,iy,iz,it,mu)%Cl = Usm_mux%Cl + U_mux%Cl
          Um_xd(ix,iy,iz,it,mu)%Cl = Usm_mux%Cl - U_mux%Cl
       end do; end do; end do; end do
    end do
    call SetBoundaryConditions(Up_xd,ff_bcx, ff_bcy, ff_bcz, ff_bct)
    call ShadowGaugeField(Up_xd,1)
    call SetBoundaryConditions(Um_xd,ff_bcx, ff_bcy, ff_bcz, ff_bct)
    call ShadowGaugeField(Um_xd,1)
    mfic_sw = kappa*c_sw
    if ( present(u0_sm) ) mfic_sw = mfic_sw/(u0_sm**4)
    if ( .not. allocated(Fcl_munu) ) allocate(Fcl_munu(nxp,nyp,nzp,ntp,nplaq))
    !Clover term
    ! iplaq = 1
    ! also, iplaq = 6
    ! sigmaphi(:,:,:,:,1:2,:) = -sigmaphi(:,:,:,:,1:2,:)
    ! iplaq = 2
    ! also, iplaq = 5
    ! sigmaphi(:,:,:,:,3:4,:) = -sigmaphi(:,:,:,:,3:4,:)
    ! iplaq = 3
    ! also, iplaq = 4
    ! sigmaphi(:,:,:,:,1:2,:) = -sigmaphi(:,:,:,:,1:2,:)
    !code is in the chiral basis so we only need these sums and differences. (halve multiplies)
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       F_x(1) = F_munu(ix,iy,iz,it,1)
       F_x(2) = F_munu(ix,iy,iz,it,2)
       F_x(3) = F_munu(ix,iy,iz,it,3)
       F_x(4) = F_munu(ix,iy,iz,it,4)
       F_x(5) = F_munu(ix,iy,iz,it,5)
       F_x(6) = F_munu(ix,iy,iz,it,6)
       Fcl_munu(ix,iy,iz,it,1)%Cl = mfic_sw*(F_x(1)%Cl - F_x(6)%Cl)
       Fcl_munu(ix,iy,iz,it,2)%Cl = mfic_sw*(F_x(2)%Cl + F_x(5)%Cl)
       Fcl_munu(ix,iy,iz,it,3)%Cl = mfic_sw*(F_x(3)%Cl - F_x(4)%Cl)
       Fcl_munu(ix,iy,iz,it,4)%Cl = mfic_sw*(F_x(1)%Cl + F_x(6)%Cl)
       Fcl_munu(ix,iy,iz,it,5)%Cl = mfic_sw*(F_x(2)%Cl - F_x(5)%Cl)
       Fcl_munu(ix,iy,iz,it,6)%Cl = mfic_sw*(F_x(3)%Cl + F_x(4)%Cl)
    end do; end do; end do; end do
    fm_initialised_flic = .true.
  end subroutine InitialiseFermionMatrix_flic
  subroutine FinaliseFermionMatrix_flic
    fm_initialised_flic = .false.
    if ( allocated(Up_xd) ) call DestroyGaugeField(Up_xd)
    if ( allocated(Um_xd) ) call DestroyGaugeField(Um_xd)
    if ( allocated(Fcl_munu) ) deallocate(Fcl_munu)
  end subroutine FinaliseFermionMatrix_flic
  subroutine Hsqflic(phi,Hsqphi)
    ! begin args: phi, Hsqphi
    type(colour_vector), dimension(:,:,:,:,:) :: phi, Hsqphi
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Dphi
    ! begin execution
    allocate(Dphi(nxp,nyp,nzp,ntp,ns))
    call Hflic(phi,Dphi)
    call Hflic(Dphi,Hsqphi)
    deallocate(Dphi)
  end subroutine Hsqflic
  subroutine Hflic(phi,Hphi)
    ! begin args: phi, Hphi
    type(colour_vector), dimension(:,:,:,:,:) :: phi, Hphi
    ! begin local_vars
    integer :: ix,iy,iz,it
    ! begin execution
    call Dflic(phi,Hphi)
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       Hphi(ix,iy,iz,it,3)%cl(:) = -Hphi(ix,iy,iz,it,3)%cl(:)
       Hphi(ix,iy,iz,it,4)%cl(:) = -Hphi(ix,iy,iz,it,4)%cl(:)
    end do ; end do; end do; end do
  end subroutine Hflic
  subroutine Dflic_dag(phi, Dphi)
    ! begin args: phi, Dphi, i_eo
    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    integer :: ix,iy,iz,it
    ! begin local_vars
    ! begin execution
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       phi(ix,iy,iz,it,3)%cl(:) = -phi(ix,iy,iz,it,3)%cl(:)
       phi(ix,iy,iz,it,4)%cl(:) = -phi(ix,iy,iz,it,4)%cl(:)
    end do ; end do; end do; end do
    call Dflic(phi,Dphi)
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       phi(ix,iy,iz,it,3)%cl(:) = -phi(ix,iy,iz,it,3)%cl(:)
       phi(ix,iy,iz,it,4)%cl(:) = -phi(ix,iy,iz,it,4)%cl(:)
       Dphi(ix,iy,iz,it,3)%cl(:) = -Dphi(ix,iy,iz,it,3)%cl(:)
       Dphi(ix,iy,iz,it,4)%cl(:) = -Dphi(ix,iy,iz,it,4)%cl(:)
    end do ; end do; end do; end do
  end subroutine Dflic_dag
  subroutine GetflicShadow(phi)
    ! begin args: phi, ishdw
    type(colour_vector), dimension(:,:,:,:,:) :: phi
    integer :: ishdw
    ! begin local_vars
    integer :: ierror, intime, outtime
    integer :: is, it, src_rank, dest_rank, nxd, nyd, nzd, ntd
    integer :: sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2), sendrecv_reqzs(nts,ns,2), ierr
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2), sendrecvzs_status(nmpi_status,nts*ns*2)
    real(dp) :: t_in, t_out
    ! begin execution
    t_in = mpi_wtime()
    nxd = size(phi,1)
    nyd = size(phi,2)
    nzd = size(phi,3)
    ntd = size(phi,4)
    ishdw = 1
    if ( nprocz > 1 ) then
       dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
       src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)
       do is=1,ns
          do it=1,nt
             call MPI_ISSend(phi(1,1,mapz(1) ,it,is), nc*nx*ny,mpi_dc,dest_rank,it+ns*is,&
                  & mpi_comm,sendrecv_reqz(it,is,1),ierr)
             call MPI_IRecv (phi(1,1,mapz(nz+1),it,is), nc*nx*ny, mpi_dc, src_rank, it+ns*is, &
                  & mpi_comm,sendrecv_reqz(it,is,2),ierr)
          end do
       end do
       call MPI_WaitAll(nt*ns*2,sendrecv_reqz,sendrecvz_status,ierr)
       dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)
       src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
       do is=1,ns
          do it=1,nt
             call MPI_ISSend(phi(1,1,mapz(nz),it,is), nc*nx*ny, mpi_dc, dest_rank, it+ns*is, &
                  & mpi_comm, sendrecv_reqz(it,is,1), ierr)
             call MPI_IRecv (phi(1,1,mapz(0) ,it,is), nc*nx*ny, mpi_dc, src_rank, it+ns*is, &
                  & mpi_comm, sendrecv_reqz(it,is,2), ierr)
          end do
       end do
       call MPI_WaitAll(nt*ns*2,sendrecv_reqz,sendrecvz_status,ierr)
    end if
    if ( nproct > 1 ) then
       dest_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
       src_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)
       do is=1,ns
          call MPI_ISSend(phi(1,1,1,mapt(1) ,is), nc*nx*ny*nz, mpi_dc, dest_rank, is, &
               & mpi_comm, sendrecv_reqt(is,1), ierr)
          call MPI_IRecv (phi(1,1,1,mapt(nt+1),is), nc*nx*ny*nz, mpi_dc, src_rank, is, &
               & mpi_comm, sendrecv_reqt(is,2), ierr)
       end do
       call MPI_WaitAll(nd*2,sendrecv_reqt,sendrecvt_status,ierr)
       dest_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)
       src_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
       do is=1,ns
          call MPI_ISSend(phi(1,1,1,mapt(nt),is), nc*nx*ny*nz, mpi_dc, dest_rank, is, &
               & mpi_comm, sendrecv_reqt(is,1), ierr)
          call MPI_IRecv (phi(1,1,1,mapt(0) ,is), nc*nx*ny*nz, mpi_dc, src_rank, is, &
               & mpi_comm, sendrecv_reqt(is,2), ierr)
       end do
       call MPI_WaitAll(ns*2,sendrecv_reqt,sendrecvt_status,ierr)
    end if
    t_out = mpi_wtime()
    call TimingUpdate(nshdwff,t_out,t_in,mint_shdwff,maxt_shdwff,t_shdwff)
  end subroutine GetflicShadow
  subroutine Dflic(phi, Dphi)
    ! begin args: phi, Dphi, i_eo
    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    ! i_eo = 0 => takes odd sites to even sites.
    ! i_eo = 1 => takes even sites to odd sites.
    ! begin local_vars
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt
    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)
    ! begin execution
    !if (timing) intime = mpi_wtime()
    !commtime = 0.0d0
    call GetflicShadow(phi)
    Dphi = phi
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      !Dphi(1:2) = Dphi - (F_12 - F_34) sigma_12 phi
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,1)%Cl(1,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(1,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(1,3)*phi(ix,iy,iz,it,1)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,1)%Cl(2,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(2,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(2,3)*phi(ix,iy,iz,it,1)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,1)%Cl(3,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(3,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(3,3)*phi(ix,iy,iz,it,1)%cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,1)%Cl(1,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(1,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(1,3)*phi(ix,iy,iz,it,2)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,1)%Cl(2,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(2,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(2,3)*phi(ix,iy,iz,it,2)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,1)%Cl(3,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(3,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,1)%Cl(3,3)*phi(ix,iy,iz,it,2)%cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      !Dphi(1:2) = Dphi - (F_13 + F_24) sigma_13 phi
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,2)%Cl(1,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(1,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(1,3)*phi(ix,iy,iz,it,1)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,2)%Cl(2,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(2,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(2,3)*phi(ix,iy,iz,it,1)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,2)%Cl(3,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(3,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(3,3)*phi(ix,iy,iz,it,1)%cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,2)%Cl(1,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(1,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(1,3)*phi(ix,iy,iz,it,2)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,2)%Cl(2,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(2,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(2,3)*phi(ix,iy,iz,it,2)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,2)%Cl(3,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(3,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,2)%Cl(3,3)*phi(ix,iy,iz,it,2)%cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) + psi_x%Cl(3)
      !Dphi(1:2) = Dphi - (F_14 - F_23) sigma_14 phi
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,3)%Cl(1,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(1,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(1,3)*phi(ix,iy,iz,it,1)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,3)%Cl(2,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(2,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(2,3)*phi(ix,iy,iz,it,1)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,3)%Cl(3,1)*phi(ix,iy,iz,it,1)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(3,2)*phi(ix,iy,iz,it,1)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(3,3)*phi(ix,iy,iz,it,1)%cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,3)%Cl(1,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(1,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(1,3)*phi(ix,iy,iz,it,2)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,3)%Cl(2,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(2,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(2,3)*phi(ix,iy,iz,it,2)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,3)%Cl(3,1)*phi(ix,iy,iz,it,2)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(3,2)*phi(ix,iy,iz,it,2)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,3)%Cl(3,3)*phi(ix,iy,iz,it,2)%cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      !Dphi(3:4) = Dphi - (F_12 + F_34) sigma_12 phi
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,4)%Cl(1,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(1,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(1,3)*phi(ix,iy,iz,it,3)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,4)%Cl(2,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(2,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(2,3)*phi(ix,iy,iz,it,3)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,4)%Cl(3,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(3,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(3,3)*phi(ix,iy,iz,it,3)%cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,4)%Cl(1,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(1,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(1,3)*phi(ix,iy,iz,it,4)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,4)%Cl(2,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(2,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(2,3)*phi(ix,iy,iz,it,4)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,4)%Cl(3,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(3,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,4)%Cl(3,3)*phi(ix,iy,iz,it,4)%cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      !Dphi(3:4) = Dphi - (F_13 - F_24) sigma_13 phi
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,5)%Cl(1,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(1,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(1,3)*phi(ix,iy,iz,it,3)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,5)%Cl(2,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(2,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(2,3)*phi(ix,iy,iz,it,3)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,5)%Cl(3,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(3,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(3,3)*phi(ix,iy,iz,it,3)%cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - psi_x%Cl(3)
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,5)%Cl(1,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(1,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(1,3)*phi(ix,iy,iz,it,4)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,5)%Cl(2,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(2,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(2,3)*phi(ix,iy,iz,it,4)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,5)%Cl(3,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(3,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,5)%Cl(3,3)*phi(ix,iy,iz,it,4)%cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + psi_x%Cl(3)
      !Dphi(3:4) = Dphi - (F_14 + F_23) sigma_14 phi
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,6)%Cl(1,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(1,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(1,3)*phi(ix,iy,iz,it,3)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,6)%Cl(2,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(2,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(2,3)*phi(ix,iy,iz,it,3)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,6)%Cl(3,1)*phi(ix,iy,iz,it,3)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(3,2)*phi(ix,iy,iz,it,3)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(3,3)*phi(ix,iy,iz,it,3)%cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      psi_x%Cl(1) = Fcl_munu(ix,iy,iz,it,6)%Cl(1,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(1,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(1,3)*phi(ix,iy,iz,it,4)%cl(3)
      psi_x%Cl(2) = Fcl_munu(ix,iy,iz,it,6)%Cl(2,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(2,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(2,3)*phi(ix,iy,iz,it,4)%cl(3)
      psi_x%Cl(3) = Fcl_munu(ix,iy,iz,it,6)%Cl(3,1)*phi(ix,iy,iz,it,4)%cl(1) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(3,2)*phi(ix,iy,iz,it,4)%cl(2) + &
           & Fcl_munu(ix,iy,iz,it,6)%Cl(3,3)*phi(ix,iy,iz,it,4)%cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 1
      jx = mapx(ix + 1)
      ! G_1^- Up phi_xpmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(jx,iy,iz,it,1)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(1)),real(phi(jx,iy,iz,it,4)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(jx,iy,iz,it,1)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(2)),real(phi(jx,iy,iz,it,4)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(jx,iy,iz,it,1)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(3)),real(phi(jx,iy,iz,it,4)%Cl(3)),dc)
      psi_x%Cl(1) = Up_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(jx,iy,iz,it,2)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(1)),real(phi(jx,iy,iz,it,3)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(jx,iy,iz,it,2)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(2)),real(phi(jx,iy,iz,it,3)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(jx,iy,iz,it,2)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(3)),real(phi(jx,iy,iz,it,3)%Cl(3)),dc)
      psi_x%Cl(1) = Up_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! G_1^+ Um phi_xpmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(jx,iy,iz,it,1)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(1)),real(phi(jx,iy,iz,it,4)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(jx,iy,iz,it,1)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(2)),real(phi(jx,iy,iz,it,4)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(jx,iy,iz,it,1)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(3)),real(phi(jx,iy,iz,it,4)%Cl(3)),dc)
      psi_x%Cl(1) = Um_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(jx,iy,iz,it,2)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(1)),real(phi(jx,iy,iz,it,3)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(jx,iy,iz,it,2)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(2)),real(phi(jx,iy,iz,it,3)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(jx,iy,iz,it,2)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(3)),real(phi(jx,iy,iz,it,3)%Cl(3)),dc)
      psi_x%Cl(1) = Um_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 2
      jy = mapy(iy + 1)
      ! G_2^- Up phi_xpmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,jy,iz,it,1)%Cl(1) + phi(ix,jy,iz,it,4)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,jy,iz,it,1)%Cl(2) + phi(ix,jy,iz,it,4)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,jy,iz,it,1)%Cl(3) + phi(ix,jy,iz,it,4)%Cl(3)
      psi_x%Cl(1) = Up_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,jy,iz,it,2)%Cl(1) - phi(ix,jy,iz,it,3)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,jy,iz,it,2)%Cl(2) - phi(ix,jy,iz,it,3)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,jy,iz,it,2)%Cl(3) - phi(ix,jy,iz,it,3)%Cl(3)
      psi_x%Cl(1) = Up_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + psi_x%Cl(3)
      ! G_2^+ Um phi_xpmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,jy,iz,it,1)%Cl(1) - phi(ix,jy,iz,it,4)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,jy,iz,it,1)%Cl(2) - phi(ix,jy,iz,it,4)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,jy,iz,it,1)%Cl(3) - phi(ix,jy,iz,it,4)%Cl(3)
      psi_x%Cl(1) = Um_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,jy,iz,it,2)%Cl(1) + phi(ix,jy,iz,it,3)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,jy,iz,it,2)%Cl(2) + phi(ix,jy,iz,it,3)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,jy,iz,it,2)%Cl(3) + phi(ix,jy,iz,it,3)%Cl(3)
      psi_x%Cl(1) = Um_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - psi_x%Cl(3)
      ! mu = 3
      jz = mapz(iz + 1)
      ! G_3^- Up phi_xpmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,iy,jz,it,1)%Cl(1) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(1)),real(phi(ix,iy,jz,it,3)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(ix,iy,jz,it,1)%Cl(2) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(2)),real(phi(ix,iy,jz,it,3)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(ix,iy,jz,it,1)%Cl(3) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(3)),real(phi(ix,iy,jz,it,3)%Cl(3)),dc)
      psi_x%Cl(1) = Up_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,iy,jz,it,2)%Cl(1) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(1)),real(phi(ix,iy,jz,it,4)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(ix,iy,jz,it,2)%Cl(2) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(2)),real(phi(ix,iy,jz,it,4)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(ix,iy,jz,it,2)%Cl(3) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(3)),real(phi(ix,iy,jz,it,4)%Cl(3)),dc)
      psi_x%Cl(1) = Up_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! G_3^+ Um phi_xpmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,iy,jz,it,1)%Cl(1) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(1)),real(phi(ix,iy,jz,it,3)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(ix,iy,jz,it,1)%Cl(2) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(2)),real(phi(ix,iy,jz,it,3)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(ix,iy,jz,it,1)%Cl(3) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(3)),real(phi(ix,iy,jz,it,3)%Cl(3)),dc)
      psi_x%Cl(1) = Um_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,iy,jz,it,2)%Cl(1) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(1)),real(phi(ix,iy,jz,it,4)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(ix,iy,jz,it,2)%Cl(2) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(2)),real(phi(ix,iy,jz,it,4)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(ix,iy,jz,it,2)%Cl(3) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(3)),real(phi(ix,iy,jz,it,4)%Cl(3)),dc)
      psi_x%Cl(1) = Um_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 4
      jt = mapt(it + 1)
      ! G_4^- Up phi_xpmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,iy,iz,jt,1)%Cl(1) - phi(ix,iy,iz,jt,3)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,iy,iz,jt,1)%Cl(2) - phi(ix,iy,iz,jt,3)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,iy,iz,jt,1)%Cl(3) - phi(ix,iy,iz,jt,3)%Cl(3)
      psi_x%Cl(1) = Up_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,iy,iz,jt,2)%Cl(1) - phi(ix,iy,iz,jt,4)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,iy,iz,jt,2)%Cl(2) - phi(ix,iy,iz,jt,4)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,iy,iz,jt,2)%Cl(3) - phi(ix,iy,iz,jt,4)%Cl(3)
      psi_x%Cl(1) = Up_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Up_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Up_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Up_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + psi_x%Cl(3)
      ! G_4^+ Um phi_xpmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,iy,iz,jt,1)%Cl(1) + phi(ix,iy,iz,jt,3)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,iy,iz,jt,1)%Cl(2) + phi(ix,iy,iz,jt,3)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,iy,iz,jt,1)%Cl(3) + phi(ix,iy,iz,jt,3)%Cl(3)
      psi_x%Cl(1) = Um_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,iy,iz,jt,2)%Cl(1) + phi(ix,iy,iz,jt,4)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,iy,iz,jt,2)%Cl(2) + phi(ix,iy,iz,jt,4)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,iy,iz,jt,2)%Cl(3) + phi(ix,iy,iz,jt,4)%Cl(3)
      psi_x%Cl(1) = Um_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
      psi_x%Cl(2) = Um_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
      psi_x%Cl(3) = Um_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
           & Um_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - psi_x%Cl(3)
      ! mu = 1
      jx = mapx(ix - 1)
      ! G_1^+ Up phi_xmmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(jx,iy,iz,it,1)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(1)),real(phi(jx,iy,iz,it,4)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(jx,iy,iz,it,1)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(2)),real(phi(jx,iy,iz,it,4)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(jx,iy,iz,it,1)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(3)),real(phi(jx,iy,iz,it,4)%Cl(3)),dc)
      psi_x%Cl(1) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(jx,iy,iz,it,2)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(1)),real(phi(jx,iy,iz,it,3)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(jx,iy,iz,it,2)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(2)),real(phi(jx,iy,iz,it,3)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(jx,iy,iz,it,2)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(3)),real(phi(jx,iy,iz,it,3)%Cl(3)),dc)
      psi_x%Cl(1) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! G_1^- Um phi_xmmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(jx,iy,iz,it,1)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(1)),real(phi(jx,iy,iz,it,4)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(jx,iy,iz,it,1)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(2)),real(phi(jx,iy,iz,it,4)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(jx,iy,iz,it,1)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(3)),real(phi(jx,iy,iz,it,4)%Cl(3)),dc)
      psi_x%Cl(1) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(jx,iy,iz,it,2)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(1)),real(phi(jx,iy,iz,it,3)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(jx,iy,iz,it,2)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(2)),real(phi(jx,iy,iz,it,3)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(jx,iy,iz,it,2)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(3)),real(phi(jx,iy,iz,it,3)%Cl(3)),dc)
      psi_x%Cl(1) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 2
      jy = mapy(iy - 1)
      ! G_2^+ Up phi_xmmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,jy,iz,it,1)%Cl(1) - phi(ix,jy,iz,it,4)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,jy,iz,it,1)%Cl(2) - phi(ix,jy,iz,it,4)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,jy,iz,it,1)%Cl(3) - phi(ix,jy,iz,it,4)%Cl(3)
      psi_x%Cl(1) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,jy,iz,it,2)%Cl(1) + phi(ix,jy,iz,it,3)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,jy,iz,it,2)%Cl(2) + phi(ix,jy,iz,it,3)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,jy,iz,it,2)%Cl(3) + phi(ix,jy,iz,it,3)%Cl(3)
      psi_x%Cl(1) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - psi_x%Cl(3)
      ! G_2^- Um phi_xmmu
      ! i_s = 1
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,jy,iz,it,1)%Cl(1) + phi(ix,jy,iz,it,4)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,jy,iz,it,1)%Cl(2) + phi(ix,jy,iz,it,4)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,jy,iz,it,1)%Cl(3) + phi(ix,jy,iz,it,4)%Cl(3)
      psi_x%Cl(1) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - psi_x%Cl(3)
      ! i_s = 2
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,jy,iz,it,2)%Cl(1) - phi(ix,jy,iz,it,3)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,jy,iz,it,2)%Cl(2) - phi(ix,jy,iz,it,3)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,jy,iz,it,2)%Cl(3) - phi(ix,jy,iz,it,3)%Cl(3)
      psi_x%Cl(1) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + psi_x%Cl(3)
      ! mu = 3
      jz = mapz(iz - 1)
      ! G_3^+ Up phi_xmmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,iy,jz,it,1)%Cl(1) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(1)),real(phi(ix,iy,jz,it,3)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(ix,iy,jz,it,1)%Cl(2) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(2)),real(phi(ix,iy,jz,it,3)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(ix,iy,jz,it,1)%Cl(3) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(3)),real(phi(ix,iy,jz,it,3)%Cl(3)),dc)
      psi_x%Cl(1) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,iy,jz,it,2)%Cl(1) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(1)),real(phi(ix,iy,jz,it,4)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(ix,iy,jz,it,2)%Cl(2) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(2)),real(phi(ix,iy,jz,it,4)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(ix,iy,jz,it,2)%Cl(3) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(3)),real(phi(ix,iy,jz,it,4)%Cl(3)),dc)
      psi_x%Cl(1) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! G_3^- Um phi_xmmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,iy,jz,it,1)%Cl(1) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(1)),real(phi(ix,iy,jz,it,3)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(ix,iy,jz,it,1)%Cl(2) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(2)),real(phi(ix,iy,jz,it,3)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(ix,iy,jz,it,1)%Cl(3) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(3)),real(phi(ix,iy,jz,it,3)%Cl(3)),dc)
      psi_x%Cl(1) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,iy,jz,it,2)%Cl(1) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(1)),real(phi(ix,iy,jz,it,4)%Cl(1)),dc)
      Gammaphi%Cl(2) = phi(ix,iy,jz,it,2)%Cl(2) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(2)),real(phi(ix,iy,jz,it,4)%Cl(2)),dc)
      Gammaphi%Cl(3) = phi(ix,iy,jz,it,2)%Cl(3) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(3)),real(phi(ix,iy,jz,it,4)%Cl(3)),dc)
      psi_x%Cl(1) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)
      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)
      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)
      ! mu = 4
      jt = mapt(it - 1)




      ! G_4^+ Up phi_xmmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,iy,iz,jt,1)%Cl(1) + phi(ix,iy,iz,jt,3)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,iy,iz,jt,1)%Cl(2) + phi(ix,iy,iz,jt,3)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,iy,iz,jt,1)%Cl(3) + phi(ix,iy,iz,jt,3)%Cl(3)

      psi_x%Cl(1) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)

      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) - psi_x%Cl(3)

      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,iy,iz,jt,2)%Cl(1) + phi(ix,iy,iz,jt,4)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,iy,iz,jt,2)%Cl(2) + phi(ix,iy,iz,jt,4)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,iy,iz,jt,2)%Cl(3) + phi(ix,iy,iz,jt,4)%Cl(3)

      psi_x%Cl(1) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)

      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) - psi_x%Cl(3)


      ! G_4^- Um phi_xmmu
      ! i_s = 1
      ! j_s = 3
      Gammaphi%Cl(1) = phi(ix,iy,iz,jt,1)%Cl(1) - phi(ix,iy,iz,jt,3)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,iy,iz,jt,1)%Cl(2) - phi(ix,iy,iz,jt,3)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,iy,iz,jt,1)%Cl(3) - phi(ix,iy,iz,jt,3)%Cl(3)

      psi_x%Cl(1) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

      Dphi(ix,iy,iz,it,1)%cl(1) = Dphi(ix,iy,iz,it,1)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,1)%cl(2) = Dphi(ix,iy,iz,it,1)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,1)%cl(3) = Dphi(ix,iy,iz,it,1)%cl(3) - psi_x%Cl(3)

      Dphi(ix,iy,iz,it,3)%cl(1) = Dphi(ix,iy,iz,it,3)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,3)%cl(2) = Dphi(ix,iy,iz,it,3)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,3)%cl(3) = Dphi(ix,iy,iz,it,3)%cl(3) + psi_x%Cl(3)

      ! i_s = 2
      ! j_s = 4
      Gammaphi%Cl(1) = phi(ix,iy,iz,jt,2)%Cl(1) - phi(ix,iy,iz,jt,4)%Cl(1)
      Gammaphi%Cl(2) = phi(ix,iy,iz,jt,2)%Cl(2) - phi(ix,iy,iz,jt,4)%Cl(2)
      Gammaphi%Cl(3) = phi(ix,iy,iz,jt,2)%Cl(3) - phi(ix,iy,iz,jt,4)%Cl(3)

      psi_x%Cl(1) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
      psi_x%Cl(2) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
      psi_x%Cl(3) = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
           & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

      Dphi(ix,iy,iz,it,2)%cl(1) = Dphi(ix,iy,iz,it,2)%cl(1) - psi_x%Cl(1)
      Dphi(ix,iy,iz,it,2)%cl(2) = Dphi(ix,iy,iz,it,2)%cl(2) - psi_x%Cl(2)
      Dphi(ix,iy,iz,it,2)%cl(3) = Dphi(ix,iy,iz,it,2)%cl(3) - psi_x%Cl(3)

      Dphi(ix,iy,iz,it,4)%cl(1) = Dphi(ix,iy,iz,it,4)%cl(1) + psi_x%Cl(1)
      Dphi(ix,iy,iz,it,4)%cl(2) = Dphi(ix,iy,iz,it,4)%cl(2) + psi_x%Cl(2)
      Dphi(ix,iy,iz,it,4)%cl(3) = Dphi(ix,iy,iz,it,4)%cl(3) + psi_x%Cl(3)
    end do; end do; end do; end do
    !if (timing) then
    ! outtime = mpi_wtime()
    ! call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    !end if
    !call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
    !waste = commtime/(outtime - intime)
    !call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)
  end subroutine Dflic
end module flicMatrix
