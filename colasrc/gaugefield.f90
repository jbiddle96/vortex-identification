!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/gaugefield.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: gaugefield.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

!Author: Waseem Kamleh
!Date: May, 2001

module GaugeField

  use GaugeFieldMPIComms
  use VectorAlgebra
  use MatrixAlgebra
  use GL3Diag
#ifdef _hydra_
  use IFLPort, no_abort => abort
#endif

  use ReduceOps
  use CollectiveOps
  use ColourFieldOps
  use LatticeSize
  use MPIInterface
  use Kinds
  use ColourTypes
  implicit none
  private

  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public  :: U_xd

  real(DP), public :: beta,lastPlaq,plaqbarAvg,uzero,u0_bar = 1.0d0

  public :: FixGaugeField
  public :: FixSU3Matrix
  public :: GetUZero
  public :: GetPlaqBar
  public :: SetBoundaryConditions
  public :: AddPlaqBar_munu
  public :: GaugeRotate
  public :: UndoGaugeRotate
  public :: ContraGaugeRotate
  public :: RandomSU3Matrix
  public :: RandomSU3Field
  public :: RandomGaugeField

  interface SetBoundaryConditions
     module procedure SetBoundaryConditions_4x
     module procedure SetBoundaryConditions_eo
  end interface
contains

  subroutine FixGaugeField(U_xd)
    ! begin args: U_xd

    type(colour_matrix), dimension(:,:,:,:,:), intent(inout) :: U_xd
    ! begin local_vars
    integer :: ix, iy, iz, it, id

    ! begin execution

    do id=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          call FixSU3Matrix(U_xd(ix,iy,iz,it,id))

       end do; end do; end do; end do
    end do


  end subroutine FixGaugeField

  elemental subroutine FixSU3Matrix(U_x)
    ! begin args: U_x

    type(colour_matrix), intent(inout) :: U_x

    ! begin local_vars
    type(colour_vector) :: v1,v2,v3

    ! begin execution

    v1%Cl(:) = U_x%Cl(1,:)

    call normalise_vector(v1)

    v2%Cl(:) = U_x%Cl(2,:)

    call orthogonalise_vectors(v2,v1)

    call normalise_vector(v2)

    call vector_product(v3,v1,v2)

    call normalise_vector(v3)
    
    U_x%Cl(1,:) = v1%Cl(:)
    U_x%Cl(2,:) = v2%Cl(:)
    U_x%Cl(3,:) = v3%Cl(:)

    !TO DO: Rework this to not modify rows 1 and 2?

  end subroutine FixSU3Matrix

  subroutine GetUZero(U_xd, uzero)
    ! begin args: U_xd, uzero

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: uzero

    ! begin local_vars
    integer :: mu, nu
    real(dp) :: plaqbar, plaqbarpp

    ! begin execution

    plaqbarpp = 0.0d0

    do mu=1,nd
       do nu=mu+1,nd

          call AddPlaqBar_munu(plaqbarpp,U_xd,mu,nu)

       end do
    end do

    call AllSum(plaqbarpp, plaqbar)
    !u_0 = (Mean (1/3)ReTr U_munu)^(1/4)
    uzero = (plaqbar/(nlattice*nplaq*nc))**0.25d0


  end subroutine GetUZero

  function GetPlaqBar(U_xd) result(plaqbar)
    ! begin args: U_xd

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    ! begin local_vars
    real(dp) :: uzero

    integer :: mu, nu
    real(dp) :: plaqbar, plaqbarpp

    ! begin execution

    plaqbarpp = 0.0d0

    do mu=1,nd
       do nu=mu+1,nd

          call AddPlaqBar_munu(plaqbarpp,U_xd,mu,nu)

       end do
    end do

    call AllSum(plaqbarpp, plaqbar)


  end function GetPlaqBar

  subroutine SetBoundaryConditions_4x(U_xd, bcx, bcy, bcz, bct)
    ! begin args: U_xd, bcx, bcy, bcz, bct

    type(colour_matrix), dimension(:,:,:,:,:), intent(out) :: U_xd
    real(dp) :: bcx, bcy, bcz, bct

    ! begin execution

    if ( bcx /= 1.0d0 .and. j_nx == nlx ) U_xd(nx,:,:,:,1) = bcx*U_xd(nx,:,:,:,1)
    if ( bcy /= 1.0d0 .and. j_ny == nly ) U_xd(:,ny,:,:,2) = bcy*U_xd(:,ny,:,:,2)
    if ( bcz /= 1.0d0 .and. j_nz == nlz ) U_xd(:,:,nz,:,3) = bcz*U_xd(:,:,nz,:,3)
    if ( bct /= 1.0d0 .and. j_nt == nlt ) U_xd(:,:,:,nt,4) = bct*U_xd(:,:,:,nt,4)


  end subroutine SetBoundaryConditions_4x

  subroutine SetBoundaryConditions_eo(U_xd, bcx, bcy, bcz, bct)
    ! begin args: U_xd, bcx, bcy, bcz, bct

    type(colour_matrix), dimension(:,:,0:), intent(out) :: U_xd
    real(dp) :: bcx, bcy, bcz, bct
    integer :: i_xeo, i_eo,ix,iy,iz,it
    ! begin execution

    if ( bcx /= 1.0d0 .and. j_nx == nlx ) then
       do it=1,nt; do iz=1,nz; do iy=1,ny
          i_xeo = x2eo( (/nx,iy,iz,it/) )
          i_eo = site_eo( (/nx,iy,iz,it/) )
          U_xd(i_xeo,1,i_eo) = bcx*U_xd(i_xeo,1,i_eo)
       end do; end do; end do
    end if

    if ( bcy /= 1.0d0 .and. j_ny == nly ) then
       do it=1,nt; do iz=1,nz; do ix=1,nx
          i_xeo = x2eo( (/ix,ny,iz,it/) )
          i_eo = site_eo( (/ix,ny,iz,it/) )
          U_xd(i_xeo,2,i_eo) = bcy*U_xd(i_xeo,2,i_eo)
       end do; end do; end do
    end if

    if ( bcz /= 1.0d0 .and. j_nz == nlz ) then
       do it=1,nt; do iy=1,ny; do ix=1,nx
          i_xeo = x2eo( (/ix,iy,nz,it/) )
          i_eo = site_eo( (/ix,iy,nz,it/) )
          U_xd(i_xeo,3,i_eo) = bcz*U_xd(i_xeo,3,i_eo)
       end do; end do; end do
    end if

    if ( bct /= 1.0d0 .and. j_nt == nlt ) then
       do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_xeo = x2eo( (/ix,iy,iz,nt/) )
          i_eo = site_eo( (/ix,iy,iz,nt/) )
          U_xd(i_xeo,4,i_eo) = bct*U_xd(i_xeo,4,i_eo)
       end do; end do; end do
    end if

  end subroutine SetBoundaryConditions_eo

  subroutine AddPlaqBar_munu(plaqbar,U_xd,mu,nu)
    ! begin args: plaqbar, U_xd, mu, nu

    real(dp) :: plaqbar
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: mu, nu

    ! begin local_vars
    type(colour_matrix) :: UmuUnu,UmudagUnudag
    real(dp) :: plaq_x

    integer, dimension(nd) :: dmu, dnu
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt

#define U_mux U_xd(ix,iy,iz,it,mu)
#define U_nux U_xd(ix,iy,iz,it,nu)

#define U_muxpmu U_xd(jx,jy,jz,jt,mu)
#define U_nuxpmu U_xd(jx,jy,jz,jt,nu)

#define U_muxpnu U_xd(kx,ky,kz,kt,mu)
#define U_nuxpnu U_xd(kx,ky,kz,kt,nu)

    ! begin execution

    dmu = 0
    dnu = 0

    dmu(mu) = 1
    dnu(nu) = 1

    do it=1,nt
       jt = mapt(it + dmu(4))
       kt = mapt(it + dnu(4))
       do iz=1,nz
          jz = mapz(iz + dmu(3))
          kz = mapz(iz + dnu(3))
          do iy=1,ny
             jy = mapy(iy + dmu(2))
             ky = mapy(iy + dnu(2))
             do ix=1,nx
                jx = mapx(ix + dmu(1))
                kx = mapx(ix + dnu(1))

                call MultiplyMatMat(UmuUnu,U_mux,U_nuxpmu)
                call MultiplyMatDagMatDag(UmudagUnudag,U_muxpnu,U_nux)
                call RealTraceMultMatMat(plaq_x,UmuUnu,UmudagUnudag)

                plaqbar = plaqbar + plaq_x

             end do
          end do
       end do
    end do

#undef U_mux
#undef U_nux


  end subroutine AddPlaqBar_munu

  subroutine GaugeRotate(U_xd,G_l)
    ! begin args: U_xd, G_l

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:) :: G_l

    ! begin local_vars
    integer, dimension(nd) :: dmu
    integer :: ix,iy,iz,it, mu
    integer :: jx,jy,jz,jt
    type(colour_matrix) :: GU, GUGdag

#define U_mux U_xd(ix,iy,iz,it,mu)
#define G_x G_l(ix,iy,iz,it)
#define G_xpmu G_l(jx,jy,jz,jt)

    ! begin execution

    do mu=1,nd
       dmu = 0
       dmu(mu) = 1

       do it=1,nt
          jt = mapt(it + dmu(4))
          do iz=1,nz
             jz = mapz(iz + dmu(3))
             do iy=1,ny
                jy = mapy(iy + dmu(2))
                do ix=1,nx
                   jx = mapx(ix + dmu(1))

                   call MultiplyMatMat(GU,G_x,U_mux)
                   call MultiplyMatMatDag(GUGdag,GU,G_xpmu)

                   U_mux = GUGdag

                end do
             end do
          end do
       end do

    end do

#undef U_mux
#undef G_x
#undef G_xpmu


  end subroutine GaugeRotate

  subroutine UndoGaugeRotate(U_xd,G_l)
    ! begin args: U_xd, G_l

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:) :: G_l

    ! begin local_vars
    integer, dimension(nd) :: dmu
    integer :: ix,iy,iz,it, mu
    integer :: jx,jy,jz,jt
    type(colour_matrix) :: GU, GUGdag

#define U_mux U_xd(ix,iy,iz,it,mu)
#define G_x G_l(ix,iy,iz,it)
#define G_xpmu G_l(jx,jy,jz,jt)

    ! begin execution

    do mu=1,nd
       dmu = 0
       dmu(mu) = 1

       do it=1,nt
          jt = mapt(it + dmu(4))
          do iz=1,nz
             jz = mapz(iz + dmu(3))
             do iy=1,ny
                jy = mapy(iy + dmu(2))
                do ix=1,nx
                   jx = mapx(ix + dmu(1))

                   call MultiplyMatDagMat(GU,G_x,U_mux)
                   call MultiplyMatMat(GUGdag,GU,G_xpmu)

                   U_mux = GUGdag

                end do
             end do
          end do
       end do

    end do

#undef U_mux
#undef G_x
#undef G_xpmu


  end subroutine UndoGaugeRotate

  subroutine ContraGaugeRotate(U_xd,G_l)
    ! begin args: U_xd, G_l

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:) :: G_l

    ! begin local_vars
    integer, dimension(nd) :: dmu
    integer :: ix,iy,iz,it, mu
    integer :: jx,jy,jz,jt
    type(colour_matrix) :: GU, GUGdag

#define U_mux U_xd(ix,iy,iz,it,mu)
#define G_x G_l(ix,iy,iz,it)
#define G_xpmu G_l(jx,jy,jz,jt)

    ! begin execution

    do mu=1,nd
       dmu = 0
       dmu(mu) = 1

       do it=1,nt
          jt = mapt(it + dmu(4))
          do iz=1,nz
             jz = mapz(iz + dmu(3))
             do iy=1,ny
                jy = mapy(iy + dmu(2))
                do ix=1,nx
                   jx = mapx(ix + dmu(1))

                   call MultiplyMatMat(GU,G_xpmu,U_mux)
                   call MultiplyMatMatDag(GUGdag,GU,G_x)

                   U_mux = GUGdag

                end do
             end do
          end do
       end do

    end do

#undef U_mux
#undef G_x
#undef G_xpmu


  end subroutine ContraGaugeRotate

  subroutine RandomSU3Matrix(U_x)
    ! begin args: U_x

    type(colour_matrix) :: U_x

    ! begin local_vars
    real(dp), dimension(n_a) :: omega_a
    type(colour_matrix) :: H_x, V_x
    type(real_vector)   :: lambda_x
    type(colour_vector) :: theta_x
    integer :: i_a

    ! begin execution

    do i_a=1,n_a
#ifndef _hydra_
       call random_number(omega_a(i_a))
#else
       omega_a(i_a) = drand(0)
#endif
    end do

    !Generate an Hermitian colour matrix field based on omega_a

    !Calculate the exponent, sum_a omega_a*lambda_a
    !where lambda_a are the generators of SU(3), the Gellmann matrices
    H_x%Cl(1,1) =        omega_a(3) + omega_a(8)/sqrt(3.0d0)
    H_x%Cl(1,2) =  cmplx(omega_a(1), -omega_a(2),dc)
    H_x%Cl(1,3) =  cmplx(omega_a(4), -omega_a(5),dc)

    H_x%Cl(2,1) =  cmplx(omega_a(1),  omega_a(2),dc)
    H_x%Cl(2,2) =       -omega_a(3) + omega_a(8)/sqrt(3.0d0)
    H_x%Cl(2,3) =  cmplx(omega_a(6), -omega_a(7),dc)

    H_x%Cl(3,1) =  cmplx(omega_a(4),  omega_a(5),dc)
    H_x%Cl(3,2) =  cmplx(omega_a(6),  omega_a(7),dc)
    H_x%Cl(3,3) = -2.0d0*omega_a(8)/sqrt(3.0d0)

    !Generate and SU3 colour matrix field based on H_x
    call DiagonaliseMat(H_x, lambda_x, V_x)

    theta_x%Cl = exp((two_pi*I*0.5d0)*lambda_x%Cl)

    !Undiagonalise back to H_x basis, by setting U_l = V_x*D[theta_x]*V_x^dag,
    !where V_x is the matrix of eigenvectors.

    !Note: [M*D]_ij = M_ij*theta_j
    call MultiplyMatDiagMat(H_x,V_x,theta_x)

    call MultiplyMatMatDag(U_x,H_x,V_x)


  end subroutine RandomSU3Matrix

  subroutine RandomSU3Field(U_x)
    ! begin args: U_x

    type(colour_matrix), dimension(:,:,:,:) :: U_x

    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       call RandomSU3Matrix(U_x(ix,iy,iz,it))
       call FixSU3Matrix(U_x(ix,iy,iz,it))
    end do; end do; end do; end do


  end subroutine RandomSU3Field

  subroutine RandomGaugeField(U_xd)
    ! begin args: U_xd

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd

    ! begin local_vars
    integer :: ix,iy,iz,it,id

    ! begin execution

    do id=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
          call RandomSU3Matrix(U_xd(ix,iy,iz,it,id))
          call FixSU3Matrix(U_xd(ix,iy,iz,it,id))
       end do; end do; end do; end do
    end do


  end subroutine RandomGaugeField

end module GaugeField

