!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

#include "defines.h"

module FermionField

  use ColourTypes
  use FermionFieldMPIComms
  use GaugeField
  use RealFieldMPIComms
  use ComplexFieldMPIComms
  use MatrixAlgebra
  use VectorAlgebra
  use CollectiveOps
  use MPIInterface
  use ReduceOps
  use SpinorTypes
  use FermionTypes
  use LatticeSize
  use Kinds
#ifdef _hydra_
  use IFLPort, no_abort => abort
#endif
  implicit none
  private

  real(dp), public :: m_f , bct = 1.0d0! Fermion mass parameter
  logical, public :: Use_Hopping_Parameter = .false.

  ! Declare boundary conditions for the fermion field.
  real(dp), public :: ff_bcx = 1.0_dp, ff_bcy = 1.0_dp, ff_bcz = 1.0_dp, ff_bct = 1.0_dp
  
  public :: random_fermion_field
  public :: ComplexGaussianField
  public :: inner_product
  public :: real_inner_product
  public :: fermion_norm
  public :: fermion_normsq
  public :: normalise
  public :: ProjectVector
  public :: orthogonalise
  public :: ProjectVectorSpace
  public :: GammaPlusProject
  public :: GammaMinusProject
  public :: GammaPhi
  public :: SigmaPhi
  public :: GammaPhi_Chiral
  public :: SigmaPhi_Chiral
  public :: GammaPhi_Sakurai
  public :: SigmaPhi_Sakurai

  interface random_fermion_field
     module procedure random_fermion_field_4x
     module procedure random_fermion_field_eo
  end interface

  interface inner_product
     module procedure inner_product_4x
     module procedure inner_product_eo
  end interface

  interface real_inner_product
     module procedure real_inner_product_4x
     module procedure real_inner_product_eo
  end interface

  interface fermion_norm
     module procedure fermion_norm_4x
     module procedure fermion_norm_eo
  end interface

  interface fermion_normsq
     module procedure fermion_normsq_4x
     module procedure fermion_normsq_eo
  end interface

  interface normalise
     module procedure normalise_4x
     module procedure normalise_eo
  end interface

  interface ComplexGaussianField
     module procedure ComplexGaussianField_4x
     module procedure ComplexGaussianField_eo
  end interface

  interface ProjectVector
     module procedure ProjectVector_4x
     module procedure ProjectVector_eo
  end interface

  interface orthogonalise
     module procedure orthogonalise_4x
     module procedure orthogonalise_eo
  end interface

  interface ProjectVectorSpace
     module procedure ProjectVectorSpace_4x
     module procedure ProjectVectorSpace_eo
  end interface

  interface GammaPhi
#ifdef _chiral_basis_
     module procedure GammaPhi_Chiral
#endif
#ifdef _sakurai_basis_
     module procedure GammaPhi_Sakurai
#endif
  end interface

  interface SigmaPhi
#ifdef _chiral_basis_
     module procedure SigmaPhi_Chiral
#endif
#ifdef _sakurai_basis_
     module procedure SigmaPhi_Sakurai
#endif
  end interface

contains

  subroutine random_fermion_field_4x(psi)
    ! begin args: psi

    type(colour_vector), dimension(:,:,:,:,:) :: psi
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns

    ! begin execution

    ns = size(psi,5)

    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       call random_vector(psi(ix,iy,iz,it,is))
    end do; end do; end do; end do; end do


  end subroutine random_fermion_field_4x

  subroutine random_fermion_field_eo(psi)
    ! begin args: psi

    type(dirac_fermion), dimension(:) :: psi
    ! begin local_vars
    integer :: i_xeo

    ! begin execution

    do i_xeo = 1,n_xeo
       call random_dirac_fermion(psi(i_xeo))
    end do

  end subroutine random_fermion_field_eo

  ! let x in [0,1] be a random number field, with uniform distribution, p(x)dx = dx
  ! being the probability of generating a random number between x and x+dx.
  ! let y(x) be a random number field, then |p(y)dy| = |p(x)dx|, thus
  ! p(y) = p(x)|dx/dy|. Similarly, p(y1(x_i),y2(x_i)) = p(x1,x2)|d(x_i)/d(y_i)|.
  ! So we can generate 2 real Gaussian fields by making use of the following 2-d fields
  ! y1(x1,x2) = sqrt(-2*ln(x1))*cos(2*pi*x2), y2(x1,x2) = sqrt(-2*ln(x1))*sin(2*pi*x2). (Num Rec)

  subroutine ComplexGaussianField_4x(xi)
    ! begin args: xi

    type(colour_vector), dimension(:,:,:,:,:) :: xi
    ! begin local_vars
    real(dp) :: alpha, theta !random number fields
    integer :: ix,iy,iz,it,is,ic

    !Return complex random numbers according to P(xi) = exp(-abs(xi)**2)/pi = P(Re(xi))P(Im(xi))

    ! begin execution

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
          do ic=1,nc
#ifndef _hydra_
             call random_number(alpha)
             call random_number(theta)
#else
             alpha = drand(0)
             theta = drand(0)
#endif
             alpha = sqrt(-log(alpha))
             theta=two_pi*theta
             xi(ix,iy,iz,it,is)%Cl(ic) = cmplx(alpha*cos(theta),alpha*sin(theta),dc)

          end do
       end do; end do; end do; end do
    end do


  end subroutine ComplexGaussianField_4x

  subroutine ComplexGaussianField_eo(xi)
    ! begin args: xi

    type(dirac_fermion), dimension(:) :: xi
    ! begin local_vars
    real(dp) :: alpha, theta !random number fields
    integer :: i_xeo, is, ic

    !Return complex random numbers according to P(xi) = exp(-abs(xi)**2)/pi = P(Re(xi))P(Im(xi))

    ! begin execution
    do i_xeo = 1,n_xeo
       do is=1,ns
          do ic=1,nc
#ifndef _hydra_
             call random_number(alpha)
             call random_number(theta)
#else
             alpha = drand(0)
             theta = drand(0)
#endif
             alpha = sqrt(-log(alpha))
             theta=two_pi*theta
             xi(i_xeo)%cs(ic,is) = cmplx(alpha*cos(theta),alpha*sin(theta),dc)

          end do
       end do
    end do

  end subroutine ComplexGaussianField_eo

  function inner_product_4x(psi,phi) result (psidotphi)
    ! begin args: psi, phi

    type(colour_vector), dimension(:,:,:,:,:) :: psi,phi

    ! begin local_vars
    complex(dc) :: psidotphi, psidotphipp
    integer :: ix,iy,iz,it,is,ns

    ! begin execution

    ns = size(psi,5)

    psidotphipp = 0.0d0
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       psidotphipp = psidotphipp + sum(conjg(psi(ix,iy,iz,it,is)%Cl)*phi(ix,iy,iz,it,is)%Cl)
    end do; end do; end do; end do; end do

    call AllSum(psidotphipp,psidotphi)


  end function inner_product_4x

  function real_inner_product_4x(psi,phi) result (psidotphi)
    ! begin args: psi, phi

    type(colour_vector), dimension(:,:,:,:,:) :: psi,phi
    ! begin local_vars
    real(dp) :: psidotphi
    real(dp) :: psidotphipp
    integer :: ix,iy,iz,it,is,ns

    ! begin execution

    ns = size(psi,5)

    psidotphipp = 0.0d0
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       psidotphipp = psidotphipp + sum(real(conjg(psi(ix,iy,iz,it,is)%Cl)*phi(ix,iy,iz,it,is)%Cl))
    end do; end do; end do; end do; end do

    call AllSum(psidotphipp,psidotphi)


  end function real_inner_product_4x

  function fermion_norm_4x(psi) result (norm_psi)
    ! begin args: psi

    type(colour_vector), dimension(:,:,:,:,:) :: psi
    ! begin local_vars
    real(dp) :: norm_psi
    real(dp) :: norm_psipp
    integer :: ix,iy,iz,it,is,ns

    ! begin execution

    ns = size(psi,5)

    norm_psipp = 0.0d0
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       norm_psipp = norm_psipp +  sum(real(conjg(psi(ix,iy,iz,it,is)%Cl)*psi(ix,iy,iz,it,is)%Cl))
    end do; end do; end do; end do; end do

    call AllSum(norm_psipp,norm_psi)
    norm_psi = sqrt(norm_psi)


  end function fermion_norm_4x

  function fermion_normsq_4x(psi) result (normsq_psi)
    ! begin args: psi

    type(colour_vector), dimension(:,:,:,:,:) :: psi
    ! begin local_vars
    real(dp) :: normsq_psi
    real(dp) :: normsq_psipp
    integer :: ix,iy,iz,it,is,ns

    ! begin execution

    ns = size(psi,5)

    normsq_psipp = 0.0d0
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       normsq_psipp = normsq_psipp +  sum(real(conjg(psi(ix,iy,iz,it,is)%Cl)*psi(ix,iy,iz,it,is)%Cl))
    end do; end do; end do; end do; end do

    call AllSum(normsq_psipp,normsq_psi)


  end function fermion_normsq_4x

  subroutine normalise_4x(phi,norm_phi)
    ! begin args: phi, norm_phi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    real(dp) :: norm_phi

    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns

    ! begin execution
    ns = size(phi,5)

    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl/norm_phi
    end do; end do; end do; end do; end do


  end subroutine normalise_4x

  function inner_product_eo(psi,phi) result (psidotphi)
    ! begin args: psi, phi

    type(dirac_fermion), dimension(:) :: psi,phi

    ! begin local_vars
    complex(dc) :: psidotphi, psidotphipp
    integer :: i_xeo

    ! begin execution

    psidotphipp = 0.0d0
    do i_xeo=1,n_xeo
       psidotphipp = psidotphipp + sum(conjg(psi(i_xeo)%cs)*phi(i_xeo)%cs)
    end do

    call AllSum(psidotphipp,psidotphi)


  end function inner_product_eo

  function real_inner_product_eo(psi,phi) result (psidotphi)
    ! begin args: psi, phi

    type(dirac_fermion), dimension(:) :: psi,phi
    ! begin local_vars
    real(dp) :: psidotphi
    real(dp) :: psidotphipp
    integer :: i_xeo

    ! begin execution

    psidotphipp = 0.0d0
    do i_xeo=1,n_xeo
       psidotphipp = psidotphipp + sum(real(conjg(psi(i_xeo)%cs)*phi(i_xeo)%cs))
    end do

    call AllSum(psidotphipp,psidotphi)


  end function real_inner_product_eo

  function fermion_norm_eo(psi) result (norm_psi)
    ! begin args: psi

    type(dirac_fermion), dimension(:) :: psi
    ! begin local_vars
    real(dp) :: norm_psi
    real(dp) :: norm_psipp
    integer :: i_xeo

    ! begin execution

    norm_psipp = 0.0d0
    do i_xeo=1,n_xeo
       norm_psipp = norm_psipp +  sum(real(conjg(psi(i_xeo)%cs)*psi(i_xeo)%cs))
    end do

    call AllSum(norm_psipp,norm_psi)
    norm_psi = sqrt(norm_psi)


  end function fermion_norm_eo

  function fermion_normsq_eo(psi) result (normsq_psi)
    ! begin args: psi

    type(dirac_fermion), dimension(:) :: psi
    ! begin local_vars
    real(dp) :: normsq_psi
    real(dp) :: normsq_psipp
    integer :: i_xeo

    ! begin execution

    normsq_psipp = 0.0d0
    do i_xeo=1,n_xeo
       normsq_psipp = normsq_psipp +  sum(real(conjg(psi(i_xeo)%cs)*psi(i_xeo)%cs))
    end do

    call AllSum(normsq_psipp,normsq_psi)


  end function fermion_normsq_eo

  subroutine normalise_eo(phi,norm_phi)
    ! begin args: phi, norm_phi

    type(dirac_fermion), dimension(:) :: phi
    real(dp) :: norm_phi

    ! begin local_vars
    integer :: i_xeo

    ! begin execution

    do i_xeo=1,n_xeo
       phi(i_xeo)%cs = phi(i_xeo)%cs/norm_phi
    end do


  end subroutine normalise_eo

  subroutine ProjectVector_4x(v_lambda,phi)
    ! begin args: v_lambda, phi

    type(colour_vector), dimension(:,:,:,:,:) :: v_lambda,phi
    ! begin local_vars
    complex(dc) :: v_lambdadotphi
    integer :: ix,iy,iz,it,is,ns
    real(dp) :: normsq_v

    ! begin execution

    ns = size(phi,5)

    normsq_v = fermion_normsq_4x(v_lambda)
    v_lambdadotphi = inner_product_4x(v_lambda,phi)/normsq_v

    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl - v_lambda(ix,iy,iz,it,is)%Cl*v_lambdadotphi
    end do; end do; end do; end do; end do


  end subroutine ProjectVector_4x

  subroutine orthogonalise_4x(phi,psi,psidotphi_norm)
    ! begin args: phi, psi, psidotphi_norm

    type(colour_vector), dimension(:,:,:,:,:) :: phi,psi
    complex(dc) :: psidotphi_norm
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns

    ! begin execution

    ns = size(phi,5)

    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl - psi(ix,iy,iz,it,is)%Cl*psidotphi_norm
    end do; end do; end do; end do; end do


  end subroutine orthogonalise_4x

  subroutine ProjectVectorSpace_4x(n_v,v_i,phi)
    ! begin args: n_v, v_i, phi

    integer, intent(in) :: n_v
    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_i

    ! begin local_vars
    complex(dc) :: v_idotphi
    real(dp) :: normsq_v
    integer :: ix,iy,iz,it,is,ns,i_v

    ! begin execution

    ns = size(phi,5)

    do i_v = 1,n_v
       normsq_v = fermion_normsq_4x(v_i(:,:,:,:,:,i_v))
       v_idotphi = inner_product_4x(v_i(:,:,:,:,:,i_v),phi)/normsq_v
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl - v_i(ix,iy,iz,it,is,i_v)%Cl*v_idotphi/normsq_v
       end do; end do; end do; end do; end do
    end do


  end subroutine ProjectVectorSpace_4x

  subroutine ProjectVector_eo(v_lambda,phi)
    ! begin args: v_lambda, phi

    type(dirac_fermion), dimension(:) :: v_lambda,phi
    ! begin local_vars
    complex(dc) :: v_lambdadotphi
    integer :: i_xeo,ns
    real(dp) :: normsq_v

    ! begin execution

    normsq_v = fermion_normsq_eo(v_lambda)
    v_lambdadotphi = inner_product_eo(v_lambda,phi)/normsq_v

    do i_xeo =1,n_xeo
       phi(i_xeo)%cs = phi(i_xeo)%cs - v_lambda(i_xeo)%cs*v_lambdadotphi
    end do


  end subroutine ProjectVector_eo

  subroutine orthogonalise_eo(phi,psi,psidotphi_norm)
    ! begin args: phi, psi, psidotphi_norm

    type(dirac_fermion), dimension(:) :: phi,psi
    complex(dc) :: psidotphi_norm
    ! begin local_vars
    integer :: i_xeo,ns

    ! begin execution

    do i_xeo =1,n_xeo
       phi(i_xeo)%cs = phi(i_xeo)%cs - psi(i_xeo)%cs*psidotphi_norm
    end do


  end subroutine orthogonalise_eo

  subroutine ProjectVectorSpace_eo(n_v,v_i,phi)
    ! begin args: n_v, v_i, phi

    integer, intent(in) :: n_v
    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:,:) :: v_i

    ! begin local_vars
    complex(dc) :: v_idotphi
    real(dp) :: normsq_v
    integer :: i_xeo,ns,i_v

    ! begin execution

    do i_v = 1,n_v
       normsq_v = fermion_normsq_eo(v_i(:,i_v))
       v_idotphi = inner_product_eo(v_i(:,i_v),phi)/normsq_v
       do i_xeo =1,n_xeo
          phi(i_xeo)%cs = phi(i_xeo)%cs - v_i(i_xeo,i_v)%cs*v_idotphi/normsq_v
       end do
    end do


  end subroutine ProjectVectorSpace_eo

  subroutine OrthogonaliseEigenspace_eo(n_v,v_i,phi,Dv_i,Dphi)
    ! begin args: n_v, v_i, phi, Dv_i, Dphi

    integer, intent(in) :: n_v
    type(dirac_fermion), dimension(:) :: phi, Dphi
    type(dirac_fermion), dimension(:,:) :: v_i,Dv_i

    ! begin local_vars
    complex(dc) :: v_idotphi
    integer :: i_xeo,i_v

    ! begin execution


    do i_v = 1,n_v
       v_idotphi = inner_product(v_i(:,i_v),phi)
       do i_xeo =1,n_xeo
          phi(i_xeo)%cs = phi(i_xeo)%cs - v_i(i_xeo,i_v)%cs*v_idotphi
          Dphi(i_xeo)%cs = Dphi(i_xeo)%cs - Dv_i(i_xeo,i_v)%cs*v_idotphi
       end do
    end do


  end subroutine OrthogonaliseEigenspace_eo

  subroutine ScaleVector_eo(Dphi, lambda_scale)
    ! begin args: Dphi, lambda_scale

    type(dirac_fermion), dimension(:) :: Dphi
    real(dp) :: lambda_scale
    ! begin local_vars
    integer :: i_xeo

    ! begin execution


    if ( (lambda_scale /= 1.0d0) .and. (lambda_scale /= 0.0d0) ) then
       do i_xeo =1,n_xeo
          Dphi(i_xeo)%cs = lambda_scale * Dphi(i_xeo)%cs
       end do
    end if


  end subroutine ScaleVector_eo

  pure subroutine GammaPlusProject(Gplus_muphi,phi,mu)
    ! begin args: Gplus_muphi, phi, mu

    type(colour_vector), dimension(ns), intent(out) :: Gplus_muphi
    type(colour_vector), dimension(ns), intent(in) :: phi
    integer, intent(in) :: mu

    !Apply the spinor projector Gplus = (1 + gamma_mu) to phi and optionally shift the result in the mu direction.
    !Naturally we are in a chiral basis.

    ! begin execution

    select case(mu)
    case(1)
       Gplus_muphi(1)%Cl = phi(1)%Cl - cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       Gplus_muphi(2)%Cl = phi(2)%Cl - cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       Gplus_muphi(3)%Cl = cmplx(-aimag(Gplus_muphi(2)%Cl),real(Gplus_muphi(2)%Cl),dc)
       Gplus_muphi(4)%Cl = cmplx(-aimag(Gplus_muphi(1)%Cl),real(Gplus_muphi(1)%Cl),dc)
    case(2)
       Gplus_muphi(1)%Cl = phi(1)%Cl - phi(4)%Cl
       Gplus_muphi(2)%Cl = phi(2)%Cl + phi(3)%Cl
       Gplus_muphi(3)%Cl =  Gplus_muphi(2)%Cl
       Gplus_muphi(4)%Cl = -Gplus_muphi(1)%Cl
    case(3)
       Gplus_muphi(1)%Cl = phi(1)%Cl - cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       Gplus_muphi(2)%Cl = phi(2)%Cl + cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       Gplus_muphi(3)%Cl = cmplx(-aimag(Gplus_muphi(1)%Cl),real(Gplus_muphi(1)%Cl),dc)
       Gplus_muphi(4)%Cl = cmplx(aimag(Gplus_muphi(2)%Cl),-real(Gplus_muphi(2)%Cl),dc)
    case(4)
#ifdef _chiral_basis_
       Gplus_muphi(1)%Cl = phi(1)%Cl + phi(3)%Cl
       Gplus_muphi(2)%Cl = phi(2)%Cl + phi(4)%Cl
       Gplus_muphi(3)%Cl = Gplus_muphi(1)%Cl
       Gplus_muphi(4)%Cl = Gplus_muphi(2)%Cl
#endif
#ifdef _sakurai_basis_
       Gplus_muphi(1)%Cl = 2.0_dp*phi(1)%Cl
       Gplus_muphi(2)%Cl = 2.0_dp*phi(2)%Cl
       Gplus_muphi(3)%Cl = 0.0_dp
       Gplus_muphi(4)%Cl = 0.0_dp
#endif
    end select


  end subroutine GammaPlusProject

  pure subroutine GammaMinusProject(Gminus_muphi,phi,mu)
    ! begin args: Gminus_muphi, phi, mu

    type(colour_vector), dimension(ns), intent(out) :: Gminus_muphi
    type(colour_vector), dimension(ns), intent(in) :: phi
    integer, intent(in) :: mu

    !Apply the spinor projector (1 - gamma_mu) to phi and optionally shift the result in the mu direction.
    !Naturally we are in a chiral basis.

    ! begin execution

    select case(mu)
    case(1)
       Gminus_muphi(1)%Cl = phi(1)%Cl + cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       Gminus_muphi(2)%Cl = phi(2)%Cl + cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       Gminus_muphi(3)%Cl = cmplx(aimag(Gminus_muphi(2)%Cl),-real(Gminus_muphi(2)%Cl),dc)
       Gminus_muphi(4)%Cl = cmplx(aimag(Gminus_muphi(1)%Cl),-real(Gminus_muphi(1)%Cl),dc)
    case(2)
       Gminus_muphi(1)%Cl = phi(1)%Cl + phi(4)%Cl
       Gminus_muphi(2)%Cl = phi(2)%Cl - phi(3)%Cl
       Gminus_muphi(3)%Cl = -Gminus_muphi(2)%Cl
       Gminus_muphi(4)%Cl =  Gminus_muphi(1)%Cl
    case(3)
       Gminus_muphi(1)%Cl = phi(1)%Cl + cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       Gminus_muphi(2)%Cl = phi(2)%Cl - cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       Gminus_muphi(3)%Cl = cmplx(aimag(Gminus_muphi(1)%Cl),-real(Gminus_muphi(1)%Cl),dc)
       Gminus_muphi(4)%Cl = cmplx(-aimag(Gminus_muphi(2)%Cl),real(Gminus_muphi(2)%Cl),dc)
    case(4)
#ifdef _chiral_basis_
       Gminus_muphi(1)%Cl = phi(1)%Cl - phi(3)%Cl
       Gminus_muphi(2)%Cl = phi(2)%Cl - phi(4)%Cl
       Gminus_muphi(3)%Cl = -Gminus_muphi(1)%Cl
       Gminus_muphi(4)%Cl = -Gminus_muphi(2)%Cl
#endif
#ifdef _sakurai_basis_
       Gminus_muphi(1)%Cl = 0.0_dp
       Gminus_muphi(2)%Cl = 0.0_dp
       Gminus_muphi(3)%Cl = 2.0_dp*phi(3)%Cl
       Gminus_muphi(4)%Cl = 2.0_dp*phi(4)%Cl
#endif
    end select


  end subroutine GammaMinusProject

  subroutine GammaPhi_Chiral(gamma_muphi,phi,mu)
    ! begin args: gamma_muphi, phi, mu

    type(colour_vector), dimension(ns) :: gamma_muphi
    type(colour_vector), dimension(ns) :: phi
    integer :: mu

    !Naturally we are in a chiral basis.

    ! begin execution

    select case(mu)
    case(1)
       gamma_muphi(1)%Cl = -cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       gamma_muphi(2)%Cl = -cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       gamma_muphi(3)%Cl =  cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       gamma_muphi(4)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
    case(2)
       gamma_muphi(1)%Cl = -phi(4)%Cl
       gamma_muphi(2)%Cl =  phi(3)%Cl
       gamma_muphi(3)%Cl =  phi(2)%Cl
       gamma_muphi(4)%Cl = -phi(1)%Cl
    case(3)
       gamma_muphi(1)%Cl = -cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       gamma_muphi(2)%Cl =  cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       gamma_muphi(3)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       gamma_muphi(4)%Cl = -cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
    case(4)
       gamma_muphi(1)%Cl =  phi(3)%Cl
       gamma_muphi(2)%Cl =  phi(4)%Cl
       gamma_muphi(3)%Cl =  phi(1)%Cl
       gamma_muphi(4)%Cl =  phi(2)%Cl
    case(5)
       gamma_muphi(1)%Cl =  phi(1)%Cl
       gamma_muphi(2)%Cl =  phi(2)%Cl
       gamma_muphi(3)%Cl = -phi(3)%Cl
       gamma_muphi(4)%Cl = -phi(4)%Cl
    end select


  end subroutine GammaPhi_Chiral

  subroutine SigmaPhi_Chiral(sigma_munuphi,phi,mu,nu)
    ! begin args: sigma_munuphi, phi, mu, nu

    type(colour_vector), dimension(ns) :: sigma_munuphi
    type(colour_vector), dimension(ns) :: phi
    ! begin local_vars
    integer :: mu, nu, iplaq, is

    ! begin execution

    if ( mu < nu ) then
       iplaq = mu+nu-1-1/mu
    else
       iplaq = nu+mu-1-1/nu
    end if

    select case(iplaq)
    case(1) ! mu = 1, nu = 2
       sigma_munuphi(1)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       sigma_munuphi(2)%Cl = -cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       sigma_munuphi(3)%Cl =  cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       sigma_munuphi(4)%Cl = -cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
    case(2) ! mu = 1, nu = 3
       sigma_munuphi(1)%Cl = -phi(2)%Cl
       sigma_munuphi(2)%Cl =  phi(1)%Cl
       sigma_munuphi(3)%Cl = -phi(4)%Cl
       sigma_munuphi(4)%Cl =  phi(3)%Cl
    case(3) ! mu = 1, nu = 4
       sigma_munuphi(1)%Cl = -cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       sigma_munuphi(2)%Cl = -cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       sigma_munuphi(3)%Cl =  cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       sigma_munuphi(4)%Cl =  cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
    case(4) ! mu = 2, nu = 3
       sigma_munuphi(1)%Cl =  cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       sigma_munuphi(2)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       sigma_munuphi(3)%Cl =  cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       sigma_munuphi(4)%Cl =  cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
    case(5) ! mu = 2, nu = 4
       sigma_munuphi(1)%Cl = -phi(2)%Cl
       sigma_munuphi(2)%Cl =  phi(1)%Cl
       sigma_munuphi(3)%Cl =  phi(4)%Cl
       sigma_munuphi(4)%Cl = -phi(3)%Cl
    case(6) ! mu = 3, nu = 4
       sigma_munuphi(1)%Cl = -cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       sigma_munuphi(2)%Cl =  cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       sigma_munuphi(3)%Cl =  cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       sigma_munuphi(4)%Cl = -cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
    end select

    if ( mu > nu ) then
       do is=1,ns
          sigma_munuphi(is)%Cl = -sigma_munuphi(is)%Cl
       end do
    end if


  end subroutine SigmaPhi_Chiral

  subroutine GammaPhi_Sakurai(gamma_muphi,phi,mu)

    type(colour_vector), dimension(ns) :: gamma_muphi
    type(colour_vector), dimension(ns) :: phi
    integer :: mu

    select case(mu)
    case(1)
       gamma_muphi(1)%Cl = -cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       gamma_muphi(2)%Cl = -cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       gamma_muphi(3)%Cl =  cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       gamma_muphi(4)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
    case(2)
       gamma_muphi(1)%Cl = -phi(4)%Cl
       gamma_muphi(2)%Cl =  phi(3)%Cl
       gamma_muphi(3)%Cl =  phi(2)%Cl
       gamma_muphi(4)%Cl = -phi(1)%Cl
    case(3)
       gamma_muphi(1)%Cl = -cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       gamma_muphi(2)%Cl =  cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       gamma_muphi(3)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       gamma_muphi(4)%Cl = -cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
    case(4)
       gamma_muphi(1)%Cl =  phi(1)%Cl
       gamma_muphi(2)%Cl =  phi(2)%Cl
       gamma_muphi(3)%Cl = -phi(3)%Cl
       gamma_muphi(4)%Cl = -phi(4)%Cl
    case(5)
       gamma_muphi(1)%Cl =  -phi(3)%Cl
       gamma_muphi(2)%Cl =  -phi(4)%Cl
       gamma_muphi(3)%Cl =  -phi(1)%Cl
       gamma_muphi(4)%Cl =  -phi(2)%Cl
    end select

  end subroutine GammaPhi_Sakurai

  subroutine SigmaPhi_Sakurai(sigma_munuphi,phi,mu,nu)

    type(colour_vector), dimension(ns) :: sigma_munuphi
    type(colour_vector), dimension(ns) :: phi
    integer :: mu, nu, iplaq, is

    if ( mu < nu ) then
       iplaq = mu+nu-1-1/mu
    else
       iplaq = nu+mu-1-1/nu
    end if

    select case(iplaq)
    case(1) ! mu = 1, nu = 2
       sigma_munuphi(1)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       sigma_munuphi(2)%Cl = -cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       sigma_munuphi(3)%Cl =  cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       sigma_munuphi(4)%Cl = -cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
    case(2) ! mu = 1, nu = 3
       sigma_munuphi(1)%Cl = -phi(2)%Cl
       sigma_munuphi(2)%Cl =  phi(1)%Cl
       sigma_munuphi(3)%Cl = -phi(4)%Cl
       sigma_munuphi(4)%Cl =  phi(3)%Cl
    case(3) ! mu = 1, nu = 4
       sigma_munuphi(1)%Cl =  cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       sigma_munuphi(2)%Cl =  cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       sigma_munuphi(3)%Cl =  cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       sigma_munuphi(4)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
    case(4) ! mu = 2, nu = 3
       sigma_munuphi(1)%Cl =  cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
       sigma_munuphi(2)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       sigma_munuphi(3)%Cl =  cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       sigma_munuphi(4)%Cl =  cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
    case(5) ! mu = 2, nu = 4
       sigma_munuphi(1)%Cl =  phi(4)%Cl
       sigma_munuphi(2)%Cl = -phi(3)%Cl
       sigma_munuphi(3)%Cl =  phi(2)%Cl
       sigma_munuphi(4)%Cl = -phi(1)%Cl
    case(6) ! mu = 3, nu = 4
       sigma_munuphi(1)%Cl =  cmplx(-aimag(phi(3)%Cl),real(phi(3)%Cl),dc)
       sigma_munuphi(2)%Cl = -cmplx(-aimag(phi(4)%Cl),real(phi(4)%Cl),dc)
       sigma_munuphi(3)%Cl =  cmplx(-aimag(phi(1)%Cl),real(phi(1)%Cl),dc)
       sigma_munuphi(4)%Cl = -cmplx(-aimag(phi(2)%Cl),real(phi(2)%Cl),dc)
    end select

    if ( mu > nu ) then
       do is=1,ns
          sigma_munuphi(is)%Cl = -sigma_munuphi(is)%Cl
       end do
    end if

  end subroutine SigmaPhi_Sakurai

end module FermionField

