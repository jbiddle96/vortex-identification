F90C = mpif90
OPTS = -O2 -ffree-line-length-huge
F90FLAGS = -cpp $(OPTS)
LINK = -lcola

CPP = cpp
CPPFLAGS = -P

.SUFFIXES:
.SUFFIXES: .f90 .o .x .f
.f90.o:
	$(F90C) $(F90FLAGS) -o $@ -c $<
.f90.x:
	$(F90C) $(F90FLAGS) -o $@ -I $< $(LINK)

