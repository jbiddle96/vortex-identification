#include "defines.h"

module Trinlat

  use GaugeField
  use ReduceOps
  use MatrixAlgebra
  use GaugeFieldMPIComms
  use CollectiveOps
  use ColourFieldOps
  use LatticeSize
  use MPIInterface
  use Kinds
  use ColourTypes
  implicit none
  private

  public :: ReadGaugeField_trinlat
  public :: GetU0_s

contains

  subroutine ReadGaugeField_trinlat(filename, U_xd)
    ! begin args: filename, U_xd

    character(len=*) :: filename
    type(colour_matrix), dimension(:,:,:,:,:), intent(out) :: U_xd

    ! begin local_vars
    real(DP), dimension(:,:,:,:,:,:,:), allocatable ::  ReU, ImU
    integer :: ic, jc, irank, mu, irec
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    character(len=8), dimension(5) :: header

    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: V_xd
    type(colour_matrix) :: UdagU, I_xd
    integer  :: nconfig,nxdim,nydim,nzdim,ntdim
    real(dp) :: nonU,nonU_pp

    ! begin execution
    allocate(V_xd(nx,ny,nz,nt,nd))

    if ( i_am_root ) then

       allocate(ReU(nc,nc,nlx,nly,nlz,nlt,nd))
       allocate(ImU(nc,nc,nlx,nly,nlz,nlt,nd))

       ReU = 0.0d0
       ImU = 0.0d0

       open(101,file=filename,form='unformatted',access='direct',status='old',action='read',recl=8)

       irec = 1
       do ic=1,5
          read (101,rec=irec) header(ic)
          irec = irec + 1
       end do

       do it=1,nlt
          do mu=1,nd
             do iz=1,nlz; do iy=1,nly; do ix=1,nlx
                do ic=1,nc; do jc=1,nc
                   read(101,rec=irec) ReU(ic,jc,ix,iy,iz,it,mu)
                   irec = irec + 1
                   read(101,rec=irec) ImU(ic,jc,ix,iy,iz,it,mu)
                   irec = irec + 1
                end do; end do
             end do; end do; end do
          end do
       end do
       close(101)

       do irank=0,nproc-1
          call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
          do jc=1,nc; do ic=1,nc
             V_xd(1:nx,1:ny,1:nz,1:nt,:)%Cl(ic,jc) = cmplx(ReU(ic,jc,ix:jx,iy:jy,iz:jz,it:jt,:),&
                  & ImU(ic,jc,ix:jx,iy:jy,iz:jz,it:jt,:),dc)
          end do; end do
          if ( irank /= mpi_root_rank ) then
             call SendMatrixField(V_xd, irank)
          else
             U_xd(1:nx,1:ny,1:nz,1:nt,:)  = V_xd
          end if
       end do

       deallocate(ReU)
       deallocate(ImU)

    else
       call RecvMatrixField(V_xd, mpi_root_rank)
       U_xd(1:nx,1:ny,1:nz,1:nt,:)  = V_xd
    end if

    call Broadcast(uzero,mpi_root_rank)
    !mpiprint *, lastPlaq, plaqbarAvg, uzero

    call ShadowGaugeField(U_xd,1)
    !mpiprint *, lastPlaq, plaqbarAvg, uzero

    I_xd = unit_matrix

    nonU_pp = 0.0d0
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          call MultiplyMatDagMat(UdagU,U_xd(ix,iy,iz,it,mu),U_xd(ix,iy,iz,it,mu))
          nonU_pp = nonU_pp + sqrt(sum(abs(I_xd%Cl - UdagU%Cl)**2))
       end do; end do; end do; end do
    end do

    call AllSum(nonU_pp,nonU)

    mpiprint *, "Non-Unitarity ", nonU

    deallocate(V_xd)

  end subroutine ReadGaugeField_trinlat

  subroutine GetU0_s(U_xd, uzero)
    ! begin args: U_xd, uzero

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: uzero

    ! begin local_vars
    integer :: mu, nu, nplaq
    real(dp) :: plaqbar, plaqbarpp

    ! begin execution

    plaqbarpp = 0.0d0

    do mu=1,nd-1
       do nu=mu+1,nd-1

          call AddPlaqBar_munu(plaqbarpp,U_xd,mu,nu)

       end do
    end do

    call AllSum(plaqbarpp, plaqbar)
    !u_0 = (Mean (1/3)ReTr U_munu)^(1/4)
    nplaq = (nd-1)*(nd-2)/2
    uzero = (plaqbar/(nlattice*nplaq*nc))**0.25d0


  end subroutine GetU0_s

end module Trinlat

