module myTimer

  use kinds

  implicit none

  integer, public :: iTime, fTime, rate

  contains

    subroutine InitialiseTimer

      call system_clock(count_rate=rate)

    end subroutine InitialiseTimer

    subroutine StartTimer

      call System_clock(iTime)

    end subroutine StartTimer

    subroutine EndTimer(finalTime)

      real(dp), intent(out), optional :: finalTime

      call system_clock(fTime)

      if(present(finalTime)) finalTime = real(fTime-iTime,dp)/real(rate,dp)

    end subroutine EndTimer

    subroutine timerOut( str )

      character(len=*), intent(in), optional :: str

      if(present(str)) then
         write(*,*) trim(str),real(fTime-iTime,dp)/real(rate,dp)
      else
         write(*,*) real(fTime-iTime,dp)/real(rate,dp)
      end if

    end subroutine timerOut

  end module myTimer
