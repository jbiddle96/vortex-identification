
module EOWilsonMatrix_FV

  use Timer
  use MPIInterface
  use FermionField
  use FermionAlgebra


  
  use SpinorTypes
  use FermionTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  use ColourFieldOps
  implicit none
  private

  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:), private, allocatable :: Up_xd
  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), public :: kappa_FVWilson



  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean

  real(dp), public :: bcx_FVWilson = 1.0d0, bcy_FVWilson = 1.0d0, bcz_FVWilson = 1.0d0, bct_FVWilson = 1.0d0
  public :: DEOWilson_fw
  public :: EOWilson_fw
  public :: DEOWilson_fx
  public :: EOWilson_fx

  public :: DEOWilson_fv
  public :: DEOWilson_dag_fv
  public :: HsqEOWilson_fv
  public :: HEOWilson_fv
  public :: InitialiseEOWilsonOperator_fv
  public :: FinaliseEOWilsonOperator_fv

  public :: EOWilsonDag_fv
  public :: EOWilson_fv
  public :: x2eo
  public :: eo2x
  public :: phi_x2phi_eo
  public :: phi_eo2phi_x
  
  integer, dimension(:,:,:), allocatable :: i_xpmu, i_xmmu

contains

  subroutine InitialiseEOWilsonOperator_fv(U_xd , kappa, u0)
    ! begin args: U_xd , kappa, u0

    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: kappa
    real(dp), optional :: u0

    ! begin local_vars
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfic_sw, mfir, mfir_sm !coefficents to be absorbed.
    integer :: i_eo, i_xeo, j_xeo
    integer, dimension(nd) :: i_x, j_x, n_x

    !EO preconditioning is in the hopping parameter formalism.
    ! begin execution
    n_xeo = nx*ny*nz*nt/2
    if ( .not. allocated(Up_xd) ) allocate(Up_xd(n_xpeo,nd,0:1))
    if ( .not. allocated(i_xpmu) ) allocate(i_xpmu(n_xpeo,nd,0:1))
    if ( .not. allocated(i_xmmu) ) allocate(i_xmmu(n_xpeo,nd,0:1))

    mfir = kappa
    if ( present(u0) ) mfir = mfir/u0

    kappa_FVWilson = kappa

    !Construct the forward and backward hop indices.
    n_x = (/ nx, ny, nz, nt /)
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       i_x = (/ ix,iy,iz,it /)
       i_eo = modulo(sum(i_x),2)
       i_xeo = x2eo(i_x)
       do mu=1,nd
          j_x = i_x

          j_x(mu) = modc(j_x(mu)+1,n_x(mu))
          j_xeo = x2eo(j_x)
          i_xpmu(i_xeo,mu,i_eo) = j_xeo

          j_x = i_x

          j_x(mu) = modc(j_x(mu)-1,n_x(mu))
          j_xeo = x2eo(j_x)
          i_xmmu(i_xeo,mu,i_eo) = j_xeo

       end do
    end do; end do; end do; end do

    !Absorb the mean field improvement into the gauge fields.
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          Up_xd(i_xeo,mu,i_eo)%Cl = mfir*U_xd(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do

    !call SetBoundaryConditions(Up_xd,bcx_FVWilson, bcy_FVWilson, bcz_FVWilson, bct_FVWilson)
    !call ShadowGaugeField(Up_xd,0)
  end subroutine InitialiseEOWilsonOperator_fv

  subroutine FinaliseEOWilsonOperator_fv

    if ( allocated(Up_xd) ) deallocate(Up_xd)
    if ( allocated(i_xpmu) ) deallocate(i_xpmu)
    if ( allocated(i_xmmu) ) deallocate(i_xmmu)
  end subroutine FinaliseEOWilsonOperator_fv

  subroutine phi_x2phi_eo(psi,psi_e,psi_o)
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(dirac_fermion), dimension(:) :: psi_e, psi_o

    integer :: ix,iy,iz,it,is,i_eo,i_xeo,i_x(nd)

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          if ( i_eo == 0 ) then
             psi_e(i_xeo)%cs(:,is) = psi(ix,iy,iz,it,is)%cl(:) 
          else
             psi_o(i_xeo)%cs(:,is) = psi(ix,iy,iz,it,is)%cl(:) 
          end if
       end do; end do; end do; end do
    end do
  end subroutine phi_x2phi_eo

  subroutine phi_eo2phi_x(psi_e,psi_o,psi)
    type(dirac_fermion), dimension(:) :: psi_e, psi_o
    type(colour_vector), dimension(:,:,:,:,:) :: psi

    integer :: ix,iy,iz,it,is,i_eo,i_xeo,i_x(nd)

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_eo = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          if ( i_eo == 0 ) then
             psi(ix,iy,iz,it,is)%cl(:) = psi_e(i_xeo)%cs(:,is) 
          else
             psi(ix,iy,iz,it,is)%cl(:) = psi_o(i_xeo)%cs(:,is)
          end if
       end do; end do; end do; end do
    end do
  end subroutine phi_eo2phi_x

  subroutine DEOWilson_fv(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: phi_e,phi_o
    integer :: i_xeo,is

    ! begin execution
    allocate(phi_e(n_xpeo))
    allocate(phi_o(n_xpeo))

    call EOWilson_fv(phi,phi_e,0)




    call EOWilson_fv(phi_e,phi_o,1)

    Dphi = phi

    do i_xeo=1,n_xeo
       Dphi(i_xeo)%cs = Dphi(i_xeo)%cs - phi_o(i_xeo)%cs
    end do

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine DEOWilson_fv

  subroutine DEOWilson_dag_fv(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: phi_e, phi_o
    integer :: i_xeo,is

    ! begin execution
    allocate(phi_e(n_xpeo))
    allocate(phi_o(n_xpeo))

    call EOWilsonDag_fv(phi,phi_e,1)

    call EOWilsonDag_fv(phi_e,phi_o,0)

    Dphi = phi

    do i_xeo=1,n_xeo
       Dphi(i_xeo)%cs = Dphi(i_xeo)%cs - phi_o(i_xeo)%cs
    end do

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine DEOWilson_dag_fv

  subroutine HsqEOWilson_fv(phi,Hsqphi)
    ! begin args: phi, Hsqphi

    type(dirac_fermion), dimension(:) :: phi, Hsqphi

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dphi

    ! begin execution
    allocate(Dphi(n_xpeo))

    call HEOWilson_fv(phi,Dphi)
    call HEOWilson_fv(Dphi,Hsqphi)

    deallocate(Dphi)

  end subroutine HsqEOWilson_fv

  subroutine HEOWilson_fv(phi,Hphi)
    ! begin args: phi, Hphi

    type(dirac_fermion), dimension(:) :: phi, Hphi
    ! begin local_vars
    integer :: i_xeo

    ! begin execution

    call DEOWilson_fv(phi,Hphi)

    do i_xeo=1,n_xeo
       Hphi(i_xeo)%cs(:,3) = -Hphi(i_xeo)%cs(:,3)
       Hphi(i_xeo)%cs(:,4) = -Hphi(i_xeo)%cs(:,4)
    end do

  end subroutine HEOWilson_fv

  subroutine EOWilsonDag_fv(phi, Dphi, op_parity)
    ! begin args: phi, Dphi, op_parity

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    integer :: op_parity

    ! begin local_vars
    integer :: i_xeo


    ! begin execution

    do i_xeo=1,n_xeo
       phi(i_xeo)%cs(:,3) = -phi(i_xeo)%cs(:,3)
       phi(i_xeo)%cs(:,4) = -phi(i_xeo)%cs(:,4)
    end do
    call EOWilson_fv(phi,Dphi,1-op_parity)

    do i_xeo=1,n_xeo
       phi(i_xeo)%cs(:,3) = -phi(i_xeo)%cs(:,3)
       phi(i_xeo)%cs(:,4) = -phi(i_xeo)%cs(:,4)

       Dphi(i_xeo)%cs(:,3) = -Dphi(i_xeo)%cs(:,3)
       Dphi(i_xeo)%cs(:,4) = -Dphi(i_xeo)%cs(:,4)
    end do
  end subroutine EOWilsonDag_fv

  subroutine EOWilson_fv(phi, Dphi,i_eo)
    ! begin args: phi, Dphi, op_parity

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    integer :: i_eo

    ! op_parity = 0 => takes odd sites to even sites.
    ! op_parity = 1 => takes even sites to odd sites.

    ! begin local_vars
    integer :: i_xeo, j_xeo, is, js, j_eo
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

!!    if (timing) intime = mpi_wtime()
    commtime = 0.0d0
    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0


    j_eo = 1 - i_eo
    p_xyzt = modulo(i_nx+i_ny+i_nz+i_nt,2) ! Offset due to processor sub-lattice

    Dphi = zero_dirac_fermion
    !mu = 1

    !G_1^- Up phi_xpmu

    do i_xeo=1,n_xeo
       j_xeo = i_xpmu(i_xeo,1,i_eo)

       do is=1,nsp
          js = 5-is

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) + cmplx(-aimag(phi(j_xeo)%cs(1,js)),real(phi(j_xeo)%cs(1,js)),dc)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) + cmplx(-aimag(phi(j_xeo)%cs(2,js)),real(phi(j_xeo)%cs(2,js)),dc)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) + cmplx(-aimag(phi(j_xeo)%cs(3,js)),real(phi(j_xeo)%cs(3,js)),dc)

          psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)


       end do
    end do
    !mu = 2




    !G_2^- Up phi_xpmu

    do i_xeo=1,n_xeo
       j_xeo = i_xpmu(i_xeo,2,i_eo)

       do is=1,nsp
          js = 5-is

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) + pm(is)*phi(j_xeo)%cs(1,js)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) + pm(is)*phi(j_xeo)%cs(2,js)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) + pm(is)*phi(j_xeo)%cs(3,js)

          psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) + pm(js)*psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) + pm(js)*psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) + pm(js)*psi_x%Cl(3)

       end do
    end do
    !mu = 3




    !G_3^- Up phi_xpmu

    do i_xeo=1,n_xeo
       j_xeo = i_xpmu(i_xeo,3,i_eo)

       do is=1,nsp
          js = is+2

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) + pm(is)*cmplx(-aimag(phi(j_xeo)%cs(1,js)),real(phi(j_xeo)%cs(1,js)),dc)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) + pm(is)*cmplx(-aimag(phi(j_xeo)%cs(2,js)),real(phi(j_xeo)%cs(2,js)),dc)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) + pm(is)*cmplx(-aimag(phi(j_xeo)%cs(3,js)),real(phi(j_xeo)%cs(3,js)),dc)

          psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) + pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) + pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) + pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
    end do
    !mu = 4




    !G_4^- Up phi_xpmu

    do i_xeo=1,n_xeo
       j_xeo = i_xpmu(i_xeo,4,i_eo)

       do is=1,nsp
          js = is+2

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) - phi(j_xeo)%cs(1,js)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) - phi(j_xeo)%cs(2,js)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) - phi(j_xeo)%cs(3,js)
          psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)


          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) + psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) + psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) + psi_x%Cl(3)

       end do
    end do
    !mu = 1




    !G_1^+ Up phi_xmmu

    do i_xeo=1,n_xeo
       j_xeo = i_xmmu(i_xeo,1,i_eo)

       do is=1,nsp
          js = 5-is

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) - cmplx(-aimag(phi(j_xeo)%cs(1,js)),real(phi(j_xeo)%cs(1,js)),dc)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) - cmplx(-aimag(phi(j_xeo)%cs(2,js)),real(phi(j_xeo)%cs(2,js)),dc)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) - cmplx(-aimag(phi(j_xeo)%cs(3,js)),real(phi(j_xeo)%cs(3,js)),dc)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
    end do
    !mu = 2




    !G_2^+ Up phi_xmmu

    do i_xeo=1,n_xeo
       j_xeo = i_xmmu(i_xeo,2,i_eo)

       do is=1,nsp
          js = 5-is

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) - pm(is)*phi(j_xeo)%cs(1,js)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) - pm(is)*phi(j_xeo)%cs(2,js)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) - pm(is)*phi(j_xeo)%cs(3,js)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) - pm(js)*psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) - pm(js)*psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) - pm(js)*psi_x%Cl(3)


       end do
    end do
    !mu = 3




    !G_3^+ Up phi_xmmu

    do i_xeo=1,n_xeo
       j_xeo = i_xmmu(i_xeo,3,i_eo)

       do is=1,nsp
          js = is+2

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) - pm(is)*cmplx(-aimag(phi(j_xeo)%cs(1,js)),real(phi(j_xeo)%cs(1,js)),dc)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) - pm(is)*cmplx(-aimag(phi(j_xeo)%cs(2,js)),real(phi(j_xeo)%cs(2,js)),dc)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) - pm(is)*cmplx(-aimag(phi(j_xeo)%cs(3,js)),real(phi(j_xeo)%cs(3,js)),dc)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) - pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) - pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) - pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
    end do
    !mu = 4




    !G_4^+ Up phi_xmmu

    do i_xeo=1,n_xeo
       j_xeo = i_xmmu(i_xeo,4,i_eo)

       do is=1,nsp
          js = is+2

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) + phi(j_xeo)%cs(1,js)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) + phi(j_xeo)%cs(2,js)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) + phi(j_xeo)%cs(3,js)
          psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)


          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) - psi_x%Cl(3)

       end do
    end do

!!    if (timing) then
!!       outtime = mpi_wtime()
!!       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
!!    end if
!!
!!    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
!!    waste = commtime/(outtime - intime)
!!    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)

  end subroutine EOWilson_fv

  subroutine DEOWilson_fw(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: phi_e,phi_o
    integer :: i_xeo,is

    ! begin execution
    allocate(phi_e(n_xpeo))
    allocate(phi_o(n_xpeo))

    call EOWilson_fw(phi,phi_e,0)

    call EOWilson_fw(phi_e,phi_o,1)

    Dphi = phi

    do i_xeo=1,n_xeo
       Dphi(i_xeo)%cs = Dphi(i_xeo)%cs - phi_o(i_xeo)%cs
    end do

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine DEOWilson_fw

  subroutine EOWilson_fw(phi, Dphi,i_eo)
    ! begin args: phi, Dphi, op_parity

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    integer :: i_eo

    ! op_parity = 0 => takes odd sites to even sites.
    ! op_parity = 1 => takes even sites to odd sites.

    ! begin local_vars
    integer :: i_xeo, j_xeo, is, js, j_eo
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

!!    if (timing) intime = mpi_wtime()
!!    commtime = 0.0d0
    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0


    p_xyzt = modulo(i_nx+i_ny+i_nz+i_nt,2) ! Offset due to processor sub-lattice

    j_eo = 1 - i_eo
    Dphi = zero_dirac_fermion
    !mu = 1

    !G_1^- Up phi_xpmu

    do i_xeo=1,n_xeo

       j_xeo = i_xpmu(i_xeo,1,i_eo)

       do is=1,nsp
          js = 5-is

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) + cmplx(-aimag(phi(j_xeo)%cs(1,js)),real(phi(j_xeo)%cs(1,js)),dc)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) + cmplx(-aimag(phi(j_xeo)%cs(2,js)),real(phi(j_xeo)%cs(2,js)),dc)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) + cmplx(-aimag(phi(j_xeo)%cs(3,js)),real(phi(j_xeo)%cs(3,js)),dc)

          psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)


       end do
       !mu = 2




       !G_2^- Up phi_xpmu

       j_xeo = i_xpmu(i_xeo,2,i_eo)

       do is=1,nsp
          js = 5-is

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) + pm(is)*phi(j_xeo)%cs(1,js)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) + pm(is)*phi(j_xeo)%cs(2,js)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) + pm(is)*phi(j_xeo)%cs(3,js)

          psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) + pm(js)*psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) + pm(js)*psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) + pm(js)*psi_x%Cl(3)

       end do
       !mu = 3




       !G_3^- Up phi_xpmu
       j_xeo = i_xpmu(i_xeo,3,i_eo)

       do is=1,nsp
          js = is+2

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) + pm(is)*cmplx(-aimag(phi(j_xeo)%cs(1,js)),real(phi(j_xeo)%cs(1,js)),dc)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) + pm(is)*cmplx(-aimag(phi(j_xeo)%cs(2,js)),real(phi(j_xeo)%cs(2,js)),dc)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) + pm(is)*cmplx(-aimag(phi(j_xeo)%cs(3,js)),real(phi(j_xeo)%cs(3,js)),dc)

          psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) + pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) + pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) + pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
       !mu = 4




       !G_4^- Up phi_xpmu
       j_xeo = i_xpmu(i_xeo,4,i_eo)

       do is=1,nsp
          js = is+2

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) - phi(j_xeo)%cs(1,js)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) - phi(j_xeo)%cs(2,js)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) - phi(j_xeo)%cs(3,js)
          psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
          psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
          psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)


          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) + psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) + psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) + psi_x%Cl(3)

       end do
       !mu = 1




       !G_1^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,1,i_eo)

       do is=1,nsp
          js = 5-is

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) - cmplx(-aimag(phi(j_xeo)%cs(1,js)),real(phi(j_xeo)%cs(1,js)),dc)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) - cmplx(-aimag(phi(j_xeo)%cs(2,js)),real(phi(j_xeo)%cs(2,js)),dc)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) - cmplx(-aimag(phi(j_xeo)%cs(3,js)),real(phi(j_xeo)%cs(3,js)),dc)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
       !mu = 2




       !G_2^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,2,i_eo)

       do is=1,nsp
          js = 5-is

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) - pm(is)*phi(j_xeo)%cs(1,js)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) - pm(is)*phi(j_xeo)%cs(2,js)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) - pm(is)*phi(j_xeo)%cs(3,js)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) - pm(js)*psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) - pm(js)*psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) - pm(js)*psi_x%Cl(3)


       end do
       !mu = 3




       !G_3^+ Up phi_xmmu

       j_xeo = i_xmmu(i_xeo,3,i_eo)

       do is=1,nsp
          js = is+2

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) - pm(is)*cmplx(-aimag(phi(j_xeo)%cs(1,js)),real(phi(j_xeo)%cs(1,js)),dc)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) - pm(is)*cmplx(-aimag(phi(j_xeo)%cs(2,js)),real(phi(j_xeo)%cs(2,js)),dc)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) - pm(is)*cmplx(-aimag(phi(j_xeo)%cs(3,js)),real(phi(j_xeo)%cs(3,js)),dc)

          psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)

          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) - pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) - pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) - pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       end do
       !mu = 4




       !G_4^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,4,i_eo)

       do is=1,nsp
          js = is+2

          Gammaphi%Cl(1) = phi(j_xeo)%cs(1,is) + phi(j_xeo)%cs(1,js)
          Gammaphi%Cl(2) = phi(j_xeo)%cs(2,is) + phi(j_xeo)%cs(2,js)
          Gammaphi%Cl(3) = phi(j_xeo)%cs(3,is) + phi(j_xeo)%cs(3,js)
          psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
          psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
          psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(i_xeo)%cs(1,is) = Dphi(i_xeo)%cs(1,is) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,is) = Dphi(i_xeo)%cs(2,is) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,is) = Dphi(i_xeo)%cs(3,is) - psi_x%Cl(3)


          Dphi(i_xeo)%cs(1,js) = Dphi(i_xeo)%cs(1,js) - psi_x%Cl(1)
          Dphi(i_xeo)%cs(2,js) = Dphi(i_xeo)%cs(2,js) - psi_x%Cl(2)
          Dphi(i_xeo)%cs(3,js) = Dphi(i_xeo)%cs(3,js) - psi_x%Cl(3)

       end do
    end do

!!    if (timing) then
!!       outtime = mpi_wtime()
!!       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
!!    end if
!!
!!    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
!!    waste = commtime/(outtime - intime)
!!    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)

  end subroutine EOWilson_fw

  subroutine DEOWilson_fx(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: phi_e,phi_o
    integer :: i_xeo,is

    ! begin execution
    allocate(phi_e(n_xpeo))
    allocate(phi_o(n_xpeo))

    call EOWilson_fx(phi,phi_e,0)

    call EOWilson_fx(phi_e,phi_o,1)

    Dphi = phi

    do i_xeo=1,n_xeo
       Dphi(i_xeo)%cs = Dphi(i_xeo)%cs - phi_o(i_xeo)%cs
    end do

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine DEOWilson_fx

  subroutine EOWilson_fx(phi, Dphi,i_eo)
    ! begin args: phi, Dphi, op_parity

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    integer :: i_eo

    ! op_parity = 0 => takes odd sites to even sites.
    ! op_parity = 1 => takes even sites to odd sites.

    ! begin local_vars
    integer :: i_xeo, j_xeo, j_eo
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt

    real(dp) :: intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

!!    if (timing) intime = mpi_wtime()
!!    commtime = 0.0d0

    j_eo = 1 - i_eo
    p_xyzt = modulo(i_nx+i_ny+i_nz+i_nt,2) ! Offset due to processor sub-lattice

    Dphi = zero_dirac_fermion
    !mu = 1

    !G_1^- Up phi_xpmu

    do i_xeo=1,n_xeo

       j_xeo = i_xpmu(i_xeo,1,i_eo)

       !i_s = 1
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)

       psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !i_s = 2
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)

       psi_x%Cl(1) = Up_xd(i_xeo,1,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,1,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,1,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,1,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)


       !mu = 2

       !G_2^- Up phi_xpmu

       j_xeo = i_xpmu(i_xeo,2,i_eo)

       !i_s = 1
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + phi(j_xeo)%cs(1,4)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + phi(j_xeo)%cs(2,4)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + phi(j_xeo)%cs(3,4)

       psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - psi_x%Cl(3)

       !i_s = 2
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - phi(j_xeo)%cs(1,3)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - phi(j_xeo)%cs(2,3)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - phi(j_xeo)%cs(3,3)

       psi_x%Cl(1) = Up_xd(i_xeo,2,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,2,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,2,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,2,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + psi_x%Cl(3)

       !mu = 3




       !G_3^- Up phi_xpmu
       j_xeo = i_xpmu(i_xeo,3,i_eo)

       !i_s = 1
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)

       psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !i_s = 2
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)

       psi_x%Cl(1) = Up_xd(i_xeo,3,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,3,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,3,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,3,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !mu = 4




       !G_4^- Up phi_xpmu
       j_xeo = i_xpmu(i_xeo,4,i_eo)

       !i_s = 1
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - phi(j_xeo)%cs(1,3)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - phi(j_xeo)%cs(2,3)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - phi(j_xeo)%cs(3,3)
       psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)


       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) + psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) + psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) + psi_x%Cl(3)

       !i_s = 2
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - phi(j_xeo)%cs(1,4)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - phi(j_xeo)%cs(2,4)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - phi(j_xeo)%cs(3,4)
       psi_x%Cl(1) = Up_xd(i_xeo,4,i_eo)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(i_xeo,4,i_eo)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(i_xeo,4,i_eo)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(i_xeo,4,i_eo)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)


       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + psi_x%Cl(3)

       !mu = 1




       !G_1^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,1,i_eo)

       !i_s = 1
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !i_s = 2
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) - cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) - cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) - cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,1,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,1,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !mu = 2




       !G_2^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,2,i_eo)

       !i_s = 1
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - phi(j_xeo)%cs(1,4)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - phi(j_xeo)%cs(2,4)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - phi(j_xeo)%cs(3,4)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + psi_x%Cl(3)

       !i_s = 2
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + phi(j_xeo)%cs(1,3)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + phi(j_xeo)%cs(2,3)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + phi(j_xeo)%cs(3,3)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,2,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,2,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - psi_x%Cl(3)

       !mu = 3




       !G_3^+ Up phi_xmmu

       j_xeo = i_xmmu(i_xeo,3,i_eo)

       !i_s = 1
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) - cmplx(-aimag(phi(j_xeo)%cs(1,3)),real(phi(j_xeo)%cs(1,3)),dc)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) - cmplx(-aimag(phi(j_xeo)%cs(2,3)),real(phi(j_xeo)%cs(2,3)),dc)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) - cmplx(-aimag(phi(j_xeo)%cs(3,3)),real(phi(j_xeo)%cs(3,3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !i_s = 2
       !j_s = 4

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + cmplx(-aimag(phi(j_xeo)%cs(1,4)),real(phi(j_xeo)%cs(1,4)),dc)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + cmplx(-aimag(phi(j_xeo)%cs(2,4)),real(phi(j_xeo)%cs(2,4)),dc)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + cmplx(-aimag(phi(j_xeo)%cs(3,4)),real(phi(j_xeo)%cs(3,4)),dc)

       psi_x%Cl(1) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,3,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,3,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)

       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !mu = 4




       !G_4^+ Up phi_xmmu
       j_xeo = i_xmmu(i_xeo,4,i_eo)

       !i_s = 1
       !j_s = 3

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,1) + phi(j_xeo)%cs(1,3)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,1) + phi(j_xeo)%cs(2,3)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,1) + phi(j_xeo)%cs(3,3)
       psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,1) = Dphi(i_xeo)%cs(1,1) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,1) = Dphi(i_xeo)%cs(2,1) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,1) = Dphi(i_xeo)%cs(3,1) - psi_x%Cl(3)

       !i_s = 2
       !j_s = 4

       Dphi(i_xeo)%cs(1,3) = Dphi(i_xeo)%cs(1,3) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,3) = Dphi(i_xeo)%cs(2,3) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,3) = Dphi(i_xeo)%cs(3,3) - psi_x%Cl(3)

       Gammaphi%Cl(1) = phi(j_xeo)%cs(1,2) + phi(j_xeo)%cs(1,4)
       Gammaphi%Cl(2) = phi(j_xeo)%cs(2,2) + phi(j_xeo)%cs(2,4)
       Gammaphi%Cl(3) = phi(j_xeo)%cs(3,2) + phi(j_xeo)%cs(3,4)
       psi_x%Cl(1) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(j_xeo,4,j_eo)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(j_xeo,4,j_eo)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(i_xeo)%cs(1,2) = Dphi(i_xeo)%cs(1,2) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,2) = Dphi(i_xeo)%cs(2,2) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,2) = Dphi(i_xeo)%cs(3,2) - psi_x%Cl(3)


       Dphi(i_xeo)%cs(1,4) = Dphi(i_xeo)%cs(1,4) - psi_x%Cl(1)
       Dphi(i_xeo)%cs(2,4) = Dphi(i_xeo)%cs(2,4) - psi_x%Cl(2)
       Dphi(i_xeo)%cs(3,4) = Dphi(i_xeo)%cs(3,4) - psi_x%Cl(3)

    end do

!!    if (timing) then
!!       outtime = mpi_wtime()
!!       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
!!    end if
!!
!!    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
!!    waste = commtime/(outtime - intime)
!!    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)

  end subroutine EOWilson_fx

end module EOWilsonMatrix_FV
