!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/signfunction.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: signfunction.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module SignFunction

  use ColourTypes
  use Timer
  use FermionField
  use ZolotarevApprox
  use ConjGradSolvers
  use LatticeSize
  use SpinorTypes
  use Kinds
  use MPIInterface
  implicit none
  private

  !!logical, parameter, public :: debug = .false.

  integer, public :: ncalls1 = 0
  real(dp), public :: maxtime1 = 0, mintime1
  real(dp), public :: meantime1 = 0.0

  logical, public :: project_eigenspace = .true.

  !!real(dp), public :: c_1

  public :: MatrixSignFunctionOperate
  ! ToDo: Revise this module.

contains

#define chi_x(is) chi(ix,iy,iz,it,is)
#define phi_x(is) phi(ix,iy,iz,it,is)
#define psi_x(is) psi(ix,iy,iz,it,is)
#define Mphi_x(is) Mphi(ix,iy,iz,it,is)
#define Dphi_x(is) Dphi(ix,iy,iz,it,is)
#define phi_ix(is) phi_i(ix,iy,iz,it,is,i_v)
#define residue_x(is) residue(ix,iy,iz,it,is)
#define chi_ix(is) chi_i(ix,iy,iz,it,is,i_v)
#define xi_x(is) xi(ix,iy,iz,it,is)
#define eta_x(is) eta(ix,iy,iz,it,is)

  subroutine MatrixSignFunctionOperate(n_v,psi,chi,tolerance,iterations_i,MatrixOperate,SqMatrixOperate,n_lambda, lambda_i, v_lambda, delta_lambda)
    ! begin args: n_v, psi, chi, tolerance, iterations_i, MatrixOperate, SqMatrixOperate, n_lambda, lambda_i, v_lambda, delta_lambda

    !A routine to solve the matrix equation (M+sigma)v = psi for a system of shifts sigma
    !sigma must be ordered such that sigma(1) contains the least convergent system
    ! (i.e. the system that converges slowest).

    !M _must_ be hermitian and positive definite.
    !Implements the sign function through the Zolotarev rational approximation

    !Assumed: tolerance >= epsilon(1.0d0)

    integer, intent(in) :: n_v !number of shifts
    type(colour_vector), dimension(:,:,:,:,:) :: psi !source vector
    type(colour_vector), dimension(:,:,:,:,:) :: chi !solution vector
    real(DP) :: tolerance !the precision desired for the solution
    integer, dimension(n_v) :: iterations_i !convergence information
    interface
       subroutine MatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine MatrixOperate
    end interface
    interface
       subroutine SqMatrixOperate(phi, Dphi)
         use ColourTypes
         type(colour_vector), dimension(:,:,:,:,:) :: phi
         type(colour_vector), dimension(:,:,:,:,:) :: Dphi
       end subroutine SqMatrixOperate
    end interface

    integer, intent(in) :: n_lambda !number of eigenvectors to project out of M
    real(DP), dimension(n_lambda) :: lambda_i !The sign function acting on the eigenvalues.
    type(colour_vector), dimension(:,:,:,:,:,:) :: v_lambda !eigenvectors to project out of M
    real(DP) :: delta_lambda !The precision to which the eigenvectors are correct.
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: v_l
    logical :: ProjectEV
    integer :: ProjectFreq=10, i_lambda

    integer :: iterations !the total number of iterations required
    real(DP), dimension(n_v) :: sigma_i !offset matrix shifts
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: residue
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, Mphi
    type(colour_vector), dimension(:,:,:,:,:,:), allocatable :: phi_i
    real(DP) :: normsq_r, prevnormsq_r, beta, prevbeta, alpha, phidotMphi, offset, tau = 1.0d-1, normpsi, normphi
    real(DP), dimension(n_v) :: alpha_i, beta_i, prevbeta_i, zeta_i, prevzeta_i, nextzeta_i
    logical, dimension(n_v) :: converged_i, prevconverged_i
    real(dp) :: intime, outtime
    complex(dc) :: v_ldotphi
    integer :: ix,iy,iz,it,is, i_v

    ! begin execution
    allocate(residue(nxp,nyp,nzp,ntp,ns))
    allocate(phi(nxp,nyp,nzp,ntp,ns))
    allocate(Mphi(nxp,nyp,nzp,ntp,ns))
    allocate(v_l(nxp,nyp,nzp,ntp,ns))
    allocate(phi_i(nxp,nyp,nzp,ntp,ns,n_v))

    ProjectEV = project_eigenspace

    ProjectFreq = 10 - log10(delta_lambda)

    !translate the sigma shifts so that sigma_i(1)=0

    offset = c_l(1)
    if (offset /= 0.0d0) then
       sigma_i = c_l - offset
    else
       sigma_i = c_l
    end if

    residue = psi

    chi = zero_vector

    if ( ProjectEV ) call ProjectVectorSpace(n_lambda,v_lambda,residue)

    do i_v=1,n_v
       phi_i(:,:,:,:,:,i_v) = residue
    end do

    prevbeta = 1.0d0
    prevzeta_i = 1.0d0
    zeta_i= 1.0d0
    alpha_i = 0

    normsq_r = fermion_normsq(residue)
    normpsi = sqrt(normsq_r)

    if (normsq_r == 0.0d0) return !source was contained in projected orthogonal space
    !if (sqrt(normsq_r) <= tolerance) return !source was contained in projected orthogonal space

    phi = residue

    call SqMatrixOperate(phi,Mphi)
    if ( ProjectEV ) call ProjectVectorSpace(n_lambda,v_lambda,Mphi)
    if (offset /= 0) then
       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Mphi_x(is)%Cl = Mphi_x(is)%Cl + offset*phi_x(is)%Cl
       end do; end do; end do; end do; end do
    end if

    phidotMphi = real_inner_product(phi,Mphi)
    if (phidotMphi == 0.0d0) return  !source was contained in projected orthogonal space

    iterations = 0
    iterations_i = 0
    converged_i = .false.

    do
       if (timing) intime = mpi_wtime()

       !If the slowest converegence is extremely slow compared to the other shifts, we need to have a "safety net"
       !that prevents underflow in zeta_i for the already converged values.
       prevconverged_i = converged_i
       converged_i = (sqrt(normsq_r)*zeta_i/normpsi < tau*tolerance )
       !converged_i = (sqrt(normsq_r)*zeta_i/normpsi < epsilon(1.0d0) )
       do i_v=1,n_v
          if (converged_i(i_v)) zeta_i(i_v) = epsilon(1.0d0)
          if (converged_i(i_v) .and. .not. prevconverged_i(i_v)) iterations_i(i_v) = iterations
       end do

       if (sqrt(normsq_r)/normpsi < tolerance) iterations_i(1) = iterations
       if (sqrt(normsq_r)/normpsi < tolerance) exit

       !mpiprint '(I4,4F20.16)', iterations, sqrt(normsq_r), alpha, beta, phidotMphi

       iterations = iterations + 1
       ProjectEV = ( modulo(iterations,ProjectFreq)==0 ) .and. project_eigenspace

       beta = -normsq_r/phidotMphi

       nextzeta_i = zeta_i*prevzeta_i*prevbeta/( beta*alpha*(prevzeta_i-zeta_i)+prevzeta_i*prevbeta*(1.0d0-sigma_i*beta) )
       beta_i = beta*nextzeta_i/zeta_i

       do i_v=1,n_v
          if (.not. converged_i(i_v)) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                chi_x(is)%Cl = chi_x(is)%Cl - (b_l(i_v)*beta_i(i_v))*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do
       if ( ProjectEV ) call ProjectVectorSpace(n_lambda,v_lambda,chi)

       do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          residue_x(is)%Cl = residue_x(is)%Cl + beta*Mphi_x(is)%Cl
       end do; end do; end do; end do; end do
       if ( ProjectEV ) call ProjectVectorSpace(n_lambda,v_lambda,residue)

       prevnormsq_r = normsq_r
       normsq_r = fermion_normsq(residue)

       alpha = normsq_r/prevnormsq_r
       alpha_i = alpha*nextzeta_i*beta_i/(zeta_i*beta)

       do i_v=1,n_v
          if (.not. converged_i(i_v) ) then
             do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                phi_ix(is)%Cl = nextzeta_i(i_v)*residue_x(is)%Cl + alpha_i(i_v)*phi_ix(is)%Cl
             end do; end do; end do; end do; end do
          end if
       end do

       phi = phi_i(:,:,:,:,:,1)
       call SqMatrixOperate(phi,Mphi)
       if (offset /= 0) then
          do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             Mphi_x(is)%Cl = Mphi_x(is)%Cl + offset*phi_x(is)%Cl
          end do; end do; end do; end do; end do
       end if
       if ( ProjectEV ) call ProjectVectorSpace(n_lambda,v_lambda,Mphi)
       phidotMphi = real_inner_product(phi,Mphi)

       prevzeta_i = zeta_i
       zeta_i = nextzeta_i

       prevbeta = beta

       prevbeta_i = beta_i

       if (timing) then
          outtime = mpi_wtime()
          call TimingUpdate(ncalls1,outtime,intime,mintime1,maxtime1,meantime1)
       end if

    end do

    ProjectEV = project_eigenspace

    call SqMatrixOperate(chi,Mphi)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       chi_x(is)%Cl = Mphi_x(is)%Cl + c_2n*chi_x(is)%Cl
    end do; end do; end do; end do; end do
    if ( ProjectEV ) call ProjectVectorSpace(n_lambda,v_lambda,chi)

    call MatrixOperate(chi,Mphi)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       chi_x(is)%Cl = d_n*Mphi_x(is)%Cl
    end do; end do; end do; end do; end do
    if ( ProjectEV ) call ProjectVectorSpace(n_lambda,v_lambda,chi)

    if ( ProjectEV ) then
       do i_lambda = 1, n_lambda
          v_l = v_lambda(:,:,:,:,:,i_lambda)
          v_ldotphi = inner_product(v_l,psi)
          do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             chi_x(is)%Cl = chi_x(is)%Cl + (lambda_i(i_lambda)*v_ldotphi)*v_lambda(ix,iy,iz,it,is,i_lambda)%Cl
          end do; end do; end do; end do; end do
       end do
    end if

    normpsi = fermion_norm(psi)

    deallocate(residue)
    deallocate(phi)
    deallocate(Mphi)
    deallocate(v_l)
    deallocate(phi_i)

  end subroutine MatrixSignFunctionOperate

#undef chi_x
#undef phi_x
#undef Mphi_x
#undef phi_ix

end module SignFunction

