!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/gl3diag.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: gl3diag.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module GL3Diag

  use ColourTypes
  use VectorAlgebra
  use MatrixAlgebra
  use MPIInterface
  use Kinds
  implicit none
  private

  !Routines for the 3x3 matrices (General linear group)

  public :: DiagonaliseMat

contains

  subroutine DiagonaliseMat(H_xd,lambda_xd,V_xd)
    ! begin args: H_xd, lambda_xd, V_xd

    type(colour_matrix) :: H_xd !Hermitian matrix
    type(real_vector)   :: lambda_xd !eigenvalues
    type(colour_matrix) :: V_xd !eigenvector matrix
    ! begin local_vars
    integer :: mu

    type(colour_vector) :: psi, Dpsi, grad_mu, search, Dsearch
    real(dp) :: normsq_grad_mu, normsq_grad_mu_old
    real(dp) :: norm_search, norm_psi

    logical :: converged, recalc
    real(dp)  :: tolerance = 1.0d-12
    integer :: iterations, mycount

    complex(dc) :: psidotsearch, grad_mudotsearch
    real(dp) :: psidotDpsi, searchdotDsearch

    real(dp) :: cos_delta, sin_delta, cos_theta, sin_theta
    real(dp) :: a2, a3

    real(dp) :: alpha, beta, mu_psi, mu_phi, mu_chi
    complex(dc) :: phidotDchi, chidotDphi

    type(colour_vector) :: phi, chi
#define Dphi grad_mu
#define Dchi search

    complex(dc) :: phidotv_p, chidotv_p, phidotv_m, chidotv_m

    real(dp) :: lambda_p, lambda_m, temp1, temp2
    complex(dc) :: nu_p, nu_m

    integer :: t_in, t_out
    integer :: ix,iy,iz,it

    ! begin execution

    psi%Cl = 0.0d0
    psi%Cl(1) = 1.0d0

    call normalise_vector(psi)

    call MultiplyMatClrVec(Dpsi,H_xd,psi)

    mu_psi = real_vector_inner_product(psi,Dpsi)

    grad_mu%Cl = Dpsi%Cl - mu_psi*psi%Cl

    normsq_grad_mu = vector_normsq(grad_mu)

    search = grad_mu
    norm_search = vector_norm(search)

    call MultiplyMatClrVec(Dsearch,H_xd,search)

    converged = .false.
    recalc = .false.
    iterations = 0

    psidotsearch = vector_inner_product(psi,search)

    temp1 = sqrt(normsq_grad_mu)
    temp2 = norm_search

    do
       converged = ( sqrt(normsq_grad_mu) < tolerance ) .or. ( norm_search**2 < tolerance**2 )

       if ( converged ) exit
       norm_psi = vector_norm(psi)
       if ( iterations >= 60 ) print '(a,I4,5E12.4)', "Diag Mat ", iterations, sqrt(normsq_grad_mu), norm_search, temp1, temp2
       if ( iterations >= 60 ) exit

       iterations = iterations + 1
       recalc = ( modulo(iterations,3) == 0 )

       normsq_grad_mu_old = normsq_grad_mu
       psidotDpsi = real_vector_inner_product(psi,Dpsi)
       searchdotDsearch = real_vector_inner_product(search,Dsearch)/(norm_search**2)

       a2 = 0.5d0*(psidotDpsi - searchdotDsearch)
       a3 = normsq_grad_mu/norm_search
       alpha = sqrt( a2**2 + a3**2)

       cos_delta = a2/alpha
       sin_delta = a3/alpha

       if (cos_delta <= 0.0d0 ) then
          cos_theta = sqrt(0.5d0*(1.0d0-cos_delta))
          sin_theta = -0.5d0*sin_delta/cos_theta
       else
          sin_theta = -sqrt(0.5d0*(1.0d0+cos_delta))
          cos_theta = -0.5d0*sin_delta/sin_theta
       end if

       psi%Cl = cos_theta*psi%Cl + sin_theta*(search%Cl/norm_search)

       if ( recalc ) then

          call normalise_vector(psi)
          call MultiplyMatClrVec(Dpsi,H_xd,psi)

       else

          Dpsi%Cl = cos_theta*Dpsi%Cl + sin_theta*(Dsearch%Cl/norm_search)

       end if

       mu_psi =  real_vector_inner_product(psi,Dpsi)

       grad_mu%Cl = Dpsi%Cl - mu_psi*psi%Cl

       normsq_grad_mu = vector_normsq(grad_mu)

       converged = ( sqrt(normsq_grad_mu) < tolerance )
       if ( converged ) exit

       beta = cos_theta*(normsq_grad_mu/normsq_grad_mu_old)

       if ( recalc ) beta = 0.0d0

       psidotsearch = vector_inner_product(psi,search)
       search%Cl = grad_mu%Cl + beta*( search%Cl - psidotsearch*psi%Cl )

       if ( recalc ) then
          psidotsearch = vector_inner_product(psi,search)

          Dsearch%Cl = search%Cl-grad_mu%Cl

          grad_mudotsearch = vector_inner_product(grad_mu,Dsearch)
          grad_mudotsearch = grad_mudotsearch/normsq_grad_mu

          search%Cl = search%Cl - psidotsearch*psi%Cl - grad_mudotsearch*grad_mu%Cl
       end if

       norm_search = vector_norm(search)
       call MultiplyMatClrVec(Dsearch,H_xd,search)

    end do

    call normalise_vector(psi)

    call MultiplyMatClrVec(Dpsi,H_xd,psi)
    mu_psi = real_vector_inner_product(psi,Dpsi)

    phi%Cl = 0.0d0
    phi%Cl(2) = 1.0d0

    call orthogonalise_vectors(phi,psi)
    call normalise_vector(phi)

    call vector_product(chi,psi,phi)
    call normalise_vector(chi)

    call MultiplyMatClrVec(Dphi,H_xd,phi)
    call orthogonalise_vectors(Dphi,psi)

    call MultiplyMatClrVec(Dchi,H_xd,chi)
    call orthogonalise_vectors(Dchi,psi)

    mu_phi = real_vector_inner_product(phi,Dphi)
    mu_chi = real_vector_inner_product(chi,Dchi)

    grad_mu%Cl = Dphi%Cl - mu_phi*phi%Cl
    normsq_grad_mu = vector_normsq(grad_mu)

    if ( sqrt(normsq_grad_mu) > tolerance )  then
       !Perform a complex rotation to obtain the last two eigenvectors.

       phidotDchi = vector_inner_product(phi,Dchi)
       chidotDphi = vector_inner_product(chi,Dphi)

       beta = -(mu_phi+mu_chi)
       alpha = mu_phi*mu_chi - real(phidotDchi*chidotDphi)

       lambda_p = 0.5d0*(-beta - sqrt(beta**2 - 4.0d0*alpha))
       lambda_m = 0.5d0*(-beta + sqrt(beta**2 - 4.0d0*alpha))

       nu_p = -phidotDchi/(mu_phi - lambda_p)

       alpha = sqrt(real(nu_p)**2 + aimag(nu_p)**2 + 1.0d0)
       phidotv_p = nu_p/alpha
       chidotv_p = 1.0d0/alpha

       nu_m = -chidotDphi/(mu_chi - lambda_m)

       alpha = sqrt(real(nu_m)**2 + aimag(nu_m)**2 + 1.0d0)

       phidotv_m = 1.0d0/alpha
       chidotv_m = nu_m/alpha

       !Temporarily store the eigenvectors rotated from phi and chi
       Dphi%Cl = phi%Cl*phidotv_p + chi%Cl*chidotv_p
       Dchi%Cl = phi%Cl*phidotv_m + chi%Cl*chidotv_m

       phi = Dphi
       chi = Dchi

       call normalise_vector(phi)
       call normalise_vector(chi)

       call MultiplyMatClrVec(Dphi,H_xd,phi)
       call MultiplyMatClrVec(Dchi,H_xd,chi)

       mu_phi = real_vector_inner_product(phi,Dphi)
       mu_chi = real_vector_inner_product(chi,Dchi)
    end if

    lambda_xd%Cl(1) = mu_psi
    V_xd%Cl(:,1) = psi%Cl(:)

    lambda_xd%Cl(2) = mu_phi
    V_xd%Cl(:,2) = phi%Cl(:)

    lambda_xd%Cl(3) = mu_chi
    V_xd%Cl(:,3) = chi%Cl(:)

#undef Dphi
#undef Dchi


  end subroutine DiagonaliseMat

end module GL3Diag

