!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

#include "defines.h"

module QuarkPropagator
  use GaugeField
  use GaugeFieldMPIComms
  use FermionField
  use ConjGradSolvers
  use MPIInterface
  use LatticeSize
  use SpinorTypes
  use FermionTypes
  use ColourTypes
  use Kinds
  use MatrixAlgebra
  use VectorAlgebra
  use StoutLinks
  use ColourFieldOps
  use TextIO
  use Strings
  use SourceTypes
  use FermionAction
  implicit none
  private

  real(dp), public :: alpha_sm
  integer, public :: n_smear, js_start=1, js_end=1, jc_start=1, jc_end = nc
  logical, public :: SmearedSource
  logical, public :: parallel_io = .false.

  public :: WriteSourceMetaData
  public :: ReadSourceMetaData
  public :: GetSourceParams
  !public :: GetQuarkPropParams
  public :: GetSource
  public :: GetPointSource
  public :: GetWallSource
  public :: GetZ2NoiseSource
  public :: SmearSource
  public :: SmearSink

  interface ChiralBasisToSakurai
     module procedure ChiralBasisToSakurai_cs
     module procedure ChiralBasisToSakurai_sc
  end interface

  interface SakuraiBasisToChiral
     module procedure SakuraiBasisToChiral_cs
     module procedure SakuraiBasisToChiral_sc
  end interface

  public :: ChiralBasisToSakurai
  public :: SakuraiBasisToChiral

  interface GetPropColumn
     module procedure GetPropColumn_cs
     module procedure GetPropColumn_sc
  end interface

  interface PutPropColumn
     module procedure PutPropColumn_cs
     module procedure PutPropColumn_sc
  end interface

  public :: GetPropColumn
  public :: PutPropColumn

  type SourcePositionType
     integer :: jx
     integer :: jy
     integer :: jz
     integer :: jt
  end type SourcePositionType

  type SourceSmearingType
     integer  :: n_smsrc
     real(dp) :: alpha_smsrc
     logical  :: UseUzero
     real(dp) :: u0_smsrc
     logical  :: UseStoutLinks
     real(dp) :: alpha_stout
     integer  :: n_stout
  end type SourceSmearingType

  type(SourcePositionType), public :: qpsrc_pt !! Point source
  type(WallSourceType), public :: qpsrc_wl !! Wall Source
  type(NoiseSourceType), public :: qpsrc_ns !! Noise Source
  type(SourceSmearingType), public :: qpsrc_sm !! Smeared Source
  type(SSTSourceType), public :: qpsrc_sst !! SST Source

  type(spin_colour_matrix), dimension(:,:,:,:), allocatable, public :: psipsibar ! the full propagator
  type(spin_colour_matrix), dimension(:,:,:,:), pointer, public :: qp_sst_source_prop
  type(colour_vector), dimension(:,:,:,:,:), allocatable, public :: qp_noise_field
  
contains

/*
   subroutine InitialiseQuarkPropCode(file_stub, U_xd)
    character(len=*) :: file_stub
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd

    !call Put("Reading propagator IO parameters.")
    !call GetQuarkPropIOParams(file_stub)

    call Put("Reading quark propagator parameters.")
    call GetQuarkPropParams(file_stub)

    call Put("Reading fermion action parameters.")
    call SelectFermionAction(qp_FermionAction)
    call GetFermionActionParams(file_stub)

    call InitialiseFermionMatrix(U_xd)

    !#ifdef _overlap_
    !    call Put("Reading overlap parameters.")
    !    call GetOverlapParams(file_stub)
    !#endif

  end subroutine InitialiseQuarkPropCode
*/

  subroutine WriteSourceMetaData(file_unit, iSourceType)
    integer :: file_unit, iSourceType

    if ( i_am_root ) then
       select case(iSourceType)
       case(1) ! Point Source
          write(file_unit,'(i20,a)') qpsrc_pt%jx, " !qpsrc_pt%jx"
          write(file_unit,'(i20,a)') qpsrc_pt%jy, " !qpsrc_pt%jy"
          write(file_unit,'(i20,a)') qpsrc_pt%jz, " !qpsrc_pt%jz"
          write(file_unit,'(i20,a)') qpsrc_pt%jt, " !qpsrc_pt%jt"
       case(2) ! Wall Source
          write(file_unit,'(i20,a)') qpsrc_wl%jt, " !qpsrc_wl%jt"
       case(3) ! Smeared Source
          write(file_unit,'(i20,a)') qpsrc_pt%jx, " !qpsrc_pt%jx"
          write(file_unit,'(i20,a)') qpsrc_pt%jy, " !qpsrc_pt%jy"
          write(file_unit,'(i20,a)') qpsrc_pt%jz, " !qpsrc_pt%jz"
          write(file_unit,'(i20,a)') qpsrc_pt%jt, " !qpsrc_pt%jt"

          write(file_unit,'(i20,a)') qpsrc_sm%n_smsrc, " !qpsrc_sm%n_smsrc"
          write(file_unit,'(f20.10,a)') qpsrc_sm%alpha_smsrc, " !qpsrc_sm%alpha_smsrc"
          write(file_unit,'(l20,a)') qpsrc_sm%UseUzero, " !qpsrc_sm%UseUzero"
          write(file_unit,'(f20.10,a)') qpsrc_sm%u0_smsrc, " !qpsrc_sm%u0_smsrc"
          write(file_unit,'(l20,a)') qpsrc_sm%UseStoutLinks, " !qpsrc_sm%UseStoutLinks"
          write(file_unit,'(f20.10,a)') qpsrc_sm%alpha_stout, " !qpsrc_sm%alpha_stout"
          write(file_unit,'(i20,a)') qpsrc_sm%n_stout, " !qpsrc_sm%n_stout"
       case(4) ! Noise Source
          write(file_unit,'(a)') qpsrc_ns%NoiseFile
          write(file_unit,'(l20,a)') qpsrc_ns%iColourDilution, " !qpsrc_ns%ColourDilution"
          write(file_unit,'(l20,a)') qpsrc_ns%iSpinDilution, " !qpsrc_ns%iSpin_dilution"
          write(file_unit,'(i20,a)') qpsrc_ns%iTimeDilution, " !qpsrc_ns%iTime_dilution"
       case(5)  ! SST Source
          write(file_unit,'(a)') trim(qpsrc_sst%SourcePropFile)
          write(file_unit,'(i20,a)') qpsrc_sst%j_mu, " !qpsrc_sst%j_mu"
          write(file_unit,'(i20,a)') qpsrc_sst%it_s, " !qpsrc_sst%it_s"
          write(file_unit,'(a5,3i5,a)') "", qpsrc_sst%j_x, " !qpsrc_sst%j_x"
          write(file_unit,'(a5,3i5,a)') "", qpsrc_sst%p_x, " !qpsrc_sst%p_x"
          write(file_unit,'(i20,a)') qpsrc_sst%currentType, " !qpsrc_sst%currentType"
       end select
    end if

  end subroutine WriteSourceMetaData

  subroutine ReadSourceMetaData(file_unit, iSourceType)
    integer :: file_unit, iSourceType

    integer :: mu

    select case(iSourceType)
    case(1) ! Point Source
       call Get(qpsrc_pt%jx,file_unit)
       call Get(qpsrc_pt%jy,file_unit)
       call Get(qpsrc_pt%jz,file_unit)
       call Get(qpsrc_pt%jt,file_unit)
    case(2) ! Wall Source
       call Get(qpsrc_wl%jt,file_unit)
    case(3) ! Smeared Source
       call Get(qpsrc_pt%jx,file_unit)
       call Get(qpsrc_pt%jy,file_unit)
       call Get(qpsrc_pt%jz,file_unit)
       call Get(qpsrc_pt%jt,file_unit)

       call Get(qpsrc_sm%n_smsrc,file_unit)
       call Get(qpsrc_sm%alpha_smsrc,file_unit)
       call Get(qpsrc_sm%UseUzero,file_unit)
       call Get(qpsrc_sm%u0_smsrc,file_unit)
       call Get(qpsrc_sm%UseStoutLinks,file_unit)
       call Get(qpsrc_sm%alpha_stout,file_unit)
       call Get(qpsrc_sm%n_stout,file_unit)

    case(4) ! Noise Source
       call Get(qpsrc_ns%NoiseFile,file_unit)
       call Get(qpsrc_ns%iColourDilution,file_unit)
       call Get(qpsrc_ns%iSpinDilution,file_unit)
       call Get(qpsrc_ns%iTimeDilution,file_unit)
    case(5)  ! SST Source
       call Get(qpsrc_sst%SourcePropFile,file_unit)
       call Get(qpsrc_sst%j_mu,file_unit)
       call Get(qpsrc_sst%it_s,file_unit)
       call Get(qpsrc_sst%j_x,file_unit)
       call Get(qpsrc_sst%p_x,file_unit)
       call Get(qpsrc_sst%currentType,file_unit)
    end select
  end subroutine ReadSourceMetaData

  subroutine GetSourceParams(file_stub, iSourceType)
    character(len=*) :: file_stub
    integer :: iSourceType

    character(len=_FILENAME_LEN_) :: qpsrcfile
    integer :: file_unit
    integer :: mu

    select case(iSourceType)
    case(1)
       qpsrcfile = trim(file_stub)//".qpsrc_pt"
    case(2)
       qpsrcfile = trim(file_stub)//".qpsrc_wl"
    case(3)
       qpsrcfile = trim(file_stub)//".qpsrc_sm"
    case(4)
       qpsrcfile = trim(file_stub)//".qpsrc_ns"
    case(5)
       qpsrcfile = trim(file_stub)//".qpsrc_sst"
    end select

    call OpenTextfile(trim(qpsrcfile),file_unit,action='read')
    call ReadSourceMetaData(file_unit,iSourceType)
    call CloseTextfile(file_unit)

    call Put("iSourceType:"//str(iSourceType,24))
    select case(iSourceType)
    case(1) ! Point Source
       call Put("Point source parameters")
       call Put("qpsrc_pt%jx="//str(qpsrc_pt%jx,6))
       call Put("qpsrc_pt%jy="//str(qpsrc_pt%jy,6))
       call Put("qpsrc_pt%jz="//str(qpsrc_pt%jz,6))
       call Put("qpsrc_pt%jt="//str(qpsrc_pt%jt,6))

    case(2) ! Wall Source
       call Put("Wall source parameters")
       call Put("qpsrc_wl%jt="//str(qpsrc_wl%jt,6))
    case(3) ! Smeared Source
       call Put("Smeared source parameters")
       call Put("qpsrc_pt%jx="//str(qpsrc_pt%jx,6))
       call Put("qpsrc_pt%jy="//str(qpsrc_pt%jy,6))
       call Put("qpsrc_pt%jz="//str(qpsrc_pt%jz,6))
       call Put("qpsrc_pt%jt="//str(qpsrc_pt%jt,6))
       call Put("qpsrc_sm%n_smsrc="//str(qpsrc_sm%n_smsrc,6))
       call Put("qpsrc_sm%alpha_smsrc="//str(qpsrc_sm%alpha_smsrc,20))
       call Put("qpsrc_sm%UseUzero="//str(qpsrc_sm%UseUzero,6))
       call Put("qpsrc_sm%u0_smsrc="//str(qpsrc_sm%u0_smsrc,20))
       call Put("qpsrc_sm%UseStoutLinks="//str(qpsrc_sm%UseStoutLinks,6))
       call Put("qpsrc_sm%alpha_stout="//str(qpsrc_sm%alpha_stout,20))
       call Put("qpsrc_sm%n_stout="//str(qpsrc_sm%n_stout,6))

    case(4) ! Noise Source
       call Put("Noise source parameters") 
       call Put("Noise Source file name:"//trim(qpsrc_ns%NoiseFile))
       call Put("qpsrc_ns%iColourDilution="//str(qpsrc_ns%iColourDilution,24))
       call Put("qpsrc_ns%iSpinDilution="//str(qpsrc_ns%iSpinDilution,24))
       call Put("qpsrc_ns%iTimeDilution="//str(qpsrc_ns%iTimeDilution,24))
    case(5)  ! SST Source
       call Put("SST source parameters")
       call Put("qpsrc_sst%SourcePropFile="//trim(qpsrc_sst%SourcePropFile))
       call Put("qpsrc_sst%j_mu="//str(qpsrc_sst%j_mu,6))
       call Put("qpsrc_sst%it_s="//str(qpsrc_sst%it_s,6))
       call Put("qpsrc_sst%j_x="//str(qpsrc_sst%j_x,18,'(3i6)'))
       call Put("qpsrc_sst%p_x="//str(qpsrc_sst%p_x,18,'(3i6)'))
       call Put("qpsrc_sst%currentType="//str(qpsrc_sst%currentType,6))
    end select

  end subroutine GetSourceParams

  subroutine GetPropColumn_cs(chi,js,jc,psipsibar)

    type(colour_vector), dimension(:,:,:,:,:) :: chi
    type(colour_spin_matrix), dimension(:,:,:,:) :: psipsibar
    integer :: js,jc

    integer :: ix,iy,iz,it,is,ic

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       do is=1,ns; do ic=1,nc
          chi(ix,iy,iz,it,is)%Cl(ic) = psipsibar(ix,iy,iz,it)%cs(ic,jc,is,js) 
       end do; end do
    end do; end do; end do; end do
    
  end subroutine GetPropColumn_cs

  subroutine PutPropColumn_cs(chi,js,jc,psipsibar)

    type(colour_vector), dimension(:,:,:,:,:) :: chi
    type(colour_spin_matrix), dimension(:,:,:,:) :: psipsibar

    integer :: ix,iy,iz,it,is,ic
    integer :: js,jc

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       do is=1,ns; do ic=1,nc
          psipsibar(ix,iy,iz,it)%cs(ic,jc,is,js) = chi(ix,iy,iz,it,is)%Cl(ic) 
       end do; end do
    end do; end do; end do; end do
    
  end subroutine PutPropColumn_cs

  subroutine GetPropColumn_sc(chi,js,jc,psipsibar)

    type(colour_vector), dimension(:,:,:,:,:) :: chi
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar
    integer :: js,jc

    integer :: ix,iy,iz,it,is,ic

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       do is=1,ns; do ic=1,nc
          chi(ix,iy,iz,it,is)%Cl(ic) = psipsibar(ix,iy,iz,it)%sc(is,js,ic,jc)
       end do; end do
    end do; end do; end do; end do
    
  end subroutine GetPropColumn_sc

  subroutine PutPropColumn_sc(chi,js,jc,psipsibar)

    type(colour_vector), dimension(:,:,:,:,:) :: chi
    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar

    integer :: ix,iy,iz,it,is,ic
    integer :: js,jc

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       do is=1,ns; do ic=1,nc
          psipsibar(ix,iy,iz,it)%sc(is,js,ic,jc) = chi(ix,iy,iz,it,is)%Cl(ic) 
       end do; end do
    end do; end do; end do; end do
    
  end subroutine PutPropColumn_sc

  subroutine ChiralBasisToSakurai_cs(psipsibar)
    ! begin args: psipsibar

    type(colour_spin_matrix), dimension(:,:,:,:) :: psipsibar ! the full propagator

    ! begin local_vars
    type(colour_spin_matrix) :: eta_x, xi_x
    integer :: is, js, ikappa
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    real(dp), parameter :: inv_sqrt_two = sqrt(0.5_dp)
    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       
       eta_x = psipsibar(ix,iy,iz,it)

       !!          1    ( I  I )  
       !! xi =  ------- (      ) eta    
       !!       sqrt(2) (-I  I )     

       do js=1,ns
          xi_x%cs(:,:,1,js) = inv_sqrt_two*(eta_x%cs(:,:,1,js) +  eta_x%cs(:,:,3,js))
          xi_x%cs(:,:,2,js) = inv_sqrt_two*(eta_x%cs(:,:,2,js) +  eta_x%cs(:,:,4,js))
          xi_x%cs(:,:,3,js) = inv_sqrt_two*(eta_x%cs(:,:,3,js) -  eta_x%cs(:,:,1,js))
          xi_x%cs(:,:,4,js) = inv_sqrt_two*(eta_x%cs(:,:,4,js) -  eta_x%cs(:,:,2,js))
       end do

       !!          1        ( I -I )  
       !! eta =  ------- xi (      )    
       !!        sqrt(2)    ( I  I )     

       do is=1,ns
          eta_x%cs(:,:,is,1) = inv_sqrt_two*(xi_x%cs(:,:,is,1) +  xi_x%cs(:,:,is,3))
          eta_x%cs(:,:,is,2) = inv_sqrt_two*(xi_x%cs(:,:,is,2) +  xi_x%cs(:,:,is,4))
          eta_x%cs(:,:,is,3) = inv_sqrt_two*(xi_x%cs(:,:,is,3) -  xi_x%cs(:,:,is,1))
          eta_x%cs(:,:,is,4) = inv_sqrt_two*(xi_x%cs(:,:,is,4) -  xi_x%cs(:,:,is,2))
       end do
       
       psipsibar(ix,iy,iz,it) = eta_x

    end do; end do; end do; end do

  end subroutine ChiralBasisToSakurai_cs

  subroutine SakuraiBasisToChiral_cs(psipsibar)
    ! begin args: psipsibar

    type(colour_spin_matrix), dimension(:,:,:,:) :: psipsibar ! the full propagator

    ! begin local_vars
    type(colour_spin_matrix) :: eta_x, xi_x
    integer :: is, js, ikappa
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    real(dp), parameter :: inv_sqrt_two = sqrt(0.5_dp)

    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       
       eta_x = psipsibar(ix,iy,iz,it)
       
       !!           1    ( I -I )  
       !! xi =   ------- (      ) eta    
       !!        sqrt(2) ( I  I )     

       do js=1,ns
          xi_x%cs(:,:,1,js) = inv_sqrt_two*(eta_x%cs(:,:,1,js) -  eta_x%cs(:,:,3,js))
          xi_x%cs(:,:,2,js) = inv_sqrt_two*(eta_x%cs(:,:,2,js) -  eta_x%cs(:,:,4,js))
          xi_x%cs(:,:,3,js) = inv_sqrt_two*(eta_x%cs(:,:,3,js) +  eta_x%cs(:,:,1,js))
          xi_x%cs(:,:,4,js) = inv_sqrt_two*(eta_x%cs(:,:,4,js) +  eta_x%cs(:,:,2,js))
       end do

       !!          1         ( I  I )  
       !! eta =  -------  xi (      )    
       !!        sqrt(2)     (-I  I )     

       do is=1,ns
          eta_x%cs(:,:,is,1) = inv_sqrt_two*(xi_x%cs(:,:,is,1) -  xi_x%cs(:,:,is,3))
          eta_x%cs(:,:,is,2) = inv_sqrt_two*(xi_x%cs(:,:,is,2) -  xi_x%cs(:,:,is,4))
          eta_x%cs(:,:,is,3) = inv_sqrt_two*(xi_x%cs(:,:,is,3) +  xi_x%cs(:,:,is,1))
          eta_x%cs(:,:,is,4) = inv_sqrt_two*(xi_x%cs(:,:,is,4) +  xi_x%cs(:,:,is,2))
       end do
       
       psipsibar(ix,iy,iz,it) = eta_x

    end do; end do; end do; end do

  end subroutine SakuraiBasisToChiral_cs

  subroutine ChiralBasisToSakurai_sc(psipsibar)
    ! begin args: psipsibar

    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar ! the full propagator

    ! begin local_vars
    type(spin_colour_matrix) :: eta_x, xi_x
    integer :: is, js, ikappa
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    real(dp), parameter :: inv_sqrt_two = sqrt(0.5_dp)
    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       
       eta_x = psipsibar(ix,iy,iz,it)

       !!          1    ( I  I )  
       !! xi =  ------- (      ) eta    
       !!       sqrt(2) (-I  I )     

       do js=1,ns
          xi_x%sc(1,js,:,:) = inv_sqrt_two*(eta_x%sc(1,js,:,:) +  eta_x%sc(3,js,:,:))
          xi_x%sc(2,js,:,:) = inv_sqrt_two*(eta_x%sc(2,js,:,:) +  eta_x%sc(4,js,:,:))
          xi_x%sc(3,js,:,:) = inv_sqrt_two*(eta_x%sc(3,js,:,:) -  eta_x%sc(1,js,:,:))
          xi_x%sc(4,js,:,:) = inv_sqrt_two*(eta_x%sc(4,js,:,:) -  eta_x%sc(2,js,:,:))
       end do

       !!          1        ( I -I )  
       !! eta =  ------- xi (      )    
       !!        sqrt(2)    ( I  I )     

       do is=1,ns
          eta_x%sc(is,1,:,:) = inv_sqrt_two*(xi_x%sc(is,1,:,:) +  xi_x%sc(is,3,:,:))
          eta_x%sc(is,2,:,:) = inv_sqrt_two*(xi_x%sc(is,2,:,:) +  xi_x%sc(is,4,:,:))
          eta_x%sc(is,3,:,:) = inv_sqrt_two*(xi_x%sc(is,3,:,:) -  xi_x%sc(is,1,:,:))
          eta_x%sc(is,4,:,:) = inv_sqrt_two*(xi_x%sc(is,4,:,:) -  xi_x%sc(is,2,:,:))
       end do
       
       psipsibar(ix,iy,iz,it) = eta_x

    end do; end do; end do; end do

  end subroutine ChiralBasisToSakurai_sc

  subroutine SakuraiBasisToChiral_sc(psipsibar)
    ! begin args: psipsibar

    type(spin_colour_matrix), dimension(:,:,:,:) :: psipsibar ! the full propagator

    ! begin local_vars
    type(spin_colour_matrix) :: eta_x, xi_x
    integer :: is, js, ikappa
    integer :: ix, jx, iy, jy, iz, jz, it, jt
    real(dp), parameter :: inv_sqrt_two = sqrt(0.5_dp)

    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       
       eta_x = psipsibar(ix,iy,iz,it)
       
       !!           1    ( I -I )  
       !! xi =   ------- (      ) eta    
       !!        sqrt(2) ( I  I )     

       do js=1,ns
          xi_x%sc(1,js,:,:) = inv_sqrt_two*(eta_x%sc(1,js,:,:) -  eta_x%sc(3,js,:,:))
          xi_x%sc(2,js,:,:) = inv_sqrt_two*(eta_x%sc(2,js,:,:) -  eta_x%sc(4,js,:,:))
          xi_x%sc(3,js,:,:) = inv_sqrt_two*(eta_x%sc(3,js,:,:) +  eta_x%sc(1,js,:,:))
          xi_x%sc(4,js,:,:) = inv_sqrt_two*(eta_x%sc(4,js,:,:) +  eta_x%sc(2,js,:,:))
       end do

       !!          1         ( I  I )  
       !! eta =  -------  xi (      )    
       !!        sqrt(2)     (-I  I )     

       do is=1,ns
          eta_x%sc(is,1,:,:) = inv_sqrt_two*(xi_x%sc(is,1,:,:) -  xi_x%sc(is,3,:,:))
          eta_x%sc(is,2,:,:) = inv_sqrt_two*(xi_x%sc(is,2,:,:) -  xi_x%sc(is,4,:,:))
          eta_x%sc(is,3,:,:) = inv_sqrt_two*(xi_x%sc(is,3,:,:) +  xi_x%sc(is,1,:,:))
          eta_x%sc(is,4,:,:) = inv_sqrt_two*(xi_x%sc(is,4,:,:) +  xi_x%sc(is,2,:,:))
       end do
       
       psipsibar(ix,iy,iz,it) = eta_x

    end do; end do; end do; end do

  end subroutine SakuraiBasisToChiral_sc

  subroutine GetSource(rho,js,jc,iSourceType,U_xd)

    type(colour_vector), dimension(:,:,:,:,:) :: rho !source vector
    integer :: js,jc,iSourceType
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: jx,jy,jz,jt
    real(dp) :: u0
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: Usm_xd
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi

    !! ToDo: Remove the option of mean field improvement in the source and sink smearing?
    select case (iSourceType)
    case(1)
       jx = qpsrc_pt%jx
       jy = qpsrc_pt%jy
       jz = qpsrc_pt%jz
       jt = qpsrc_pt%jt
       call GetPointSource(rho,jx,jy,jz,jt,js,jc)
    case(2)
       jt = qpsrc_wl%jt
       call GetWallSource(rho,jt,js,jc)
    case(3)
       jx = qpsrc_pt%jx
       jy = qpsrc_pt%jy
       jz = qpsrc_pt%jz
       jt = qpsrc_pt%jt
       call GetPointSource(rho,jx,jy,jz,jt,js,jc)

       call CreateGaugeField(Usm_xd,1)
       if ( qpsrc_sm%UseStoutLinks ) then
          call StoutSmearLinks(U_xd,Usm_xd,qpsrc_sm%alpha_stout,qpsrc_sm%n_stout)
       else
          Usm_xd(:,:,:,:,:) = U_xd(1:nx,1:ny,1:nz,1:nt,:)
       end if
       u0 = 1.0d0
       if ( qpsrc_sm%UseUzero ) then
          u0 = qpsrc_sm%u0_smsrc
          if ( u0 < 0.0d0 ) call GetUzero(Usm_xd,u0)
       end if
       call SetBoundaryConditions(Usm_xd,ff_bcx, ff_bcy, ff_bcz, ff_bct)
       call ShadowGaugeField(Usm_xd,1)
       call SmearSource(rho,jt,js,qpsrc_sm%alpha_smsrc,qpsrc_sm%n_smsrc,Usm_xd,u0)
       call DestroyGaugeField(Usm_xd)
    case(4)
       !! Noise Source
       !! Do nothing here - handled externally at the moment.
    case(5)
       !! SST Source.
       !! Read in the regular propagator column.
       call CreateFermionField(chi,1)
       call GetPropColumn(chi,js,jc,qp_sst_source_prop)
       rho = zero_vector
       call AddSSTSource(rho,chi,U_xd,qpsrc_sst%kappa,qpsrc_sst%j_x,qpsrc_sst%j_mu,qpsrc_sst%it_s, &
            & qpsrc_sst%p_x,qpsrc_sst%currentType)
       call DestroyFermionField(chi)
    end select

  end subroutine GetSource

  subroutine GetPointSource(rho,jx,jy,jz,jt,js,jc)
    ! begin args: rho, jx, jy, jz, jt, js, jc, SmearedSource, alpha_smear, n_smear, U_xd

    type(colour_vector), dimension(:,:,:,:,:) :: rho
    integer :: jx,jy,jz,jt,js,jc
    
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    rho = zero_vector

    if ( site_is_mine(jx,jy,jz,jt) ) then
       call LatticeToSubLattice(jx,jy,jz,jt,ix,iy,iz,it)
       rho(ix,iy,iz,it,js)%Cl(jc) = 1.0d0
    end if

  end subroutine GetPointSource

  subroutine GetWallSource(rho,jt,js,jc)
    ! begin args: rho, jt, js, jc

    type(colour_vector), dimension(:,:,:,:,:) :: rho
    integer :: jx,jy,jz,jt,js,jc
    
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    rho = zero_vector

    if ( i_nt <= jt .and. jt <= j_nt ) then
       it = jt - i_nt + 1
       do iz=1,nz; do iy=1,ny; do ix=1,nx
          rho(ix,iy,iz,it,js)%Cl(jc) = 1.0d0
       end do; end do; end do
    end if

  end subroutine GetWallSource

  subroutine GetZ2NoiseSource(rho,jt,js,jc)
    ! begin args: rho, jt, js, jc

    type(colour_vector), dimension(:,:,:,:,:) :: rho
    integer :: jx,jy,jz,jt,js,jc
    
    ! begin local_vars
    integer :: ix,iy,iz,it
    real(dp) :: alpha
    ! begin execution

    rho = zero_vector
    if ( jt == 0 ) then
       ! jt ==0 => do all timeslices. 
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          call random_number(alpha)
          if ( alpha == 1.0d0 ) then
             rho(ix,iy,iz,it,js)%Cl(jc) = 0.0d0
          elseif ( alpha < 0.5d0 ) then
             rho(ix,iy,iz,it,js)%Cl(jc) = -1.0d0
          else
             rho(ix,iy,iz,it,js)%Cl(jc) = +1.0d0
          end if
       end do; end do; end do; end do
    else
       if ( i_nt <= jt .and. jt <= j_nt ) then
          ! Convert to sublattice coordinate.
          it = jt - i_nt + 1
          do iz=1,nz; do iy=1,ny; do ix=1,nx
             call random_number(alpha)
             if ( alpha == 1.0d0 ) then
                rho(ix,iy,iz,it,js)%Cl(jc) = 0.0d0
             elseif ( alpha < 0.5d0 ) then
                rho(ix,iy,iz,it,js)%Cl(jc) = -1.0d0
             else
                rho(ix,iy,iz,it,js)%Cl(jc) = +1.0d0
             end if
          end do; end do; end do
       end if
    end if
  end subroutine GetZ2NoiseSource

  subroutine SmearSource(rho,jt,js,alpha_smear,n_smear,U_xd,u0)
    ! begin args: rho, jt, js, alpha_smear, n_smear, U_xd

    type(colour_vector), dimension(:,:,:,:,:) :: rho
    integer :: jt,js
    real(dp), intent(in) :: alpha_smear
    integer, intent(in) :: n_smear
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: u0


    ! Gauge invariant (spatial) fermion source smearing.
    ! Recoded by Waseem Kamleh for use in MPI code.
    ! ref: NPB(proc. suppl.) 17, 361 (1990),
    !      PRD47, 5128 (1993)

    ! begin local_vars
    type(colour_vector), dimension(:,:,:), allocatable :: phi, rho_pr

    type(colour_vector) :: phi_px, phi_mx

    integer :: mu,ismear
    integer :: ix,iy,iz,it,dmu(nd)
    integer :: lx,ly,lz
    integer :: mx,my,mz
    integer :: mpi_status(nmpi_status), mpierror, mrank, prank, tag

#define U_mux        U_xd(ix,iy,iz,it,mu)
#define U_muxmmu     U_xd(mx,my,mz,it,mu)

#define rho_x    rho_pr(ix,iy,iz)
#define rho_xpmu rho_pr(lx,ly,lz)
#define rho_xmmu rho_pr(mx,my,mz)

#define phi_x    phi(ix,iy,iz)

    ! begin execution
    allocate(rho_pr(nxs,nys,nzs))
    allocate(phi(nxp,nyp,nzp))

    if ( (i_nt <= jt) .and. (jt <= j_nt) ) then
       it = jt - i_nt + 1
       rho_pr(1:nx,1:ny,1:nz) = rho(1:nx,1:ny,1:nz,it,js)

       do ismear=1,n_smear
          if ( nprocz > 1 ) then
             mrank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
             prank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)

             tag = nxs*nys
             
             call MPI_SendRecv(rho_pr(1,1,1)         ,nc*nxs*nys,mpi_dc,mrank,tag,&
                  &            rho_pr(1,1,mapz(nz+1)),nc*nxs*nys,mpi_dc,prank,tag,&
                  &            mpi_comm, mpi_status, mpierror)

             call MPI_SendRecv(rho_pr(1,1,nz)        ,nc*nxs*nys,mpi_dc,prank,tag,&
                  &            rho_pr(1,1,mapz(0))   ,nc*nxs*nys,mpi_dc,mrank,tag,&
                  &            mpi_comm, mpi_status, mpierror)
          end if

          phi = zero_vector

          do mu=1,nd-1
             dmu = 0
             dmu(mu) = 1

             do iz=1,nz
                lz = mapz(iz + dmu(3))
                mz = mapz(iz - dmu(3))
                do iy=1,ny
                   ly = mapy(iy + dmu(2))
                   my = mapy(iy - dmu(2))
                   do ix=1,nx
                      lx = mapx(ix + dmu(1))
                      mx = mapx(ix - dmu(1))

                      call MultiplyMatClrVec   (phi_px,U_mux   ,rho_xpmu)
                      call MultiplyMatDagClrVec(phi_mx,U_muxmmu,rho_xmmu)

                      phi_x%Cl = phi_x%Cl + (phi_px%Cl + phi_mx%Cl)/u0

                   end do
                end do
             end do

          end do

          do iz=1,nz; do iy=1,ny; do ix=1,nx
             rho_x%Cl = (1.0d0 - alpha_smear)*rho_x%Cl + (alpha_smear/6.0d0)*phi_x%Cl
          end do; end do; end do

       end do

       rho(1:nx,1:ny,1:nz,it,js) = rho_pr(1:nx,1:ny,1:nz)

    end if

    deallocate(rho_pr)
    deallocate(phi)

  end subroutine SmearSource
  
  subroutine SmearSink(chi,alpha_smear,n_smear,U_xd,u0)
    ! begin args: chi, alpha_smear, n_smear, U_xd

    type(spin_colour_matrix), dimension(:,:,:,:) :: chi
    real(dp) :: alpha_smear
    integer :: n_smear
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: u0

    !Note: sink indices should appear before source indices.
    !So, if (ic,is) are sink indices and (jc,js) are source indices, then
    !the appropriate elements of the propagator are chi(:,:,:,:)%cs(ic,jc,is,js)

    ! begin local_vars
    type(spin_colour_matrix), dimension(:,:,:), allocatable :: chi_pr, phi

    type(spin_colour_matrix) :: phi_px, phi_mx

    integer :: is,js
    integer :: mu,ismear
    integer :: ix,iy,iz,it,dmu(nd)
    integer :: lx,ly,lz
    integer :: mx,my,mz
    integer :: mpi_status(nmpi_status), mpierror, mrank, prank, tag

#define U_mux        U_xd(ix,iy,iz,it,mu)
#define U_muxmmu     U_xd(mx,my,mz,it,mu)

#define chi_x    chi_pr(ix,iy,iz)
#define chi_xpmu chi_pr(lx,ly,lz)
#define chi_xmmu chi_pr(mx,my,mz)

#define phi_x    phi(ix,iy,iz)

    allocate(chi_pr(nxs,nys,nzs))
    allocate(phi(nxp,nyp,nzp))
    ! begin execution

    do it=1,nt

       chi_pr(1:nx,1:ny,1:nz) = chi(1:nx,1:ny,1:nz,it)

       do ismear=1,n_smear

          if ( nprocz > 1 ) then
             mrank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
             prank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)

             tag = nxs*nys

             call MPI_SendRecv(chi_pr(1,1,1)         ,ns*ns*nc*nc*nxs*nys,mpi_dc,mrank,tag,&
                  &            chi_pr(1,1,mapz(nz+1)),ns*ns*nc*nc*nxs*nys,mpi_dc,prank,tag,&
                  &            mpi_comm, mpi_status, mpierror)

             call MPI_SendRecv(chi_pr(1,1,nz)        ,nc*nc*nxs*nys,mpi_dc,prank,tag,&
                  &            chi_pr(1,1,mapz(0))   ,nc*nc*nxs*nys,mpi_dc,mrank,tag,&
                  &            mpi_comm, mpi_status, mpierror)
          end if

          phi = zero_spin_colour_matrix

          do mu=1,nd-1
             dmu = 0
             dmu(mu) = 1

             do iz=1,nz
                lz = mapz(iz + dmu(3))
                mz = mapz(iz - dmu(3))
                do iy=1,ny
                   ly = mapy(iy + dmu(2))
                   my = mapy(iy - dmu(2))
                   do ix=1,nx
                      lx = mapx(ix + dmu(1))
                      mx = mapx(ix - dmu(1))

                      call MultiplyMatSCMat   (phi_px,U_mux   ,chi_xpmu)
                      call MultiplyMatDagSCMat(phi_mx,U_muxmmu,chi_xmmu)

                      phi_x%sc = phi_x%sc + (phi_px%sc + phi_mx%sc)/u0
                      
                   end do
                end do
             end do

          end do

          do iz=1,nz; do iy=1,ny; do ix=1,nx
             chi_x%sc = (1.0d0 - alpha_smear)*chi_x%sc + (alpha_smear/6.0d0)*phi_x%sc
          end do; end do; end do

       end do

       chi(1:nx,1:ny,1:nz,it) = chi_pr(1:nx,1:ny,1:nz)

    end do

    deallocate(chi_pr)
    deallocate(phi)

  contains

    pure subroutine MultiplyMatSCMat(MM,left,right)

      type(colour_matrix), intent(in) :: left
      type(spin_colour_matrix), intent(in) :: right
      type(spin_colour_matrix), intent(out) :: MM

      MM%sc(:,:,1,1) = left%cl(1,1)*right%sc(:,:,1,1) + left%cl(1,2)*right%sc(:,:,2,1) + left%cl(1,3)*right%sc(:,:,3,1)
      MM%sc(:,:,2,1) = left%cl(2,1)*right%sc(:,:,1,1) + left%cl(2,2)*right%sc(:,:,2,1) + left%cl(2,3)*right%sc(:,:,3,1)
      MM%sc(:,:,3,1) = left%cl(3,1)*right%sc(:,:,1,1) + left%cl(3,2)*right%sc(:,:,2,1) + left%cl(3,3)*right%sc(:,:,3,1)

      MM%sc(:,:,1,2) = left%cl(1,1)*right%sc(:,:,1,2) + left%cl(1,2)*right%sc(:,:,2,2) + left%cl(1,3)*right%sc(:,:,3,2)
      MM%sc(:,:,2,2) = left%cl(2,1)*right%sc(:,:,1,2) + left%cl(2,2)*right%sc(:,:,2,2) + left%cl(2,3)*right%sc(:,:,3,2)
      MM%sc(:,:,3,2) = left%cl(3,1)*right%sc(:,:,1,2) + left%cl(3,2)*right%sc(:,:,2,2) + left%cl(3,3)*right%sc(:,:,3,2)

      MM%sc(:,:,1,3) = left%cl(1,1)*right%sc(:,:,1,3) + left%cl(1,2)*right%sc(:,:,2,3) + left%cl(1,3)*right%sc(:,:,3,3)
      MM%sc(:,:,2,3) = left%cl(2,1)*right%sc(:,:,1,3) + left%cl(2,2)*right%sc(:,:,2,3) + left%cl(2,3)*right%sc(:,:,3,3)
      MM%sc(:,:,3,3) = left%cl(3,1)*right%sc(:,:,1,3) + left%cl(3,2)*right%sc(:,:,2,3) + left%cl(3,3)*right%sc(:,:,3,3)

    end subroutine MultiplyMatSCMat

    
    pure subroutine MultiplyMatdagSCMat(MM,left,right)

      type(colour_matrix), intent(in) :: left
      type(spin_colour_matrix), intent(in) :: right
      type(spin_colour_matrix), intent(out) :: MM

      MM%sc(:,:,1,1) = conjg(left%cl(1,1))*right%sc(:,:,1,1) + conjg(left%cl(2,1))*right%sc(:,:,2,1) + conjg(left%cl(3,1))*right%sc(:,:,3,1)
      MM%sc(:,:,2,1) = conjg(left%cl(1,2))*right%sc(:,:,1,1) + conjg(left%cl(2,2))*right%sc(:,:,2,1) + conjg(left%cl(3,2))*right%sc(:,:,3,1)
      MM%sc(:,:,3,1) = conjg(left%cl(1,3))*right%sc(:,:,1,1) + conjg(left%cl(2,3))*right%sc(:,:,2,1) + conjg(left%cl(3,3))*right%sc(:,:,3,1)

      MM%sc(:,:,1,2) = conjg(left%cl(1,1))*right%sc(:,:,1,2) + conjg(left%cl(2,1))*right%sc(:,:,2,2) + conjg(left%cl(3,1))*right%sc(:,:,3,2)
      MM%sc(:,:,2,2) = conjg(left%cl(1,2))*right%sc(:,:,1,2) + conjg(left%cl(2,2))*right%sc(:,:,2,2) + conjg(left%cl(3,2))*right%sc(:,:,3,2)
      MM%sc(:,:,3,2) = conjg(left%cl(1,3))*right%sc(:,:,1,2) + conjg(left%cl(2,3))*right%sc(:,:,2,2) + conjg(left%cl(3,3))*right%sc(:,:,3,2)

      MM%sc(:,:,1,3) = conjg(left%cl(1,1))*right%sc(:,:,1,3) + conjg(left%cl(2,1))*right%sc(:,:,2,3) + conjg(left%cl(3,1))*right%sc(:,:,3,3)
      MM%sc(:,:,2,3) = conjg(left%cl(1,2))*right%sc(:,:,1,3) + conjg(left%cl(2,2))*right%sc(:,:,2,3) + conjg(left%cl(3,2))*right%sc(:,:,3,3)
      MM%sc(:,:,3,3) = conjg(left%cl(1,3))*right%sc(:,:,1,3) + conjg(left%cl(2,3))*right%sc(:,:,2,3) + conjg(left%cl(3,3))*right%sc(:,:,3,3)

    end subroutine MultiplyMatdagSCMat


  end subroutine SmearSink

  subroutine SmearSinkOld(rho,alpha_smear,n_smear,U_xd,u0)
    ! begin args: rho, alpha_smear, n_smear, U_xd

    type(colour_matrix), dimension(:,:,:,:,:,:) :: rho
    real(dp) :: alpha_smear
    integer :: n_smear
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: u0

    !Note: sink indices should appear before source indices.
    !So, if (ic,is) are sink indices and (jc,js) are source indices, then
    !the appropriate elements of the propagator are rho(:,:,:,:,is,js)%cl(ic,jc)

    ! begin local_vars
    type(colour_matrix), dimension(:,:,:), allocatable :: rho_pr, phi

    type(colour_matrix) :: phi_px, phi_mx

    integer :: is,js
    integer :: mu,ismear
    integer :: ix,iy,iz,it,dmu(nd)
    integer :: lx,ly,lz
    integer :: mx,my,mz
    integer :: mpi_status(nmpi_status), mpierror, mrank, prank, tag

#define U_mux        U_xd(ix,iy,iz,it,mu)
#define U_muxmmu     U_xd(mx,my,mz,it,mu)

#define rho_x    rho_pr(ix,iy,iz)
#define rho_xpmu rho_pr(lx,ly,lz)
#define rho_xmmu rho_pr(mx,my,mz)

#define phi_x    phi(ix,iy,iz)

    allocate(rho_pr(nxs,nys,nzs))
    allocate(phi(nxp,nyp,nzp))
    ! begin execution

    do js=1,ns; do is=1,ns
       do it=1,nt

          rho_pr(1:nx,1:ny,1:nz) = rho(1:nx,1:ny,1:nz,it,is,js)

          do ismear=1,n_smear

             if ( nprocz > 1 ) then
                mrank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
                prank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)

                tag = nxs*nys
                
                call MPI_SendRecv(rho_pr(1,1,1)         ,nc*nc*nxs*nys,mpi_dc,mrank,tag,&
                     &            rho_pr(1,1,mapz(nz+1)),nc*nc*nxs*nys,mpi_dc,prank,tag,&
                     &            mpi_comm, mpi_status, mpierror)

                call MPI_SendRecv(rho_pr(1,1,nz)        ,nc*nc*nxs*nys,mpi_dc,prank,tag,&
                     &            rho_pr(1,1,mapz(0))   ,nc*nc*nxs*nys,mpi_dc,mrank,tag,&
                     &            mpi_comm, mpi_status, mpierror)
             end if

             phi = zero_matrix

             do mu=1,nd-1
                dmu = 0
                dmu(mu) = 1

                do iz=1,nz
                   lz = mapz(iz + dmu(3))
                   mz = mapz(iz - dmu(3))
                   do iy=1,ny
                      ly = mapy(iy + dmu(2))
                      my = mapy(iy - dmu(2))
                      do ix=1,nx
                         lx = mapx(ix + dmu(1))
                         mx = mapx(ix - dmu(1))

                         call MultiplyMatMat   (phi_px,U_mux   ,rho_xpmu)
                         call MultiplyMatDagMat(phi_mx,U_muxmmu,rho_xmmu)

                         phi_x%cl = phi_x%cl + (phi_px%cl + phi_mx%cl)/u0

                      end do
                   end do
                end do

             end do

             do iz=1,nz; do iy=1,ny; do ix=1,nx
                rho_x%cl = (1.0d0 - alpha_smear)*rho_x%cl + (alpha_smear/6.0d0)*phi_x%cl
             end do; end do; end do

          end do

          rho(1:nx,1:ny,1:nz,it,is,js) = rho_pr(1:nx,1:ny,1:nz)

       end do
    end do; end do

    deallocate(rho_pr)
    deallocate(phi)

  end subroutine SmearSinkOld

end module QuarkPropagator

