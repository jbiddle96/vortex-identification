#include "defines.h"

module FermionTypes

  use Kinds
  use ColourTypes
  use SpinorTypes
  implicit none
  private

  !! Standard (Dirac) fermions.
  type dirac_fermion
     sequence
     complex(wc), dimension(nc,ns) :: cs
  end type dirac_fermion

  !! Chiral (Weyl) fermions.
  type weyl_fermion
     sequence
     complex(wc), dimension(nc,ns_weyl) :: cs
  end type weyl_fermion

  !! Default quark propagator type.
  type colour_spin_matrix
     sequence
     complex(wc), dimension(nc,nc,ns,ns) :: cs
  end type colour_spin_matrix

  type colour_weyl_spin_matrix
     sequence
     complex(wc), dimension(nc,nc,ns_weyl,ns_weyl) :: cs
  end type colour_weyl_spin_matrix
  
  !! Alternative quark propagator type.
  type spin_colour_matrix
     sequence
     complex(wc), dimension(ns,ns,nc,nc) :: sc
  end type spin_colour_matrix

  ! *To Do*: Decide on a type for propagators.
  type(dirac_fermion), parameter :: zero_dirac_fermion = dirac_fermion( reshape( (/ 0,0,0,0,0,0,0,0,0,0,0,0 /), (/ 3,4 /) ) )
  type(colour_spin_matrix), parameter :: zero_colour_spin_matrix = colour_spin_matrix( 0 )
  type(spin_colour_matrix), parameter :: zero_spin_colour_matrix = spin_colour_matrix( 0 )
  type(colour_weyl_spin_matrix), parameter :: zero_colour_weyl_spin_matrix = colour_weyl_spin_matrix( 0 )

  public :: dirac_fermion
  public :: weyl_fermion
  public :: colour_spin_matrix
  public :: spin_colour_matrix
  public :: colour_weyl_spin_matrix
  public :: random_dirac_fermion
  public :: random_weyl_fermion
  public :: random_colour_spin_matrix
  public :: random_spin_colour_matrix

  public :: zero_dirac_fermion, zero_colour_spin_matrix, zero_spin_colour_matrix, zero_colour_weyl_spin_matrix
  
  
contains

  subroutine random_dirac_fermion(z)
    
    type(dirac_fermion), intent(out) :: z
    real(wp) :: zr(nc,ns), zi(nc,ns)
    
    call random_number(zr)
    call random_number(zi)

    z%cs = cmplx(zr,zi,wc)

  end subroutine random_dirac_fermion

  subroutine random_weyl_fermion(z)
    
    type(weyl_fermion), intent(out) :: z
    real(dp) :: zr(nc,ns_weyl), zi(nc,ns_weyl)
    
    call random_number(zr)
    call random_number(zi)

    z%cs = cmplx(zr,zi,dc)

  end subroutine random_weyl_fermion

  subroutine random_colour_spin_matrix(z)
    
    type(colour_spin_matrix), intent(out) :: z
    real(dp) :: zr(nc,nc,ns,ns), zi(nc,nc,ns,ns)
    
    call random_number(zr)
    call random_number(zi)

    z%cs = cmplx(zr,zi,dc)

  end subroutine random_colour_spin_matrix

  subroutine random_spin_colour_matrix(z)
    
    type(spin_colour_matrix), intent(out) :: z
    real(dp) :: zr(ns,ns,nc,nc), zi(ns,ns,nc,nc)
    
    call random_number(zr)
    call random_number(zi)

    z%sc = cmplx(zr,zi,dc)

  end subroutine random_spin_colour_matrix

end module FermionTypes
