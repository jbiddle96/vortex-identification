module WilsonMatrix_fx

  use Timer
  use MPIInterface
  use FermionField
  use FermionFieldMPIComms
  use FermionAlgebra



  use SpinorTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  use ColourFieldOps
  implicit none
  private

  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:,:,:), private, allocatable :: Up_xd
  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), public :: kappa_Wilson



  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean

  real(dp), public :: bcx_Wilson = 1.0d0, bcy_Wilson = 1.0d0, bcz_Wilson = 1.0d0, bct_Wilson = 1.0d0
  public :: DWilson_fx

  public :: DWilson
  public :: DWilson_dag
  public :: HsqWilson
  public :: HWilson
  public :: InitialiseWilsonOperator
  public :: FinaliseWilsonOperator
contains
  subroutine InitialiseWilsonOperator(U_xd , kappa, u0)
    ! begin args: U_xd , kappa, u0

    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: kappa
    real(dp), optional :: u0

    ! begin local_vars
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfic_sw, mfir, mfir_sm !coefficents to be absorbed.







    !EO preconditioning is in the hopping parameter formalism.
    ! begin execution
    if ( .not. allocated(Up_xd) ) call CreateGaugeField(Up_xd,1)




    mfir = kappa
    if ( present(u0) ) mfir = mfir/u0




    kappa_Wilson = kappa

    !Absorb the mean field improvement into the gauge fields.






    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx







          Up_xd(ix,iy,iz,it,mu)%Cl = mfir*U_xd(ix,iy,iz,it,mu)%Cl

       end do; end do; end do; end do
    end do

    call SetBoundaryConditions(Up_xd,bcx_Wilson, bcy_Wilson, bcz_Wilson, bct_Wilson)
    call ShadowGaugeField(Up_xd,0)
  end subroutine InitialiseWilsonOperator

  subroutine FinaliseWilsonOperator

    if ( allocated(Up_xd) ) call DestroyGaugeField(Up_xd)
  end subroutine FinaliseWilsonOperator
  subroutine HsqWilson(phi,Hsqphi)
    ! begin args: phi, Hsqphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi, Hsqphi

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Dphi

    ! begin execution
    allocate(Dphi(nxp,nyp,nzp,ntp,ns))

    call HWilson(phi,Dphi)
    call HWilson(Dphi,Hsqphi)

    deallocate(Dphi)

  end subroutine HsqWilson

  subroutine HWilson(phi,Hphi)
    ! begin args: phi, Hphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi, Hphi
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    call DWilson(phi,Hphi)


    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       Hphi(ix,iy,iz,it,3)%Cl = -Hphi(ix,iy,iz,it,3)%Cl
       Hphi(ix,iy,iz,it,4)%Cl = -Hphi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do
  end subroutine HWilson

  subroutine DWilson_dag(phi, Dphi)
    ! begin args: phi, Dphi, op_parity

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi



    ! begin local_vars
    integer :: ix,iy,iz,it


    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       phi(ix,iy,iz,it,3)%Cl = -phi(ix,iy,iz,it,3)%Cl
       phi(ix,iy,iz,it,4)%Cl = -phi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do
    call DWilson(phi,Dphi)



    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       phi(ix,iy,iz,it,3)%Cl = -phi(ix,iy,iz,it,3)%Cl
       phi(ix,iy,iz,it,4)%Cl = -phi(ix,iy,iz,it,4)%Cl

       Dphi(ix,iy,iz,it,3)%Cl = -Dphi(ix,iy,iz,it,3)%Cl
       Dphi(ix,iy,iz,it,4)%Cl = -Dphi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do
  end subroutine DWilson_dag

  subroutine DWilson(phi, Dphi)
    ! begin args: phi, Dphi, op_parity

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi



    ! op_parity = 0 => takes odd sites to even sites.
    ! op_parity = 1 => takes even sites to odd sites.

    ! begin local_vars
    integer :: ix,iy,iz,it,is
    integer :: jx,jy,jz,jt,js
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

    if (timing) intime = mpi_wtime()
    commtime = 0.0d0
    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0


    call ShadowFermionField(phi,1)




    Dphi = phi
    !mu = 1




    !G_1^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; ix=nx; do jx=1,nx





             Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = Up_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Up_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Up_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)




          ix = jx
       end do; end do; end do; end do
    end do
    !mu = 2




    !G_2^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; iy=ny; do jy=1,ny; do ix=1,nx





             Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) + pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) + pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) + pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

             psi_x%Cl(1) = Up_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Up_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Up_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*psi_x%Cl(3)




       end do; iy=jy; end do; end do; end do
    end do
    !mu = 3




    !G_3^- Up phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; do iz=1,nz; jz=mapz(iz+1); do iy=1,ny; do ix=1,nx







             Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = Up_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Up_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Up_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)




       end do; end do; end do; end do
    end do
    !mu = 4




    !G_4^- Up phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jt=mapt(it+1); do iz=1,nz; do iy=1,ny; do ix=1,nx
             Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) - phi(ix,iy,iz,jt,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) - phi(ix,iy,iz,jt,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) - phi(ix,iy,iz,jt,js)%Cl(3)
             psi_x%Cl(1) = Up_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
             psi_x%Cl(2) = Up_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
             psi_x%Cl(3) = Up_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Up_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)


             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + psi_x%Cl(3)





       end do; end do; end do; end do
    end do
    !mu = 1




    !G_1^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jx=nx; do ix=1,nx






             Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)




          jx = ix

       end do; end do; end do; end do
    end do
    !mu = 2




    !G_2^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; jy=ny; do iy=1,ny; do ix=1,nx






             Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) - pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) - pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) - pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

             psi_x%Cl(1) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*psi_x%Cl(3)




       end do; jy=iy; end do; end do; end do
    end do
    !mu = 3




    !G_3^+ Up phi_xmmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jz=nz; do iz=1,nz; do iy=1,ny; do ix=1,nx







             Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
             Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
             Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

             psi_x%Cl(1) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)




       end do; end do; jz=iz; end do; end do
    end do
    !mu = 4




    !G_4^+ Up phi_xmmu

    do is=1,nsp
       js = is+2



       jt=nt

       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx







             Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) + phi(ix,iy,iz,jt,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) + phi(ix,iy,iz,jt,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) + phi(ix,iy,iz,jt,js)%Cl(3)
             psi_x%Cl(1) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
             psi_x%Cl(2) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
             psi_x%Cl(3) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)


             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - psi_x%Cl(3)





       end do; end do; end do; jt=it; end do
    end do
    if (timing) then
       outtime = mpi_wtime()
       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    end if

    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
    waste = commtime/(outtime - intime)
    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)



  end subroutine DWilson

  subroutine DWilson_fx(phi, Dphi)
    ! begin args: phi, Dphi, op_parity

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi



    ! op_parity = 0 => takes odd sites to even sites.
    ! op_parity = 1 => takes even sites to odd sites.

    ! begin local_vars
    integer :: ix,iy,iz,it,is
    integer :: jx,jy,jz,jt,js
    type(colour_vector) :: Gammaphi, psi_x
    integer :: site_parity, p_xyzt

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

    !if (timing) intime = mpi_wtime()
    !commtime = 0.0d0
    !pm(1) = 1.0d0
    !pm(2) = -1.0d0
    !pm(3) = 1.0d0
    !pm(4) = -1.0d0

    call ShadowFermionField(phi,1)

    Dphi = phi
    !mu = 1

    !G_1^- Up phi_xpmu

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

       !is = 1
       !js = 4
       jx = mapx(ix+1)

       Gammaphi%Cl(1) = phi(jx,iy,iz,it,1)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(1)),real(phi(jx,iy,iz,it,4)%Cl(1)),dc)
       Gammaphi%Cl(2) = phi(jx,iy,iz,it,1)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(2)),real(phi(jx,iy,iz,it,4)%Cl(2)),dc)
       Gammaphi%Cl(3) = phi(jx,iy,iz,it,1)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(3)),real(phi(jx,iy,iz,it,4)%Cl(3)),dc)

       psi_x%Cl(1) = Up_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,1)%Cl(1) = Dphi(ix,iy,iz,it,1)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,1)%Cl(2) = Dphi(ix,iy,iz,it,1)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,1)%Cl(3) = Dphi(ix,iy,iz,it,1)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,4)%Cl(1) = Dphi(ix,iy,iz,it,4)%Cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(ix,iy,iz,it,4)%Cl(2) = Dphi(ix,iy,iz,it,4)%Cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(ix,iy,iz,it,4)%Cl(3) = Dphi(ix,iy,iz,it,4)%Cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !is = 2
       !js = 3

       Gammaphi%Cl(1) = phi(jx,iy,iz,it,2)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(1)),real(phi(jx,iy,iz,it,3)%Cl(1)),dc)
       Gammaphi%Cl(2) = phi(jx,iy,iz,it,2)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(2)),real(phi(jx,iy,iz,it,3)%Cl(2)),dc)
       Gammaphi%Cl(3) = phi(jx,iy,iz,it,2)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(3)),real(phi(jx,iy,iz,it,3)%Cl(3)),dc)

       psi_x%Cl(1) = Up_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,2)%Cl(1) = Dphi(ix,iy,iz,it,2)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,2)%Cl(2) = Dphi(ix,iy,iz,it,2)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,2)%Cl(3) = Dphi(ix,iy,iz,it,2)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,3)%Cl(1) = Dphi(ix,iy,iz,it,3)%Cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(ix,iy,iz,it,3)%Cl(2) = Dphi(ix,iy,iz,it,3)%Cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(ix,iy,iz,it,3)%Cl(3) = Dphi(ix,iy,iz,it,3)%Cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !mu = 2




       !G_2^- Up phi_xpmu


       !is = 1
       !js = 4
       jy = mapy(iy+1)

       Gammaphi%Cl(1) = phi(ix,jy,iz,it,1)%Cl(1) + phi(ix,jy,iz,it,4)%Cl(1)
       Gammaphi%Cl(2) = phi(ix,jy,iz,it,1)%Cl(2) + phi(ix,jy,iz,it,4)%Cl(2)
       Gammaphi%Cl(3) = phi(ix,jy,iz,it,1)%Cl(3) + phi(ix,jy,iz,it,4)%Cl(3)

       psi_x%Cl(1) = Up_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,1)%Cl(1) = Dphi(ix,iy,iz,it,1)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,1)%Cl(2) = Dphi(ix,iy,iz,it,1)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,1)%Cl(3) = Dphi(ix,iy,iz,it,1)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,4)%Cl(1) = Dphi(ix,iy,iz,it,4)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,4)%Cl(2) = Dphi(ix,iy,iz,it,4)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,4)%Cl(3) = Dphi(ix,iy,iz,it,4)%Cl(3) - psi_x%Cl(3)

       !is = 2
       !js = 3

       Gammaphi%Cl(1) = phi(ix,jy,iz,it,2)%Cl(1) - phi(ix,jy,iz,it,3)%Cl(1)
       Gammaphi%Cl(2) = phi(ix,jy,iz,it,2)%Cl(2) - phi(ix,jy,iz,it,3)%Cl(2)
       Gammaphi%Cl(3) = phi(ix,jy,iz,it,2)%Cl(3) - phi(ix,jy,iz,it,3)%Cl(3)

       psi_x%Cl(1) = Up_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,2)%Cl(1) = Dphi(ix,iy,iz,it,2)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,2)%Cl(2) = Dphi(ix,iy,iz,it,2)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,2)%Cl(3) = Dphi(ix,iy,iz,it,2)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,3)%Cl(1) = Dphi(ix,iy,iz,it,3)%Cl(1) + psi_x%Cl(1)
       Dphi(ix,iy,iz,it,3)%Cl(2) = Dphi(ix,iy,iz,it,3)%Cl(2) + psi_x%Cl(2)
       Dphi(ix,iy,iz,it,3)%Cl(3) = Dphi(ix,iy,iz,it,3)%Cl(3) + psi_x%Cl(3)

       !mu = 3




       !G_3^- Up phi_xpmu

       !is = 1
       !js = 3
       jz = mapz(iz+1)

       Gammaphi%Cl(1) = phi(ix,iy,jz,it,1)%Cl(1) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(1)),real(phi(ix,iy,jz,it,3)%Cl(1)),dc)
       Gammaphi%Cl(2) = phi(ix,iy,jz,it,1)%Cl(2) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(2)),real(phi(ix,iy,jz,it,3)%Cl(2)),dc)
       Gammaphi%Cl(3) = phi(ix,iy,jz,it,1)%Cl(3) + cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(3)),real(phi(ix,iy,jz,it,3)%Cl(3)),dc)

       psi_x%Cl(1) = Up_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,1)%Cl(1) = Dphi(ix,iy,iz,it,1)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,1)%Cl(2) = Dphi(ix,iy,iz,it,1)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,1)%Cl(3) = Dphi(ix,iy,iz,it,1)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,3)%Cl(1) = Dphi(ix,iy,iz,it,3)%Cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(ix,iy,iz,it,3)%Cl(2) = Dphi(ix,iy,iz,it,3)%Cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(ix,iy,iz,it,3)%Cl(3) = Dphi(ix,iy,iz,it,3)%Cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !is = 2
       !js = 4

       Gammaphi%Cl(1) = phi(ix,iy,jz,it,2)%Cl(1) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(1)),real(phi(ix,iy,jz,it,4)%Cl(1)),dc)
       Gammaphi%Cl(2) = phi(ix,iy,jz,it,2)%Cl(2) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(2)),real(phi(ix,iy,jz,it,4)%Cl(2)),dc)
       Gammaphi%Cl(3) = phi(ix,iy,jz,it,2)%Cl(3) - cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(3)),real(phi(ix,iy,jz,it,4)%Cl(3)),dc)

       psi_x%Cl(1) = Up_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,2)%Cl(1) = Dphi(ix,iy,iz,it,2)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,2)%Cl(2) = Dphi(ix,iy,iz,it,2)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,2)%Cl(3) = Dphi(ix,iy,iz,it,2)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,4)%Cl(1) = Dphi(ix,iy,iz,it,4)%Cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(ix,iy,iz,it,4)%Cl(2) = Dphi(ix,iy,iz,it,4)%Cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(ix,iy,iz,it,4)%Cl(3) = Dphi(ix,iy,iz,it,4)%Cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !mu = 4




       !G_4^- Up phi_xpmu

       !is = 1
       !js = 3
       jt = mapt(it+1)

       Gammaphi%Cl(1) = phi(ix,iy,iz,jt,1)%Cl(1) - phi(ix,iy,iz,jt,3)%Cl(1)
       Gammaphi%Cl(2) = phi(ix,iy,iz,jt,1)%Cl(2) - phi(ix,iy,iz,jt,3)%Cl(2)
       Gammaphi%Cl(3) = phi(ix,iy,iz,jt,1)%Cl(3) - phi(ix,iy,iz,jt,3)%Cl(3)
       psi_x%Cl(1) = Up_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,1)%Cl(1) = Dphi(ix,iy,iz,it,1)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,1)%Cl(2) = Dphi(ix,iy,iz,it,1)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,1)%Cl(3) = Dphi(ix,iy,iz,it,1)%Cl(3) - psi_x%Cl(3)


       Dphi(ix,iy,iz,it,3)%Cl(1) = Dphi(ix,iy,iz,it,3)%Cl(1) + psi_x%Cl(1)
       Dphi(ix,iy,iz,it,3)%Cl(2) = Dphi(ix,iy,iz,it,3)%Cl(2) + psi_x%Cl(2)
       Dphi(ix,iy,iz,it,3)%Cl(3) = Dphi(ix,iy,iz,it,3)%Cl(3) + psi_x%Cl(3)


       !is = 2
       !js = 4

       Gammaphi%Cl(1) = phi(ix,iy,iz,jt,2)%Cl(1) - phi(ix,iy,iz,jt,4)%Cl(1)
       Gammaphi%Cl(2) = phi(ix,iy,iz,jt,2)%Cl(2) - phi(ix,iy,iz,jt,4)%Cl(2)
       Gammaphi%Cl(3) = phi(ix,iy,iz,jt,2)%Cl(3) - phi(ix,iy,iz,jt,4)%Cl(3)
       psi_x%Cl(1) = Up_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
       psi_x%Cl(2) = Up_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
       psi_x%Cl(3) = Up_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
            & Up_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,2)%Cl(1) = Dphi(ix,iy,iz,it,2)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,2)%Cl(2) = Dphi(ix,iy,iz,it,2)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,2)%Cl(3) = Dphi(ix,iy,iz,it,2)%Cl(3) - psi_x%Cl(3)


       Dphi(ix,iy,iz,it,4)%Cl(1) = Dphi(ix,iy,iz,it,4)%Cl(1) + psi_x%Cl(1)
       Dphi(ix,iy,iz,it,4)%Cl(2) = Dphi(ix,iy,iz,it,4)%Cl(2) + psi_x%Cl(2)
       Dphi(ix,iy,iz,it,4)%Cl(3) = Dphi(ix,iy,iz,it,4)%Cl(3) + psi_x%Cl(3)

       !mu = 1

       !G_1^+ Up phi_xmmu

       !is = 1
       !js = 4
       jx = mapx(ix-1)

       Gammaphi%Cl(1) = phi(jx,iy,iz,it,1)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(1)),real(phi(jx,iy,iz,it,4)%Cl(1)),dc)
       Gammaphi%Cl(2) = phi(jx,iy,iz,it,1)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(2)),real(phi(jx,iy,iz,it,4)%Cl(2)),dc)
       Gammaphi%Cl(3) = phi(jx,iy,iz,it,1)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,4)%Cl(3)),real(phi(jx,iy,iz,it,4)%Cl(3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,1)%Cl(1) = Dphi(ix,iy,iz,it,1)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,1)%Cl(2) = Dphi(ix,iy,iz,it,1)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,1)%Cl(3) = Dphi(ix,iy,iz,it,1)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,4)%Cl(1) = Dphi(ix,iy,iz,it,4)%Cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(ix,iy,iz,it,4)%Cl(2) = Dphi(ix,iy,iz,it,4)%Cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(ix,iy,iz,it,4)%Cl(3) = Dphi(ix,iy,iz,it,4)%Cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !is = 2
       !js = 3

       Gammaphi%Cl(1) = phi(jx,iy,iz,it,2)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(1)),real(phi(jx,iy,iz,it,3)%Cl(1)),dc)
       Gammaphi%Cl(2) = phi(jx,iy,iz,it,2)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(2)),real(phi(jx,iy,iz,it,3)%Cl(2)),dc)
       Gammaphi%Cl(3) = phi(jx,iy,iz,it,2)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,3)%Cl(3)),real(phi(jx,iy,iz,it,3)%Cl(3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,2)%Cl(1) = Dphi(ix,iy,iz,it,2)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,2)%Cl(2) = Dphi(ix,iy,iz,it,2)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,2)%Cl(3) = Dphi(ix,iy,iz,it,2)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,3)%Cl(1) = Dphi(ix,iy,iz,it,3)%Cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(ix,iy,iz,it,3)%Cl(2) = Dphi(ix,iy,iz,it,3)%Cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(ix,iy,iz,it,3)%Cl(3) = Dphi(ix,iy,iz,it,3)%Cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)


       !mu = 2




       !G_2^+ Up phi_xmmu

       !is = 1
       !js = 4
       jy = mapy(iy-1)

       Gammaphi%Cl(1) = phi(ix,jy,iz,it,1)%Cl(1) - phi(ix,jy,iz,it,4)%Cl(1)
       Gammaphi%Cl(2) = phi(ix,jy,iz,it,1)%Cl(2) - phi(ix,jy,iz,it,4)%Cl(2)
       Gammaphi%Cl(3) = phi(ix,jy,iz,it,1)%Cl(3) - phi(ix,jy,iz,it,4)%Cl(3)

       psi_x%Cl(1) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,1)%Cl(1) = Dphi(ix,iy,iz,it,1)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,1)%Cl(2) = Dphi(ix,iy,iz,it,1)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,1)%Cl(3) = Dphi(ix,iy,iz,it,1)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,4)%Cl(1) = Dphi(ix,iy,iz,it,4)%Cl(1) + psi_x%Cl(1)
       Dphi(ix,iy,iz,it,4)%Cl(2) = Dphi(ix,iy,iz,it,4)%Cl(2) + psi_x%Cl(2)
       Dphi(ix,iy,iz,it,4)%Cl(3) = Dphi(ix,iy,iz,it,4)%Cl(3) + psi_x%Cl(3)

       !is = 2
       !js = 3

       Gammaphi%Cl(1) = phi(ix,jy,iz,it,2)%Cl(1) + phi(ix,jy,iz,it,3)%Cl(1)
       Gammaphi%Cl(2) = phi(ix,jy,iz,it,2)%Cl(2) + phi(ix,jy,iz,it,3)%Cl(2)
       Gammaphi%Cl(3) = phi(ix,jy,iz,it,2)%Cl(3) + phi(ix,jy,iz,it,3)%Cl(3)

       psi_x%Cl(1) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,2)%Cl(1) = Dphi(ix,iy,iz,it,2)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,2)%Cl(2) = Dphi(ix,iy,iz,it,2)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,2)%Cl(3) = Dphi(ix,iy,iz,it,2)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,3)%Cl(1) = Dphi(ix,iy,iz,it,3)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,3)%Cl(2) = Dphi(ix,iy,iz,it,3)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,3)%Cl(3) = Dphi(ix,iy,iz,it,3)%Cl(3) - psi_x%Cl(3)

       !mu = 3

       !G_3^+ Up phi_xmmu


       !is = 1
       !js = 3
       jz = mapz(iz-1)

       Gammaphi%Cl(1) = phi(ix,iy,jz,it,1)%Cl(1) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(1)),real(phi(ix,iy,jz,it,3)%Cl(1)),dc)
       Gammaphi%Cl(2) = phi(ix,iy,jz,it,1)%Cl(2) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(2)),real(phi(ix,iy,jz,it,3)%Cl(2)),dc)
       Gammaphi%Cl(3) = phi(ix,iy,jz,it,1)%Cl(3) - cmplx(-aimag(phi(ix,iy,jz,it,3)%Cl(3)),real(phi(ix,iy,jz,it,3)%Cl(3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,1)%Cl(1) = Dphi(ix,iy,iz,it,1)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,1)%Cl(2) = Dphi(ix,iy,iz,it,1)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,1)%Cl(3) = Dphi(ix,iy,iz,it,1)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,3)%Cl(1) = Dphi(ix,iy,iz,it,3)%Cl(1) - cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(ix,iy,iz,it,3)%Cl(2) = Dphi(ix,iy,iz,it,3)%Cl(2) - cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(ix,iy,iz,it,3)%Cl(3) = Dphi(ix,iy,iz,it,3)%Cl(3) - cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)

       !is = 2
       !js = 4

       Gammaphi%Cl(1) = phi(ix,iy,jz,it,2)%Cl(1) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(1)),real(phi(ix,iy,jz,it,4)%Cl(1)),dc)
       Gammaphi%Cl(2) = phi(ix,iy,jz,it,2)%Cl(2) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(2)),real(phi(ix,iy,jz,it,4)%Cl(2)),dc)
       Gammaphi%Cl(3) = phi(ix,iy,jz,it,2)%Cl(3) + cmplx(-aimag(phi(ix,iy,jz,it,4)%Cl(3)),real(phi(ix,iy,jz,it,4)%Cl(3)),dc)

       psi_x%Cl(1) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,2)%Cl(1) = Dphi(ix,iy,iz,it,2)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,2)%Cl(2) = Dphi(ix,iy,iz,it,2)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,2)%Cl(3) = Dphi(ix,iy,iz,it,2)%Cl(3) - psi_x%Cl(3)

       Dphi(ix,iy,iz,it,4)%Cl(1) = Dphi(ix,iy,iz,it,4)%Cl(1) + cmplx(-aimag(psi_x%Cl(1)),real(psi_x%Cl(1)),dc)
       Dphi(ix,iy,iz,it,4)%Cl(2) = Dphi(ix,iy,iz,it,4)%Cl(2) + cmplx(-aimag(psi_x%Cl(2)),real(psi_x%Cl(2)),dc)
       Dphi(ix,iy,iz,it,4)%Cl(3) = Dphi(ix,iy,iz,it,4)%Cl(3) + cmplx(-aimag(psi_x%Cl(3)),real(psi_x%Cl(3)),dc)




       !mu = 4




       !G_4^+ Up phi_xmmu

       !is = 1
       !js = 3
       jt = mapt(it-1)

       Gammaphi%Cl(1) = phi(ix,iy,iz,jt,1)%Cl(1) + phi(ix,iy,iz,jt,3)%Cl(1)
       Gammaphi%Cl(2) = phi(ix,iy,iz,jt,1)%Cl(2) + phi(ix,iy,iz,jt,3)%Cl(2)
       Gammaphi%Cl(3) = phi(ix,iy,iz,jt,1)%Cl(3) + phi(ix,iy,iz,jt,3)%Cl(3)
       psi_x%Cl(1) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,1)%Cl(1) = Dphi(ix,iy,iz,it,1)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,1)%Cl(2) = Dphi(ix,iy,iz,it,1)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,1)%Cl(3) = Dphi(ix,iy,iz,it,1)%Cl(3) - psi_x%Cl(3)


       Dphi(ix,iy,iz,it,3)%Cl(1) = Dphi(ix,iy,iz,it,3)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,3)%Cl(2) = Dphi(ix,iy,iz,it,3)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,3)%Cl(3) = Dphi(ix,iy,iz,it,3)%Cl(3) - psi_x%Cl(3)


       !is = 2
       !js = 4

       Gammaphi%Cl(1) = phi(ix,iy,iz,jt,2)%Cl(1) + phi(ix,iy,iz,jt,4)%Cl(1)
       Gammaphi%Cl(2) = phi(ix,iy,iz,jt,2)%Cl(2) + phi(ix,iy,iz,jt,4)%Cl(2)
       Gammaphi%Cl(3) = phi(ix,iy,iz,jt,2)%Cl(3) + phi(ix,iy,iz,jt,4)%Cl(3)
       psi_x%Cl(1) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
       psi_x%Cl(2) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
       psi_x%Cl(3) = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
            & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

       Dphi(ix,iy,iz,it,2)%Cl(1) = Dphi(ix,iy,iz,it,2)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,2)%Cl(2) = Dphi(ix,iy,iz,it,2)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,2)%Cl(3) = Dphi(ix,iy,iz,it,2)%Cl(3) - psi_x%Cl(3)


       Dphi(ix,iy,iz,it,4)%Cl(1) = Dphi(ix,iy,iz,it,4)%Cl(1) - psi_x%Cl(1)
       Dphi(ix,iy,iz,it,4)%Cl(2) = Dphi(ix,iy,iz,it,4)%Cl(2) - psi_x%Cl(2)
       Dphi(ix,iy,iz,it,4)%Cl(3) = Dphi(ix,iy,iz,it,4)%Cl(3) - psi_x%Cl(3)



    end do; end do; end do; end do

    !if (timing) then
    !   outtime = mpi_wtime()
    !   call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    !end if

    !call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
    !waste = commtime/(outtime - intime)
    !call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)

  end subroutine DWilson_fx

end module WilsonMatrix_fx
