!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

#include "defines.h"

module GaugeFieldMPIComms

  use LatticeSize
  use MPIInterface
  use ColourTypes
  use Timer
  implicit none
  private

  !TODO: Decide on publicity of timing vars...
  integer, public  :: nshdwgf = 0
  real(dp), public :: maxt_shdwgf, mint_shdwgf, t_shdwgf

  public :: ShadowSU3Field
  public :: ShadowGaugeField
  public :: CShiftGaugePlane

  interface ShadowGaugeField
     module procedure ShadowGaugeField_4x
     module procedure ShadowGaugeField_eo
  end interface

contains

  subroutine ShadowSU3Field(U_x,ishdw)

    type(colour_matrix), dimension(:,:,:,:) :: U_x
    integer :: ishdw
    integer :: tag, dest_rank, src_rank
    integer :: it, nxd,nyd,nzd,ntd ! d = deferred shape array
    integer :: sendrecv_reqz(nt,2),sendrecv_reqt(2), ierr
    integer :: sendrecvz_status(nmpi_status,nt*2), sendrecvt_status(nmpi_status,2)

    nxd = size(U_x,1)
    nyd = size(U_x,2)
    nzd = size(U_x,3)
    ntd = size(U_x,4)

    select case(ishdw)
    case(1)

       if ( nprocz > 1 ) then
          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(nz)/nz), nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(nz)/nz), nprocz)

          do it=1,nt
             call MPI_ISSend(U_x(1,1,mapz(1)   ,it), nc*nc*nxd*nyd,mpi_dc,dest_rank,it,&
                  &                                      mpi_comm,sendrecv_reqz(it,1),ierr)
             call MPI_IRecv (U_x(1,1,mapz(nz+1),it), nc*nc*nxd*nyd, mpi_dc,  src_rank, it, &
                  &                                      mpi_comm,sendrecv_reqz(it,2),ierr)
          end do

          call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)

          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(-1)/nz), nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(-1)/nz), nprocz)

          do it=1,nt
             call MPI_ISSend(U_x(1,1,mapz(nz),it), nc*nc*nxd*nyd, mpi_dc, dest_rank, it, &
                  &                                    mpi_comm, sendrecv_reqz(it,1), ierr)
             call MPI_IRecv (U_x(1,1,mapz(0) ,it), nc*nc*nxd*nyd, mpi_dc,  src_rank, it, &
                  &                                    mpi_comm, sendrecv_reqz(it,2), ierr)
          end do

          call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
       end if

       if ( nproct > 1 ) then
          dest_rank = modulo(mpi_coords(4) - floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)

          call MPI_ISSend(U_x(1,1,1,mapt(1)   ), nc*nc*nxd*nyd*nzs, mpi_dc, dest_rank, 1, &
               &                                     mpi_comm, sendrecv_reqt(1), ierr)
          call MPI_IRecv (U_x(1,1,1,mapt(nt+1)), nc*nc*nxd*nyd*nzs, mpi_dc,  src_rank, 1, &
               &                                     mpi_comm, sendrecv_reqt(2), ierr)

          call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)

          dest_rank = modulo(mpi_coords(4) - floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)

          call MPI_ISSend(U_x(1,1,1,mapt(nt)), nc*nc*nxd*nyd*nzs, mpi_dc, dest_rank, 1, &
               &                                   mpi_comm, sendrecv_reqt(1), ierr)
          call MPI_IRecv (U_x(1,1,1,mapt(0) ), nc*nc*nxd*nyd*nzs, mpi_dc,  src_rank, 1, &
               &                                   mpi_comm, sendrecv_reqt(2), ierr)

          call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
       end if
    end select

  end subroutine ShadowSU3Field

  subroutine ShadowGaugeField_4x(U_xd,ishdw)

    integer :: ishdw
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: ierror, tag, dest_rank, src_rank
    integer :: status(nmpi_status,4)
    integer :: id, it, nxd,nyd,nzd,ntd,ndd ! d = deferred shape array
    integer :: sendrecv_reqz(nt,2),sendrecv_reqt(2), sendrecv_reqzs(nts,2), ierr
    integer :: sendrecvz_status(nmpi_status,nt*2), sendrecvt_status(nmpi_status,2), sendrecvzs_status(nmpi_status,nts*2)
    real(dp) :: t_in, t_out

    t_in = mpi_wtime()

    nxd = size(U_xd,1)
    nyd = size(U_xd,2)
    nzd = size(U_xd,3)
    ntd = size(U_xd,4)
    ndd = size(U_xd,5)

    select case(ishdw)
    case(0)

       if ( nprocz > 1 ) then
          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(-1)/nz), nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(-1)/nz), nprocz)

          do id=1,ndd
             do it=1,nt
                call MPI_ISSend(U_xd(1,1,nz  ,it,id), nc*nc*nxd*nyd, mpi_dc, dest_rank, it+ndd*id, mpi_comm, &
                     & sendrecv_reqz(it,1), ierr)
                call MPI_IRecv (U_xd(1,1,nz+1,it,id), nc*nc*nxd*nyd, mpi_dc,  src_rank, it+ndd*id, mpi_comm, &
                     & sendrecv_reqz(it,2), ierr)
             end do
             call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
          end do


       end if


       if ( nproct > 1 ) then

          dest_rank = modulo(mpi_coords(4) - floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)

          do id=1,ndd

             call MPI_ISSend(U_xd(1,1,1,nt  ,id), nc*nc*nxd*nyd*nzd, mpi_dc, dest_rank, id, mpi_comm, sendrecv_reqt(1), ierr)
             call MPI_IRecv (U_xd(1,1,1,nt+1,id), nc*nc*nxd*nyd*nzd, mpi_dc,  src_rank, id, mpi_comm, sendrecv_reqt(2), ierr)
             call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)

          end do


       end if

    case(1)

       if ( nprocz > 1 ) then

          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(nz)/nz), nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(nz)/nz), nprocz)

          do id=1,ndd
             do it=1,nt
                call MPI_ISSend(U_xd(1,1,mapz(1)   ,it,id), nc*nc*nxd*nyd,mpi_dc,dest_rank,it+nd*id,&
                     &                                      mpi_comm,sendrecv_reqz(it,1),ierr)
                call MPI_IRecv (U_xd(1,1,mapz(nz+1),it,id), nc*nc*nxd*nyd, mpi_dc,  src_rank, it+nd*id, &
                     &                                      mpi_comm,sendrecv_reqz(it,2),ierr)
             end do
             call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
          end do

          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(-1)/nz), nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(-1)/nz), nprocz)

          do id=1,ndd
             do it=1,nt
                call MPI_ISSend(U_xd(1,1,mapz(nz),it,id), nc*nc*nxd*nyd, mpi_dc, dest_rank, it+ndd*id, &
                     &                                    mpi_comm, sendrecv_reqz(it,1), ierr)
                call MPI_IRecv (U_xd(1,1,mapz(0) ,it,id), nc*nc*nxd*nyd, mpi_dc,  src_rank, it+ndd*id, &
                     &                                    mpi_comm, sendrecv_reqz(it,2), ierr)
             end do
             call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
          end do


       end if

       if ( nproct > 1 ) then

          dest_rank = modulo(mpi_coords(4) - floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)

          do id=1,ndd
             call MPI_ISSend(U_xd(1,1,1,mapt(1)   ,id), nc*nc*nxd*nyd*nzs, mpi_dc, dest_rank, id, &
                  &                                     mpi_comm, sendrecv_reqt(1), ierr)
             call MPI_IRecv (U_xd(1,1,1,mapt(nt+1),id), nc*nc*nxd*nyd*nzs, mpi_dc,  src_rank, id, &
                  &                                     mpi_comm, sendrecv_reqt(2), ierr)
             call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
          end do

          dest_rank = modulo(mpi_coords(4) - floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)

          do id=1,ndd
             call MPI_ISSend(U_xd(1,1,1,mapt(nt),id), nc*nc*nxd*nyd*nzs, mpi_dc, dest_rank, id, &
                  &                                   mpi_comm, sendrecv_reqt(1), ierr)
             call MPI_IRecv (U_xd(1,1,1,mapt(0) ,id), nc*nc*nxd*nyd*nzs, mpi_dc,  src_rank, id, &
                  &                                   mpi_comm, sendrecv_reqt(2), ierr)
             call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
          end do

       end if

    case(2)

       if ( nprocz > 1 ) then

          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(nz)/nz), nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(nz)/nz), nprocz)

          do id=1,ndd
             do it=1,nts
                call MPI_ISSend(U_xd(1,1,mapz(2)   ,it,id), nc*nc*nxd*nyd,mpi_dc,dest_rank,it+ndd*id,&
                     &                                      mpi_comm,sendrecv_reqzs(it,1),ierr)
                call MPI_IRecv (U_xd(1,1,mapz(nz+2),it,id), nc*nc*nxd*nyd, mpi_dc,  src_rank, it+nd*id, &
                     &                                      mpi_comm,sendrecv_reqzs(it,2),ierr)
             end do
             call MPI_WaitAll(nts*2,sendrecv_reqzs,sendrecvzs_status,ierr)
          end do

          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(-1)/nz), nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(-1)/nz), nprocz)

          do id=1,ndd
             do it=1,nts
                call MPI_ISSend(U_xd(1,1,mapz(nz-1),it,id), nc*nc*nxd*nyd, mpi_dc, dest_rank, it+ndd*id, &
                     &                                      mpi_comm, sendrecv_reqzs(it,1), ierr)
                call MPI_IRecv (U_xd(1,1,mapz(-1)  ,it,id), nc*nc*nxd*nyd, mpi_dc,  src_rank, it+ndd*id, &
                     &                                      mpi_comm, sendrecv_reqzs(it,2), ierr)
             end do
             call MPI_WaitAll(nts*2,sendrecv_reqzs,sendrecvzs_status,ierr)
          end do

       end if

       if ( nproct > 1 ) then

          dest_rank = modulo(mpi_coords(4) - floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)

          do id=1,ndd
             call MPI_ISSend(U_xd(1,1,1,mapt(2)   ,id),nc*nc*nxd*nyd*nzss, mpi_dc, dest_rank, id, &
                  &                                    mpi_comm, sendrecv_reqt(1), ierr)
             call MPI_IRecv (U_xd(1,1,1,mapt(nt+2),id),nc*nc*nxd*nyd*nzss, mpi_dc,  src_rank, id, &
                  &                                    mpi_comm, sendrecv_reqt(2), ierr)
             call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
          end do

          dest_rank = modulo(mpi_coords(4) - floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)

          do id=1,ndd
             call MPI_ISSend(U_xd(1,1,1,mapt(nt-1),id), nc*nc*nxd*nyd*nzss, mpi_dc, dest_rank, id, &
                  &                                     mpi_comm, sendrecv_reqt(1), ierr)
             call MPI_IRecv (U_xd(1,1,1,mapt(-1)  ,id), nc*nc*nxd*nyd*nzss, mpi_dc,  src_rank, id, &
                  &                                     mpi_comm, sendrecv_reqt(2), ierr)
             call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
          end do

       end if

    end select

    t_out = mpi_wtime()
    call TimingUpdate(nshdwgf,t_out,t_in,mint_shdwgf,maxt_shdwgf,t_shdwgf)

  end subroutine ShadowGaugeField_4x

  subroutine ShadowGaugeField_eo(U_xd,ishdw)

    type(colour_matrix), dimension(:,:,0:) :: U_xd
    integer :: ishdw

    integer :: it, mu, src_rank, dest_rank, nxd, nyd, nzd, ntd
    integer :: sendrecv_reqz(2,nt*nd*2),sendrecv_reqt(2,nd*2), sendrecv_reqzs(2,nts*nd*2), ierr, tag
    integer :: sendrecvz_status(nmpi_status,nt*nd*2*2), sendrecvt_status(nmpi_status,nd*2*2), sendrecvzs_status(nmpi_status,nts*nd*2*2)
    integer :: i_xeo, j_xeo, i_eo

    nxd = nx
    nyd = ny
    nzd = nz
    ntd = nt

    select case(ishdw)
    case(1)

       if ( nprocz > 1 ) then
          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)

          ! Get the forward shadow.
          ! # of sites = (nx*ny*/2)*nt (even field or odd field)
          do i_eo=0,1
             do mu=1,nd
                do it=1,nt
                   i_xeo = x2eo((/1,1,1,it/))
                   j_xeo = x2eo((/1,1,nz+1,it/))

                   tag = it+(mu-1)*nt+i_eo*nd*nt
                   call MPI_ISSend(U_xd(i_xeo,mu,i_eo), nc*nc*(nx*ny)/2, mpi_dc,dest_rank,tag, mpi_comm,sendrecv_reqz(1,tag),ierr)
                   call MPI_IRecv (U_xd(j_xeo,mu,i_eo), nc*nc*(nx*ny)/2, mpi_dc, src_rank,tag, mpi_comm,sendrecv_reqz(2,tag),ierr)
                end do
             end do
          end do

          call MPI_WaitAll(nt*nd*2*2,sendrecv_reqz,sendrecvz_status,ierr)

          dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + 1, nprocz)
          src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - 1, nprocz)

          ! Get the backward shadow.
          ! # of sites = (nx*ny*/2)*nt (even field or odd field)
          do i_eo=0,1
             do mu=1,nd
                do it=1,nt
                   tag = it+(mu-1)*nt+i_eo*nd*nt
                   i_xeo = x2eo((/1,1,nz,it/))
                   j_xeo = x2eo((/1,1,0,it/))

                   call MPI_ISSend(U_xd(i_xeo,mu,i_eo), nc*nc*(nx*ny)/2, mpi_dc,dest_rank,tag, mpi_comm,sendrecv_reqz(1,tag),ierr)
                   call MPI_IRecv (U_xd(j_xeo,mu,i_eo), nc*nc*(nx*ny)/2, mpi_dc, src_rank,tag, mpi_comm,sendrecv_reqz(2,tag),ierr)
                end do
             end do
          end do

          call MPI_WaitAll(nt*nd*2*2,sendrecv_reqz,sendrecvz_status,ierr)

          nzd = nz + 2*ishdw
       end if

       if ( nproct > 1 ) then
          dest_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)

          ! Get the forward shadow.
          i_xeo = x2eo((/1,1,1,1/))
          j_xeo = x2eo((/1,1,1,nt+1/))

          ! # of sites = nx*ny*nz/2 (even field or odd field)
          do i_eo=0,1
             do mu=1,nd
                tag = mu+i_eo*nd
                call MPI_ISSend(U_xd(i_xeo,mu,i_eo), nc*nc*(nx*ny*nz)/2, mpi_dc, dest_rank, tag, mpi_comm, sendrecv_reqt(1,tag),ierr)
                call MPI_IRecv (U_xd(j_xeo,mu,i_eo), nc*nc*(nx*ny*nz)/2, mpi_dc,  src_rank, tag, mpi_comm, sendrecv_reqt(2,tag),ierr)
             end do
          end do

          call MPI_WaitAll(nd*2*2,sendrecv_reqt,sendrecvt_status,ierr)

          if ( nprocz > 1 ) then
             i_xeo = x2eo((/1,1,nz+1,1/))
             j_xeo = x2eo((/1,1,nz+1,nt+1/))

             ! Get the forward shadow "corner".
             ! # of sites = nx*ny*2/2 (even field or odd field)
             do i_eo=0,1
                do mu=1,nd
                   tag = mu+i_eo*nd
                   call MPI_ISSend(U_xd(i_xeo,mu,i_eo), nc*nc*nxd*nyd, mpi_dc, dest_rank, tag, mpi_comm, sendrecv_reqt(1,tag),ierr)
                   call MPI_IRecv (U_xd(j_xeo,mu,i_eo), nc*nc*nxd*nyd, mpi_dc,  src_rank, tag, mpi_comm, sendrecv_reqt(2,tag),ierr)
                end do
             end do

             call MPI_WaitAll(nd*2*2,sendrecv_reqt,sendrecvt_status,ierr)
          end if

          dest_rank = modulo(mpi_coords(4) + 1, nproct) + nproct*mpi_coords(3)
          src_rank = modulo(mpi_coords(4) - 1, nproct) + nproct*mpi_coords(3)

          ! Get the backward shadow.
          i_xeo = x2eo((/1,1,1,nt/))
          j_xeo = x2eo((/1,1,1,0/))

          do i_eo=0,1
             do mu=1,nd
                tag = mu+i_eo*nd
                call MPI_ISSend(U_xd(i_xeo,mu,i_eo), nc*nc*(nx*ny*nz)/2, mpi_dc, dest_rank, tag, mpi_comm, sendrecv_reqt(1,tag),ierr)
                call MPI_IRecv (U_xd(j_xeo,mu,i_eo), nc*nc*(nx*ny*nz)/2, mpi_dc,  src_rank, tag, mpi_comm, sendrecv_reqt(2,tag),ierr)
             end do
          end do

          call MPI_WaitAll(nd*2*2,sendrecv_reqt,sendrecvt_status,ierr)

          if ( nprocz > 1 ) then
             i_xeo = x2eo((/1,1,nz+1,nt/))
             j_xeo = x2eo((/1,1,nz+1,0/))

             do i_eo=0,1
                do mu=1,nd
                   tag = mu+i_eo*nd
                   call MPI_ISSend(U_xd(i_xeo,mu,i_eo), nc*nc*nxd*nyd, mpi_dc, dest_rank, tag, mpi_comm, sendrecv_reqt(1,tag),ierr)
                   call MPI_IRecv (U_xd(j_xeo,mu,i_eo), nc*nc*nxd*nyd, mpi_dc,  src_rank, tag, mpi_comm, sendrecv_reqt(2,tag),ierr)
                end do
             end do

             call MPI_WaitAll(nd*2*2,sendrecv_reqt,sendrecvt_status,ierr)
          end if

          ntd = nz + 2*ishdw
       end if

    end select

  end subroutine ShadowGaugeField_eo

  function CShiftGaugePlane(U_x,mu,nshift) result (U_xpmmu)

    type(colour_matrix), dimension(:,:,:,:) :: U_x
    type(colour_matrix), dimension(size(U_x,1),size(U_x,2),size(U_x,3),size(U_x,4)) :: U_xpmmu
    !type(colour_matrix), dimension(size(U_x)) :: U_xpmmu
    integer :: mu, nshift

    type(colour_matrix), dimension(:,:,:), allocatable :: S_x
    integer :: src_rank, dest_rank, send_sl, recv_sl, tag, mpi_status(nmpi_status), ierror
    integer :: ix, iy, iz, it, t0, t1, tr

    select case(mu)
    case(1) 
       do ix=0,nx-1
          recv_sl = ix+1
          send_sl = modulo(ix+nshift,nx)+1
          U_xpmmu(recv_sl,:,:,:) = U_x(send_sl,:,:,:)
       end do
    case(2)
       do iy=0,ny-1
          recv_sl = iy+1
          send_sl = modulo(iy+nshift,ny)+1
          U_xpmmu(:,recv_sl,:,:) = U_x(:,send_sl,:,:)
       end do
    case(3)
       if ( nprocz > 1 ) then 
          !For each slice, determine the source and destination ranks
          allocate(S_x(nx,ny,nt))
          tag = nx*ny*nt

          do iz=0,nz-1
             src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(iz + nshift)/nz), nprocz)
             dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(iz + nshift)/nz), nprocz)

             recv_sl = iz+1
             send_sl = modulo(iz+nshift,nz)+1

             if ( (src_rank /= mpi_rank) .and. (dest_rank /= mpi_rank) ) then
                S_x(:,:,:) = U_x(:,:,send_sl,:)
                call MPI_SendRecv_Replace(S_x, nc*nc*nx*ny*nt, mpi_dc, dest_rank, tag, src_rank, tag,&
                     &                   mpi_comm, mpi_status, ierror)
                U_xpmmu(:,:,recv_sl,:) = S_x(:,:,:)
             else
                U_xpmmu(:,:,recv_sl,:) = U_x(:,:,send_sl,:)
             end if
          end do
          deallocate(S_x)
       else
          do iz=0,nz-1
             recv_sl = iz+1
             send_sl = modulo(iz+nshift,nz)+1
             U_xpmmu(:,:,recv_sl,:) = U_x(:,:,send_sl,:)
          end do
       end if
    case(4)
       if ( nproct > 1 ) then
          allocate(S_x(nx,ny,nz))
          tag = nx*ny*nz

          do it=0,nt-1
             src_rank = modulo(mpi_coords(4) + floor(real(it + nshift)/nt), nproct) + nproct*mpi_coords(3)
             dest_rank = modulo(mpi_coords(4) - floor(real(it + nshift)/nt), nproct) + nproct*mpi_coords(3)

             recv_sl = it+1
             send_sl = modulo(it+nshift,nt)+1

             if ( (src_rank /= mpi_rank) .and. (dest_rank /= mpi_rank) ) then
                S_x(:,:,:) = U_x(:,:,:,send_sl)
                call MPI_SendRecv_Replace(S_x, nc*nc*nx*ny*nz, mpi_dc, dest_rank, tag, src_rank, tag,&
                     &                   mpi_comm, mpi_status, ierror)
                U_xpmmu(:,:,:,recv_sl) = S_x(:,:,:)
             else
                U_xpmmu(:,:,:,recv_sl) = U_x(:,:,:,send_sl)
             end if
          end do
          deallocate(S_x)
       else
          do it=0,nt-1
             recv_sl = it+1
             send_sl = modulo(it+nshift,nt)+1
             U_xpmmu(:,:,:,recv_sl) = U_x(:,:,:,send_sl)
          end do
       end if
    end select

  end function CShiftGaugePlane

  subroutine InitSendMatrixField_4(phi, dest_rank, request)

    type(colour_matrix), dimension(:,:,:,:) :: phi
    integer :: dest_rank, request
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend_Init(phi, size(phi)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine InitSendMatrixField_4

  subroutine InitRecvMatrixField_4(phi, src_rank, request)

    type(colour_matrix), dimension(:,:,:,:) :: phi
    integer :: src_rank, request
    integer :: mpierror, tag    
    tag = size(phi)

    call MPI_Recv_Init(phi, size(phi)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine InitRecvMatrixField_4

  subroutine InitSendMatrixField_5(phi, dest_rank, request)

    type(colour_matrix), dimension(:,:,:,:,:) :: phi
    integer :: dest_rank, request
    integer :: mpierror, tag
    tag = size(phi)

    call MPI_SSend_Init(phi, size(phi)*nc*nc, mpi_dc, dest_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine InitSendMatrixField_5

  subroutine InitRecvMatrixField_5(phi, src_rank, request)

    type(colour_matrix), dimension(:,:,:,:,:) :: phi
    integer :: src_rank, request
    integer :: mpierror, tag    
    tag = size(phi)

    call MPI_Recv_Init(phi, size(phi)*nc*nc, mpi_dc, src_rank, tag, mpi_comm, request, mpierror)
    call MPI_Errorcheck(mpierror)

  end subroutine InitRecvMatrixField_5

end module GaugeFieldMPIComms
