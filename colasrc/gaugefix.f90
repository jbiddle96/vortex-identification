module GaugeFix

  contains
    
    subroutine shadowrealfield(field)
      use LatticeSize
      use MPIInterface
    
      real(dp), dimension(:,:,:,:) :: field
      
      integer :: tag, dest_rank, src_rank
      integer :: it, nxd,nyd,nzd,ntd ! d = deferred shape array
      integer :: sendrecv_reqz(nt,2),sendrecv_reqt(2), ierr
      integer :: sendrecvz_status(nmpi_status,nt*2), sendrecvt_status(nmpi_status,2)
      
      nxd = size(field,1)
      nyd = size(field,2)
      nzd = size(field,3)
      ntd = size(field,4)
      
      if ( nprocz > 1 ) then
       
         dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(nz)/nz), nprocz)
         src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(nz)/nz), nprocz)
         
         do it=1,nt
            call MPI_ISSend(field(1,1,mapz(1)   ,it), nxd*nyd,mpi_dp,dest_rank,it+nd*id,&
                 &                                      mpi_comm,sendrecv_reqz(it,1),ierr)
            call MPI_IRecv (field(1,1,mapz(nz+1),it), nxd*nyd, mpi_dp,  src_rank, it+nd*id, &
                 &                                      mpi_comm,sendrecv_reqz(it,2),ierr)
         end do
         call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
         
         dest_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) - floor(real(-1)/nz), nprocz)
         src_rank = mpi_coords(4) + nproct*modulo(mpi_coords(3) + floor(real(-1)/nz), nprocz)
         
         do it=1,nt
            call MPI_ISSend(field(1,1,mapz(nz),it), nxd*nyd, mpi_dp, dest_rank, it+ndd*id, &
                 &                                    mpi_comm, sendrecv_reqz(it,1), ierr)
            call MPI_IRecv (field(1,1,mapz(0) ,it), nxd*nyd, mpi_dp,  src_rank, it+ndd*id, &
                 &                                    mpi_comm, sendrecv_reqz(it,2), ierr)
         end do
         call MPI_WaitAll(nt*2,sendrecv_reqz,sendrecvz_status,ierr)
         
      end if
    
      if ( nproct > 1 ) then
         
         dest_rank = modulo(mpi_coords(4) - floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)
         src_rank = modulo(mpi_coords(4) + floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)
         
         call MPI_ISSend(field(1,1,1,mapt(1)   ), nxd*nyd*nzd, mpi_dp, dest_rank, id, &
              &                                     mpi_comm, sendrecv_reqt(1), ierr)
         call MPI_IRecv (field(1,1,1,mapt(nt+1)), nxd*nyd*nzd, mpi_dp,  src_rank, id, &
              &                                     mpi_comm, sendrecv_reqt(2), ierr)
         call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
       
         dest_rank = modulo(mpi_coords(4) - floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)
         src_rank = modulo(mpi_coords(4) + floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)
       
         call MPI_ISSend(field(1,1,1,mapt(nt)), nxd*nyd*nzd, mpi_dp, dest_rank, id, &
              &                                   mpi_comm, sendrecv_reqt(1), ierr)
         call MPI_IRecv (field(1,1,1,mapt(0) ), nxd*nyd*nzd, mpi_dp,  src_rank, id, &
              &                                   mpi_comm, sendrecv_reqt(2), ierr)
         call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)
       
      end if
    
    end subroutine shadowrealfield
  
    subroutine dototal(tg_l,g_l)
      use ColourTypes
      use MatrixAlgebra
      use Kinds
      use LatticeSize
      
      type(colour_matrix), dimension(:,:,:,:), intent(inout) :: tg_l
      type(colour_matrix), dimension(:,:,:,:), intent(in)    :: g_l
      
      type(colour_matrix), dimension(nx,ny,nz,nt)            :: temp
      integer                                                :: ix,iy,iz,it

      do ix=1,nx;do iy=1,ny;do iz=1,nz; do it=1,nt
         call MultiplyMatMat(temp,g_l(1:nx,1:ny,1:nz,1:nt),tg_l(1:nx,1:ny,1:nz,1:nt))
         tg_l(1:nx,1:ny,1:nz,1:nt) = temp(1:nx,1:ny,1:nz,1:nt)
      end do;end do;end do;end do

    end subroutine dototal

    subroutine conjgrad(ans, known, mres)
      use Kinds
      use LatticeSize
      use ColourTypes
      use ReduceOps
      use MPIInterface
      use CollectiveOps
      use GaugeFieldMPIComms
      use myTimer
      implicit none

      type(colour_matrix), dimension(:,:,:,:),intent(in)    :: known
      type(colour_matrix), dimension(:,:,:,:),intent(inout) :: ans ! ans comes in as the initial guess
      integer                                               :: id  ! and goes out as the answer.
      real(dp)                                              :: a, r, rn
      real(dp)                                              :: sublatr, sublatrn
      real(dp),dimension(nx,ny,nz,nt)                       :: lap, res
      real(dp), dimension(nxs,nys,nzs,nts)                  :: dir

      real(dp),dimension(nxs,nys,nzs,nts)                   :: pans

      integer                                               :: icg, theTotal
      integer,parameter                                     :: ncg = 1000
      real(dp)                                              :: mres
      integer                                               :: ic, jc
      logical                                               :: converged

      integer, dimension(4)                                 :: dmu
      integer                                               :: ix,iy,iz,it, icmplx
      real(dp)                                              :: globalMax
      real(dp)                                              :: sublatDirByLap, dirByLap

      do icmplx = 1,2
! This loops over real and complex parts. Inelegant, but It'll get the job done.               

 !        call StartTimer
         theTotal = 0
         do ic = 1, nc-1
            do jc = 1, nc
               lap = 0.0_dp
               select case(icmplx)
               case(1)
                  pans(:,:,:,:) =  real(ans(:,:,:,:)%cl(ic,jc),dp)
               case(2)
                  pans(:,:,:,:) = aimag(ans(:,:,:,:)%cl(ic,jc)   )
               end select
               do id = 1, nd
                  dmu(:) = 0
                  dmu(id) = 1
                  
                  do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                     lap(ix,iy,iz,it) = lap(ix,iy,iz,it) & 
                          &   + pans(mapx(ix+dmu(1)),mapy(iy+dmu(2)),mapz(iz+dmu(3)),mapt(it+dmu(4))) &
                          &   + pans(mapx(ix-dmu(1)),mapy(iy-dmu(2)),mapz(iz-dmu(3)),mapt(it-dmu(4)))
                  end do;end do;end do;end do                     
               end do
               lap(:,:,:,:) = lap(:,:,:,:) - real(nd,dp) * 2.0_dp * pans(1:nx,1:ny,1:nz,1:nt)

               select case(icmplx)
               case(1)
                  res =  real(known(1:nx,1:ny,1:nz,1:nt)%cl(ic,jc),dp) - lap
               case(2)
                  res = aimag(known(1:nx,1:ny,1:nz,1:nt)%cl(ic,jc)   ) - lap
               end select
               call AllMax(maxval(abs(res)), globalMax)
               
               if( globalMax < mres ) then 
                  converged = .true.
               else
                  dir(1:nx,1:ny,1:nz,1:nt) = res
                  call ShadowRealField(dir)
                  sublatr = sum(res**2)
                  call AllSum(sublatr,r)
                  icg = 0
                  converged = .false.
               endif

               do while( .not. converged .and. icg < ncg )
                  lap = 0.0_dp
                  do id = 1, nd
                     dmu(:) = 0
                     dmu(id) = 1
                     do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
                        lap(ix,iy,iz,it) = lap(ix,iy,iz,it) & 
                             &   + dir(mapx(ix+dmu(1)),mapy(iy+dmu(2)),mapz(iz+dmu(3)),mapt(it+dmu(4))) &
                             &   + dir(mapx(ix-dmu(1)),mapy(iy-dmu(2)),mapz(iz-dmu(3)),mapt(it-dmu(4)))
                     end do;end do;end do;end do 
                  enddo
                  lap(:,:,:,:) = lap(:,:,:,:) - real(nd,dp) * 2.0_dp * dir(1:nx,1:ny,1:nz,1:nt)
                  sublatDirByLap = sum(dir(1:nx,1:ny,1:nz,1:nt)*lap(:,:,:,:))
                  call AllSum(sublatDirByLap,DirByLap)
                  
                  a = r / DirByLap
                  pans(1:nx,1:ny,1:nz,1:nt) = pans(1:nx,1:ny,1:nz,1:nt) + a * dir(1:nx,1:ny,1:nz,1:nt)
                  res(:,:,:,:) = res(:,:,:,:) - a * lap(:,:,:,:)
                  call ShadowRealField(pans)
                  !     test convergence
                  call AllMax(maxval(abs(res)), globalMax)
                  if( globalMax < mres ) then
                     converged = .true.
                  end if
                  
                  sublatrn = sum(res**2)
                  call AllSum(sublatrn,rn)
                  dir(1:nx,1:ny,1:nz,1:nt) = res(:,:,:,:) + rn / r * dir(1:nx,1:ny,1:nz,1:nt)
                  call ShadowRealField(dir)
                  
                  icg = icg + 1
                  r = rn
               enddo
               theTotal = theTotal + icg
               select case(icmplx)
               case(1)
                  ans(1:nx,1:ny,1:nz,1:nt)%cl(ic,jc) = cmplx(pans(1:nx,1:ny,1:nz,1:nt),aimag(ans(1:nx,1:ny,1:nz,1:nt)%cl(ic,jc)),dc)
               case(2)
                  ans(1:nx,1:ny,1:nz,1:nt)%cl(ic,jc) = cmplx(real(ans(1:nx,1:ny,1:nz,1:nt)%cl(ic,jc),dp),pans(1:nx,1:ny,1:nz,1:nt),dc)
               end select
               if( .not. converged ) then
                  print *, 'laplacian inversion did not converge in ', ncg, ' steps.'
                  stop
               endif
               
            enddo
         enddo
!         call EndTimer
!         if(i_am_root) call TimerOut("Time for half of conjgrad:")
!         if(i_am_root) write(*,*) "total iterations:", theTotal
      end do

      call ShadowSU3Field(ans,1)
      return
    end subroutine conjgrad
    
    function unitaritu
      
      use Kinds
      use GaugeField
      use ReduceOps
      use LatticeSize
      use ColourTypes
      use MatrixAlgebra
      use ColourFieldOps

      implicit none

      type(colour_matrix), dimension(:,:,:,:,:),allocatable  :: unit
      real(dp)                                               :: sublatUnitaritu, unitaritu
      integer                                                :: ic,jc

      call CreateGaugeField(unit,0)

      call MultiplyMatMatdag(unit,U_xd(1:nx,1:ny,1:nz,1:nt,:),U_xd(1:nx,1:ny,1:nz,1:nt,:))
      sublatUnitaritu = 0.0_dp
      
      do ic=1,nc
         do jc=1,nc
            sublatUnitaritu = sublatUnitaritu + sum(abs(real(unit(:,:,:,:,:)%cl(ic,jc),dp)))
         end do
      end do
      sublatUnitaritu = sublatUnitaritu / real(nlattice*nc*nd,dp)
      do ic=1,nc
         do jc=1,nc
            sublatUnitaritu = sublatUnitaritu + sum(abs(aimag(unit(:,:,:,:,:)%cl(ic,jc))))
         end do
      end do
      
      call AllSum(sublatUnitaritu,unitaritu)
      call DestroyGaugeField(unit)

      return
    end function unitaritu

    function unitaritg(g)

      use Kinds
      use GaugeField
      use ReduceOps
      use LatticeSize
      use ColourTypes
      use MatrixAlgebra
      use ColourFieldOps

      implicit none

      type(colour_matrix),dimension(:,:,:,:),intent(in)       :: g
      type(colour_matrix),dimension(:,:,:,:),allocatable      :: unit
      real(dp)                                                :: sublatUnitaritg, unitaritg
      integer                                                 :: ic ,jc
      character(len=4),parameter                              :: sdim = 'xyzt'
      integer, dimension(4),parameter                         :: idim = (/ 0, 0, 0, 0 /)

      call AllocMatrixField(unit,sdim,idim)

      call MultiplyMatMatdag(unit,g(1:nx,1:ny,1:nz,1:nt),g(1:nx,1:ny,1:nz,1:nt))

      sublatUnitaritg = 0.0_dp
      do ic=1,nc
         do jc=1,nc
            sublatUnitaritg = sublatUnitaritg + sum(abs(real(unit(:,:,:,:)%cl(ic,jc),dp)))
         end do
      end do
      sublatUnitaritg = sublatUnitaritg / real(nlattice*nc,dp)
      do ic=1,nc
         do jc=1,nc
            sublatUnitaritg = sublatUnitaritg + sum(abs(aimag(unit(:,:,:,:)%cl(ic,jc))))
         end do
      end do

      call AllSum(sublatUnitaritg,unitaritg)
      call DeallocMatrixField(unit)

    end function unitaritg

    subroutine WriteGaugeTrans(filename,tg)
      use Kinds
      use MPIInterface
      use ColourFieldOps
      use LatticeSize
      use ColourTypes
      use GaugeFieldMPIComms
      use ColourFieldOps
      character(len=*)                        :: filename
      type(colour_matrix), dimension(:,:,:,:) :: tg
      real(dp)                                :: beta

    ! begin local_vars
      integer :: ic, jc, irank
      integer :: ix, jx, iy, jy, iz, jz, it, jt

      type(colour_matrix), dimension(:,:,:,:), allocatable  :: V_xd
      type(colour_matrix), dimension(:,:,:,:), allocatable  :: U_lxd !! Global gauge to be read in.
      real(DP), dimension(:,:,:,:,:,:), allocatable ::  ReU, ImU

      ! begin execution
      allocate(V_xd(nx,ny,nz,nt))

      if ( i_am_root ) allocate(U_lxd(nlx,nly,nlz,nlt))

      if ( i_am_root ) then

         do irank=0,nproc-1
            if ( irank /= mpi_root_rank ) then
               call RecvMatrixField(V_xd, irank)
            else
               V_xd = tg(1:nx,1:ny,1:nz,1:nt)
            end if
            call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
            U_lxd(ix:jx,iy:jy,iz:jz,it:jt) = V_xd(1:nx,1:ny,1:nz,1:nt)
         end do
      else
         V_xd = tg(1:nx,1:ny,1:nz,1:nt)
         call SendMatrixField(V_xd, mpi_root_rank)
      end if

      if ( i_am_root ) then
         allocate(ReU(nlx,nly,nlz,nlt,nc,nc))
         allocate(ImU(nlx,nly,nlz,nlt,nc,nc))

         do jc=1,nc; do ic=1,nc
            ReU(:,:,:,:,ic,jc) =  real(U_lxd(:,:,:,:)%Cl(ic,jc))
            ImU(:,:,:,:,ic,jc) = aimag(U_lxd(:,:,:,:)%Cl(ic,jc))
         end do; end do

         open(101,file=filename,form='unformatted',status='replace',action='write')
         
          do ic = 1, nc-1
            write (101) ReU(:,:,:,:,ic,:)
            write (101) ImU(:,:,:,:,ic,:)
         end do
         
         close(101)

         deallocate(ReU)
         deallocate(ImU)
      end if

      call MPIBarrier

    end subroutine WriteGaugeTrans

    subroutine ReadGaugeTrans(filename,tg)
      use Kinds
      use MPIInterface
      use ColourFieldOps
      use LatticeSize
      use ColourTypes
      use GaugeFieldMPIComms
      use ColourFieldOps
      use GaugeField
      character(len=*)                        :: filename
      type(colour_matrix), dimension(:,:,:,:) :: tg

    ! begin local_vars
      integer :: ic, jc, irank
      integer :: ix, jx, iy, jy, iz, jz, it, jt

      type(colour_matrix), dimension(:,:,:,:), allocatable  :: V_xd
      type(colour_matrix), dimension(:,:,:,:), allocatable  :: U_lxd !! Global gauge to be read in.
      real(DP), dimension(:,:,:,:,:,:), allocatable ::  ReU, ImU

      ! begin execution
      allocate(V_xd(nx,ny,nz,nt))

      if ( i_am_root ) allocate(U_lxd(nlx,nly,nlz,nlt))

 !     if ( i_am_root ) then

 !        do irank=0,nproc-1
 !           if ( irank /= mpi_root_rank ) then
 !              call RecvMatrixField(V_xd, irank)
 !           else
 !              V_xd = tg(1:nx,1:ny,1:nz,1:nt)
 !           end if
 !           call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
 !           U_lxd(ix:jx,iy:jy,iz:jz,it:jt) = V_xd(1:nx,1:ny,1:nz,1:nt)
 !        end do
 !     else
 !        V_xd = tg(1:nx,1:ny,1:nz,1:nt)
 !        call SendMatrixField(V_xd, mpi_root_rank)
 !     end if

      if ( i_am_root ) then
         allocate(ReU(nlx,nly,nlz,nlt,nc,nc))
         allocate(ImU(nlx,nly,nlz,nlt,nc,nc))

 !        do jc=1,nc; do ic=1,nc
 !           ReU(:,:,:,:,ic,jc) =  real(U_lxd(:,:,:,:)%Cl(ic,jc))
 !           ImU(:,:,:,:,ic,jc) = aimag(U_lxd(:,:,:,:)%Cl(ic,jc))
 !        end do; end do

         open(101,file=filename,form='unformatted',status='old',action='read')
         
!         read (101) beta,nlx,nly,nlz,nlt

         do ic = 1, nc-1
            read (101) ReU(:,:,:,:,ic,:)
            read (101) ImU(:,:,:,:,ic,:)
         end do

         close(101)

         do jc=1,nc; do ic=1,nc-1
            U_lxd(:,:,:,:)%Cl(ic,jc) = cmplx(ReU(:,:,:,:,ic,jc),ImU(:,:,:,:,ic,jc),dc)
         end do; end do

         do it=1,nlt; do iz=1,nlz; do iy=1,nly; do ix=1,nlx
            call FixSU3Matrix(U_lxd(ix,iy,iz,it))
         end do; end do; end do; end do

         deallocate(ReU)
         deallocate(ImU)
      end if

      if( i_am_root ) then
         do irank = 0,nproc-1
            if( irank /= mpi_root_rank )  then
               call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
               call SendMatrixField(U_lxd(ix:jx,iy:jy,iz:jz,it:jt),irank)
            else
               tg(1:nx,1:ny,1:nz,1:nt) = U_lxd(1:nx,1:ny,1:nz,1:nt)
            end if
         end do
      else
         call RecvMatrixField(V_xd,mpi_root_rank)
         tg(1:nx,1:ny,1:nz,1:nt) = V_xd
      end if

      call ShadowSU3Field(tg,1)


      call MPIBarrier

    end subroutine ReadGaugeTrans
    

  end module GaugeFix
