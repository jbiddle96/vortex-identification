
#include "defines.h"

module LatticeSize

  use MPIInterface
  use TextIO
  use CollectiveOps
  use Strings
  implicit none
  private
  
  integer, public :: nlx !! The extent of the lattice in the *x* direction.
  integer, public :: nly !! The extent of the lattice in the *y* direction.
  integer, public :: nlz !! The extent of the lattice in the *z* direction.
  integer, public :: nlt !! The extent of the lattice in the *t* direction.

  integer, public :: lpx, upx !! The extents of the lattice momenta in the *x* direction.
  integer, public :: lpy, upy !! The extents of the lattice momenta in the *y* direction.
  integer, public :: lpz, upz !! The extents of the lattice momenta in the *z* direction.
  integer, public :: lpt, upt !! The extents of the lattice momenta in the *t* direction.

  integer, public :: nprocx !! The extent of the processor topology in the *x* direction.
  integer, public :: nprocy !! The extent of the processor topology in the *y* direction.
  integer, public :: nprocz !! The extent of the processor topology in the *z* direction.
  integer, public :: nproct !! The extent of the processor topology in the *t* direction.

  integer, public :: nx !! The extent of the processor sublattice in the *x* direction.
  integer, public :: ny !! The extent of the processor sublattice in the *y* direction.
  integer, public :: nz !! The extent of the processor sublattice in the *z* direction.
  integer, public :: nt !! The extent of the processor sublattice in the *t* direction.

  integer, public :: nproc !! The total number of processors.

  integer, public :: shdwx, shdwy, shdwz, shdwt ! Dimension shadowing for cshifts.
  integer, public :: nxs, nys, nzs, nts  ! single shadow (Nearest neighbour)
  integer, public :: nxss, nyss, nzss, ntss ! double shadow (Next Nearest neighbour)

  integer, public :: padx, pady, padz, padt ! Dimension padding for caching
  integer, public :: nxp, nyp, nzp, ntp !padded dimensions

  integer, parameter, public :: nd=4 !! The number of space-time dimensions.

  !*To Do*: Change/relocate these declarations?
  integer, parameter, public :: nplaq = 6 !# of unique Plaquettes per site= nd*(nd-1)/2
  integer, parameter, public :: n_a = 8 !number of generators of SU(3)

  integer, public :: nlattice      !!lattice size
  integer, public :: nsublattice   !!sublattice size
  integer, public :: nlattice_eo   !!even odd lattice size
  integer, public :: n_xeo         !!even odd lattice size per processor
  integer, public :: n_xpeo        !!padded even odd lattice size per processor

  integer, dimension(:), allocatable, public :: mapx, mapy, mapz, mapt ! shadow mapping
  integer, dimension(:,:), allocatable, public :: n_offset_eo ! even-odd field shadow mapping

  integer, public :: i_nx, j_nx, i_ny, j_ny, i_nt, j_nt, i_nz, j_nz !the sublattice that is stored on this processor
  integer, public :: proc_eo

  integer, dimension(nd), public :: mpi_coords

  public :: modc
  public :: InitialiseLattice
  public :: FinaliseLattice
  public :: coords_to_rank
  public :: rank_to_coords
  public :: GetSubLattice
  public :: site_is_mine
  public :: LatticeToSubLattice
  public :: SubLatticeToLattice
  public :: site_eo
  public :: x2eo
  public :: eo2x
  public :: GetSubLattice_eo
  public :: AssertSublatticeIsContiguous

contains


  !! Maps an integer a to the set of integers [1,b] i.e. positive integers with cycle length b.
  elemental function modc(a,b) result (c)

    integer, intent(in) :: a,b
    integer :: c

    !c = a - ((a-1)/b)*b
    c = modulo(a-1,b)+1

  end function modc

  !! Reads from the input file named "{_file_stub_}.lat" and initialises the lattice size,
  !! sublattices, shadows/halos and padding.

  subroutine InitialiseLattice(file_stub)

    character(len=*) :: file_stub

    character(len=_FILENAME_LEN_) :: latticefile
    integer :: iproc
    integer :: file_unit

    integer :: ix,iy,iz,it,is,id
    integer :: length_t(4), disp_t(4), length_z(nt*4), disp_z(nt*4)
    integer :: length_zs(nts*4), disp_zs(nts*4)
    integer :: n_x(nd),nproc_x(nd),i_offset,i_plus,n_xshdw,i_shdw,mu
    logical :: mask_x(nd)

    latticefile = trim(file_stub)//".lat"

    file_unit = 171

    call OpenTextfile(trim(latticefile),file_unit,action='read')
    call Get(nlx,file_unit)
    call Get(nly,file_unit)
    call Get(nlz,file_unit)
    call Get(nlt,file_unit)
    call CloseTextfile(file_unit)

    if ( modulo(nlx,2) .ne. 0 ) call Put("WARNING :: extent of lattice in x dimension is not even.") 
    if ( modulo(nly,2) .ne. 0 ) call Put("WARNING :: extent of lattice in y dimension is not even.") 
    if ( modulo(nlz,2) .ne. 0 ) call Put("WARNING :: extent of lattice in z dimension is not even.") 
    if ( modulo(nlt,2) .ne. 0 ) call Put("WARNING :: extent of lattice in t dimension is not even.") 

    nprocx = 1
    nprocy = 1
    nprocz = 1
    nproct = 1

    iproc = mpi_size
    !! Allocate processors in the t direction.
    if ( iproc <= nlt ) then
       if ( modulo(nlt,iproc) == 0 ) then
          nproct = iproc
          iproc = iproc/nproct
       else
          call Abort("InitialiseLattice:Process count and lattice size mismatch (nlt .mod. nproc /= 0).") 
       end if
    else
       if ( modulo(iproc,nlt) == 0 ) then
          nproct = nlt
          iproc = iproc/nproct
       else
          call Abort("InitialiseLattice:Process count and lattice size mismatch (nproc .mod. nlt /= 0).") 
       end if
    end if

    !! Allocate processors in the z direction.
    if ( iproc <= nlz ) then
       if ( modulo(nlz,iproc) == 0 ) then
          nprocz = iproc
          iproc = iproc/nprocz
       else
          call Abort("InitialiseLattice:Process count and lattice size mismatch (nlz .mod. [nproc/nproct] /= 0).") 
       end if
    else
       if ( modulo(iproc,nlz) == 0 ) then
          nprocz = nlz
          iproc = iproc/nprocz
       else
          call Abort("InitialiseLattice:Process count and lattice size mismatch ([nproc/nproct] .mod. nlz /= 0).") 
       end if
    end if

    !! Allocate processors in the y direction.
    if ( iproc <= nly ) then
       if ( modulo(nly,iproc) == 0 ) then
          nprocy = iproc
          iproc = iproc/nprocy
       else
          call Abort("InitialiseLattice:Process count and lattice size mismatch (nly .mod. [nproc/(nprocz*nproct)] /= 0).") 
       end if
    else
       if ( modulo(iproc,nly) == 0 ) then
          nprocy = nly
          iproc = iproc/nprocy
       else
          call Abort("InitialiseLattice:Process count and lattice size mismatch ([nproc/(nprocz*nproct)] .mod. nly /= 0).") 
       end if
    end if

    if ( iproc /= 1 ) then
       call Abort("InitialiseLattice:Could not allocate processor topology:MPI process count and lattice size mismatch.") 
    end if

    nproc = nprocx*nprocy*nprocz*nproct

    upx = nlx/2
    upy = nly/2
    upz = nlz/2
    upt = nlt/2
    
    lpx = -nlx/2 + 1
    lpy = -nly/2 + 1
    lpz = -nlz/2 + 1
    lpt = -nlt/2 + 1

    nx=nlx/nprocx
    ny=nly/nprocy
    nz=nlz/nprocz
    nt=nlt/nproct

    call Put("Lattice size (x,y,z,t) = ("//str(nlx,6)//str(nly,6)//","//str(nlz,6)//","//str(nlt,6)//")")
    call Put("Lattice size per processor (x,y,z,t) = ("//str(nx,6)//str(ny,6)//","//str(nz,6)//","//str(nt,6)//")")
    call Put("Allocated processor topology (y,z,t) = ("//str(nprocy,4)//","//str(nprocz,4)//","//str(nproct,4)//")")

    shdwx = 0
    shdwy = 0
    shdwz = 0
    shdwt = 0

    if ( nprocx > 1 ) shdwx = 2
    if ( nprocy > 1 ) shdwy = 2
    if ( nprocz > 1 ) shdwz = 2
    if ( nproct > 1 ) shdwt = 2

    nxs=nx+shdwx
    nys=ny+shdwy
    nzs=nz+shdwz
    nts=nt+shdwt

    nxss=nx+2*shdwx
    nyss=ny+2*shdwy
    nzss=nz+2*shdwz
    ntss=nt+2*shdwt

    padx = 0
    pady = 0
    padz = 0
    padt = 0

    if ( nprocx > 1 ) padx = 2
    if ( nprocy > 1 ) pady = 2
    if ( nprocz > 1 ) padz = 2
    if ( nproct > 1 ) padt = 2

    nxp=nx+padx
    nyp=ny+pady
    nzp=nz+padz
    ntp=nt+padt 

    if ( nprocx > 1 ) shdwx = 2
    if ( nprocy > 1 ) shdwy = 2
    if ( nprocz > 1 ) shdwz = 2
    if ( nproct > 1 ) shdwt = 2

    nlattice = nlx*nly*nlz*nlt
    nsublattice = nx*ny*nz*nt
    nlattice_eo = (nlx*nly*nlz*nlt)/2
    n_xeo = (nx*ny*nz*nt)/2
    n_xpeo = (nxp*nyp*nzp*ntp)/2

    allocate(mapx(-1:nx+2))
    allocate(mapy(-1:ny+2))
    allocate(mapz(-1:nz+2))
    allocate(mapt(-1:nt+2))

    !! Do not distribute over the x dimension.
    mapx(-1) = nx-1
    mapx(0) = nx
    do ix=1,nx
       mapx(ix) = ix
    end do
    mapx(nx+1) = 1
    mapx(nx+2) = 2

    do iy=1,ny
       mapy(iy) = iy
    end do
    
    if ( nprocy == 1 ) then
       mapy(-1) = ny-1
       mapy(0) = ny
       mapy(ny+1) = 1
       mapy(ny+2) = 2
    else
       mapy(-1) = ny+4
       mapy(0) = ny+2
       mapy(ny+1) = ny+1
       mapy(ny+2) = ny+3
    end if

    do iz=1,nz
       mapz(iz) = iz
    end do

    if ( nprocz == 1 ) then
       mapz(-1) = nz-1
       mapz(0) = nz
       mapz(nz+1) = 1
       mapz(nz+2) = 2
    else
       mapz(-1) = nz+4
       mapz(0) = nz+2
       mapz(nz+1) = nz+1
       mapz(nz+2) = nz+3
    end if

    do it=1,nt
       mapt(it) = it
    end do

    if ( nproct == 1 ) then
       mapt(-1) = nt-1
       mapt(0) = nt
       mapt(nt+1) = 1
       mapt(nt+2) = 2
    else
       mapt(-1) = nt+4
       mapt(0) = nt+2
       mapt(nt+1) = nt+1
       mapt(nt+2) = nt+3
    end if

    allocate(n_offset_eo(nd,+1:+4))
    n_offset_eo = 0

    n_x = (/ nx,ny,nz,nt /)
    nproc_x = (/ nprocx,nprocy,nprocz,nproct /)
    i_offset = n_xeo
    do i_shdw=+1,+2
       do mu = 2,nd
          if ( nproc_x(mu) > 1 ) then
             ! The shadow consists of two lattice slices, each with the index in the mu direction fixed. 
             ! Here we ensure that the + and - shadows are adjacent.
             mask_x = .true.
             mask_x(mu) = .false.
             n_xshdw = product(n_x,mask=mask_x)/2

             i_plus = 2*i_shdw-1
             n_offset_eo(mu,i_plus) = i_offset
             i_offset = i_offset + n_xshdw

             i_plus = 2*i_shdw
             n_offset_eo(mu,i_plus) = i_offset
             i_offset = i_offset + n_xshdw 

             n_x(mu) = n_x(mu) + 2
          end if
       end do
    end do

    mpi_coords = rank_to_coords(mpi_rank)

    call GetSubLattice(mpi_rank,i_nx,j_nx,i_ny,j_ny,i_nz,j_nz,i_nt,j_nt)

    proc_eo = modulo(i_nx+i_ny+i_nz+i_nt,2)

  end subroutine InitialiseLattice

  subroutine FinaliseLattice

    deallocate(mapx)
    deallocate(mapy)
    deallocate(mapz)
    deallocate(mapt)

    deallocate(n_offset_eo)
  end subroutine FinaliseLattice

  pure function coords_to_rank(coords) result (rank)

    integer, dimension(nd), intent(in) :: coords(nd)
    integer :: rank

    rank = coords(4) + nproct*coords(3) 

  end function coords_to_rank

  pure function rank_to_coords(rank) result (coords)

    integer, intent(in) :: rank
    integer, dimension(nd) :: coords(nd)

    coords(4) = mod(rank,nproct)
    coords(3) = mod(rank/nproct,nprocz)
    coords(2) = rank/(nproct*nprocz)
    coords(1) = 0

  end function rank_to_coords

  pure subroutine GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)

    integer, intent(in) :: irank
    integer, intent(out) :: ix,jx,iy,jy,iz,jz,it,jt
    integer, dimension(nd) :: coords(nd)

    coords = rank_to_coords(irank)

    ix = nx*coords(1)+ 1
    jx = nx*(coords(1) + 1)

    iy = ny*coords(2)+ 1
    jy = ny*(coords(2) + 1)

    iz = nz*coords(3)+ 1
    jz = nz*(coords(3) + 1)

    it = nt*coords(4)+ 1
    jt = nt*(coords(4) + 1)

  end subroutine GetSubLattice

  function site_is_mine(ix,iy,iz,it)

    integer :: ix,iy,iz,it
    logical :: site_is_mine

    site_is_mine = (i_nx <= ix) .and. (ix <= j_nx) .and. (i_ny <= iy) .and. (iy <= j_ny) .and. &
         &         (i_nz <= iz) .and. (iz <= j_nz) .and. (i_nt <= it) .and. (it <= j_nt)

  end function site_is_mine

  subroutine LatticeToSubLattice(ix,iy,iz,it,jx,jy,jz,jt)

    integer :: ix,jx,iy,jy,iz,jz,it,jt

    jx = ix - i_nx + 1 
    jy = iy - i_ny + 1 
    jz = iz - i_nz + 1 
    jt = it - i_nt + 1 

  end subroutine LatticeToSubLattice

  subroutine SubLatticeToLattice(ix,iy,iz,it,jx,jy,jz,jt)

    integer :: ix,jx,iy,jy,iz,jz,it,jt

    jx = ix + i_nx - 1 
    jy = iy + i_ny - 1 
    jz = iz + i_nz - 1 
    jt = it + i_nt - 1 

  end subroutine SubLatticeToLattice

  pure function site_eo(i_x) result (i_eo)
    integer, dimension(nd), intent(in) :: i_x
    integer :: i_eo

    i_eo = modulo(sum(i_x)+proc_eo,2)
  end function site_eo

  pure function x2eo(i_x) result(i_xeo)
    integer, dimension(nd), intent(in) :: i_x
    integer :: i_xeo
    
    integer :: i_xyzt, i_offset, j_x(nd)
    logical :: in_shadow(nd)

    ! See which dimensions are in the shadow/halo.
    in_shadow = ((/1,1,1,1/) > i_x) .or. (i_x > (/ nx,ny,nz,nt /))

    if ( any(in_shadow) ) then
       j_x = (/ mapx(i_x(1)),mapy(i_x(2)),mapz(i_x(3)),mapt(i_x(4)) /)
       if ( (nproct > 1) .and. in_shadow(4) ) then
          i_xyzt = j_x(1) + (j_x(2)-1)*nxs + (j_x(3)-1)*nxs*nys 
          ! Get the base offset, after mapping the t index into a positive linear offset 
          !(assumes shadow storage is after normal lattice).
          i_offset = n_offset_eo(4,j_x(4)-nt)
          !print '(4I4)', mpi_rank, i_xyzt, i_offset, mapt(i_x(4))-nt
       else if ( (nprocz > 1) .and. in_shadow(3) ) then
          i_xyzt = j_x(1) + (j_x(2)-1)*nxs + (j_x(4)-1)*nxs*nys 
          ! Get the base offset, after mapping the z index into a positive linear offset 
          !(assumes shadow storage is after normal lattice).
          i_offset = n_offset_eo(3,j_x(3)-nz)
       else if ( (nprocy > 1) .and. in_shadow(2) ) then
          i_xyzt = j_x(1) + (j_x(3)-1)*nxs + (j_x(4)-1)*nxs*nzs 
          ! Get the base offset, after mapping the y index into a positive linear offset 
          !(assumes shadow storage is after normal lattice).
          i_offset = n_offset_eo(2,mapy(i_x(2))-ny)
       else
          i_xyzt = j_x(1) + (j_x(2)-1)*nx + (j_x(3)-1)*nx*ny + (j_x(4)-1)*nx*ny*nz
          i_offset = 0
       end if
    else
       i_xyzt = i_x(1) + (i_x(2)-1)*nx + (i_x(3)-1)*nx*ny + (i_x(4)-1)*nx*ny*nz
       i_offset = 0
    end if
    i_xeo = i_offset + (i_xyzt+1)/2

  end function x2eo

  pure function eo2x(i_xeo,i_eo) result(i_x)
    integer, intent(in) :: i_xeo,i_eo
    integer, dimension(nd) :: i_x
    
    integer :: i_xyzt, j_eo

    i_xyzt = (i_xeo-1)*2
    
    i_x(1) = 1+modulo(i_xyzt,nx)
    i_x(2) = 1+modulo((i_xyzt)/nx,ny)
    i_x(3) = 1+modulo((i_xyzt)/(nx*ny),nz)
    i_x(4) = 1+(i_xyzt)/(nx*ny*nz)

    j_eo = modulo(sum(i_x)+proc_eo,2)
    i_x(1) = i_x(1) + abs(j_eo - i_eo)

  end function eo2x

  pure subroutine GetSublattice_eo(irank,i_lxeo,j_lxeo)
    integer, intent(in) :: irank
    integer, intent(out) :: i_lxeo, j_lxeo

    integer :: ix,jx,iy,jy,iz,jz,it,jt
    integer :: i_xyzt, j_xyzt

    call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)

    i_xyzt = ix + (iy-1)*nlx + (iz-1)*nlx*nly + (it-1)*nlx*nly*nlz
    j_xyzt = jx + (jy-1)*nlx + (jz-1)*nlx*nly + (jt-1)*nlx*nly*nlz

    i_lxeo = (i_xyzt+1)/2
    j_lxeo = (j_xyzt+1)/2

  end subroutine GetSublattice_eo
  
  subroutine AssertSublatticeIsContiguous

    logical :: assertion

    if ( nprocx > 1 ) then
       assertion = (ny == 1) .and. (nz == 1) .and. (nt == 1)
    else
       if ( nprocy > 1 ) then
          assertion = (nz == 1) .and. (nt == 1)
       else
          if ( nprocz > 1 ) then
             assertion = (nt == 1)
          else
             assertion = .true.
          end if
       end if
    end if

    if ( .not. assertion ) then
       call Abort("Assertion failed: Sublattice is not contiguous.")
    end if

  end subroutine AssertSublatticeIsContiguous

end module LatticeSize
