!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/realfieldmpicomms.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: realfieldmpicomms.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :


module RealFieldMPIComms

  use MPIInterface
  use Kinds
  implicit none
  private

  interface SendRealField
     module procedure SendRealField_0
     module procedure SendRealField_1
     module procedure SendRealField_2
     module procedure SendRealField_3
     module procedure SendRealField_4
     module procedure SendRealField_5
     module procedure SendRealField_6
     module procedure SendRealField_7
  end interface

  interface RecvRealField
     module procedure RecvRealField_0
     module procedure RecvRealField_1
     module procedure RecvRealField_2
     module procedure RecvRealField_3
     module procedure RecvRealField_4
     module procedure RecvRealField_5
     module procedure RecvRealField_6
     module procedure RecvRealField_7
  end interface

  public :: SendRealField
  public :: RecvRealField

contains

  subroutine SendRealField_0(phi, dest_rank)
    ! begin args: phi, dest_rank

    real(dp) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror
    integer :: tag, nphi

    ! begin execution

    nphi = 1
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dp, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendRealField_0

  subroutine RecvRealField_0(phi, src_rank)
    ! begin args: phi, src_rank

    real(dp) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = 1
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dp, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvRealField_0

  subroutine SendRealField_1(phi, dest_rank)
    ! begin args: phi, dest_rank

    real(dp), dimension(:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror
    integer :: tag, nphi

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dp, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendRealField_1

  subroutine RecvRealField_1(phi, src_rank)
    ! begin args: phi, src_rank

    real(dp), dimension(:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dp, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvRealField_1

  subroutine SendRealField_2(phi, dest_rank)
    ! begin args: phi, dest_rank

    real(dp), dimension(:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dp, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendRealField_2

  subroutine RecvRealField_2(phi, src_rank)
    ! begin args: phi, src_rank

    real(dp), dimension(:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dp, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvRealField_2

  subroutine SendRealField_3(phi, dest_rank)
    ! begin args: phi, dest_rank

    real(dp), dimension(:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dp, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendRealField_3

  subroutine RecvRealField_3(phi, src_rank)
    ! begin args: phi, src_rank

    real(dp), dimension(:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dp, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvRealField_3

  subroutine SendRealField_4(phi, dest_rank)
    ! begin args: phi, dest_rank

    real(dp), dimension(:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dp, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendRealField_4

  subroutine RecvRealField_4(phi, src_rank)
    ! begin args: phi, src_rank

    real(dp), dimension(:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dp, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvRealField_4

  subroutine SendRealField_5(phi, dest_rank)
    ! begin args: phi, dest_rank

    real(dp), dimension(:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dp, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendRealField_5

  subroutine RecvRealField_5(phi, src_rank)
    ! begin args: phi, src_rank

    real(dp), dimension(:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dp, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvRealField_5

  subroutine SendRealField_6(phi, dest_rank)
    ! begin args: phi, dest_rank

    real(dp), dimension(:,:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dp, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendRealField_6

  subroutine RecvRealField_6(phi, src_rank)
    ! begin args: phi, src_rank

    real(dp), dimension(:,:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dp, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvRealField_6

  subroutine SendRealField_7(phi, dest_rank)
    ! begin args: phi, dest_rank

    real(dp), dimension(:,:,:,:,:,:,:) :: phi
    integer :: dest_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Send(phi, nphi, mpi_dp, dest_rank, tag, mpi_comm, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine SendRealField_7

  subroutine RecvRealField_7(phi, src_rank)
    ! begin args: phi, src_rank

    real(dp), dimension(:,:,:,:,:,:,:) :: phi
    integer :: src_rank
    ! begin local_vars
    integer :: mpierror, tag, nphi
    integer, dimension(nmpi_status) :: mpi_status

    ! begin execution

    nphi = size(phi)
    tag = nphi

    call MPI_Recv(phi, nphi, mpi_dp, src_rank, tag, mpi_comm, mpi_status, mpierror)
    call MPI_Errorcheck(mpierror)


  end subroutine RecvRealField_7

end module RealFieldMPIComms

