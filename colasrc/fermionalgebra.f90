#include "defines.h"

module FermionAlgebra
  use LatticeSize
  use ColourTypes
  use FermionTypes
  use Kinds
  implicit none
  private

  public :: PsiPlusPhi
  public :: PsiMinusPhi
  public :: PsiPlusAlphaPhi
  public :: PsiMinusAlphaPhi
  public :: AlphaPsiPlusPhi
  public :: AlphaPsiMinusPhi
  public :: MultiplyAlphaPhi
  public :: AlphaPsi

  interface PsiPlusPhi
    module procedure PsiPlusPhi_4x
    module procedure PsiPlusPhi_eo
  end interface PsiPlusPhi

  interface PsiMinusPhi
    module procedure PsiMinusPhi_4x
    module procedure PsiMinusPhi_eo
  end interface PsiMinusPhi

  interface PsiPlusAlphaPhi
    module procedure PsiPlusAlphaPhi_4x_dp
    module procedure PsiPlusAlphaPhi_4x_dc
    module procedure PsiPlusAlphaPhi_eo_dp
    module procedure PsiPlusAlphaPhi_eo_dc
  end interface PsiPlusAlphaPhi

  interface PsiMinusAlphaPhi
    module procedure PsiMinusAlphaPhi_4x_dp
    module procedure PsiMinusAlphaPhi_4x_dc
    module procedure PsiMinusAlphaPhi_eo_dp
    module procedure PsiMinusAlphaPhi_eo_dc
  end interface PsiMinusAlphaPhi

  interface AlphaPsiPlusPhi
    module procedure AlphaPsiPlusPhi_4x_dp
    module procedure AlphaPsiPlusPhi_4x_dc
    module procedure AlphaPsiPlusPhi_eo_dp
    module procedure AlphaPsiPlusPhi_eo_dc
  end interface AlphaPsiPlusPhi

  interface AlphaPsiMinusPhi
    module procedure AlphaPsiMinusPhi_4x_dp
    module procedure AlphaPsiMinusPhi_4x_dc
    module procedure AlphaPsiMinusPhi_eo_dp
    module procedure AlphaPsiMinusPhi_eo_dc
  end interface AlphaPsiMinusPhi

  interface MultiplyAlphaPhi
    module procedure MultiplyAlphaPhi_4x_dp
    module procedure MultiplyAlphaPhi_4x_dc
    module procedure MultiplyAlphaPhi_eo_dp
    module procedure MultiplyAlphaPhi_eo_dc
  end interface MultiplyAlphaPhi

  interface AlphaPsi
    module procedure AlphaPsi_4x_dp
    module procedure AlphaPsi_4x_dc
    module procedure AlphaPsi_eo_dp
    module procedure AlphaPsi_eo_dc
  end interface AlphaPsi

contains

  subroutine PsiPlusPhi_4x(psi,phi)
    ! begin args: psi,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = psi(ix,iy,iz,it,is)%cl + phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine PsiPlusPhi_4x

  subroutine PsiMinusPhi_4x(psi,phi)
    ! begin args: psi,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = psi(ix,iy,iz,it,is)%cl - phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine PsiMinusPhi_4x

  subroutine PsiPlusPhi_eo(psi,phi)
    ! begin args: psi,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = psi(i_xeo)%cs + phi(i_xeo)%cs
    end do
  end subroutine PsiPlusPhi_eo

  subroutine PsiMinusPhi_eo(psi,phi)
    ! begin args: psi,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = psi(i_xeo)%cs - phi(i_xeo)%cs
    end do
  end subroutine PsiMinusPhi_eo

  subroutine PsiPlusAlphaPhi_4x_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = psi(ix,iy,iz,it,is)%cl + alpha * phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine PsiPlusAlphaPhi_4x_dp

  subroutine PsiMinusAlphaPhi_4x_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = psi(ix,iy,iz,it,is)%cl - alpha * phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine PsiMinusAlphaPhi_4x_dp

  subroutine AlphaPsiPlusPhi_4x_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = alpha * psi(ix,iy,iz,it,is)%cl + phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine AlphaPsiPlusPhi_4x_dp

  subroutine AlphaPsiMinusPhi_4x_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = alpha * psi(ix,iy,iz,it,is)%cl - phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine AlphaPsiMinusPhi_4x_dp

  subroutine MultiplyAlphaPhi_4x_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = alpha * phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine MultiplyAlphaPhi_4x_dp

  subroutine AlphaPsi_4x_dp(psi,alpha)
    ! begin args: psi,alpha
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    real(dp) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = alpha * psi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine AlphaPsi_4x_dp

  subroutine PsiPlusAlphaPhi_4x_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = psi(ix,iy,iz,it,is)%cl + alpha * phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine PsiPlusAlphaPhi_4x_dc

  subroutine PsiMinusAlphaPhi_4x_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = psi(ix,iy,iz,it,is)%cl - alpha * phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine PsiMinusAlphaPhi_4x_dc

  subroutine AlphaPsiPlusPhi_4x_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = alpha * psi(ix,iy,iz,it,is)%cl + phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine AlphaPsiPlusPhi_4x_dc

  subroutine AlphaPsiMinusPhi_4x_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = alpha * psi(ix,iy,iz,it,is)%cl - phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine AlphaPsiMinusPhi_4x_dc

  subroutine MultiplyAlphaPhi_4x_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(colour_vector), dimension(:,:,:,:,:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = alpha * phi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine MultiplyAlphaPhi_4x_dc

  subroutine AlphaPsi_4x_dc(psi,alpha)
    ! begin args: psi,alpha
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: ix,iy,iz,it,is,ns
    ! begin execution
    ns = size(psi,5)
    do is=1,ns; do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
      psi(ix,iy,iz,it,is)%cl = alpha * psi(ix,iy,iz,it,is)%cl
    end do; end do; end do; end do; end do
  end subroutine AlphaPsi_4x_dc

  subroutine PsiPlusAlphaPhi_eo_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = psi(i_xeo)%cs + alpha * phi(i_xeo)%cs
    end do
  end subroutine PsiPlusAlphaPhi_eo_dp

  subroutine PsiMinusAlphaPhi_eo_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = psi(i_xeo)%cs - alpha * phi(i_xeo)%cs
    end do
  end subroutine PsiMinusAlphaPhi_eo_dp

  subroutine AlphaPsiPlusPhi_eo_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = alpha * psi(i_xeo)%cs + phi(i_xeo)%cs
    end do
  end subroutine AlphaPsiPlusPhi_eo_dp

  subroutine AlphaPsiMinusPhi_eo_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = alpha * psi(i_xeo)%cs - phi(i_xeo)%cs
    end do
  end subroutine AlphaPsiMinusPhi_eo_dp

  subroutine MultiplyAlphaPhi_eo_dp(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    real(dp) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = alpha * phi(i_xeo)%cs
    end do
  end subroutine MultiplyAlphaPhi_eo_dp

  subroutine AlphaPsi_eo_dp(psi,alpha)
    ! begin args: psi,alpha
    type(dirac_fermion), dimension(:) :: psi
    real(dp) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = alpha * psi(i_xeo)%cs
    end do
  end subroutine AlphaPsi_eo_dp

  subroutine PsiPlusAlphaPhi_eo_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = psi(i_xeo)%cs + alpha * phi(i_xeo)%cs
    end do
  end subroutine PsiPlusAlphaPhi_eo_dc

  subroutine PsiMinusAlphaPhi_eo_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = psi(i_xeo)%cs - alpha * phi(i_xeo)%cs
    end do
  end subroutine PsiMinusAlphaPhi_eo_dc

  subroutine AlphaPsiPlusPhi_eo_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = alpha * psi(i_xeo)%cs + phi(i_xeo)%cs
    end do
  end subroutine AlphaPsiPlusPhi_eo_dc

  subroutine AlphaPsiMinusPhi_eo_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = alpha * psi(i_xeo)%cs - phi(i_xeo)%cs
    end do
  end subroutine AlphaPsiMinusPhi_eo_dc

  subroutine MultiplyAlphaPhi_eo_dc(psi,alpha,phi)
    ! begin args: psi,alpha,phi
    type(dirac_fermion), dimension(:) :: psi, phi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = alpha * phi(i_xeo)%cs
    end do
  end subroutine MultiplyAlphaPhi_eo_dc

  subroutine AlphaPsi_eo_dc(psi,alpha)
    ! begin args: psi,alpha
    type(dirac_fermion), dimension(:) :: psi
    complex(dc) :: alpha
    ! begin local_vars
    integer :: i_xeo
    ! begin execution
    do i_xeo=1,n_xeo
      psi(i_xeo)%cs = alpha * psi(i_xeo)%cs
    end do
  end subroutine AlphaPsi_eo_dc

end module FermionAlgebra
