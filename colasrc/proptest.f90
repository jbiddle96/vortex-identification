#include "defines.h"
#include "fermionfields.h"

  program proptest
    use LatticeSize
    use ColourTypes
    use SpinorTypes
    use FermionTypes
    use Kinds
    use Strings
    use TextIO
    use ColourFieldOps
    use Timer
    use MPIInterface
    use QuarkPropagator
    use PropagatorIO
    use GaugeField
    use GaugeFieldIO
    use FermionField
    use FermionFieldIO
    use FermionAction
    use FermionAlgebra
    use ConjGradSolvers
    use QuarkPropTypes
    implicit none

    complex(dc), dimension(8000,40,3,4,3,4) :: phi
    character (len=300) :: propname

    call InitialiseMPI
    propname = '/data/cssm/dtrewartha/su3b394k1324s20t40IMPFLIC/propagators/proptwo/repsu3b394k1324s20t40IMPFLICc000_sw30.overpropmu01271.solntsp'

    open(200,file=trim(propname),form='unformatted',status='old',action='read')

    call ReadTimeSlicePropagatortwo(phi,200)

    write(*,*) phi(1,1,1,1,1,1)

    call FinaliseMPI

  contains

    subroutine ReadTimeSlicePropagatortwo(phi,OutFileId)
      ! begin args: phi, OutFileId

      complex(dc), dimension(:,:,:,:,:,:) :: phi
      integer :: OutFileId
      ! begin local_vars
      complex(dc), dimension(:,:,:,:,:,:), allocatable :: chi
      real(dp), dimension(:,:,:,:,:,:), allocatable :: phir, phii
      integer :: ix, jx, iy, jy, iz, jz, it, jt
      integer :: kx,ky,kz,kt, i_xyz, k_xyz
      integer :: irank
      integer :: nx,ny,nz,nt,nc,ns

      nx = 20
      ny = 20
      nz = 20
      nt = 40
      nc = 3
      ns = 4

      ! begin execution

      allocate(chi(nx*ny*nz,nt,nc,ns,nc,ns))

      allocate(phir(nx*ny*nz,nt,nc,ns,nc,ns))
      allocate(phii(nx*ny*nz,nt,nc,ns,nc,ns))

      do it=1,nt
         read(OutFileId) phir(:,it,:,:,:,:)
         read(OutFileId) phii(:,it,:,:,:,:)
      end do

      irank = 0
      call GetSubLattice(irank,ix,jx,iy,jy,iz,jz,it,jt)
      do kt=1,nt; do kz=1,nz; do ky=1,ny; do kx=1,nx
         k_xyz =  kx + (ky-1)*nx + (kz-1)*nx*ny
         i_xyz = (ix+(kx-1)) + (iy-1+(ky-1))*nx + (iz-1+(kz-1))*nx*ny
         chi(k_xyz,kt,:,:,:,:) = cmplx(phir(i_xyz,it+kt-1,:,:,:,:),phii(i_xyz,it+kt-1,:,:,:,:),dc)
      end do; end do; end do; end do

      phi = chi

      deallocate(phir)
      deallocate(phii)

      deallocate(chi)

    end subroutine ReadTimeSlicePropagatortwo
  end program proptest

