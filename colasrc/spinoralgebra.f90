
module SpinorAlgebra
  use Kinds
  use ColourTypes
  use SpinorTypes
  use FermionTypes
  implicit none
  private

  public :: trace, traceGammaMu, colour_trace, MultiplyMatMat, MultiplyMatMatDag

contains

  elemental function trace(S) result (tr)
    type(spin_matrix), intent(in) :: S
    complex(dp) :: tr

    tr = S%sp(1,1) + S%sp(2,2) + S%sp(3,3) + S%sp(4,4)
  end function trace

  elemental function traceGammaMu(S,mu) result (tr)
    type(spin_matrix), intent(in) :: S
    integer, intent(in) :: mu

    complex(dp) :: tr

    select case(mu)
    case(1)
       tr = -i*(S%sp(4,1) + S%sp(3,2) - S%sp(2,3) - S%sp(1,4))
    case(2)
       tr = -S%sp(4,1) + S%sp(3,2) + S%sp(2,3) - S%sp(1,4)
    case(3)
       tr = -i*(S%sp(3,1) - S%sp(4,2) - S%sp(1,3) + S%sp(2,4))
    case(4)
       tr = S%sp(1,1) + S%sp(2,2) - S%sp(3,3) - S%sp(4,4)
    end select
  end function traceGammaMu

  elemental function colour_trace(SC) result (S)
    type(spin_colour_matrix), intent(in) :: SC
    type(spin_matrix) :: S

    S%sp(:,:) = SC%sc(:,:,1,1) + SC%sc(:,:,2,2) + SC%sc(:,:,3,3) 
  end function colour_trace

  elemental subroutine MultiplyMatMat(MM,left,right)

    type(colour_matrix), intent(in) :: left
    type(spin_colour_matrix), intent(in) :: right
    type(spin_colour_matrix), intent(out) :: MM

    MM%sc(:,:,1,1) = left%Cl(1,1)*right%sc(:,:,1,1) + left%Cl(1,2)*right%sc(:,:,2,1) + left%Cl(1,3)*right%sc(:,:,3,1)
    MM%sc(:,:,2,1) = left%Cl(2,1)*right%sc(:,:,1,1) + left%Cl(2,2)*right%sc(:,:,2,1) + left%Cl(2,3)*right%sc(:,:,3,1)
    MM%sc(:,:,3,1) = left%Cl(3,1)*right%sc(:,:,1,1) + left%Cl(3,2)*right%sc(:,:,2,1) + left%Cl(3,3)*right%sc(:,:,3,1)

    MM%sc(:,:,1,2) = left%Cl(1,1)*right%sc(:,:,1,2) + left%Cl(1,2)*right%sc(:,:,2,2) + left%Cl(1,3)*right%sc(:,:,3,2)
    MM%sc(:,:,2,2) = left%Cl(2,1)*right%sc(:,:,1,2) + left%Cl(2,2)*right%sc(:,:,2,2) + left%Cl(2,3)*right%sc(:,:,3,2)
    MM%sc(:,:,3,2) = left%Cl(3,1)*right%sc(:,:,1,2) + left%Cl(3,2)*right%sc(:,:,2,2) + left%Cl(3,3)*right%sc(:,:,3,2)

    MM%sc(:,:,1,3) = left%Cl(1,1)*right%sc(:,:,1,3) + left%Cl(1,2)*right%sc(:,:,2,3) + left%Cl(1,3)*right%sc(:,:,3,3)
    MM%sc(:,:,2,3) = left%Cl(2,1)*right%sc(:,:,1,3) + left%Cl(2,2)*right%sc(:,:,2,3) + left%Cl(2,3)*right%sc(:,:,3,3)
    MM%sc(:,:,3,3) = left%Cl(3,1)*right%sc(:,:,1,3) + left%Cl(3,2)*right%sc(:,:,2,3) + left%Cl(3,3)*right%sc(:,:,3,3)

  end subroutine MultiplyMatMat

  elemental subroutine MultiplyMatMatdag(MM,left,right)

    type(spin_colour_matrix), intent(in) :: left
    type(colour_matrix), intent(in) :: right
    type(spin_colour_matrix), intent(out) :: MM

    MM%sc(:,:,1,1) = left%sc(:,:,1,1)*conjg(right%Cl(1,1)) + left%sc(:,:,1,2)*conjg(right%Cl(1,2)) + left%sc(:,:,1,3)*conjg(right%Cl(1,3))
    MM%sc(:,:,2,1) = left%sc(:,:,2,1)*conjg(right%Cl(1,1)) + left%sc(:,:,2,2)*conjg(right%Cl(1,2)) + left%sc(:,:,2,3)*conjg(right%Cl(1,3))
    MM%sc(:,:,3,1) = left%sc(:,:,3,1)*conjg(right%Cl(1,1)) + left%sc(:,:,3,2)*conjg(right%Cl(1,2)) + left%sc(:,:,3,3)*conjg(right%Cl(1,3))

    MM%sc(:,:,1,2) = left%sc(:,:,1,1)*conjg(right%Cl(2,1)) + left%sc(:,:,1,2)*conjg(right%Cl(2,2)) + left%sc(:,:,1,3)*conjg(right%Cl(2,3))
    MM%sc(:,:,2,2) = left%sc(:,:,2,1)*conjg(right%Cl(2,1)) + left%sc(:,:,2,2)*conjg(right%Cl(2,2)) + left%sc(:,:,2,3)*conjg(right%Cl(2,3))
    MM%sc(:,:,3,2) = left%sc(:,:,3,1)*conjg(right%Cl(2,1)) + left%sc(:,:,3,2)*conjg(right%Cl(2,2)) + left%sc(:,:,3,3)*conjg(right%Cl(2,3))

    MM%sc(:,:,1,3) = left%sc(:,:,1,1)*conjg(right%Cl(3,1)) + left%sc(:,:,1,2)*conjg(right%Cl(3,2)) + left%sc(:,:,1,3)*conjg(right%Cl(3,3))
    MM%sc(:,:,2,3) = left%sc(:,:,2,1)*conjg(right%Cl(3,1)) + left%sc(:,:,2,2)*conjg(right%Cl(3,2)) + left%sc(:,:,2,3)*conjg(right%Cl(3,3))
    MM%sc(:,:,3,3) = left%sc(:,:,3,1)*conjg(right%Cl(3,1)) + left%sc(:,:,3,2)*conjg(right%Cl(3,2)) + left%sc(:,:,3,3)*conjg(right%Cl(3,3))

  end subroutine MultiplyMatMatdag

 

end module SpinorAlgebra
