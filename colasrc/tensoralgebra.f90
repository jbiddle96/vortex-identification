!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/tensoralgebra.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: tensoralgebra.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module TensorAlgebra

  use ColourTypes
  use SpinorTypes
  use Kinds
  implicit none
  private

  public :: StarInnerProduct
  public :: AddVectorOuterProduct
  public :: SubVectorOuterProduct

contains

  subroutine StarInnerProduct(LinnerR,left,right)
    ! begin args: LinnerR, left, right

    complex(dc) :: LinnerR
    type(colour_matrix) :: left, right

    ! begin execution

    LinnerR = left%Cl(1,1)*right%Cl(1,1) + left%Cl(1,2)*right%Cl(2,1) + left%Cl(1,3)*right%Cl(3,1) + &
         &    left%Cl(2,1)*right%Cl(1,2) + left%Cl(2,2)*right%Cl(2,2) + left%Cl(2,3)*right%Cl(3,2) + &
         &    left%Cl(3,1)*right%Cl(1,3) + left%Cl(3,2)*right%Cl(2,3) + left%Cl(3,3)*right%Cl(3,3)


  end subroutine StarInnerProduct

  subroutine AddVectorOuterProduct(LouterR,left,right)
    ! begin args: LouterR, left, right

    type(colour_matrix)  :: LouterR
    type(colour_vector), dimension(ns) :: left, right

    ! begin execution

    LouterR%Cl(1,1) = LouterR%Cl(1,1) + left(1)%Cl(1)*right(1)%Cl(1) + left(2)%Cl(1)*right(2)%Cl(1) + &
         &                              left(3)%Cl(1)*right(3)%Cl(1) + left(4)%Cl(1)*right(4)%Cl(1)
    LouterR%Cl(2,1) = LouterR%Cl(2,1) + left(1)%Cl(1)*right(1)%Cl(2) + left(2)%Cl(1)*right(2)%Cl(2) + &
         &                              left(3)%Cl(1)*right(3)%Cl(2) + left(4)%Cl(1)*right(4)%Cl(2)
    LouterR%Cl(3,1) = LouterR%Cl(3,1) + left(1)%Cl(1)*right(1)%Cl(3) + left(2)%Cl(1)*right(2)%Cl(3) + &
         &                              left(3)%Cl(1)*right(3)%Cl(3) + left(4)%Cl(1)*right(4)%Cl(3)

    LouterR%Cl(1,2) = LouterR%Cl(1,2) + left(1)%Cl(2)*right(1)%Cl(1) + left(2)%Cl(2)*right(2)%Cl(1) + &
         &                              left(3)%Cl(2)*right(3)%Cl(1) + left(4)%Cl(2)*right(4)%Cl(1)
    LouterR%Cl(2,2) = LouterR%Cl(2,2) + left(1)%Cl(2)*right(1)%Cl(2) + left(2)%Cl(2)*right(2)%Cl(2) + &
         &                              left(3)%Cl(2)*right(3)%Cl(2) + left(4)%Cl(2)*right(4)%Cl(2)
    LouterR%Cl(3,2) = LouterR%Cl(3,2) + left(1)%Cl(2)*right(1)%Cl(3) + left(2)%Cl(2)*right(2)%Cl(3) + &
         &                              left(3)%Cl(2)*right(3)%Cl(3) + left(4)%Cl(2)*right(4)%Cl(3)

    LouterR%Cl(1,3) = LouterR%Cl(1,3) + left(1)%Cl(3)*right(1)%Cl(1) + left(2)%Cl(3)*right(2)%Cl(1) + &
         &                              left(3)%Cl(3)*right(3)%Cl(1) + left(4)%Cl(3)*right(4)%Cl(1)
    LouterR%Cl(2,3) = LouterR%Cl(2,3) + left(1)%Cl(3)*right(1)%Cl(2) + left(2)%Cl(3)*right(2)%Cl(2) + &
         &                              left(3)%Cl(3)*right(3)%Cl(2) + left(4)%Cl(3)*right(4)%Cl(2)
    LouterR%Cl(3,3) = LouterR%Cl(3,3) + left(1)%Cl(3)*right(1)%Cl(3) + left(2)%Cl(3)*right(2)%Cl(3) + &
         &                              left(3)%Cl(3)*right(3)%Cl(3) + left(4)%Cl(3)*right(4)%Cl(3)


  end subroutine AddVectorOuterProduct

  subroutine SubVectorOuterProduct(LouterR,left,right)
    ! begin args: LouterR, left, right

    type(colour_matrix)  :: LouterR
    type(colour_vector), dimension(ns) :: left, right

    ! begin execution

    LouterR%Cl(1,1) = LouterR%Cl(1,1) - left(1)%Cl(1)*right(1)%Cl(1) - left(2)%Cl(1)*right(2)%Cl(1) - &
         &                              left(3)%Cl(1)*right(3)%Cl(1) - left(4)%Cl(1)*right(4)%Cl(1)
    LouterR%Cl(2,1) = LouterR%Cl(2,1) - left(1)%Cl(1)*right(1)%Cl(2) - left(2)%Cl(1)*right(2)%Cl(2) - &
         &                              left(3)%Cl(1)*right(3)%Cl(2) - left(4)%Cl(1)*right(4)%Cl(2)
    LouterR%Cl(3,1) = LouterR%Cl(3,1) - left(1)%Cl(1)*right(1)%Cl(3) - left(2)%Cl(1)*right(2)%Cl(3) - &
         &                              left(3)%Cl(1)*right(3)%Cl(3) - left(4)%Cl(1)*right(4)%Cl(3)

    LouterR%Cl(1,2) = LouterR%Cl(1,2) - left(1)%Cl(2)*right(1)%Cl(1) - left(2)%Cl(2)*right(2)%Cl(1) - &
         &                              left(3)%Cl(2)*right(3)%Cl(1) - left(4)%Cl(2)*right(4)%Cl(1)
    LouterR%Cl(2,2) = LouterR%Cl(2,2) - left(1)%Cl(2)*right(1)%Cl(2) - left(2)%Cl(2)*right(2)%Cl(2) - &
         &                              left(3)%Cl(2)*right(3)%Cl(2) - left(4)%Cl(2)*right(4)%Cl(2)
    LouterR%Cl(3,2) = LouterR%Cl(3,2) - left(1)%Cl(2)*right(1)%Cl(3) - left(2)%Cl(2)*right(2)%Cl(3) - &
         &                              left(3)%Cl(2)*right(3)%Cl(3) - left(4)%Cl(2)*right(4)%Cl(3)

    LouterR%Cl(1,3) = LouterR%Cl(1,3) - left(1)%Cl(3)*right(1)%Cl(1) - left(2)%Cl(3)*right(2)%Cl(1) - &
         &                              left(3)%Cl(3)*right(3)%Cl(1) - left(4)%Cl(3)*right(4)%Cl(1)
    LouterR%Cl(2,3) = LouterR%Cl(2,3) - left(1)%Cl(3)*right(1)%Cl(2) - left(2)%Cl(3)*right(2)%Cl(2) - &
         &                              left(3)%Cl(3)*right(3)%Cl(2) - left(4)%Cl(3)*right(4)%Cl(2)
    LouterR%Cl(3,3) = LouterR%Cl(3,3) - left(1)%Cl(3)*right(1)%Cl(3) - left(2)%Cl(3)*right(2)%Cl(3) - &
         &                              left(3)%Cl(3)*right(3)%Cl(3) - left(4)%Cl(3)*right(4)%Cl(3)


  end subroutine SubVectorOuterProduct

end module TensorAlgebra

