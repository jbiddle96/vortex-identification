
module EVTypes
  use Kinds
  use TextIO
  use MPIInterface
  use LatticeSize
  implicit none
  private

  type, public :: EVMetaDataType
     character(len=128) :: header
     integer :: n_x
     integer :: n_ev
     integer :: n_auxev
     integer, dimension(nd) :: nl_d ! lattice dimensions
     !character(len=_FILENAME_LEN_) :: GaugeFieldFile
     character(len=64) :: FermActName
     integer :: kBFStrength
     real(dp) :: kappa
     real(dp) :: tolerance
     real(dp), dimension(:), allocatable :: lambda_i ! eigenvalues
   contains
     procedure :: Read => ReadEVMetaDataType
     procedure :: Write => WriteEVMetaDataType
     procedure :: Get => GetEVMetaDataType
     procedure :: Put => PutEVMetaDataType
  end type EVMetaDataType

contains

  subroutine ReadEVMetaDataType(this,filename)
    class(EVMetaDataType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='read')
    call this%Get(file_unit)
    call CloseTextfile(file_unit)

  end subroutine ReadEVMetaDataType

  subroutine WriteEVMetaDataType(this,filename)
    class(EVMetaDataType) :: this
    character(len=*) :: filename
    integer :: file_unit

    call OpenTextfile(filename,file_unit,action='write')
    call this%Put(file_unit)
    call CloseTextfile(file_unit)

  end subroutine WriteEVMetaDataType

  subroutine GetEVMetadataType(this,file_unit)
    class(EVMetaDataType) :: this
    integer :: file_unit

    integer :: i_ev

    call Get(this%header,file_unit)
    call Get(this%n_x,file_unit)
    call Get(this%n_ev,file_unit)
    call Get(this%n_auxev,file_unit)
    call Get(this%nl_d,file_unit)
    if ( .not. all( this%nl_d == (/ nlx, nly, nlz, nlt /) ) ) then
       call Abort("ReadFMEVMetaData: Lattice size mismatch.")
    end if
    call Get(this%FermActName,file_unit)
    call Get(this%kBFStrength,file_unit)
    call Get(this%kappa,file_unit)
    call Get(this%tolerance,file_unit)
    if ( allocated(this%lambda_i) ) deallocate(this%lambda_i)
    allocate(this%lambda_i(this%n_ev))
    do i_ev=1,this%n_ev
       call Get(this%lambda_i(i_ev),file_unit)
    end do

  end subroutine GetEVMetadataType

  subroutine PutEVMetadataType(this,file_unit)
    class(EVMetaDataType) :: this
    integer :: file_unit

    integer :: i_ev

    ! begin execution
    if ( i_am_root ) then
       write(file_unit,'(a)') "! binary file format = fmev, evio_version = 1.0, gamma_basis = chiral"
       write(file_unit,'(i20,a)') this%n_x, " !n_x"
       write(file_unit,'(i20,a)') this%n_ev, " !n_ev"
       write(file_unit,'(i20,a)') this%n_auxev, " !n_auxev"
       write(file_unit,'(4i5,a)') this%nl_d, " !qp%nl_d"
       write(file_unit,'(a)') this%FermActName
       write(file_unit,'(i20,a)') this%kBFStrength, " !qp_kBFStrength"
       write(file_unit,'(f20.10,a)') this%kappa, " !kappa"
       write(file_unit,'(es20.10,a)') this%tolerance, " !tolerance"
       do i_ev=1,this%n_ev
          write (file_unit,'(f,a,i5)') this%lambda_i(i_ev), ' !lambda_i(i_ev), i_ev = ', i_ev
       end do
    end if

  end subroutine PutEVMetadataType

end module EVTypes
