!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/gaugeaction.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: gaugeaction.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module GaugeAction

  use MatrixAlgebra
  use MPIInterface
  use GaugeField
  use ReduceOps
  use LatticeSize
  use Kinds
  use ColourTypes
  implicit none
  private

  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: U_xdss

  integer, public :: actiontype

  integer, parameter, public :: wilson_glue = 0, twoloop_glue = 1, improved_glue = 2, iwasaki_glue = 3, dbw2_glue = 42

  public :: ActionParameters
  public :: S_gauge
  public :: GetdS_gbydU
  public :: GetAction
  public :: GetStaples

contains

  subroutine ActionParameters(beta,c_R,beta_eff)
    ! begin args: beta, c_R, beta_eff

    real(dp) :: beta
    real(dp) :: beta_eff, c_R

    ! begin execution

    select case (actiontype)
    case (wilson_glue)
       beta_eff = beta
       c_R = 0.0d0
    case (twoloop_glue)
       beta_eff = (5.0d0/3.0d0)*beta
       c_R = -1.0/20.0d0
    case (improved_glue)
       beta_eff = (5.0d0/3.0d0)*beta
       c_R = -1.0/(20.0d0*u0_bar**2)
    case (iwasaki_glue)
       ! Reference hep-lat/9610023
       beta_eff = (3.648d0)*beta
       c_R = -0.331d0/3.648d0
    case (dbw2_glue)
       !DBW2 glue
       beta_eff = beta
       c_R = -1.4067d0/12.2536d0
    end select


  end subroutine ActionParameters

  function S_gauge(U_xd,beta)
    ! begin args: U_xd, beta

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    ! begin local_vars
    real(DP) :: S_gauge,beta

    real(dp) :: c_R, beta_eff, beta_11, beta_12, beta_21, S_gpp
    integer :: mu,nu

    ! begin execution

    call ActionParameters(beta,c_R,beta_eff)

    beta_11 = beta_eff/3.0d0
    beta_12 = beta_eff*c_R/3.0d0
    beta_21 = beta_eff*c_R/3.0d0

    S_gpp = 0.0d0

    do mu=1,nd
       do nu=mu+1,nd
          call GetAction(S_gpp,U_xd,mu,nu,beta_11,beta_12,beta_21)
       end do
    end do

    S_gpp = beta_eff*(1.0d0 + 2.0d0*c_R)*nsublattice*nd*(nd-1)/2.0d0 - S_gpp

    call AllSum(S_gpp,S_gauge)


  end function S_gauge

  subroutine GetdS_gbydU(dS_gbydU,U_xd,beta)
    ! begin args: dS_gbydU, U_xd, beta

    type(colour_matrix), dimension(:,:,:,:,:) :: dS_gbydU
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd

    ! begin local_vars
    real(dp) :: beta, c_R, beta_11, beta_12, beta_21, beta_eff
    integer :: mu,nu
    integer :: ix,iy,iz,it

    ! begin execution

    call ActionParameters(beta,c_R,beta_eff)

    beta_11 = beta_eff/6.0d0
    beta_12 = beta_eff*c_R/6.0d0
    beta_21 = beta_eff*c_R/6.0d0

    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dS_gbydU(ix,iy,iz,it,mu)%Cl = 0.0d0
       end do; end do; end do; end do
    end do

    do mu=1,nd
       do nu=mu+1,nd
          call GetStaples(dS_gbydU,U_xd,mu,nu,beta_11,beta_12,beta_21)
       end do
    end do


  end subroutine GetdS_gbydU

  subroutine GetAction(S_g,U_xd,mu,nu,beta_11,beta_12,beta_21)
    ! begin args: S_g, U_xd, mu, nu, beta_11, beta_12, beta_21

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: mu, nu
    ! begin local_vars
    real(dp) :: S_g, beta_11, beta_12, beta_21, S_11, S_12, S_21

    type(colour_matrix) :: U2,UmuUnu,UmudagUnudag,UnuUmudag,T_munu

    integer, dimension(nd) :: dmu, dnu
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt
    integer :: lx,ly,lz,lt
    integer :: ax,ay,az,at
    integer :: bx,by,bz,bt

#define U_mux U_xd(ix,iy,iz,it,mu)
#define U_nux U_xd(ix,iy,iz,it,nu)

#define U_muxpmu U_xd(jx,jy,jz,jt,mu)
#define U_nuxpmu U_xd(jx,jy,jz,jt,nu)

#define U_muxpnu U_xd(kx,ky,kz,kt,mu)
#define U_nuxpnu U_xd(kx,ky,kz,kt,nu)

#define U_muxpmupnu U_xd(lx,ly,lz,lt,mu)
#define U_nuxpmupnu U_xd(lx,ly,lz,lt,nu)

#define U_nuxp2mu U_xd(ax,ay,az,at,nu)
#define U_muxp2nu U_xd(bx,by,bz,bt,mu)

    ! begin execution

    dmu = 0
    dnu = 0

    dmu(mu) = 1
    dnu(nu) = 1

    do it=1,nt
       jt = mapt(it + dmu(4))
       kt = mapt(it + dnu(4))
       lt = mapt(it + dmu(4) + dnu(4))
       at = mapt(it + 2*dmu(4))
       bt = mapt(it + 2*dnu(4))
       do iz=1,nz
          jz = mapz(iz + dmu(3))
          kz = mapz(iz + dnu(3))
          lz = mapz(iz + dmu(3) + dnu(3))
          az = mapz(iz + 2*dmu(3))
          bz = mapz(iz + 2*dnu(3))
          do iy=1,ny
             jy = mapy(iy + dmu(2))
             ky = mapy(iy + dnu(2))
             ly = mapy(iy + dmu(2) + dnu(2))
             ay = mapy(iy + 2*dmu(2))
             by = mapy(iy + 2*dnu(2))
             do ix=1,nx
                jx = mapx(ix + dmu(1))
                kx = mapx(ix + dnu(1))
                lx = mapx(ix + dmu(1) + dnu(1))
                ax = mapx(ix + 2*dmu(1))
                bx = mapx(ix + 2*dnu(1))

                call MultiplyMatMat(UmuUnu,U_mux,U_nuxpmu)
                call MultiplyMatDagMatDag(UmudagUnudag,U_muxpnu,U_nux)
                call RealTraceMultMatMat(S_11,UmuUnu,UmudagUnudag)

                call MultiplyMatMat(U2,U_mux,U_muxpmu)
                call MultiplyMatMatDag(UnuUmudag,U_nuxp2mu,U_muxpmupnu)
                call MultiplyMatMat(T_munu,U2,UnuUmudag)
                call RealTraceMultMatMat(S_21,T_munu,UmudagUnudag)

                call MultiplyMatMat(U2,U_nux,U_nuxpnu)
                call MultiplyMatMatDag(UnuUmudag,U_nuxpmupnu,U_muxp2nu)
                call MultiplyMatMatDag(T_munu,UnuUmudag,U2)
                call RealTraceMultMatMat(S_12,UmuUnu,T_munu)

                S_g = S_g + beta_11*S_11 + beta_21*S_21 + beta_12*S_12

             end do
          end do
       enddo
    end do

#undef U_mu
#undef U_nu

#undef U_muxpmu
#undef U_nuxpmu

#undef U_muxpnu
#undef U_nuxpnu

#undef U_nuxpmupnu
#undef U_muxpmupnu

#undef U_nuxp2mu
#undef U_muxp2nu


  end subroutine GetAction

  subroutine GetStaples(dS_gbydU,U_xd,mu,nu,beta_11,beta_12,beta_21)
    ! begin args: dS_gbydU, U_xd, mu, nu, beta_11, beta_12, beta_21

    type(colour_matrix), dimension(:,:,:,:,:) :: dS_gbydU
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: mu, nu
    ! begin local_vars
    real(dp) :: S_g, beta_11, beta_12, beta_21

    type(colour_matrix) :: U2,UmuUnu,UnuUmu,UmudagUnudag,UnuUmudag,UnudagUmudag,UnudagUmu,UmuUnudag
    type(colour_matrix) :: T_xp, T_xm, R_xp, R_xm, V_muxp, V_muxm, V_nuxp, V_nuxm

    integer, dimension(nd) :: dmu, dnu
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: kx,ky,kz,kt
    integer :: lx,ly,lz,lt
    integer :: mx,my,mz,mt
    integer :: ax,ay,az,at
    integer :: bx,by,bz,bt
    integer :: cx,cy,cz,ct
    integer :: dx,dy,dz,dt
    integer :: ex,ey,ez,et
    integer :: fx,fy,fz,ft
    integer :: gx,gy,gz,gt
    integer :: hx,hy,hz,ht
    integer :: ox,oy,oz,ot
    integer :: px,py,pz,pt
    integer :: qx,qy,qz,qt
    integer :: rx,ry,rz,rt

#define U_mux        U_xd(ix,iy,iz,it,mu)
#define U_nux        U_xd(ix,iy,iz,it,nu)
#define dS_gbydU_mux dS_gbydU(ix,iy,iz,it,mu)
#define dS_gbydU_nux dS_gbydU(ix,iy,iz,it,nu)
#define U_muxpmu     U_xd(jx,jy,jz,jt,mu)
#define U_nuxpmu     U_xd(jx,jy,jz,jt,nu)
#define U_muxpnu     U_xd(kx,ky,kz,kt,mu)
#define U_nuxpnu     U_xd(kx,ky,kz,kt,nu)
#define U_muxmmu     U_xd(lx,ly,lz,lt,mu)
#define U_nuxmmu     U_xd(lx,ly,lz,lt,nu)
#define U_muxmnu     U_xd(mx,my,mz,mt,mu)
#define U_nuxmnu     U_xd(mx,my,mz,mt,nu)

#define U_nuxp2mu    U_xd(ax,ay,az,at,nu)
#define U_muxp2nu    U_xd(bx,by,bz,bt,mu)
#define U_nuxm2mu    U_xd(cx,cy,cz,ct,nu)
#define U_muxm2mu    U_xd(cx,cy,cz,ct,mu)
#define U_nuxm2nu    U_xd(dx,dy,dz,dt,nu)
#define U_muxm2nu    U_xd(dx,dy,dz,dt,mu)
#define U_nuxpmupnu  U_xd(ex,ey,ez,et,nu)
#define U_muxpmupnu  U_xd(ex,ey,ez,et,mu)
#define U_nuxpmumnu  U_xd(fx,fy,fz,ft,nu)
#define U_muxpmumnu  U_xd(fx,fy,fz,ft,mu)
#define U_nuxmmupnu  U_xd(gx,gy,gz,gt,nu)
#define U_muxmmupnu  U_xd(gx,gy,gz,gt,mu)
#define U_nuxmmumnu  U_xd(hx,hy,hz,ht,nu)
#define U_muxmmumnu  U_xd(hx,hy,hz,ht,mu)
#define U_nuxp2mumnu U_xd(ox,oy,oz,ot,nu)
#define U_muxmmup2nu U_xd(px,py,pz,pt,mu)
#define U_nuxm2nupmu U_xd(qx,qy,qz,qt,nu)
#define U_muxm2mupnu U_xd(rx,ry,rz,rt,mu)

    ! begin execution

    dmu = 0
    dnu = 0

    dmu(mu) = 1
    dnu(nu) = 1

    do it=1,nt
       jt = mapt(it + dmu(4))
       kt = mapt(it + dnu(4))
       lt = mapt(it - dmu(4))
       mt = mapt(it - dnu(4))
       at = mapt(it + 2*dmu(4))
       bt = mapt(it + 2*dnu(4))
       ct = mapt(it - 2*dmu(4))
       dt = mapt(it - 2*dnu(4))
       et = mapt(it + dmu(4) + dnu(4))
       ft = mapt(it + dmu(4) - dnu(4))
       gt = mapt(it - dmu(4) + dnu(4))
       ht = mapt(it - dmu(4) - dnu(4))
       ot = mapt(it + 2*dmu(4) - dnu(4))
       pt = mapt(it - dmu(4) + 2*dnu(4))
       qt = mapt(it + dmu(4) - 2*dnu(4))
       rt = mapt(it - 2*dmu(4) + dnu(4))

       do iz=1,nz
          jz = mapz(iz + dmu(3))
          kz = mapz(iz + dnu(3))
          lz = mapz(iz - dmu(3))
          mz = mapz(iz - dnu(3))
          az = mapz(iz + 2*dmu(3))
          bz = mapz(iz + 2*dnu(3))
          cz = mapz(iz - 2*dmu(3))
          dz = mapz(iz - 2*dnu(3))
          ez = mapz(iz + dmu(3) + dnu(3))
          fz = mapz(iz + dmu(3) - dnu(3))
          gz = mapz(iz - dmu(3) + dnu(3))
          hz = mapz(iz - dmu(3) - dnu(3))
          oz = mapz(iz + 2*dmu(3) - dnu(3))
          pz = mapz(iz - dmu(3) + 2*dnu(3))
          qz = mapz(iz + dmu(3) - 2*dnu(3))
          rz = mapz(iz - 2*dmu(3) + dnu(3))

          do iy=1,ny
             jy = mapy(iy + dmu(2))
             ky = mapy(iy + dnu(2))
             ly = mapy(iy - dmu(2))
             my = mapy(iy - dnu(2))
             ay = mapy(iy + 2*dmu(2))
             by = mapy(iy + 2*dnu(2))
             cy = mapy(iy - 2*dmu(2))
             dy = mapy(iy - 2*dnu(2))
             ey = mapy(iy + dmu(2) + dnu(2))
             fy = mapy(iy + dmu(2) - dnu(2))
             gy = mapy(iy - dmu(2) + dnu(2))
             hy = mapy(iy - dmu(2) - dnu(2))
             oy = mapy(iy + 2*dmu(2) - dnu(2))
             py = mapy(iy - dmu(2) + 2*dnu(2))
             qy = mapy(iy + dmu(2) - 2*dnu(2))
             ry = mapy(iy - 2*dmu(2) + dnu(2))

             do ix=1,nx
                jx = mapx(ix + dmu(1))
                kx = mapx(ix + dnu(1))
                lx = mapx(ix - dmu(1))
                mx = mapx(ix - dnu(1))
                ax = mapx(ix + 2*dmu(1))
                bx = mapx(ix + 2*dnu(1))
                cx = mapx(ix - 2*dmu(1))
                dx = mapx(ix - 2*dnu(1))
                ex = mapx(ix + dmu(1) + dnu(1))
                fx = mapx(ix + dmu(1) - dnu(1))
                gx = mapx(ix - dmu(1) + dnu(1))
                hx = mapx(ix - dmu(1) - dnu(1))
                ox = mapx(ix + 2*dmu(1) - dnu(1))
                px = mapx(ix - dmu(1) + 2*dnu(1))
                qx = mapx(ix + dmu(1) - 2*dnu(1))
                rx = mapx(ix - 2*dmu(1) + dnu(1))


                !    _                               _
                !   | | +     [ V_muxp +        ] :   |
                !         |_| [          V_muxm ] :  _|

                call MultiplyMatMatDag(UnuUmudag,U_nuxpmu,U_muxpnu)
                call MultiplyMatDagMatDag(UnudagUmudag,U_nuxpmumnu,U_muxmnu)

                call MultiplyMatMatDag(V_muxp,UnuUmudag,U_nux)
                call MultiplyMatMat(V_muxm,UnudagUmudag,U_nuxmnu)

                dS_gbydU_mux%Cl = dS_gbydU_mux%Cl - beta_11*(V_muxp%Cl + V_muxm%Cl)

                !  _   _                         _
                ! |_ + _| [ V_nuxm + V_nuxp ] : | .
                !

                call MultiplyMatDagMatDag(UmudagUnudag,U_muxmmupnu,U_nuxmmu)
                call MultiplyMatMat(V_nuxm,UmudagUnudag,U_muxmmu)

                call MultiplyMatDagMatDag(V_nuxp,UnuUmudag,U_mux)

                dS_gbydU_nux%Cl = dS_gbydU_nux%Cl - beta_11*(V_nuxp%Cl + V_nuxm%Cl)

                !  __     __    __
                ! | _| + |_ | :  _|
                !

                call MultiplyMatMat(UmuUnu,U_muxpmu,U_nuxp2mu)
                call MultiplyMatMat(U2,U_muxpnu,U_muxpmupnu)
                call MultiplyMatMatDag(T_xp,UmuUnu,U2)

                call MultiplyMatMatDag(R_xp,T_xp,U_nux)
                call MultiplyMatMat(R_xm,UnuUmudag,V_nuxm)

                dS_gbydU_mux%Cl = dS_gbydU_mux%Cl - beta_21*(R_xp%Cl + R_xm%Cl)

                !  __   __
                ! |__ + __|
                !

                call MultiplyMatMat(U2,U_muxm2mu,U_muxmmu)
                call MultiplyMatMat(UnuUmu,U_nuxm2mu,U_muxm2mupnu)
                call MultiplyMatDagMat(T_xm,UnuUmu,U2)

                call MultiplyMatDagMat(R_xm,U_muxmmupnu,T_xm)
                call MultiplyMatDagMatDag(R_xp,T_xp,U_mux)

                dS_gbydU_nux%Cl = dS_gbydU_nux%Cl - beta_12*(R_xp%Cl + R_xm%Cl)

                !  _            _
                ! | | +  _   : | |
                !  _|     |  :   |
                !       |_|

                call MultiplyMatMat(UnuUmu,U_nuxpnu,U_muxp2nu)
                call MultiplyMatMat(U2,U_nuxpmu,U_nuxpmupnu)
                call MultiplyMatMatDag(T_xp,UnuUmu,U2)

                call MultiplyMatMatDag(R_xp,T_xp,U_mux)
                call MultiplyMatDagMat(R_xm,UnuUmudag,V_muxm)

                dS_gbydU_nux%Cl = dS_gbydU_nux%Cl - beta_21*(R_xp%Cl + R_xm%Cl)

                !  _
                ! | |
                ! | |
                !     + | |
                !       |_|

                call MultiplyMatMat(U2,U_nuxm2nu,U_nuxmnu)
                call MultiplyMatMat(UmuUnu,U_muxm2nu,U_nuxm2nupmu)
                call MultiplyMatDagMat(T_xm,UmuUnu,U2)

                call MultiplyMatDagMatDag(R_xp,T_xp,U_nux)
                call MultiplyMatDagMat(R_xm,U_nuxpmumnu,T_xm)

                dS_gbydU_mux%Cl = dS_gbydU_mux%Cl - beta_12*(R_xp%Cl + R_xm%Cl)

                !        _
                !  _    | |
                ! |   + |_  :   .
                ! |_|       : |_

                call MultiplyMatDagMat(UnudagUmu,U_nuxmmumnu,U_muxmmumnu)
                call MultiplyMatMat(T_xm,UmudagUnudag,UnudagUmu)

                call MultiplyMatMatDag(UnuUmudag,U_nuxpnu,U_muxmmup2nu)
                call MultiplyMatMat(U2,U_nuxmmu,U_nuxmmupnu)
                call MultiplyMatMatDag(T_xp,UnuUmudag,U2)

                call MultiplyMatMat(R_xm,T_xm,U_nuxmnu)
                call MultiplyMatMat(R_xp,T_xp,U_muxmmu)

                dS_gbydU_nux%Cl = dS_gbydU_nux%Cl - beta_21*(R_xp%Cl + R_xm%Cl)

                !
                !   _  +  _
                ! |__|   |__|

                call MultiplyMatMatDag(UmuUnudag,U_muxpmu,U_nuxp2mumnu)
                call MultiplyMatMat(U2,U_muxmnu,U_muxpmumnu)
                call MultiplyMatMatDag(T_xp,UmuUnudag,U2)

                call MultiplyMatMatDag(T_xm,UnudagUmudag,UnudagUmu)

                call MultiplyMatMat(R_xp,T_xp,U_nuxmnu)
                call MultiplyMatMat(R_xm,T_xm,U_muxmmu)

                dS_gbydU_mux%Cl = dS_gbydU_mux%Cl - beta_21*(R_xp%Cl + R_xm%Cl)

             end do
          end do
       enddo
    end do


  end subroutine GetStaples

end module GaugeAction


