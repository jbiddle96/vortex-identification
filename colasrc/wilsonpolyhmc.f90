/* wilson.h */


module wilsonPolyHMC

  use wilsonFermionMD
  use VectorAlgebra
  use FermionAlgebra
  use ConjgradSolvers
  use ZolotarevApprox
  use StoutLinks
  use FatLinks
  use LatticeSize
  use SpinorTypes
  use FermionTypes
  use ConjGradSolvers
  use wilsonMatrix
  use ColourTypes
  use Kinds
  use FermionField
  implicit none
  private
  integer, public :: n_poly2f, n_poly1f
  integer, public :: n_q2f, n_p2f
  integer, public :: n_q1f, n_p1f
  real(dp), public :: mu_p, nu_p
  real(dp), public :: eps_poly2f, eps_poly1f, lambda_p
  logical, public :: poly_filter = .false.
  real(dp), public :: c_poly2f, c_poly1f
  real(dp), public :: c_p2f, c_q2f
  real(dp), public :: c_p1f, c_q1f
  complex(dc), dimension(:), allocatable, public :: z_poly2f, zz_poly2f, z_poly1f
  complex(dc), dimension(:), allocatable, public :: a_poly2f, aa_poly2f
  complex(dc), dimension(:), allocatable, public :: b_poly2f, bb_poly2f
  complex(dc), dimension(:), allocatable, public :: z_q2f, z_p2f
  complex(dc), dimension(:), allocatable, public :: z_q1f, z_p1f
  complex(dc), dimension(:), allocatable, public :: a_md, b_md, c_md
  complex(dc), dimension(:), allocatable, public :: a_mc, b_mc, c_mc
  interface MultiplyM_poly2f
     module procedure PolyMultiply
  end interface
  interface MultiplyM_poly1f
     module procedure PolyMultiply
  end interface
  interface GetPolynomialField_2f
     module procedure GetPolynomialField
  end interface
  interface GetPolynomialField_1f
     module procedure GetPolynomialField
  end interface
  interface GetdS_polybydU_2f
     module procedure GetdS_polybydU
  end interface
  interface GetdS_polybydU_1f
     module procedure GetdS_polybydU
  end interface
  !! Export these two quantities?
  public :: PolyMultiply
  public :: GetPolynomialField
  public :: MultiplyM_poly2f
  public :: MultiplyM_poly1f
  public :: GetPolynomialField_2f
  public :: GetPolynomialField_1f
  public :: Init2PFPolyFilter
  public :: Init1PFPolyFilter
  public :: GetPseudofermionField_twopf_poly
  public :: GetPseudofermionField_onepf_poly
  public :: MultiplyM_twopf_poly
  public :: MultiplyM_onepf_poly
  public :: InvPolyMultiply
  public :: GetdS_polybydU_2f
  public :: GetdS_polybydU_1f
  public :: GetdS_twopfbydU_poly
  public :: GetdS_onepfbydU_poly
contains
  subroutine Init2PFPolyFilter(n_poly,n_p,n_q,mu_p,nu_p,kappa)
    ! begin args: n_poly, n_p, n_q, mu_p, nu_p, kappa
    integer :: n_poly,n_p,n_q
    real(dp) :: mu_p,nu_p, kappa
    ! begin local_vars
    real(dp) :: theta_k, d, c_n
    integer :: k, k_p, k_q
    integer :: n_tot, ratio
    ! begin execution
    do k=1,n_poly
       theta_k = (2*pi*k)/(n_poly+1)
       !z_poly2f(k) = 0.5d0*(1.0d0+eps_poly)*( 1.0_dp - cos(theta_k) ) - sqrt(eps_poly)*i*sin(theta_k)
       z_poly2f(k) = mu_p*( 1.0_dp - cos(theta_k) ) - sqrt(mu_p**2 - nu_p**2)*i*sin(theta_k)
       !z_poly2f(k) = 1.0d0 - sqrt(eps_poly)*i*sin(theta_k)
       !z_poly2f(k) = -theta_k !+ i*theta_k
       !z_poly2f(k) = -theta_k !+ i*theta_k
    end do
! do k=n_poly/2+1,n_poly
       !z_poly2f(k) = -0.5d0*(1.0d0+eps_poly)*( 1.0_dp - cos(theta_k) ) - sqrt(eps_poly)*i*sin(theta_k)
       !z_poly2f(k) = 1.0d0 - sqrt(eps_poly)*i*sin(theta_k)
! z_poly2f(k) = conjg(z_poly2f(k-n_poly/2))
! end do
! c_poly2f = 1.0d0
    !lambda = 1.0d0
! lambda = 1.0d0 + 8*kappa
! lambda = 1.0d0 + 64*kappa**2
! lambda = sqrt(1.0d0 + 64*kappa**2)
    z_poly2f = lambda_p*z_poly2f
    !d = 0.5d0*(1.0_dp+eps_poly)
    d = mu_p
    c_poly2f = 1.0_dp/(d*product(d-z_poly2f(1:n_poly)))
    if ( n_q > 0 ) then
       ratio = ( n_poly + 1 ) / (n_p + 1)
       k_p = 1
       k_q = 1
       do k=1,n_poly
          if ( modulo(k,ratio) == 0 ) then
             z_p2f(k_p) = z_poly2f(k)
             k_p = k_p + 1
          else
             z_q2f(k_q) = z_poly2f(k)
             k_q = k_q + 1
          end if
       end do
       c_p2f = 1.0_dp/(d*product(d-z_p2f(1:n_p2f)))
       c_q2f = c_poly2f/c_p2f
    else
       c_p2f = c_poly2f
       z_p2f = z_poly2f
    end if
    zz_poly2f(1:n_poly) = z_poly2f
    zz_poly2f(n_poly+1) = 0.0d0
    !zz_poly2f(n_poly+1) = -0.1d0 + i*0.2d0
    !zz_poly2f(2:n_poly+1) = z_poly2f
    !zz_poly2f(1) = 0.0d0
  end subroutine Init2PFPolyFilter
  subroutine Init1PFPolyFilter(n_poly,n_p,n_q,mu_p,nu_p,kappa)
    ! begin args: n_poly, n_p, n_q, mu_p, nu_p, kappa
    integer :: n_poly,n_p,n_q
    real(dp) :: mu_p,nu_p, kappa
    ! begin local_vars
    real(dp) :: theta_k, d, c_n
    integer :: k, k_p, k_q
    integer :: n_tot, ratio
    ! begin execution
    do k=1,n_poly
       theta_k = (2*pi*k)/(n_poly+1)
       !z_poly1f(k) = 0.5d0*(1.0d0+eps_poly)*( 1.0_dp - cos(theta_k) ) - sqrt(eps_poly)*i*sin(theta_k)
       z_poly1f(k) = mu_p*( 1.0_dp - cos(theta_k) ) - sqrt(mu_p**2 - nu_p**2)*i*sin(theta_k)
       !z_poly1f(k) = 1.0d0 - sqrt(eps_poly)*i*sin(theta_k)
       !z_poly1f(k) = -theta_k !+ i*theta_k
       !z_poly1f(k) = -theta_k !+ i*theta_k
    end do
! do k=n_poly/2+1,n_poly
       !z_poly1f(k) = -0.5d0*(1.0d0+eps_poly)*( 1.0_dp - cos(theta_k) ) - sqrt(eps_poly)*i*sin(theta_k)
       !z_poly1f(k) = 1.0d0 - sqrt(eps_poly)*i*sin(theta_k)
! z_poly1f(k) = conjg(z_poly1f(k-n_poly/2))
! end do
! c_poly1f = 1.0d0
    !lambda = 1.0d0
! lambda = 1.0d0 + 8*kappa
! lambda = 1.0d0 + 64*kappa**2
! lambda = sqrt(1.0d0 + 64*kappa**2)
    z_poly1f = lambda_p*z_poly1f
    !d = 0.5d0*(1.0_dp+eps_poly)
    d = mu_p
    c_poly1f = 1.0_dp/(d*product(d-z_poly1f(1:n_poly)))
    if ( n_q > 0 ) then
       ratio = ( n_poly + 1 ) / (n_p + 1)
       k_p = 1
       k_q = 1
       do k=1,n_poly
          if ( modulo(k,ratio) == 0 ) then
             z_p1f(k_p) = z_poly1f(k)
             k_p = k_p + 1
          else
             z_q1f(k_q) = z_poly1f(k)
             k_q = k_q + 1
          end if
       end do
       c_p1f = 1.0_dp/(d*product(d-z_p1f(1:n_p1f)))
       c_q1f = c_poly1f/c_p1f
    else
       c_p1f = c_poly1f
       z_p1f = z_poly1f
    end if
    !zz_poly1f(1:n_poly) = z_poly1f
    !zz_poly1f(n_poly+1) = 0.0d0
    !zz_poly1f(n_poly+1) = -0.1d0 + i*0.2d0
    !zz_poly1f(2:n_poly+1) = z_poly1f
    !zz_poly1f(1) = 0.0d0
  end subroutine Init1PFPolyFilter
/*
!!  subroutine Init1PFPolyFilter(n_poly,n_p,n_q,eps_poly,kappa)
!!    ! begin args: n_poly, n_p, n_q, eps_poly, kappa
!!
!!    integer :: n_poly,n_p,n_q
!!    real(dp) :: eps_poly, kappa
!!    ! begin local_vars
!!    real(dp) :: theta_k, d, c_n, lambda
!!    integer :: k, k_p, k_q
!!    integer :: n_tot, ratio
!!
!!    ! begin execution
!!
!!    do k=1,n_poly
!!       theta_k = (2*pi*k)/(2*n_poly+1)
!!       z_poly1f(k) = 0.5d0*(1.0d0+eps_poly)*( 1.0_dp - cos(theta_k) ) - sqrt(eps_poly)*i*sin(theta_k)
!!    end do
!!
!!    lambda = 1.0d0
!!!    lambda = 1.0d0 + 8*kappa
!!!    lambda = 1.0d0 + 64*kappa**2
!!!    lambda = sqrt(1.0d0 + 64*kappa**2)
!!    z_poly1f = lambda*z_poly1f
!!    d = 0.5d0*(1.0_dp+eps_poly)
!!    c_poly1f = 1.0_dp/sqrt( d * product((d-z_poly1f)*(d-conjg(z_poly1f))) )
!!
!!    if ( n_q > 0 ) then
!!       ratio = ( n_poly + 1 ) / (n_p + 1)
!!       k_p = 1
!!       k_q = 1
!!       do k=1,n_poly
!!          if ( modulo(k,ratio) == 0 ) then
!!             z_p1f(k_p) = z_poly1f(k)
!!             k_p = k_p + 1
!!          else
!!             z_q1f(k_q) = z_poly1f(k)
!!             k_q = k_q + 1
!!          end if
!!       end do
!!
!!       c_p1f = 1.0_dp/(d*product(d-z_p1f(1:n_p1f)))
!!       c_q1f = c_poly1f/c_p1f
!!
!!    else
!!       c_p1f = c_poly1f
!!       z_p1f = z_poly1f
!!
!!    end if
!!
!!
!!  end subroutine Init1PFPolyFilter
*/
  subroutine GetPolynomialField(phiP,c_n,z_k)
    ! begin args: phiP, c_n, z_k
    type(colour_vector), dimension(:,:,:,:,:) :: phiP
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: xi
    integer :: n_p
    !We wish to generate phi according to the distribution e^(-S_poly) = e^(-phi* P(H^2) phi )
    !Assume P(H^2) = Q(H^2)Q*(H^2). Note that xi = Q(H^2) phi is Gaussian distributed,
    !P(xi) = e^-(xi* xi) and therefore easily generated, and hence set phi = Q(H^2)^-1 xi.
    ! begin execution
    allocate(xi(nxp,nyp,nzp,ntp,ns))
    n_p = size(z_k)
    call ComplexGaussianField(xi)
    call InvPolyMultiply(xi,phiP,sqrt(c_n),z_k(1:n_p/2))
    deallocate(xi)
  end subroutine GetPolynomialField
  subroutine GetPseudofermionField_twopf_poly(phi2f,c_n,z_k)
    ! begin args: phi2f, c_n, z_k
    type(colour_vector), dimension(:,:,:,:,:) :: phi2f
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: xi, Pxi
    integer :: n_p
    !We wish to generate phi according to the distribution e^(-S_2pf_poly) = e^(-phi* [H^2 P(H^2)]^-1 phi )
    !Assume P(H^2) = Q(H^2)Q*(H^2). Note that xi = [H Q(H^2)]^-1 phi is Gaussian distributed,
    !P(xi) = e^-(xi* xi) and therefore easily generated, and hence set phi = H Q(H^2) xi.
    ! begin execution
    allocate(xi(nxp,nyp,nzp,ntp,ns))
    allocate(Pxi(nxp,nyp,nzp,ntp,ns))
    call ComplexGaussianField(xi)
    n_p = size(z_k)
    call PolyMultiply(xi,Pxi,sqrt(c_n),conjg(z_k(1:n_p/2)))
    call Hwilson(Pxi,phi2f)
    !norm_phi = fermion_norm(xi)
    !call normalise(xi,norm_phi)
    !call PolyMultiply(xi,Pxi,c_poly2f,zz_poly2f)
    !call PsiMinusPhi(Pxi,xi)
    !poly_error = fermion_norm(Pxi)
    deallocate(xi)
    deallocate( Pxi)
  end subroutine GetPseudofermionField_twopf_poly
  subroutine GetPseudofermionField_onepf_poly(phi1f,d_n,a_k,c_k,c_n,z_k)
    ! begin args: phi1f, d_n, a_k, c_k, c_n, z_k
    type(colour_vector), dimension(:,:,:,:,:) :: phi1f
    real(dp) :: d_n
    real(dp), dimension(:) :: a_k,c_k
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: xi, Pxi
    integer :: n_p
    complex(dc), dimension(size(c_k)) :: p_k, q_k, r_k
    integer, dimension(size(c_k)) :: iterations_i
    integer :: i_v
    integer :: j,k,n_poles
    !We wish to generate phi according to the distribution e^(-S_1pf_poly) = e^(-phi* P(H^2)^-1 R(H^2) phi )
    !Assume P(H^2) = Q(H^2)Q*(H^2). R(x) is a rational approximation to the inverse square root.
    !R(x^2) can be factored R(x^2) = S(x)* S(x), and hence xi = Q(H^2)^-1 S(H^2) phi is Gaussian distributed.
    !So we set phi = T(H) xi, where T(x) = 1/S(x) is another rational polynomial.
    ! begin execution
    allocate(xi(nxp,nyp,nzp,ntp,ns))
    allocate(Pxi(nxp,nyp,nzp,ntp,ns))
    call ComplexGaussianField(xi)
    n_p = size(z_k)
    call PolyMultiply(xi,Pxi,sqrt(c_n),conjg(z_k(1:n_p/2)))
    ! R(x^2) = d_n \prod (x^2 + a_k)/(x^2 + c_k) = S(x)*S(x), where
    ! S(x) = sqrt(d_n) \prod (x + i sqrt(a_k) )/(x + i\sqrt(c_k))
    n_poles = size(a_k)
    p_k = i*sqrt(c_k)
    q_k = i*sqrt(a_k)
    ! Convert the rational polynomial S(x)^-1 to a sum over poles.
    do k=1,n_poles
       r_k(k) = 1.0_dp/sqrt(d_n)
       do j=1,n_poles
          r_k(k) = r_k(k)*(p_k(j)-q_k(k))
          if ( j /= k ) r_k(k) = r_k(k)/(q_k(j)-q_k(k))
       end do
    end do
    call MultiCRInvert(n_poles, Pxi, chi_i( :,:,:,:,: ,1:n_poles), q_k, tolerance_cg, iterations_i, Hwilson)
    call MultiplyAlphaPhi(phi1f,1.0_dp/sqrt(d_n),Pxi)
    do i_v=1,n_poles
       call PsiPlusAlphaPhi(phi1f,r_k(i_v),chi_i( :,:,:,:,: ,i_v))
    end do
    deallocate(xi)
    deallocate(Pxi)
  end subroutine GetPseudofermionField_onepf_poly
  subroutine MultiplyM_twopf_poly(phi,eta,c_n,z_k)
    ! begin args: phi, eta, c_n, z_k
    type(colour_vector), dimension(:,:,:,:,:) :: phi, eta
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    ! begin local_vars
    integer :: n_p
    complex(dc), dimension(size(z_k)+1) :: zz_k
    ! begin execution
    n_p = size(z_k)
    zz_k(1:n_p) = z_k(:)
    zz_k(n_p+1) = 0.0d0
    call InvPolyMultiply(phi,eta,c_n,zz_k)
  end subroutine MultiplyM_twopf_poly
  subroutine MultiplyM_onepf_poly(phi,eta,d_n,a_k,c_k,c_n,z_k)
    ! begin args: phi, eta, d_n, a_k, c_k, c_n, z_k
    type(colour_vector), dimension(:,:,:,:,:) :: phi, eta
    real(dp) :: d_n
    real(dp), dimension(:) :: a_k,c_k
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    ! begin local_vars
    integer :: i_v,j_v,n_poly,n_zolo,n_poles,j,k
    complex(dc), dimension(1:size(z_k)+size(c_k)) :: p_i,q_i
    integer :: iterations_i(1:size(z_k)+size(c_k))
    ! begin execution
    n_poly = size(z_k)
    n_zolo = size(c_k)
    n_poles = n_poly + n_zolo
    q_i(1:n_poly/2) = -z_k(1:n_poly)
    q_i(n_poly/2+1:n_poly) = -conjg(z_k(1:n_poly/2))
    q_i(n_poly+1:n_poles) = c_k(1:n_zolo)
    ! Convert the rational polynomial to a sum over poles.
    do k=1,n_poles
       p_i(k) = d_n / c_n
       do j=1,n_poles
          if ( j <= n_zolo ) p_i(k) = p_i(k)*(a_k(j)-q_i(k))
          if ( j /= k ) p_i(k) = p_i(k)/(q_i(j)-q_i(k))
       end do
    end do
    call MultiCGInvert_z(n_poles, phi, chi_i(:,:,:,:,:,1:n_poles), q_i, tolerance_cg, iterations_i, Hsqwilson)
    iter_multicg = maxval(iterations_i)
    ! The introduction of the polynomial filter into the denominator ensures
    ! that the denominator is of greater degree than the numerator in the
    ! rational polynomial, and hence there is no residue term.
    eta = zero_vector
    do i_v=1,n_poles
       call PsiPlusAlphaPhi(eta,p_i(i_v),chi_i( :,:,:,:,: ,i_v))
    end do
  end subroutine MultiplyM_onepf_poly
  subroutine PolyMultiply(psi,chi,c_n,z_k)
    ! begin args: psi, chi, c_n, z_k
    ! Calculates P(D) = c_n \prod_k (D - z_k)
    type(colour_vector), dimension(:,:,:,:,:) :: psi, chi
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Mchi
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    integer :: k, n_poly
    ! begin execution
    allocate(Mchi(nxp,nyp,nzp,ntp,ns))
    n_poly = size(z_k)
    chi = psi
    do k=1,n_poly
       call Hsqwilson(chi,Mchi)
       call PsiMinusAlphaPhi(Mchi,z_k(k),chi)
       chi = Mchi
    end do
    call AlphaPsi(chi,c_n)
    deallocate(Mchi)
  end subroutine PolyMultiply
  subroutine InvPolyMultiply(psi,chi,c_n,z_k)
    ! begin args: psi, chi, c_n, z_k
    ! Calculates inverse of P(D) = c_n \prod_k (D - z_k)
    type(colour_vector), dimension(:,:,:,:,:) :: psi, chi
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    ! begin local_vars
    integer :: j,k, n_poly
    complex(dc), dimension(1:size(z_k)) :: a_k, b_k
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Dpsi
    complex(dc) :: dot
    !real(dp), dimension(1:size(z_k)) :: a_k, b_k
    integer :: ix,iy,iz,it,is,i_v
    integer :: iterations_i(1:size(z_k))
    complex(dc) :: b_0
    ! begin execution
    allocate(Dpsi(nxp,nyp,nzp,ntp,ns))
    n_poly = size(z_k)
    b_k(1:n_poly) = -z_k(1:n_poly)
    do k=1,n_poly
       a_k(k) = 1.0d0 / c_n
       do j=1,n_poly
          if ( j /= k ) a_k(k) = a_k(k)/(b_k(j)-b_k(k))
       end do
    end do
    call MultiCGInvert_z(n_poly, psi, chi_i(:,:,:,:,:,1:n_poly), b_k, tolerance_cg, iterations_i, Hsqwilson)
    iter_multicg = maxval(iterations_i)
    !if (i_am_root) print *, n_poly, iter_multicg, b_k, a_k
    !if (i_am_root) print *, n_poly, iter_multicg, iterations_i
    chi = zero_vector
    do i_v=1,n_poly
       call PsiPlusAlphaPhi(chi,a_k(i_v),chi_i(:,:,:,:,:,i_v))
    end do
    deallocate(Dpsi)
  end subroutine InvPolyMultiply
  subroutine GetdS_polybydU(dS_polybydU,U_xd,phi,P_phi,c_n,z_k)
    ! begin args: dS_polybydU, U_xd, phi, P_phi, c_n, z_k
    type(colour_matrix), dimension(:,:,:,:,:) :: dS_polybydU
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phi, P_phi
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dSbydU
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi,Mchi,eta
    integer :: ix,iy,iz,it,is,i_v, j_v
    integer :: mu, isweeps, tsum
    integer :: k,n_poly
    real(dp) :: t0, t1, t00
    ! begin execution
    allocate(dSbydU(nxp,nyp,nzp,ntp,nd))
    allocate(chi(nxp,nyp,nzp,ntp,ns))
    allocate(Mchi(nxp,nyp,nzp,ntp,ns))
    allocate(eta(nxp,nyp,nzp,ntp,ns))
    dSbydU = zero_matrix
    n_poly = size(z_k)
    chi = phi
    chi_i(:,:,:,:,:,n_poly) = chi
    do i_v=1,n_poly-1
       j_v = n_poly-i_v+1
       call Hsqwilson(chi,Mchi)
       call AlphaPsiPlusPhi(chi,-z_k(j_v),Mchi)
       chi_i(:,:,:,:,:,j_v-1) = chi
    end do
    call MultiplyAlphaPhi(eta,c_n,P_phi)
    chi = chi_i(:,:,:,:,:,1)
    call dS_wilsonbydU(dSbydU,eta,chi,.true.)
    do i_v=2,n_poly
       call Hsqwilson(eta,Mchi)
       call AlphaPsiPlusPhi(eta,-conjg(z_k(i_v-1)),Mchi)
       chi = chi_i(:,:,:,:,:,i_v)
       call dS_wilsonbydU(dSbydU,eta,chi,.true.)
    end do
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dS_polybydU(ix,iy,iz,it,mu)%Cl = dS_polybydU(ix,iy,iz,it,mu)%Cl + dSbydU(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do
    deallocate(dSbydU)
    deallocate(chi)
    deallocate(Mchi)
    deallocate(eta)
  end subroutine GetdS_polybydU
/*
!!  subroutine GetdS_polybydU_1f(dS_polybydU,U_xd,phi,c_n,z_k)
!!    ! begin args: dS_polybydU, U_xd, phi, c_n, z_k
!!
!!    type(colour_matrix), dimension(:,:,:,:,:) :: dS_polybydU
!!    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
!!    _fermion_field_type_ :: phi
!!    real(dp) :: c_n
!!    complex(dc), dimension(:) :: z_k
!!
!!    ! begin local_vars
!!    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: _dS_links_
!!    _fermion_field_type_, allocatable :: chi,Mchi,eta
!!
!!    integer :: ix,iy,iz,it,is,i_v, j_v
!!    integer :: mu, isweeps, tsum
!!
!!    integer :: k,n_poly
!!    real(dp) :: t0, t1, t00
!!
!!#define dS_polybydU_mux dS_polybydU(ix,iy,iz,it,mu)
!!#define dSbydU_mux dSbydU(ix,iy,iz,it,mu)
!!#define dSbydUsm_mux dSbydUsm(ix,iy,iz,it,mu)
!!
!!    ! begin execution
!!    allocate(dSbydU(nxp,nyp,nzp,ntp,nd))
!!    allocate(chi(_fermion_field_extents_))
!!    allocate(Mchi(_fermion_field_extents_))
!!    allocate(eta(_fermion_field_extents_))
!!
!!    dSbydU = zero_matrix
!!#ifdef _split_links_
!!    allocate(dSbydUsm(nxp,nyp,nzp,ntp,nd))
!!    dSbydUsm = zero_matrix
!!#endif
!!#ifdef _clover_
!!    Omega = zero_matrix
!!#endif
!!
!!    n_poly = size(z_k)
!!
!!    chi_i = _zero_fermion_vector_
!!
!!    chi = phi
!!    chi_i(_fermion_colons_,n_poly) = phi
!!    do i_v=1,n_poly-1
!!       j_v = n_poly-i_v+1
!!       call Hsqwilson(chi,Mchi)
!!       call AlphaPsiPlusPhi(chi,-z_k(j_v),Mchi)
!!       chi_i(_fermion_colons_,j_v-1) = chi
!!    end do
!!
!!    call MultiplyAlphaPhi(eta,c_n,phi)
!!    chi = chi_i(_fermion_colons_,1)
!!    call dS_wilsonbydU(_dS_links_,eta,chi)
!!
!!    do i_v=2,n_poly
!!       call Hsqwilson(eta,Mchi)
!!       call AlphaPsiPlusPhi(eta,-conjg(z_k(i_v-1)),Mchi)
!!
!!       chi = chi_i(_fermion_colons_,i_v)
!!       call dS_wilsonbydU(_dS_links_,eta,chi)
!!    end do
!!
!!#ifdef _clover_
!!    call dFbydU(_dSbydU_clover_,_U_clover_)
!!#endif
!!
!!#ifdef _smeared_links_
!!    do isweeps=smear_sweeps,1,-1
!!
!!#ifdef _stout_smearing_
!!       call dU_prbydU_stout(_dS_smear_,Un_xd(:,:,:,:,:,isweeps),alpha_smear)
!!#endif
!!#ifdef _ape_smearing_
!!       call dU_prbydU_ape(_dS_smear_,Un_xd(:,:,:,:,:,isweeps),alpha_smear)
!!#endif
!!    end do
!!
!!#ifdef _split_links_
!!    do mu=1,nd
!!       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
!!          dSbydU_mux%Cl = dSbydU_mux%Cl + dSbydUsm_mux%Cl
!!       end do; end do; end do; end do
!!    end do
!!#endif
!!
!!#endif 
!!
!!    !dS_polybydU = dSbydU
!!    do mu=1,nd
!!       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
!!          dS_polybydU_mux%Cl = dS_polybydU_mux%Cl + dSbydU_mux%Cl
!!       end do; end do; end do; end do
!!    end do
!!
!!#undef dS_polybydU_mux
!!#undef dSbydU_mux
!!
!!    deallocate(_dS_links_)
!!    deallocate(chi)
!!    deallocate(Mchi)
!!    deallocate(eta)
!!
!!  end subroutine GetdS_polybydU_1f
*/
  subroutine GetdS_twopfbydU_poly(dS_pfbydU,U_xd,phi,c_n,z_k)
    ! begin args: dS_pfbydU, U_xd, phi, c_n, z_k
    ! it is assumed that n = size(z_k) is even and that z_k(1:n/2) contains the roots with
    ! positive imaginary part, and that the remaining roots are the complex conjugate of these.
    type(colour_matrix), dimension(:,:,:,:,:) :: dS_pfbydU
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phi
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    ! begin local_vars
    integer :: ix,iy,iz,it,is,i_v,j_v,n_poly, j,k
    integer :: mu
    complex(dc), dimension(1:size(z_k)+1) :: p_i, q_i
    integer, dimension(1:size(z_k)+1) :: iconjg
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi,eta,phidag
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dSbydU
    integer :: iterations_i(1:size(z_k)+1)
    real(dp) :: err
    integer :: isweeps
    ! begin execution
    allocate(chi(nxp,nyp,nzp,ntp,ns))
    allocate(eta(nxp,nyp,nzp,ntp,ns))
    allocate(phidag(nxp,nyp,nzp,ntp,ns))
    allocate(dSbydU(nxp,nyp,nzp,ntp,nd))
    dSbydU = zero_matrix
    n_poly = size(z_k)
    n_poles = n_poly + 1
    !For complex shifts, we need to keep track of which solution corresponds to the complex conjugate.
    ! That is if chi_i(k) is the solution to (H^2 + q_i(k))chi = psi, we need j = iconjg(k) to be
    ! such that chi_i(j) is the solution to (H^2 + q_i(k)*)chi = psi. Take care of this by organising
    ! the poles q_i appropriately.
    q_i(1:n_poly/2) = -z_k(1:n_poly)
    q_i(n_poly/2+1:n_poly) = -conjg(z_k(1:n_poly/2))
    do k=1,n_poly/2
       iconjg(k) = k + n_poly/2
       iconjg(k + n_poly/2) = k
    end do
    ! For real roots ( in this case just z = 0), the conjugate solution is itself.
    q_i(n_poly+1) = 0.0d0
    iconjg(n_poly+1) = n_poly+1
    ! Convert the rational polynomial to a sum over poles.
    do k=1,n_poles
       p_i(k) = 1.0d0 / c_n
       do j=1,n_poles
          if ( j /= k ) p_i(k) = p_i(k)/(q_i(j)-q_i(k))
       end do
    end do
    call MultiCGInvert_z(n_poles, phi, chi_i(:,:,:,:,:,1:n_poles), q_i, tolerance_cg, iterations_i, Hsqwilson)
    iter_cg = maxval(iterations_i)
    do i_v=1,n_poles
       j_v = iconjg(i_v)
       chi = chi_i(:,:,:,:,:,i_v)
       eta = chi_i(:,:,:,:,:,j_v)
       call AlphaPsi(eta,-conjg(p_i(i_v)))
       call dS_wilsonbydU(dSbydU,eta,chi)
    end do
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dS_pfbydU(ix,iy,iz,it,mu)%Cl = dS_pfbydU(ix,iy,iz,it,mu)%Cl + dSbydU(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do
    deallocate(chi)
    deallocate(eta)
    deallocate(phidag)
    deallocate(dSbydU)
  end subroutine GetdS_twopfbydU_poly
  subroutine GetdS_onepfbydU_poly(dS_pfbydU,U_xd,phi,c_n,z_k,d_n,a_k,c_k)
    ! begin args: dS_pfbydU, U_xd, phi, c_n, z_k, d_n, a_k, c_k
    ! it is assumed that n = size(z_k) is even and that z_k(1:n/2) contains the roots with
    ! positive imaginary part, and that the remaining roots are the complex conjugate of these.
    type(colour_matrix), dimension(:,:,:,:,:) :: dS_pfbydU
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_vector), dimension(:,:,:,:,:) :: phi
    real(dp) :: c_n
    complex(dc), dimension(:) :: z_k
    real(dp) :: d_n
    real(dp), dimension(:) :: a_k, c_k
    ! begin local_vars
    integer :: ix,iy,iz,it,is
    integer :: i_v,j_v,n_poly,n_zolo,n_poles,j,k
    integer :: mu
    complex(dc), dimension(1:size(z_k)+size(c_k)) :: p_i,q_i
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: chi,eta,phidag
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dSbydU
    integer :: iterations_i(1:size(z_k)+size(c_k))
    integer, dimension(1:size(z_k)+size(c_k)) :: iconjg
    real(dp) :: err
    integer :: isweeps
    ! begin execution
    allocate(chi(nxp,nyp,nzp,ntp,ns))
    allocate(eta(nxp,nyp,nzp,ntp,ns))
    allocate(phidag(nxp,nyp,nzp,ntp,ns))
    allocate(dSbydU(nxp,nyp,nzp,ntp,nd))
    dSbydU = zero_matrix
    n_poly = size(z_k)
    n_zolo = size(c_k)
    n_poles = n_poly + n_zolo
    ! For complex shifts, we need to keep track of which solution corresponds to the complex conjugate.
    ! That is if chi_i(k) is the solution to (H^2 + q_i(k))chi = psi, we need j = iconjg(k) to be
    ! such that chi_i(j) is the solution to (H^2 + q_i(k)*)chi = psi. Take care of this by organising
    ! the poles q_i appropriately.
    q_i(1:n_poly/2) = -z_k(1:n_poly)
    q_i(n_poly/2+1:n_poly) = -conjg(z_k(1:n_poly/2))
    do k=1,n_poly/2
       iconjg(k) = k + n_poly/2
       iconjg(k + n_poly/2) = k
    end do
    ! For real roots ( from the zolotarev approximation ), the conjugate solution is itself.
    q_i(n_poly+1:n_poles) = c_k(1:n_zolo)
    do k = n_poly+1,n_poles
       iconjg(k) = k
    end do
    ! Convert the rational polynomial to a sum over poles.
    do k=1,n_poles
       p_i(k) = d_n / c_n
       do j=1,n_poles
          if ( j <= n_zolo ) p_i(k) = p_i(k)*(a_k(j)-q_i(k))
          if ( j /= k ) p_i(k) = p_i(k)/(q_i(j)-q_i(k))
       end do
    end do
    call MultiCGInvert_z(n_poles, phi, chi_i(:,:,:,:,:,1:n_poles), q_i, tolerance_cg, iterations_i, Hsqwilson)
    iter_multicg = maxval(iterations_i)
    do i_v=1,n_poles
       j_v = iconjg(i_v)
       chi = chi_i(:,:,:,:,:,i_v)
       eta = chi_i(:,:,:,:,:,j_v)
       call AlphaPsi(eta,-conjg(p_i(i_v)))
       call dS_wilsonbydU(dSbydU,eta,chi)
    end do
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dS_pfbydU(ix,iy,iz,it,mu)%Cl = dS_pfbydU(ix,iy,iz,it,mu)%Cl + dSbydU(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do
    deallocate(chi)
    deallocate(eta)
    deallocate(phidag)
    deallocate(dSbydU)
  end subroutine GetdS_onepfbydU_poly
end module wilsonPolyHMC
