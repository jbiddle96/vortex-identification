!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/vectoralgebra.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: vectoralgebra.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module VectorAlgebra

  use Kinds
  use ColourTypes
  use SpinorTypes
  implicit none
  private

  public :: normalise_vector
  public :: vector_inner_product
  public :: real_vector_inner_product
  public :: vector_norm
  public :: vector_normsq
  public :: orthogonalise_vectors
  public :: vector_product
  public :: MultiplyMatClrVec
  public :: MultiplyMatDagClrVec
  public :: MultiplyClrVecDagMat
  public :: MultiplyClrVecDagMatDag
  public :: VecDag
  public :: ClrVecDag
  public :: MultiplyMatVec
  public :: MultiplyMatDagVec
  public :: MultiplyVecDagMat
  public :: MultiplyVecDagMatDag
 
contains

  pure subroutine normalise_vector(v)

    type(colour_vector), intent(inout) :: v
    real(dp) :: norm

    norm = sqrt(sum(real(v%Cl)**2 + aimag(v%Cl)**2))
    v%Cl = v%Cl/norm

  end subroutine normalise_vector

  pure function vector_inner_product(v,w) result (vdotw)

    type(colour_vector), intent(in) :: v, w
    complex(dc) :: vdotw

    vdotw = sum(conjg(v%Cl)*w%Cl)

  end function vector_inner_product

  pure function real_vector_inner_product(v,w) result (vdotw)

    type(colour_vector), intent(in) :: v, w
    real(dp) :: vdotw

    vdotw = sum(real(v%Cl)*real(w%Cl) + aimag(v%Cl)*aimag(w%Cl))

  end function real_vector_inner_product

  elemental function vector_norm(v) result (norm)

    type(colour_vector), intent(in) :: v
    real(dp) :: norm

    norm = sqrt(sum(real(v%Cl)**2 + aimag(v%Cl)**2))

  end function vector_norm

  pure function vector_normsq(v) result (normsq)

    type(colour_vector), intent(in) :: v
    real(dp) :: normsq

    normsq = sum(real(v%Cl)**2 + aimag(v%Cl)**2)

  end function vector_normsq

  pure subroutine orthogonalise_vectors(w,v)

    type(colour_vector), intent(inout) :: w
    type(colour_vector), intent(in) :: v
    complex(dc) :: vdotw

    vdotw = sum(conjg(v%Cl)*w%Cl)

    w%Cl = w%Cl - v%Cl*vdotw

  end subroutine orthogonalise_vectors

  pure subroutine vector_product(x,v,w)

    type(colour_vector), intent(out) :: x
    type(colour_vector), intent(in) :: v,w
    integer :: ic,jc,kc

    do ic=1,nc
       jc = modulo(ic,3)+1
       kc = modulo(jc,3)+1

       x%Cl(ic) = conjg(v%Cl(jc)*w%Cl(kc) - v%Cl(kc)*w%Cl(jc))
    end do

  end subroutine vector_product

  subroutine MultiplyMatClrVec(times,left,right)

    type(colour_vector), intent(out) :: times
    type(colour_matrix), intent(in)  :: left
    type(colour_vector), intent(in) :: right

    times%Cl(1) = left%Cl(1,1)*right%Cl(1) + left%Cl(1,2)*right%Cl(2) + left%Cl(1,3)*right%Cl(3)
    times%Cl(2) = left%Cl(2,1)*right%Cl(1) + left%Cl(2,2)*right%Cl(2) + left%Cl(2,3)*right%Cl(3)
    times%Cl(3) = left%Cl(3,1)*right%Cl(1) + left%Cl(3,2)*right%Cl(2) + left%Cl(3,3)*right%Cl(3)

  end subroutine MultiplyMatClrVec

  subroutine MultiplyMatDagClrVec(times,left,right)

    type(colour_vector), intent(out) :: times
    type(colour_matrix), intent(in)  :: left
    type(colour_vector), intent(in) :: right

    times%Cl(1) = conjg(left%Cl(1,1))*right%Cl(1) + conjg(left%Cl(2,1))*right%Cl(2) + conjg(left%Cl(3,1))*right%Cl(3)
    times%Cl(2) = conjg(left%Cl(1,2))*right%Cl(1) + conjg(left%Cl(2,2))*right%Cl(2) + conjg(left%Cl(3,2))*right%Cl(3)
    times%Cl(3) = conjg(left%Cl(1,3))*right%Cl(1) + conjg(left%Cl(2,3))*right%Cl(2) + conjg(left%Cl(3,3))*right%Cl(3)

  end subroutine MultiplyMatDagClrVec

  subroutine MultiplyClrVecDagMat(times,left,right)

    type(colour_vector), intent(out) :: times
    type(colour_vector), intent(in) :: left
    type(colour_matrix), intent(in)  :: right

    times%Cl(1) = conjg(left%Cl(1))*right%Cl(1,1) + conjg(left%Cl(2))*right%Cl(2,1) + conjg(left%Cl(3))*right%Cl(3,1)
    times%Cl(2) = conjg(left%Cl(1))*right%Cl(1,2) + conjg(left%Cl(2))*right%Cl(2,2) + conjg(left%Cl(3))*right%Cl(3,2)
    times%Cl(3) = conjg(left%Cl(1))*right%Cl(1,3) + conjg(left%Cl(2))*right%Cl(2,3) + conjg(left%Cl(3))*right%Cl(3,3)

  end subroutine MultiplyClrVecDagMat

  subroutine MultiplyClrVecDagMatDag(times,left,right)

    type(colour_vector), intent(out) :: times
    type(colour_vector), intent(in) :: left
    type(colour_matrix), intent(in)  :: right

    times%Cl(1) = conjg(left%Cl(1)*right%Cl(1,1) + left%Cl(2)*right%Cl(1,2) + left%Cl(3)*right%Cl(1,3))
    times%Cl(2) = conjg(left%Cl(1)*right%Cl(2,1) + left%Cl(2)*right%Cl(2,2) + left%Cl(3)*right%Cl(2,3))
    times%Cl(3) = conjg(left%Cl(1)*right%Cl(3,1) + left%Cl(2)*right%Cl(3,2) + left%Cl(3)*right%Cl(3,3))

  end subroutine MultiplyClrVecDagMatDag

  pure subroutine VecDag(dag,left)

    type(colour_vector), dimension(ns), intent(out) :: dag
    type(colour_vector), dimension(ns), intent(in) :: left
    integer :: is

    do is=1,ns
       dag(is)%Cl = conjg(left(is)%Cl)
    end do

  end subroutine VecDag

  elemental subroutine ClrVecDag(dag,left)

    type(colour_vector), intent(out) :: dag
    type(colour_vector), intent(in) :: left

    dag%Cl = conjg(left%Cl)

  end subroutine ClrVecDag

  subroutine MultiplyMatVec(times,left,right)

    type(colour_vector), dimension(ns), intent(out) :: times
    type(colour_matrix), intent(in)  :: left
    type(colour_vector), dimension(ns), intent(in) :: right

    integer :: is

    do is=1,ns
       times(is)%Cl(1) = left%Cl(1,1)*right(is)%Cl(1) + left%Cl(1,2)*right(is)%Cl(2) + left%Cl(1,3)*right(is)%Cl(3)
       times(is)%Cl(2) = left%Cl(2,1)*right(is)%Cl(1) + left%Cl(2,2)*right(is)%Cl(2) + left%Cl(2,3)*right(is)%Cl(3)
       times(is)%Cl(3) = left%Cl(3,1)*right(is)%Cl(1) + left%Cl(3,2)*right(is)%Cl(2) + left%Cl(3,3)*right(is)%Cl(3)
    end do

  end subroutine MultiplyMatVec

  subroutine MultiplyMatDagVec(times,left,right)

    type(colour_vector), dimension(ns), intent(out) :: times
    type(colour_matrix), intent(in)  :: left
    type(colour_vector), dimension(ns), intent(in) :: right

    integer :: is

    do is=1,ns
       times(is)%Cl(1) = conjg(left%Cl(1,1))*right(is)%Cl(1) + conjg(left%Cl(2,1))*right(is)%Cl(2) + &
            &               conjg(left%Cl(3,1))*right(is)%Cl(3)
       times(is)%Cl(2) = conjg(left%Cl(1,2))*right(is)%Cl(1) + conjg(left%Cl(2,2))*right(is)%Cl(2) + &
            &               conjg(left%Cl(3,2))*right(is)%Cl(3)
       times(is)%Cl(3) = conjg(left%Cl(1,3))*right(is)%Cl(1) + conjg(left%Cl(2,3))*right(is)%Cl(2) + &
            &               conjg(left%Cl(3,3))*right(is)%Cl(3)
    end do

  end subroutine MultiplyMatDagVec

  subroutine MultiplyVecDagMat(times,left,right)

    type(colour_vector), dimension(ns), intent(out) :: times
    type(colour_vector), dimension(ns), intent(in) :: left
    type(colour_matrix), intent(in)  :: right

    integer :: is

    do is=1,ns
       times(is)%Cl(1) = conjg(left(is)%Cl(1))*right%Cl(1,1) + conjg(left(is)%Cl(2))*right%Cl(2,1) + &
            &               conjg(left(is)%Cl(3))*right%Cl(3,1)
       times(is)%Cl(2) = conjg(left(is)%Cl(1))*right%Cl(1,2) + conjg(left(is)%Cl(2))*right%Cl(2,2) + &
            &               conjg(left(is)%Cl(3))*right%Cl(3,2)
       times(is)%Cl(3) = conjg(left(is)%Cl(1))*right%Cl(1,3) + conjg(left(is)%Cl(2))*right%Cl(2,3) + &
            &               conjg(left(is)%Cl(3))*right%Cl(3,3)
    end do

  end subroutine MultiplyVecDagMat

  subroutine MultiplyVecDagMatDag(times,left,right)

    type(colour_vector), dimension(ns), intent(out) :: times
    type(colour_vector), dimension(ns), intent(in) :: left
    type(colour_matrix), intent(in)  :: right

    integer :: is

    do is=1,ns
       times(is)%Cl(1) = conjg(left(is)%Cl(1)*right%Cl(1,1) + left(is)%Cl(2)*right%Cl(1,2) + &
            &                     left(is)%Cl(3)*right%Cl(1,3))
       times(is)%Cl(2) = conjg(left(is)%Cl(1)*right%Cl(2,1) + left(is)%Cl(2)*right%Cl(2,2) + &
            &                     left(is)%Cl(3)*right%Cl(2,3))
       times(is)%Cl(3) = conjg(left(is)%Cl(1)*right%Cl(3,1) + left(is)%Cl(2)*right%Cl(3,2) + &
            &                     left(is)%Cl(3)*right%Cl(3,3))
    end do

  end subroutine MultiplyVecDagMatDag

end module VectorAlgebra
