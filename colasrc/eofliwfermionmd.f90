/* eofliw.h */


module eofliwFermionMD

  use CloverFmunu
  use eofliwMatrix
  use fliwPropagator
  use ConjGradSolvers
  use GaugeField
  use FatLinks
  use GL3Diag
  use VectorAlgebra
  use MatrixAlgebra
  use TensorAlgebra
  use ZolotarevApprox
  use AccMinEVCG




  use StoutLinks

  use ReduceOps
  use FermionField
  use FermionFieldMPIComms
  use SpinorTypes
  use FermionTypes
  use FermionAlgebra
  use GaugeFieldMPIComms
  use LatticeSize
  use Kinds
  use ColourTypes
  implicit none
  private
  type(colour_matrix), dimension(:,:,:,:,:,:), allocatable, public :: Un_xd
  type(dirac_fermion), dimension(:,:), allocatable, public :: chi_i
  integer, public :: n_aux = 0 ! the size of each of the two auxillary fields (chi_i,eta_i) currently allocated.
  real(dp), dimension(:), allocatable, public :: a_k1f, b_k1f, c_k1f
  real(dp), public :: d_n1f
  integer, public :: n_z1f = 0
  !complex(dc), dimension(:), allocatable :: a_j, b_j, c_j
  integer, public :: n_rpmd, n_rp
  real(dp), public :: eps_zolo
  type(dirac_fermion), dimension(:,:), allocatable, public :: v_min, v_max, Dv_i
  integer, public :: iter_cg = 0, iter_ev = 0, iter_multicg = 0, min_z=0, max_z=0
  real(dp), public :: tolerance_cg = 1.0d-8, tolerance_z = 1.0d-8
  real(dp), public :: kappa_ud, kappa_s
  real(dp), public :: m_ud = 0.0d0, m_s = 0.0d0 !bare sea quark masses
  logical, public :: init_call = .true.
  integer, public :: ev_calls = 0
  real(dp), public :: ev_min=0.0d0, ev_max=0.0d0
  public :: RefreshMatrixFields
  public :: InitialiseTwoFlavourMatrix
  public :: InitialiseOneFlavourMatrix
  public :: Init1PFRationalPoly
  public :: MultiplyM_twopf
  public :: GetPseudofermionField_twopf
  public :: GetPseudofermionField_onepf
  public :: MultiplyM_onepf
  public :: GetdS_onepfbydU
  public :: dS_eofliwbydU
  public :: AddDeofliwbydU
  public :: GetdS_twopfbydU
  ! To Do: Update the derivative routines so each call RefreshMatrixField to make things a bit more sane?
contains
  subroutine RefreshMatrixFields(U_xd, Usm_xd ,UpdateMatrixFields)
    ! begin args: U_xd, Usm_xd , UpdateMatrixFields
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd, Usm_xd
    logical :: UpdateMatrixFields
    ! begin local_vars
    integer :: isweeps
    ! begin execution
    if ( UpdateMatrixFields ) then
       Usm_xd = U_xd(1:nxs,1:nys,1:nzs,1:nts,:)
       do isweeps =1,smear_sweeps
          Un_xd(:,:,:,:,:,isweeps) = Usm_xd
          call StoutSmear(Usm_xd,alpha_smear)
          !!if ( isweeps == smear_sweeps) call FixGaugeField(Usm_xd)
          call ShadowGaugeField(Usm_xd,1)
       end do
       UpdateMatrixFields = .false.
    end if
  end subroutine RefreshMatrixFields
  subroutine InitialiseTwoFlavourMatrix(U_xd,UpdateMatrixFields)
    ! begin args: U_xd, UpdateMatrixFields
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    logical :: UpdateMatrixFields
    ! begin local_vars
    real(dp) :: u0, u0sm
    ! begin execution
    call RefreshMatrixFields(U_xd, Usm_xd , UpdateMatrixFields)
    kappa_fliw = kappa_ud
    call InitialiseeofliwOperator(U_xd, Usm_xd , kappa_ud , u0_bar, u0fl_bar)
  end subroutine InitialiseTwoFlavourMatrix
  subroutine InitialiseOneFlavourMatrix(U_xd,UpdateMatrixFields)
    ! begin args: U_xd, UpdateMatrixFields
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    logical :: UpdateMatrixFields
    ! begin execution
    call RefreshMatrixFields(U_xd, Usm_xd , UpdateMatrixFields)
    kappa_fliw = kappa_s
    call InitialiseeofliwOperator(U_xd, Usm_xd , kappa_s , u0_bar, u0fl_bar)
  end subroutine InitialiseOneFlavourMatrix
  subroutine Init1PFRationalPoly(n_zolo,tol_z)
    ! begin args: n_zolo, tol_z, resize
    integer :: n_zolo
    real(dp) :: tol_z
!! logical :: resize
    ! begin local_vars
    real(dp) :: x_min, x_max
    real(dp) :: tol_ev
!! real(dp) :: kappa, eps_zolo, tol_z, tol_ev
    integer :: j,k
    real(dp), dimension(1) :: mu_min, mu_max
    integer :: n_est, MinIter_i(1), MaxIter_i(1)
    !x_min = eps_zolo
    !x_max = 1.0d0 + 10*kappa
! begin execution
    tol_ev = 1.0d-3
    call MinEVSpectrum(1,mu_min, v_min, Dv_i, tol_ev, MinIter_i, init_call, Hsqeofliw, 0, 1.0d0 )
    ev_min = sqrt(mu_min(1))
    x_min = 0.8d0*ev_min
    call MinEVSpectrum(1,mu_max, v_max, Dv_i, tol_ev, MaxIter_i, init_call, Hsqeofliw, 0,-1.0d0 )
    ev_max = sqrt(mu_max(1))
    x_max = 1.1d0*ev_max
    n_est = n_zolo
    n_zolo = GetZolotarevEstOrder(tol_z,x_min,x_max)
    !n_rp = GetZolotarevEstOrder(1.0d-14,x_min,x_max) + 4
    !n_rpmd = GetZolotarevEstOrder(tol_z**2,x_min,x_max) + 4
    !resize =
    if ( n_zolo /= n_est ) then
       if ( allocated(a_k1f) .and. allocated(b_k1f) .and. allocated(c_k1f) ) then
          deallocate(a_k1f,b_k1f,c_k1f)
       end if
       allocate(a_k1f(n_zolo),b_k1f(n_zolo),c_k1f(n_zolo))
    end if
    call GetZolotarevCoeffs(n_zolo,a_k1f,b_k1f,c_k1f,c_2n,d_n1f,x_min,x_max,delta_z)
    !call GetZolotarevCoeffs(n_zolo,a_l,b_l,c_l,c_2n,d_n,x_min,x_max,delta_z)
    !a_l = a_l*(x_min)
!! c_k(1:n_zolo) = c_l(1:n_zolo)
!!
!! do k=1,n_zolo
!! b_l(k) = d_n
!! do j=1,n_zolo
!! if ( j <= n_zolo ) b_l(k) = b_l(k)*(a_l(j)-c_l(k))
!! if ( j /= k ) b_l(k) = b_l(k)/(c_l(j)-c_l(k))
!! end do
!! end do
!!
!! q_l = a_l
!!
!! do k=1,n_zolo
!! p_l(k) = 1.0d0/d_n
!! do j=1,n_zolo
!! if ( j <= n_zolo ) p_l(k) = p_l(k)*(c_l(j)-q_l(k))
!! if ( j /= k ) p_l(k) = p_l(k)/(q_l(j)-q_l(k))
!! end do
!! end do
!!
!! b_l(1:n_zolo) = real(b_l(1:n_zolo))
!! c_l(1:n_zolo) = real(c_l(1:n_zolo))
!!
!! init_call = .false.
    iter_ev = sum(MinIter_i(1:1)) + sum(MaxIter_i(1:1))
    if ( min_z == 0 ) min_z = n_zolo
    min_z = min(min_z,n_zolo)
    max_z = max(max_z,n_zolo)
  end subroutine Init1PFRationalPoly
  subroutine MultiplyM_twopf(phi,eta)
    ! begin args: phi, eta
    type(dirac_fermion), dimension(:) :: phi, eta
    ! begin execution
    eta = phi ! Initial guess (for reversibility)
    call CGInvert(phi,eta,tolerance_cg,iter_cg,Hsqeofliw)
  end subroutine MultiplyM_twopf
  subroutine GetPseudofermionField_twopf(phi)
    ! begin args: phi
    type(dirac_fermion), dimension(:) :: phi
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: xi
    !The pseudo fermion fields have no dynamic, they are simply an auxillary field used to calculate
    !the fermionic determinant, det(M^dag M) = \int Dphi* Dphi e^(-S_pf), where S_pf = phi* (M^dag M)^-1 phi
    !So we wish to generate phi according to the distribution e^(-S_pf) = e^(-phi* M^-1 (M^dag)^-1 phi )
    !Note that xi = (M^dag)^-1 phi is Gaussian distributed, P(xi) = e^-(xi* xi) and therefore easily
    !generated, and hence to obtain the appropriate distribution for phi, we set phi = M^dag xi.
    ! begin execution
    allocate(xi(n_xpeo))
    call ComplexGaussianField(xi)
    call Deofliw_dag(xi,phi)
    deallocate(xi)
  end subroutine GetPseudofermionField_twopf
  subroutine GetPseudofermionField_onepf(phi,d_n,a_k,c_k)
    ! begin args: phi, d_n, a_k, c_k
    type(dirac_fermion), dimension(:) :: phi
    real(dp) :: d_n
    real(dp), dimension(:) :: a_k, c_k
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: xi
    complex(dc), dimension(size(c_k)) :: p_k, q_k, r_k
    integer, dimension(size(c_k)) :: iterations_i
    integer :: i_v
    integer :: j,k,n_poles
    !For a single flavour, S_pf = phi* (M^dag M)^-1/2 phi ~= phi* R(H^2) phi, where R(x) is a
    !rational approximation to the inverse square root. R(x^2) can be factored R(x^2) = Q(x)* Q(x),
    !and hence xi = Q(x) phi is Gaussian distributed. So we set phi = T(H) xi, where T(x) = 1/Q(x)
    !is another rational polynomial.
    ! begin execution
    allocate(xi(n_xpeo))
    call ComplexGaussianField(xi)
    ! R(x^2) = d_n \prod (x^2 + a_k)/(x^2 + c_k) = Q(x)*Q(x), where
    ! Q(x) = sqrt(d_n) \prod (x + i sqrt(a_k) )/(x + i\sqrt(c_k))
    n_poles = size(c_k)
    p_k = i*sqrt(c_k)
    q_k = i*sqrt(a_k)
    ! Convert the rational polynomial Q(x)^-1 to a sum over poles.
    do k=1,n_poles
       r_k(k) = 1.0_dp/sqrt(d_n)
       do j=1,n_poles
          r_k(k) = r_k(k)*(p_k(j)-q_k(k))
          if ( j /= k ) r_k(k) = r_k(k)/(q_k(j)-q_k(k))
       end do
    end do
    call MultiCRInvert(n_poles, xi, chi_i( : ,1:n_poles), q_k, tolerance_cg, iterations_i, Heofliw)
    call MultiplyAlphaPhi(phi,1.0_dp/sqrt(d_n),xi)
    do i_v=1,n_poles
       call PsiPlusAlphaPhi(phi,r_k(i_v),chi_i( : ,i_v))
    end do
    deallocate(xi)
  end subroutine GetPseudofermionField_onepf
  subroutine MultiplyM_onepf(phi,chi,d_n,a_k,c_k)
    ! begin args: phi, chi, d_n, a_k, c_k
    type(dirac_fermion), dimension(:) :: phi, chi
    real(dp) :: d_n
    real(dp), dimension(:) :: a_k, c_k
    ! begin local_vars
    integer :: i_v
    real(dp), dimension(size(c_k)) :: b_k
    integer, dimension(size(c_k)) :: iterations_i
    integer :: j,k,n_poles
    ! begin execution
    n_poles = size(c_k)
    do k=1,n_poles
       b_k(k) = d_n
       do j=1,n_poles
          b_k(k) = b_k(k)*(a_k(j)-c_k(k))
          if ( j /= k ) b_k(k) = b_k(k)/(c_k(j)-c_k(k))
       end do
    end do
    call MultiCGInvert(n_poles, phi, chi_i( : ,1:n_poles), c_k, tolerance_cg, iterations_i, Hsqeofliw)
    iter_multicg = iterations_i(1)
    call MultiplyAlphaPhi(chi,d_n,phi)
    do i_v=1,n_poles
       call PsiPlusAlphaPhi(chi,b_k(i_v),chi_i( : ,i_v))
    end do
  end subroutine MultiplyM_onepf
  subroutine GetdS_onepfbydU(dS_pfbydU,U_xd,phi,d_n,a_k,c_k)
    ! begin args: dS_pfbydU, U_xd, phi, d_n, a_k, c_k
    type(colour_matrix), dimension(:,:,:,:,:) :: dS_pfbydU
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(dirac_fermion), dimension(:) :: phi
    real(dp) :: d_n
    real(dp), dimension(:) :: a_k, c_k
    ! begin local_vars
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dSbydU, dSbydUsm
    type(dirac_fermion), dimension(:), allocatable :: chi, eta
    integer :: ix,iy,iz,it,is,i_v
    integer :: j,k,n_poles
    integer :: mu
    real(dp), dimension(size(c_k)) :: b_k
    integer, dimension(size(c_k)) :: iterations_i
    integer :: isweeps
    ! begin execution
    allocate(dSbydU(nxp,nyp,nzp,ntp,nd))
    allocate(dSbydUsm(nxp,nyp,nzp,ntp,nd))
    allocate(chi(n_xpeo))
    allocate(eta(n_xpeo))
    dSbydU = zero_matrix
    dSbydUsm = zero_matrix
    n_poles = size(c_k)
    ! Convert the rational polynomial to a sum over poles.
    do k=1,n_poles
       b_k(k) = d_n
       do j=1,n_poles
          b_k(k) = b_k(k)*(a_k(j)-c_k(k))
          if ( j /= k ) b_k(k) = b_k(k)/(c_k(j)-c_k(k))
       end do
    end do
    call MultiCGInvert(n_poles, phi, chi_i( : ,1:n_poles), c_k, tolerance_cg, iterations_i, Hsqeofliw)
    do i_v=1,n_poles
       chi = chi_i( : ,i_v)
       eta = chi_i( : ,i_v)
       call AlphaPsi(eta,-b_k(i_v))
       call dS_eofliwbydU(dSbydU, dSbydUsm,eta,chi)
    end do
    do isweeps=smear_sweeps,1,-1
       call dU_prbydU_stout(dSbydUsm,Un_xd(:,:,:,:,:,isweeps),alpha_smear)
    end do
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dSbydU(ix,iy,iz,it,mu)%Cl = dSbydU(ix,iy,iz,it,mu)%Cl + dSbydUsm(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dS_pfbydU(ix,iy,iz,it,mu)%Cl = dS_pfbydU(ix,iy,iz,it,mu)%Cl + dSbydU(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do
    deallocate(dSbydU, dSbydUsm)
    deallocate(chi)
    deallocate(eta)
  end subroutine GetdS_onepfbydU
  subroutine dS_eofliwbydU(dSbydU, dSbydUsm,eta,chi,add_hc)
    ! begin args: dSbydU, dSbydUsm, eta, chi, add_hc
    type(colour_matrix), dimension(:,:,:,:,:) :: dSbydU, dSbydUsm
    type(dirac_fermion), dimension(:) :: eta, chi
    logical, optional :: add_hc
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Deta, Dchi
    type(dirac_fermion), dimension(:), allocatable :: chi_e, eta_e, Dchi_e, Deta_e, phi_e
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: eta_pr, chi_pr, Deta_pr, Dchi_pr
    logical :: add_conjg
    integer :: site_parity,op_parity,p_xyzt
    integer :: i_xeo
    ! begin execution
    allocate(Deta(n_xpeo))
    allocate(Dchi(n_xpeo))
    allocate(chi_e(n_xpeo))
    allocate(eta_e(n_xpeo))
    allocate(Dchi_e(n_xpeo))
    allocate(Deta_e(n_xpeo))
    allocate(phi_e(n_xpeo))
    allocate(eta_pr(nxs,nys,nzs,nts,ns))
    allocate(chi_pr(nxs,nys,nzs,nts,ns))
    allocate(Deta_pr(nxs,nys,nzs,nts,ns))
    allocate(Dchi_pr(nxs,nys,nzs,nts,ns))
    call Heofliw(eta,Deta)
    call Heofliw(chi,Dchi)
    ! Deal with the even-odd preconditioning here.
    call EOWilson_Split(eta,eta_e,0)
    call EOWilson_Split(chi,chi_e,0)
    call EOWilson_Split(Deta,Deta_e,0)
    call EOWilson_Split(Dchi,Dchi_e,0)
    do i_xeo = 1,n_xeo
       eta_e(i_xeo)%cs = -eta_e(i_xeo)%cs
       chi_e(i_xeo)%cs = -chi_e(i_xeo)%cs
       Deta_e(i_xeo)%cs = -Deta_e(i_xeo)%cs
       Dchi_e(i_xeo)%cs = -Dchi_e(i_xeo)%cs
    end do
    call phi_eo2phi_x(eta_e,eta,eta_pr)
    call phi_eo2phi_x(chi_e,chi,chi_pr)
    call phi_eo2phi_x(Deta_e,Deta,Deta_pr)
    call phi_eo2phi_x(Dchi_e,Dchi,Dchi_pr)
    !Now perform a standard (unpreconditioned) derivative
    call ShadowFermionField(eta_pr,1)
    call ShadowFermionField(chi_pr,1)
    call ShadowFermionField(Deta_pr,1)
    call ShadowFermionField(Dchi_pr,1)
    call AddDeofliwbydU(dSbydU, dSbydUsm,eta_pr,chi_pr,Deta_pr,Dchi_pr)
    if ( present(add_hc) ) then
       add_conjg = add_hc
    else
       add_conjg = .false.
    end if
    if ( add_conjg ) call AddDeofliwbydU(dSbydU, dSbydUsm,chi_pr,eta_pr,Dchi_pr,Deta_pr)
    deallocate(Deta)
    deallocate(Dchi)
    deallocate(chi_e)
    deallocate(eta_e)
    deallocate(Dchi_e)
    deallocate(Deta_e)
    deallocate(phi_e)
    deallocate(eta_pr)
    deallocate(chi_pr)
    deallocate(Deta_pr)
    deallocate(Dchi_pr)
  end subroutine dS_eofliwbydU
  subroutine AddDeofliwbydU(dSbydU, dSbydUsm,eta,chi,Deta,Dchi)
    ! begin args: dSbydU, dSbydUsm, eta, chi, Deta, Dchi
    !Adds dS_eofliwbydU = eta^dag*D^dag*dDbydU chi + eta^dag*dD^dagbydU*D*chi to dSbydU (and dSbydUfl)
    type(colour_matrix), dimension(:,:,:,:,:) :: dSbydU, dSbydUsm
    type(colour_vector), dimension(:,:,:,:,:) :: eta, chi, Deta, Dchi
    ! begin local_vars
    real(dp) :: d_u, w_u
    integer, dimension(nd) :: dmu
    integer :: ix,iy,iz,it,is
    integer :: jx,jy,jz,jt
    integer :: mu, nu,imunu
    type(colour_vector), dimension(ns) :: eta_l, chi_r, chi_l, eta_r, g5eta_l, g5chi_l
    type(colour_matrix) :: dS_d, dS_w
    ! begin execution
    d_u = -kappa_fliw ! coefficient of the Dirac term
    w_u = -kappa_fliw ! coefficient of the Wilson term
    d_u = d_u / u0_bar
    w_u = w_u / u0fl_bar
    do mu=1,nd
       dmu = 0
       dmu(mu) = 1
       do it=1,nt
          jt = mapt(it + dmu(4))
          do iz=1,nz
             jz = mapz(iz + dmu(3))
             do iy=1,ny
                jy = mapy(iy + dmu(2))
                do ix=1,nx
                   jx = mapx(ix + dmu(1))
                   dS_d = zero_matrix
                   dS_w = zero_matrix
                   call VecDag(eta_l,Deta(ix,iy,iz,it,:))
                   call GammaPhi(g5eta_l,eta_l,5)
                   do is=1,ns
                      eta_l(is)%Cl = d_u*g5eta_l(is)%Cl
                   end do
                   call GammaPhi(chi_r,chi(jx,jy,jz,jt,:),mu)
                   call SubVectorOuterProduct(dS_d,eta_l,chi_r)
                   call VecDag(eta_l,eta(ix,iy,iz,it,:))
                   call GammaPhi(g5eta_l,eta_l,5)
                   do is=1,ns
                      eta_l(is)%Cl = d_u*g5eta_l(is)%Cl
                   end do
                   call GammaPhi(chi_r,Dchi(jx,jy,jz,jt,:),mu)
                   call SubVectorOuterProduct(dS_d,eta_l,chi_r)
                   call VecDag(eta_l,Deta(ix,iy,iz,it,:))
                   call GammaPhi(g5eta_l,eta_l,5)
                   do is=1,ns
                      eta_l(is)%Cl = w_u*g5eta_l(is)%Cl
                   end do
                   chi_r = chi(jx,jy,jz,jt,:)
                   call AddVectorOuterProduct(dS_w,eta_l,chi_r)
                   call VecDag(eta_l,eta(ix,iy,iz,it,:))
                   call GammaPhi(g5eta_l,eta_l,5)
                   do is=1,ns
                      eta_l(is)%Cl = w_u*g5eta_l(is)%Cl
                   end do
                   chi_r = Dchi(jx,jy,jz,jt,:)
                   call AddVectorOuterProduct(dS_w,eta_l,chi_r)
                   !Deal with the fermion boundary conditions here.
                   if ( (j_nx == nlx) .and. (ix==nx) .and. (mu==1) ) then
                      dS_d%cl = ff_bcx*dS_d%cl
                      dS_w%cl = ff_bcx*dS_w%cl
                   end if
                   if ( (j_ny == nly) .and. (iy==ny) .and. (mu==2) ) then
                      dS_d%cl = ff_bcy*dS_d%cl
                      dS_w%cl = ff_bcy*dS_w%cl
                   end if
                   if ( (j_nz == nlz) .and. (iz==nz) .and. (mu==3) ) then
                      dS_d%cl = ff_bcz*dS_d%cl
                      dS_w%cl = ff_bcz*dS_w%cl
                   end if
                   if ( (j_nt == nlt) .and. (it==nt) .and. (mu==4) ) then
                      dS_d%cl = ff_bct*dS_d%cl
                      dS_w%cl = ff_bct*dS_w%cl
                   end if
                   dSbydU(ix,iy,iz,it,mu)%cl = dSbydU(ix,iy,iz,it,mu)%cl + dS_d%cl
                   dSbydUsm(ix,iy,iz,it,mu)%cl = dSbydUsm(ix,iy,iz,it,mu)%cl + dS_w%cl
                end do
             end do
          enddo
       end do
    end do
  end subroutine AddDeofliwbydU
  subroutine GetdS_twopfbydU(dS_pfbydU,U_xd,phi)
    ! begin args: dS_pfbydU, U_xd, phi
    type(colour_matrix), dimension(:,:,:,:,:) :: dS_pfbydU
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(dirac_fermion), dimension(:) :: phi
    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: chi, eta
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: dSbydU, dSbydUsm
    integer, dimension(nd) :: dmu
    integer :: ix,iy,iz,it,is
    integer :: mu, isweeps, t0, t1, tsum, nu, inu
    ! begin execution
    allocate(chi(n_xpeo))
    allocate(eta(n_xpeo))
    allocate(dSbydU(nxp,nyp,nzp,ntp,nd))
    dSbydU = zero_matrix
    allocate(dSbydUsm(nxp,nyp,nzp,ntp,nd))
    dSbydUsm = zero_matrix
    eta = phi
    call CGInvert(phi,eta,tolerance_cg,iter_cg,Hsqeofliw)
    chi = eta
    call dS_eofliwbydU(dSbydU, dSbydUsm,eta,chi)
    do isweeps=smear_sweeps,1,-1
       call dU_prbydU_stout(dSbydUsm,Un_xd(:,:,:,:,:,isweeps),alpha_smear)
    end do
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dSbydU(ix,iy,iz,it,mu)%Cl = dSbydU(ix,iy,iz,it,mu)%Cl + dSbydUsm(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do
    !Overall minus sign, due to dA^(-1)/dA = -A^(-1) x A^(-1).
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          dS_pfbydU(ix,iy,iz,it,mu)%Cl = dS_pfbydU(ix,iy,iz,it,mu)%Cl - dSbydU(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do
    deallocate(chi)
    deallocate(eta)
    deallocate(dSbydU, dSbydUsm)
  end subroutine GetdS_twopfbydU
end module eofliwFermionMD
