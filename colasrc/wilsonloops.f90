
#include "defines.h"

module WilsonLoops

  use Kinds
  use LatticeSize
  use ColourTypes
  use MPIInterface
  use GaugeFieldMPIComms
  use MatrixAlgebra
  use ReduceOps
  implicit none
  private

  public :: GetImpWilsonLoops
  public :: GetWilsonLoops
  public :: WilsonLoop
  public :: WriteWilsonLoops
  public :: MultiplyUU
  public :: MakePath

contains

  subroutine GetImpWilsonLoops(W_rt,U_xd,that,nrx,nry,nrz,nrt)

    real(dp), dimension(:,0:,0:,:,:,:) :: W_rt
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: that !Time-like dimension. (Hardcoded for that=4 for now.)
    integer :: nrx,nry,nrz,nrt ! Maximum path lengths for first, second, third and time axes.
    type(colour_matrix), dimension(:,:,:,:), allocatable :: R_x, R_xpdt
    real(dp), dimension(:,:,:,:), allocatable :: W_x
    real(dp) :: multiplicity(nd-1), W_bar, W_pp
    integer :: id,jd, ix,iy,iz, irx,iry,irz, rmax
    integer :: iaxes, dx(nd), dt
    integer :: xhat(nd-1), ir(nd-1)
    real(dp) :: t0, t1
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: T_xl

    multiplicity = (/ 1.0d0, 4.0d0, 36.0d0 /)
    xhat = (/ 1,2,3 /)
    rmax = max(nrx,nry,nrz)

    allocate(T_xl(nx,ny,nz,nt,nrt))
    allocate(W_x(nx,ny,nz,nt))
    allocate(R_x(nx,ny,nz,nt))
    allocate(R_xpdt(nx,ny,nz,nt))

    id = 4

    T_xl(1:nx,1:ny,1:nz,1:nt,1) = U_xd(1:nx,1:ny,1:nz,1:nt,id)
    R_xpdt = T_xl(:,:,:,:,1)
    do dt=2,nrt
       R_xpdt = CShiftGaugePlane(R_xpdt,id,1)
       call MultiplyUU(R_x,T_xl(:,:,:,:,dt-1),R_xpdt) !Use R_x as temporary storage for the result,
       T_xl(:,:,:,:,dt) = R_x ! as T_xl becomes read only if it appears twice in the function call.
    end do

    W_rt = 0.0d0

    do id=1,nd-1
       ix = xhat(id)
       iy = xhat(modc(id+1,nd-1))
       iz = xhat(modc(id+2,nd-1))

       ir = (/ ix,iy,iz /)

       do irx=1,nrx
          dx = 0
          dx(ix) = irx
          do iry=0,nry
             dx(iy) = iry
             do irz=0,nrz
                dx(iz) = irz

                call MakePath(R_x,U_xd,dx,ir)
                
                R_xpdt = R_x
                do dt=1,nrt
                   t0 = mpi_wtime()
                   R_xpdt = CShiftGaugePlane(R_xpdt,that,1)
                   call WilsonLoop(W_x,R_x,T_xl,dx,dt,R_xpdt)
                   W_pp = sum(W_x)
                   call AllSum(W_pp,W_bar)
                   W_rt(irx,iry,irz,dt,ix,that) = W_bar / nlattice 

                   t1 = mpi_wtime()
                   mpiprint '(4I4,F20.15)', irx, iry, irz, that, W_rt(irx,iry,irz,dt,ix,that)
                end do
                   
             end do
          end do

       end do

    end do

    deallocate(T_xl)
    deallocate(W_x)
    deallocate(R_x)
    deallocate(R_xpdt)

  end subroutine GetImpWilsonLoops

  subroutine GetWilsonLoops(W_rt,U_xd,that,nrx,nry,nrz,nrt)

    real(dp), dimension(:,0:,0:,:,:,:) :: W_rt
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: that !Time-like dimension. (Hardcoded for that=4 for now.)
    integer :: nrx,nry,nrz,nrt ! Maximum path lengths for first, second, third and time axes.
    type(colour_matrix), dimension(:,:,:,:), allocatable :: R_x, R_y, R_z
    type(colour_matrix), dimension(:,:,:,:), allocatable :: R_xy, R_xz, R_yz
    type(colour_matrix), dimension(:,:,:,:), allocatable :: R_xyz, R_xpdt
    real(dp), dimension(:,:,:,:), allocatable :: W_x
    real(dp) :: multiplicity(nd-1), W_bar, W_pp
    integer :: id,jd, ix,iy,iz, irx,iry,irz, rmax
    integer :: iaxes, dx(nd), dy(nd), dz(nd), dxy(nd), dxz(nd), dyz(nd), dxyz(nd), dt
    integer :: xhat(nd-1)
    real(dp) :: t0, t1
    type(colour_matrix), dimension(:,:,:,:,:,:), allocatable, target :: S_xdl
    type(colour_matrix), dimension(:,:,:,:,:), allocatable, target :: T_xl
#define R_xpdmu R_xpdt

    multiplicity = (/ 1.0d0, 4.0d0, 36.0d0 /)
    xhat = (/ 1,2,3 /)
    rmax = max(nrx,nry,nrz)

    allocate(S_xdl(nx,ny,nz,nt,nd-1,rmax))
    allocate(T_xl(nx,ny,nz,nt,nrt))
    allocate(W_x(nx,ny,nz,nt))
    allocate(R_x(nx,ny,nz,nt))
    allocate(R_y(nx,ny,nz,nt))
    allocate(R_z(nx,ny,nz,nt))
    allocate(R_xy(nx,ny,nz,nt))
    allocate(R_xz(nx,ny,nz,nt))
    allocate(R_yz(nx,ny,nz,nt))
    allocate(R_xyz(nx,ny,nz,nt))
    allocate(R_xpdt(nx,ny,nz,nt))

    do id=1,nd-1
       S_xdl(1:nx,1:ny,1:nz,1:nt,id,1) = U_xd(1:nx,1:ny,1:nz,1:nt,id)
       R_xpdmu = S_xdl(:,:,:,:,id,1)
       do irx=2,rmax
          R_xpdmu = CShiftGaugePlane(R_xpdmu,id,1)
          call MultiplyUU(R_x,S_xdl(:,:,:,:,id,irx-1),R_xpdmu) !Use R_x as temporary storage for the result,
          S_xdl(:,:,:,:,id,irx) = R_x ! as S_xdl becomes read only if it appears twice in the function call.
       end do
    end do
       
    id = 4

    T_xl(1:nx,1:ny,1:nz,1:nt,1) = U_xd(1:nx,1:ny,1:nz,1:nt,id)
    R_xpdt = T_xl(:,:,:,:,1)
    do dt=2,nrt
       R_xpdt = CShiftGaugePlane(R_xpdt,id,1)
       call MultiplyUU(R_x,T_xl(:,:,:,:,dt-1),R_xpdt) !Use R_x as temporary storage for the result,
       T_xl(:,:,:,:,dt) = R_x ! as T_xl becomes read only if it appears twice in the function call.
    end do

    W_rt = 0.0d0

    do id=1,nd-1
       ix = xhat(id)
       iy = xhat(modc(id+1,nd-1))
       iz = xhat(modc(id+2,nd-1))

       do irx=1,nrx
          dx = 0

          R_x = S_xdl(:,:,:,:,ix,irx)
          dx(ix) = irx
          
          do iry=0,nry
             dy = 0
             if ( iry > 0 ) then
                R_y = S_xdl(:,:,:,:,iy,iry)
                dy(iy) = iry

                R_xy = zero_matrix
                call AppendLine(R_xy,R_x,R_y,dx)
                call AppendLine(R_xy,R_y,R_x,dy)
                dxy = dx + dy
             else
                R_xy = R_x
             end if

             do irz=0,nrz
                dz = 0
                if ( irz > 0 ) then
                   R_z = S_xdl(:,:,:,:,iz,irz)
                   dz(iz) = irz
                   
                   R_xz = zero_matrix
                   call AppendLine(R_xz,R_x,R_z,dx)
                   call AppendLine(R_xz,R_z,R_x,dz)
                   dxz = dx + dz

                   if (iry > 0) then
                      R_yz = zero_matrix
                      call AppendLine(R_yz,R_y,R_z,dy)
                      call AppendLine(R_yz,R_z,R_y,dz)
                      dyz = dy + dz

                      R_xyz = zero_matrix
                      call AppendLine(R_xyz,R_xy,R_z,dxy)
                      call AppendLine(R_xyz,R_xz,R_y,dxz)
                      call AppendLine(R_xyz,R_yz,R_x,dyz)
                   else
                      R_xyz = R_xz
                   end if
                else
                   R_xyz = R_xy
                end if
                dxyz = dx + dy + dz

                iaxes = count( (/ irx > 0, iry > 0, irz > 0 /) )

                R_xpdt = R_xyz
                do dt=1,nrt
                   t0 = mpi_wtime()
                   R_xpdt = CShiftGaugePlane(R_xpdt,that,1)
                   call WilsonLoop(W_x,R_xyz,T_xl,dxyz,dt,R_xpdt)
                   W_pp = sum(W_x)
                   call AllSum(W_pp,W_bar)
                   W_rt(irx,iry,irz,dt,ix,that) = W_bar / ( nlattice * multiplicity(iaxes)) 

                   t1 = mpi_wtime()
                   mpiprint '(4I4,F20.15)', irx, iry, irz, that, W_rt(irx,iry,irz,dt,ix,that)
                end do
                   
             end do
          end do

       end do

    end do

    deallocate(S_xdl)
    deallocate(T_xl)
    deallocate(W_x)
    deallocate(R_x)
    deallocate(R_y)
    deallocate(R_z)
    deallocate(R_xy)
    deallocate(R_xz)
    deallocate(R_yz)
    deallocate(R_xyz)
    deallocate(R_xpdt)

#undef R_xpdmu

  end subroutine GetWilsonLoops

  subroutine AppendLine(R_xy,R_x,R_y,dx)

    type(colour_matrix), dimension(:,:,:,:) :: R_xy,R_x,R_y
    integer :: mu,dx(nd)
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt

#define R_ix R_x(ix,iy,iz,it)
#define R_jy R_y(jx,jy,jz,it)
#define R_ixy R_xy(ix,iy,iz,it)

    do it=1,nt
       !jt = modc(it + dx(4),nt)
       do iz=1,nz
          jz = modc(iz + dx(3),nz)
          do iy=1,ny
             jy = modc(iy + dx(2),ny)
             do ix=1,nx
                jx = modc(ix + dx(1),nx)
                
                call MatPlusMatTimesMat(R_ixy,R_ix,R_jy)

             end do
          end do
       end do
    end do

#undef R_ix
#undef R_jy
#undef R_ixy

  end subroutine AppendLine

  subroutine WilsonLoop(W_x,R_x,T_xl,dx,dt,R_xpdt)

    real(dp), dimension(:,:,:,:) :: W_x
    type(colour_matrix), dimension(:,:,:,:) :: R_x,R_xpdt
    type(colour_matrix), dimension(:,:,:,:,:) :: T_xl
    integer :: dx(nd),dt
    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    type(colour_matrix) :: RT,RdagTdag

#define R_ix R_x(ix,iy,iz,it)
#define R_ixpdt R_xpdt(ix,iy,iz,it)
#define T_ix T_xl(ix,iy,iz,it,dt)
#define T_jx T_xl(jx,jy,jz,it,dt)
#define W_ix W_x(ix,iy,iz,it)

    do it=1,nt
       !jt = modc(it + dx(4),nt)
       do iz=1,nz
          jz = modc(iz + dx(3),nz)
          do iy=1,ny
             jy = modc(iy + dx(2),ny)
             do ix=1,nx
                jx = modc(ix + dx(1),nx)

                call MultiplyMatMat(RT,R_ix,T_jx)
                call MultiplyMatDagMatDag(RdagTdag,R_ixpdt,T_ix)
                call RealTraceMultMatMat(W_ix,RT,RdagTdag)
                !mpiprint '(A,4I5,2F20.10)', "W", ix,iy,iz,it, T_jx%Cl(1,1)

             end do
          end do
       end do
    end do


#undef R_ix
#undef R_ixpdt
#undef T_ix
#undef T_jx
#undef W_ix

  end subroutine WilsonLoop

  subroutine WriteWilsonLoops(W_rt,OutFile,that,nrx,nry,nrz,nrt)

    real(dp), dimension(:,0:,0:,:,:,:) :: W_rt
    character(len=*) :: OutFile
    integer :: that,nrx,nry,nrz,nrt
    integer :: id, jd
    real(dp) :: t0, t1


    if ( i_am_root ) then
       open(unit=200,file=OutFile,form="unformatted",action="write",status="new")
       write(200) nrx,nry,nrz,nrt,that
       do id=1,3
          write(200) W_rt(:,:,:,:,id,4)
       end do
       close(200)
    end if

  end subroutine WriteWilsonLoops

  subroutine MultiplyUU(M,L,R)

    type(colour_matrix), dimension(:,:,:,:), intent(in) :: L
    type(colour_matrix), dimension(:,:,:,:), intent(in) :: R
    type(colour_matrix), dimension(:,:,:,:), intent(inout) :: M
    integer :: ix,iy,iz,it

#define M_x M(ix,iy,iz,it)
#define L_x L(ix,iy,iz,it)
#define R_x R(ix,iy,iz,it)

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       M_x%Cl(1,1) = L_x%Cl(1,1)*R_x%Cl(1,1) + L_x%Cl(1,2)*R_x%Cl(2,1) + L_x%Cl(1,3)*R_x%Cl(3,1)
       M_x%Cl(2,1) = L_x%Cl(2,1)*R_x%Cl(1,1) + L_x%Cl(2,2)*R_x%Cl(2,1) + L_x%Cl(2,3)*R_x%Cl(3,1)
       M_x%Cl(3,1) = L_x%Cl(3,1)*R_x%Cl(1,1) + L_x%Cl(3,2)*R_x%Cl(2,1) + L_x%Cl(3,3)*R_x%Cl(3,1)

       M_x%Cl(1,2) = L_x%Cl(1,1)*R_x%Cl(1,2) + L_x%Cl(1,2)*R_x%Cl(2,2) + L_x%Cl(1,3)*R_x%Cl(3,2)
       M_x%Cl(2,2) = L_x%Cl(2,1)*R_x%Cl(1,2) + L_x%Cl(2,2)*R_x%Cl(2,2) + L_x%Cl(2,3)*R_x%Cl(3,2)
       M_x%Cl(3,2) = L_x%Cl(3,1)*R_x%Cl(1,2) + L_x%Cl(3,2)*R_x%Cl(2,2) + L_x%Cl(3,3)*R_x%Cl(3,2)

       M_x%Cl(1,3) = L_x%Cl(1,1)*R_x%Cl(1,3) + L_x%Cl(1,2)*R_x%Cl(2,3) + L_x%Cl(1,3)*R_x%Cl(3,3)
       M_x%Cl(2,3) = L_x%Cl(2,1)*R_x%Cl(1,3) + L_x%Cl(2,2)*R_x%Cl(2,3) + L_x%Cl(2,3)*R_x%Cl(3,3)
       M_x%Cl(3,3) = L_x%Cl(3,1)*R_x%Cl(1,3) + L_x%Cl(3,2)*R_x%Cl(2,3) + L_x%Cl(3,3)*R_x%Cl(3,3)
    end do; end do; end do; end do

#undef M_x
#undef L_x
#undef R_x

  end subroutine MultiplyUU

  subroutine MakePath(R_x,U_xd,dx,ir)

    type(colour_matrix), dimension(:,:,:,:) :: R_x
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: dx(nd),ir(nd-1)

    integer :: ix,iy,iz,it
    integer :: jx,jy,jz,jt
    integer :: dy(nd), dr(nd), cx(nd-1), ncx(nd-1), id, jd, kd, imin(1)
    type(colour_matrix) :: R_y
    integer :: iter 
#define R_ix R_x(ix,iy,iz,it)
#define U_jy U_xd(jx,jy,jz,jt,id)

    iter = 0


    dy = dx
    dr = 0
    cx = 0
    R_x = unit_matrix

    ! Determine the step sizes in each direction.
    do id=1,nd-1
       ncx(id) = dy(ir(id))
    end do
    imin = minloc(ncx,ncx > 0)
    id = imin(1)
    ncx = ncx/ncx(id)
    if ( count(ncx > 1) > 1 ) then
       imin = minloc(ncx,ncx > 1)
       jd = imin(1)
       where ( ncx > 1 ) 
          ncx = ncx/ncx(jd)
       end where
    end if

    ! Make the link paths.
    do while ( any(dy /= 0) )  

       !print '(A,4I4)', "a dx = ", dx
       !print '(A,4I4)', "a dy = ", dy
       !print '(A,6I4)', "a dr = ", dr, id, jd
       !print '(A,3I4)', "a cx = ", cx
       !print '(A,3I4)', "a ncx = ", ncx

       ! Select direction for next step.
       ! Do this by checking the current step count (cx) against the 
       ! step size in each direction (ncx).
       !print *, "loop"
       do jd=1,nd-1
          id = ir(jd)
          if ( cx(jd) < ncx(jd) ) then
             cx(jd) = cx(jd)+1
             !print *, "loop chose", id, jd
             exit  
          end if
       end do

       !print '(A,4I4)', "dx = ", dx
       !print '(A,4I4)', "dy = ", dy
       !print '(A,6I4)', "dr = ", dr, id, jd
       !print '(A,3I4)', "cx = ", cx
       !print '(A,3I4)', "ncx = ", ncx

       if ( all(cx == ncx) ) cx = 0

       do it=1,nt
          jt = modc(it + dr(4),nt)
          do iz=1,nz
             jz = modc(iz + dr(3),nz)
             do iy=1,ny
                jy = modc(iy + dr(2),ny)
                do ix=1,nx
                   jx = modc(ix + dr(1),nx)
                
                   R_y = R_ix
                   call MultiplyMatMat(R_ix,R_y,U_jy)

                end do
             end do
          end do
       end do

       dr(id) = dr(id)+1
       dy(id) = dy(id)-1

       ! Test if we have just exhausted a direction.
       ! If so, reset the counter in the next valid direction. 
       if ( dy(id) == 0 ) then
          cx(jd) = 0
          ncx(jd) = 0
          if ( dy(ir(modc(jd+1,3))) > 0 ) then
             cx(modc(jd+1,3)) = 0
          else
             cx(modc(jd+2,3)) = 0
          end if
       end if

       !iter = iter + 1
       !if ( iter > 8 ) stop
 
    end do

#undef R_ix
#undef R_jy
#undef R_ixy

  end subroutine MakePath

end module WilsonLoops
