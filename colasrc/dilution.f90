#include "defines.h"

module Dilution
  use Kinds
  use LatticeSize
  use MPIInterface
  use ColourTypes
  use SpinorTypes
  implicit none
  private

  type, public ::  DilutionType
     integer :: nv_c, nv_s, nv_t ! Number of solution vectors per index.
     integer :: nc_dil, ns_dil, nt_dil ! Number of non-trivial index values, post-dilution.
     integer :: dc, ds, dt ! Separation of non-trivial index values, post-dilution.
   contains
     procedure :: Initialise => InitialiseDilution
     procedure :: GetSource => GetDilutedSource
  end type DilutionType

  public :: InitialiseDilution, GetDilutedSource

contains

  subroutine InitialiseDilution(this,iColourDilute,iSpinDilute,iTimeDilute)
    class(DilutionType) :: this
    integer :: iColourDilute,iSpinDilute,iTimeDilute

    select case(iTimeDilute)
    case(0) ! No time dilution. 
       this%nv_t = 1
       this%nt_dil = nlt
       this%dt = 1
    case(1) ! Full time dilution.
       this%nv_t = nlt
       this%nt_dil = 1
       this%dt = 1
    case(2:) ! Interleave time dilution.
       this%nv_t = iTimeDilute
       this%nt_dil = nlt/iTimeDilute
       this%dt = iTimeDilute
       if ( modulo(nlt,iTimeDilute) /= 0 ) call Abort("InitialiseDilution: Invalid choice for iTimeDilute.")
    end select

    select case(iSpinDilute)
    case(0) ! No spin dilution. 
       this%nv_s = 1
       this%ns_dil = ns
       this%ds = 1
    case(1) ! Full spin dilution.
       this%nv_s = ns
       this%ns_dil = 1
       this%ds = 1
    case(2) ! Half spin dilution.
       this%nv_s = 2
       this%ns_dil = 2
       this%ds = 2
    case default
       call Abort("InitialiseDilution: Invalid choice for iSpinDilute.")
    end select

    select case(iColourDilute)
    case(0) ! No colour dilution.
       this%nv_c = 1
       this%nc_dil = nc
       this%dc = 1
    case(1) ! Full colour dilution.
       this%nv_c = nc
       this%nc_dil = 1
       this%dc = 1
    case default
       call Abort("InitialiseDilution: Invalid choice for iColourDilute.")
    end select

  end subroutine InitialiseDilution

  subroutine GetDilutedSource(this,rho,jc,js,jt,rho_noise)
    class(DilutionType) :: this
    type(colour_vector), dimension(:,:,:,:,:), intent(out) :: rho
    integer, intent(in) :: jc,js,jt
    type(colour_vector), dimension(:,:,:,:,:), intent(in) :: rho_noise

    integer :: ic,is,ix,iy,iz,it,ilt
    integer :: ic_dil, is_dil, it_dil

    rho = zero_vector
    is = js
    do is_dil=1,this%ns_dil
       ilt = jt
       do it_dil=1,this%nt_dil
          if ( (i_nt <= ilt) .and. (ilt <= j_nt) ) then
             it = ilt - i_nt + 1
             do iz=1,nz; do iy=1,ny; do ix=1,nx
                ic = jc
                do ic_dil=1,this%nc_dil
                   rho(ix,iy,iz,it,is)%cl(ic) = rho_noise(ix,iy,iz,it,is)%cl(ic)
                   ic = ic + this%dc
                end do
             end do; end do; end do
          end if
          ilt = ilt + this%dt
       end do
       is = is + this%ds
    end do

  end subroutine GetDilutedSource

end module Dilution
