
module EOWilsonMatrix_FW

  use Timer
  use MPIInterface
  use FermionField
  use FermionAlgebra


  
  use SpinorTypes
  use FermionTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  use ColourFieldOps
  implicit none
  private

  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:), private, allocatable :: Up_xd
  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), public :: kappa_FWWilson



  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean

  real(dp), public :: bcx_FWWilson = 1.0d0, bcy_FWWilson = 1.0d0, bcz_FWWilson = 1.0d0, bct_FWWilson = 1.0d0
  public :: DEOWilson_fw
  public :: DEOWilson_dag_fw
  public :: HsqEOWilson_fw
  public :: HEOWilson_fw
  public :: InitialiseEOWilsonOperator_fw
  public :: FinaliseEOWilsonOperator_fw

  public :: EOWilsonDag_fw
  public :: EOWilson_fw
  public :: x2eo
  public :: eo2x
  public :: phi_x2phi_fw
  public :: phi_fw2phi_x
  
  integer, dimension(:,:), allocatable :: i_xpmu, i_xmmu

contains

  subroutine InitialiseEOWilsonOperator_fw(U_xd , kappa, u0)
    ! begin args: U_xd , kappa, u0

    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: kappa
    real(dp), optional :: u0

    ! begin local_vars
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfic_sw, mfir, mfir_sm !coefficents to be absorbed.
    integer :: i_fw, i_xeo, j_xeo
    integer, dimension(nd) :: i_x, j_x, n_x

    !EO preconditioning is in the hopping parameter formalism.
    ! begin execution
    nlatt_eo = nx*ny*nz*nt/2
    if ( .not. allocated(Up_xd) ) allocate(Up_xd(nlatt_eo,nd,0:1))
    if ( .not. allocated(i_xpmu) ) allocate(i_xpmu(nlatt_eo,nd))
    if ( .not. allocated(i_xmmu) ) allocate(i_xmmu(nlatt_eo,nd))

    mfir = kappa
    if ( present(u0) ) mfir = mfir/u0

    kappa_FWWilson = kappa

    !Construct the forward and backward hop indices.
    n_x = (/ nx, ny, nz, nt /)
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       i_x = (/ ix,iy,iz,it /)
       i_fw = modulo(sum(i_x),2)
       i_xeo = x2eo(i_x)
       do mu=1,nd
          j_x = i_x

          j_x(mu) = modc(j_x(mu)+1,n_x(mu))
          j_xeo = x2eo(j_x)
          i_xpmu(i_xeo,mu) = j_xeo

          j_x(mu) = modc(j_x(mu)-1,n_x(mu))
          j_xeo = x2eo(j_x)
          i_xmmu(i_xeo,mu) = j_xeo

       end do
    end do; end do; end do; end do

    !Absorb the mean field improvement into the gauge fields.
    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_fw = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          Up_xd(i_xeo,mu,i_fw)%Cl = mfir*U_xd(ix,iy,iz,it,mu)%Cl
       end do; end do; end do; end do
    end do

    !call SetBoundaryConditions(Up_xd,bcx_FWWilson, bcy_FWWilson, bcz_FWWilson, bct_FWWilson)
    !call ShadowGaugeField(Up_xd,0)
  end subroutine InitialiseEOWilsonOperator_fw

  subroutine FinaliseEOWilsonOperator_fw

    if ( allocated(Up_xd) ) deallocate(Up_xd)
    if ( allocated(i_xpmu) ) deallocate(i_xpmu)
    if ( allocated(i_xmmu) ) deallocate(i_xmmu)
  end subroutine FinaliseEOWilsonOperator_fw

  subroutine phi_x2phi_fw(psi,psi_e,psi_o)
    type(colour_vector), dimension(:,:,:,:,:) :: psi
    type(dirac_fermion), dimension(:) :: psi_e, psi_o

    integer :: ix,iy,iz,it,is,i_fw,i_xeo,i_x(nd)

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_fw = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          if ( i_fw == 0 ) then
             psi_e(i_xeo)%cs(:,is) = psi(ix,iy,iz,it,is)%cl(:) 
          else
             psi_o(i_xeo)%cs(:,is) = psi(ix,iy,iz,it,is)%cl(:) 
          end if
       end do; end do; end do; end do
    end do
  end subroutine phi_x2phi_fw

  subroutine phi_fw2phi_x(psi_e,psi_o,psi)
    type(dirac_fermion), dimension(:) :: psi_e, psi_o
    type(colour_vector), dimension(:,:,:,:,:) :: psi

    integer :: ix,iy,iz,it,is,i_fw,i_xeo,i_x(nd)

    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          i_x = (/ ix,iy,iz,it /)
          i_fw = modulo(sum(i_x),2)
          i_xeo = x2eo(i_x)
          if ( i_fw == 0 ) then
             psi(ix,iy,iz,it,is)%cl(:) = psi_e(i_xeo)%cs(:,is) 
          else
             psi(ix,iy,iz,it,is)%cl(:) = psi_o(i_xeo)%cs(:,is)
          end if
       end do; end do; end do; end do
    end do
  end subroutine phi_fw2phi_x

  subroutine DEOWilson_dag_fw(phi,Dphi)
    !! Note: the EO clover action omits the T_ee term.
    ! begin args: phi, Dphi

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: phi_e, phi_o
    integer :: i_xeo,is

    ! begin execution
    allocate(phi_e(nlatt_eo))
    allocate(phi_o(nlatt_eo))

    call EOWilsonDag_fw(phi,phi_e,1)

    call EOWilsonDag_fw(phi_e,phi_o,0)

    Dphi = phi

    do i_xeo=1,nlatt_eo
       Dphi(i_xeo)%cs = Dphi(i_xeo)%cs - phi_o(i_xeo)%cs
    end do

    deallocate(phi_e)
    deallocate(phi_o)

  end subroutine DEOWilson_dag_fw

  subroutine HsqEOWilson_fw(phi,Hsqphi)
    ! begin args: phi, Hsqphi

    type(dirac_fermion), dimension(:) :: phi, Hsqphi

    ! begin local_vars
    type(dirac_fermion), dimension(:), allocatable :: Dphi

    ! begin execution
    allocate(Dphi(nlatt_eo))

    call HEOWilson_fw(phi,Dphi)
    call HEOWilson_fw(Dphi,Hsqphi)

    deallocate(Dphi)

  end subroutine HsqEOWilson_fw

  subroutine HEOWilson_fw(phi,Hphi)
    ! begin args: phi, Hphi

    type(dirac_fermion), dimension(:) :: phi, Hphi
    ! begin local_vars
    integer :: i_xeo

    ! begin execution

    call DEOWilson_fw(phi,Hphi)

    do i_xeo=1,nlatt_eo
       Hphi(i_xeo)%cs(:,3) = -Hphi(i_xeo)%cs(:,3)
       Hphi(i_xeo)%cs(:,4) = -Hphi(i_xeo)%cs(:,4)
    end do

  end subroutine HEOWilson_fw

  subroutine EOWilsonDag_fw(phi, Dphi, op_parity)
    ! begin args: phi, Dphi, op_parity

    type(dirac_fermion), dimension(:) :: phi
    type(dirac_fermion), dimension(:) :: Dphi

    integer :: op_parity

    ! begin local_vars
    integer :: i_xeo


    ! begin execution

    do i_xeo=1,nlatt_eo
       phi(i_xeo)%cs(:,3) = -phi(i_xeo)%cs(:,3)
       phi(i_xeo)%cs(:,4) = -phi(i_xeo)%cs(:,4)
    end do
    call EOWilson_fw(phi,Dphi,1-op_parity)

    do i_xeo=1,nlatt_eo
       phi(i_xeo)%cs(:,3) = -phi(i_xeo)%cs(:,3)
       phi(i_xeo)%cs(:,4) = -phi(i_xeo)%cs(:,4)

       Dphi(i_xeo)%cs(:,3) = -Dphi(i_xeo)%cs(:,3)
       Dphi(i_xeo)%cs(:,4) = -Dphi(i_xeo)%cs(:,4)
    end do
  end subroutine EOWilsonDag_fw

end module EOWilsonMatrix_FW
