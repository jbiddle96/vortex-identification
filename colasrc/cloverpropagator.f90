!! Filename : $Source$
!! Author : Waseem Kamleh
!! Created On : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag : $Name$
!! Revision : $Revision$
!! Update History : $Log$
/* clover.h */
module cloverPropagator
  use cloverMatrix
  use eocloverMatrix
  use BackgroundField
  use GaugeField
  use GaugeFieldMPIComms
  use FermionField
  use FermionFieldMPIComms
  use ConjGradSolvers
  use MPIInterface
  use RealFieldMPIComms
  use LatticeSize
  use SpinorTypes
  use FermionTypes
  use ColourTypes
  use Kinds
  use MatrixAlgebra
  use VectorAlgebra
  use ColourFieldOps
  use FermionAlgebra
  use CloverFmunu
  use TextIO
  use Strings
  implicit none
  private
/*  public :: GetQuarkProp */
  public :: WriteFermionActionParams_clover
  public :: ReadFermionActionParams_clover
  public :: GetFermionActionParams_clover
  public :: InitialiseFermionAction_clover
  public :: FinaliseFermionAction_clover
  public :: AddSSTSource_clover
  real(dp), public :: kappa_clover
  real(dp), public :: u0_clover = 1.0_dp
  real(dp), public :: c_sw_clover
contains
  subroutine WriteFermionActionParams_clover(file_unit)
    integer :: file_unit
    if ( i_am_root ) then
       !write(file_unit,'(f20.10,a)') kappa_clover, " !kappa_clover"
       write(file_unit,'(f20.10,a)') ff_bcx , " !ff_bcx "
       write(file_unit,'(f20.10,a)') ff_bcy , " !ff_bcy "
       write(file_unit,'(f20.10,a)') ff_bcz , " !ff_bcz "
       write(file_unit,'(f20.10,a)') ff_bct , " !ff_bct "
       write(file_unit,'(f20.10,a)') u0_clover , " !u0_clover "
       write(file_unit,'(f20.10,a)') c_sw_clover, " !c_sw_clover"
    end if
  end subroutine WriteFermionActionParams_clover
  subroutine ReadFermionActionParams_clover(file_unit)
    integer :: file_unit
    !call Get(kappa_clover,file_unit)
    call Get(ff_bcx ,file_unit)
    call Get(ff_bcy ,file_unit)
    call Get(ff_bcz ,file_unit)
    call Get(ff_bct ,file_unit)
    call Get(u0_clover ,file_unit)
    call Get(c_sw_clover,file_unit)
  end subroutine ReadFermionActionParams_clover
  subroutine GetFermionActionParams_clover(fermactfile)
    character(len=*) :: fermactfile
    integer :: file_unit
    call OpenTextfile(trim(fermactfile),file_unit,action='read')
    call ReadFermionActionParams_clover(file_unit)
    call CloseTextfile(file_unit)
    call Put("clover parameters")
    !call Put("Hopping parameter, kappa_clover ="//str(kappa_clover,24))
    call Put("Boundary conditions")
    call Put("bcx ="//str(ff_bcx,12,'(f12.7)')//", bcy ="//str(ff_bcy,12,'(f12.7)')//", bcz ="//str(ff_bcz,12,'(f12.7)')//", bct ="//str(ff_bct,12,'(f12.7)'))
    call Put("Mean link, u0_clover ="//str(u0_clover,24))
    call Put("Clover term c_sw="//str(c_sw_clover,24))
  end subroutine GetFermionActionParams_clover
  subroutine InitialiseFermionAction_clover(U_xd,kappa,kBFStrength,even_odd)
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    real(dp) :: kappa
    integer, optional :: kBFStrength
    logical, optional :: even_odd
    real(dp) :: u0
    integer :: k_B
    logical :: eo = .false.
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: UB_xd
    real(dp) :: c_sw
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: F_munu
    if ( present(kBFStrength) ) then
       k_B = kBFStrength
    else
       k_B = 0
    end if
    if ( present(even_odd) ) then
       eo = even_odd
    else
       eo = .false.
    end if
    u0 = u0_clover
    if ( u0 < 0.0d0 ) then
       if ( uzero < 1.0d0 ) then
          u0 = uzero
       else
          call GetUZero(U_xd,u0)
       end if
    end if
! call Put("Mean link value, u0 ="//str(u0,24))
    call AllocMatrixField(F_munu,"xyzt-",(/1,1,1,1,nplaq/))
    c_sw = c_sw_clover
    if ( k_B == 0 ) then
       call CalculateFmunuClover(F_munu,U_xd)
       if ( eo ) then
          call InitialiseFermionMatrix_eoclover(U_xd , F_munu, kappa, c_sw, u0)
       else
          call InitialiseFermionMatrix_clover(U_xd , F_munu, kappa, c_sw, u0)
       end if
    else
       call CreateGaugeField(UB_xd,1)
       UB_xd(1:nx,1:ny,1:nz,1:nt,:) = U_xd(1:nx,1:ny,1:nz,1:nt,:)
       call MultiplyBF(k_B,UB_xd)
       call CalculateFmunuClover(F_munu,UB_xd)
       if ( eo ) then
          call InitialiseFermionMatrix_eoclover(UB_xd , F_munu, kappa, c_sw, u0)
       else
          call InitialiseFermionMatrix_clover(UB_xd , F_munu, kappa, c_sw, u0)
       end if
       call DestroyGaugeField(UB_xd)
    end if
    kappa_clover = kappa
    call DeallocMatrixField(F_munu)
  end subroutine InitialiseFermionAction_clover
  subroutine FinaliseFermionAction_clover(even_odd)
    logical, optional :: even_odd
    logical :: eo
    if ( present(even_odd) ) then
       eo = even_odd
    else
       eo = .false.
    end if
    if ( eo ) then
       call FinaliseFermionMatrix_eoclover
    else
       call FinaliseFermionMatrix_clover
    end if
  end subroutine FinaliseFermionAction_clover
/*
  subroutine GetQuarkProp(rho,chi,cg_tol,cg_err,cg_iter)

    type(colour_vector), dimension(:,:,:,:,:) :: rho,chi
    real(dp) :: cg_tol, cg_err
    integer :: cg_iter

    _fermion_field_type_, allocatable :: Dchi
#ifdef _even_odd_
    _fermion_field_type_eo_, allocatable :: rho_e, rho_o, chi_e, chi_o
#endif


#ifdef _even_odd_
    allocate(chi_e(_fermion_field_extents_eo_))
    allocate(rho_e(_fermion_field_extents_eo_))

    allocate(chi_o(_fermion_field_extents_eo_))
    allocate(rho_o(_fermion_field_extents_eo_))

    allocate(Dchi(_fermion_field_extents_eo_))
#define _rho_ rho_o
#define _chi_ chi_o
#else
    allocate(Dchi(_fermion_field_extents_4x_))
#define _rho_ rho
#define _chi_ chi
#endif


#ifdef _even_odd_
    call RightILUMatrix_clover(chi,chi_e,chi_o)
    call InvLeftILUMatrix_clover(rho,rho_e,rho_o)
    chi_e = rho_e
#endif

    call BiCGStabInvert(_rho_, _chi_, cg_tol, cg_iter, Dclover)

#ifdef _even_odd_
    call InvRightILUMatrix_clover(chi_e,chi_o,chi)
#endif

    call Dclover(_chi_,Dchi)
    call PsiMinusPhi(Dchi,_rho_)

    cg_err = fermion_norm(Dchi)

    deallocate(Dchi)

#ifdef _even_odd_
    deallocate(chi_e)
    deallocate(rho_e)

    deallocate(chi_o)
    deallocate(rho_o)
#endif

  end subroutine GetQuarkProp
*/
  subroutine AddSSTSource_clover(chi,psi,U_xd,kappa,j_x,j_mu,it_s,p_x,currentType)
    type(colour_vector), dimension(:,:,:,:,:) :: chi !! The resulting current-inserted source. (Should be initialised externally)
    type(colour_vector), dimension(:,:,:,:,:) :: psi !! The source.
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd !! Links in the Dirac term.
    real(dp) :: kappa
    integer :: j_x(nd-1) !! spatial source position
    integer :: j_mu !! Lorentz index for the current.
    integer :: it_s !! time slice to insert the current.
    integer :: p_x(nd-1) !! momentum
    integer :: currentType
    !! The parameter currentType selects the level of current improvement
    !!
    !! currentType == 0 selects the naive non-conserved current
    !! currentType == 1 selects the standard conserved current
    !! see Electromagnetic form factors of hadrons.
    !! Terrence Drapper and R.M. Woloshyn.
    !! currentType == 2 NOT IMPLEMENTED: selects an improved conserved current for NNN type actions.
    !! currentType == 3 NOT IMPLEMENTED: selects the same improved conserved current for NNN type actions. Ultra Improvement is not coded.
    !! currentType == 4 selects the order-a four-divergenge term for improved conserved (FLI)Clover(4)-actions.
    !!
    !! Based on HPF code by : Derek B. Leinweber
    type(colour_matrix), dimension(:,:,:,:,:), allocatable :: U0_xd !! Links in the Dirac term.
    complex(dc), dimension(:,:,:), allocatable :: phase
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: phi, psi_pr, Gmu_psi
    type(colour_vector) :: UPsi, UGPsi
    integer :: ix,iy,iz,it,is,ilt
    integer :: kx,ky,kz,kt
    integer :: lx,ly,lz,lt
    integer :: irho,rho
    integer :: dmu(nd)
    real(dp) :: u0, u0_w !! Make sure these get implemented properly.
    real(dp) :: pm
    !kappa = qpsrc_sst%kappa
    !j_x = qpsrc_sst%j_x
    !j_mu = qpsrc_sst%j_mu
    !it_s = qpsrc_sst%it_s
    !p_x = qpsrc_sst%p_x
    !currentType = qpsrc_sst%currentType
    allocate(phase(nx,ny,nz))
    allocate(phi(nxs,nys,nzs,nts,ns))
    allocate(psi_pr(nxs,nys,nzs,nts,ns))
    allocate(Gmu_psi(nxs,nys,nzs,nts,ns))
    u0 = u0_clover
    u0_w = u0_clover
    call CreateGaugeField(U0_xd,1)
    U0_xd(1:nx,1:ny,1:nz,1:nt,:) = U_xd(1:nx,1:ny,1:nz,1:nt,:)
    call SetBoundaryConditions(U0_xd,ff_bcx, ff_bcy, ff_bcz, ff_bct)
    call ShadowGaugeField(U0_xd,1)
    psi_pr(1:nx,1:ny,1:nz,1:nt,:) = psi(1:nx,1:ny,1:nz,1:nt,:)
    call ShadowFermionField(psi_pr,1)
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       call GammaPhi(Gmu_psi(ix,iy,iz,it,:),psi(ix,iy,iz,it,:),j_mu)
    end do; end do; end do; end do
    call ShadowFermionField(Gmu_psi,1)
    if ( currentType == 0 ) then
       ! For Naive Current Insertion
       phi = zero_vector
       if ( i_nt <= it_s .and. it_s <= j_nt ) then
          it = it_s - i_nt + 1
          do iz=1,nz; do iy=1,ny; do ix=1,nx
             phi(ix,iy,iz,it,:)= Gmu_psi(ix,iy,iz,it,:)
          end do; end do; end do
       end if
       ! Wave Function Renormalization (Note: a factor of kappa is otherwise in the links already, and the 2 is explicit below)
       call AlphaPsi(phi,2.0d0*kappa)
       ! Create source: chi may already have some source data (and must be initialised by the calling routine.)
       phase = 0.0d0
       call AddPhaseFactor(phase, 0, 0, j_x, p_x)
       if ( i_nt <= it_s .and. it_s <= j_nt ) then
          it = it_s - i_nt + 1
          do is=1,ns
             do iz=1,nz; do iy=1,ny; do ix=1,nx
                chi(ix,iy,iz,it,is)%Cl = chi(ix,iy,iz,it,is)%Cl + phase(ix,iy,iz)*phi(ix,iy,iz,it,is)%Cl
             end do; end do; end do
          end do
       end if
    end if
    if ( currentType == 2 .or. currentType == 3 ) then
       call Put("--------------------------------------------------")
       call Put("WARNING: This SST current type is not implemented.")
       call Put("--------------------------------------------------")
       return
    end if
    if ( currentType == 1 .or. currentType == 4 ) then
       ! Standard Conserved Current Insertion
       if ( j_mu == 4 ) then
          !--- t direction ---------------------------------
          ! For standard Wilson action
          !
          ! phi(.) += - (r-Gamma_4) U(.) g(+)
          ! phi(-) += - (r-Gamma_4) U(-) g(.)
          !
          phi = zero_vector
          do ilt = it_s-1,it_s
             if ( i_nt <= ilt .and. ilt <= j_nt ) then
                it = ilt - i_nt + 1
                kt = mapt(it+1)
                do is=1,ns
                   do iz=1,nz; do iy=1,ny; do ix=1,nx
                      kz = iz; ky = iy; kx = ix
                      call MultiplyMatClrVec(UGpsi,U0_xd(ix,iy,iz,it,j_mu),Gmu_psi(kx,ky,kz,kt,is))
                      call MultiplyMatClrVec(Upsi,U0_xd(ix,iy,iz,it,j_mu),psi_pr(kx,ky,kz,kt,is))
                      phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl - (kappa/u0_w)*Upsi%Cl + (kappa/u0)*UGpsi%Cl
                   end do; end do; end do
                end do
             end if
          end do
          ! phi(+) += + (r+Gamma_4) U^dagger(.) g(.)
          ! phi(.) += + (r+Gamma_4) U^dagger(-) g(-)
          do ilt = it_s,it_s+1
             if ( i_nt <= ilt .and. ilt <= j_nt ) then
                it = ilt - i_nt + 1
                lt = mapt(it-1)
                do is=1,ns
                   do iz=1,nz; do iy=1,ny; do ix=1,nx
                      lz = iz; ly = iy; lx = ix
                      call MultiplyMatDagClrVec(UGpsi,U0_xd(lx,ly,lz,lt,j_mu),Gmu_psi(lx,ly,lz,lt,is))
                      call MultiplyMatDagClrVec(Upsi,U0_xd(lx,ly,lz,lt,j_mu),psi_pr(lx,ly,lz,lt,is))
                      phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl + (kappa/u0_w)*Upsi%Cl + (kappa/u0)*UGpsi%Cl
                   end do; end do; end do
                end do
             end if
          end do
          ! Create source: chi may already have some source data (and must be initialised by the calling routine.)
          ! Divide by two to average over forward and backward hopping steps
          phase = 0.0d0
          call AddPhaseFactor(phase, 0, 0, j_x, p_x)
          do ilt = it_s-1,it_s+1
             if ( i_nt <= ilt .and. ilt <= j_nt ) then
                it = ilt - i_nt + 1
                do is=1,ns
                   do iz=1,nz; do iy=1,ny; do ix=1,nx
                      chi(ix,iy,iz,it,is)%Cl = chi(ix,iy,iz,it,is)%Cl + 0.5d0*phase(ix,iy,iz)*phi(ix,iy,iz,it,is)%Cl
                   end do; end do; end do
                end do
             end if
          end do
       else
          ! We're doing a spatial direction
          ! phi_p = ( - (r-Gamma_mu) U(.) g(+) ) (exp(iq.r) + exp(iq.(r+mu)))/2
          ! phi_m = ( + (r+Gamma_mu) U^dagger(-) g(-) ) (exp(iq.r) + exp(iq.(r-mu)))/2
          ! Note: The phase factors for forward and backward hopping steps will be different
          dmu = 0
          dmu(j_mu) = 1
          ilt = it_s
          if ( i_nt <= ilt .and. ilt <= j_nt ) then
             it = ilt - i_nt + 1
             kt = it
             lt = it
             ! Forward term.
             phi = zero_vector
             do is=1,ns
                do iz=1,nz
                   kz = mapz(iz + dmu(3))
                   do iy=1,ny
                      ky = mapy(iy + dmu(2))
                      do ix=1,nx
                         kx = mapx(ix + dmu(1))
                         call MultiplyMatClrVec(UGpsi,U0_xd(ix,iy,iz,it,j_mu),Gmu_psi(kx,ky,kz,kt,is))
                         call MultiplyMatClrVec(Upsi,U0_xd(ix,iy,iz,it,j_mu),psi_pr(kx,ky,kz,kt,is))
                         phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl - (kappa/u0_w)*Upsi%Cl + (kappa/u0)*UGpsi%Cl
                      end do
                   end do
                end do
             end do
             phase = 0.0d0
             call AddPhaseFactor(phase, 0, 0, j_x, p_x)
             call AddPhaseFactor(phase, j_mu, +1, j_x, p_x)
             ! Create source: chi may already have some source data (and must be initialised by the calling routine.)
             ! Divide by two to average over forward and backward hopping steps
             do is=1,ns
                do iz=1,nz; do iy=1,ny; do ix=1,nx
                   chi(ix,iy,iz,it,is)%Cl = chi(ix,iy,iz,it,is)%Cl + 0.5d0*phase(ix,iy,iz)*phi(ix,iy,iz,it,is)%Cl
                end do; end do; end do
             end do
             ! Backward term.
             phi = zero_vector
             do is=1,ns
                do iz=1,nz
                   lz = mapz(iz - dmu(3))
                   do iy=1,ny
                      ly = mapy(iy - dmu(2))
                      do ix=1,nx
                         lx = mapx(ix - dmu(1))
                         call MultiplyMatDagClrVec(UGpsi,U0_xd(lx,ly,lz,lt,j_mu),Gmu_psi(lx,ly,lz,lt,is))
                         call MultiplyMatDagClrVec(Upsi,U0_xd(lx,ly,lz,lt,j_mu),psi_pr(lx,ly,lz,lt,is))
                         phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl + (kappa/u0_w)*Upsi%Cl + (kappa/u0)*UGpsi%Cl
                      end do
                   end do
                end do
             end do
             phase = 0.0d0
             call AddPhaseFactor(phase, 0, 0, j_x, p_x)
             call AddPhaseFactor(phase, j_mu, -1, j_x, p_x)
             ! Create source: chi may already have some source data (and must be initialised by the calling routine.)
             ! Divide by two to average over forward and backward hopping steps
             do is=1,ns
                do iz=1,nz; do iy=1,ny; do ix=1,nx
                   chi(ix,iy,iz,it,is)%Cl = chi(ix,iy,iz,it,is)%Cl + 0.5d0*phase(ix,iy,iz)*phi(ix,iy,iz,it,is)%Cl
                end do; end do; end do
             end do
          end if
          call MPIBarrier
       end if ! Space vs time
    end if ! current_type
    if ( currentType == 4 ) then
       !
       ! Calculate the order-a four-divergence term for improved conserved (FLI)Clover(4)-actions.
       do irho = 1,3
          rho = modc(j_mu + irho,nd)
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             call SigmaPhi(Gmu_psi(ix,iy,iz,it,:),psi(ix,iy,iz,it,:),rho,j_mu)
          end do; end do; end do; end do
          call ShadowFermionField(Gmu_psi,1)
          if ( rho /= 4 ) then ! rho is a Spatial direction
             ! phi_p = ( r sigma_{rho,j_mu} U(.) g(+) ) (+exp(iq.r) - exp(iq.(r+mu)))/2
             ! phi_m = ( r sigma_{rho,j_mu} U^dagger(-) g(-) ) (-exp(iq.r) + exp(iq.(r-mu)))/2
             dmu = 0
             dmu(rho) = 1
             ilt = it_s
             if ( i_nt <= ilt .and. ilt <= j_nt ) then
                it = ilt - i_nt + 1
                kt = it
                lt = it
                ! Forward term.
                phi = zero_vector
                do is=1,ns
                   do iz=1,nz
                      kz = mapz(iz + dmu(3))
                      do iy=1,ny
                         ky = mapy(iy + dmu(2))
                         do ix=1,nx
                            kx = mapx(ix + dmu(1))
                            call MultiplyMatClrVec(UGpsi,U0_xd(ix,iy,iz,it,rho),Gmu_psi(kx,ky,kz,kt,is))
                            phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl + (kappa/u0_w)*UGpsi%Cl
                         end do
                      end do
                   end do
                end do
                phase = 0.0d0
                call AddPhaseFactor(phase, rho, +1, j_x, p_x)
                phase = -phase
                call AddPhaseFactor(phase, 0, 0, j_x, p_x)
                ! Create source: chi may already have some source data (and must be initialised by the calling routine.)
                ! Divide by two to average over forward and backward hopping steps
                do is=1,ns
                   do iz=1,nz; do iy=1,ny; do ix=1,nx
                      chi(ix,iy,iz,it,is)%Cl = chi(ix,iy,iz,it,is)%Cl + 0.5d0*phase(ix,iy,iz)*phi(ix,iy,iz,it,is)%Cl
                   end do; end do; end do
                end do
                ! Backward term.
                phi = zero_vector
                do is=1,ns
                   do iz=1,nz
                      lz = mapz(iz - dmu(3))
                      do iy=1,ny
                         ly = mapy(iy - dmu(2))
                         do ix=1,nx
                            lx = mapx(ix - dmu(1))
                            call MultiplyMatDagClrVec(UGpsi,U0_xd(lx,ly,lz,lt,rho),Gmu_psi(lx,ly,lz,lt,is))
                            phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl + (kappa/u0_w)*UGpsi%Cl
                         end do
                      end do
                   end do
                end do
                phase = 0.0d0
                call AddPhaseFactor(phase, 0, 0, j_x, p_x)
                phase = -phase
                call AddPhaseFactor(phase, rho, -1, j_x, p_x)
                ! Create source: chi may already have some source data (and must be initialised by the calling routine.)
                ! Divide by two to average over forward and backward hopping steps
                do is=1,ns
                   do iz=1,nz; do iy=1,ny; do ix=1,nx
                      chi(ix,iy,iz,it,is)%Cl = chi(ix,iy,iz,it,is)%Cl + 0.5d0*phase(ix,iy,iz)*phi(ix,iy,iz,it,is)%Cl
                   end do; end do; end do
                end do
             end if
          else ! rho=4, the time direction.
             !--- t direction --------------
             ! phi(.) += ( + r sigma_{4,j_mu} U(.) g(+) ) (+exp(iq.r))
             ! phi(-) += ( - r sigma_{4,j_mu} U(-) g(.) ) (+exp(iq.r))
             phi = zero_vector
             do ilt = it_s-1,it_s
                pm = (-1.0d0)**(it_s - ilt) !Adjust for the sign difference between the first and second terms.
                if ( i_nt <= ilt .and. ilt <= j_nt ) then
                   it = ilt - i_nt + 1
                   kt = mapt(it+1)
                   do is=1,ns
                      do iz=1,nz; do iy=1,ny; do ix=1,nx
                         kz = iz; ky = iy; kx = ix
                         call MultiplyMatClrVec(UGpsi,U0_xd(ix,iy,iz,it,rho),Gmu_psi(kx,ky,kz,kt,is))
                         phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl + pm*(kappa/u0_w)*UGpsi%Cl
                      end do; end do; end do
                   end do
                end if
             end do
             ! phi(+) += ( + r sigma_{4,j_mu} U^dagger(.) g(.) ) (+exp(iq.r))
             ! phi(.) += ( - r sigma_{4,j_mu} U^dagger(-) g(-) ) (+exp(iq.r))
             do ilt = it_s,it_s+1
                pm = (-1.0d0)**(ilt - it_s) !Adjust for the sign difference between the first and second terms.
                if ( i_nt <= ilt .and. ilt <= j_nt ) then
                   it = ilt - i_nt + 1
                   lt = mapt(it-1)
                   do is=1,ns
                      do iz=1,nz; do iy=1,ny; do ix=1,nx
                         lz = iz; ly = iy; lx = ix
                         call MultiplyMatDagClrVec(UGpsi,U0_xd(lx,ly,lz,lt,rho),Gmu_psi(lx,ly,lz,lt,is))
                         phi(ix,iy,iz,it,is)%Cl = phi(ix,iy,iz,it,is)%Cl - pm*(kappa/u0_w)*UGpsi%Cl
                      end do; end do; end do
                   end do
                end if
             end do
             ! Create source: chi may already have some source data (and must be initialised by the calling routine.)
             ! Divide by two to average over forward and backward hopping steps
             phase = 0.0d0
             call AddPhaseFactor(phase, 0, 0, j_x, p_x)
             do ilt = it_s-1,it_s+1
                if ( i_nt <= ilt .and. ilt <= j_nt ) then
                   it = ilt - i_nt + 1
                   do is=1,ns
                      do iz=1,nz; do iy=1,ny; do ix=1,nx
                         chi(ix,iy,iz,it,is)%Cl = chi(ix,iy,iz,it,is)%Cl + 0.5d0*phase(ix,iy,iz)*phi(ix,iy,iz,it,is)%Cl
                      end do; end do; end do
                   end do
                end if
             end do
          end if
       end do
    end if ! Current type
    deallocate(phase)
    deallocate(phi)
    deallocate(psi_pr)
    deallocate(Gmu_psi)
    call DestroyGaugeField(U0_xd)
  contains
    subroutine AddPhaseFactor(phase, imu, is, j_x, p_x)
      complex(dc), dimension(:,:,:) :: phase
      integer :: imu, is
      integer :: j_x(nd-1) ! spatial source position
      integer :: p_x(nd-1) ! Momentum
      ! Momentum based phase factor
      ! phase must be initialized prior to calling this routine
      ! Used by ApplySST
      integer :: ix,iy,iz,it
      integer :: ilx,ily,ilz
      real(dp) :: qx, qy, qz
      integer :: jxs, jys, jzs ! Shifted values for spatial directions.
      qx = ( 2.0d0 * pi * p_x(1) ) / nlx
      qy = ( 2.0d0 * pi * p_x(2) ) / nly
      qz = ( 2.0d0 * pi * p_x(3) ) / nlz
      jxs = j_x(1)
      jys = j_x(2)
      jzs = j_x(3)
      select case(imu)
      case(1)
         jxs = jxs - is
      case(2)
         jys = jys - is
      case(3)
         jzs = jzs - is
      end select
      ! We'll accumulate here to make it easy to build up combinations of shifts
      do iz=1,nz
         ilz = i_nz + iz - 1
         do iy=1,ny
            ily = i_ny + iy - 1
            do ix=1,nx
               ilx = i_nx + ix - 1
               phase(ix,iy,iz) = phase(ix,iy,iz) + exp( i*(qx*(ilx-jxs) + qy*(ily-jys) + qz*(ilz-jzs)) )
            end do
         end do
      end do
    end subroutine AddPhaseFactor
  end subroutine AddSSTSource_clover
end module cloverPropagator
