!! Filename : $Source$
!! Author : Waseem Kamleh
!! Created On : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag : $Name$
!! Revision : $Revision$
!! Update History : $Log$

!! Filename : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/chiralfliwoperator.f90,v $
!! Author : Waseem Kamleh
!! Created On : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag : $Name: $
!! Revision : $Revision: 0.1 $
!! Update History : $Log: chiralfliwoperator.f90,v $
!! Update History : Revision 0.1 2004/07/21 07:15:33 wkamleh
!! Update History : Orion version
!! Update History :
!! Update History : Revision 1.1.1.2 2004/06/20 07:42:01 wkamleh
!! Update History : Hydra version
!! Update History :

!! Filename : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/chiralfliwoperator.f90,v $
!! Author : Waseem Kamleh
!! Created On : Sun Jun 20 17:02:06 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag : $Name: $
!! Revision : $Revision: 0.1 $
!! Update History : $Log: chiralfliwoperator.f90,v $
!! Update History : Revision 0.1 2004/07/21 07:15:33 wkamleh
!! Update History : Orion version
!! Update History :
!! Update History : Revision 1.1.1.2 2004/06/20 07:42:01 wkamleh
!! Update History : Hydra version
!! Update History :
module ChiralFLIWOperator

  use Timer
  use MPIInterface
  use FatLinks
  use FermionField
  use SpinorTypes
  use GaugeFieldMPIComms
  use GaugeField
  use LatticeSize
  use Kinds
  use ColourTypes
  implicit none
  private

  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: Up_xd
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: Um_xd

  !MFI Fat Link Wilson fermions using negative mass term, in Chiral basis
  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), private :: m_r
  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean

  public :: InitialiseFLIWOperator
  public :: FLIWOperate
  public :: SqFLIWOperate
  public :: Dfliw
  public :: Dfliwdag

contains

  subroutine InitialiseFLIWOperator(U_xd, UFL_xd, u0, u0fl, mass, bct)
    ! begin args: U_xd, UFL_xd, u0, u0fl, mass, bct

    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed

    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd, UFL_xd

    ! begin local_vars
    real(DP) :: mass, c_sw, u0, u0fl, bct
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfic_sw, mfir, mfir_fl !coefficents to be absorbed.

    type(colour_matrix) :: U_mu, Ufl_mu ! temporary storage
    type(colour_matrix), dimension(nplaq) :: F_x

    ! begin execution

    m_f = mass

    Up_xd(1:nx,1:ny,1:nz,1:nt,:) = U_xd(1:nx,1:ny,1:nz,1:nt,:)
    Um_xd(1:nx,1:ny,1:nz,1:nt,:) = UFL_xd(1:nx,1:ny,1:nz,1:nt,:)

    mfir = (0.25d0/u0)
    mfir_fl = (0.25d0/u0fl)

    !Absorb the mean field improvement into the gauge fields.
    !Define symmetrised and antisymmetrised gauge fields for efficiency (halve multiplies)

    if ( bct/=1.0d0 ) then
       call SetBoundaryConditions(Up_xd,1.0d0,1.0d0,1.0d0,bct)
       call SetBoundaryConditions(Um_xd,1.0d0,1.0d0,1.0d0,bct)
    end if

    do mu=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          U_mu%Cl = mfir*Up_xd(ix,iy,iz,it,mu)%Cl
          Ufl_mu%Cl = mfir_fl*Um_xd(ix,iy,iz,it,mu)%Cl

          Up_xd(ix,iy,iz,it,mu)%Cl = U_mu%Cl + Ufl_mu%Cl

          Um_xd(ix,iy,iz,it,mu)%Cl = U_mu%Cl - Ufl_mu%Cl

       end do; end do; end do; end do
    end do

    call ShadowGaugeField(Up_xd,0)
    call ShadowGaugeField(Um_xd,0)


  end subroutine InitialiseFLIWOperator

  subroutine FLIWOperate(phi, Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    ! begin local_vars
    integer :: ix,iy,iz,it,is
    integer :: jx,jy,jz,jt,js
    complex(dc) :: psi_x
    type(colour_vector) :: Gammaphi

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

    if (timing) intime = mpi_wtime()
    commtime = 0.0d0





    m_r = 4.0d0 - m_f
    !mu = 1





    !G_1^+ Um phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; ix=nx; do jx=1,nx

          Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

          psi_x = Um_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Um_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Um_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + cmplx(-aimag(psi_x),real(psi_x),dc)

          ix = jx

       end do; end do; end do; end do
    end do

    !G_1^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; ix=nx; do jx=1,nx

          Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

          psi_x = Up_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Up_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Up_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + cmplx(-aimag(psi_x),real(psi_x),dc)

          ix = jx

       end do; end do; end do; end do
    end do

    !mu = 2





    !G_2^+ Um phi_xpmu

    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; iy=ny; do jy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) - pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) - pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) - pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

          psi_x = Um_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*psi_x

          psi_x = Um_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*psi_x

          psi_x = Um_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*psi_x

       end do; iy=jy; end do; end do; end do
    end do

    !G_2^- Up phi_xpmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; iy=ny; do jy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) + pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) + pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) + pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

          psi_x = Up_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*psi_x

          psi_x = Up_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*psi_x

          psi_x = Up_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*psi_x

       end do; iy=jy; end do; end do; end do
    end do
    !mu = 3





    !G_3^+ Um phi_xpmu

    pm(1) = 1.0d0
    pm(2) = -1.0d0
    pm(3) = 1.0d0
    pm(4) = -1.0d0

    do is=1,nsp
       js = is+2



       do it=1,nt; do iz=1,nz; jz=mapz(iz+1); do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

          psi_x = Um_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Um_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Um_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

       end do; end do; end do; end do
    end do

    !G_3^- Up phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; do iz=1,nz; jz=mapz(iz+1); do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

          psi_x = Up_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Up_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = Up_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

       end do; end do; end do; end do
    end do
    !mu = 4





    !G_4^+ Um phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jt=mapt(it+1); do iz=1,nz; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) + phi(ix,iy,iz,jt,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) + phi(ix,iy,iz,jt,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) + phi(ix,iy,iz,jt,js)%Cl(3)

          psi_x = Um_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + psi_x

          psi_x = Um_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + psi_x

          psi_x = Um_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Um_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + psi_x

       end do; end do; end do; end do
    end do

    !G_4^- Up phi_xpmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jt=mapt(it+1); do iz=1,nz; do iy=1,ny; do ix=1,nx


          Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) - phi(ix,iy,iz,jt,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) - phi(ix,iy,iz,jt,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) - phi(ix,iy,iz,jt,js)%Cl(3)

          psi_x = Up_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + psi_x

          psi_x = Up_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + psi_x

          psi_x = Up_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Up_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + psi_x

       end do; end do; end do; end do
    end do
    !mu = 1





    !G_1^- Um phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jx=nx; do ix=1,nx

          Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) + cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

          psi_x = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Um_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - cmplx(-aimag(psi_x),real(psi_x),dc)

          jx = ix

       end do; end do; end do; end do
    end do

    !G_1^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jx=nx; do ix=1,nx

          Gammaphi%Cl(1) = phi(jx,iy,iz,it,is)%Cl(1) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(1)),real(phi(jx,iy,iz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(jx,iy,iz,it,is)%Cl(2) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(2)),real(phi(jx,iy,iz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(jx,iy,iz,it,is)%Cl(3) - cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl(3)),real(phi(jx,iy,iz,it,js)%Cl(3)),dc)

          psi_x = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Up_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - cmplx(-aimag(psi_x),real(psi_x),dc)

          jx = ix

       end do; end do; end do; end do
    end do

    !mu = 2





    !G_2^- Um phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; jy=ny; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) + pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) + pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) + pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

          psi_x = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*psi_x

          psi_x = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*psi_x

          psi_x = conjg(Um_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*psi_x

       end do; jy=iy; end do; end do; end do
    end do

    !G_2^+ Up phi_xmmu

    do is=1,nsp
       js = 5-is
       do it=1,nt; do iz=1,nz; jy=ny; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,jy,iz,it,is)%Cl(1) - pm(is)*phi(ix,jy,iz,it,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,jy,iz,it,is)%Cl(2) - pm(is)*phi(ix,jy,iz,it,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,jy,iz,it,is)%Cl(3) - pm(is)*phi(ix,jy,iz,it,js)%Cl(3)

          psi_x = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*psi_x

          psi_x = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*psi_x

          psi_x = conjg(Up_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*psi_x

       end do; jy=iy; end do; end do; end do
    end do







    !mu = 3





    !G_3^- Um phi_xmmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jz=nz; do iz=1,nz; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) + pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

          psi_x = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Um_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

       end do; end do; jz=iz; end do; end do
    end do

    !G_3^+ Up phi_xmmu

    do is=1,nsp
       js = is+2



       do it=1,nt; jz=nz; do iz=1,nz; do iy=1,ny; do ix=1,nx


          Gammaphi%Cl(1) = phi(ix,iy,jz,it,is)%Cl(1) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(1)),real(phi(ix,iy,jz,it,js)%Cl(1)),dc)
          Gammaphi%Cl(2) = phi(ix,iy,jz,it,is)%Cl(2) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(2)),real(phi(ix,iy,jz,it,js)%Cl(2)),dc)
          Gammaphi%Cl(3) = phi(ix,iy,jz,it,is)%Cl(3) - pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl(3)),real(phi(ix,iy,jz,it,js)%Cl(3)),dc)

          psi_x = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

          psi_x = conjg(Up_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - pm(js)*cmplx(-aimag(psi_x),real(psi_x),dc)

       end do; end do; jz=iz; end do; end do
    end do
    !mu = 4





    !G_4^- Um phi_xmmu

    do is=1,nsp
       js = is+2



       jt=nt

       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) - phi(ix,iy,iz,jt,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) - phi(ix,iy,iz,jt,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) - phi(ix,iy,iz,jt,js)%Cl(3)

          psi_x = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - psi_x

          psi_x = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - psi_x

          psi_x = conjg(Um_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Um_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - psi_x

       end do; end do; end do; jt=it; end do
    end do

    !G_4^+ Up phi_xmmu

    do is=1,nsp
       js = is+2



       jt=nt

       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) + phi(ix,iy,iz,jt,js)%Cl(1)
          Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) + phi(ix,iy,iz,jt,js)%Cl(2)
          Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) + phi(ix,iy,iz,jt,js)%Cl(3)

          psi_x = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - psi_x

          psi_x = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - psi_x

          psi_x = conjg(Up_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Up_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x
          Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - psi_x

       end do; end do; end do; jt=it; end do
    end do

    !Multiply by gamma_5
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       Dphi(ix,iy,iz,it,3)%Cl = -Dphi(ix,iy,iz,it,3)%Cl
       Dphi(ix,iy,iz,it,4)%Cl = -Dphi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do
    if (timing) then
       outtime = mpi_wtime()
       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    end if

    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
    waste = commtime/(outtime - intime)
    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)



  end subroutine FLIWOperate

  subroutine SqFLIWOperate(phi,Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Dsqphi

    ! begin execution
    allocate(Dsqphi(nxp,nyp,nzp,ntp,ns))

    call FLIWOperate(phi, Dphi)

    call FLIWOperate(Dphi, Dsqphi)

    Dphi = Dsqphi

    deallocate(Dsqphi)

  end subroutine SqFLIWOperate

  subroutine Dfliw(phi,Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    call FLIWOperate(phi,Dphi)

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       Dphi(ix,iy,iz,it,3)%Cl = -Dphi(ix,iy,iz,it,3)%Cl
       Dphi(ix,iy,iz,it,4)%Cl = -Dphi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do


  end subroutine Dfliw

  subroutine Dfliwdag(phi,Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi
    ! begin local_vars
    integer :: ix,iy,iz,it

    ! begin execution

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       phi(ix,iy,iz,it,3)%Cl = -phi(ix,iy,iz,it,3)%Cl
       phi(ix,iy,iz,it,4)%Cl = -phi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do

    call FLIWOperate(phi,Dphi)

    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx
       phi(ix,iy,iz,it,3)%Cl = -phi(ix,iy,iz,it,3)%Cl
       phi(ix,iy,iz,it,4)%Cl = -phi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do


  end subroutine Dfliwdag





end module ChiralFLIWOperator
