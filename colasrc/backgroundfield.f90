#include "defines.h"

module BackgroundField
  use GaugeField
  use ColourTypes
  use GaugeFieldMPIComms
  use LatticeSize
  use Kinds
  use MPIInterface
  implicit none
  private

  public :: MultiplyBF, MultiplyAltBF
  public :: ApplyBF, ApplyAltBF

contains

  subroutine MultiplyBF(k_B,U_xd,Usm_xd)
    ! begin args: k_B, _links_
    integer :: k_B
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:), optional :: Usm_xd

    ! begin local_vars
    complex(dc), dimension(:,:,:,:,:), allocatable :: U_ext
    integer :: ilx,ily,ilz,ilt
    real(dp) :: w_B
    integer :: ix,iy,iz,it,id,ic

    ! Applying a constant background photon field in the z direction.

    ! begin execution
    allocate(U_ext(nxp,nyp,nzp,ntp,nd))

    w_B = (2*pi*k_B)/real(nlx*nly)

    ! Create the background field.
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       ! Convert sublattice to lattice coordinate ily.
       ! ily must be indexed from 0, otherwise twisted BCS are introduced.
       ily = (iy-1) + (i_ny-1)
       
       U_ext(ix,iy,iz,it,1) = exp(-i*w_B*ily)
    end do; end do; end do; end do

    do id=2,4
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          U_ext(ix,iy,iz,it,id) = 1.0d0
       end do; end do; end do; end do
    end do

    ! Check if the boundary is on this processor.
    if ( j_ny == nly ) then
       do it=1,nt; do iz=1,nz; do ix=1,nx
          ! Convert sublattice to lattice coordinate ilx.
          ! ilx must be indexed from 0, otherwise twisted BCS are introduced.
          ilx = (ix-1) + (i_nx-1)

          U_ext(ix,ny,iz,it,2) = exp(i*w_B*nly*ilx)
       end do; end do; end do
    end if

    ! Apply the background field.
    do id=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          U_xd(ix,iy,iz,it,id)%Cl = U_ext(ix,iy,iz,it,id)*U_xd(ix,iy,iz,it,id)%Cl
       end do; end do; end do; end do
    end do
    call ShadowGaugeField(U_xd,1)

    if (present(Usm_xd)) then
       ! Apply the background field.
       do id=1,nd
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             Usm_xd(ix,iy,iz,it,id)%Cl = U_ext(ix,iy,iz,it,id)*Usm_xd(ix,iy,iz,it,id)%Cl
          end do; end do; end do; end do
       end do
       call ShadowGaugeField(Usm_xd,1)
    end if

    deallocate(U_ext)

  end subroutine MultiplyBF

  subroutine MultiplyAltBF(k_B,U_xd,Usm_xd)
    ! begin args: k_B, _links_
    integer :: k_B
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:), optional :: Usm_xd
    
    ! begin local_vars
    complex(dc), dimension(:,:,:,:,:), allocatable :: U_ext
    integer :: ilx,ily,ilz,ilt
    real(dp) :: w_B
    integer :: ix,iy,iz,it,id,ic

    ! Applying a constant background photon field in the z direction.

    ! begin execution
    allocate(U_ext(nxp,nyp,nzp,ntp,nd))

    w_B = (2*pi*k_B)/real(nlx*nly)

    !Initialise to the identity.
    U_ext(:,:,:,:,:) = 1.0d0

    ! Create the background field.
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       ! Convert sublattice to lattice coordinate ilx.
       ! ilx must be indexed from 0, otherwise twisted BCS are introduced.
       ilx = ix + i_nx - 1

       U_ext(ix,iy,iz,it,2) = exp(+i*w_B*ilx)
    end do; end do; end do; end do

    ! Check if the boundary is on this processor.
    if ( j_nx == nlx ) then
       do it=1,nt; do iz=1,nz; do iy=1,ny
          ! Convert sublattice to lattice coordinate ily.
          ! ily must be indexed from 0, otherwise twisted BCS are introduced.
          ily = iy + i_ny - 1
          
          U_ext(nx,iy,iz,it,1) = exp(-i*w_B*nlx*ily)
       end do; end do; end do
    end if

    ! Apply the background field.
    do id=1,nd
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          U_xd(ix,iy,iz,it,id)%Cl = U_ext(ix,iy,iz,it,id)*U_xd(ix,iy,iz,it,id)%Cl
       end do; end do; end do; end do
    end do
    call ShadowGaugeField(U_xd,1)

    if (present(Usm_xd)) then
       ! Apply the background field.
       do id=1,nd
          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
             Usm_xd(ix,iy,iz,it,id)%Cl = U_ext(ix,iy,iz,it,id)*Usm_xd(ix,iy,iz,it,id)%Cl
          end do; end do; end do; end do
       end do
       call ShadowGaugeField(Usm_xd,1)
    end if

    deallocate(U_ext)

  end subroutine MultiplyAltBF

  subroutine ApplyBF(iB,nB,U_xd,Usm_xd)
    ! begin args: iB, nB, _links_

    ! begin local_vars
    integer :: iB, nB
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:), optional :: Usm_xd
  
    integer, dimension(nB) :: k_B
    integer :: jB

    ! Preserve the old style background field enumeration,
    ! with each field being -2 times the previous one.

    ! Applying a constant background photon field in the z direction.
    k_B(1) = 0
    k_B(2) = 1
    do jB=3,nB
       k_B(jB) = -2*k_B(jB-1)
    end do

    if ( iB > 1) call MultiplyBF(k_B(iB),U_xd,Usm_xd)

  end subroutine ApplyBF

  subroutine ApplyAltBF(iB,nB,U_xd,Usm_xd)
    ! begin args: iB, nB, _links_

    ! begin local_vars
    integer :: iB, nB
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    type(colour_matrix), dimension(:,:,:,:,:), optional :: Usm_xd
  
    integer, dimension(nB) :: k_B
    integer :: jB

    ! Preserve the old style background field enumeration,
    ! with each field being -2 times the previous one.

    ! Applying a constant background photon field in the z direction.
    k_B(1) = 0
    k_B(2) = 1
    do jB=3,nB
       k_B(jB) = -2*k_B(jB-1)
    end do

    if ( iB > 1) call MultiplyAltBF(k_B(iB),U_xd,Usm_xd)

  end subroutine ApplyAltBF

end module BackgroundField

