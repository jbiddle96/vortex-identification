!! Filename        : $Source$
!! Author          : Waseem Kamleh
!! Created On      : Tue Oct 5 17:54:47 EST 2004
!! Last Modified By: $Author$
!! Last Modified On: $Date$
!! Branch Tag      : $Name$
!! Revision        : $Revision$
!! Update History  : $Log$

!! Filename        : $Source: /users/sapac/wkamleh/cocacola/guavacoke/include/RCS/timer.f90,v $
!! Author          : Waseem Kamleh
!! Created On      : Sun Jun 20 17:09:21 CST 2004
!! Last Modified By: $Author: wkamleh $
!! Last Modified On: $Date: 2004/07/21 07:15:33 $
!! Branch Tag      : $Name:  $
!! Revision        : $Revision: 0.1 $
!! Update History  : $Log: timer.f90,v $
!! Update History  : Revision 0.1  2004/07/21 07:15:33  wkamleh
!! Update History  : Orion version
!! Update History  :
!! Update History  : Revision 1.1.1.2  2004/06/20 07:42:01  wkamleh
!! Update History  : Hydra version
!! Update History  :

#include "defines.h"

module Timer

  !store the clock max for timing purposes
  use Kinds
  use MPIInterface
  implicit none
  private

  !* To Do: Revise this module.

  logical, parameter, public :: timing = .true.

  real(dp), public :: clockres
  real, public :: Mflops

  public :: InitTimer
  public :: TimingUpdate
  public :: UpdateStatistics
  public :: UpdateStatistic

contains

  subroutine InitTimer

    clockres = mpi_wtick()


  end subroutine InitTimer

  subroutine TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    ! begin args: ncalls, outtime, intime, mintime, maxtime, meantime

    integer :: ncalls
    real(DP) :: intime,outtime,maxtime, mintime,meantime

    ! begin execution

    ncalls = ncalls + 1
    if ( ncalls == 1 ) then
       mintime = outtime - intime
       maxtime = outtime - intime
       meantime = outtime - intime
    else
       if ( outtime - intime < mintime ) mintime = outtime - intime
       if ( outtime - intime > maxtime ) maxtime = outtime - intime
       meantime = (ncalls - 1)*meantime/ncalls + (outtime - intime)/ncalls
    end if


  end subroutine TimingUpdate

  subroutine UpdateStatistics(n,x,minx,meanx,maxx)
    ! begin args: n, x, minx, meanx, maxx

    integer :: n
    real(DP), dimension(3) :: x,minx,meanx,maxx

    ! begin execution

    n = n + 1
    if ( n == 1 ) then
       minx = x
       meanx = x
       maxx = x
    else
       where ( x < minx ) minx = x
       where ( x > maxx ) maxx = x
       meanx = (n - 1)*meanx/n + x/n
    end if


  end subroutine UpdateStatistics

  subroutine UpdateStatistic(n,x,xbar)
    ! begin args: n, x, xbar

    integer :: n
    real(DP), dimension(3) :: x,xbar

    ! begin execution

    n = n + 1
    if ( n == 1 ) then
       xbar = x
    else
       if ( x(1) < xbar(1) ) xbar(1) = x(1)
       xbar(2) = (n - 1)*xbar(2)/n + x(2)/n
       if ( x(3) < xbar(3) ) xbar(3) = x(3)
    end if


  end subroutine UpdateStatistic

end module Timer

