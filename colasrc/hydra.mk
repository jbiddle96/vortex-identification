F90C = mpif90
OPTS = -mp -O3 -tpp7 -xW -ipo -Vaxlib
F90FLAGS = -fpp -D_hydra_ $(OPTS) 

.SUFFIXES:
.SUFFIXES: .f90 .o .f
.f90.o:
	$(F90C) $(F90FLAGS) -o $@ -I$(COMDIR) -I/usr/mpich-gm-intel/include -module $(@D)/ -c $<
