module nrutil

  use Kinds
  implicit none
  private

  interface assert_eq
     module procedure assert_eq2,assert_eq3,assert_eq4,assert_eqn
  end interface
  interface diagmult
     module procedure diagmult_rv,diagmult_r
  end interface
  interface poly
     module procedure poly_rr,poly_rrv,poly_dd,poly_ddv,&
          poly_rc,poly_cc,poly_zz,poly_msk_rrv,poly_msk_ddv
  end interface
  interface poly_term
     module procedure poly_term_rr,poly_term_dd,poly_term_cc,poly_term_zz
  end interface

  interface indexx
     module procedure indexx_sp, indexx_dp, indexx_i4b
  end interface
  interface arth
     module procedure arth_r, arth_d, arth_i
  end interface
  interface swap
     module procedure swap_i,swap_r,swap_rv,swap_c, &
          swap_cv,swap_cm,swap_z,swap_zv,swap_zm, &
          masked_swap_rs,masked_swap_rv,masked_swap_rm
  end interface

  interface outerprod
     module procedure outerprod_r,outerprod_d, outerprod_dc
  end interface

  interface imaxloc
     module procedure imaxloc_d,imaxloc_i
  end interface


  integer, parameter, public :: NPAR_ARTH=16,NPAR2_ARTH=8 !Each NPAR2 must be <= the corresponding NPAR.
  integer, parameter, public :: NPAR_POLY=8
  integer, parameter, public :: NPAR_POLYTERM=8

  public :: assert_eq
  public :: diagmult
  public :: poly
  public :: poly_term
  public :: indexx
  public :: arth
  public :: swap
  public :: outerprod
  public :: imaxloc
  public :: arth_d
  public :: arth_i
  public :: swap_r
  public :: swap_rv
  public :: swap_c
  public :: swap_cv
  public :: swap_cm
  public :: swap_z
  public :: swap_zv
  public :: swap_zm
  public :: masked_swap_rs
  public :: masked_swap_rv
  public :: masked_swap_rm
  public :: indexx_dp
  public :: indexx_i4b
  public :: poly_dd
  public :: poly_rc
  public :: poly_cc
  public :: poly_zz
  public :: poly_rrv
  public :: poly_ddv
  public :: poly_msk_rrv
  public :: poly_msk_ddv
  public :: poly_term_cc
  public :: poly_term_dd
  public :: poly_term_zz
  public :: outerprod_d
  public :: outerprod_dc
  public :: outerand
  public :: diagmult_r
  public :: assert_eq3
  public :: assert_eq4
  public :: assert_eqn
  public :: imaxloc_i
  public :: nrerror

contains

  !Routines relating to polynomials and recurrences:
  function arth_r(first,increment,n)
    ! begin args: first, increment, n
    !Array function returning an arithmetic progression.
    real(SP), intent(IN) :: first,increment
    integer, intent(IN) :: n
    ! begin local_vars
    real(SP), dimension(n) :: arth_r
    integer :: k,k2
    real(SP) :: temp
    ! begin execution

    if (n > 0) arth_r(1)=first
    if (n <= NPAR_ARTH) then
       do k=2,n
          arth_r(k)=arth_r(k-1)+increment
       end do
    else
       do k=2,NPAR2_ARTH
          arth_r(k)=arth_r(k-1)+increment
       end do
       temp=increment*NPAR2_ARTH
       k=NPAR2_ARTH
       do
          if (k >= n) exit
          k2=k+k
          arth_r(k+1:min(k2,n))=temp+arth_r(1:min(k,n-k))
          temp=temp+temp
          k=k2
       end do
    end if

  end function arth_r

  function arth_d(first,increment,n)
    ! begin args: first, increment, n
    real(DP), intent(IN) :: first,increment
    integer, intent(IN) :: n
    ! begin local_vars
    real(DP), dimension(n) :: arth_d
    integer :: k,k2
    real(DP) :: temp
    ! begin execution

    if (n > 0) arth_d(1)=first
    if (n <= NPAR_ARTH) then
       do k=2,n
          arth_d(k)=arth_d(k-1)+increment
       end do
    else
       do k=2,NPAR2_ARTH
          arth_d(k)=arth_d(k-1)+increment
       end do
       temp=increment*NPAR2_ARTH
       k=NPAR2_ARTH
       do
          if (k >= n) exit
          k2=k+k
          arth_d(k+1:min(k2,n))=temp+arth_d(1:min(k,n-k))
          temp=temp+temp
          k=k2
       end do
    end if

  end function arth_d

  function arth_i(first,increment,n)
    ! begin args: first, increment, n
    integer, intent(IN) :: first,increment,n
    ! begin local_vars
    integer, dimension(n) :: arth_i
    integer :: k,k2,temp
    ! begin execution

    if (n > 0) arth_i(1)=first
    if (n <= NPAR_ARTH) then
       do k=2,n
          arth_i(k)=arth_i(k-1)+increment
       end do
    else
       do k=2,NPAR2_ARTH
          arth_i(k)=arth_i(k-1)+increment
       end do
       temp=increment*NPAR2_ARTH
       k=NPAR2_ARTH
       do
          if (k >= n) exit
          k2=k+k
          arth_i(k+1:min(k2,n))=temp+arth_i(1:min(k,n-k))
          temp=temp+temp
          k=k2
       end do
    end if

  end function arth_i

  subroutine swap_i(a,b)
    ! begin args: a, b
    !Swap the contents of a and b.
    integer, intent(INOUT) :: a,b
    ! begin local_vars
    integer :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_i

  subroutine swap_r(a,b)
    ! begin args: a, b
    real(SP), intent(INOUT) :: a,b
    ! begin local_vars
    real(SP) :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_r

  subroutine swap_rv(a,b)
    ! begin args: a, b
    real(SP), dimension(:), intent(INOUT) :: a,b
    ! begin local_vars
    real(SP), dimension(size(a)) :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_rv

  subroutine swap_c(a,b)
    ! begin args: a, b
    complex(SC), intent(INOUT) :: a,b
    ! begin local_vars
    complex(SC) :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_c

  subroutine swap_cv(a,b)
    ! begin args: a, b
    complex(SC), dimension(:), intent(INOUT) :: a,b
    ! begin local_vars
    complex(SC), dimension(size(a)) :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_cv

  subroutine swap_cm(a,b)
    ! begin args: a, b
    complex(SC), dimension(:,:), intent(INOUT) :: a,b
    ! begin local_vars
    complex(SC), dimension(size(a,1),size(a,2)) :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_cm

  subroutine swap_z(a,b)
    ! begin args: a, b
    complex(DC), intent(INOUT) :: a,b
    ! begin local_vars
    complex(DC) :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_z

  subroutine swap_zv(a,b)
    ! begin args: a, b
    complex(DC), dimension(:), intent(INOUT) :: a,b
    ! begin local_vars
    complex(DC), dimension(size(a)) :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_zv

  subroutine swap_zm(a,b)
    ! begin args: a, b
    complex(DC), dimension(:,:), intent(INOUT) :: a,b
    ! begin local_vars
    complex(DC), dimension(size(a,1),size(a,2)) :: dum
    ! begin execution

    dum=a
    a=b
    b=dum

  end subroutine swap_zm

  subroutine masked_swap_rs(a,b,mask)
    ! begin args: a, b, mask
    real(SP), intent(INOUT) :: a,b
    logical, intent(IN) :: mask
    ! begin local_vars
    real(SP) :: swp

    ! begin execution

    if (mask) then
       swp=a
       a=b
       b=swp
    end if

  end subroutine masked_swap_rs

  subroutine masked_swap_rv(a,b,mask)
    ! begin args: a, b, mask
    real(SP), dimension(:), intent(INOUT) :: a,b
    logical, dimension(:), intent(IN) :: mask
    ! begin local_vars
    real(SP), dimension(size(a)) :: swp
    ! begin execution

    where (mask)
       swp=a
       a=b
       b=swp
    end where

  end subroutine masked_swap_rv

  subroutine masked_swap_rm(a,b,mask)
    ! begin args: a, b, mask
    real(SP), dimension(:,:), intent(INOUT) :: a,b
    logical, dimension(:,:), intent(IN) :: mask
    ! begin local_vars
    real(SP), dimension(size(a,1),size(a,2)) :: swp
    ! begin execution

    where (mask)
       swp=a
       a=b
       b=swp
    end where

  end subroutine masked_swap_rm

  subroutine indexx_sp(arr,index)
    ! begin args: arr, index
    real(SP), dimension(:), intent(IN) :: arr
    integer, dimension(:), intent(OUT) :: index
    ! begin local_vars
    integer, parameter :: NN=15, NSTACK=50
    !Indexes an array arr, i.e., outputs the array index of length N such that arr(index(j))
    !is in ascending order for j = 1, 2, . . . ,N. The input quantity arr is not changed.
    real(SP) :: a
    integer :: n,k,i,j,indext,jstack,l,r
    integer, dimension(NSTACK) :: istack
    ! begin execution

    n=assert_eq(size(index),size(arr),'indexx_sp')
    index=arth(1,1,n)
    jstack=0
    l=1
    r=n
    do
       if (r-l < NN) then
          do j=l+1,r
             indext=index(j)
             a=arr(indext)
             do i=j-1,l,-1
                if (arr(index(i)) <= a) exit
                index(i+1)=index(i)
             end do
             index(i+1)=indext
          end do
          if (jstack == 0) return
          r=istack(jstack)
          l=istack(jstack-1)
          jstack=jstack-2
       else
          k=(l+r)/2
          call swap(index(k),index(l+1))
          call icomp_xchg(index(l),index(r))
          call icomp_xchg(index(l+1),index(r))
          call icomp_xchg(index(l),index(l+1))
          i=l+1
          j=r
          indext=index(l+1)
          a=arr(indext)
          do
             do
                i=i+1
                if (arr(index(i)) >= a) exit
             end do
             do
                j=j-1
                if (arr(index(j)) <= a) exit
             end do
             if (j < i) exit
             call swap(index(i),index(j))
          end do
          index(l+1)=index(j)
          index(j)=indext
          jstack=jstack+2
          if (jstack > NSTACK) call nrerror('indexx: NSTACK too small')
          if (r-i+1 >= j-l) then
             istack(jstack)=r

             istack(jstack-1)=i
             r=j-1
          else
             istack(jstack)=j-1
             istack(jstack-1)=l
             l=i
          end if
       end if
    end do
  contains
    subroutine icomp_xchg(i,j)
      ! begin args: i, j
      integer, intent(INOUT) :: i,j
      ! begin local_vars
      integer :: swp
      ! begin execution

      if (arr(j) < arr(i)) then
         swp=i
         i=j
         j=swp
      end if

    end subroutine icomp_xchg

  end subroutine indexx_sp

  subroutine indexx_dp(arr,index)
    ! begin args: arr, index
    real(DP), dimension(:), intent(IN) :: arr
    integer, dimension(:), intent(OUT) :: index
    ! begin local_vars
    integer, parameter :: NN=15, NSTACK=50
    !Indexes an array arr, i.e., outputs the array index of length N such that arr(index(j))
    !is in ascending order for j = 1, 2, . . . ,N. The input quantity arr is not changed.
    real(DP) :: a
    integer :: n,k,i,j,indext,jstack,l,r
    integer, dimension(NSTACK) :: istack
    ! begin execution

    n=assert_eq(size(index),size(arr),'indexx_sp')
    index=arth(1,1,n)
    jstack=0
    l=1
    r=n
    do
       if (r-l < NN) then
          do j=l+1,r
             indext=index(j)
             a=arr(indext)
             do i=j-1,l,-1
                if (arr(index(i)) <= a) exit
                index(i+1)=index(i)
             end do
             index(i+1)=indext
          end do
          if (jstack == 0) return
          r=istack(jstack)
          l=istack(jstack-1)
          jstack=jstack-2
       else
          k=(l+r)/2
          call swap(index(k),index(l+1))
          call icomp_xchg(index(l),index(r))
          call icomp_xchg(index(l+1),index(r))
          call icomp_xchg(index(l),index(l+1))
          i=l+1
          j=r
          indext=index(l+1)
          a=arr(indext)
          do
             do
                i=i+1
                if (arr(index(i)) >= a) exit
             end do
             do
                j=j-1
                if (arr(index(j)) <= a) exit
             end do
             if (j < i) exit
             call swap(index(i),index(j))
          end do
          index(l+1)=index(j)
          index(j)=indext
          jstack=jstack+2
          if (jstack > NSTACK) call nrerror('indexx: NSTACK too small')
          if (r-i+1 >= j-l) then
             istack(jstack)=r

             istack(jstack-1)=i
             r=j-1
          else
             istack(jstack)=j-1
             istack(jstack-1)=l
             l=i
          end if
       end if
    end do
  contains
    subroutine icomp_xchg(i,j)
      ! begin args: i, j
      integer, intent(INOUT) :: i,j
      ! begin local_vars
      integer :: swp
      ! begin execution

      if (arr(j) < arr(i)) then
         swp=i
         i=j
         j=swp
      end if

    end subroutine icomp_xchg

  end subroutine indexx_dp

  subroutine indexx_i4b(iarr,index)
    ! begin args: iarr, index
    integer, dimension(:), intent(IN) :: iarr
    integer, dimension(:), intent(OUT) :: index
    ! begin local_vars
    integer, parameter :: NN=15, NSTACK=50
    integer :: a
    integer :: n,k,i,j,indext,jstack,l,r
    integer, dimension(NSTACK) :: istack
    ! begin execution

    n=assert_eq(size(index),size(iarr),'indexx_sp')
    index=arth(1,1,n)
    jstack=0
    l=1
    r=n
    do
       if (r-l < NN) then
          do j=l+1,r
             indext=index(j)
             a=iarr(indext)
             do i=j-1,l,-1
                if (iarr(index(i)) <= a) exit
                index(i+1)=index(i)
             end do
             index(i+1)=indext
          end do
          if (jstack == 0) return
          r=istack(jstack)
          l=istack(jstack-1)
          jstack=jstack-2
       else
          k=(l+r)/2
          call swap(index(k),index(l+1))
          call icomp_xchg(index(l),index(r))
          call icomp_xchg(index(l+1),index(r))
          call icomp_xchg(index(l),index(l+1))
          i=l+1
          j=r
          indext=index(l+1)
          a=iarr(indext)
          do
             do
                i=i+1
                if (iarr(index(i)) >= a) exit
             end do
             do
                j=j-1
                if (iarr(index(j)) <= a) exit
             end do
             if (j < i) exit
             call swap(index(i),index(j))
          end do
          index(l+1)=index(j)
          index(j)=indext
          jstack=jstack+2
          if (jstack > NSTACK) call nrerror('indexx: NSTACK too small')
          if (r-i+1 >= j-l) then
             istack(jstack)=r
             istack(jstack-1)=i
             r=j-1
          else
             istack(jstack)=j-1
             istack(jstack-1)=l
             l=i
          end if
       end if
    end do
  contains
    subroutine icomp_xchg(i,j)
      ! begin args: i, j
      integer, intent(INOUT) :: i,j
      ! begin local_vars
      integer :: swp
      ! begin execution

      if (iarr(j) < iarr(i)) then
         swp=i
         i=j
         j=swp
      end if

    end subroutine icomp_xchg

  end subroutine indexx_i4b

  function poly_rr(x,coeffs)
    ! begin args: x, coeffs
    !Polynomial evaluation.
    real(SP), intent(IN) :: x
    real(SP), dimension(:), intent(IN) :: coeffs
    ! begin local_vars
    real(SP) :: poly_rr
    real(SP) :: pow
    real(SP), dimension(:), allocatable :: vec
    integer :: i,n,nn
    ! begin execution

    n=size(coeffs)
    if (n <= 0) then
       poly_rr=0.0_sp
    else if (n < NPAR_POLY) then
       poly_rr=coeffs(n)
       do i=n-1,1,-1
          poly_rr=x*poly_rr+coeffs(i)
       end do
    else
       allocate(vec(n+1))
       pow=x
       vec(1:n)=coeffs
       do
          vec(n+1)=0.0_sp
          nn=ishft(n+1,-1)
          vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
          if (nn == 1) exit
          pow=pow*pow
          n=nn
       end do
       poly_rr=vec(1)
       deallocate(vec)
    end if

  end function poly_rr

  function poly_dd(x,coeffs)
    ! begin args: x, coeffs
    real(DP), intent(IN) :: x
    real(DP), dimension(:), intent(IN) :: coeffs
    ! begin local_vars
    real(DP) :: poly_dd
    real(DP) :: pow
    real(DP), dimension(:), allocatable :: vec
    integer :: i,n,nn
    ! begin execution

    n=size(coeffs)
    if (n <= 0) then
       poly_dd=0.0_dp
    else if (n < NPAR_POLY) then
       poly_dd=coeffs(n)
       do i=n-1,1,-1
          poly_dd=x*poly_dd+coeffs(i)
       end do
    else
       allocate(vec(n+1))
       pow=x
       vec(1:n)=coeffs
       do
          vec(n+1)=0.0_dp
          nn=ishft(n+1,-1)
          vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
          if (nn == 1) exit
          pow=pow*pow
          n=nn
       end do
       poly_dd=vec(1)
       deallocate(vec)
    end if

  end function poly_dd

  function poly_rc(x,coeffs)
    ! begin args: x, coeffs
    complex(SC), intent(IN) :: x
    real(SP), dimension(:), intent(IN) :: coeffs
    ! begin local_vars
    complex(SC) :: poly_rc
    complex(SC) :: pow
    complex(SC), dimension(:), allocatable :: vec
    integer :: i,n,nn
    ! begin execution

    n=size(coeffs)
    if (n <= 0) then
       poly_rc=0.0_sp
    else if (n < NPAR_POLY) then
       poly_rc=coeffs(n)
       do i=n-1,1,-1
          poly_rc=x*poly_rc+coeffs(i)
       end do
    else
       allocate(vec(n+1))
       pow=x
       vec(1:n)=coeffs
       do
          vec(n+1)=0.0_sp
          nn=ishft(n+1,-1)
          vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
          if (nn == 1) exit
          pow=pow*pow
          n=nn
       end do
       poly_rc=vec(1)
       deallocate(vec)
    end if

  end function poly_rc

  function poly_cc(x,coeffs)
    ! begin args: x, coeffs
    complex(SC), intent(IN) :: x
    complex(SC), dimension(:), intent(IN) :: coeffs
    ! begin local_vars
    complex(SC) :: poly_cc
    complex(SC) :: pow
    complex(SC), dimension(:), allocatable :: vec
    integer :: i,n,nn
    ! begin execution

    n=size(coeffs)
    if (n <= 0) then
       poly_cc=0.0_sp
    else if (n < NPAR_POLY) then
       poly_cc=coeffs(n)
       do i=n-1,1,-1
          poly_cc=x*poly_cc+coeffs(i)
       end do
    else
       allocate(vec(n+1))
       pow=x
       vec(1:n)=coeffs
       do
          vec(n+1)=0.0_sp
          nn=ishft(n+1,-1)
          vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
          if (nn == 1) exit
          pow=pow*pow
          n=nn
       end do
       poly_cc=vec(1)
       deallocate(vec)
    end if

  end function poly_cc

  function poly_zz(x,coeffs)
    ! begin args: x, coeffs
    complex(DC), intent(IN) :: x
    complex(DC), dimension(:), intent(IN) :: coeffs
    ! begin local_vars
    complex(DC) :: poly_zz
    complex(DC) :: pow
    complex(DC), dimension(:), allocatable :: vec
    integer :: i,n,nn
    ! begin execution

    n=size(coeffs)
    if (n <= 0) then
       poly_zz=0.0_dp
    else if (n < NPAR_POLY) then
       poly_zz=coeffs(n)
       do i=n-1,1,-1
          poly_zz=x*poly_zz+coeffs(i)
       end do
    else
       allocate(vec(n+1))
       pow=x
       vec(1:n)=coeffs
       do
          vec(n+1)=0.0_dp
          nn=ishft(n+1,-1)
          vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
          if (nn == 1) exit
          pow=pow*pow
          n=nn
       end do
       poly_zz=vec(1)
       deallocate(vec)
    end if

  end function poly_zz

  function poly_rrv(x,coeffs)
    ! begin args: x, coeffs
    real(SP), dimension(:), intent(IN) :: coeffs,x
    ! begin local_vars
    real(SP), dimension(size(x)) :: poly_rrv
    integer :: i,n,m
    ! begin execution

    m=size(coeffs)
    n=size(x)
    if (m <= 0) then
       poly_rrv=0.0_sp
    else if (m < n .or. m < NPAR_POLY) then
       poly_rrv=coeffs(m)
       do i=m-1,1,-1
          poly_rrv=x*poly_rrv+coeffs(i)
       end do
    else
       do i=1,n
          poly_rrv(i)=poly_rr(x(i),coeffs)
       end do
    end if

  end function poly_rrv

  function poly_ddv(x,coeffs)
    ! begin args: x, coeffs
    real(DP), dimension(:), intent(IN) :: coeffs,x
    ! begin local_vars
    real(DP), dimension(size(x)) :: poly_ddv
    integer :: i,n,m
    ! begin execution

    m=size(coeffs)
    n=size(x)
    if (m <= 0) then
       poly_ddv=0.0_dp
    else if (m < n .or. m < NPAR_POLY) then
       poly_ddv=coeffs(m)
       do i=m-1,1,-1
          poly_ddv=x*poly_ddv+coeffs(i)
       end do
    else
       do i=1,n
          poly_ddv(i)=poly_dd(x(i),coeffs)
       end do
    end if

  end function poly_ddv

  function poly_msk_rrv(x,coeffs,mask)
    ! begin args: x, coeffs, mask
    real(SP), dimension(:), intent(IN) :: coeffs,x
    logical, dimension(:), intent(IN) :: mask
    ! begin local_vars
    real(SP), dimension(size(x)) :: poly_msk_rrv
    ! begin execution

    poly_msk_rrv=unpack(poly_rrv(pack(x,mask),coeffs),mask,0.0_sp)

  end function poly_msk_rrv

  function poly_msk_ddv(x,coeffs,mask)
    ! begin args: x, coeffs, mask
    real(DP), dimension(:), intent(IN) :: coeffs,x
    logical, dimension(:), intent(IN) :: mask
    ! begin local_vars
    real(DP), dimension(size(x)) :: poly_msk_ddv
    ! begin execution

    poly_msk_ddv=unpack(poly_ddv(pack(x,mask),coeffs),mask,0.0_dp)

  end function poly_msk_ddv

  recursive function poly_term_rr(a,b) result(u)
    ! begin args: a, b
    !Tabulate cumulants of a polynomial.
    real(SP), dimension(:), intent(IN) :: a
    real(SP), intent(IN) :: b
    ! begin local_vars
    real(SP), dimension(size(a)) :: u
    integer :: n,j
    ! begin execution

    n=size(a)
    if (n <= 0) return
    u(1)=a(1)
    if (n < NPAR_POLYTERM) then
       do j=2,n
          u(j)=a(j)+b*u(j-1)
       end do
    else
       u(2:n:2)=poly_term_rr(a(2:n:2)+a(1:n-1:2)*b,b*b)
       u(3:n:2)=a(3:n:2)+b*u(2:n-1:2)
    end if

  end function poly_term_rr

  recursive function poly_term_cc(a,b) result(u)
    ! begin args: a, b
    complex(SC), dimension(:), intent(IN) :: a
    complex(SC), intent(IN) :: b
    ! begin local_vars
    complex(SC), dimension(size(a)) :: u
    integer :: n,j
    ! begin execution

    n=size(a)
    if (n <= 0) return
    u(1)=a(1)
    if (n < NPAR_POLYTERM) then
       do j=2,n
          u(j)=a(j)+b*u(j-1)
       end do
    else
       u(2:n:2)=poly_term_cc(a(2:n:2)+a(1:n-1:2)*b,b*b)
       u(3:n:2)=a(3:n:2)+b*u(2:n-1:2)
    end if

  end function poly_term_cc

  recursive function poly_term_dd(a,b) result(u)
    ! begin args: a, b
    !Tabulate cumulants of a polynomial.
    real(DP), dimension(:), intent(IN) :: a
    real(DP), intent(IN) :: b
    ! begin local_vars
    real(DP), dimension(size(a)) :: u
    integer :: n,j
    ! begin execution

    n=size(a)
    if (n <= 0) return
    u(1)=a(1)
    if (n < NPAR_POLYTERM) then
       do j=2,n
          u(j)=a(j)+b*u(j-1)
       end do
    else
       u(2:n:2)=poly_term_dd(a(2:n:2)+a(1:n-1:2)*b,b*b)
       u(3:n:2)=a(3:n:2)+b*u(2:n-1:2)
    end if

  end function poly_term_dd

  recursive function poly_term_zz(a,b) result(u)
    ! begin args: a, b
    complex(DC), dimension(:), intent(IN) :: a
    complex(DC), intent(IN) :: b
    ! begin local_vars
    complex(DC), dimension(size(a)) :: u
    integer :: n,j
    ! begin execution

    n=size(a)
    if (n <= 0) return
    u(1)=a(1)
    if (n < NPAR_POLYTERM) then
       do j=2,n
          u(j)=a(j)+b*u(j-1)
       end do
    else
       u(2:n:2)=poly_term_zz(a(2:n:2)+a(1:n-1:2)*b,b*b)
       u(3:n:2)=a(3:n:2)+b*u(2:n-1:2)
    end if

  end function poly_term_zz


  function outerprod_r(a,b)
    ! begin args: a, b
    real(SP), dimension(:), intent(IN) :: a,b
    ! begin local_vars
    real(SP), dimension(size(a),size(b)) :: outerprod_r
    ! begin execution

    outerprod_r = spread(a,dim=2,ncopies=size(b)) * &
         spread(b,dim=1,ncopies=size(a))

  end function outerprod_r

  function outerprod_d(a,b)
    ! begin args: a, b
    real(DP), dimension(:), intent(IN) :: a,b
    ! begin local_vars
    real(DP), dimension(size(a),size(b)) :: outerprod_d
    ! begin execution

    outerprod_d = spread(a,dim=2,ncopies=size(b)) * &
         spread(b,dim=1,ncopies=size(a))

  end function outerprod_d

  function outerprod_dc(a,b)
    ! begin args: a, b
    complex(dc), dimension(:), intent(IN) :: a,b
    ! begin local_vars
    complex(dc), dimension(size(a),size(b)) :: outerprod_dc
    ! begin execution

    outerprod_dc = spread(a,dim=2,ncopies=size(b)) * &
         spread(b,dim=1,ncopies=size(a))

  end function outerprod_dc

  function outerand(a,b)
    ! begin args: a, b
    logical, dimension(:), intent(IN) :: a,b
    ! begin local_vars
    logical, dimension(size(a),size(b)) :: outerand
    ! begin execution

    outerand = spread(a,dim=2,ncopies=size(b)) .and. &
         spread(b,dim=1,ncopies=size(a))

  end function outerand

  subroutine diagmult_rv(mat,diag)
    ! begin args: mat, diag
    !Multiplies vector or scalar diag into the diagonal of matrix mat.
    real(SP), dimension(:,:), intent(INOUT) :: mat
    real(SP), dimension(:), intent(IN) :: diag
    ! begin local_vars
    integer :: j,n
    ! begin execution

    n = assert_eq2(size(diag),min(size(mat,1),size(mat,2)),'diagmult_rv')
    do j=1,n
       mat(j,j)=mat(j,j)*diag(j)
    end do

  end subroutine diagmult_rv

  subroutine diagmult_r(mat,diag)
    ! begin args: mat, diag
    real(SP), dimension(:,:), intent(INOUT) :: mat
    real(SP), intent(IN) :: diag
    ! begin local_vars
    integer :: j,n
    ! begin execution

    n = min(size(mat,1),size(mat,2))
    do j=1,n
       mat(j,j)=mat(j,j)*diag
    end do

  end subroutine diagmult_r

  function assert_eq2(n1,n2,string)
    ! begin args: n1, n2, string
    !Report and die if integers not all equal (used for size checking).
    character(LEN=*), intent(IN) :: string
    integer, intent(IN) :: n1,n2
    ! begin local_vars
    integer :: assert_eq2
    ! begin execution

    if (n1 == n2) then
       assert_eq2=n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', &
            string
       stop 'program terminated by assert_eq2'
    end if


  end function assert_eq2

  function assert_eq3(n1,n2,n3,string)
    ! begin args: n1, n2, n3, string
    character(LEN=*), intent(IN) :: string
    integer, intent(IN) :: n1,n2,n3
    ! begin local_vars
    integer :: assert_eq3
    ! begin execution

    if (n1 == n2 .and. n2 == n3) then
       assert_eq3=n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', &
            string
       stop 'program terminated by assert_eq3'
    end if

  end function assert_eq3

  function assert_eq4(n1,n2,n3,n4,string)
    ! begin args: n1, n2, n3, n4, string
    character(LEN=*), intent(IN) :: string
    integer, intent(IN) :: n1,n2,n3,n4
    ! begin local_vars
    integer :: assert_eq4
    ! begin execution

    if (n1 == n2 .and. n2 == n3 .and. n3 == n4) then
       assert_eq4=n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', &
            string
       stop 'program terminated by assert_eq4'
    end if

  end function assert_eq4

  function assert_eqn(nn,string)
    ! begin args: nn, string
    character(LEN=*), intent(IN) :: string
    integer, dimension(:), intent(IN) :: nn
    ! begin local_vars
    integer :: assert_eqn
    ! begin execution

    if (all(nn(2:) == nn(1))) then
       assert_eqn=nn(1)
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', &
            string
       stop 'program terminated by assert_eqn'
    end if

  end function assert_eqn

  function imaxloc_d(arr)
    ! begin args: arr
    !Index of maxloc on an array.
    real(dp), dimension(:), intent(IN) :: arr
    ! begin local_vars
    integer :: imaxloc_d
    integer, dimension(1) :: imax
    ! begin execution

    imax=maxloc(arr(:))
    imaxloc_d=imax(1)

  end function imaxloc_d

  function imaxloc_i(iarr)
    ! begin args: iarr
    integer, dimension(:), intent(IN) :: iarr
    ! begin local_vars
    integer, dimension(1) :: imax
    integer :: imaxloc_i
    ! begin execution

    imax=maxloc(iarr(:))
    imaxloc_i=imax(1)

  end function imaxloc_i

  subroutine nrerror(string)
    ! begin args: string
    !Report a message, then die.
    character(LEN=*), intent(IN) :: string
    ! begin execution

    write (*,*) 'nrerror: ',string
    stop 'program terminated by nrerror'

  end subroutine nrerror


end module nrutil

