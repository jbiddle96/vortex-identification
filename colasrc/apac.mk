F90C = ifort
OPTS = -O3 -assume byterecl #-tpp2 -align all
F90FLAGS = -cpp $(OPTS) -convert big_endian
LINK = -lmpi -i_dynamic

.SUFFIXES:
.SUFFIXES: .f90 .o .f
.f90.o:
	$(F90C) $(F90FLAGS) -o $@ -I$(COMDIR) -module $(@D)/ -c $<

