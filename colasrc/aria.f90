# 1 "../include/aria.f90"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "../include/aria.f90"



module ARIA

  use Timer
  use MPIInterface
  use FermionAlgebra
  use FermionField
  use SpinorTypes
  use GaugeFieldMPIComms
  use GaugeField
  use MatrixAlgebra
  use LatticeSize
  use Kinds
  use ColourTypes
  implicit none
  private

  !shifted gauge fields
  type(colour_matrix), dimension(:,:,:,:,:), allocatable, public :: U1_xd, U2_xd

  integer, public :: ncalls = 0, ncomm = 0, nwaste = 0
  real(dp), public :: maxtime, mintime, meantime
  real(dp), public :: commmaxtime, commmintime, commmeantime
  real(dp), public :: wastemax, wastemin, wastemean

  real(dp), public :: xi_q
  real(dp), private :: m_r, mu_r

  real(dp), public :: s_scale = 4.641d0, r_scale = 0.115d0
  integer, public :: n_chebyshev = 30

  public :: InitialiseAriaOperator
  public :: D_aria
  public :: Wilson_aria
  public :: H_aria
  public :: Hsq_aria
  public :: Hsq_aria_rescale
  public :: H_chebyshev

contains

  subroutine InitialiseAriaOperator(Ut_xd, Us_xd, u0_t, u0_s, mass, xi_q, bct)
    ! begin args: Ut_xd, Us_xd, u0_t, u0_s, mass, xi_q, bct

    !Must call Initialise Operators each time the fermion mass is changed
    !or the gauge field is changed

    type(colour_matrix), dimension(:,:,:,:,:) :: Ut_xd, Us_xd

    real(DP) :: u0_t, u0_s, mass, xi_q, bct
    ! begin local_vars
    integer :: ix,iy,iz,it,mu, ic,jc
    real(dp) :: mfi_t, mfi_s1, mfi_s2 !coefficents to be absorbed.
    integer, dimension(nd) :: dmu
    integer :: jx,jy,jz,jt

    ! begin execution

    m_f = mass
    mu_r = 1.0d0 !+ 0.5d0*m_f
    m_r = mu_r*m_f + 18.0d0*0.125d0/xi_q + 1.0d0
    !mu_r = 0.75d0

    U1_xd(1:nx,1:ny,1:nz,1:nt,1:3) = Us_xd(1:nx,1:ny,1:nz,1:nt,1:3)
    U1_xd(1:nx,1:ny,1:nz,1:nt,4) = Ut_xd(1:nx,1:ny,1:nz,1:nt,4)
    U2_xd(1:nx,1:ny,1:nz,1:nt,4) = Ut_xd(1:nx,1:ny,1:nz,1:nt,4)





    do mu=1,nd-1
       dmu = 0
       dmu(mu) = 1

       do it=1,nt
          jt = it
          do iz=1,nz
             jz = mapz(iz + dmu(3))
             do iy=1,ny
                jy = mapy(iy + dmu(2))
                do ix=1,nx
                   jx = mapx(ix + dmu(1))

                   call MultiplyMatMat(U2_xd(ix,iy,iz,it,mu),Us_xd(ix,iy,iz,it,mu),Us_xd(jx,jy,jz,jt,mu))

                end do
             end do
          end do
       end do
    end do





    mfi_s1 = 1.0d0/(m_r*xi_q*u0_s)
    mfi_s2 = -1.0d0/(m_r*xi_q*u0_s**2)
    do mu=1,nd-1
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          U1_xd(ix,iy,iz,it,mu)%Cl = mfi_s1*U1_xd(ix,iy,iz,it,mu)%Cl
          U2_xd(ix,iy,iz,it,mu)%Cl = mfi_s2*U2_xd(ix,iy,iz,it,mu)%Cl

       end do; end do; end do; end do
    end do

    mfi_t = 0.5d0/(m_r*u0_t)
    mu = nd
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

       U1_xd(ix,iy,iz,it,mu)%Cl = mfi_t*U1_xd(ix,iy,iz,it,mu)%Cl
       U2_xd(ix,iy,iz,it,mu)%Cl = mfi_t*U2_xd(ix,iy,iz,it,mu)%Cl

    end do; end do; end do; end do

    !Absorb the mean field improvement into the gauge fields.
    if ( bct/=1.0d0 ) then
       call SetBoundaryConditions(U1_xd,1.0d0,1.0d0,1.0d0,bct)
       call SetBoundaryConditions(U2_xd,1.0d0,1.0d0,1.0d0,bct)
    end if

    call ShadowGaugeField(U1_xd,0)
    call ShadowGaugeField(U2_xd,0)


  end subroutine InitialiseAriaOperator

  subroutine D_aria(phi,Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    ! begin local_vars
    real(dp) :: g_s, r_s

    ! begin execution

    g_s = 2.0d0*mu_r/3.0d0
    r_s = 0.5d0 ! use s = 1/8

    call Wilson_aria(phi,Dphi,g_s,r_s,U1_xd,1)

    g_s = mu_r/12.0d0
    r_s = 0.125d0 ! use s = 1/8

    call Wilson_aria(phi,Dphi,g_s,r_s,U2_xd,2)


  end subroutine D_aria

  subroutine Wilson_aria(phi, Dphi, g_s, r_s, Ul_xd, i_l)
    ! begin args: phi, Dphi, g_s, r_s, Ul_xd, i_l

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    real(dp) :: g_s, r_s

    type(colour_matrix), dimension(:,:,:,:,:) :: Ul_xd
    integer :: i_l

    ! begin local_vars
    integer :: ix,iy,iz,it,is
    integer :: jx,jy,jz,jt,js
    type(colour_vector) :: psi_x
    type(colour_vector) :: Gammaphi

    real(dp) :: pm(ns),intime, outtime, t0,t1, commtime, waste
    integer :: mrank,prank,sendrecv_reqz(nt,ns,2),sendrecv_reqt(ns,2),sendrecv_status(nmpi_status,2), mpierror
    integer :: sendrecvz_status(nmpi_status,nt*ns*2), sendrecvt_status(nmpi_status,ns*2)

    ! begin execution

    if (timing) intime = mpi_wtime()
    commtime = 0.0d0




    if ( i_l == 1 ) then
       Dphi = phi
! do is=1,ns
! do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
! Dphi(ix,iy,iz,it,is)%Cl = m_r*phi(ix,iy,iz,it,is)%Cl
! end do; end do; end do; end do
! end do
    end if
# 208 "../include/aria.f90"
    !mu = 1




    !(r_s - g_s*gamma_1) U phi_xpmu

    pm = (/ -1.0d0,-1.0d0,1.0d0,1.0d0 /)
    do is=1,ns
       js = 5 - is
       do it=1,nt; do iz=1,nz; do iy=1,ny; ix=nx; do ix=1,nx
          jx = mapx(ix + i_l)

          psi_x%Cl(1) = Ul_xd(ix,iy,iz,it,1)%Cl(1,1)*phi(jx,iy,iz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(1,2)*phi(jx,iy,iz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(1,3)*phi(jx,iy,iz,it,is)%Cl(3)

          psi_x%Cl(2) = Ul_xd(ix,iy,iz,it,1)%Cl(2,1)*phi(jx,iy,iz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(2,2)*phi(jx,iy,iz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(2,3)*phi(jx,iy,iz,it,is)%Cl(3)

          psi_x%Cl(3) = Ul_xd(ix,iy,iz,it,1)%Cl(3,1)*phi(jx,iy,iz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(3,2)*phi(jx,iy,iz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(3,3)*phi(jx,iy,iz,it,is)%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - r_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - r_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - r_s*psi_x%Cl(3)

          Gammaphi%Cl = pm(is)*cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl),real(phi(jx,iy,iz,it,js)%Cl),dc)

          psi_x%Cl(1) = Ul_xd(ix,iy,iz,it,1)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(1,3)*Gammaphi%Cl(3)

          psi_x%Cl(2) = Ul_xd(ix,iy,iz,it,1)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(2,3)*Gammaphi%Cl(3)

          psi_x%Cl(3) = Ul_xd(ix,iy,iz,it,1)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,1)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + g_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + g_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + g_s*psi_x%Cl(3)

       end do; end do; end do; end do
    end do

    !mu = 2




    !(r_s - g_s*gamma_2) U phi_xpmu

    pm = (/ -1.0d0,1.0d0,1.0d0,-1.0d0 /)
    do is=1,ns
       js = 5 - is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jy = mapy(iy+i_l); do ix=1,nx

          psi_x%Cl(1) = Ul_xd(ix,iy,iz,it,2)%Cl(1,1)*phi(ix,jy,iz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(1,2)*phi(ix,jy,iz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(1,3)*phi(ix,jy,iz,it,is)%Cl(3)

          psi_x%Cl(2) = Ul_xd(ix,iy,iz,it,2)%Cl(2,1)*phi(ix,jy,iz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(2,2)*phi(ix,jy,iz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(2,3)*phi(ix,jy,iz,it,is)%Cl(3)

          psi_x%Cl(3) = Ul_xd(ix,iy,iz,it,2)%Cl(3,1)*phi(ix,jy,iz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(3,2)*phi(ix,jy,iz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(3,3)*phi(ix,jy,iz,it,is)%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - r_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - r_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - r_s*psi_x%Cl(3)

          Gammaphi%Cl = pm(is)*phi(ix,jy,iz,it,js)%Cl

          psi_x%Cl(1) = Ul_xd(ix,iy,iz,it,2)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(1,3)*Gammaphi%Cl(3)

          psi_x%Cl(2) = Ul_xd(ix,iy,iz,it,2)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(2,3)*Gammaphi%Cl(3)

          psi_x%Cl(3) = Ul_xd(ix,iy,iz,it,2)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,2)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + g_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + g_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + g_s*psi_x%Cl(3)

       end do; end do; end do; end do
    end do

    !mu = 3




    !(r_s - g_s*gamma_3) U phi_xpmu

    pm = (/ -1.0d0,1.0d0,1.0d0,-1.0d0 /)
    do is=1,ns
       js = 2 + is - ((is-1)/2)*4
       do it=1,nt; do iz=1,nz; jz=mapz(iz+i_l); do iy=1,ny; do ix=1,nx

          psi_x%Cl(1) = Ul_xd(ix,iy,iz,it,3)%Cl(1,1)*phi(ix,iy,jz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(1,2)*phi(ix,iy,jz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(1,3)*phi(ix,iy,jz,it,is)%Cl(3)

          psi_x%Cl(2) = Ul_xd(ix,iy,iz,it,3)%Cl(2,1)*phi(ix,iy,jz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(2,2)*phi(ix,iy,jz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(2,3)*phi(ix,iy,jz,it,is)%Cl(3)

          psi_x%Cl(3) = Ul_xd(ix,iy,iz,it,3)%Cl(3,1)*phi(ix,iy,jz,it,is)%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(3,2)*phi(ix,iy,jz,it,is)%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(3,3)*phi(ix,iy,jz,it,is)%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - r_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - r_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - r_s*psi_x%Cl(3)

          Gammaphi%Cl = pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl),real(phi(ix,iy,jz,it,js)%Cl),dc)

          psi_x%Cl(1) = Ul_xd(ix,iy,iz,it,3)%Cl(1,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(1,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(1,3)*Gammaphi%Cl(3)

          psi_x%Cl(2) = Ul_xd(ix,iy,iz,it,3)%Cl(2,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(2,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(2,3)*Gammaphi%Cl(3)

          psi_x%Cl(3) = Ul_xd(ix,iy,iz,it,3)%Cl(3,1)*Gammaphi%Cl(1) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(3,2)*Gammaphi%Cl(2) + &
               & Ul_xd(ix,iy,iz,it,3)%Cl(3,3)*Gammaphi%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) + g_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) + g_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) + g_s*psi_x%Cl(3)

       end do; end do; end do; end do
    end do
# 365 "../include/aria.f90"
    !mu = 4




    !G_4^- Up phi_xpmu

    if ( i_l == 1 ) then
       do is=1,nsp
          js = is+2



          do it=1,nt; jt=mapt(it+1); do iz=1,nz; do iy=1,ny; do ix=1,nx


             Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) - phi(ix,iy,iz,jt,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) - phi(ix,iy,iz,jt,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) - phi(ix,iy,iz,jt,js)%Cl(3)

             psi_x%Cl(1) = Ul_xd(ix,iy,iz,it,4)%Cl(1,1)*Gammaphi%Cl(1) + &
                  & Ul_xd(ix,iy,iz,it,4)%Cl(1,2)*Gammaphi%Cl(2) + &
                  & Ul_xd(ix,iy,iz,it,4)%Cl(1,3)*Gammaphi%Cl(3)

             psi_x%Cl(2) = Ul_xd(ix,iy,iz,it,4)%Cl(2,1)*Gammaphi%Cl(1) + &
                  & Ul_xd(ix,iy,iz,it,4)%Cl(2,2)*Gammaphi%Cl(2) + &
                  & Ul_xd(ix,iy,iz,it,4)%Cl(2,3)*Gammaphi%Cl(3)

             psi_x%Cl(3) = Ul_xd(ix,iy,iz,it,4)%Cl(3,1)*Gammaphi%Cl(1) + &
                  & Ul_xd(ix,iy,iz,it,4)%Cl(3,2)*Gammaphi%Cl(2) + &
                  & Ul_xd(ix,iy,iz,it,4)%Cl(3,3)*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) + psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) + psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) + psi_x%Cl(3)

          end do; end do; end do; end do
       end do
    end if
# 424 "../include/aria.f90"
    !mu = 1




    !(r_s + g_s*gamma_1) U^dag phi_xmmu

    pm = (/ -1.0d0,-1.0d0,1.0d0,1.0d0 /)
    do is=1,ns
       js = 5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

          jx = mapx(ix-i_l)

          psi_x%Cl(1) = conjg(Ul_xd(jx,iy,iz,it,1)%Cl(1,1))*phi(jx,iy,iz,it,is)%Cl(1) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(2,1))*phi(jx,iy,iz,it,is)%Cl(2) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(3,1))*phi(jx,iy,iz,it,is)%Cl(3)

          psi_x%Cl(2) = conjg(Ul_xd(jx,iy,iz,it,1)%Cl(1,2))*phi(jx,iy,iz,it,is)%Cl(1) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(2,2))*phi(jx,iy,iz,it,is)%Cl(2) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(3,2))*phi(jx,iy,iz,it,is)%Cl(3)

          psi_x%Cl(3) = conjg(Ul_xd(jx,iy,iz,it,1)%Cl(1,3))*phi(jx,iy,iz,it,is)%Cl(1) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(2,3))*phi(jx,iy,iz,it,is)%Cl(2) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(3,3))*phi(jx,iy,iz,it,is)%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - r_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - r_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - r_s*psi_x%Cl(3)

          Gammaphi%Cl = pm(is)*cmplx(-aimag(phi(jx,iy,iz,it,js)%Cl),real(phi(jx,iy,iz,it,js)%Cl),dc)

          psi_x%Cl(1) = conjg(Ul_xd(jx,iy,iz,it,1)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(3,1))*Gammaphi%Cl(3)

          psi_x%Cl(2) = conjg(Ul_xd(jx,iy,iz,it,1)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(3,2))*Gammaphi%Cl(3)

          psi_x%Cl(3) = conjg(Ul_xd(jx,iy,iz,it,1)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(jx,iy,iz,it,1)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - g_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - g_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - g_s*psi_x%Cl(3)


       end do; end do; end do; end do
    end do

    !mu = 2




    !(r_s + g_s*gamma_2) U^dag phi_xmmu

    pm = (/ -1.0d0,1.0d0,1.0d0,-1.0d0 /)
    do is=1,ns
       js=5-is
       do it=1,nt; do iz=1,nz; do iy=1,ny; jy = mapy(iy-i_l); do ix=1,nx

          psi_x%Cl(1) = conjg(Ul_xd(ix,jy,iz,it,2)%Cl(1,1))*phi(ix,jy,iz,it,is)%Cl(1) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(2,1))*phi(ix,jy,iz,it,is)%Cl(2) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(3,1))*phi(ix,jy,iz,it,is)%Cl(3)

          psi_x%Cl(2) = conjg(Ul_xd(ix,jy,iz,it,2)%Cl(1,2))*phi(ix,jy,iz,it,is)%Cl(1) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(2,2))*phi(ix,jy,iz,it,is)%Cl(2) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(3,2))*phi(ix,jy,iz,it,is)%Cl(3)

          psi_x%Cl(3) = conjg(Ul_xd(ix,jy,iz,it,2)%Cl(1,3))*phi(ix,jy,iz,it,is)%Cl(1) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(2,3))*phi(ix,jy,iz,it,is)%Cl(2) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(3,3))*phi(ix,jy,iz,it,is)%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - r_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - r_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - r_s*psi_x%Cl(3)

          Gammaphi%Cl = pm(is)*phi(ix,jy,iz,it,js)%Cl

          psi_x%Cl(1) = conjg(Ul_xd(ix,jy,iz,it,2)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(3,1))*Gammaphi%Cl(3)

          psi_x%Cl(2) = conjg(Ul_xd(ix,jy,iz,it,2)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(3,2))*Gammaphi%Cl(3)

          psi_x%Cl(3) = conjg(Ul_xd(ix,jy,iz,it,2)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(ix,jy,iz,it,2)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - g_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - g_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - g_s*psi_x%Cl(3)

       end do; end do; end do; end do
    end do

    !mu = 3




    !(r_s + g_s*gamma_3) U^dag phi_xmmu

    pm = (/ -1.0d0,1.0d0,1.0d0,-1.0d0 /)
    do is=1,ns
       js = 2 + is - ((is-1)/2)*4
       do it=1,nt; do iz=1,nz; jz = mapz(iz-i_l); do iy=1,ny; do ix=1,nx

          psi_x%Cl(1) = conjg(Ul_xd(ix,iy,jz,it,3)%Cl(1,1))*phi(ix,iy,jz,it,is)%Cl(1) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(2,1))*phi(ix,iy,jz,it,is)%Cl(2) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(3,1))*phi(ix,iy,jz,it,is)%Cl(3)

          psi_x%Cl(2) = conjg(Ul_xd(ix,iy,jz,it,3)%Cl(1,2))*phi(ix,iy,jz,it,is)%Cl(1) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(2,2))*phi(ix,iy,jz,it,is)%Cl(2) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(3,2))*phi(ix,iy,jz,it,is)%Cl(3)

          psi_x%Cl(3) = conjg(Ul_xd(ix,iy,jz,it,3)%Cl(1,3))*phi(ix,iy,jz,it,is)%Cl(1) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(2,3))*phi(ix,iy,jz,it,is)%Cl(2) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(3,3))*phi(ix,iy,jz,it,is)%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - r_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - r_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - r_s*psi_x%Cl(3)

          Gammaphi%Cl = pm(is)*cmplx(-aimag(phi(ix,iy,jz,it,js)%Cl),real(phi(ix,iy,jz,it,js)%Cl),dc)

          psi_x%Cl(1) = conjg(Ul_xd(ix,iy,jz,it,3)%Cl(1,1))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(2,1))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(3,1))*Gammaphi%Cl(3)

          psi_x%Cl(2) = conjg(Ul_xd(ix,iy,jz,it,3)%Cl(1,2))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(2,2))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(3,2))*Gammaphi%Cl(3)

          psi_x%Cl(3) = conjg(Ul_xd(ix,iy,jz,it,3)%Cl(1,3))*Gammaphi%Cl(1) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(2,3))*Gammaphi%Cl(2) + &
               & conjg(Ul_xd(ix,iy,jz,it,3)%Cl(3,3))*Gammaphi%Cl(3)

          Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - g_s*psi_x%Cl(1)
          Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - g_s*psi_x%Cl(2)
          Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - g_s*psi_x%Cl(3)

       end do; end do; end do; end do
    end do
# 583 "../include/aria.f90"
    !mu = 4




    !G_4^+ Up phi_xmmu

    if ( i_l == 1 ) then
       do is=1,nsp
          js = is+2



          jt=nt

          do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx

             Gammaphi%Cl(1) = phi(ix,iy,iz,jt,is)%Cl(1) + phi(ix,iy,iz,jt,js)%Cl(1)
             Gammaphi%Cl(2) = phi(ix,iy,iz,jt,is)%Cl(2) + phi(ix,iy,iz,jt,js)%Cl(2)
             Gammaphi%Cl(3) = phi(ix,iy,iz,jt,is)%Cl(3) + phi(ix,iy,iz,jt,js)%Cl(3)

             psi_x%Cl(1) = conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(1,1))*Gammaphi%Cl(1) + &
                  & conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(2,1))*Gammaphi%Cl(2) + &
                  & conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(3,1))*Gammaphi%Cl(3)

             psi_x%Cl(2) = conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(1,2))*Gammaphi%Cl(1) + &
                  & conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(2,2))*Gammaphi%Cl(2) + &
                  & conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(3,2))*Gammaphi%Cl(3)

             psi_x%Cl(3) = conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(1,3))*Gammaphi%Cl(1) + &
                  & conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(2,3))*Gammaphi%Cl(2) + &
                  & conjg(Ul_xd(ix,iy,iz,jt,4)%Cl(3,3))*Gammaphi%Cl(3)

             Dphi(ix,iy,iz,it,is)%Cl(1) = Dphi(ix,iy,iz,it,is)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,is)%Cl(2) = Dphi(ix,iy,iz,it,is)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,is)%Cl(3) = Dphi(ix,iy,iz,it,is)%Cl(3) - psi_x%Cl(3)

             Dphi(ix,iy,iz,it,js)%Cl(1) = Dphi(ix,iy,iz,it,js)%Cl(1) - psi_x%Cl(1)
             Dphi(ix,iy,iz,it,js)%Cl(2) = Dphi(ix,iy,iz,it,js)%Cl(2) - psi_x%Cl(2)
             Dphi(ix,iy,iz,it,js)%Cl(3) = Dphi(ix,iy,iz,it,js)%Cl(3) - psi_x%Cl(3)

          end do; end do; end do; jt=it; end do
       end do
    end if





    if (timing) then
       outtime = mpi_wtime()
       call TimingUpdate(ncalls,outtime,intime,mintime,maxtime,meantime)
    end if

    call TimingUpdate(ncomm,commtime,0.0d0,commmintime,commmaxtime,commmeantime)
    waste = commtime/(outtime - intime)
    call TimingUpdate(nwaste,waste,0.0d0,wastemin,wastemax,wastemean)



  end subroutine Wilson_aria

  subroutine H_aria(phi,Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    ! begin local_vars
    integer :: ix,iy,iz,it,is

    ! begin execution

    call D_aria(phi,Dphi)

    !Multiply by gamma_5
    do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
       Dphi(ix,iy,iz,it,3)%Cl = -Dphi(ix,iy,iz,it,3)%Cl
       Dphi(ix,iy,iz,it,4)%Cl = -Dphi(ix,iy,iz,it,4)%Cl
    end do; end do; end do; end do


  end subroutine H_aria

  subroutine Hsq_aria(phi,Hsqphi)
    ! begin args: phi, Hsqphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Hsqphi
    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: Hphi

    integer :: ix,iy,iz,it,is

    ! begin execution
    allocate(Hphi(nxp,nyp,nzp,ntp,ns))

    call H_aria(phi,Hphi)
    call H_aria(Hphi,Hsqphi)

    deallocate(Hphi)

  end subroutine Hsq_aria

  subroutine Hsq_aria_rescale(phi,Dphi)
    ! begin args: phi, Dphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: Dphi

    ! begin local_vars
    integer :: ix,iy,iz,it,is

    ! The polynomial of Q^2 which we use is 2/(s^2)*Q^2 - (1+r)I
    ! where s is the spectral radius of Q^2 and r is a parameter
    ! which controls the portion of the spectrum we concentrate
    ! on. This polynomial maps the (all positive) spectrum of
    ! Q^2 into the region [-1-r,1-r]

    ! begin execution

    call Hsq_aria(phi,Dphi)
    do is=1,ns
       do it=1,nt; do iz=1,nz; do iy=1,ny; do ix=1,nx
          Dphi(ix,iy,iz,it,is)%Cl = (2.0d0/s_scale)*Dphi(ix,iy,iz,it,is)%Cl - (1.0d0 + r_scale)*phi(ix,iy,iz,it,is)%Cl
       end do; end do; end do; end do
    end do

  end subroutine Hsq_aria_rescale

  subroutine H_chebyshev(phi,T_nphi)
    ! begin args: phi, T_nphi

    type(colour_vector), dimension(:,:,:,:,:) :: phi
    type(colour_vector), dimension(:,:,:,:,:) :: T_nphi

    ! begin local_vars
    type(colour_vector), dimension(:,:,:,:,:), allocatable :: xT_nphi, T_nm1phi

    integer :: i_k

    ! We use a Chebyshev polynomial of Q_poly to decrease the
    ! level density of eigenvalues. This works due to the rapid
    ! increase of Chebyshev polynomials outside the interval
    ! [-1,1] (within which they are close to zero).

    ! begin execution
    allocate(xT_nphi(nxp,nyp,nzp,ntp,ns))
    allocate( T_nm1phi(nxp,nyp,nzp,ntp,ns))

    T_nm1phi = phi
    call Hsq_aria_rescale(phi,T_nphi)

    do i_k=2,n_chebyshev
       call Hsq_aria_rescale(T_nphi,xT_nphi)
       call AlphaPsiMinusPhi(xT_nphi,2.0d0,T_nm1phi)
       T_nm1phi = T_nphi
       T_nphi = xT_nphi
    end do

    deallocate(xT_nphi)
    deallocate( T_nm1phi)

  end subroutine H_chebyshev





end module ARIA
