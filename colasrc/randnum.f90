
module RandNum

  !Author (in HPF): Paul Coddington,
  !Modified to MPI by Waseem Kamleh

  use Kinds
  use MPIInterface
#ifdef _hydra_
  use IFLPort
#endif
  implicit none

contains
  
  !!     Initialize the intrinsic Fortran random number generator
  !!     using a single integer seed.
  !!
  !!     Fortran 90/95 has an intrinsic function RANDOM_NUMBER for
  !!     generating pseudo-random numbers. There is also an intrinsic
  !!     function RANDOM_SEED for initializing the random number
  !!     generator.
  !!
  !!     The problem with the RANDOM_SEED intrinsic function is that
  !!     the number of seeds it takes as input parameters depends on
  !!     how the random number generator is implemented, so it will
  !!     vary with different Fortran compilers. It is up to the user
  !!     to check how many seeds are required and then to provide the
  !!     appropriate number of *random* seeds -- choosing correlated
  !!     seeds such as seed, seed+1, seed+2, etc will affect the
  !!     randomness properties of most random number generators.
  !!
  !!     Of course this means that you need to use a random number
  !!     generator in order to initialize the random number generator,
  !!     and that is what is provided here.
  !!
  !!     The routine HPF_RANDOM_SEED takes a single seed as input,
  !!     checks the Fortran 90 intrinsic RANDOM_SEED to see how many
  !!     seeds are required, and then calls the routine bitrand_seeder,
  !!     which uses a simple linear congruential random number
  !!     generator to create the appropriate number of random seeds,
  !!     using random values for each bit of each seed. The seeds are 
  !!     passed to RANDOM_SEED to initialize the RANDOM_NUMBER
  !!     intrinsic function.


  subroutine mpi_random_seed(iseed)

    implicit none
    integer :: iseed
    integer :: i,N
    integer, dimension(:), allocatable :: seeder_array

    !--- Find out the size of the seed array used in the Fortran 90 
    !--- random number generator
    call random_seed(size=N)

    !--- Initialize the seed array by setting each bit using a 
    !--- simple linear congruential generator (in bitrand_seeder)
    allocate ( seeder_array(N) ) 

    !Modification to MPI:
    !This code executes identically in each process,
    !so we must include a rank dependent seed to make sure that
    !we get different random numbers on each process.

#ifndef _hydra_
    call bitrand_seeder(iseed+mpi_rank,N,seeder_array)
#else
    call seed(2**31 - 2*iseed - 4096*mpi_rank - 1)
    do i=1,N
       seeder_array(i) = irand()
    end do
#endif

    

    call random_seed( put=seeder_array(1:N) )
    deallocate( seeder_array )

  end subroutine mpi_random_seed


  !----------------------------------------------------------------------!
  !                                                                      !
  !     Initialize each bit of the 32-bit integer seed array using a     !
  !     simple linear congruential generator.                            !
  !                                                                      !
  !----------------------------------------------------------------------!

  subroutine bitrand_seeder(seed,N,seed_array)
    implicit none
    integer :: seed,N
    integer :: seed_array(N)
    integer :: i,j,k
    integer :: s,t,randx
    real(dp), parameter :: TWOTO31 = 2147483648.0   ! 2^{31} 
    real(dp), parameter :: TWOTONEG31=(1.0/TWOTO31)
    integer, parameter :: A=1103515245, C=12345
    integer :: M
    data  M   /Z'7FFFFFFF'/

    !--- Initialize the linear congruential generator RAND.
    randx = seed

    do i = 1, N
       s = 0
       t = 1
       do j = 0, 30
          randx = iand((A * randx + C),M)  
          if((randx * TWOTONEG31) .lt. 0.5) then
             s = s + t
          endif
          t = 2 * t
          !------- Throw away a few random numbers just to foil any 
          !------- possible correlations that might affect RAND.
          do k=1,5
             randx = iand((A * randx + C),M)     
          enddo
       enddo
       !------- Throw away a few random numbers just to foil any 
       !------- possible correlations that might affect RAND.
       do k=1,13
          randx = iand((A * randx + C),M)     
       enddo

       seed_array(i) = s

    enddo

    return
  end subroutine bitrand_seeder

end module RandNum
