!! A program to produce random gauge transformations

program RandomGauge
  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use ReduceOps
  use MatrixAlgebra
  use gaugefix
  use MCGops
  use gaugeops
  use genmin
  use matrixops
  use RandNum
  implicit none

  character(len=512) :: UTconfig, outconfig
  character(len=32) :: gaugefieldformat
  character(len=20) :: startDate,startTime,endDate,endTime
  type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: Uug_xd

  type(colour_matrix),dimension(:,:,:,:),allocatable :: G
  complex(wc) :: Rconfig,Rconfigprev,Rconfiginit,Rconfigfin
  integer,dimension(4) :: x,xmu
  real(dp) :: tol
  integer :: ix, iy, iz, it, id

  call InitialiseMPI
  call InitTimer

  call mpi_random_seed(314159)

  call Put("Enter config name")
  call Get(UTconfig)
  call Put(UTconfig)
  call Put("Enter gauge field format")
  call Get(gaugefieldformat)
  call Put("Enter Output Location")
  call Get(outconfig)
  call Put(outconfig)

  call Put("Initialising the lattice.")
  call InitialiseLattice(outconfig)

  call Put("Starting time")
  call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(Uug_xd,1)

  call Put("Reading Gauge Field, format = "//trim(gaugefieldformat))
  call ReadGaugeField(UTConfig, Uug_xd, file_format = gaugefieldformat)
  allocate(G(nlx,nly,nlz,nlt))

  call RandomSU3Field(G)
  call put("Performing gauge transformation")
  call GaugeRotate (Uug_xd,G)
  ! do it=1,nt; do iz=1,nz; do iy=1,ny; do ix = 1,nx; do id = 1,nd
  !    call FixSU3Matrix(Uug_xd(ix,iy,iz,it,id))
  ! end do; end do; end do; end do; end do

  call WriteGaugeField(trim(outconfig),Uug_xd,1,'unknown',trim(gaugefieldformat))
  call WriteGaugeTrans(trim(outconfig)//".gag",G)
  call DestroyGaugeField(Uug_xd)

  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if

  call FinaliseMPI  

end program RandomGauge
