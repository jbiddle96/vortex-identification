! Program to output the MCG R values from each site of a single configuration
program RHist
  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use RealFieldMPIComms
  use ReduceOps
  use MatrixAlgebra
  implicit none

  character(len=512) :: inConfig, outFile
  character(len=32)  :: gaugefieldformat
  character(len=20)  :: startDate,startTime,endDate,endTime
  integer :: otfl

  real(DP), dimension(:), allocatable :: R_vals
  integer :: counter, iproc

  ! Binning variables
  real(DP), dimension(:, :), allocatable :: R_hist, temp_hist
  real(DP) :: bin_width, max_value, lb, ub
  integer :: nbins, ibin
  logical :: bin_output

  real(DP) :: R
  integer :: ix, iy, iz, it, mu

  call InitialiseMPI
  call InitTimer

  ! Get inputs
  call Put("Enter input config name")
  call Get(inConfig)
  call Put(inConfig)
  call Put("Enter gauge field format")
  call Get(gaugefieldformat)
  call Put("Enter output filename")
  call Get(outFile)
  call Put(outFile)
  call Put("Should the output be binned?")
  call Get(bin_output)
  call Put(bin_output)
  call Put("How many bins should be used?")
  call Put("If not binning, enter any number")
  call Get(nbins)
  call Put(nbins)

  ! Initialise the lattice
  call Put("Initialising the lattice.")
  call InitialiseLattice(outFile)

  call Put("Starting time")
  call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(U_xd,1)

  call Put("Reading Gauge Field, format = "//trim(gaugefieldformat))
  call ReadGaugeField(inConfig, U_xd, file_format = gaugefieldformat)

  allocate(R_vals(nsublattice * nd))

  ! Begin calculation
  counter = 1
  do ix=1,nx; do iy=1,ny; do iz=1,nz; do it=1,nt;do mu=1,4
    R = abs(U_xd(ix,iy,iz,it,mu)%cl(1,1) &
        + U_xd(ix,iy,iz,it,mu)%cl(2,2) &
        + U_xd(ix,iy,iz,it,mu)%cl(3,3))**2
    R = R/(nc**2)
    R_vals(counter) = R
    counter = counter + 1
    ! write(otfl, *) R
  enddo;enddo;enddo;enddo;enddo

  if(bin_output) then
    ! Hist arrays store (upper_bound, counts)
    allocate(R_hist(2, nbins), temp_hist(2, nbins))
    max_value = 1.0d0
    bin_width = max_value / real(nbins, DP)

    lb = 0.0d0
    ub = bin_width
    do ibin = 1, nbins
      R_hist(1, ibin) = ub
      R_hist(2, ibin) = count(R_vals >= lb .and. R_vals < ub)

      lb = lb + bin_width
      ub = ub + bin_width
    end do
  end if

  call OpenTextfile(outFile, otfl, 'write')
  if(bin_output) then
    ! Output binned data
    if(i_am_root) then
      do iproc = 1, mpi_size - 1
        call RecvRealField(temp_hist, iproc)
        R_hist(2, :) = R_hist(2, :) + temp_hist(2, :)
        write(*,*) "Received from ", iproc
      end do
      call output_hist(R_hist, otfl)
    else
      call SendRealField(R_hist, mpi_root_rank)
    end if
  else
    ! Output raw data
    if(i_am_root) then
      call output_raw(R_vals, otfl)
      write(*,*) "Finished writing from root"
    else
      call SendRealField(R_vals, mpi_root_rank)
      write(*,*) "Sent from ", mpi_rank
    end if

    if(i_am_root) then
      do iproc = 1, mpi_size - 1
        call RecvRealField(R_vals, iproc)
        write(*,*) "Received from ", iproc
        call output_raw(R_vals, otfl)
      end do
    end if
  end if
  call CloseTextFile(otfl)

  ! Finalise
  call DestroyGaugeField(U_xd)
  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if

  call FinaliseMPI

contains

  subroutine output_raw(array, file)
    real(DP), dimension(:) :: array
    integer :: file

    integer :: counter

    counter = 1
    do mu = 1,nd
      do it = 1,nt; do iz = 1,nz; do iy = 1,ny; do ix = 1,nx
        write(file, *) array(counter)
        counter = counter + 1
      end do; end do; end do; end do;
    end do

  end subroutine output_raw

  subroutine output_hist(hist, file)
    real(DP), dimension(:, :) :: hist
    integer :: file

    do ibin = 1, nbins
      write(file, *) hist(:, ibin)
    end do

  end subroutine output_hist

end program RHist
