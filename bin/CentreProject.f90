program CentreProject
  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use TextIO
  use Kinds
  use ColourTypes
  use ColourFieldOps
  use ProjectionOps
  use MPIInterface
  implicit none

  character(len=4) :: field_format
  
  character(len=512) :: MCGconfig, outconfig
  type(colour_matrix), dimension(:,:,:,:,:), allocatable :: Uproj_xd
  complex(wc), dimension(:,:,:,:,:), allocatable :: TrUmcg_xd

  call InitialiseMPI

  ! Read in MCG field
  call Put("Enter MCG config name")
  call Get(MCGconfig)
  call Put(MCGconfig)
  call Put("Enter output config name")
  call Get(outconfig)
  call Put(outconfig)
  call Put("Enter field format")
  call Get(field_format)
  call Put(field_format)

  call Put("Initialising the lattice.")
  call InitialiseLattice(outconfig)

  call CreateGaugeField(Uproj_xd,1)

  call Put("Reading Gauge Field, format = "//trim(field_format))
  call ReadGaugeField(MCGConfig, Uproj_xd, file_format = field_format)

  allocate(TrUmcg_xd(nlx,nly,nlz,nlt,4))
  ! Get traces and centre project
  call traces(Uproj_xd,TrUmcg_xd)
  call Project(TrUmcg_xd,Uproj_xd)

  ! Write the projected field
  call WriteGaugeField(outconfig, Uproj_xd,0,"unknown",file_format = field_format)
  call MPIBarrier
  if(I_AM_ROOT) write(*,*) "Finished writing"

  call DestroyGaugeField(Uproj_xd)
  
  call FinaliseMPI
end program CentreProject
