!! A program to gaugefix configurations to maximal center gauge

program MCGfix
  use GaugeField
  use GaugeFieldIO
  use GaugeFieldMPIComms
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use ReduceOps
  use MatrixAlgebra
  use gaugefix
  use MCGops
  use gaugeops
  use genmin
  use matrixops
  use MPIOps
  implicit none

  integer, parameter :: ishdw = 1
  integer, parameter :: logfl = 200

  character(len=512) :: UTconfig,outconfig
  character(len=32) :: gaugefieldformat
  character(len=20) :: startDate,startTime,endDate,endTime,dateFormatted,timeFormatted
  type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: Umcg_xd
  integer :: maxsweeps, reunitarise_every

  complex(wc), dimension(4) :: B
  complex(wc) :: C
  complex(wc), dimension(4,4) :: A

  real(dp), dimension(4) :: Gcomp
  type(colour_matrix) :: dummy, Gsweep
  type(colour_matrix),dimension(:,:,:,:),allocatable :: Gtotal
  complex(wc) :: Rconfig,Rconfigprev,Rconfiginit,Rconfigfin
  integer,dimension(4) :: x, x_lat

  integer :: jx,jy,jz,jt,isweeps,subg,evenodd,ii

  character(len=256) :: gaugeloc
  logical :: initguess

  character(len=9) :: i_char

  call InitialiseMPI
  call InitTimer

  call Put("Enter untouched config name")
  call Get(UTconfig)
  call Put(trim(UTconfig))
  call Put("Enter output config name")
  call Get(outconfig)
  call Put(trim(outconfig))
  call Put("Use initial guess?")
  call Get(initguess)
  call Put("Initial Guess location")
  call Get(gaugeloc)
  call Put("Enter Format")
  call Get(gaugefieldformat)
  call Put(gaugefieldformat)
  call Put("Enter Max Sweeps")
  call Get(maxsweeps)
  call Put(maxsweeps)
  call Put("How often should we re-unitarise the gauge field?")
  call Get(reunitarise_every)
  call Put(reunitarise_every)

  call Put("Initialising the lattice.")
  call InitialiseLattice(outconfig)

  if(nprocz > 1) then
     call Put("MCG fixing code currently only supports nproct > 1")
     call Put("Aborting")
     call FinaliseMPI()
     Stop
  end if

  call Put("Starting time")
  if(i_am_root) call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(Umcg_xd,ishdw)
  call AllocMatrixField(Gtotal,'xyzt', (/ishdw,ishdw,ishdw,ishdw/))

  call Put("Reading Gauge Field, format = "//trim(gaugefieldformat))
  call ReadGaugeField(UTConfig, Umcg_xd, file_format = gaugefieldformat)

  call GetUZero(Umcg_xd, uzero)
  call Put("Initial uzero:")
  call Put(uzero)

  if (initguess) then
     call ReadGaugeTrans(trim(gaugeloc),Gtotal)
     call ShadowSU3Field(Gtotal, ishdw)
     call Put("Using initial guess")
     call GaugeRotate(Umcg_xd, Gtotal)
     call ShadowGaugeField(Umcg_xd, ishdw)

     call GetUZero(Umcg_xd, uzero)
     call Put("Gauge transformed uzero")
     call Put(uzero)
  else
     Gtotal(:,:,:,:) = unit_matrix
     Gsweep = unit_matrix
     call ShadowSU3Field(Gtotal, ishdw)
  endif
  call Put("Initial Total R")
  call CalcR(Umcg_xd,Rconfiginit)
  call Put(Rconfiginit)
  Rconfig=Rconfiginit

  ! Open log file to store output
  if(i_am_root) then
     open(logfl, file=trim(outconfig)//".log", status="replace", action="write")
     write(logfl,'(a11,2(a19))') "Sweep      ", "R                  ", "R/Rprev            "
     write(i_char, '(i9)') 0
     write(logfl, '(a9,2(f19.15))') adjustl(i_char), real(Rconfiginit), 0.0d0
  end if

  do isweeps=1,maxsweeps
     call Put("sweep")
     call Put(isweeps)
     Rconfigprev=Rconfig
     if(i_am_root) then
        write(*,*) "Before sweep:"
     end if
     if(mpi_rank==0) then
        call SubLatticeToLattice(1,1,1,1,jx,jy,jz,jt)
        write(*,*) jx,jy,jz,jt
     end if
     if(mpi_rank==1) then
        call SubLatticeToLattice(1,1,1,1,jx,jy,jz,jt)
        write(*,*) jx,jy,jz,jt
     end if
     do evenodd=0,1
        do subg=1,3
           do jx=1,nx;do jy=1,ny; do jz=1,nz; do jt=1,nt
              x(1)=jx
              x(2)=jy
              x(3)=jz
              x(4)=jt
              ! Sweep over even lattice sites, then odd
              ! if(mod(sum(x), 2) == evenodd) then
              if(check_eo(x) == evenodd) then
                 !!Find polynomial corresponding to local gauge-fixing condition at this site
                 call RTerms(Umcg_xd,x,A,B,C,subg)
                 !!Find Gcomp that maximises local gauge-fixing condition
                 call MaxR(A,B,C,Gcomp)
                 !!Create corresponding local gauge-transformation
                 call CreateGaugeMat(Gcomp, Gsweep, subg)
                 !!Store it
                 call MultiplyMatMat(dummy, Gsweep, Gtotal(jx,jy,jz,jt))
                 Gtotal(jx,jy,jz,jt)=dummy
                 !!Apply gauge transformation to lattice
                 call updatesite(Gsweep, Umcg_xd, x)
              end if
           enddo;enddo;enddo;enddo
        end do
        ! Send relevant information to adjacent processors
        call ShadowEvenOdd(Umcg_xd, evenodd)

     end do
     ! Shadow the gauge transformation
     call ShadowSU3Field(Gtotal, ishdw)

     if(modulo(isweeps, reunitarise_every) == 0) then
        call FixGaugeField(Umcg_xd)
     end if

     ! Calculate sweep R value and improvement
     call Put("New total R")
     call CalcR(Umcg_xd,Rconfig)
     call Put(Rconfig)
     call Put("Improvement of")
     call Put(real(Rconfig)/real(Rconfigprev))

     ! Write to log file
     if(i_am_root) then
        write(i_char, '(i9)') isweeps
        write(logfl, '(a9,2(f19.15))') adjustl(i_char), real(Rconfig), real(Rconfig)/real(Rconfigprev)
     end if

  end do

  call Put("FinalR")
  call CalcR(Umcg_xd,Rconfigfin)
  call Put(Rconfigfin)

  call Put("Resulting in proportional R increase of:")
  call Put(real(Rconfigfin)/real(Rconfiginit))

  ! Shadowing necessary as even-odd shadowing only updates backward links
  call ShadowGaugeField(Umcg_xd, ishdw)
  call GetUZero(Umcg_xd, uzero)
  call Put("Final uzero:")
  call Put(uzero)

  call WriteGaugeTrans(trim(outconfig)//".MCGgag",Gtotal)
  call WriteGaugeField(outconfig, Umcg_xd,0,"unknown",file_format = gaugefieldformat)

  call Put("Finished writing")

  call DeallocMatrixField(Gtotal)
  call DestroyGaugeField(Umcg_xd)
  call Put("Finished deallocate")

  if ( i_am_root ) then
     call date_and_time(date=endDate, time=endTime)
     write(dateFormatted,'(a4,2(a1,a2))') startDate(1:4),"/",startDate(5:6),"/",startDate(7:8)
     write(timeFormatted,'(2(a2,a1),a2)') startTime(1:2),":",startTime(3:4),":",startTime(5:6)
     write(*,'(a,a10,a,a10)') "Started on ", trim(dateFormatted), " at ", trim(timeFormatted)
     write(logfl,'(a,a10,a,a10)') "Started on ", trim(dateFormatted), " at ", trim(timeFormatted)
     write(dateFormatted,'(a4,2(a1,a2))') endDate(1:4), "/", endDate(5:6), "/", endDate(7:8)
     write(timeFormatted,'(2(a2,a1),a2)') endTime(1:2),":",endTime(3:4),":",endTime(5:6)
     write(*,'(a,a10,a,a10)') "Finished on ", trim(dateFormatted), " at ", trim(timeFormatted)
     write(logfl,'(a,a10,a,a10)') "Finished on ", trim(dateFormatted), " at ", trim(timeFormatted)
     close(logfl)
  end if

  call FinaliseMPI

end program
