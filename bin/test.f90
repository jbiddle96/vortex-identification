!! File for testing functionality

program MCGfix
  use GaugeField
  use GaugeFieldIO
  use GaugeFieldMPIComms
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use ReduceOps
  use MatrixAlgebra
  use gaugefix
  use MCGops
  use gaugeops
  use genmin
  use matrixops
  implicit none

  character(len=512) :: UTconfig
  character(len=32) :: gaugefieldformat
  character(len=20) :: startDate,startTime,endDate,endTime
  type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: Uut_xd
  type(colour_matrix),dimension(:,:,:,:),allocatable :: G1, G2, GRandom

  complex(wc), dimension(4) :: B
  complex(wc) :: C
  complex(wc), dimension(4,4) :: A

  real(dp), dimension(4) :: Gcomp
  integer,dimension(4) :: x,xmu
  real(dp), parameter :: tol = 1d-10

  integer :: jx,jy,jz,jt,isweeps,subg,mu,ic,jc, diffcounter

  character(len=256) :: gaugeloc1, gaugeloc2, stub
  logical :: initguess


  call InitialiseMPI
  call InitTimer

  call Put("Enter untouched config name")
  call Get(UTconfig)
  call Put(UTconfig)
  call Put("Enter input stub")
  call Get(stub)
  call Put(trim(stub))
  call Put("Enter first gauge transformation")
  call Get(gaugeloc1)
  call Put(trim(gaugeloc1))
  call Put("Enter second gauge transformation")
  call Get(gaugeloc2)
  call Put(trim(gaugeloc2))
  call Put("Enter Format")
  call Get(gaugefieldformat)
  call Put(gaugefieldformat)

  call Put("Initialising the lattice.")
  call InitialiseLattice(UTconfig)

  call Put("Starting time")
  call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(Uut_xd,1)
  call AllocMatrixField(G1,'xyzt', (/1,1,1,1/))
  call AllocMatrixField(G2,'xyzt', (/1,1,1,1/))
  call AllocMatrixField(GRandom, 'xyzt', (/1,1,1,1/))

  call ReadGaugeField(UTConfig, Uut_xd, file_format = gaugefieldformat)
  call ReadGaugeTrans(trim(gaugeloc1),G1)
  call ShadowSU3Field(G1,1)
  call ReadGaugeTrans(trim(gaugeloc2),G2)
  call ShadowSU3Field(G2,1)
  call RandomSU3Field(GRandom)
  call ShadowSU3Field(GRandom,1)

  ! Initial values
  call GetUZero(Uut_xd, uzero)
  call Put("Initial uzero")
  call Put(uzero)
  call Put("")

  call GaugeRotate(Uut_xd, GRandom)
  call ShadowGaugeField(Uut_xd, 1)
  call GetUZero(Uut_xd, uzero)
  call Put("Gauge transformation random uzero")
  call Put(uzero)
  call Put("")
  call UndoGaugeRotate(Uut_xd, GRandom)
  call ShadowGaugeField(Uut_xd, 1)

  call GaugeRotate(Uut_xd, G1)
  call ShadowGaugeField(Uut_xd, 1)
  call GetUZero(Uut_xd, uzero)
  call Put("Gauge transformation 1 uzero")
  call Put(uzero)
  call Put("")
  call UndoGaugeRotate(Uut_xd, G1)
  call ShadowGaugeField(Uut_xd, 1)

  call GaugeRotate(Uut_xd, G2)
  call ShadowGaugeField(Uut_xd, 1)
  call GetUZero(Uut_xd, uzero)
  call Put("Gauge transformation 2 uzero")
  call Put(uzero)
  call Put("")
  ! call UndoGaugeRotate(Uut_xd, G2)

  call DestroyGaugeField(Uut_xd)
  call Put("p1")
  call DeallocMatrixField(G1)
  call Put("p2")
  call DeallocMatrixField(G2)
  call Put("p3")
  call DeallocMatrixField(GRandom)
  call Put("p4")

  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if

  call FinaliseMPI

end program MCGfix
