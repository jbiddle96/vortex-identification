
!Author: Waseem Kamleh
!Date: May, 2001

program UZeroBar

  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use FatLinks
  use StoutLinks
  implicit none

  character(len=8) :: startDate, endDate
  character(len=10) :: startTime, endTime
  character(len=128) :: file_stub
  character(len=256)  :: InputConfig, InputListFile
  character(len=32) :: InputFileFormat
  character(len=3) :: Config

  integer :: icfg,ncfg,ismear,listfile_unit
  real(dp), allocatable :: u0_i(:), u0fl_i(:)
  real(dp) :: u0_err, u0fl_err


  call InitialiseMPI
  call InitTimer

  call Put("Please enter the data file prefix:")
  call Get(file_stub)
  call Put("Reading configuration parameters.")
  call GetUzeroBarParams(file_stub)
  call Put("Initialising the lattice.")
  call InitialiseLattice(file_stub)

  call Put("Starting time")
  call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(U_xd,1)
  if ( ismear > 0) call CreateGaugeField(Usm_xd,1)

  call Put("Reading Gauge Fields, format = "//trim(InputFileFormat))

  allocate(u0_i(ncfg))
  allocate(u0fl_i(ncfg))

!  mpiprint '(A3,3A20)', "icfg", "uzero", "u0_bar", "u0fl_bar"
  write(*,*) "icfg","uzero","u0_bar","u0fl_bar"
  call OpenTextfile(trim(InputListFile),listfile_unit,action='read')
  do icfg=1,ncfg
     call Get(InputConfig,listfile_unit)
     call ReadGaugeField(InputConfig, U_xd, file_format = InputFileFormat)
     call GetUZero(U_xd,u0_bar)

     if ( ismear == 1 ) then
        call StoutSmearLinks(U_xd,Usm_xd,alpha_smear,ape_sweeps)
        call GetUZero(Usm_xd,u0fl_bar)
     else if ( ismear == 2 ) then
        call APESmearLinks(U_xd,Usm_xd,alpha_smear,ape_sweeps)
        call GetUZero(Usm_xd,u0fl_bar)
     else
        u0fl_bar = u0_bar
     end if

!     mpiprint '(I3.3,3F20.15)', icfg, uzero, u0_bar, u0fl_bar
     write(*,*) icfg,uzero,u0_bar,u0fl_bar
     u0_i(icfg) = u0_bar
     u0fl_i(icfg) = u0fl_bar
  end do
  call CloseTextfile(listfile_unit)

  u0_bar = sum(u0_i)/size(u0_i)
  u0fl_bar = sum(u0fl_i)/size(u0fl_i)

  u0_err = maxval(abs(u0_i - u0_bar))
  u0fl_err = maxval(abs(u0fl_i - u0fl_bar))

!  mpiprint '(A,3F20.15)', "AVG", uzero, u0_bar, u0fl_bar
!  mpiprint '(A,3F20.15)', "ERR",   0.0, u0_err, u0fl_err
  write(*,*) "AVG", uzero, u0_bar, u0fl_bar
  write(*,*) "ERR",   0.0, u0_err, u0fl_err

  deallocate(u0_i)
  deallocate(u0fl_i)

  call DestroyGaugeField(U_xd)
  if ( ismear > 0 ) call DestroyGaugeField(Usm_xd)
  call FinaliseLattice

  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if

  call FinaliseMPI

contains

  subroutine GetUzeroBarParams(file_stub)
    character(len=*) :: file_stub
    character(len=256) :: propfile
    character(len=6), dimension(0:2) :: smear_text
    integer :: file_unit

    smear_text(0) = " none "
    smear_text(1) = " stout"
    smear_text(2) = " APE  "
    
    propfile = trim(file_stub)//".u0bar"
    call OpenTextfile(trim(propfile),file_unit,action='read')
    call Get(InputListFile,file_unit)
    call Get(InputFileFormat,file_unit)
    call Get(ncfg,file_unit)
    call Get(ismear,file_unit)
    call Get(alpha_smear ,file_unit)
    call Get(ape_sweeps,file_unit)
    call CloseTextfile(file_unit)

    call Put("UzeroBar parameters:")
    call Put("Input list file:"//trim(InputListFile))
    call Put("Input cfg file format:"//trim(InputFileFormat))
    call Put("No of configs to read: "//str(ncfg,8))
    call Put("Smearing type: ismear="//str(ismear,6)//smear_text(ismear))
    call Put("Smearing fraction: alpha_smear="//str(alpha_smear,24))
    call Put("Smearing sweeps: ape_sweeps="//str(ape_sweeps,10))

  end subroutine GetUzeroBarParams

end program UZeroBar




