! A program to read in a configuration in maximal centre gauge, project to centre elements and output a vortex only and vortex removed configuration

program IdentifyVortices
  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use ReduceOps
  use MatrixAlgebra
  use gaugefix
  use MCGops
  use gaugeops
  use projectionops
  use matrixops
  implicit none

  type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: Umcg_xd,Uvr_xd,Uvo_xd
  character(len=256) :: Umcg_xdloc,Uvr_xdloc,Uvo_xdloc,gaugeloc
  character(len=4) :: gaugefieldformat
  character(len=20) :: startDate,startTime,endDate,endTime

  type(colour_matrix),dimension(:,:,:,:),allocatable :: G
  complex(wc),dimension(:,:,:,:,:),allocatable :: TRUut_xd
  real(dp),dimension(:,:),allocatable :: bins
  real(dp) :: binmin,binmax
  integer :: binsize

  integer :: ix,iy,iz,it,imu
  integer :: ic,jc
  logical :: gaugetrans,isWriteFields,isWritePhase


  call InitialiseMPI
  call InitTimer
  call Put("MCG configuration name?")
  call Get(Umcg_xdloc)
  call Put(trim(Umcg_xdloc))
  call Put("Vortex removed configuration name?")
  call Get(Uvr_xdloc)
  call Put(trim(Uvr_xdloc))
  call Put("Vortex only configuration name?")
  call Get(Uvo_xdloc)
  call Put(trim(Uvo_xdloc))
  call Put("Gauge Field Format?")
  call Get(gaugefieldformat)
  call Put(gaugefieldformat)  


  call Put("Initialising the lattice.")
  call InitialiseLattice(Uvo_xdloc)

  allocate(G(nlx,nly,nlz,nlt),TRUut_xd(nlx,nly,nlz,nlt,4))

  call Put("Starting time")
  call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(Umcg_xd,1)
  call CreateGaugeField(Uvr_xd,1)
  call CreateGaugeField(Uvo_xd,1)

  call Put("Reading Gauge Field, format = "//trim(gaugefieldformat))
  call ReadGaugeField(Umcg_xdloc, Umcg_xd, file_format = gaugefieldformat)

  call Put("Calculating Trace")
  call traces(Umcg_xd,TRUut_xd)

  call Put("Projecting to Z3")
  call Project(TRUut_xd,Uvo_xd)


  call Put("Binning Phase Data")
  binsize = 40
  binmin = -2
  binmax = 2
  allocate(bins(binsize,2))
  call BinCentres(TRUut_xd,bins,binmin,binmax,binsize)
  call Put("Writing Centres")
  open(unit=30,file=trim(Uvo_xdloc)//".dists")
  do ic=1,binsize
     write(30,'(f5.2,a1,f8.0)') bins(ic,1)," ", bins(ic,2)
  enddo
  close(30)


  call Put("Removing Vortices")
  call VortexRemove(Umcg_xd,Uvr_xd,Uvo_xd)

  call Put("Writing Gauge Fields")
  call WriteGaugeField(trim(Uvr_xdloc),Uvr_xd,1,'unknown',gaugefieldformat)
  call WriteGaugeField(trim(Uvo_xdloc),Uvo_xd,1,'unknown',gaugefieldformat)

  call Put("Finishing up")
  deallocate(G,TRUut_xd)
  call DestroyGaugeField(Umcg_xd)
  call DestroyGaugeField(Uvr_xd)
  call DestroyGaugeField(Uvo_xd)


  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if

  call FinaliseMPI

end program IdentifyVortices
