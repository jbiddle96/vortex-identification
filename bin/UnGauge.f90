!! A program to read in a configuration and a gauge transformation, then apply the inverse transformation.

program UnGauge
  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use ReduceOps
  use MatrixAlgebra
  use gaugefix
  use MCGops
  use gaugeops
  use genmin
  use matrixops
  implicit none

  character(len=512) :: VRconfig,UTconfig,VOconfig,outconfig
  character(len=32) :: gaugefieldformat
  character(len=20) :: startDate,startTime,endDate,endTime
  type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: Uug_xd

  type(colour_matrix),dimension(:,:,:,:),allocatable :: G,Gadj

  integer :: jx,jy,jz,jt,isweeps,subg,mu,ic,jc

  character(len=256) :: gaugeloc
  logical :: initguess


  call InitialiseMPI
  call InitTimer

  call Put("Enter config name")
  call Get(UTconfig)
  call Put(UTconfig)
  call Put("Enter Output Location")
  call Get(outconfig)
  call Put(outconfig)
  call Put("Enter Gauge Transformation Location")
  call Get(gaugeloc)
  call Put(gaugeloc)
  call Put("Enter Format")
  call Get(gaugefieldformat)
  call Put(gaugefieldformat)

  call Put("Initialising the lattice.")
  call InitialiseLattice(UTconfig)

  call Put("Starting time")
  call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(Uug_xd,1)

  call Put("Reading Gauge Field, format = "//trim(gaugefieldformat))
  call ReadGaugeField(UTConfig, Uug_xd, file_format = gaugefieldformat)
  allocate(G(nlx,nly,nlz,nlt),Gadj(nlx,nly,nlz,nlt))
 
  call Put("Reading Gauge Transformation")
  call ReadGaugeTrans(trim(gaugeloc),G)

  do jx=1,nlx;do jy=1,nly;do jz=1,nlz;do jt=1,nlt
     do ic=1,3;do jc=1,3
        Gadj(jx,jy,jz,jt)%cl(ic,jc)=CONJG(G(jx,jy,jz,jt)%cl(jc,ic))
     enddo;enddo
  enddo;enddo;enddo;enddo

  call Put("Performing transformation")
  call updatelattice(Gadj,Uug_xd)

  call WriteGaugeField(trim(outconfig),Uug_xd,1,'unknown','cssm')

  call DestroyGaugeField(Uug_xd)
  
  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if
  
  call FinaliseMPI  
  
end program UnGauge
