!! A program to produce random gauge transformations

program ApplyGauge
  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use ReduceOps
  use MatrixAlgebra
  use gaugefix
  use MCGops
  use gaugeops
  use genmin
  use matrixops
  implicit none

  character(len=512) :: UTconfig,GTconfig,outconfig
  character(len=32) :: gaugefieldformat
  character(len=20) :: startDate,startTime,endDate,endTime
  type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: Uug_xd, Uug_xd2

  type(colour_matrix),dimension(:,:,:,:),allocatable :: G
  complex(wc) :: Rconfig,Rconfigprev,Rconfiginit,Rconfigfin
  integer,dimension(4) :: x,xmu
  real(dp) :: tol
  integer :: ii

  call InitialiseMPI
  call InitTimer

  call Put("Enter config name")
  call Get(UTconfig)
  call Put(trim(UTconfig))
  call Put("Enter Output Location")
  call Get(outconfig)
  call Put(trim(outconfig))
  call Put("Enter gauge transformation")
  call Get(GTconfig)
  call Put(trim(GTconfig))

  gaugefieldformat="cssm"

  call Put("Initialising the lattice.")
  call InitialiseLattice(UTconfig)

  call Put("Starting time")
  call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(Uug_xd,0)
  call CreateGaugeField(Uug_xd2,0)

  call Put("Reading Gauge Field, format = "//trim(gaugefieldformat))
  call ReadGaugeField(trim(UTConfig), Uug_xd, file_format = gaugefieldformat)

  allocate(G(nlx,nly,nlz,nlt))
  call Put("Reading Gauge Transformation")
  call ReadGaugeTrans(trim(GTconfig),G)

  open(110,file="./uzero.log",status="unknown", action="write",position="append")
  call Getuzero(Uug_xd,uzero)
  write(*,*) nc,nt,nx,ny,nz
  write(*,*) "Initial u0 value:  ",uzero
  write(110,*) "Initial u0 value:  ",uzero
  !  call RandomSU3Field(G)
  !  call GaugeRotate (Uug_xd,G)

  !  call Put("Reading Gauge Transformation")
  !  call ReadGaugeTrans(trim(gaugeloc),G)

  !  do jx=1,nlx;do jy=1,nly;do jz=1,nlz;do jt=1,nlt
  !     do ic=1,3;do jc=1,3
  !        Gadj(jx,jy,jz,jt)%cl(ic,jc)=CONJG(G(jx,jy,jz,jt)%cl(jc,ic))

  !     enddo;enddo
  !  enddo;enddo;enddo;enddo

  call Put("Performing transformation")
  call GaugeRotate(Uug_xd,G)

  call Getuzero(Uug_xd,uzero)
  write(*,*) "Final u0 value:  ",uzero
  write(110,*) "Final u0 value:  ",uzero

  if(i_am_root) then
     do ii=1,3
        write(*,*) Uug_xd(1,1,1,1,1)%cl(ii,:)
     end do
     write(*,*)
     !do ii=1,3
     !   write(*,*) Uug_xd(1,1,1,1,2)%cl(ii,:)
     !end do
     !write(*,*)
  end if
  call WriteGaugeField(trim(outconfig),Uug_xd,0,"replace",file_format=gaugefieldformat)

  call ReadGaugeField(trim(outconfig),Uug_xd2,file_format = gaugefieldformat)

  if(i_am_root) then
     do ii=1,3
        write(*,*) Uug_xd2(1,1,1,1,1)%cl(ii,:)
     end do
     write(*,*)
     !do ii=1,3
     !   write(*,*) Uug_xd2(1,1,1,1,2)%cl(ii,:)
     !end do
  end if

  call WriteGaugeField(trim(outconfig),Uug_xd2,0,"replace",file_format=gaugefieldformat)

  call ReadGaugeField(trim(outconfig),Uug_xd2,file_format = gaugefieldformat)

  if(i_am_root) then
     do ii=1,3
        write(*,*) Uug_xd2(1,1,1,1,1)%cl(ii,:)
     end do
     write(*,*)
     !do ii=1,3
     !   write(*,*) Uug_xd2(1,1,1,1,2)%cl(ii,:)
     !end do
  end if


  write(*,*) uzero
  call Getuzero(Uug_xd2,uzero)
  write(*,*) "Re-read u0 value:  ",uzero
  write(110,*) "Re-read u0 value:  ",uzero

  close(110)
  call DestroyGaugeField(Uug_xd)
  call DestroyGaugeField(Uug_xd2)

  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if

  call FinaliseMPI

end program ApplyGauge
