!! A program to read in configurations and output their values of gauge fixing constant R

program Rvals
  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use ReduceOps
  use MatrixAlgebra
  use gaugefix
  use MCGops
  use gaugeops
  use genmin
  use matrixops
  implicit none

  character(len=512),dimension(:),allocatable :: ConfigNames
  integer :: Nconfigs
  character(len=32) :: gaugefieldformat
  character(len=20) :: startDate,startTime,endDate,endTime
  character(len=512) :: configloc,outputloc
  type(colour_matrix), dimension(:,:,:,:,:), allocatable  :: Uut_xd
  complex(wc),dimension(:),allocatable :: R

  integer :: ic

  call InitialiseMPI
  call InitTimer

  call Put("Gaugefield format?")
  call Get(gaugefieldformat)
  call Put(gaugefieldformat)
  call Put("How many configurations?")
  call Get(Nconfigs)
  call Put(Nconfigs)
  call Put("Names in?")
  call Get(configloc)
  call Put(configloc)
  call Put("Output in?")
  call Get(outputloc)
  call Put(outputloc)

  allocate(ConfigNames(Nconfigs),R(Nconfigs))

  open(unit=44,file=configloc)
  do ic=1,Nconfigs
     read(44,'(a512)') Confignames(ic)
     confignames(ic)=trim(confignames(ic))
  enddo
  

  call Put("Initialising the lattice.")
  call InitialiseLattice(Confignames(1))

  call Put("Starting time")
  call date_and_time(date=startDate, time=startTime)

  call CreateGaugeField(Uut_xd,1)


  do ic=1,Nconfigs
     call ReadGaugeField(trim(ConfigNames(ic)), Uut_xd, file_format = gaugefieldformat)
     call CalcR(Uut_xd,R(ic))
     write(*,*) "Config, R"
     write(*,*) ic,R(ic)
  enddo

  open(unit=45,file=outputloc)
  do ic=1,Nconfigs
     write(45,'(f11.8)') real(R(ic))
  enddo
 
  call DestroyGaugeField(Uut_xd)                                                                                                                                                                                                           

  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if

  call FinaliseMPI


end program Rvals
