#include "defines.h"

module MPIOps
  use Kinds
  use LatticeSize
  use MPIInterface
  use ColourTypes
  ! use Timer
  implicit none

  type(colour_matrix), dimension(:,:,:), allocatable, private :: masked_send, masked_recv

contains

  subroutine ShadowEvenOdd(U_xd, evenodd)
    ! Updates even/odd links on adjacent processors
    ! evenodd == 0 -> even
    ! evenodd == 1 -> odd
    ! Note that this only updates the backwards shadowed links needed for MCG fixing
    ! Hence any results that depend on forward shadowed links will be wrong.
    ! Remedy this by calling the standard cola ShadowGaugeField() routine
    type(colour_matrix), dimension(:,:,:,:,:) :: U_xd
    integer :: evenodd

    integer :: eo
    integer :: ierror, tag, dest_rank, src_rank
    integer :: status(nmpi_status,4)
    integer :: id, ix, iy, iz, it, zcount, nxd,nyd,nzd,ntd! d = deferred shape array
    integer :: sendrecv_reqz(nt,2),sendrecv_reqt(2), sendrecv_reqzs(nts,2), ierr
    integer :: sendrecvz_status(nmpi_status,nt*2), sendrecvt_status(nmpi_status,2), sendrecvzs_status(nmpi_status,nts*2)

    nxd = size(U_xd,1)
    nyd = size(U_xd,2)
    nzd = size(U_xd,3)
    ntd = size(U_xd,4)

    if(.NOT. allocated(masked_send)) then
       allocate(masked_send(nxd, nyd, nzs/2))
    end if

    if(.NOT. allocated(masked_recv)) then
       allocate(masked_recv(nxd, nyd, nzs/2))
    end if

    if ( nproct > 1 ) then

       dest_rank = modulo(mpi_coords(4) - floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)
       src_rank = modulo(mpi_coords(4) + floor(real(nt)/nt), nproct) + nproct*mpi_coords(3)


       ! First send is the even/odd elements of the left-most shadow plane
       ! Destination is the right-most plane of the previous processor

       do ix = 1,nxd
          do iy = 1,nyd
             eo=check_eo([ix,iy,1,1])
             if(eo == evenodd) then
                masked_send(ix, iy, :) = U_xd(ix, iy, 1:nzs-1:2, mapt(0), nd)
             else
                masked_send(ix, iy, :) = U_xd(ix, iy, 2:nzs:2, mapt(0), nd)
             end if
          end do
       end do

       call MPI_ISSend(masked_send(1,1,1), nc*nc*nxd*nyd*nzs/2, mpi_dc, dest_rank, nd, &
            &                                     mpi_comm, sendrecv_reqt(1), ierr)
       call MPI_IRecv (masked_recv(1,1,1), nc*nc*nxd*nyd*nzs/2, mpi_dc,  src_rank, nd, &
            &                                     mpi_comm, sendrecv_reqt(2), ierr)

       call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)

       do ix = 1,nxd
          do iy = 1,nyd
             eo=check_eo([ix,iy,1,nt])
             if(eo == evenodd) then
                U_xd(ix, iy, 2:nzs:2, mapt(nt), nd) = masked_recv(ix, iy, :)
             else
                U_xd(ix, iy, 1:nzs-1:2, mapt(nt), nd) = masked_recv(ix, iy, :)
             end if
          end do
       end do


       dest_rank = modulo(mpi_coords(4) - floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)
       src_rank = modulo(mpi_coords(4) + floor(real(-1)/nt), nproct) + nproct*mpi_coords(3)

       ! Second send is the even/odd right-most stored plane
       ! Destination is the left-most shadowed plane of the next processor

       do ix = 1,nxd
          do iy = 1,nyd
             eo = check_eo([ix,iy,1,nt])
             if(eo == evenodd) then
                masked_send(ix, iy, :) = U_xd(ix, iy, 1:nzs-1:2, mapt(nt), nd)
             else
                masked_send(ix, iy, :) = U_xd(ix, iy, 2:nzs:2, mapt(nt), nd)
             end if
          end do
       end do

       call MPI_ISSend(masked_send(1,1,1), nc*nc*nxd*nyd*nzs/2, mpi_dc, dest_rank, nd, &
            &                                     mpi_comm, sendrecv_reqt(1), ierr)
       call MPI_IRecv (masked_recv(1,1,1), nc*nc*nxd*nyd*nzs/2, mpi_dc,  src_rank, nd, &
            &                                     mpi_comm, sendrecv_reqt(2), ierr)

       call MPI_WaitAll(2,sendrecv_reqt,sendrecvt_status,ierr)

       do ix = 1,nxd
          do iy = 1,nyd
             eo = check_eo([ix,iy,1,1])
             if(eo == evenodd) then
                U_xd(ix, iy, 2:nzs:2, mapt(0), nd) = masked_recv(ix, iy, :)
             else
                U_xd(ix, iy, 1:nzs-1:2, mapt(0), nd) = masked_recv(ix, iy, :)
             end if
          end do
       end do

    end if

  end subroutine ShadowEvenOdd


  integer function check_eo(x)
    ! Check whether x is even or odd with resepct to the full lattice
    ! check_eo=0 => even
    ! check_eo=1 => odd
    integer, dimension(4), intent(in) :: x

    integer, dimension(4) :: x_lat

    call SubLatticeToLattice(x(1),x(2),x(3),x(4),x_lat(1),x_lat(2),x_lat(3),x_lat(4))
    check_eo = mod(sum(x_lat), 2)

  end function check_eo

end module MPIOps
