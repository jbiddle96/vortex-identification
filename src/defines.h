#define mpiprint if (i_am_root) print
#define mpiread if  (i_am_root) read

#define _FILENAME_LEN_ 512
#define _chiral_basis_ 1

#define _alloc_matrix_field(name,rank) \
subroutine name(U_x,nshdw) \n \
type(colour_matrix), rank, allocatable :: U_x \
end subroutine name


 
