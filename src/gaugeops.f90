!!To do: better integrate with cola
#include "defines.h"
module gaugeops
  use GaugeField
  use LatticeSize
  use ColourTypes
  use Kinds
  use ColourFieldOps
  use MatrixAlgebra
  use matrixops
  implicit none
  private

  public :: updatelattice
  public :: CreateGaugeMat
  public :: WriteMCGGaugeTrans
  public :: ReadMCGGaugeTrans
  public :: updatesite


contains

  !! Reads in lattice Ufix_xd and a set of gauge transformations G, applies G to U_xd
  subroutine updatelattice(G,U_xd)
    type(colour_matrix),dimension(:,:,:,:),intent(in) :: G
    type(colour_matrix),dimension(:,:,:,:,:),intent(inout) :: U_xd

    integer :: ix,iy,iz,it,mu
    integer,dimension(4) :: X,Xmu
    type(colour_matrix) :: dummy
    type(colour_matrix) :: Gforward
    type(colour_matrix) :: Ghere

!    write(*,*) "Update Lattice Debug"
!    write(*,*) nlx,nly,nlz,nlt
!    ix=1
!    iy=1
!    iz=1
!    it=1
!    mu=1
    do ix=1,nlx;do iy=1,nly;do iz=1,nlz;do it=1,nlt;do mu=1,4
       Ghere=G(ix,iy,iz,it)
       x(1)=ix
       x(2)=iy
       x(3)=iz
       x(4)=it
       call forwardincrement(x,xmu,mu)
       Gforward=adjoint(G(xmu(1),xmu(2),xmu(3),xmu(4)))
!       write(*,*) x,xmu
!       write(*,*) det(Ghere)
!       write(*,*) det(Gforward)
!       write(*,*) det(U_xd(ix,iy,iz,it,mu))
       call MultiplyMatMat(dummy,U_xd(ix,iy,iz,it,mu),Gforward)
!       write(*,*) det(dummy)
       call MultiplyMatMat(U_xd(ix,iy,iz,it,mu),Ghere,dummy)
!       write(*,*) det(U_xd(ix,iy,iz,it,mu))
!       if(abs(det(U_xd(ix,iy,iz,it,mu))-1) .ge. 1e-8) then
!          write(*,*) "Update Lattice Debug"
!          write(*,*) x,xmu
!          write(*,*) det(Ghere)
!          write(*,*) det(Gforward)
!          write(*,*) det(dummy)
!          write(*,*) det(U_xd(ix,iy,iz,it,mu))
!       endif
    enddo;enddo;enddo;enddo;enddo

  end subroutine updatelattice

  subroutine updatesite(G,U_xd,x)
    !!Applies local gauge transformation G(x) to Uut_xd,stores in Ufix_xd
    type(colour_matrix), intent(in) :: G
    type(colour_matrix), dimension(:,:,:,:,:),intent(inout) :: U_xd
    integer,dimension(4),intent(in) :: x
    
    type(colour_matrix) :: dummy,Gadj
    integer :: mu
    integer,dimension(4) :: xmu
    
    Gadj=adjoint(G)
    do mu=1,nd
       call MultiplyMatMat(dummy,G,U_xd(x(1),x(2),x(3),x(4),mu))
       U_xd(x(1),x(2),x(3),x(4),mu)=dummy
       call backincrement(x,xmu,mu)
       call MultiplyMatMat(dummy,U_xd(xmu(1),xmu(2),xmu(3),xmu(4),mu),Gadj)
       U_xd(xmu(1),xmu(2),xmu(3),xmu(4),mu)=dummy
    enddo
    
  end subroutine updatesite

  subroutine CreateGaugeMat(Gcomp,G,subg)
    !!Elements of SU(2) can be written g=g_{4}I - ig_{i}\sigma_{i}, given g_{i} this subroutine finds g and embeds in the subg SU(2) subgroup of SU(3) matrix G
    real(dp), dimension(4), intent(in) :: Gcomp
    integer,intent(in) :: subg
    type(colour_matrix), intent(out) :: G
    
    complex(wc), dimension(2,2) :: subgroup
    complex(wc) :: detcheck
    integer :: ic,jc
    
    !!Find SU(2) matrix
    subgroup(1,1) = Gcomp(4) - i*Gcomp(3)
    subgroup(1,2) = -i*Gcomp(1) - Gcomp(2)
    subgroup(2,1) = -i*Gcomp(1) + Gcomp(2)
    subgroup(2,2) = Gcomp(4) + i*Gcomp(3)
    
    !!Embed in SU(3)
    if (subg == 1) then
       do ic=1,2
          do jc=1,2
             G%cl(ic,jc) = subgroup(ic,jc)
          enddo
       enddo
       
       G%cl(1,3) = 0
       G%cl(2,3) = 0
       G%cl(3,3) = 1.0
       G%cl(3,1) = 0
       G%cl(3,2) = 0
       
    elseif(subg == 2) then
       do ic=1,2
          do jc=1,2
             G%cl(ic+1,jc+1) = subgroup(ic,jc)
          enddo
       enddo
       G%cl(1,1) = 1.0
       G%cl(1,2) = 0
       G%cl(1,3) = 0
       G%cl(2,1) = 0
       G%cl(3,1) = 0
       
    elseif (subg == 3) then
       G%cl(1,1) = subgroup(1,1)
       G%cl(1,2) = 0
       G%cl(1,3) = subgroup(1,2)
       G%cl(2,1) = 0
       G%cl(2,2) = 1.0
       G%cl(2,3) = 0
       G%cl(3,1) = subgroup(2,1)
       G%cl(3,2) = 0
       G%cl(3,3) = subgroup(2,2)
       
    else
       write(*,*) "Invalid Subgroup"
    endif
    
  end subroutine CreateGaugeMat

  subroutine WriteMCGGaugeTrans(gaugeloc,G)
    character(len=256),intent(in) :: gaugeloc
    type(colour_matrix), dimension(:,:,:,:),intent(in) :: G

    open(53,file=gaugeloc)
    write(53,*) G
    close(53)

  end subroutine WriteMCGGaugeTrans


  subroutine ReadMCGGaugeTrans(gaugeloc,G)
    character(len=256),intent(in) :: gaugeloc
    type(colour_matrix),dimension(:,:,:,:),intent(out) :: G

    open(53,file=gaugeloc)
    read(54,*) G
    close(54)

  end subroutine ReadMCGGaugeTrans





end module gaugeops
