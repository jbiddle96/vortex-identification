
#define _fermion_field_type_4x_ type(colour_vector), dimension(:,:,:,:,:)
#define _fermion_field_type_eo_ type(dirac_fermion), dimension(:)
#define _fermion_field_extents_4x_ nxp,nyp,nzp,ntp,ns
#define _fermion_field_extents_eo_ n_xpeo
#define _fermion_eigenspace_type_4x_ type(colour_vector), dimension(:,:,:,:,:,:)
#define _fermion_eigenspace_type_eo_ type(dirac_fermion), dimension(:,:)

#ifdef _even_odd_
#define _fermion_field_type_ _fermion_field_type_eo_
#define _fermion_field_extents_ n_xpeo
#define _fermion_colons_ :
#define _zero_fermion_vector_ zero_dirac_fermion
#define _fermion_eigenspace_type_ _fermion_eigenspace_type_eo_
#define _fermion_eigenspace_extents_(n_ev) n_xpeo,n_ev
#else
#define _fermion_field_type_ _fermion_field_type_4x_
#define _fermion_field_extents_ nxp,nyp,nzp,ntp,ns
#define _fermion_colons_ :,:,:,:,:
#define _zero_fermion_vector_ zero_vector
#define _fermion_eigenspace_type_ _fermion_eigenspace_type_4x_
#define _fermion_eigenspace_extents_(n_ev) nxp,nyp,nzp,ntp,ns,n_ev
#endif


#ifdef _split_links_

#define _links_ U_xd, Usm_xd
#define _mfi_params_ u0_bar, u0fl_bar

#ifdef _clover_
#define _U_clover_ Usm_xd
#endif

#else

#ifdef _smeared_links_

#define _links_ Usm_xd
#define _mfi_params_ u0fl_bar

#ifdef _clover_
#define _U_clover_ Usm_xd
#endif

#else

#define _links_ U_xd
#define _mfi_params_ u0_bar

#ifdef _clover_
#define _U_clover_ U_xd
#endif

#endif

#endif /* _split_links_ */
