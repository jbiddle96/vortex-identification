#include "defines.h"
module matrixops
  use Kinds
  use Colourtypes
  use latticesize

  public :: adjoint
  public :: backincrement
  public :: forwardincrement
  public :: det
  public :: configdet

contains

    function adjoint(G)
      !!Self-explanatory
      type(colour_matrix) :: G
      type(colour_matrix) :: adjoint

      integer :: ic,jc

      do ic=1,3
         do jc=1,3
            adjoint%cl(jc,ic) = CONJG(G%cl(ic,jc))
         enddo
      enddo

    end function adjoint

    function det(U)
      !!Self-explanatory
      type(colour_matrix) :: U
      complex(wc) :: det

      det = U%cl(1,1)*(U%cl(2,2)*U%cl(3,3)-U%cl(2,3)*U%cl(3,2))  &
           - U%cl(1,2)*(U%cl(2,1)*U%cl(3,3)-U%cl(2,3)*U%cl(3,1)) &
           + U%cl(1,3)*(U%cl(2,1)*U%cl(3,2)-U%cl(2,2)*U%cl(3,1))

    end function det
    
    subroutine configdet(U_xd,nlx,nly,nlz,nlt)
      type(colour_matrix),dimension(:,:,:,:,:),intent(in) :: U_xd
      integer,intent(in) :: nlx,nly,nlz,nlt
      real(dp),dimension(:,:,:,:,:),allocatable :: detU_xd
      real(dp) :: averr,maxerr
      integer :: ix,iy,iz,it,imu

      averr=0
      allocate(detU_xd(nlx,nly,nlz,nlt,nd))
      do ix=1,nlx;do iy=1,nly;do iz=1,nlz;do it=1,nlt; do imu=1,nd
         detU_xd(ix,iy,iz,it,imu) = abs(det(U_xd(ix,iy,iz,it,imu)))
         averr=averr+detU_xd(ix,iy,iz,it,imu)
      enddo;enddo;enddo;enddo;enddo

      averr = averr/(nlx*nly*nlz*nlt*4) - 1
      detU_xd(:,:,:,:,:) = abs(detU_xd(:,:,:,:,:) - 1)
      maxerr = maxval(detU_xd)
      write(*,*) "Average error"
      write(*,*) averr
      write(*,*) "Max error"
      write(*,*) maxerr

    end subroutine configdet

    subroutine backincrement(x,xmu,mu)
      !!Finds the lattice site xmu corresponding to a step in the direction of negative mu
      integer,dimension(4),intent(in) :: x
      integer,dimension(4),intent(out) :: xmu
      integer,intent(in) :: mu

      xmu = x
      xmu(mu) = x(mu) - 1
      xmu(1) = mapx(xmu(1))
      xmu(2) = mapy(xmu(2))
      xmu(3) = mapz(xmu(3))
      xmu(4) = mapt(xmu(4))
      ! if (x(mu)-1 == 0) then
      !    if(mu == 4) then
      !       xmu(mu)=nlt
      !    else
      !       xmu(mu)=nlx
      !    endif
      ! else
      !    xmu(mu)=x(mu)-1
      ! endif

    end subroutine backincrement

    subroutine forwardincrement(x,xmu,mu)
      !!Finds the lattice site xmu corresponding to a step in the direction of positive mu                                                                                                                                                   
      integer,dimension(4),intent(in) :: x
      integer,dimension(4),intent(out) :: xmu
      integer,intent(in) :: mu

      xmu = x
      xmu(mu) = x(mu) + 1
      xmu(1) = mapx(xmu(1))
      xmu(2) = mapy(xmu(2))
      xmu(3) = mapz(xmu(3))
      xmu(4) = mapt(xmu(4))
      
      ! if(mu == 4 .and. x(mu)+1 == nlt+1) then
      !    xmu(mu) = 1
      ! else if (x(mu)+1 == nlx+1) then
      !    xmu(mu) = 1
      ! else
      !    xmu(mu)=x(mu)+1
      ! endif

    end subroutine forwardincrement


end module matrixops
