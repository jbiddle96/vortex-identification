#include "defines.h"
module MCGops
  use GaugeField
  use GaugeFieldIO
  use LatticeSize
  use ColourTypes
  use Kinds
  use Strings
  use TextIO
  use ColourFieldOps
  use Timer
  use MPIInterface
  use ReduceOps
  use MatrixAlgebra
  use gaugefix
  use MatrixOps
  implicit none
  private

  public :: Rterms
  public :: CalcR

  interface CalcR
    module procedure CalcRlocalexpl
    module procedure CalcRlocalpoly
    module procedure CalcRconfig
  end interface CalcR

contains
  subroutine Rterms(Uut_xd,x,A,B,C,subg)
    !! For SU(2) subgroup subg, find the polynomial corresponding to the local gauge-fixing condition
    !! Polynomial coefficients defined by A,B,C
    !! Note naming convention U(x)=U,U(x-mu)=V
    type(colour_matrix), dimension(:,:,:,:,:), intent(in)  :: Uut_xd
    integer,dimension(4),intent(in) :: x
    integer, intent(in) :: subg
    complex(wc),dimension(4,4),intent(out) :: A
    complex(wc),dimension(4),intent(out) :: B
    complex(wc),intent(out) :: C

    complex(wc), dimension(nc-1,nc-1) :: Umat,Vmat
    complex(wc) :: Uscal,Vscal
    integer :: mu,ic,jc

    complex(wc),dimension(4,4) :: Ap
    complex(wc),dimension(4) :: Bp
    complex(wc) :: Cp
    complex(wc),dimension(nc,nc) :: U,V

    A(:,:)=(0,0)
    B(:)=(0,0)
    C=(0,0)
    do mu=1,4
      !! Define Uut_xd(x,mu) = U, Uut_xd(x-mu,mu)=V
      call defineUV(Uut_xd,x,mu,U,V)

      !! Define Umat to be components of U corresponding to positions of SU(2) subgroup components, Uscal to be component of U corresponding to diagonal not in SU(2) subgroup
      call definesubelements(U,Umat,Uscal,subg)
      call definesubelements(V,Vmat,Vscal,subg)

      !! Find the coefficients of the quadratic for direction mu
      call definequadterms(Umat,Uscal,Vmat,Vscal,Ap,Bp,Cp)

      !! Sum over directions
      do ic=1,4
        do jc=1,4
          A(ic,jc)=A(ic,jc)+Ap(ic,jc)
        enddo
        B(ic)=B(ic)+Bp(ic)
      enddo
      C=C+Cp

    enddo

  end subroutine Rterms

  subroutine CalcRlocalpoly(Gcomp,A,B,C,R)
    !!Calculate the local gauge-fixing condition R at site x given polynomial form
    real(dp), dimension(4), intent(in) :: Gcomp
    complex(wc),intent(in) :: C
    complex(wc),dimension(4),intent(in) :: B
    complex(wc),dimension(4,4),intent(in) :: A

    complex(wc), intent(out) :: R

    integer :: ic,jc

    R = C
    do ic=1,4
      R = R + Gcomp(ic)*B(ic)
    enddo

    do ic=1,4
      do jc=1,4
        R = R + Gcomp(ic)*Gcomp(jc)*A(ic,jc)
      enddo
    enddo


  end subroutine CalcRlocalpoly

  subroutine CalcRlocalexpl(G,Uut_xd,x,R)
    !! Calculate local gauge-fixing condition R at site x
    type(colour_matrix), intent(in) :: G
    type(colour_matrix), dimension(:,:,:,:,:),intent(in) :: Uut_xd
    integer,dimension(4),intent(in) :: x

    complex(wc), intent(out) :: R

    complex(wc) :: Tracel,Tracer
    type(colour_matrix) :: Gadj
    integer :: ic,jc,mu
    complex(wc),dimension(nc,nc):: U,V

    R=0
    do mu=1,4
      call defineUV(Uut_xd,x,mu,U,V)
      do ic=1,3
        do jc=1,3
          Gadj%cl(jc,ic) = CONJG(G%cl(ic,jc))
        enddo
      enddo
      Tracel = G%cl(1,1)*U(1,1)+G%cl(1,2)*U(2,1)+G%cl(1,3)*U(3,1)+G%cl(2,1)*U(1,2)+G%cl(2,2)*U(2,2)+G%cl(2,3)*U(3,2)+G%cl(3,1)*U(1,3)+G%cl(3,2)*U(2,3)+G%cl(3,3)*U(3,3)
      Tracer = V(1,1)*Gadj%cl(1,1)+V(1,2)*Gadj%cl(2,1)+V(1,3)*Gadj%cl(3,1)+V(2,1)*Gadj%cl(1,2)+V(2,2)*Gadj%cl(2,2)+V(2,3)*Gadj%cl(3,2)+V(3,1)*Gadj%cl(1,3)+V(3,2)*Gadj%cl(2,3)+V(3,3)*Gadj%cl(3,3)
      R = R + ABS(Tracel)**2 + ABS(Tracer)**2
    enddo
  end subroutine CalcRlocalexpl

  subroutine CalcRconfig(U_xd,R)
    !!Calculate the gauge-fixing condition R across the lattice
    type(colour_matrix), dimension(:,:,:,:,:),intent(in) :: U_xd
    complex(wc), intent(out) :: R

    integer :: ix,iy,iz,it,mu
    complex(wc) :: Rdummy, R_local
    integer,dimension(4) :: x
    integer :: mpierror

    R_local=0
    do ix=1,nx; do iy=1,ny; do iz=1,nz; do it=1,nt;do mu=1,4
      Rdummy=U_xd(ix,iy,iz,it,mu)%cl(1,1) &
          + U_xd(ix,iy,iz,it,mu)%cl(2,2) &
          + U_xd(ix,iy,iz,it,mu)%cl(3,3)
      Rdummy= ABS(Rdummy)**2
      R_local=R_local+Rdummy
    enddo;enddo;enddo;enddo;enddo
    !! Normalisation factor 1/(Nsite*Ndim*N**2)
    R_local=R_local/(nlx*nly*nlz*nlt*36)

    ! call MPI_Allreduce(R_local, R, 1, mpi_dc, MPI_SUM, mpi_comm, mpierror)
    call AllSum(R_local, R)
  end subroutine CalcRconfig

  subroutine defineUV(Uut_xd,x,mu,U,V)
    type(colour_matrix),dimension(:,:,:,:,:),intent(in) :: Uut_xd
    integer,dimension(4),intent(in) :: x
    integer, intent(in) :: mu
    complex(wc),dimension(nc,nc),intent(out) :: U,V

    integer,dimension(4) :: xmu


    U=Uut_xd(x(1),x(2),x(3),x(4),mu)%cl
    call backincrement(x,xmu,mu)
    V=Uut_xd(xmu(1),xmu(2),xmu(3),xmu(4),mu)%cl


  end subroutine defineUV

  subroutine definesubelements(U,Umat,Uscal,subg)
    complex(wc), dimension(nc,nc), intent(in) :: U
    integer, intent(in) :: subg
    complex(wc), dimension(nc-1,nc-1),intent(out) :: Umat
    complex(wc), intent(out) :: Uscal

    integer :: ic,jc


    if (subg == 1) then
      do ic=1,2
        do jc=1,2
          Umat(ic,jc) = U(ic,jc)
        enddo
      enddo

      Uscal = U(3,3)

    elseif (subg == 2) then
      do ic=1,2
        do jc=1,2
          Umat(ic,jc) = U(ic+1,jc+1)
        enddo
      enddo
      Uscal = U(1,1)
    elseif (subg == 3) then
      Umat(1,1) = U(1,1)
      Umat(1,2) = U(1,3)
      Umat(2,1) = U(3,1)
      Umat(2,2) = U(3,3)
      Uscal = U(2,2)
    else
      write(*,*) "Invalid Subgroup"
    endif
  end subroutine definesubelements

  subroutine definequadterms(Umat,Uscal,Vmat,Vscal,A,B,C)
    complex(wc),dimension(nc-1,nc-1), intent(in) :: Umat,Vmat
    complex(wc),intent(in) :: Uscal,Vscal

    complex(wc),dimension(4,4), intent(out) :: A
    complex(wc),dimension(4), intent(out) :: B
    complex(wc),intent(out) :: C

    call findscalar(Uscal,Vscal,C)
    call findlinear(Umat,Uscal,Vmat,Vscal,B)
    call findquadratic(Umat,Vmat,A)

  end subroutine definequadterms


  !!Below here be dragons
  !!Derivation of any/all of these co-efficients is left as an exercise for the reader

  subroutine findscalar(Uscal,Vscal,C)
    complex(wc), intent(in) :: Uscal,Vscal
    complex(wc), intent(out) :: C

    C = ABS(Uscal)**2 + ABS(Vscal)**2

  end subroutine findscalar

  subroutine findlinear(Umat,Uscal,Vmat,Vscal,B)
    complex(wc), dimension(nc-1,nc-1), intent(in) :: Umat,Vmat
    complex(wc), intent(in) :: Uscal,Vscal
    complex(wc), dimension(4),intent(out) :: B

    complex(wc) :: im

    im = (0.0,1.0)

    !Contributions from U_mu(x)
    B(1) = -im*Umat(2,1)*CONJG(Uscal) - im*Umat(1,2)*Conjg(Uscal) + im*CONJG(Umat(2,1))*Uscal + im*CONJG(Umat(1,2))*Uscal
    B(2) = Umat(1,2)*CONJG(Uscal) - Umat(2,1)*CONJG(Uscal) + CONJG(Umat(1,2))*Uscal - CONJG(Umat(2,1))*Uscal
    B(3) = -im*Umat(1,1)*CONJG(Uscal) + im*Umat(2,2)*CONJG(Uscal) + im*CONJG(Umat(1,1))*Uscal - im*CONJG(Umat(2,2))*Uscal
    B(4) = Umat(1,1)*CONJG(Uscal) + Umat(2,2)*CONJG(Uscal) + CONJG(Umat(1,1))*Uscal + CONJG(Umat(2,2))*Uscal

    !Contributions from U_mu(x-mu)

    B(1) = B(1) + i*Vmat(1,2)*CONJG(Vscal) + i*Vmat(2,1)*CONJG(Vscal) - i*CONJG(Vmat(1,2))*Vscal - i*CONJG(Vmat(2,1))*Vscal
    B(2) = B(2) + Vmat(2,1)*CONJG(Vscal) - Vmat(1,2)*CONJG(Vscal) + CONJG(Vmat(2,1))*Vscal - CONJG(Vmat(1,2))*Vscal
    B(3) = B(3) + i*Vmat(1,1)*CONJG(Vscal) - i*Vmat(2,2)*CONJG(Vscal) - i*CONJG(Vmat(1,1))*Vscal + i*CONJG(Vmat(2,2))*Vscal
    B(4) = B(4) + Vmat(1,1)*CONJG(Vscal) + Vmat(2,2)*CONJG(Vscal) + CONJG(Vmat(1,1))*Vscal + CONJG(Vmat(2,2))*Vscal



  end subroutine findlinear

  subroutine findquadratic(Umat,Vmat,A)
    complex(wc),dimension(nc-1,nc-1),intent(in) :: Umat,Vmat

    complex(wc),dimension(4,4) :: A
    integer :: ic,jc

    !Contributions from U_mu(x)
    A(1,1) = ABS(Umat(2,1))**2 + ABS(Umat(1,2))**2 + Umat(2,1)*CONJG(Umat(1,2)) + Umat(1,2)*CONJG(Umat(2,1))
    A(1,2) = -2*i*Umat(2,1)*CONJG(Umat(1,2)) + 2*i*Umat(1,2)*CONJG(Umat(2,1))
    A(2,2) = ABS(Umat(2,1))**2 - Umat(2,1)*CONJG(Umat(1,2)) - CONJG(Umat(2,1))*Umat(1,2) + ABS(Umat(1,2))**2
    A(1,3) = Umat(2,1)*CONJG(Umat(1,1)) - Umat(2,1)*CONJG(Umat(2,2)) + Umat(1,2)*CONJG(Umat(1,1)) - Umat(1,2)*CONJG(Umat(2,2)) + Umat(1,1)*CONJG(Umat(2,1)) + Umat(1,1)*CONJG(Umat(1,2)) - Umat(2,2)*CONJG(Umat(2,1)) - Umat(2,2)*CONJG(Umat(1,2))
    A(2,3) = i*Umat(1,1)*CONJG(Umat(2,1)) - i*Umat(1,1)*CONJG(Umat(1,2)) - i*Umat(2,2)*CONJG(Umat(2,1)) + i*Umat(2,2)*CONJG(Umat(1,2)) - i*Umat(2,1)*CONJG(Umat(1,1)) + i*Umat(2,1)*CONJG(Umat(2,2)) + i*Umat(1,2)*CONJG(Umat(1,1)) -i*Umat(1,2)*CONJG(Umat(2,2))
    A(3,3) = ABS(Umat(1,1))**2 + ABS(Umat(2,2))**2 - Umat(1,1)*CONJG(Umat(2,2)) - Umat(2,2)*CONJG(Umat(1,1))
    A(1,4) = -i*Umat(2,1)*CONJG(Umat(1,1)) - i*Umat(2,1)*CONJG(Umat(2,2)) - i*Umat(1,2)*CONJG(Umat(1,1)) - i*Umat(1,2)*CONJG(Umat(2,2)) + i*Umat(1,1)*CONJG(Umat(2,1)) + i*Umat(1,1)*CONJG(Umat(1,2)) + i*Umat(2,2)*CONJG(Umat(2,1)) + i*Umat(2,2)*CONJG(Umat(1,2))
    A(2,4) = -Umat(1,1)*CONJG(Umat(2,1)) + Umat(1,1)*CONJG(Umat(1,2)) - Umat(2,2)*CONJG(Umat(2,1)) + Umat(2,2)*CONJG(Umat(1,2)) - Umat(2,1)*CONJG(Umat(1,1)) - Umat(2,1)*CONJG(Umat(2,2)) + Umat(1,2)*CONJG(Umat(1,1)) + Umat(1,2)*CONJG(Umat(2,2))
    A(3,4) = -2*i*Umat(1,1)*CONJG(Umat(2,2)) + 2*i*Umat(2,2)*CONJG(Umat(1,1))
    A(4,4) = ABS(Umat(1,1))**2 + ABS(Umat(2,2))**2 + Umat(1,1)*CONJG(Umat(2,2)) + Umat(2,2)*CONJG(Umat(1,1))

    !Contributions from U_mu(x-mu)
    A(1,1) = A(1,1) + ABS(Vmat(1,2))**2 + ABS(Vmat(2,1))**2 + Vmat(1,2)*CONJG(Vmat(2,1)) + Vmat(2,1)*CONJG(Vmat(1,2))
    A(1,2) = A(1,2) + (-Vmat(1,2) + Vmat(2,1))*CONJG(i*Vmat(1,2) + i*Vmat(2,1)) + CONJG(-Vmat(1,2) + Vmat(2,1))*(i*Vmat(1,2) + i*Vmat(2,1))
    A(1,3) = A(1,3) + Vmat(1,2)*CONJG(Vmat(1,1)) - Vmat(1,2)*CONJG(Vmat(2,2)) + Vmat(2,1)*CONJG(Vmat(1,1)) - Vmat(2,1)*CONJG(Vmat(2,2)) + Vmat(1,1)*CONJG(Vmat(1,2)) + Vmat(1,1)*CONJG(Vmat(2,1)) - Vmat(2,2)*CONJG(Vmat(1,2)) - Vmat(2,2)*CONJG(Vmat(2,1))
    A(1,4) = A(1,4) + i*Vmat(1,2)*CONJG(Vmat(1,1)) + i*Vmat(1,2)*CONJG(Vmat(2,2)) + i*Vmat(2,1)*CONJG(Vmat(1,1)) + i*Vmat(2,1)*CONJG(Vmat(2,2)) - i*Vmat(1,1)*CONJG(Vmat(1,2)) - i*Vmat(1,1)*CONJG(Vmat(2,1)) - i*Vmat(2,2)*CONJG(Vmat(1,2)) -i*Vmat(2,2)*CONJG(Vmat(2,1))
    A(2,2) = A(2,2) + ABS(Vmat(2,1))**2 - Vmat(2,1)*CONJG(Vmat(1,2)) - Vmat(1,2)*CONJG(Vmat(2,1)) + ABS(Vmat(1,2))**2
    A(2,3) = A(2,3) + (i*Vmat(1,1) - i*Vmat(2,2))*CONJG(-Vmat(1,2) + Vmat(2,1)) + CONJG(i*Vmat(1,1) - i*Vmat(2,2))*(-Vmat(1,2)+Vmat(2,1))
    A(2,4) = A(2,4) - Vmat(1,1)*CONJG(Vmat(1,2)) + Vmat(1,1)*CONJG(Vmat(2,1)) - Vmat(2,2)*CONJG(Vmat(1,2)) + Vmat(2,2)*CONJG(Vmat(2,1)) - Vmat(1,2)*CONJG(Vmat(1,1)) - Vmat(1,2)*CONJG(Vmat(2,2)) + Vmat(2,1)*CONJG(Vmat(1,1)) + Vmat(2,1)*CONJG(Vmat(2,2))
    A(3,3) = A(3,3) + ABS(Vmat(1,1))**2 + ABS(Vmat(2,2))**2 - Vmat(1,1)*CONJG(Vmat(2,2)) - Vmat(2,2)*CONJG(Vmat(1,1))
    A(3,4) = A(3,4) + 2*i*Vmat(1,1)*CONJG(Vmat(2,2)) - 2*i*Vmat(2,2)*CONJG(Vmat(1,1))
    A(4,4) = A(4,4) + ABS(Vmat(1,1))**2 + ABS(Vmat(2,2))**2 + Vmat(1,1)*CONJG(Vmat(2,2)) + Vmat(2,2)*CONJG(Vmat(1,1))

    do ic=1,4
      do jc=ic+1,4
        A(ic,jc)=0.5*A(ic,jc)
        A(jc,ic)=A(ic,jc)
      enddo
    enddo

  end subroutine findquadratic


end module MCGops
