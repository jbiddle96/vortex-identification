#include "defines.h"
module genmin
  use Kinds
  use MCGops
  use gaugeops
  use lapack95
  implicit none
  private


  public :: MaxR


contains

  !!A maximisation routine following Gander, Golub & von Matt, Linear Algebra and its Applications 114/115:815-839(1989)
  subroutine MaxR(A,B,C,Gcomp)
    implicit none
    complex(wc),dimension(4,4),intent(in) :: A
    complex(wc),dimension(4),intent(in) :: B
    complex(wc),intent(in) :: C    
    real(dp),dimension(4),intent(out) :: Gcomp

    real(dp),dimension(4,4) :: Adash
    real(dp),dimension(4) :: Bdash
    real(dp),dimension(4) :: d
    real(dp),dimension(4,4) :: Q,Qt
    real(dp),dimension(4) :: evals
    real(dp) :: lambda !! lagrangian multiplier

    !! Take negatives to turn that minimisation frown upside down
    Bdash(:) = real((0.5_dp,0)*B(:))
    Adash(:,:) = real((-1_dp,0)*A(:,:))

    !! Find the eigenvalue decomposition of A
    call diagonalise(Adash,evals,Q,Qt)
    !! define d=Qt*B
    call getd(Qt,Bdash,d)
    !! Find the 0 of the secular equation, corresponding to the minimal value of lambda
    call solvesec(evals,d,lambda)                                                                                                                                                                                             
    !! Using this value of lagrangian multiplier, find the minimal location
    call getGcomp(lambda,Q,Qt,evals,Bdash,Gcomp)
  end subroutine MaxR

  subroutine diagonalise(A,evals,Q,Qt)
    !!finds eigenvalues and eigenvectors of A, stores eigenvalues in evals and eigenvectors in Q and Qt
    implicit none
    real(dp),dimension(4,4),intent(inout) :: A
    real(dp),dimension(4,4),intent(out) :: Q,Qt
    real(dp),dimension(4),intent(out) :: evals

    real(dp) :: vl,vu,il,iu
    real(dp) :: tol
    integer :: lwork,liwork,info,numev
    real(dp),dimension(104) :: work
    integer,dimension(40) :: iwork
    integer,dimension(8) :: isuppz
    integer :: ie,ij



    tol = 1e-15
    lwork=104
    liwork=40
    vl=0
    vu=0
    il=0
    iu=i
    !      call mkl_set_num_threads(1)
    call dsyevr('V', 'A', 'U', 4, A, 4, vl, vu, il, iu, tol, numev, evals, Q, 4, isuppz, work, lwork, iwork, liwork, info)
    ! call mkl_free_buffers()
    do ie=1,4;do ij=1,4
       Qt(ij,ie)=Q(ie,ij)
    enddo;enddo
  end subroutine diagonalise

  subroutine getd(Qt,Bdash,d)
    real(dp),dimension(4),intent(in) :: Bdash
    real(dp),dimension(4,4),intent(in) :: Qt
    real(dp),dimension(4),intent(out) :: d

    integer :: ii,ij
    d(:)=0
    do ii=1,4
       do ij=1,4
          d(ii)=d(ii)+Qt(ii,ij)*Bdash(ij)
       enddo
    enddo

  end subroutine getd

  !! Find smallest 0 of the secular equation using a bisection method
  !! Justified by remarkably nice behaviour of the secular equation in region less than lowest e-val
  !! You may mock its primitiveness, but do not mock its mighty speed
  subroutine solvesec(evals,d,lambda)
    real(dp),dimension(4),intent(in) :: evals,d
    real(dp),intent(out) :: lambda

    integer ::  ia,maxit,itcount

    real(dp) :: fx,leftb,rightb

    maxit=50
    !! maxit=50 since log(2^(-50)) ~ -15

    leftb = evals(1) - abs(evals(1))
    rightb = evals(1)
    do ia=1,maxit
       lambda=(rightb+leftb)/2
       fx=calcsec(evals,d,lambda)
       if (fx .le. 0) then
          leftb = lambda
       else
          rightb = lambda
       endif
    enddo
  end subroutine solvesec


  !! Find smallest 0 of the secular equation using a Newton-Raphson method
  !! Slower than bisection method, not in use
  subroutine NRsolvesec(evals,d,lambda)
    real(dp),dimension(4),intent(in) :: evals,d
    real(dp),intent(inout) :: lambda
    real(dp) :: lambdaprev,calcsechere
    integer :: ia,maxit

    maxit=100
    do ia=1,maxit
       lambdaprev=lambda
       calcsechere=calcsec(evals,d,lambdaprev)
       lambda=lambdaprev - calcsechere/calcsecd(evals,d,lambdaprev)
       if (calcsechere .le. 1e-10) then
          exit
       endif
       lambda=lambdaprev - calcsechere/calcsecd(evals,d,lambdaprev)
    enddo


  end subroutine NRsolvesec

  function calcsec(evals,d,lambda)
    !! calculates the value of the secular equation at point lambda
    real(dp),dimension(4) :: evals,d
    real(dp) :: lambda
    real(dp) :: calcsec

    integer :: ia

    calcsec = 0
    do ia=1,4
       calcsec=calcsec+(d(ia)/(evals(ia)-lambda))**2
    enddo
    calcsec = calcsec - 1_dp

  end function calcsec

  function calcsecd(evals,d,lambda)
    !!Calculates the derivative of the secular equation at point lambda
    real(dp),dimension(4) :: evals,d
    real(dp) :: lambda
    real(dp) :: calcsecd

    integer :: ia

    calcsecd=0
    do ia=1,4
       calcsecd=calcsecd+2*d(ia)**2/((evals(ia)-lambda)**3)
    enddo

  end function calcsecd

  subroutine getGcomp(lambda,Q,Qt,evals,B,Gcomp)
    !!Given minimal value of lagrangian multiplier lambda,finds corresponding solution Gcomp
    !!Z=Qt[D-lambdaI]Qb  
    real(dp),dimension(4),intent(in) :: B
    real(dp),dimension(4,4),intent(in) :: Q,Qt
    real(dp),dimension(4),intent(in) :: evals
    real(dp),intent(in) :: lambda 
    real(dp),dimension(4),intent(out) :: Gcomp

    integer :: ia,ib,ic

    Gcomp(:)=0
    do ia=1,4; do ib=1,4; do ic=1,4
       Gcomp(ia)=Gcomp(ia)+Q(ia,ib)*(1/(evals(ib)-lambda))*Qt(ib,ic)*B(ic)
    enddo;enddo;enddo


  end subroutine getGcomp


end module Genmin
