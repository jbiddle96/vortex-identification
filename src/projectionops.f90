#include "defines.h"
module projectionops
  use GaugeField
  use LatticeSize
  use ColourTypes
  use Kinds
  use ColourFieldOps
  use MatrixAlgebra
  implicit none
  private


  public :: traces
  public :: Project
  public :: VortexRemove
  public :: BinCentres
  
contains

  !! Read in a configuration, output the traces of each link
  subroutine traces(Uut_xd,TrU_xd)
    type(colour_matrix), dimension(:,:,:,:,:), intent(in)  :: Uut_xd
    complex(wc),dimension(:,:,:,:,:),intent(out) :: TrU_xd

    integer :: ix,iy,iz,it,imu

    do imu=1,4; do it=1,nlt; do iz=1,nlz; do iy=1,nly; do ix=1,nlx
       TrU_xd(ix,iy,iz,it,imu)=(Uut_xd(ix,iy,iz,it,imu)%cl(1,1)+Uut_xd(ix,iy,iz,it,imu)%cl(2,2)+Uut_xd(ix,iy,iz,it,imu)%cl(3,3))
    enddo;enddo;enddo;enddo;enddo
    

  end subroutine traces

  subroutine BinCentres(TrU_xd,bins,binmin,binmax,binsize)
    complex(wc),dimension(:,:,:,:,:),intent(in) :: TrU_xd
    real(dp),dimension(:,:),intent(out) :: bins
    real(dp),intent(in) :: binmin,binmax
    integer,intent(in) :: binsize

    integer :: i,ix,iy,iz,it,imu
    integer :: thisbin
    real(dp) :: phase
    real(dp) :: Z
    real(dp),dimension(binsize) :: dists
    
    bins(:,2) = 0
!    write(*,*) "Bin Centres"
!    write(*,*) "Bins"
    do i=1,binsize
       bins(i,1) = binmin + (i-1)*(binmax-binmin)/(binsize)
!       write(*,*) bins(i,1)
    enddo
    do imu=1,4;do it=1,nlt; do iz=1,nlz; do iy=1,nly; do ix=1,nlx
!    imu=1
!    ix=1
!    iy=1
!    iz=1
!    it=1
       phase=atan2(aimag(TrU_xd(ix,iy,iz,it,imu)),real(TrU_xd(ix,iy,iz,it,imu)))
       phase=3*phase/(2*pi)
!       write(*,*) "Phase"
!       write(*,*) phase
!       write(*,*) "Distances"
       do i=1,binsize
          dists(i)=abs(phase-bins(i,1))
!          write(*,*) dists(i)
       enddo
!       write(*,*) "result"
       thisbin=minloc(dists,1)
!       write(*,*) thisbin
!       write(*,*) bins(thisbin,2)
       bins(thisbin,2)=bins(thisbin,2)+1
!       write(*,*) bins(thisbin,2)
    enddo;enddo;enddo;enddo;enddo
    
  end subroutine BinCentres


  subroutine Project(TrU_xd,Uvo_xd)
    complex(wc),dimension(:,:,:,:,:),intent(in) :: TrU_xd
    type(colour_matrix), dimension(:,:,:,:,:), intent(out)  :: Uvo_xd

    integer :: ix,iy,iz,it,imu,id
    integer :: counter
    real(dp) :: phase
    real(dp) :: distance
    real(dp) :: maxdistance
    complex(wc) :: Z

    counter = 0
    maxdistance=0
    do imu=1,4;do it=1,nlt; do iz=1,nlz; do iy=1,nly; do ix=1,nlx
       phase=atan2(aimag(TrU_xd(ix,iy,iz,it,imu)),real(TrU_xd(ix,iy,iz,it,imu)))
       phase=3*phase/(2*pi)
       if(abs(phase) .le. 0.5) then
          Z=(0,0)
       else if(phase .ge. 0) then
          Z=(1,0)
       else 
          Z=(-1,0)
       endif
       !    distance = abs(real(Z)-phase)
       !    if (distance .ge. 0.1) then
       !       counter = counter + 1
       !    endif
       !    if (distance .ge. maxdistance) then
!       maxdistance=distance
       !    endif
       Uvo_xd(ix,iy,iz,it,imu)%cl=0.0_dp
       do id=1,3
          Uvo_xd(ix,iy,iz,it,imu)%cl(id,id)=EXP(i*2_dp*pi/3_dp*Z)
       enddo
       !    write(*,*) EXP(i*2_dp*pi/3_dp*Z)
       !    write(*,*) "Centre Projection"
       !    write(*,*) distance
    enddo;enddo;enddo;enddo;enddo
    !    write(*,*) counter
    !    write(*,*) maxdistance
           
  end subroutine Project
  
  subroutine VortexRemove(Uut_xd,Uvr_xd,Uvo_xd)
    type(colour_matrix), dimension(:,:,:,:,:), intent(in)  :: Uut_xd,Uvo_xd
    type(colour_matrix), dimension(:,:,:,:,:), intent(out)  :: Uvr_xd

    integer :: ix,iy,iz,it,imu
    type(colour_matrix) :: adjZ
    type(colour_matrix) :: Uh

    do imu=1,4;do it=1,nlt; do iz=1,nlz; do iy=1,nly; do ix=1,nlx
       adjZ = centreadjoint(Uvo_xd(ix,iy,iz,it,imu))
       call MultiplyMatMat(Uh,Uut_xd(ix,iy,iz,it,imu),adjZ)
       Uvr_xd(ix,iy,iz,it,imu) = Uh
    enddo;enddo;enddo;enddo;enddo

  end subroutine VortexRemove

!! Takes the adjoint of a centre element
  function centreadjoint(Z)
    type(colour_matrix) :: Z
    type(colour_matrix) :: centreadjoint

    integer :: ix
    
    centreadjoint = Z
    do ix=1,3
       centreadjoint%cl(ix,ix) = CONJG(centreadjoint%cl(ix,ix))
    enddo   

  end function centreadjoint

end module projectionops
