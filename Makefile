BIN = ./bin/
SRC = ./src/
COLA = ./colasrc/
SUBDIRS = ${BIN} ${SRC} ${COLA}

all: ${SUBDIRS}

${SUBDIRS}: depends
	${MAKE} -C $@

# Generate dependency files
depends:
	mkdeps.py ${BIN} ${SRC} ${COLA}

${BIN}: ${SRC} ${COLA}

${SRC}: ${COLA}

# Remove non-cola .mod .o .x files
clean:
	$(foreach dir, ${BIN} ${SRC}, ${MAKE} -C ${dir} clean;)

# Clean cola as well
allclean:
	$(foreach dir, ${SUBDIRS}, ${MAKE} -C ${dir} clean;)

.PHONY: clean all allclean depends ${SUBDIRS}
