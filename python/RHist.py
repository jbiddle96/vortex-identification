import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import yaml
import os
import glob
import pandas as pd
import pickle


def load_config(config_name):
    """
    Load config file and return UT, VO and VR config info
    """
    config_file = open(config_name, 'r')
    config = yaml.load(config_file, Loader=yaml.FullLoader)
    if 'general' in config:
        general = config.pop('general')
    else:
        general = None
    # result = [config[cfg] for cfg in config.keys()]
    return config, general


def load_data(file_list, nbins):
    ncfg = len(file_list)
    print(ncfg)
    hist = np.zeros(nbins)
    min_hist = 0.0
    max_hist = 1.0
    # width = max_hist - min_hist
    # interval = width / nbins
    # hist_bins = np.linspace(min_hist - interval/2, max_hist + interval/2, nbins + 1)
    # hist_bins = np.linspace(min_hist, max_hist + interval, nbins + 1)
    hist_bins = np.linspace(min_hist, max_hist, nbins + 1)
    print(hist_bins)
    # print(hist_bins)
    for filename in file_list:
        print('Loading', filename)
        this_data = np.loadtxt(filename)
        # hist += np.histogram(this_data, bins=hist_bins, density=True)[0]
        counts, bins = np.histogram(this_data, bins=hist_bins)
        hist = hist + counts

    hist = hist / np.sum(hist)

    return hist, hist_bins


def load_hist(file_list):
    """Load pre-binned data

    :param file_list: List of files to load hist data from
    :returns: counts, bins

    """

    ncfg = len(file_list)

    columns = ['bins', 'counts']
    counts = None
    for f in file_list:
        # print(f)
        data = pd.read_csv(f, sep=r'\s+', header=None, names=columns)
        if counts is None:
            counts = data['counts'].to_numpy()
        else:
            counts += data['counts'].to_numpy()

    bins = data['bins'].to_numpy()
    interval = bins[1] - bins[0]
    # Insert the lower bound for the bins
    bins = np.insert(bins, 0, bins[0] - interval)

    counts = counts / np.sum(counts)

    return counts, bins


if __name__ == '__main__':

    config, general = load_config('config.yml')

    data_type = general.get('data_type', 'hist')
    nbins = general.get('nbins', 20)
    generate_data = general.get('generate_data', True)

    mpl.rc('text', usetex=True)
    mpl.rc('font', family='serif', size=16)
    mpl.rcParams['axes.unicode_minus'] = False
    mpl.rc('text.latex', preamble=r'\usepackage{amsmath}')

    data_dir = './hist_data/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)

    for key in config:
        cfg = config[key]
        pattern = os.path.join(cfg['dir'], "*.RHist")
        file_list = sorted(glob.glob(pattern))
        max_files = cfg.get('max_files', len(file_list))
        file_list = file_list[:max_files]
        if data_type == 'raw':
            cfg['hist'] = load_data(file_list, nbins)
        elif data_type == 'hist':
            cfg['hist'] = load_hist(file_list)
        # if generate_data:
        #     cfg['hist'] = load_data(file_list, nbins)
        #     # Save pickle of hist data
        #     outname = os.path.join(data_dir, cfg['datafile'])
        #     print(f"Saving file {outname}")
        #     with open(outname, 'wb') as handle:
        #         pickle.dump(cfg['hist'], handle)
        # else:
        #     outname = os.path.join(data_dir, cfg['datafile'])
        #     print(f"Loading hist data from {outname}")
        #     with open(outname, 'rb') as handle:
        #         cfg['hist'] = pickle.load(handle)

    fig, ax = plt.subplots(constrained_layout=True)
    weights = []
    vals = []
    labels = []
    for key in config:
        cfg = config[key]
        hist = cfg['hist'][0]
        bins = cfg['hist'][1]
        vals.append(bins[:-1])
        weights.append(hist)
        labels.append(cfg['label'])
    # ax.hist(vals, bins, weights=weights, density=True, label=labels, rwidth=0.8)
    ax.hist(vals, bins, weights=weights, label=labels, rwidth=0.8)

    ax.set_xlabel("$R$")
    ax.set_ylabel("Link fraction")
    ax.legend()
    plotname = "RHist.pdf"
    fig.savefig(plotname, bbox_inches='tight')
    print(f"Generated plot {plotname}")
