import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os, sys
import re, mmap
from scipy.stats import norm, shapiro, kstest

#
orig_stdout = sys.stdout
outputFile = open('Results.txt', 'w')
sys.stdout = outputFile

serial_dir = "/g/data/e31/jb3708/data/MCG/UT/20x40_PG/Log/"
pll_dir = "/g/data/e31/jb3708/data/MCG/UT/20x40_PG_NewMCG/Logs/"

pattern = re.compile("\.MCG10000\.cfg(\d{2})")

# Get all log files in serial directory
all_serial_logs = []
for (dirpath, dirnames, filenames) in os.walk(serial_dir):
    all_serial_logs.extend(filenames)
    break

# Check which ones involve the final batch of 5000 sweeps
serial_logs = {}
for f in all_serial_logs:
    f = os.path.join(serial_dir, f)
    for i, line in enumerate(open(f)):
        match = re.search(pattern, line)
        if(match):
            serial_logs[match.group(1)] = f
            break

# Get all parallel log files
pll_logs = {}
pattern = re.compile("\.cfg(\d{2})-MCG20000\.log")
for (dirpath, dirnames, filenames) in os.walk(pll_dir):
    for name in filenames:
        match = re.search(pattern, name)
        if(match):
            pll_logs[match.group(1)] = os.path.join(pll_dir, name)
    break


def getRvals(fileDict, pattern):
    '''
    Get the R values from files stored in fileDict
    according to pattern.
    '''
    rvals = {}
    for key in sorted(fileDict.keys()):
        with open(fileDict[key]) as logfl:
            data = mmap.mmap(logfl.fileno(), 0, prot=mmap.PROT_READ)
            match = re.search(pattern, data)
            if(match):
                rvals[key] = float(match.group(1))
    return rvals


pattern = b"20000\s+(0.\d+)"
pllFinalRval = getRvals(pll_logs, pattern)

pattern = b"\sFinalR\n\s\((0.\d+)"
serialFinalRval = getRvals(serial_logs, pattern)

pll_Rvals = []
serial_Rvals = []
for key in serialFinalRval.keys():
    pll_Rvals.append(pllFinalRval[key])
    serial_Rvals.append(serialFinalRval[key])

pll_Rvals = np.array(pll_Rvals)
serial_Rvals = np.array(serial_Rvals)
diff_Rvals = pll_Rvals - serial_Rvals

mu_pll, std_pll = norm.fit(pll_Rvals)
mu_serial, std_serial = norm.fit(serial_Rvals)
mu_diff, std_diff = norm.fit(diff_Rvals)

# Calculate expected normal distribution parameters
# from difference of two normal distributions
mu_expected = mu_pll - mu_serial
std_expected = np.sqrt(std_pll**2 + std_serial**2)

print("Parallel fit parameters:")
print("mean  = ", mu_pll)
print("sigma = ", std_pll)
print()
print("Serial fit parameters:")
print("mean  = ", mu_serial)
print("sigma = ", std_serial)
print()
print("Expected difference fit parameters:")
print("mean  = ", mu_expected)
print("sigma = ", std_expected)
print()
print("Actual difference fit parameters:")
print("mean  = ", mu_diff)
print("sigma = ", std_diff)
print()

# Test if pll_Rvals and serial_Rvals are normal
alpha = 0.05 # Acceptance probability
pll_pValue, pll_Wtest = shapiro(pll_Rvals)
serial_pValue, serial_Wtest = shapiro(serial_Rvals)

if(pll_pValue < alpha):
    print("Parallel R value is not normal")
else:
    print("Parallel R value is normal")
print("Parallel p value = ", pll_pValue)
print()

if(serial_pValue < alpha):
    print("Serial R value is not normal")
else:
    print("Serial R value is normal")
print("Serial p value = ", serial_pValue)
print()

# Test if pll_Rvals - serial_Rvals is normal
diff_pValue, diff_Wtest = shapiro(diff_Rvals)
if(diff_pValue < alpha):
    print("Diff R value is not normal")
else:
    print("Diff R value is normal")
print("Diff p value = ", diff_pValue)
print()

# Test if it is probable that pll_Rvals - serial_Rvals
# is drawn from the expected normal distribution
diff_Dtest, diff_kspValue = kstest(diff_Rvals, 'norm', args=(mu_expected, std_expected))

if(diff_kspValue < alpha):
    print("Diff R values deviate from expected")
else:
    print("Diff R values do not deviate from expected")
print("KS-test p value = ", diff_kspValue)

sys.stdout = orig_stdout
outputFile.close()

x = range(len(pll_Rvals))

fig, (ax1, ax2) = plt.subplots(nrows = 2, ncols=1, figsize=(15, 10))
ax1.scatter(x, pll_Rvals, label="Checkerboard")
ax1.scatter(x, serial_Rvals, label="Serial")

ymax = np.amax([pll_Rvals, serial_Rvals])
ymin = np.amin([pll_Rvals, serial_Rvals])
yrange = (ymax - ymin)/2.0
ymid = ymax - yrange

ymax = ymid + 1.1 * yrange
ymin = ymid - 1.1 * yrange

ax1.set_ylim(ymin, ymax)
ax1.legend()

nbins = 20
ax2.hist(pll_Rvals, nbins)
ax2.hist(serial_Rvals, nbins)
ax2.scatter(x, diff_Rvals)
plt.savefig(fname="Rvals_ScatterPlot.pdf", format="pdf", bbox_inches='tight')
