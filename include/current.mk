#OPTS = -O2 -ip -xHost -check bounds -traceback -assume byterecl -mkl=sequential -convert big_endian -heap_arrays 8192 -I$(MKLROOT)/include/intel64/lp64 -I$(MKLROOT)/include
#OPTS = -O2 -ip -xHost -assume byterecl -mkl=sequential -convert big_endian -heap_arrays 8192 -I$(MKLROOT)/include
#OPTS = -O2 -ip -xHost -assume byterecl -traceback -check bounds -mkl=sequential -convert big_endian -heap_arrays 8192 -mcmodel medium -shared-intel -i8 -I$(MKLROOT)/include -I$(MKLROOT)/include/intel64/lp64
#OPTS = -O2 -ip -xHost -assume byterecl -heap_arrays 8192
#LINK = -L../lib/ -lcola -lMCG -L$(MKLROOT)"/lib/intel64" -Wl,--start-group $(MKLROOT)"/lib/intel64"/libmkl_cdft_core.a $(MKLROOT)"/lib/intel64"/libmkl_intel_lp64.a $(MKLROOT)"/lib/intel64"/libmkl_intel_thread.a $(MKLROOT)"/lib/intel64"/libmkl_core.a $(MKLROOT)"/lib/intel64"/libmkl_blacs_openmpi_lp64.a -Wl,--end-group -openmp -lpthread -Wl,--no-whole-archive
#LINK = -L../lib/ -lcola -lMCG -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_sequential.a -Wl,--end-group -lpthread -lm

F90C = mpif90
F90FLAGS = -cpp $(OPTS)
OPTS = -O2 -ip -assume byterecl -convert big_endian -mkl=sequential #-I$(MKLROOT)/include/intel64/lp64 -I$(MKLROOT)/include

LINK = -L../lib/ -lcola # -lMCG #$(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_sequential.a -Wl,--end-group $(MKLROOT)/lib/intel64/libmkl_blacs_openmpi_lp64.a -lpthread -lm

INCLUDE = -I../colasrc/ -I../src/

# .SUFFIXES: .f90 .o .x .f

%.o: %.f90
	$(F90C) $(F90FLAGS) -c $(INCLUDE) $<

%.x: %.f90
	$(F90C) $(F90FLAGS) -o $@ $(INCLUDE) $^ $(LINK)
