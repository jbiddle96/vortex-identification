#!/bin/bash

workdir="/g/data/e31/jb3708/data/Random/UT/32x64_PG/"
dirlist.py "${workdir}" "su3*" ".lat" ".gag" ".cfgcheck"
file_list=${workdir}"dirlist.txt"

{
    read
    while read -r filename
    do
	qsub -v filename=$filename,workdir=$workdir MCGfix.sh
    done
}< "$file_list"
