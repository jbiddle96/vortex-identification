#!/bin/bash
#PBS -S /bin/bash
#PBS -P e31
#PBS -V
#PBS -q normal
#PBS -l ncpus=32,mem=32gb,walltime=10:00:00
#PBS -l storage=gdata/e31
#PBS -l wd
#PBS -j oe

ulimit -s unlimited

outdir=./
initguess="f"
initloc=/
gaugefieldformat=ildg
maxsweeps=20000
reunitarise_every=100

out="${outdir}$(basename "$filename")-MCG${maxsweeps}"

cat <<EOF > ${out}.lat
32
32
32
64
EOF

mpirun -np 32 ~/MCGfix/bin/MCGfix.x > ${out}".full_log" <<EOF
$filename
$out
$initguess
$initloc
$gaugefieldformat
$maxsweeps
$reunitarise_every
EOF

rm -f ${out}.lat

LogDir="./Logs/"
mkdir -p ${LogDir}
mv ${out}.log ${LogDir}
mv ${out}.full_log ${LogDir}

GaugeTransDir="./GaugeTrans/"
mkdir -p ${GaugeTransDir}
mv ${out}.MCGgag ${GaugeTransDir}
